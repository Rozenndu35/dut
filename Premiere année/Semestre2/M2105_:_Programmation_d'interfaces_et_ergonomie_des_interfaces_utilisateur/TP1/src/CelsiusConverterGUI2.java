public class CelsiusConverterGUI2 extends javax.swing.JFrame {

  /** Creates new form CelsiusConverterGUI */
  public CelsiusConverterGUI2() {
    initComponents();
  }

  /** This method is called from within the constructor to
   * initialize the form.
   */
  private void initComponents() {
    tempTextField = new javax.swing.JTextField(); 
    celsiusLabel = new javax.swing.JLabel();
    convertButton = new javax.swing.JButton();
    fahrenheitLabel = new javax.swing.JLabel();
    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE); //demande de fermer la fenetre lors ce que l'on touvhe la croix
    setTitle("Celsius Converter"); //nom de la fenetre
    celsiusLabel.setText("Celsius"); // label a cote de la saisie
    convertButton.setText("Convert"); // text dans le bouton
    convertButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        convertButtonActionPerformed(evt); // appele la convertion lors que l'on appuis sur le bouton
      }
    });
    fahrenheitLabel.setText("Fahrenheit"); // label a coter du bouton
    
    getContentPane().setLayout(new java.awt.GridLayout(2,2));
    add(tempTextField);
    add(celsiusLabel);
    add(convertButton);
    add(fahrenheitLabel);

    pack();
  }

// pour convertirs
  private void convertButtonActionPerformed(java.awt.event.ActionEvent evt) {
  //Parse degrees Celsius as a double and convert to Fahrenheit
    int tempFahr = (int)((Double.parseDouble(tempTextField.getText())) * 1.8 + 32);
    fahrenheitLabel.setText(tempFahr + " Fahrenheit");
  }

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new CelsiusConverterGUI2().setVisible(true);
      }
    });
  }

  private javax.swing.JLabel celsiusLabel;
  private javax.swing.JButton convertButton;
  private javax.swing.JLabel fahrenheitLabel;
  private javax.swing.JTextField tempTextField;
}
