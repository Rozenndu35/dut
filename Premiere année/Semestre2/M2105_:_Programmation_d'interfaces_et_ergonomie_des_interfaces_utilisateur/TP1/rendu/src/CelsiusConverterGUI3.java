public class CelsiusConverterGUI3 extends javax.swing.JFrame {

  /** Creates new form CelsiusConverterGUI */
  public CelsiusConverterGUI3() {
    initComponents();
  }

  /** This method is called from within the constructor to
   * initialize the form.
   */
  private void initComponents() {
    tempTextField1 = new javax.swing.JTextField(); 
    tempTextField2 = new javax.swing.JTextField(); 
    celsiusLabel = new javax.swing.JLabel();
    convertButton1 = new javax.swing.JButton();
    convertButton2 = new javax.swing.JButton();
    fahrenheitLabel = new javax.swing.JLabel();
    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE); //demande de fermer la fenetre lors ce que l'on touvhe la croix
    setTitle("Converter"); //nom de la fenetre
    celsiusLabel.setText("Celsius"); // label a cote de la saisie
    convertButton1.setText("C => F"); // text dans le bouton
    convertButton2.setText("F => C"); // text dans le bouton
    convertButton1.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        convertButtonActionPerformed1(evt); // appele la convertion lors que l'on appuis sur le bouton
      }
    });
    convertButton2.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        convertButtonActionPerformed2(evt); // appele la convertion lors que l'on appuis sur le bouton
      }
    });
    fahrenheitLabel.setText("Fahrenheit"); // label a coter du bouton
    
    getContentPane().setLayout(new java.awt.GridLayout(3,2));
    add(tempTextField1);
    add(celsiusLabel);
    add(tempTextField2);
    add(fahrenheitLabel);
    add(convertButton1);
    add(convertButton2);
    pack();
  }

// pour convertirs
  private void convertButtonActionPerformed1(java.awt.event.ActionEvent evt) {
  //Parse degrees Celsius as a double and convert to Fahrenheit
    int tempFahr = (int)((Double.parseDouble(tempTextField1.getText())) * 1.8 + 32);
    String temp = String.valueOf( tempFahr);
    tempTextField2.setText(temp);
  }
  private void convertButtonActionPerformed2(java.awt.event.ActionEvent evt) {
  //Parse degrees Celsius as a double and convert to Fahrenheit
    int tempCel = (int)(((Double.parseDouble(tempTextField2.getText())) -32) / 1.8);
    String temp = String.valueOf( tempCel);
    tempTextField1.setText(temp);
  }

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new CelsiusConverterGUI3().setVisible(true);
      }
    });
  }

  private javax.swing.JLabel celsiusLabel;
  private javax.swing.JButton convertButton1;
  private javax.swing.JButton convertButton2;
  private javax.swing.JLabel fahrenheitLabel;
  private javax.swing.JTextField tempTextField1;
  private javax.swing.JTextField tempTextField2;
}
