public class CelsiusConverterGUI extends javax.swing.JFrame {

  /** Creates new form CelsiusConverterGUI */
  public CelsiusConverterGUI() {
    initComponents();
  }

  /** This method is called from within the constructor to
   * initialize the form.
   */
  private void initComponents() {
    tempTextField = new javax.swing.JTextField(); 
    celsiusLabel = new javax.swing.JLabel();
    convertButton = new javax.swing.JButton();
    fahrenheitLabel = new javax.swing.JLabel();
    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE); //demande de fermer la fenetre lors ce que l'on touvhe la croix
    setTitle("Celsius Converter"); //nom de la fenetre
    celsiusLabel.setText("Celsius"); // label a cote de la saisie
    convertButton.setText("Convert"); // text dans le bouton
    convertButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        convertButtonActionPerformed(evt); // appele la convertion lors que l'on appuis sur le bouton
      }
    });
    fahrenheitLabel.setText("Fahrenheit"); // label a coter du bouton
    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane()); //disposition
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
          .addContainerGap()
          .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
              .addComponent(tempTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 
                javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE) // met la case a remplir en haut a gauche
              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(celsiusLabel)) // met le label celsius a cote
            .addGroup(layout.createSequentialGroup()
              .addComponent(convertButton) // met le bouton en dessous
              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(fahrenheitLabel))) // met le label fahrenheit a cote
          .addContainerGap(27, Short.MAX_VALUE))
      );
    layout.linkSize(javax.swing.SwingConstants.HORIZONTAL,
      new java.awt.Component[] {convertButton, tempTextField}); // met le bouton et la case a remplir sur la meme colone
    layout.setVerticalGroup( 
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING) // met sur la meme ligne tempTextField et le label
        .addGroup(layout.createSequentialGroup()
          .addContainerGap()
          .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(tempTextField, javax.swing.GroupLayout.PREFERRED_SIZE,
            javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(celsiusLabel))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)// met sur la meme ligne le bouton et le label
          .addComponent(convertButton)
          .addComponent(fahrenheitLabel))
        .addContainerGap(21, Short.MAX_VALUE))
    );
    pack();
  }

// pour convertirs
  private void convertButtonActionPerformed(java.awt.event.ActionEvent evt) {
  //Parse degrees Celsius as a double and convert to Fahrenheit
    int tempFahr = (int)((Double.parseDouble(tempTextField.getText())) * 1.8 + 32);
    fahrenheitLabel.setText(tempFahr + " Fahrenheit");
  }

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new CelsiusConverterGUI().setVisible(true);
      }
    });
  }

  private javax.swing.JLabel celsiusLabel;
  private javax.swing.JButton convertButton;
  private javax.swing.JLabel fahrenheitLabel;
  private javax.swing.JTextField tempTextField;
}
