Brownies
Image:
../data/imgRecette/brownies.jpg
Ingredient:
• 90 g de farine
• 200 g de chocolat
• 180 g de sucre en poudre
• 3 oeufs
• 180 g de beure
• 90 g de cerneaux de noix
Recette:
• Préchauffer le four à 200°
• Mélanger les oeufs et le sucre
• Faire fondre le beurre et le chocolat au bain marie
• Ajouter les deux mélanges
• Mélanger et ajouter la farine et les noix
• Baisser la température du four à 180
• Enfourner dans un moule pendant 35 min
• Laisser refroidir