package Vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.nio.Buffer;

import javax.swing.*;

import Controleur.BoutonPrecedent;
import Controleur.BoutonRecette;
import Controleur.BoutonSuivant;
import Modele.Recette;

/**
 * the page xithe the instruction for the recipe
 * @author rozenn
 * @version 1
 */
public class PageInstruction extends Page {

	private JButton suivant;
	private JButton precedent;
	private JLabel textAttention;
	private JLabel intro;
	private JList liste;
	
	/**
	 * generate a page instruction 
	 * described by :
	 * @param recette of it
	 * @param i : number of the page
	 * @param f : of the window
	 */
	public PageInstruction ( Recette recette , int i, Fenetre f){
		super(recette.getNomRecette(), i, f);
		
		//Creer la vue (panel) des instruction
		JPanel centre = new JPanel();
		JPanel gauche = new JPanel();
		String textLabel = "";
		if (i == 1) {
			intro = new JLabel("Ingredient"); 
			DefaultListModel listModel = new DefaultListModel();
			for(int j = 0; j < recette.getIngredient().size();j++){
			    listModel.addElement(recette.getIngredient().get(j));
			}
			liste = new JList(listModel);
			textLabel = "Risque de coupure (couteau) et de chute d'objets (ingredients)";
		}
		if (i == 2) {
			intro = new JLabel("Recette"); 
			DefaultListModel listModel = new DefaultListModel();
			for(int j = 0; j < recette.getRecette().size();j++){
			    listModel.addElement(recette.getRecette().get(j));
			}
			liste = new JList(listModel);
			textLabel = "Ne renverser pas a cote et risque de brulure , de debordement";
		}
		if (i == 3) {
			intro = new JLabel("Ingredient"); 
			DefaultListModel listModel = new DefaultListModel();
			for(int j = 0; j < recette.getIngredient2().size();j++){
			    listModel.addElement(recette.getIngredient2().get(j));
			}
			liste = new JList(listModel);
			textLabel = "Risque de coupure avec le couteau";
		}
		if (i == 4) {
			intro = new JLabel("Recette"); 
			DefaultListModel listModel = new DefaultListModel();
			for(int j = 0; j < recette.getRecette2().size();j++){
			    listModel.addElement(recette.getRecette2().get(j));
			}
			liste = new JList(listModel);
			textLabel = "Risque de brulure";
		}
		if (i == 5) {
			intro = new JLabel("Recette"); 
			DefaultListModel listModel = new DefaultListModel();
			for(int j = 0; j < recette.getAssemblage().size();j++){
			    listModel.addElement(recette.getAssemblage().get(j));
			}
			liste = new JList(listModel);
			textLabel = "Risque de brulure";
		}
		gauche.add(BorderLayout.NORTH , intro);
		Image imR = new ImageIcon( recette.getPathImgRecette()).getImage().getScaledInstance(300, 300, Image.SCALE_DEFAULT);
		gauche.add(BorderLayout.SOUTH, new JLabel(new ImageIcon(imR)));
		liste.setBackground(Color.GRAY);
		liste.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		liste.setForeground(Color.WHITE);

		liste.setOpaque(true);
		centre.add(BorderLayout.WEST , gauche);
		centre.add(BorderLayout.EAST , liste);
		
		//Creer la ligne en bas
		JPanel attention = new JPanel();
		JPanel info = new JPanel();
				

		precedent = new JButton("Precedent");
		precedent.addActionListener(new BoutonPrecedent(this, this.fenetre));
		Image img2 = (new ImageIcon("data/warn.PNG").getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		JLabel imgWarn= new JLabel(new ImageIcon(img2));
		textAttention=new JLabel(textLabel);
		textAttention.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		suivant= new JButton("Suivant");
		suivant.addActionListener(new BoutonSuivant(this, this.fenetre));
		
		attention.add(BorderLayout.WEST , imgWarn);
		attention.add(BorderLayout.CENTER , textAttention);
		
		info.add(BorderLayout.WEST , precedent);
		info.add(BorderLayout.CENTER , attention);
		info.add(BorderLayout.EAST , suivant);

		this.setLayout(new BorderLayout());
		this.add(BorderLayout.CENTER, centre);
		this.add(BorderLayout.SOUTH,info);

	
		this.setPreferredSize(new Dimension(850,400));
		
		
	}

}