/**
 * The panel withe the buton to go in the menu
 * @author rozenn
 * @version 1
 */
package Vue;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Controleur.BoutonMenu;

import java.awt.Color;


public class PanelHaut extends JPanel{
	private String imgPath;
	private JLabel menu;
	/**
	 * the constructor
	 */
	public PanelHaut() {
		imgPath="data/Haut.PNG";
		menu = new JLabel(new ImageIcon(imgPath));
		add(menu);
	}
	
	/**
	 * get the Label
	 * @return the label of the menu
	 */
	public JLabel getTheLabel() {
		return this.menu;
		
	}
}