package Vue;

import java.awt.CardLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Controleur.BoutonMenu;
/**
 * See constructor
 * @author rozenn
 * @version 1
 */
public class Fenetre extends JFrame{

	private JPanel centre;
	private PanelBas bas;
	private PanelHaut haut;
	private ListePageRecette lPRecette;
	
	/**
	 * Create the main window comporting A header (haut) and a footer (bas) and a center part which contain the
	 * list of the recette (with img/button)
	 * it uses a cardLayout to then show the content of a recette after clicking on a item of the recette list 
	 */
	
	public Fenetre() {
		lPRecette = new ListePageRecette(this);
		this.centre= new JPanel(new CardLayout());
		this.haut = new PanelHaut();
		
		centre.add(new Menu(this),"menu");
		
		
		this.bas = new PanelBas();
		
		setLayout(new java.awt.BorderLayout());
		add(this.haut, java.awt.BorderLayout.NORTH);
		add(this.bas, java.awt.BorderLayout.SOUTH);
		add(this.centre, java.awt.BorderLayout.CENTER);
		
		this.haut.getTheLabel().addMouseListener(new BoutonMenu(this));
		
		this.setResizable(false);
		this.pack();
		this.setVisible(true);
		this.centre.add(new JLabel("Recette terminee"),"term");
		this.ShowMenu();
		
		
	}
	
	/**
	 * Show the menu (main menu)
	 */
	public void ShowMenu() {
		CardLayout cl = (CardLayout)(this.centre.getLayout());
        cl.show(this.centre, "menu");
	}
	/**
	 * Add a recette to the layout
	 * @param accueilRecette : The page of the acceuil
	 */
	public void addRecettePanel(AccueilRecette accueilRecette) {
		this.centre.add(accueilRecette,accueilRecette.getRecetteName());
	}
	/**
	 * Add a page to the layout
	 * @param p : a page
	 * @param str : a message
	 */
	public void addPage(Page p, String str) {
		this.centre.add(p,str);
	}
	/**
	 * Show a specific page (recette)
	 * @param str : a name
	 */
	public void showPage(String str) {
		CardLayout cl = (CardLayout)(this.centre.getLayout());
        cl.show(this.centre, str);
	}
	/**
	 * Add a message  useful for the term page
	 * @param message : the message in the center
	 */
	public void addMessage(String message) {
		this.centre.add(new JLabel(message),"term");
	}
	/**
	 * Show a specific recette according to the layout
	 * @param rec : the name
	 */
	public void showRecette(String rec) {
		CardLayout cl = (CardLayout)(this.centre.getLayout());
        cl.show(this.centre, rec);
	}
	/**
	 * Get the recette list
	 * @return the list of the page
	 */
	public ListePageRecette getListPageRecette() {
		return this.lPRecette;
	}
	
}