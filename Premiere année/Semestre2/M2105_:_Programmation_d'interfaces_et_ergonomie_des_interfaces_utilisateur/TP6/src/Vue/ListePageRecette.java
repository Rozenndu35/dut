package Vue;
/**
 * the list of the frame
 * @author rozenn
 * @version 1
 */
import java.util.ArrayList;
import Modele.Recette;

public class ListePageRecette {
	
	private ArrayList<Page> listePage;
	private Fenetre fenetre;
	/**
	 * the constructor
	 * @param f : the frame
	 */
	public ListePageRecette( Fenetre f) {
		this.fenetre= f;
		this.listePage = new ArrayList();
	}
	/**
	 * Add a page to the list (page instruction if greater than 0)
	 * @param re : a recipe
	 * @param i ; the number of the page
	 */
	public void addPage(Recette re , int i) {
		Page page;
		if (i>0) {
			page = new PageInstruction ( re ,i, this.fenetre);
			this.fenetre.addPage(page, page.getNomRecette()+page.getNumeroPage());
		}
		else {
			page = new AccueilRecette(re, this.fenetre);
			this.fenetre.addPage(page, page.getNomRecette()+"0");
		}
		this.listePage.add(page);
	}
	/**
	 * Get a specific page of the list
	 * @param nom : the name of the page
	 * @param num : the number of the page
	 * @return the page to want
	 */
	public Page getPage(String nom, int num) {
		Page laPage = null;
		for(Page p : this.listePage) {
			if (p.getNomRecette() == nom && p.getNumeroPage() == num ) {
				laPage = p;
			}
		}
		return laPage;
		
	}
}
