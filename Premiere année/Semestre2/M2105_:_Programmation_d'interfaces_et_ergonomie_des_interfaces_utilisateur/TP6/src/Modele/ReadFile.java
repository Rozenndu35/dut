/**
 * the class to read a file and put in arraylist
 * @author rozenn
 * @version1
 */
package Modele;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ReadFile {
	
	/**
	 * The constructor
	 * @param fileName : the name of the file with the recipe
	 * @return a arraylist with the information in the file
	 */
	public ArrayList<String> readFile(String fileName) {
		
		ArrayList<String> list = new ArrayList<String>();
		FileReader file = null;
		// recupere les informations
		try {
			file = new FileReader (fileName);
	
			BufferedReader in = new BufferedReader(file);
			String s = null;
			try {
				s = in.readLine();
				while(s!=null){
					list.add(s);
					s=in.readLine();
				}
				in.close();	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}

}