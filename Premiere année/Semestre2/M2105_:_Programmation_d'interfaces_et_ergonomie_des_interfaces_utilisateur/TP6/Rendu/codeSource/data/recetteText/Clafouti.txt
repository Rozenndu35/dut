Clafouti
Image:
../data/imgRecette/Clafoutis.png
Ingredient:
• 600 g de cerise
• 40 g de beure demi-sel
• 4 oeufs
• 20 cl de lait
• 100 g de farine
• 60 g de sucre 
• 1 sachet de sucre vanillé
• 1 pincée de sel 
Recette:
• Préchauffez le four à 210°C (thermostat 7).
• Lavez les cerises et égouttez-les.
• Faites fondre le beure dans une casserole 
• Mélangez la farine, le sucre,le sucre vanillé et  le sel
• Ajouter les oeufs et le lait petit à petit en continuant de mélanger. 
• Ajoutez le beurre fondu.
• beurrez le plat, mettre les cerises et versez la pâte
• Mettez au four 10 min à 210°C puis 20min à 180°C 