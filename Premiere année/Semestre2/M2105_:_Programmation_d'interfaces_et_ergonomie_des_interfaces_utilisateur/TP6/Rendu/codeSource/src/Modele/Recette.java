/**
 * the class to make the information of a recipe
 * @author rozenn
 * @version 1
 */
package Modele;

import java.util.ArrayList;

import Vue.ListePageRecette;

public class Recette {

	private String nomRecette;
	private String pathImgRecette;
	private ArrayList<String> ingredient;
	private ArrayList<String> recette;
	private ArrayList<String> ingredient2;
	private ArrayList<String> recette2;
	private ArrayList<String> assemblage;
	private ListePageRecette listPageRecette;
	/**
	 * the constructor
	 * @param fileName : the name of the file to initialize
	 * @param listePage : the liste of the page
	 */
	public Recette (String fileName, ListePageRecette listePage) {
		ReadFile read = new ReadFile();
		ArrayList<String> list = read.readFile(fileName);
		this.listPageRecette = listePage;
		this.nomRecette = "";
		this.ingredient= new ArrayList();
		this.recette =  new ArrayList();
		this.ingredient2 = null;
		this.recette2 = null;
		this.assemblage = null;
		this.pathImgRecette="";
		// initialise les valeurs
		int i =0;
		String line = (String) list.get(i);
		this.nomRecette = line.trim();
		i = i+2;
		line = (String) list.get(i);
		this.pathImgRecette = line.trim();
		this.listPageRecette.addPage(this, 0);
		i = i+2;
		line = (String) list.get(i);
		line = line.trim();
		while (line.compareTo("Recette:")!= 0) {
			this.ingredient.add( line);
			i++;
			line = (String) list.get(i);
			line = line.trim();
		}
		this.listPageRecette.addPage(this, 1);
		i++;
		line = (String) list.get(i);
		line = line.trim();
		while (line.compareTo("Ingredient:")!= 0 && i < list.size()) {
			this.recette.add( line);
			i++;
			if (i < list.size()) {
				line = (String) list.get(i);
				line = line.trim();
			}
			
		}
		this.listPageRecette.addPage(this, 2);
		if (i < list.size()) {
			this.ingredient2=new ArrayList();
			this.recette2 = new ArrayList();
			this.assemblage =new ArrayList();
			i++;
			line = (String) list.get(i);
			line = line.trim();
			while (line.compareTo("Recette:")!= 0) {
				this.ingredient2.add( line);
				i++;
				line = (String) list.get(i);
				line = line.trim();
			}
			this.listPageRecette.addPage(this, 3);
			i++;
			line = (String) list.get(i);
			line = line.trim();
			while (line.compareTo("Assemblage:")!= 0) {
				this.recette2.add( line);
				i++;
				line = (String) list.get(i);
				line = line.trim();
			}
			this.listPageRecette.addPage(this, 4);
			
			i++;
			line = (String) list.get(i);
			line = line.trim();
			while (i < list.size()-1) {
				this.assemblage.add( line);
				i++;
				line = (String) list.get(i);
				line = line.trim();
			}
			this.listPageRecette.addPage(this, 5);
		}		
	}
	
	/**
	 * get recipe name
	 * @return the name
	 */
	public String getNomRecette() {
		return this.nomRecette;
	}
	
	/**
	 * get the path of the image
	 * @return the path of the image
	 */
	public String getPathImgRecette() {
		return this.pathImgRecette;
	}
	
	/**
	 * get recipe ingredients
	 * @return the ingredients
	 */
	public ArrayList<String> getIngredient() {
		return this.ingredient;
	}

	/**
	 * get recipe
	 * @return the recipe
	 */
	public ArrayList<String>  getRecette() {
		return this.recette;
	}
	
	/**
	 * get recipe ingredients when they have two different recipe in the same recipe
	 * @return the ingredients
	 */
	public ArrayList<String>  getIngredient2() {
		return this.ingredient2;
	}

	/**
	 * get recipe when they have two different recipe in the same recipe
	 * @return the recipe
	 */
	public ArrayList<String>  getRecette2() {
		return this.recette2;
	}

	/**
	 * get recipe when they have two different recipe in the same recipe and we assemble them
	 * @return the recipe
	 */
	public ArrayList<String>  getAssemblage() {
		return this.assemblage;
	}
}