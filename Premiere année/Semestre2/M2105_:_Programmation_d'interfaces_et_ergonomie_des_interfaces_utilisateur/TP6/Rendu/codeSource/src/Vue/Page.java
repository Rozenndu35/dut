package Vue;

import javax.swing.JPanel;
/**
 * the page (abstract class)
 * @author rozenn
 * @version 1
 */
public abstract class Page extends JPanel {

	protected String nomPage;
	protected int numeroPage;
	protected Fenetre fenetre;
	

	/**
	 * the costructor
	 * @param nomRecette : the name of the recipe
	 * @param i : the number of the page
	 * @param f : the frame
	 */
	public Page(String nomRecette, int i, Fenetre f) {
		this.nomPage = nomRecette;
		this.numeroPage = i;
		this.fenetre = f;
	}
	
	/**
	 * get The name of the recette
	 * @return return the name of the panel
	 */
	public String getNomRecette() {
		return this.nomPage;
	}

	/**
	 * get the page number
	 * @return the number of the panel
	 */
	public int getNumeroPage() {
		return this.numeroPage;
	}


}