package Vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.Border;

import Controleur.BoutonSuivant;
import Modele.Recette;
/**
 * See constructor
 * @author rozenn
 *
 */
public class AccueilRecette extends Page {

	private JLabel imgLabel;
	private JButton suivant;
	private JLabel text;
	
	/**
	 * Create a page (Jpanel) including a recette and then create the welcom page of it
	 * @param recette : the recipe
	 * @param fenetre : the window
	 */
	public AccueilRecette(Recette recette, Fenetre fenetre){
		super(recette.getNomRecette(), 0, fenetre);
		//Creer la vue (panel) des recettes
		JPanel info = new JPanel();
		//On aligne sur 3 colomnes avec autant de lignes que necessaire
		BorderLayout gd = new BorderLayout();
		//On set le layout a la vue
		info.setLayout(gd);
		// On modifie l'espacement horizontal/vertical du layout
		gd.setHgap(50);
		gd.setVgap(50);
		
		Image img = (new ImageIcon(recette.getPathImgRecette()).getImage().getScaledInstance(350, 350, Image.SCALE_DEFAULT));
		//Cr�e le label avec l'image precedemment recuperee
		imgLabel = new JLabel(new ImageIcon(img));
		
		Image img2 = (new ImageIcon("data/warn.PNG").getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		JLabel imgWarn= new JLabel(new ImageIcon(img2));
		
		

		text=new JLabel("Lavez vous les main avant e commencer");

		text.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		suivant= new JButton("Suivant");
		suivant.addActionListener(new BoutonSuivant(this, this.fenetre));
		//info.setPreferredSize(new Dimension(1500,30));
		
		info.add(BorderLayout.WEST,imgWarn);
		info.add(BorderLayout.CENTER,text);
		info.add(BorderLayout.EAST,suivant);
		
		
		BorderLayout bl = new BorderLayout();
		this.setLayout(bl);
		bl.setVgap(20);
		
		this.add(BorderLayout.CENTER,imgLabel);
		this.add(BorderLayout.SOUTH,info);

	
		//this.setPreferredSize(new Dimension(850,500));
		
		
	}
	
	/**
	 * get the nema of the page
	 * @return the name
	 */
	public String getRecetteName() {
		return this.nomPage;
	}
}