package Vue;

/**
 * the panel in the SOUTH)
 * @author rozenn
 * @version 1
 */
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PanelBas extends JPanel{
	private String imgPath;
	/**
	 * Create the footer with the image footer
	 */
	public PanelBas() {
		imgPath="data/Bas.PNG";
		
		//Ajoute d'un label contenant une imageIcon au Path indiqu� 
		add(new JLabel(new ImageIcon(imgPath)));
	}

}