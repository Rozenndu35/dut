/**
 * make the page for the menu
 * @author rozenn
 * @version 1
 */
package Vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.*;

import Controleur.BoutonRecette;
import Modele.Recette;

public class Menu extends Page {

	private int largeurImage=200;
	private int hauteurImage=200;
	private ArrayList<Recette> recettes;
	Fenetre fenetre;
	/**
	 * The constructor
	 * the menu will be at the center part of the window containing as much panel of recette as the number of recette
	 * a recette panel contains a image and a button 
	 * @param fenetre : the window
	 */
	public Menu(Fenetre fenetre) {
		super("menu ", 0, fenetre);
		this.fenetre=fenetre;
		//Creer la vue (panel) des recettes
		JPanel view = new JPanel();
		//On aligne sur 2 colomnes avec autant de lignes que necessaire
		GridLayout gd = new GridLayout(0,3);
		//On set le layout a la vue
		view.setLayout(gd);
		// On modifie l'espacement horizontal/vertical du layout
		gd.setHgap(20);
		gd.setVgap(20);


		this.recettes= new ArrayList<Recette>();
		recettes.add(new Recette("../data/recetteText/Crepe.txt", this.fenetre.getListPageRecette()));
		recettes.add(new Recette("../data/recetteText/Brownies.txt",  this.fenetre.getListPageRecette()));
		recettes.add(new Recette("../data/recetteText/TarteAuxFraise.txt",  this.fenetre.getListPageRecette()));
		recettes.add(new Recette("../data/recetteText/Tiramisu.txt",  this.fenetre.getListPageRecette()));
		recettes.add(new Recette("../data/recetteText/Gauffre.txt",  this.fenetre.getListPageRecette()));
		recettes.add(new Recette("../data/recetteText/Clafouti.txt",  this.fenetre.getListPageRecette()));
		
		
		//Pour chaque recette dans this.recettes, on ajoute a la vue (panel) un item (panel) de recette avec son image et son nom
		for(Recette r : this.recettes) {
			view.add(this.getMenuItem(r.getPathImgRecette(), r.getNomRecette()));
			fenetre.addRecettePanel(new AccueilRecette(r, this.fenetre));
		}
		
		
		

		// On met la vue des recettes dans un ScrollPane = Vue d�roulante
		JScrollPane sp = new JScrollPane(view);
		
		int larg = (int) view.getSize().getWidth();
		// On definis la taille de la vue d'eroulante 
		sp.setPreferredSize(new Dimension(840,400));
		// Et enfin on ajoute a ce panel la vue d'eroulante de la vue des recettes
		this.add(sp);
	}

	/**
	 * Make a panel with the image and the button
	 * @param imgPath : the path of the image
	 * @param name : the name of the recipe
	 * @return the new panel
	 */
	public JPanel getMenuItem(String imgPath, String name){
		JPanel ret= new JPanel();
		//Notre layout en BorderLayout avec un leger VGap (espacement vertical)
		ret.setLayout(new BorderLayout(0,10));
		
		//Recupere l'image en fonction du chemin indique et la met automatiquement a une taille de largeurImage*hauteurImage
		Image img = (new ImageIcon(imgPath).getImage().getScaledInstance(largeurImage, hauteurImage, Image.SCALE_DEFAULT));
		//Cree le label avec l'image precedemment recuperee
		JLabel image = new JLabel(new ImageIcon(img));
		//Creer le boutton avec le nom 
		JButton bouton = new JButton(name);
		bouton.addActionListener(new BoutonRecette(this.fenetre, name));
		//Force la taille du boutton
		bouton.setPreferredSize(new Dimension(60,30));
		bouton.revalidate();
		
		//Disposition
		ret.add(java.awt.BorderLayout.NORTH, image);
		ret.add(java.awt.BorderLayout.SOUTH, bouton);
		
		return ret;
	}

}