import Vue.Fenetre;
/**
 * Launch the app
 * @author rozenn
 *
 */
public class Launcher {

	public static void main(String[] args) {
		 java.awt.EventQueue.invokeLater(new Runnable() {
	            public void run() {
	            	new Fenetre();
	                }
	        });
	}

}
