package Controleur;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import Modele.Recette;
import Vue.*;

public class BoutonRecette implements ActionListener {
	private Fenetre fen;
	private String rec;
	
	/**
	 * The constructor
	 * @param fen : the window
	 * @param rec : teh message in the frame
	 */
	public BoutonRecette(Fenetre fen, String rec) {
		this.fen=fen;
		this.rec=rec;
	}

	/**
	 * Method called when the button is tregered
	 * It will show the Recette menu depending of the button (fraise, chocolat...)
	 * @param e : the event
	 */
	public void actionPerformed(ActionEvent e) {
		this.fen.showRecette(rec);

	}

}