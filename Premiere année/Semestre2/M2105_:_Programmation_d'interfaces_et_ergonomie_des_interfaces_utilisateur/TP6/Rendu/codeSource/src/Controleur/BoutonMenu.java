package Controleur;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import Vue.*;

public class BoutonMenu implements MouseListener {
	private Fenetre fenetre;
	
	/**
	 * constructor taking a Fenetre to manipulate
	 * @param fenetre : the window
	 */
	
	public BoutonMenu(Fenetre fenetre) {
		this.fenetre=fenetre;
	}

	/**
	 * methods called when mouse clicked
	 * it will show the menu of the application
	 * @param arg0 : Event receive during the click
	 */

	@Override
	public void mouseClicked(MouseEvent arg0) {
		fenetre.ShowMenu();
		
	}
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


}