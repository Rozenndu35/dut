package Controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Vue.*;

public class BoutonPrecedent implements ActionListener{
	private Page actuelPage;
	private Fenetre fenetre;
	/**
	 * the constructor
	 * @param pageAct : the actual frame
	 * @param f : the window
	 */
	public BoutonPrecedent(Page pageAct, Fenetre f) {
		actuelPage = pageAct;
		this.fenetre = f;
	}

	/**
	 * Action when previous button is triggered
	 * it will get the number of the page and decrease the current page if it's not below 0
	 * @param e : the event
	 */
	public void actionPerformed(ActionEvent e) {
		int i = actuelPage.getNumeroPage();
		String nom = actuelPage.getNomRecette();
		i--;
		Page nouvPage = this.fenetre.getListPageRecette().getPage(nom,i);// cherche dans la listedesPage
		if (nouvPage!= null) {
			this.fenetre.showPage(this.actuelPage.getNomRecette()+(i));
		}
		else {
			System.err.println("BoutonPrecedent :actionPerformed : la page indiquer ne possede pas de page precedente");
		}
	}

}