package Controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Vue.*;

public class BoutonSuivant implements ActionListener {
	private Page actuelPage;
	private Fenetre fenetre;
	/**
	 * the constructor
	 * @param pageAct : the actual frame
	 * @param f ; the window
	 */
	public BoutonSuivant( Page pageAct , Fenetre f) {
		this.actuelPage = pageAct;
		this.fenetre = f;
	}

	/**
	 * Method called when the button is tregered
	 * It will Go to the next page except if it's the last page (showing term page)
	 * @param e : the event
	 */
	public void actionPerformed(ActionEvent e) {
		int i = this.actuelPage.getNumeroPage();
		String nom = this.actuelPage.getNomRecette();
		i++;
		Page nouvPage = this.fenetre.getListPageRecette().getPage(nom,i);// cherche dans la listedesPage
		if (nouvPage!= null) {
			 this.fenetre.showPage(this.actuelPage.getNomRecette()+(i));
		}
		else {
			
			this.fenetre.showPage("term");
		}
	}

}