package chronometre;

import javax.swing.*;
/**
 * The Frame 
 * @author rozenn
 */
public class Fenetre extends JFrame {
	Timer timer;
	/**
	 * The constructor
	 */
	 public Fenetre() {
	        initComponents();
	    }
	 /**
	  * Init the component in the Frame
	  */
	 private void initComponents() {
		 setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE); //demande de fermer la fenetre lors ce que l'on touvhe la croix
		 setTitle("Chronometre"); //nom de la fenetre
		 Chronometre c = new Chronometre();
		 
		 
		 //Listener du Timer
		 TimerListener timerTaskListener = new TimerListener(c);
		 this.timer = new Timer(1000, timerTaskListener);
		 //timer.start(); //question1
		 
		    
		 
		// --------------------------------------- INTERACTION ----------------------------------------
		 BouttonBool cBB = new BouttonBool(c);
	     ButtonStartListener sSL = new ButtonStartListener(c, "SS", this);
	     ButtonStartListener sRL = new ButtonStartListener(c, "reset", this);
	     c.getChrono().addActionListener(cBB);
	     c.getTimer().addActionListener(cBB);
	     c.getStartStop().addActionListener(sSL);
	     c.getReset().addActionListener(sRL);
	     
	     
	     
	  // --------------------------------------- DISPOSITION ----------------------------------------
	     getContentPane().setLayout(new java.awt.BorderLayout());
	     add ( c , java.awt.BorderLayout.CENTER );
	     pack();
	 }
	 
	
     public static void main(String[] args) {
 		java.awt.EventQueue.invokeLater(new Runnable() {
 		      public void run() {
 		        new Fenetre().setVisible(true);
 		      }
 		    });

 	}
    
}
