package chronometre;

import java.text.*;
import java.util.*;
import javax.swing.*;
/**
 * The pannel of the frame
 * @author rozenn
 *
 */
public class Chronometre extends JPanel {
	private JRadioButton chrono;
	private JRadioButton timer;
	private JButton startStop;
	private JButton reset;
	private JLabel time;
	private JPanel panelH;
	private JPanel panelD;
	private JPanel panelG;
	private Date tempsChrono;
	private Format formatter;
	private JTextField minuteur;
	
	/**
	 * The constructor
	 */
	public Chronometre () {
		initComponet();
	}
	/**
	 * init the components
	 */
	public void initComponet() {
		
		
		chrono = new JRadioButton("Chrono", true);
		timer = new JRadioButton("Timer", false);
		ButtonGroup radioButtonGroup = new ButtonGroup();
		radioButtonGroup.add(chrono);
		radioButtonGroup.add(timer);
		
		formatter = new SimpleDateFormat("mm.ss");
		
		startStop = new JButton("Start");
		reset = new JButton("Reset");
		
		minuteur = new JTextField(formatter.format(new Date(0)));
		
	    
		panelH = new JPanel();
		panelD = new JPanel();
		panelG = new JPanel();
		initChrono();
		
		panelH.setLayout(new java.awt.GridLayout(1,2));
		panelH.add(chrono); panelH.add(timer);
		
		panelD.setLayout(new java.awt.GridLayout(2,1));
		panelD.add(startStop); 
		panelD.add(reset);
		
		 setLayout(new java.awt.BorderLayout()); 
	        add(panelH,java.awt.BorderLayout.NORTH );
	        add(panelD,java.awt.BorderLayout.EAST );
	        add(panelG,java.awt.BorderLayout.WEST);		
	}	
	
	//-------------------------------------------Set et Get des bouton -------------
	/**
	  * set the chrono button
	  * @param bool : the boolean to want changed
	  */
	 public void setChrono(boolean bool) {
	    	this.chrono.setSelected(bool);
	 }
	 /**
	  * set the timer button
	  * @param bool : the boolean to want changed
	  */
	 public void setTimer(boolean bool) {
	    	this.timer.setSelected(bool);
	 }
	 /**
		 * get the chrono
		 * @return true if is select
	*/
	 public boolean etatChrono() {
	    	return this.chrono.isSelected();
	 }
	 /**
		 * get the timer
		 * @return true if is select
	*/
	 public boolean etatTimer() {
	    	return this.timer.isSelected();
	 }
	 /**
		 * get the chrono
		 * @return the chrono button
	*/
	public JRadioButton  getChrono() {
		return this.chrono;
	}
	/**
	 * get the timer
	 * @return the Timer button
	 */
	public JRadioButton  getTimer() {
		return this.timer;
	}
	/**
	 * get the button startStop
	 * @return the button
	 */
	public AbstractButton getStartStop() {
		return this.startStop;
	}
	/**
	 * get the text in the button startStop
	 * @return the text
	 */
	public String getTextStart() {
		return startStop.getText();
	}
	/**
	 * set the text in the button startStop
	 * @param string : the new text
	 */
	public void setTextStartStop(String string) {
		startStop.setText(string);
	}
	/**
	 * enable the button reset when is true
	 * @param efface : the boolean to want actived or no the button
	 */
	public void enableReset(boolean efface) {
		this.reset.setEnabled(efface);
	}
	/**
	 * get the button reset
	 * @return the button
	 */
	public JButton getReset() {
		return this.reset;
	}
	 //------------------------------------------ change le temps lors du crhonometre ou du minuteur-------------------------
	 /**
	  * add one second on the label time
	  */
	 public void addSecToChrono() {
		 tempsChrono.setTime(tempsChrono.getTime()+1000);
		 this.time.setText(formatter.format(tempsChrono));
	 }
	 /**
	  * sub one second on the label time
	  */
	 public void subSecToChrono() {
		 tempsChrono.setTime(tempsChrono.getTime()-1000);
		 this.time.setText(formatter.format(tempsChrono));
	 }
	 
	 // -------------------------------------- 
	 /**
	  * reset the chorono at 00.00
	  */
	 public void resetToChrono() {
		 tempsChrono.setTime(0);
		 this.time.setText(formatter.format(tempsChrono));
	 }
	 
	 /**
	  * init the chrono 
	  */
	 public void initChrono() {
			//Créer la date utilisée pour le chrono
			tempsChrono = new Date(0);
			//Le label du chrono avec application du format
		    time = new JLabel(formatter.format(tempsChrono));
		    panelG.removeAll();
		    panelG.add(time);   
		    panelG.revalidate();
		    panelG.repaint();
		}
	
	/**
	 * 
	 */
	public void changePanelGToTimer() {
		
		panelG.removeAll();
		panelG.add(this.minuteur);
	    panelG.revalidate();
	    panelG.repaint();
	}
	/**
	 * change on convert in the panelG the TextField on JLabel
	 */
	public void changePanelG() {
		panelG.removeAll();
		//Créer la date utilisée pour le chrono
		
		String temps = this.minuteur.getText();
		String seconde = temps.substring(3,5);
		String minute = temps.substring(0,2);
		int tempsEnMili= Integer.valueOf(seconde)*1000 + Integer.valueOf(minute)*60000;
		
		tempsChrono = new Date(tempsEnMili);
		//Le label du chrono avec application du format
	    time = new JLabel(formatter.format(tempsChrono));
		panelG.add(time);
		panelG.revalidate();
	    panelG.repaint();
	}
	
	/**
	 * get the time on the chrono (for the timer)
	 * @return the timr
	 */
	public Date getTempsChrono() {
		return tempsChrono;
	}
	/**
	 * set the time in temps chrono
	 * @param tempsChrono : the new time
	 */
	public void setTempsChrono(Date tempsChrono) {
		this.tempsChrono = tempsChrono;
	}
	/**
	 * set the text in the minuter
	 */
	public void setTimer() {
		this.minuteur.setText(time.getText());
		
	}
	/**
	 * get the JLabel
	 * @return the label of the time
	 */
	public JLabel getTime() {
		return this.time;
	}
}
