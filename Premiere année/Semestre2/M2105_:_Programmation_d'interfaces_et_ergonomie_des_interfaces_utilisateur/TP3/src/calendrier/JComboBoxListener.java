
package calendrier;

import java.awt.event.*;

/**
 * Action of the Button change the month
 * @author rozenn
 *
 */
public class JComboBoxListener implements ActionListener{
		private Calendrier c;
		
		/**
		 * The constructor
		 * @param c: the panel calendrier
		 */
		public  JComboBoxListener(Calendrier c) {
			this.c=c;	
		}
		/**
		 * when they make action
		 */
		public void actionPerformed(ActionEvent e) {
			c.setMois(c.getMois().getSelectedIndex());
		}
}
