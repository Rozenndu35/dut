package calendrier;

import javax.swing.*;
/**
 * the Frame
 * @author rozenn
 *
 */
public class Fenetre extends JFrame{

	private Calendrier c ;
	/**
	 * The constructor
	 */
    public Fenetre() {
        initComponents();
    }
    /**
     * init the component in the Frame
     */
    private void initComponents() {
        
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE); //demande de fermer la fenetre lors ce que l'on touvhe la croix
        setTitle("Calendrier"); //nom de la fenetre
        
        c = new Calendrier();
        
        
        // --------------------------------------- INTERACTION ----------------------------------------
       
        // Création des Listener en passant en paramètre l'action de chaque bouttons 
        JComboBoxListener ch = new JComboBoxListener(c);
        UnJour ecout= new UnJour(c);
        c.addKeyListener(ecout);
        c.setFocusable(true);
        c.getMois().addKeyListener(ecout);
        //Attribution des Listener à chaque boutton
        c.getMois().addActionListener(ch);
        
                
        // ------------------------------------------------------ DISPOSITION --------------------------------------
        getContentPane().setLayout(new java.awt.BorderLayout());
        add ( c , java.awt.BorderLayout.CENTER);
        pack();
    }
    
	public static void main(String[] args) {
		java.awt.EventQueue.invokeLater(new Runnable() {
		      public void run() {
		        new Fenetre().setVisible(true);
		      }
		    });

	}

}