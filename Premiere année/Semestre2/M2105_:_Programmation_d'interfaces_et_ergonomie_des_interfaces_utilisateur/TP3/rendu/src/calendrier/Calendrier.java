package calendrier;

import javax.swing.*;
import java.awt.*;
import java.time.*;
import java.util.*;
/**
 *  the pannel
 * @author rozenn
 *
 */
public class Calendrier extends JPanel {
	private JLabel jour;
	private JLabel nombre;
	private JLabel annee;
	private JComboBox <String> mois;
	private Calendar calendrier;
	private String[] list;
	
	/**
	 * the constructor
	 */
	public Calendrier() {
		initComponet();
	}

	/**
	 * init the composent
	 */
	private void initComponet() {
		// -------------------------------------------------------------- initialise ------------------
		this.jour = new JLabel();
		 this.nombre = new JLabel();
		 this.annee = new JLabel();
		 this.mois = new JComboBox<String>();
		 this.list = new String[] {"janvier", "février", "mars","avril","mai","juin","juillet","août","septembre","octobre", "novembre", "décembre"};
		 this.mois = new JComboBox<String>(list);
		 this.calendrier = Calendar.getInstance();
		 this.jour.setText(String.valueOf(this.calendrier.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.FRENCH)));
		 this.nombre.setText(String.valueOf(this.calendrier.get(Calendar.DAY_OF_MONTH)));
		 String moisN = String.valueOf(this.calendrier.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.FRENCH));
		 int index = indeMois(moisN);
		 this.mois.setSelectedIndex(index);
		 this.annee.setText(String.valueOf(this.calendrier.get(Calendar.YEAR)));
		 
	        
	        
		// ------------------------------------------------------ DISPOSITION --------------------------------------
		 	setLayout(new java.awt.GridLayout(1,4)); 
	        add(jour);
	        add(nombre);
	        add(mois);
	        add(annee);
	        
		 
	}
	/**
	 * Calculate the index of the month
	 * @param mois : the month  
	 * @return the index of the month
	 */
	public int indeMois (String mois) {
		int index= -1;
		 int i = 0;
		 while (index == -1) {
			 if (this.list[i] == mois) {
				 index=i;
			 }
			 i++;
		 }
		return index;
		
	}
	
	/**
	 * when people want to change the month and calculate the day
	 * @param mois : the index of the new month
	 */
	public  void setMois(int mois) {
		this.calendrier.set(calendrier.MONTH,mois);
		
		int newMois=this.calendrier.get(calendrier.MONTH);
		if(mois!=newMois) {
			if (mois == 1) {

				if (new GregorianCalendar().isLeapYear(this.calendrier.get(Calendar.YEAR))) {
					this.changerJour(-1);
				}
				else {
					this.changerJour(-2);
				}
			}
			this.changerJour(-1);
			
		}
		
		 this.jour.setText(String.valueOf(this.calendrier.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.FRENCH)));
		 this.nombre.setText(String.valueOf(this.calendrier.get(Calendar.DAY_OF_MONTH)));
		 
		 String moisN = String.valueOf(this.calendrier.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.FRENCH));
		 int index = indeMois(moisN);
		 this.mois.setSelectedIndex(index);
		 this.annee.setText(String.valueOf(this.calendrier.get(Calendar.YEAR)));
	}
	/**
	 * Change the day 
	 * @param i : the number of the day to whant move
	 */
	public void changerJour(int i) {
		calendrier.add(calendrier.DAY_OF_MONTH, i);
		
		 this.jour.setText(String.valueOf(this.calendrier.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.FRENCH)));
		 this.nombre.setText(String.valueOf(this.calendrier.get(Calendar.DAY_OF_MONTH)));
		 
		 String moisN = String.valueOf(this.calendrier.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.FRENCH));
		 int index = indeMois(moisN);
		 this.mois.setSelectedIndex(index);
		 this.annee.setText(String.valueOf(this.calendrier.get(Calendar.YEAR)));
		
	}

	/**
	 * get the list of the month
	 * @return the ComboBox of the month
	 */
	public  JComboBox<String> getMois() {
		return this.mois;
	}
}
