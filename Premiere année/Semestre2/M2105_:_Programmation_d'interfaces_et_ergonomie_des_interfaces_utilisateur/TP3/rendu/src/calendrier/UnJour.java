package calendrier;


import java.awt.event.*;
/**
 * When people change the day with the console
 * @author rozenn
 *
 */
public class UnJour extends KeyAdapter{
	private Calendrier c;
	/**
	 * the constructor
	 * @param c : the calendrier panel
	 */
	public UnJour(Calendrier c) {
		this.c =c;
	}
	/**
	 * when people pressed the console
	 */
	public void keyPressed(KeyEvent evt) {
		int code = evt.getKeyCode();
		if (code == KeyEvent.VK_LEFT) {	//enlever	
			c.changerJour(-1);
			
		}
		else if(code == KeyEvent.VK_RIGHT) { //ajouter
			c.changerJour(1);
		}
	}
	public void keyReleased(KeyEvent evt) {}
	public void keyTyped(KeyEvent evt) {}

}