package chronometre;

import java.awt.event.*;

import javax.swing.*;

/**
 * Action of the BoutonRadio in Information for add a grade
 * @author rozenn
 *
 */
public class ButtonStartListener implements ActionListener{
		Chronometre c;
		String bouton;
		Fenetre f;
		String mode;
		/**
		 * The constructor 
		 * @param c the chonometre panel
		 * @param bouton the bouton to have an action
		 * @param f the Frame
		 */
		public  ButtonStartListener(Chronometre c, String bouton, Fenetre f){
			this.c=c;
			this.bouton = bouton;
			this.f = f;
			this.mode = mode;
		}
		/**
		 * when it have an action in the buttons
		 */
		public void actionPerformed(ActionEvent e){	
			if (this.c.getChrono().isSelected()) {
				if (bouton.compareTo("SS")==0 ) {
					if (c.getTextStart().compareTo("Start")==0) {
				    	c.setTextStartStop("Stop");
				    	c.enableReset(false);
				    	f.timer.start();
				    }
				    else if (c.getTextStart().compareTo("Stop")==0){
				    	c.setTextStartStop("Start");
				    	c.enableReset(true);
				    	f.timer.stop();
				    }
				}
				else {
					c.resetToChrono();
				}
			}
			else {
				if (bouton.compareTo("SS")==0 ) {
					if (c.getTextStart().compareTo("Start")==0) {
				    	c.setTextStartStop("Stop");
				    	c.enableReset(false);
				    	c.changePanelG();
				    	f.timer.start();
				    }
				    else if (c.getTextStart().compareTo("Stop")==0){
				    	c.setTextStartStop("Start");
				    	c.enableReset(true);
				    	f.timer.stop();
				    	c.setTimer();
				    }
				}
				else {
					c.resetToChrono();
					c.changePanelGToTimer();
					f.timer.stop();
				}
			}
		
		}
}