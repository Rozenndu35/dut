package chronometre;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TimerListener implements ActionListener{
	Chronometre c;
	/**
	 * The constructor
	 * @param c the pannel chronometre
	 */
	public  TimerListener(Chronometre c){
		this.c=c;
	}
	/**
	 * make a action with the timer at every seconds
	 */
	public void actionPerformed(ActionEvent e){
		
		if(c.getTimer().isSelected()) {
		
			if (c.getTempsChrono().getTime()<=5000) {
				c.getTime().setOpaque(true);
				c.getTime().setBackground(new Color(255,200,0));
			}
			if(c.getTempsChrono().getTime()==0) {
				java.awt.Toolkit.getDefaultToolkit().beep();
				c.setTextStartStop("Start");
		    	c.enableReset(true);
				c.getTime().setBackground(new Color(255,0,0));
			}else {
				c.subSecToChrono();
			}
			
		}else {
			c.addSecToChrono();
		}
		
		
	    
	}
}
