package chronometre;

import java.awt.event.*;
import javax.swing.*;
/**
 * Action of the BoutonRadio
 * @author rozenn
 */
public class BouttonBool implements ActionListener {
	Chronometre c;
	
	/**
	 *  The constructor
	 * @param c : the calendrier panel
	 */
	public BouttonBool(Chronometre c ) {
			this.c=c;
		}
	
	/**
	 * when it have an action in the Radio button
	 */
	public void actionPerformed(ActionEvent e) {
		if(this.c.getChrono().isSelected()) {
			c.initChrono();
		}else {
			c.changePanelGToTimer();	
		}
	}
}