public class TestEvaElev {
	private static int nbT =0;
	private static  int nbTR =0;
	public static void main(String[] args) {
		//getEleve();
		//setEleve();
		//getEvaluation();
		//setEvaluation();
		//System.out.println( "test reussi " + nbTR +" / " + nbT);
		
		ecrireFichierEleve();
		lectureFichierEleve();
		
		ecrireFichierEleveWithDOS();
		lectureFichierEleveWithDIS();
		
		ecrireFichierEleveWithOOS();
		lectureFichierEleveWithOIS();
	}
	private static void getEleve() {
		Eleve eleve = new Eleve( "Costiou","Rozenn");
		nbT ++;
		if(eleve.getNom().compareTo("Costiou")==0) { System.out.println("getNom test reussi");nbTR++;}
		else {System.out.println("getNom test erreur");}
		nbT ++;
		if(eleve.getPrenom().compareTo("Rozenn")==0) { System.out.println("getPrenom test reussi");nbTR++;}
		else {System.out.println("getPreom test erreur");}
	}
	public static void setEleve() {
		Eleve eleve = new Eleve( "Costiou","Rozenn");
		nbT ++;
		eleve.setNom("Davail");
		if(eleve.getNom().compareTo("Davail")==0) { System.out.println("setNom test reussi");nbTR++;}
		else {System.out.println("getNom test erreur");}
		nbT ++;
		eleve.setPrenom("Malo");
		if(eleve.getPrenom().compareTo("Malo")==0) { System.out.println("setPrenom test reussi");nbTR++;}
		else {System.out.println("getPreom test erreur");}
		
	}
	private static void getEvaluation() {
		Evaluation eval = new Evaluation (5, 3);
		nbT ++;
		if(eval.getNote() == 5) { System.out.println("getNote test reussi");nbTR++;}
		else {System.out.println("getNom test erreur");}
		nbT ++;
		if(eval.getCoef()==3) { System.out.println("getCoef test reussi");nbTR++;}
		else {System.out.println("getPreom test erreur");}
	}
	public static void setEvaluation() {
		Evaluation eval = new Evaluation (5, 10);
		nbT ++;
		eval.setNote(12);
		if(eval.getNote() == 12) { System.out.println("setNote (pour cas ok) test reussi");nbTR++;}
		else {System.out.println("setNote test erreur");}
		nbT ++;
		eval.setCoef(5);
		if(eval.getCoef()==5) { System.out.println("setCoef (pour cas ok) test reussi");nbTR++;}
		else {System.out.println("setCoef test erreur");}
		nbT ++;
		eval.setNote(0);
		if(eval.getNote() == 0) { System.out.println("setNote (pour cas = 0) test reussi");nbTR++;}
		else {System.out.println("setNote test erreur");}
		nbT ++;
		eval.setCoef(0);
		if(eval.getCoef()==0) { System.out.println("setCoef (pour cas = 0) test reussi");nbTR++;}
		else {System.out.println("setCoef test erreur");}
		nbT ++;
		eval.setNote(20);
		if(eval.getNote() == 20) { System.out.println("setNote (pour cas = 20) test reussi");nbTR++;}
		else {System.out.println("setNote test erreur");}
		nbT ++;
		eval.setCoef(10);
		if(eval.getCoef()==10) { System.out.println("setCoef (pour cas = 10) test reussi");nbTR++;}
		else {System.out.println("setCoef test erreur");}
		nbT ++;
		eval.setNote(-65);
		if(eval.getNote() == -1) { System.out.println("setNote (pour cas <0) test reussi");nbTR++;}
		else {System.out.println("setNote test erreur");}
		nbT ++;
		eval.setCoef(-2);
		if(eval.getCoef()==-1) { System.out.println("setCoef (pour cas <0) test reussi");nbTR++;}
		else {System.out.println("setCoef test erreur");}
		nbT ++;
		eval.setNote(65);
		if(eval.getNote() == -1) { System.out.println("setNote (pour cas >20) test reussi");nbTR++;}
		else {System.out.println("setNote test erreur");}
		nbT ++;
		eval.setCoef(15);
		if(eval.getCoef()==-1) { System.out.println("setCoef (pour cas >10) test reussi");nbTR++;}
		else {System.out.println("setCoef test erreur");}	
	}
	public static void ecrireFichierEleve() {
		Eleve eleve = new Eleve( "Costiou","Rozenn");
		Evaluation eval1 = new Evaluation (16, 3);
		Evaluation eval2 = new Evaluation (10, 5);
		Evaluation eval3 = new Evaluation (5, 1);
		eleve.addEvaluation(eval1);
		eleve.addEvaluation(eval2);
		eleve.addEvaluation(eval3);
		eleve.ecritureFile("../data/eleveRozenn.txt");
	}
	public static void lectureFichierEleve() {
		Eleve eleve = new Eleve();
		eleve.lectureFile("../data/eleveRozenn.txt");
		 System.out.println ("------------------------------- affichage-----------------------");
		String affiche = eleve. toSring();
		System.out.println(affiche);
	}
	
	public static void ecrireFichierEleveWithDOS() {
		Eleve eleve = new Eleve( "Costiou","Rozenn");
		Evaluation eval1 = new Evaluation (16, 3);
		Evaluation eval2 = new Evaluation (10, 5);
		Evaluation eval3 = new Evaluation (5, 1);
		eleve.addEvaluation(eval1);
		eleve.addEvaluation(eval2);
		eleve.addEvaluation(eval3);
		eleve.ecritureFileWithDataOutputStream("../data/eleveRozennBinaire");
	}
	
	public static void lectureFichierEleveWithDIS() {
		Eleve eleve = new Eleve( "Costiou","Rozenn");
		Evaluation eval1 = new Evaluation (16, 3);
		Evaluation eval2 = new Evaluation (10, 5);
		Evaluation eval3 = new Evaluation (5, 1);
		eleve.addEvaluation(eval1);
		eleve.addEvaluation(eval2);
		eleve.addEvaluation(eval3);
		eleve.lectureFileWithDataInputStream("../data/eleveRozennBinaire");
		 System.out.println ("-------------------------------affichage-----------------------");
		String affiche = eleve. toSring();
		System.out.println(affiche);
	}
	
	public static void ecrireFichierEleveWithOOS() {
		Eleve eleve = new Eleve( "Costiou","Rozenn");
		Evaluation eval1 = new Evaluation (16, 3);
		Evaluation eval2 = new Evaluation (10, 5);
		Evaluation eval3 = new Evaluation (5, 1);
		eleve.addEvaluation(eval1);
		eleve.addEvaluation(eval2);
		eleve.addEvaluation(eval3);
		eleve.ecritureFileWithObjectOutputStream("../data/eleveRozennObjet");
	}
	
	public static void lectureFichierEleveWithOIS() {
		Eleve eleve = new Eleve( "Costiou","Rozenn");
		Evaluation eval1 = new Evaluation (16, 3);
		Evaluation eval2 = new Evaluation (10, 5);
		Evaluation eval3 = new Evaluation (5, 1);
		eleve.addEvaluation(eval1);
		eleve.addEvaluation(eval2);
		eleve.addEvaluation(eval3);
		eleve.lectureFileWithObjectInputStream("../data/eleveRozennObjet");
		System.out.println ("-------------------------------Affichage-----------------------");	
		String affiche = eleve. toSring();
		System.out.println(affiche);
	}
	
}
