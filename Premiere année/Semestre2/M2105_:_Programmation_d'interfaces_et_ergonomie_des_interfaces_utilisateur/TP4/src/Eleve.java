import java.io.*;
import java.util.*;

/**
 * The studient
 * @author rozenn
 *
 */
// on implémente serializable pour pouvoir utiliser les objectStream
public class Eleve implements Serializable{
	private String nom;
	private String prenom;
	private ArrayList<Evaluation> listeNote;
	/**
	 * The constructor
	 * @param nom : the nom of the student
	 * @param prenom : the name of the student
	 */
	public Eleve( String nom , String prenom) {
		this.setNom(nom);
		this.setPrenom(prenom);
		this.listeNote = new ArrayList<Evaluation>();
	}
	/**
	 * the constructor for a student to create with a file
	 */
	public Eleve() {
		this.setNom("");
		this.setPrenom("");
		this.listeNote = new ArrayList<Evaluation>();
	}

	/**
	 * get the nom
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * set the nom
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		if (nom != null) {
			this.nom = nom;
		}
		else {
			this.nom = "";
		}
		
	}
	

	/**
	 * gate the name
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * set the name
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		if (prenom != null) {
			this.prenom = prenom;
		}
		else {
			this.prenom = "";
		}
		
	}
	
	/**
	 * add a new grade in the list
	 * @param newEval : the new eval to add
	 */
	public void addEvaluation(Evaluation newEval) {
		this.listeNote.add(newEval);
	}
	
	/**
	 * calculate the average
	 * @return the average
	 */
	public int moyenne() {
		int ret = -1;
		int note = 0;
		int div = 0;
		for (int i = 0 ; i < listeNote.size() ; i++) {
			if (listeNote.get(i).getNote() !=-1) {
				note +=  listeNote.get(i).getNote() * listeNote.get(i).getCoef();
				div += listeNote.get(i).getCoef();
			}
			else {
				System.err.println ("Eleve : ecritureFile : une evalution est invalide");
			}
		}
		if (div != 0) {
			ret = (note/div);
		}
		return ret;
	}
	// -------------------------------- save in string
	/**
	 * read the information in the file String
	 * @param nameFile : the file to read
	 */
	public void lectureFile(String nameFile) {
		 System.out.println ("-------------------------------fichier lecture String-----------------------");
			
		ArrayList<String> list = new ArrayList<String>();
		FileReader file = null;
		
		try {
			file = new FileReader (nameFile);
	
			BufferedReader in = new BufferedReader(file);
			String s = null;
			try {
				s = in.readLine();
				while(s!=null){
					list.add(s);
					s=in.readLine();
				}
				in.close();	
		// Catch for the FileReader
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}//Catch for the readLine
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//Parse the ArrayList<String> to set the students
		for (int i = 0; i< list.size() ; i++) {
			if (i == 0) {
				String line = (String) list.get(i);
				String chaine1 = line.substring (line.indexOf(":") +1);
				String nom = chaine1.substring(0, chaine1.indexOf("\t"));
				this.nom = nom.trim();
				String chaine2 = chaine1.substring (chaine1.indexOf(":")+1);
				this.prenom = chaine2.trim();
			}
			if ( i>1 && i<list.size()-1) {
				String line = (String) list.get(i);
				String note = line.substring(0, line.indexOf("\t"));
				String coef = line.substring (line.indexOf("\t") +1);
				int rnote = Integer.parseInt(note);
				int rcoef = Integer.parseInt(coef);
				Evaluation eval = new Evaluation(rnote ,rcoef);
				listeNote.add(eval);
			}
		}
		System.out.println ("initialiser avec le fichier : " +nameFile);
	}
	/**
	 * write the information in the file string
	 * @param nameFile : the file to write
	 */
	public void ecritureFile(String nameFile) {
		FileWriter writer = null ;
		BufferedWriter buf;
		PrintWriter out =null;
		try {
			writer = new FileWriter (nameFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		buf = new BufferedWriter(writer);
		out = new PrintWriter (buf);
		out.println("nom : " + nom + "\t" + "prenom : " + prenom );
		out.println("note " + "\t" + "coef");
		for (int i = 0 ; i < listeNote.size() ; i++) {
			if (listeNote.get(i).getNote() !=-1) {
				out.println(listeNote.get(i).getNote() + "\t" + listeNote.get(i).getCoef());
			}
			else {
				System.err.println ("Eleve : ecritureFile : une evalution est invalide");
			}
		}
		if (moyenne() != -1) {
			out.println("moyenne = " + moyenne() );
		}
        System.out.println ("--------------------------------fichier crée ou modifié en string--------------------------");
		out.close();
	}
// ------------------------------------------- save in binaire
	/**
	 * write the information in the file binaire
	 * @param nameFile : the file to write
	 */
	public void ecritureFileWithDataOutputStream(String nameFile) {
        FileOutputStream fos;
        DataOutputStream dos;
        try {
             
            fos = new FileOutputStream(nameFile);
            dos = new DataOutputStream(fos);
          
            dos.writeUTF("nom : " + nom + "\t" + "prenom : " + prenom);
            dos.writeUTF("note " + "\t" + "coef");
            for (int i = 0 ; i < listeNote.size() ; i++) {
    			if (listeNote.get(i).getNote() !=-1) {
    				String note = String.valueOf(listeNote.get(i).getNote());
    				String coef = String.valueOf(listeNote.get(i).getCoef());
    				dos.writeUTF(note + "\t" + coef);
    			}
    			else {
    				System.err.println ("Eleve : ecritureFile : une evalution est invalide");
    			}
    		}
    		if (moyenne() != -1) {
    			String moyenne = String.valueOf(moyenne());
    			dos.writeUTF("moyenne	=" + moyenne);
    		}
            dos.close();
        }
        catch (FileNotFoundException fnfe) {
            System.out.println("File not found" + fnfe);
        }
        catch (IOException ioe) {
            System.out.println("Error while writing to file" + ioe);
        }
        
        System.out.println ("-------------------------------fichier crée ou modifié en binaire-----------------------");
	}
	/**
	 * read the information in the file binzire
	 * @param nameFile : the file to read
	 */
	public void lectureFileWithDataInputStream(String nameFile) {
         
	        FileInputStream fis;
	        DataInputStream dis;    
	        System.out.println ("-------------------------------fichier lecture  en binaire-----------------------");
	    	
	        try {
	             
	            fis = new FileInputStream(nameFile);
	 
	            dis = new DataInputStream(fis);
	            String eleve;
	            String noteCoef;
	            String eval;
	            String moyenne;
	            String chaine;
	            String comp = "moyenne";
	            this.listeNote = new ArrayList<Evaluation>();
	            
	            eleve=dis.readUTF();
	            String nom = eleve.substring(eleve.indexOf(":")+1, eleve.indexOf("\t"));
				this.nom = nom.trim();
				String chaine2 = eleve.substring (eleve.indexOf(":")+1);
				chaine2 = chaine2.substring ( eleve.indexOf("\t"));
				chaine2 = chaine2.substring (eleve.indexOf(":")+1);
				this.prenom = chaine2.trim();
				
	            noteCoef = dis.readUTF();
	            eval = dis.readUTF();
	            for (int i = 0 ; comp.compareTo (eval.substring(0, eval.indexOf("\t")).trim()) !=0; i++) {
	            	String note = eval.substring(0, eval.indexOf("\t"));
					String coef = eval.substring (eval.indexOf("\t") +1);
					int rnote = Integer.parseInt(note);
					int rcoef = Integer.parseInt(coef);
	            	if (rnote!=-1 && rcoef!=-1) {
						Evaluation evalu = new Evaluation(rnote ,rcoef);
						listeNote.add(evalu);
	    				noteCoef += "\n" + eval;
	    				eval = dis.readUTF();
	    			}
	    			else {
	    				System.err.println ("Eleve : ecritureFile : une evalution est invalide");
	    			}
	    		}
	            dis.close();
	       
	            
	        }
	        catch (FileNotFoundException fe) {
	            System.out.println("File not found: " + fe);
	        }
	        catch (IOException ioe) {
	            System.out.println("Error while reading file: " + ioe);
	        }
	        System.out.println ("initialiser avec le fichier : " +nameFile);
	}
	//-------------------------------------------------------- save in Object
	/**
	 * write the information in the file object
	 * @param nameFile : the file to write
	 */
	public void ecritureFileWithObjectOutputStream(String nameFile) {
        try {
        	 
            // Store Serialized User Object in File
            FileOutputStream fileOutputStream = new FileOutputStream(nameFile);
            ObjectOutputStream output = new ObjectOutputStream(fileOutputStream);
            output.writeObject(this);
            System.out.println ("--------------------------fichier crée ou modifié en object---------------------");
            output.close();
 
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
        	
        }
        
    }
          
	/**
	 * read the information in the file object
	 * @param nameFile : the file to read
	 */
	public void lectureFileWithObjectInputStream(String nameFile) {
		 System.out.println ("-------------------------------fichier lecture d'un objet-----------------------");
			
		try {
			 
            //Read from the stored file
            FileInputStream fileInputStream = new FileInputStream(new File(nameFile));
            ObjectInputStream input = new ObjectInputStream(fileInputStream);
            Eleve eleve = (Eleve) input.readObject();
            input.close();
            this.nom = eleve.nom;
            this.prenom = eleve.prenom;
            this.listeNote =eleve.listeNote;
 
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
        }
		System.out.println ("initialiser avec le fichier : " +nameFile);
		
	}

	// -------------------------------------------------------- toString
	/**
	 * String the student
	 * @return the information
	 */
	public String toSring() {
		String ret = "";
		ret += "nom : " + nom + "\t" + "prenom : " + prenom + "\n" + "note " + "\t" + "coef";
		for (int i = 0 ; i < listeNote.size() ; i++) {
			if (listeNote.get(i).getNote() !=-1) {
				ret += "\n" + listeNote.get(i).getNote() + "\t" + listeNote.get(i).getCoef();
			}
			else {
				System.err.println ("Eleve : ecritureFile : une evalution est invalide");
			}
		}
		if (moyenne() != -1) {
			ret += "\n"+ "moyenne = " + moyenne() ;
		}
		return ret;
		
	}
}