import java.io.Serializable;

/**
 * The Evaluation
 * @author rozenn
 *
 */
public class Evaluation implements Serializable{
	private int note;
	private int coef;
	
	/**
	 * The constructor
	 * @param note : the grade
	 * @param coef : the coefficient
	 */
	public Evaluation (int note , int coef) {
		this.setNote(note);
		this.setCoef(coef);
		
	}

	/**
	 * get the grade
	 * @return the note
	 */
	public int getNote() {
		return note;
	}

	/**
	 * set the grade and put -1 if is not OK
	 * @param note the note to set
	 */
	public void setNote(int note) {
		if (note >=0 && note <= 20 ) {
			this.note = note;
		}
		else {
			this.note = -1;
			System.err.println ("Evaluation : setNote : note invalide");
		}
	}

	/**
	 * get the coefficient
	 * @return the coef
	 */
	public int getCoef() {
		return coef;
	}

	/**
	 * set the coefficient and put -1 if is not OK
	 * @param ncoef : the coef to set
	 */
	public void setCoef(int ncoef) {
		if ( ncoef>=0 && ncoef <= 10 ) {
			this.coef = ncoef;
		}
		else {
			this.coef = -1;
			System.err.println ("Evaluation : setCoef : coefficient invalide");
		}
	}
	
}