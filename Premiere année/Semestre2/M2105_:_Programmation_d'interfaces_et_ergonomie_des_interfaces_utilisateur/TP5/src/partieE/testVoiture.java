package partieE;
/**
 * the test of the class Voiture
 * @author rozenn
 * @version 1
 */
public class testVoiture {
	public static void main(String[] args) {
		voiture();
		voitureValeurInvalide();
	}
	public static void voiture() {
		Voiture voit = new Voiture("Renault", "Scénic" , 110);
		System.out.println (voit.toString());
	}
	public static void voitureValeurInvalide () {
		Voiture voit = new Voiture(null, null , -5);
		System.out.println (voit.toString());
	}
}
