package partieE;
/**
 * the test of the parking
 * @author rozenn
 * @version 1
 */
public class TestParking {
	private static Parking park;
	
	public static void main(String[] args) throws EsceptionParking {
		parking();
		garer();
		sortir();
		
	}
	public static void parking() throws EsceptionParking {
		System.out.println( "------------------ test creation parking ---------------" + "\n");
		park = new Parking ();
		System.out.println(park.toString());
	}
	public static void garer() throws EsceptionParking {
		System.out.println( "--------------- test ajouter une voiture OK ---------------"+ "\n");
		park = new Parking ();
		Voiture voit1 = new Voiture("Renault", "Scénic" , 110);
		park.garer(voit1,0);
		System.out.println(park.toString());
		
		System.out.println( "--------------- test ajouter une voiture numero de place incorect ( superieur au nombre )---------------"+ "\n");
		park = new Parking ();
		Voiture voit2 = new Voiture("Renault", "Scénic" , 110);
		park.garer(voit2,10);
		System.out.println(park.toString());
		
		System.out.println( "--------------- test ajouter une voiture numero de place incorect ( negatif ) ---------------"+ "\n");
		park = new Parking ();
		Voiture voit3 = new Voiture("Renault", "Scénic" , 110);
		park.garer(voit3,-6);
		System.out.println(park.toString());
	
		System.out.println( "--------------- test ajouter voiture deja presente ---------------"+ "\n");
		park = new Parking ();
		Voiture voit4 = new Voiture("Renault", "Scénic" , 75);
		park.garer(voit4,0);
		System.out.println(park.toString());
		Voiture voit5 = new Voiture("Renault", "Dacia" , 110);
		park.garer(voit5,0);
	}
	
	public static void sortir() throws EsceptionParking {
		System.out.println( "--------------- test enlever une voiture---------------" +"\n");
		park = new Parking ();
		Voiture voit1 = new Voiture("Renault", "Scénic" , 110);
		park.garer(voit1,0);
		Voiture voitE = park.sortir(0);
		System.out.println(park.toString());
		System.out.println("voiture enlever : "+ "\n" + voitE.toString());
		
		System.out.println( "--------------- test enlever numero invalide---------------" +"\n");
		park = new Parking ();
		Voiture voit2 = new Voiture("Renault", "Scénic" , 110);
		park.garer(voit2,0);
		Voiture voitE2 = park.sortir(0);
		System.out.println(park.toString());
		System.out.println("voiture enlever : "+ "\n" + voitE2.toString());
		
		System.out.println( "--------------- test enlever pas de voiture dans la place---------------" +"\n");
		park = new Parking ();
		Voiture voitE3 = park.sortir(0);
		System.out.println(park.toString());
	}
}
