package partieC;
/**
 * the exeption
 * @author rozenn
 * @version 3
 */
public class FichierNonTrouveException extends java.io.FileNotFoundException{
	String message;
	public FichierNonTrouveException(String str) {
		this.message=str;
	}
	
	public String toString() {
		return "FichierNonTrouveException:"+this.message;
	}
}