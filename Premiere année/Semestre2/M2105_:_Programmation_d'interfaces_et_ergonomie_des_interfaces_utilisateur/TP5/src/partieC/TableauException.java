package partieC;
/**
 * the exeption
 * @author rozenn
 * @version 3
 */
public class TableauException extends Exception{
	String message;
	public TableauException(String str) {
		this.message=str;
	}
	
	public String toString() {
		return "TableauException:"+this.message;
	}
}