package partieA;

import java.util.Scanner;
/**
 * make and write and read a table
 * @author rozenn
 * @version 1
 */
public class Tableau {
	private int longeur;
	private int[] tabEnt;
	/**
	 * the constructor of Tableau
	 * @param nbCase : the number of the number in the table
	 */
	public Tableau (int nbCase) {
		if (nbCase > 0) {
			this.longeur = nbCase;
			this.tabEnt = new int[this.longeur];
			System.out.println ("tableau creer");
		}
		else {
			this.longeur = 0;
			this.tabEnt = new int[this.longeur];
			System.err.println ("Tableau : tableau : nombre de case incorect");
		}
		
	}
	/**
	 * read the table on the indices
	 * @param i : the indices when you read
	 * @return the number in the indices
	 */
	public int lecture(int i) {
		return this.tabEnt[i];
	}
	/**
	 * write on the table in the indices
	 * @param i : the indices when you write
	 */
	public void ecriture (int i) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Veuillez saisir un chiffre :");
		int nChiffre = sc.nextInt();
		this.tabEnt[i] = nChiffre;
	}
}