package partieB;

import java.io.*;
import java.util.*;
/**
 * make and write and read a table
 * @author rozenn
 * @version 2
 */
public class Tableau {
	private int longeur;
	private int[] tabEnt;
	/**
	 * the constructor of Tableau
	 * @param nbCase : the number of the number in the table
	 */
	public Tableau (int nbCase) {
		if (nbCase > 0) {
			this.longeur = nbCase;
			this.tabEnt = new int[this.longeur];
			System.out.println ("tableau creer");
		}
		else {
			this.longeur = 0;
			this.tabEnt = new int[this.longeur];
			System.err.println ("Tableau : tableau : nombre de case incorect");
		}
	}
	
	/**
	 * the constructor of the table
	 * @param fileName : the name of the file for init the table
	 * @throws FileNotFoundException exeption
	 * @throws IOException exeption
	 */
	public Tableau(String fileName ) throws FileNotFoundException, IOException {
		ArrayList<String> list = new ArrayList<String>();
		FileReader file = null;
		// recupere les informations
			file = new FileReader (fileName);
			BufferedReader in = new BufferedReader(file);
			String s = null;
				s = in.readLine();
				while(s!=null){
					list.add(s);
					s=in.readLine();
				}
				in.close();	
		// initialise les valeurs
		int j = 0;
		int lengthF = 0;
		for (int i = 0; i< list.size() ; i++) {
			if (i == 0) {
				String line = (String) list.get(i);
				lengthF = Integer.parseInt(line.trim());
				if (lengthF > 0) {
					this.longeur = lengthF;
					this.tabEnt = new int[this.longeur];
					System.out.println ("tableau creer");
				}
				else {
					this.longeur = 0;
					this.tabEnt = new int[this.longeur];
					System.err.println ("Tableau : tableau : nombre de case incorect");
				}
				
			}
			else {
				String line = (String) list.get(i);
				int valueF = Integer.parseInt(line.trim());
				while (j<lengthF) {
					this.tabEnt[j] = valueF;
					j ++ ;
				}
			}
		}
	}
	/**
	 * read the table on the indices
	 * @param i : the indices when you read
	 * @return the number in the indices
	 */
	public int lecture(int i) {
		return this.tabEnt[i];
	}
	/**
	 * write on the table in the indices
	 * @param i : the indices when you write
	 */
	public void ecriture (int i) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Veuillez saisir un chiffre :");
		int nChiffre = sc.nextInt();
		this.tabEnt[i] = nChiffre;
	}
}