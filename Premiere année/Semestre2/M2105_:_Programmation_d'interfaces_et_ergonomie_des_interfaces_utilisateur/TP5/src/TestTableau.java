import java.io.FileNotFoundException;
import java.io.IOException;

import partieC.TableauException;

/**
 * the test of the class Tableau
 * @author rozenn
 * @version 1
 */
public class TestTableau {
	/**
	 * the launcher of the class
	 * @param args :
	 */
	public static void main(String[] args) {
		System.out.println ("___________________________________Test Partie A ______________________________");
		//tailleOKA();
		//tailleNegatifA();
		//IndiceOKA();
		//IndiceNegatifA();
		//IndiceSuperieurA();
		//IndicePasInitialiserTableauA();
		
		System.out.println ("___________________________________Test Partie B_______________________________");
		//FichierExistant();
		//FichierIntrouvable();
		
		System.out.println ("___________________________________Test Partie C_______________________________");
		IndiceOKC();
		try {
			IndiceNegatifC();
		} catch (TableauException e) {
			e.printStackTrace();
		}
		try {
			IndiceSuperieurC();
		} catch (TableauException e1) {
			e1.printStackTrace();
		}
		try {
			IndicePasInitialiserTableauC();
		} catch (TableauException e1) {
			e1.printStackTrace();
		}
		System.out.println ("___________________________________Test Partie D_______________________________");
		IndiceOKD();
		try {
			IndiceNegatifD();
		} catch (TableauException e) {
			e.printStackTrace();
		}
		try {
			IndiceSuperieurD();
		} catch (TableauException e1) {
			e1.printStackTrace();
		}
		try {
			IndicePasInitialiserTableauC();
		} catch (TableauException e) {
			e.printStackTrace();
		}
	}
	// ________________________________________________________________________ Test Partie A ____________________________________________
	public static void tailleOK(){
		System.out.println ("-------------------Test pour un tableau OK----------------");
		partieA.Tableau tab = new partieA.Tableau (5);
	}
	
	public static void tailleNegatifA(){
		System.out.println ("-------------------Test pour un tableau avec taille negatif----------------");
		partieA.Tableau tab = new partieA.Tableau (-9);
	}
	
	public static void IndiceOKA(){
		System.out.println ("-------------------Test pour un indice OK----------------");
		int nbC = 5;
		partieA.Tableau tab = new partieA.Tableau (nbC);
		for (int i = 0; i < nbC; i++){
			tab.ecriture (i);
		}
		int rs = tab.lecture(3);
		System.out.println(rs);
		rs = tab.lecture(0);
		System.out.println(rs);
		rs = tab.lecture(4);
		System.out.println(rs);
		
	}
	
	public static void IndiceNegatifA(){
		System.out.println ("-------------------Test pour un indice negatif----------------");
		int nbC = 2;
		partieA.Tableau tab = new partieA.Tableau (nbC);
		for (int i = 0; i < nbC; i++) {
			tab.ecriture (i);
		}
		int rs = tab.lecture(-5);
		tab.ecriture(-1);
	}
	public static void IndiceSuperieurA(){
		System.out.println ("-------------------Test pour un indice superieur----------------");
		int nbC = 2;
		partieA.Tableau tab = new partieA.Tableau (nbC);
		tab.lecture(nbC+4);
		tab.ecriture(nbC+4);
	}
	
	public static void IndicePasInitialiserTableauA(){
		System.out.println ("-------------------Test pour un tableau pas initialiser----------------");
		partieA.Tableau tab = null ;
		tab.lecture(2);
		tab. ecriture(2);
	}
	// ________________________________________________________________________ Test Partie B ____________________________________________
	public static void FichierExistant() throws FileNotFoundException, IOException{
		System.out.println ("-------------------Test pour un fichier existant----------------");
		partieB.Tableau tab = new partieB.Tableau ("ExistantInitialiseTableau.txt");
	}
	public static void FichierIntrouvable() throws FileNotFoundException, IOException{
		System.out.println ("-------------------Test pour un fichier introuvable----------------");
		partieB.Tableau tab = new partieB.Tableau ("introuvable.txt");
	}
	
	// ________________________________________________________________________ Test Partie C ____________________________________________
	public static void IndiceOKC(){
		System.out.println ("-------------------Test pour un indice OK----------------");
		int nbC = 5;
		partieC.Tableau tab = new partieC.Tableau (nbC);
		for (int i = 0; i < nbC; i++){
			tab.ecriture (i);
		}
		int rs;
		try {
			rs = tab.lecture(3);
			System.out.println(rs);
			rs = tab.lecture(0);
			System.out.println(rs);
			rs = tab.lecture(4);
			System.out.println(rs);
		} catch (TableauException e) {
			e.printStackTrace();
		}
		
		
	}
	
	public static void IndiceNegatifC() throws TableauException{
		System.out.println ("-------------------Test pour un indice negatif----------------");
		int nbC = 2;
		partieC.Tableau tab = new partieC.Tableau (nbC);
		for (int i = 0; i < nbC; i++) {
			tab.ecriture (i);
		}
		int rs = tab.lecture(-5);
		tab.ecriture(-1);
	}
	public static void IndiceSuperieurC() throws TableauException{
		System.out.println ("-------------------Test pour un indice superieur----------------");
		int nbC = 2;
		partieC.Tableau tab = new partieC.Tableau (nbC);
		tab.lecture(nbC+4);
		tab.ecriture(nbC+4);
	}
	
	public static void IndicePasInitialiserTableauC() throws TableauException{
		System.out.println ("-------------------Test pour un tableau pas initialiser----------------");
		partieC.Tableau tab = null ;
		tab.lecture(2);
		tab. ecriture(2);
	}
//________________________________________________________________________ Test Partie D ____________________________________________

public static void IndiceOKD(){
	System.out.println ("-------------------Test pour un indice OK----------------");
	int nbC = 5;
	partieD.Tableau tab = new partieD.Tableau (nbC);
	for (int i = 0; i < nbC; i++){
		tab.ecriture (i);
	}
	int rs;
	rs = tab.lecture(3);
	System.out.println(rs);
	rs = tab.lecture(0);
	System.out.println(rs);
	rs = tab.lecture(4);
	System.out.println(rs);
	
	
}

public static void IndiceNegatifD() throws TableauException{
	System.out.println ("-------------------Test pour un indice negatif----------------");
	int nbC = 2;
	partieD.Tableau tab = new partieD.Tableau (nbC);
	for (int i = 0; i < nbC; i++) {
		tab.ecriture (i);
	}
	int rs = tab.lecture(-5);
	tab.ecriture(-1);
}
public static void IndiceSuperieurD() throws TableauException{
	System.out.println ("-------------------Test pour un indice superieur----------------");
	int nbC = 2;
	partieD.Tableau tab = new partieD.Tableau (nbC);
	tab.lecture(nbC+4);
	tab.ecriture(nbC+4);
}

public static void IndicePasInitialiserTableauD() throws TableauException{
	System.out.println ("-------------------Test pour un tableau pas initialiser----------------");
	partieD.Tableau tab = null ;
	tab.lecture(2);
	tab. ecriture(2);
}
}
