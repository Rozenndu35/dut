package partieE;

import java.io.FileNotFoundException;
/**
 * Make a parking
 * @author rozenn
 * @version 1
 */
public class Parking {
	private final int NB_PLACE = 2;
	public Voiture[] lesPlaces;
	/**
	 * The constructor
	 */
	public Parking () {
		
		for ( int i = 0 ; i< NB_PLACE ; i++) {
			lesPlaces = new Voiture[NB_PLACE];
			lesPlaces[i] = null;
		}
	}
	
	/**
	 * check if the number place it's OK
	 * @param numPlace : the number of the place
	 * @throws EsceptionParking
	 */
	private void numeroValide (int numPlace) throws EsceptionParking{
		if (0 > numPlace || numPlace >= NB_PLACE) {
			EsceptionParking ep = new EsceptionParking("Numéro de place non valide");
		}
	}
	
	/**
	 * for put a car in the parking		
	 * @param voit : the car
	 * @param numPlace : the number of the place
	 * @throws EsceptionParking exception
	 */
	public void garer(Voiture voit, int numPlace ) throws EsceptionParking{
		numeroValide (numPlace);
		try{
			if (lesPlaces[numPlace] == null) {
				lesPlaces[numPlace] = voit;
			}
			else{
				EsceptionParking ep = new EsceptionParking ("Place déjà occupé");
			}
		}
		catch (ArrayIndexOutOfBoundsException e){
			e.printStackTrace();
		}
			
	}
	
	/**
	 * take a car in the parking
	 * @param numPlace : the number of the place
	 * @return the car
	 * @throws EsceptionParking exception
	 */
	public Voiture sortir( int numPlace) throws EsceptionParking {
		Voiture ret = null;
		numeroValide (numPlace);
		if ( lesPlaces[numPlace] != null) {
			ret = lesPlaces[numPlace];
			lesPlaces[numPlace] = null;
		}
		else{
			EsceptionParking ep = new EsceptionParking ("Pas de voiture à cette place");
		}
		return ret;
	}
	/**
	 * make a string of the parking
	 * @return the information of the parking
	 */
	public String toString() {
		String ret = "";
		ret = "    etat du parking " + "\n";
		for (int i = 0; i< NB_PLACE ; i++) {
			ret += "\n"+" place " + (i+1) + " : ";
			if (lesPlaces[i] != null) {
				ret +=  "\n" + lesPlaces[i].toString() + "\n";
			}
			else {ret += "place libre " + "\n";}
		}
		return ret; 
	}
}
