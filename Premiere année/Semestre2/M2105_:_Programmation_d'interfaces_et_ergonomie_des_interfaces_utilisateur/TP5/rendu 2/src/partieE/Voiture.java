package partieE;
/**
 * The class to make a car
 * @author rozenn
 * @version 1
 */
public class Voiture {
	String marque;
	String modele;
	int puissance;
	
	/**
	 * The constructor
	 * @param marque : the car brand
	 * @param modele : the model car
	 * @param puissance : the power
	 */
	public Voiture (String marque , String modele , int puissance) {
		if (marque!= null && modele != null  && puissance>=0 ) {
			this.marque = marque;
			this.modele =modele;
			this.puissance = puissance;
		}
		else {
			this.marque = "";
			this.modele = "";
			this.puissance = 0;
			System.err.println("Voiture : Voiture : marque, modele ou puissance invalide");
		}
	}
	
	/**
	 * string of the information
	 * @return the information
	 */
	public String toString() {
		String ret = "marque : " + this.marque + "\n" + "modele : " + this.modele + "\n" + "puissance : " + this.puissance;
		return ret;
	}
}
