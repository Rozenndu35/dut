package pck;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JFileChooser;

/**
 * the Action of the button in the BarreEtat
 * @author rozenn
 */
public class ButtonListener implements ActionListener{
	BarreEtat ba;
	Information i;
	String action;
	
	/**
	 * The constructor
	 * @param ba : the barreEtat panel
	 * @param action : the Action to make
	 * @param i : the Information panel
	 */
	public ButtonListener(BarreEtat ba, String action, Information i) {
		this.ba=ba;
		this.i=i;
		this.action=action;
		
	}
	/**
	 * 
	 */
	public void actionPerformed(ActionEvent e) {	
		ba.setBonjourLabel(action);
		
		switch(this.action) {
		case "Nouveau": // si c'est l'action lancer une nouvelle fenetre
			java.awt.EventQueue.invokeLater(new Runnable() {
			      public void run() {
			        new Fenetre().setVisible(true);
			      }
			    });
			break;
		case "Supprimer": // si c'est supprimer renitialiser le pannel information
			i.reset();
			break;
		case "Charger": // si charger ouvre une fenetre ou choisir sons fichier et le lance dans la fenetre
			
			 JFileChooser dialogue = new JFileChooser();
	            JButton open = new JButton();
	            if(dialogue.showOpenDialog(open)==JFileChooser.APPROVE_OPTION){
	            	ba.setBonjourLabel("Ouverture du fichier :"+dialogue.getSelectedFile().getAbsolutePath());
	                try{
	                   Scanner s = new Scanner(new FileReader(dialogue.getSelectedFile().getAbsolutePath())).useDelimiter("\\s*:\\s*");
	                   i.getNomTextField().setText(s.next());
	                   i.getPrenomTextField().setText(s.next());
	                   i.getDateNaissanceTextField().setText(s.next());
	                   i.getCoutielTextField().setText(s.next());
	                   i.setSex(s.next());
	                   i.set1a(s.next());
	                   i.set2a(s.next());
	                   int nbElem = Integer.parseInt(s.next());
	                   int ii=0;
	                   i.getDlm().clear();
	                   while(ii<nbElem) {
	                	   i.getDlm().addElement(s.next());   
	                	   ii++;
	                   }

	       			s.close();
	                }
	                catch (FileNotFoundException ex){
	                	System.out.println(ex.getMessage());
	                }
	            }
			break;
		case "Enregistrer": // sauvegarder tout dans un fichier
			
			   JFileChooser dialogue2 = new JFileChooser();
	            JButton save = new JButton();
	            if(dialogue2.showSaveDialog(save)==JFileChooser.APPROVE_OPTION){
	            	 try
	        		 {
	        			PrintWriter out = new PrintWriter(dialogue2.getSelectedFile().getAbsolutePath());
	                    out.print(":"); //separateur
	        			out.print(i.getNomTextField().getText());
	                    out.print(":");
	        			out.print(i.getPrenomTextField().getText());
	                    out.print(":");
	        			out.print(i.getDateNaissanceTextField().getText());
	                    out.print(":");
	        			out.print(i.getCoutielTextField().getText());
	                    out.print(":");
	                    out.print(i.getSex());
	                    out.print(":");
	        			out.print(i.get1a());
	                    out.print(":");
	        			out.print(i.get2a());
	                    out.print(":");
	                    int nbElem=i.getNbNote();
	                    out.print(nbElem);
	                    out.print(":");
	                    int ii=0;
	                    while(ii<nbElem) {
	                    	out.print(i.getDlm().get(ii));
	                    	out.print(":");
	                    	ii++;
	                    }
	                    
	        			out.close();
	        		}
	        		catch (FileNotFoundException ex)
	        		{
	        			System.out.println(ex.getMessage());
	        		}
	            }
				
	            
			break;
		case "Enregistrer sous": // saugvgarde tout dans un fichier
			
            JFileChooser dialogue3 = new JFileChooser();
            JButton save2 = new JButton();
            if(dialogue3.showSaveDialog(save2)==JFileChooser.APPROVE_OPTION){
            	 try
        		 {
        			PrintWriter out = new PrintWriter(dialogue3.getSelectedFile().getAbsolutePath());
                    out.print(":");
        			out.print(i.getNomTextField().getText());
                    out.print(":");
        			out.print(i.getPrenomTextField().getText());
                    out.print(":");
        			out.print(i.getDateNaissanceTextField().getText());
                    out.print(":");
        			out.print(i.getCoutielTextField().getText());
                    out.print(":");
                    out.print(i.getSex());
                    out.print(":");
        			out.print(i.get1a());
                    out.print(":");
        			out.print(i.get2a());
                    out.print(":");
                    int nbElem=i.getNbNote();
                    out.print(nbElem);
                    out.print(":");
                    int ii=0;
                    while(ii<nbElem) {
                    	out.print(i.getDlm().get(ii));
                    	out.print(":");
                    	ii++;
                    }
                    
        			out.close();
        		}
        		catch (FileNotFoundException ex)
        		{
        			System.out.println(ex.getMessage());
        		}
            }
			
			break;
		}
	}

}