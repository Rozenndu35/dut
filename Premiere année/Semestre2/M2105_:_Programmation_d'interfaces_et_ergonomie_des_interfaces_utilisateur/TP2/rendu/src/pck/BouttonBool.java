package pck;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Action of the BoutonRadio in Information
 * @author rozenn
 */
public class BouttonBool implements ActionListener {
	Information i;
	String bouton;
	boolean action;
	
	/**
	 *  The constructor
	 * @param i : the information panel
	 * @param bouton : the name of the buttonRadio
	 */
	public BouttonBool(Information i, String bouton) {
			this.i=i;
			this.bouton = bouton;
			if (bouton == "h") {
				if(i.etathommeBoutonRadio() == true) {
					this.action = false;
		        } else {
		        	this.action = true;
		        }
			}
			else {
				if(i.etatfemmeBoutonRadio() == true) {
					this.action = false;
		        } else {
		        	this.action = true;
		        }
			}
		}
	
	/**
	 * 
	 */
	public void actionPerformed(ActionEvent e) {
		if (bouton == "h") {
			i.setfemmeBoutonRadio(false);
			i.sethommeBoutonRadio(true);
		}
		else {
			i.sethommeBoutonRadio(false);
			i.setfemmeBoutonRadio(true);
		}	
	}
}