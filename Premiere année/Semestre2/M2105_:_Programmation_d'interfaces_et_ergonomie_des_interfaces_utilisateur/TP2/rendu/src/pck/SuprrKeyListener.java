package pck;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class SuprrKeyListener implements KeyListener{
	Information i;
	public SuprrKeyListener(Information i) {
		this.i=i;
	}
	/**
	 * The methode of the super class
	 */
	public void keyTyped(KeyEvent e) {
	}

	@Override
	/**
	 *
	*/
	public void keyPressed(KeyEvent e) {
		//char c = e.getKeyChar();
		if(KeyEvent.VK_DELETE == e.getKeyChar()) {
			i.suprimmerNote(i.getMoyenList().getSelectedIndex());
		}
		else {
			System.out.println(e.getKeyChar());
		}	
	}

	/**
	 * The methode of the super class
	 */
	public void keyReleased(KeyEvent e) {		
	}
	
}