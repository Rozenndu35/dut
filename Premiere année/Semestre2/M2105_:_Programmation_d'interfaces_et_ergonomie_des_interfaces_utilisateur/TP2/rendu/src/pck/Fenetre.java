package pck;
import javax.swing.*;

import java.awt.event.ActionEvent;
import java.text.*;

public class Fenetre extends JFrame{

    public Fenetre() {
        initComponents();
    }

    private void initComponents() {
        
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE); //demande de fermer la fenetre lors ce que l'on touvhe la croix
        setTitle("Moyenne eleve"); //nom de la fenetre
        BarreOutils bo = new BarreOutils();
        Information i = new Information();
        BarreEtat ba = new BarreEtat(System.getProperty("user.name") , java.time.LocalDateTime.now().toString());
        
        
        // --------------------------------------- INTERACTION ----------------------------------------
       
        // Création des Listener en passant en paramètre l'action de chaque bouttons 
        ButtonListener nBL = new ButtonListener(ba,"Nouveau",i);
        ButtonListener sBL = new ButtonListener(ba,"Supprimer",i);
        ButtonListener cBL = new ButtonListener(ba,"Charger",i);
        ButtonListener eBL = new ButtonListener(ba,"Enregistrer",i);
        ButtonListener eSBL = new ButtonListener(ba,"Enregistrer sous",i);
        BouttonBool hBB = new BouttonBool(i ,"h");
        BouttonBool fBB = new BouttonBool(i ,"f");
        ButtonInfoListener bi = new ButtonInfoListener(i);
        ButtonInfoSupprListener bis = new ButtonInfoSupprListener(i);
        SuprrKeyListener sKL = new SuprrKeyListener(i);
        i.getMoyenList().addKeyListener(sKL);
        //Attribution des Listener à chaque boutton
        bo.getNouveauButton().addActionListener(nBL);
        bo.getSupprimerButton().addActionListener(sBL);
        bo.getChargerButton().addActionListener(cBL);
        bo.getEnregistrerButton().addActionListener(eBL);
        bo.getEnregistrersousButton().addActionListener(eSBL);
        i.gethommeBoutonRadio().addActionListener(hBB);
        i.getfemmeBoutonRadio().addActionListener(fBB);
        i.getAjouteNoteButton().addActionListener(bi);
        i.getSupprimeNoteButton().addActionListener(bis);
        
        // ------------------------------------------------------ DISPOSITION --------------------------------------
        getContentPane().setLayout(new java.awt.BorderLayout());
        add ( bo , java.awt.BorderLayout.WEST );
        add ( i , java.awt.BorderLayout.CENTER );
        add ( ba , java.awt.BorderLayout.SOUTH );
        pack();
    }
    
	public static void main(String[] args) {
		java.awt.EventQueue.invokeLater(new Runnable() {
		      public void run() {
		        new Fenetre().setVisible(true);
		      }
		    });

	}

}