package pck;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Action of the BoutonRadio in Information for add a grade
 * @author rozenn
 *
 */
public class ButtonInfoListener implements ActionListener{
		Information i;
		/**
		 * The constructor
		 * @param i : the panel Information
		 */
		public  ButtonInfoListener(Information i) {
			this.i=i;
	
		}
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {	
			i.ajouterNote(Double.parseDouble(i.getAjouteNoteTextField().getText()));
		}
}