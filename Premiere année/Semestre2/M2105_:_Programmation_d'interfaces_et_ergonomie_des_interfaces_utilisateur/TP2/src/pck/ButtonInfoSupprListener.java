package pck;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * the Action of the button supprimer une note
 * @author rozenn
 *
 */
public class ButtonInfoSupprListener implements ActionListener{
		Information i;
		/**
		 * The constructor
		 * @param i : the Information panel
		 */
		public  ButtonInfoSupprListener(Information i) {
			this.i=i;
	
		}
		/**
		 * 
		 */
		public void actionPerformed(ActionEvent e) {	
			i.suprimmerNote(i.getMoyenList().getSelectedIndex());
		}
}