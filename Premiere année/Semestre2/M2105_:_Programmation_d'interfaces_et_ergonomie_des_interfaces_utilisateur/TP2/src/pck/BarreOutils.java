package pck;

import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

/**
 * Make the pannel in the WEST
 * @author rozenn
*/
public class BarreOutils extends javax.swing.JPanel{
    
    private javax.swing.JButton nouveauButton;
    private javax.swing.JButton supprimerButton;
    private javax.swing.JButton chargerButton;
    private javax.swing.JButton enregistrerButton;
    private javax.swing.JButton enregistrersousButton;
    /**
     * The constructor
     */
    public BarreOutils() {
        initComponents();
    }
    /**
     * instance the Components
     */
    private void initComponents() {
 
    	nouveauButton=new javax.swing.JButton();
    	supprimerButton=new javax.swing.JButton();
    	chargerButton=new javax.swing.JButton();
    	enregistrerButton=new javax.swing.JButton();
    	enregistrersousButton=new javax.swing.JButton();
    	
    	
        getNouveauButton().setText("nouveau");
        getSupprimerButton().setText("supprimer");
        getChargerButton().setText("charger");
        getEnregistrerButton().setText("enregistrer");
        getEnregistrersousButton().setText("enregistrersous");
        
       // ------------------------------------------- DISPOSITION ----------------------------------------------------------------
       setLayout(new java.awt.GridLayout(5,1)); 
        add(getNouveauButton());
        add(getSupprimerButton());
        add(getChargerButton());
        add(getEnregistrerButton());
        add(getEnregistrersousButton());
    }
    // ------------------------------------------- GET et SET des bouttons ----------------------------------------------------------------
	/**
	 * get the nouveauButton
	 * @return the nouveauButton
	 */
	public javax.swing.JButton getNouveauButton() {
		return nouveauButton;
	}
	
	/**
	 * set the nouveauButton
	 * @param nouveauButton the nouveauButton to set
	 */
	public void setNouveauButton(javax.swing.JButton nouveauButton) {
		this.nouveauButton = nouveauButton;
	}
	
	/**
	 * get the setSupprimerButton
	 * @return the supprimerButton
	 */
	public javax.swing.JButton getSupprimerButton() {
		return supprimerButton;
	}
	
	/**
	 * set the setSupprimerButton
	 * @param supprimerButton the supprimerButton to set
	 */
	public void setSupprimerButton(javax.swing.JButton supprimerButton) {
		this.supprimerButton = supprimerButton;
	}
	
	/**
	 * get the chargerButton
	 * @return the chargerButton
	 */
	public javax.swing.JButton getChargerButton() {
		return chargerButton;
	}
	
	/**
	 * set the chargerButton
	 * @param chargerButton the chargerButton to set
	 */
	public void setChargerButton(javax.swing.JButton chargerButton) {
		this.chargerButton = chargerButton;
	}
	
	/** 
	 * get the enregistrerButton
	 * @return the enregistrerButton
	 */
	public javax.swing.JButton getEnregistrerButton() {
		return enregistrerButton;
	}
	
	/**
	 * set the enregistrerButton
	 * @param enregistrerButton the enregistrerButton to set
	 */
	public void setEnregistrerButton(javax.swing.JButton enregistrerButton) {
		this.enregistrerButton = enregistrerButton;
	}
	
	/**
	 * get the enregistrersousButton
	 * @return the enregistrersousButton
	 */
	public javax.swing.JButton getEnregistrersousButton() {
		return enregistrersousButton;
	}
	
	/**
	 * set the enregistrersousButton
	 * @param enregistrersousButton the enregistrersousButton to set
	 */
	public void setEnregistrersousButton(javax.swing.JButton enregistrersousButton) {
		this.enregistrersousButton = enregistrersousButton;
	}
	
	// ------------------------------------------- le MAIN ----------------------------------------------------------------
	/**
	 * The start
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
	  java.awt.EventQueue.invokeLater(new Runnable() {
	    public void run() {
	      new BarreOutils().setVisible(true);
	    }
	  });
	}
}