package pck;

import javax.swing.*;
import java.text.*;

/**
 * Make the pannel in the SOUTH
 * @author rozenn
*/
public class BarreEtat extends javax.swing.JPanel{
    
    private javax.swing.JLabel bonjourLabel; // bonjour < user>! (gauche)
    private javax.swing.JLabel dateLabel; // date du jour (droite)
    
/**
 * the constructor
 * @param user the user in the application
 * @param date the date of today
 */
    public BarreEtat(String user, String date ) {
    	this.bonjourLabel = new javax.swing.JLabel();
        this.dateLabel = new javax.swing.JLabel();
    	initComponents(user , date );
    }
/**
 * instance the Components
 * @param user the user in the application
 * @param date the date of today
 */
    private void initComponents(String user, String date) {
        this.bonjourLabel.setText("Bonjour " + user +"!");
        this.dateLabel.setText(date);
        
       setLayout(new java.awt.BorderLayout());
        add ( this.bonjourLabel , java.awt.BorderLayout.WEST );
        add ( this.dateLabel , java.awt.BorderLayout.EAST );

    }
/**
 * set the label
 * @param label : the new text in the label
*/
    public void setBonjourLabel(String label) {
    	this.bonjourLabel.setText(label);
    }
  
  /**
   * The start
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new BarreEtat(args[0], args[1]).setVisible(true);
      }
    });
  }

}