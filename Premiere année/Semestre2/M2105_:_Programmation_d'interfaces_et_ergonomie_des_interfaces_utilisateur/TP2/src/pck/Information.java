package pck;

import javax.swing.*;
import javax.swing.text.MaskFormatter;

import java.awt.Dimension;
import java.text.*;

public class Information extends javax.swing.JPanel{
    
    private JLabel nomLabel; // (gauche)
    private JTextField nomTextField;
	private JLabel prenomLabel; // (gauche)
    private JTextField prenomTextField; 
	private JLabel dateNaissanceLabel; // (gauche)
    private DateFormat date;
    private JFormattedTextField dateNaissanceTextField;
	private JLabel courielLabel; //(gauche)
    private JTextField coutielTextField; 
	private JRadioButton hommeBoutonRadio; //(gauche)
    private JRadioButton femmeBoutonRadio;    
    private JLabel redoublantLabel; //(gauche)
    private JCheckBox a1CheckBox; //(interigne)
    private JCheckBox a2CheckBox; //(interigne)  
    private JLabel moyenLabel; //(droite)
    private DefaultListModel dlm;
    private JList moyenList; 
	private JButton ajouteNoteButton;
    private JButton supprimeNoteButton;
    private JTextField ajouteNoteTextField;
    private JPanel panel1;
    private JPanel panel2;
    private JPanel panel3;
    private JPanel ajouteNotePanel;
    private JScrollPane scrollNote;
    private double moyentot;    
    public MaskFormatter masc;
    public JFormattedTextField[] note;
    
    /**
     * the constructor
     */
    public Information() {
        initComponents();
    }
    
    /**
     * instance the Components
     */
    private void initComponents() {

    	
        nomLabel = new JLabel();
        nomTextField = new JTextField();
        
        prenomLabel = new JLabel();
        prenomTextField = new JTextField();
        
        dateNaissanceLabel = new JLabel();
        date = new SimpleDateFormat("yyyy.MM.dd");
        dateNaissanceTextField = new JFormattedTextField(date);
        
        courielLabel = new JLabel();
        coutielTextField = new JTextField();
        
        hommeBoutonRadio = new JRadioButton("H", true);
        femmeBoutonRadio = new JRadioButton("F", false);
        
        redoublantLabel = new JLabel();
        a1CheckBox = new JCheckBox("1A", false);
        a2CheckBox = new JCheckBox("2A", false);
        
        
        
        moyenLabel = new JLabel();
        dlm = new DefaultListModel<Double>();
        moyenList = new JList(dlm);
        
        dlm.addElement(10.0);
        dlm.addElement(16.0);
        calculMoyenne();
        
        ajouteNoteButton = new JButton();
        supprimeNoteButton = new JButton();
        ajouteNoteTextField = new JTextField();
        
        MaskFormatter masc;
		try {
			masc = new MaskFormatter("##.#");
			ajouteNoteTextField = new JFormattedTextField(masc);
			ajouteNoteTextField.setText("10.0");
		} catch (ParseException e) {
			e.printStackTrace();
			ajouteNoteTextField = new JTextField();
		}
        
        panel1 = new JPanel();
        panel2 = new JPanel();
        panel3 = new JPanel();
        scrollNote = new JScrollPane(moyenList);
        ajouteNotePanel = new JPanel();
     

        nomLabel.setText("Nom");
        prenomLabel.setText("Prenom");
        dateNaissanceLabel.setText( "Date de Naissance");
        courielLabel.setText (" Courriel");
        redoublantLabel.setText ("redoublant");  
        ajouteNoteButton.setText("Ajouter une note");
        supprimeNoteButton.setText("suprrimer une note");
        
        // -------------------------------------------------panel a gauche
        panel1.setLayout(new java.awt.GridLayout(8,2)); 
        panel1.add(nomLabel); panel1.add(nomTextField);
        panel1.add(prenomLabel); panel1.add(prenomTextField);
        panel1.add(dateNaissanceLabel); panel1.add(dateNaissanceTextField);
        panel1.add(courielLabel); panel1.add(coutielTextField);
        panel1.add(hommeBoutonRadio); panel1.add(femmeBoutonRadio);
        panel1.add(redoublantLabel); panel1.add(panel3);
        panel1.add(panel3); panel1.add(a1CheckBox);
        panel1.add(panel3); panel1.add(a2CheckBox);
        
        // -------------------------------------------------panel des bouton de droite pour les note
        ajouteNoteTextField.setPreferredSize(new Dimension(75,20));
        ajouteNotePanel.setLayout(new java.awt.BorderLayout());
        ajouteNotePanel.add(ajouteNoteButton, java.awt.BorderLayout.CENTER);
        ajouteNotePanel.add(supprimeNoteButton, java.awt.BorderLayout.EAST);
        ajouteNotePanel.add(ajouteNoteTextField, java.awt.BorderLayout.WEST);
        
        // -------------------------------------------------panel a droite
        panel2.setLayout(new java.awt.BorderLayout());
        panel2.add(moyenLabel,java.awt.BorderLayout.NORTH );
        panel2.add(scrollNote, java.awt.BorderLayout.CENTER);
        panel2.add(ajouteNotePanel, java.awt.BorderLayout.SOUTH);
        
        
        // -------------------------------------------------------------disposition
        setLayout(new java.awt.BorderLayout()); 
        add(panel1,java.awt.BorderLayout.WEST );
        add(panel2,java.awt.BorderLayout.EAST );
        
    }
    /**
     * make the average
     */
    public void calculMoyenne() {
    	double no=0, nbN=0;
		if(dlm.getSize()>0) {
		for (int i = 0 ;  (i < dlm.getSize()) ; i++ ) {
			  no+= (Double) dlm.get(i);
			  nbN = i+1;
		}
		this.moyentot = no/nbN;
		this.moyenLabel.setText("Moyenne :"+moyentot);
		}
		else {
			  this.moyenLabel.setText("Moyenne : Pas de note !");
		}
	}
    /**
     * get the number of the note
     * @return the number
     */
    public int getNbNote() {
		return this.dlm.getSize();
	}

	
	/**
	 * remove a note in a index
	 * @param selectedIndex : the index to want remove the note
	 */
	public void suprimmerNote(int selectedIndex) {
		if(selectedIndex!=-1) {
			this.dlm.remove(selectedIndex);
			calculMoyenne();
		}else {
			JOptionPane.showMessageDialog(null, "vous n'avez pas selectionner la note à supprimer");
		}
	
		
	}
	/** 
	 * reset the panel
	 */
	public void reset() {
		this.nomTextField.setText("");
		this.prenomTextField.setText("");
		this.coutielTextField.setText("");
		this.dateNaissanceTextField.setText("");
		this.a1CheckBox.setSelected(false);
		this.a2CheckBox.setSelected(false);
		this.dlm.clear();
		this.calculMoyenne();
	}
	
	// ------------------------------------------- GET et SET des TextField ---------------------------------------------------
	
	/**
	 * get the textField of the nom
	 * @return the textField
	 */
	public JTextField getNomTextField() {
		return nomTextField;
	}
	
	/**
	 * set the textField of the nom
	 * @param nomTextField : the new textField 
	 */
	public void setNomTextField(JTextField nomTextField) {
		this.nomTextField = nomTextField;
	}
	/**
	 * get the textField of the prenom
	 * @return the textField
	 */
	public JTextField getPrenomTextField() {
		return prenomTextField;
	}
	
	/**
	 * 
	 * @param prenomTextField : the new textField 
	 */
	public void setPrenomTextField(JTextField prenomTextField) {
		this.prenomTextField = prenomTextField;
	}
	/**
	 * get the textField of the birthday date
	 * @return the textField
	 */
	public JFormattedTextField getDateNaissanceTextField() {
		return dateNaissanceTextField;
	}
	/**
	 * 
	 * @param dateNaissanceTextField : the new textField 
	 */
	public void setDateNaissanceTextField(JFormattedTextField dateNaissanceTextField) {
		this.dateNaissanceTextField = dateNaissanceTextField;
	}
	/**
	 * get the textField of the courier
	 * @return the textField
	 */
	public JTextField getCoutielTextField() {
		return coutielTextField;
	}
	
	/**
	 * set the textField of the courier
	 * @param coutielTextField : the new textField 
	 */
	public void setCoutielTextField(JTextField coutielTextField) {
		this.coutielTextField = coutielTextField;
	}
	/**
	 * get the textField of the add note
	 * @return the textField
	 */
	public JTextField getAjouteNoteTextField() {
		return ajouteNoteTextField;
	}
	/**
	 * set the textField of the add note
	 * @param ajouteNoteTextField : the new textField 
	 */
	public void setAjouteNoteTextField(JTextField ajouteNoteTextField) {
		this.ajouteNoteTextField = ajouteNoteTextField;
	}
	// ------------------------------------------- GET et SET des RadioButton ---------------------------------------------------
	/**
	 * get the gender
	 * @return true if is man select
	 */
	 public boolean getSex(){
	    	boolean ret = false;
	    	if(this.hommeBoutonRadio.isSelected()) {
	    		ret=true;
	    	}
	    	return ret;
	    }
	 /**
	  * set the gender with  file to open
	  * @param sex : the string for the gender
	  */
	 public void setSex(String sex){
    	if (sex.equals("true")){
    		hommeBoutonRadio.setSelected(true);
    	}
     }
	 /**
	  * set the man button
	  * @param bool : the boolean to want changed
	  */
	 public void sethommeBoutonRadio(boolean bool) {
	    	this.hommeBoutonRadio.setSelected(bool);
	 }
	 /**
		 * get the man
		 * @return true if is select
	*/
	 public boolean etathommeBoutonRadio() {
	    	return this.hommeBoutonRadio.isSelected();
	 }
	 /**
	     * get the JRadioButton man
	     * @return the JRadioButton
	 */
	 public JRadioButton gethommeBoutonRadio() {
	    	return this.hommeBoutonRadio;
	    }
	 /**
	  * set the woman button
	  * @param bool : the boolean to want changed
	  */
    public void setfemmeBoutonRadio(boolean bool) {
    	this.femmeBoutonRadio.setSelected(bool);
    }
    /**
	 * get the woman
	 * @return true if is select
	 */
    public boolean etatfemmeBoutonRadio() {
    	return this.femmeBoutonRadio.isSelected();
    }
    /**
     * get the JRadioButton woman
     * @return the JRadioButton
     */
    public JRadioButton getfemmeBoutonRadio() {
    	return this.femmeBoutonRadio;
    }
	 // ------------------------------------------------------ GET et SET de checkButton -------------------------------
	 /**
	  * Set the a1CheckBox with a String true
	  * @param oui : the String in the file want charge
	  */
	 public void set1a(String oui) {
		if (oui == "true") {
			a1CheckBox.setSelected(true);
		}
		else {
			a1CheckBox.setSelected(false);
		}
    }
	 /**
	  * Set the a2CheckBox with a String true
	  * @param oui : the String in the file want charge
	  */
    public void set2a(String oui) {
    	if (oui == "true") {
    		a2CheckBox.setSelected(true);
		}
		else {
			a2CheckBox.setSelected(false);
		}
    }

    
    public boolean get1a(){
    	boolean ret = false;
    	if(this.a1CheckBox.isSelected()) {
    		ret=true;
    	}
    	return ret;
    }
    
    public boolean get2a(){
    	boolean ret = false;
    	if(this.a1CheckBox.isSelected()) {
    		ret=true;
    	}
    	return ret;
    }
    
    // ------------------------------------------------------ GET et SET de dlm -------------------------------
    /**
     * get the dlm 
     * @return the DefaultListModel
     */
    public DefaultListModel getDlm() {
		return dlm;
	}
    /**
     * set the dlm
     * @param dlm : the new DefaultListModel
     */
	public void setDlm(DefaultListModel dlm) {
		this.dlm = dlm;
	}
	// ---------------------------------------------------- GET et SET du bouton suprimer note -------------------------------
	/**
	 * get the button supprimerNote
	 * @return the button 
	 */
	public JButton getSupprimeNoteButton() {
		return supprimeNoteButton;
	}
	
	/**
	 * set the button supprimerNote
	 * @param supprimeNoteButton : the new button
	 */
	public void setSupprimeNoteButton(JButton supprimeNoteButton) {
		this.supprimeNoteButton = supprimeNoteButton;
	}
	// ---------------------------------------------------- GET et SET du bouton ajoute note -------------------------------
	/**
	 * get the button add note  
	 * @return the button
	 */
	public JButton getAjouteNoteButton() {
			return this.ajouteNoteButton;
		}
	/**
	 * set the button add note  
	 * @param ajouteNoteButton : the new button
	 */
	public void setAjouteNoteButton(JButton ajouteNoteButton) {
			this.ajouteNoteButton = ajouteNoteButton;
		}
	/**
	 * add a note and change the average
	 * @param note : the new note
	 */
	public void ajouterNote(double note) {
		if(note>=0.0 && note<=20.0) {
			  dlm.addElement(note);
			  calculMoyenne();
		}
	  }
	// ---------------------------------------------------- GET et SET de la liste des notes -------------------------------
	/**
     * get the liste of the note
     * @return the list of notes
     */
    public JList getMoyenList() {
		return moyenList;
	}
    /**
     * set the liste of the note
     * @param moyenList : the nex list of notes
     */
	public void setMoyenList(JList moyenList) {
		this.moyenList = moyenList;
	}
	
}
