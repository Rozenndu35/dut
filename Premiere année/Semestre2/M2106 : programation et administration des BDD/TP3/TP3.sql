/*
01/02/19 15h30
TP2_COSTIOU_rozenn.sql
Costiou Rozenn groupe 1B1
*/

DROP TABLE Reservation;
DROP TABLE Bateau;
DROP TABLE Emplacement ;
DROP TABLE Proprietaire;


CREATE TABLE Proprietaire(
                lIdentifiant  NUMBER(4)
                CONSTRAINT pk_lIdentifiant PRIMARY KEY,
                nom VARCHAR2(20),
                prenom VARCHAR2(20),
                lAdresse VARCHAR2(50)
                CONSTRAINT nn_lAdresse NOT NULL,
                CONSTRAINT uq_Proprietaire_lAdresse UNIQUE (lAdresse)
                )
;

                
CREATE TABLE Emplacement(
                numeroE NUMBER(4)
                CONSTRAINT pk_Emplacement_numeroE PRIMARY KEY,
                longueurE  NUMBER(2)
                CONSTRAINT nn_Emplacement_longueurE NOT NULL
                CONSTRAINT ck_Emplacement_longueurE CHECK (( longueurE>=3) AND ( longueurE<=20)) 
                 );

CREATE TABLE Bateau(
                noImm  NUMBER(4)
                CONSTRAINT pk_noImm PRIMARY KEY,
                nom VARCHAR2(20),
                longueur  NUMBER(2)
                CONSTRAINT ck_longueurb CHECK (( longueur>=3) AND ( longueur<=20))
                CONSTRAINT nn_longueurb NOT NULL,
                largeur  NUMBER(2)
                CONSTRAINT ck_largeur CHECK ( largeur<=6),
                leProp NUMBER(4)
                CONSTRAINT fk_Bateau_leProp REFERENCES Proprietaire(lIdentifiant ) NOT NULL,
                lEmplacements NUMBER(4)
                CONSTRAINT fk_Bateau_leEmplacement REFERENCES Emplacement(numeroE),
                CONSTRAINT uq_Bateau_leEmplacement UNIQUE (lEmplacements),
                dateDebuts DATE,
                dateFins DATE
                );
                            
CREATE TABLE Reservation(
                numero NUMBER(4)
                CONSTRAINT pk_Reservation_numero PRIMARY KEY,
                dateDebutR DATE,
                dateFinR DATE,
                CONSTRAINT ck_Reservation_dateFinR CHECK ( dateDebutR <= dateFinR),
                leBateau NUMBER(4)
                CONSTRAINT fk_Reservation_leBateau REFERENCES Bateau(noImm ) NOT NULL,
                lEmplacementR NUMBER(4)
                CONSTRAINT fk_Reservation_lEmplacementR REFERENCES Emplacement(numeroE) NOT NULL
                 );
                 
                 
DELETE FROM Reservation;
DELETE FROM Bateau;
DELETE FROM Emplacement;
DELETE FROM Proprietaire;


INSERT INTO PROPRIETAIRE VALUES(1,'ORVOEN','Romain','2 rue des bouleaux 56000 Vannes');
INSERT INTO PROPRIETAIRE VALUES(2,'LANDUREIN','Enora',' rue du capitaine 56000 Lorient');
INSERT INTO PROPRIETAIRE VALUES(3,'BRUNET','Benoit','2 rue des chenes 56000 Pontivy');
INSERT INTO PROPRIETAIRE VALUES(4,'LOUVEL','Victor','8 rue du lys 56000 Sene');
INSERT INTO PROPRIETAIRE VALUES(5,'PULSFORD','Carolynn','12 rue galieni 56000 Vannes');
INSERT INTO PROPRIETAIRE VALUES(6,'CASTEL','Clement','22 rue victor Hugo 56000 Lorient');
INSERT INTO PROPRIETAIRE VALUES(7,'LE GAL','Quentin','28 rue emile Zola 56000 Vannes');
INSERT INTO PROPRIETAIRE VALUES(8,'COTHENET','Erwann','13 rue tintin 56000 Vannes');
INSERT INTO PROPRIETAIRE VALUES(9,'HAMON','Alizee','36 rue Lagaffe 56000 Vannes');

INSERT INTO EMPLACEMENT VALUES(1,6);
INSERT INTO EMPLACEMENT VALUES(2,18);
INSERT INTO EMPLACEMENT VALUES(3,18);
INSERT INTO EMPLACEMENT VALUES(4,8);
INSERT INTO EMPLACEMENT VALUES(5,8);
INSERT INTO EMPLACEMENT VALUES(6,12);
INSERT INTO EMPLACEMENT VALUES(7,12);
INSERT INTO EMPLACEMENT VALUES(8,5);
INSERT INTO EMPLACEMENT VALUES(9,5);
INSERT INTO EMPLACEMENT VALUES(10,6);
INSERT INTO EMPLACEMENT VALUES(11,6);

INSERT INTO BATEAU VALUES(1,'tient bon',3,1,1,1,TO_DATE('01/02/2018','DD/MM:YYYY'),TO_DATE('10/02/2018','DD/MM,YYYY'));
INSERT INTO BATEAU VALUES(2,'ca flotte',12,4,2,6,TO_DATE('01/11/2017','DD/MM:YYYY'),TO_DATE('10/02/2018','DD/MM,YYYY'));
INSERT INTO BATEAU VALUES(3,'Pourquoi pas',4,2,3,7,TO_DATE('01/10/2017','DD/MM:YYYY'),TO_DATE('10/02/2018','DD/MM,YYYY'));
INSERT INTO BATEAU VALUES(4,'Manureva',15,4,4,2,TO_DATE('01/12/2017','DD/MM:YYYY'),TO_DATE('10/02/2018','DD/MM,YYYY'));
INSERT INTO BATEAU VALUES(5,'TiTi 56',3,1,5,5,TO_DATE('15/02/2017','DD/MM:YYYY'),TO_DATE('10/02/2018','DD/MM,YYYY'));
INSERT INTO BATEAU VALUES(6,'petit bouchon',10,2,4,3,TO_DATE('18/02/2017','DD/MM:YYYY'),TO_DATE('10/02/2018','DD/MM,YYYY'));
INSERT INTO BATEAU VALUES(7,'Myosotis',15,3,7,NULL,NULL,NULL);
INSERT INTO BATEAU VALUES(8,'a bientot',3,1,7,NULL,NULL,NULL);
INSERT INTO BATEAU VALUES(9,'Paquebot',15,3,8,NULL,NULL,NULL);
INSERT INTO BATEAU VALUES(10,'loulou 56',3,1,9,NULL,NULL,NULL);

INSERT INTO RESERVATION VALUES(1,TO_DATE('01/02/2018','DD/MM:YYYY'),TO_DATE('10/02/2018','DD/MM,YYYY'),1,1);
INSERT INTO RESERVATION VALUES(2,TO_DATE('01/11/2018','DD/MM:YYYY'),TO_DATE('10/02/2019','DD/MM,YYYY'),2,6);
INSERT INTO RESERVATION VALUES(3,TO_DATE('01/10/2018','DD/MM:YYYY'),TO_DATE('10/02/2019','DD/MM,YYYY'),3,7);
INSERT INTO RESERVATION VALUES(4,TO_DATE('01/12/2018','DD/MM:YYYY'),TO_DATE('10/02/2019','DD/MM,YYYY'),7,2);
INSERT INTO RESERVATION VALUES(5,TO_DATE('15/01/2019','DD/MM:YYYY'),TO_DATE('10/02/2019','DD/MM,YYYY'),5,5);
INSERT INTO RESERVATION VALUES(6,TO_DATE('01/02/2019','DD/MM:YYYY'),TO_DATE('10/02/2019','DD/MM,YYYY'),7,3);
INSERT INTO RESERVATION VALUES(7,TO_DATE('15/02/2018','DD/MM:YYYY'),TO_DATE('10/06/2018','DD/MM,YYYY'),3,7);
INSERT INTO RESERVATION VALUES(8,TO_DATE('14/02/2019','DD/MM:YYYY'),TO_DATE('10/05/2019','DD/MM,YYYY'),2,6);
INSERT INTO RESERVATION VALUES(9,TO_DATE('14/02/2019','DD/MM:YYYY'),TO_DATE('10/06/2019','DD/MM,YYYY'),8,8);


--1-
SELECT numeroE
FROM Emplacement
MINUS
SELECT lEmplacementR 
FROM Reservation
;

/**
   NUMEROE
----------
         4
         9
        10
        11
    
*/

--2-
SELECT UPPER (nom) , UPPER (prenom)
FROM Proprietaire
WHERE UPPER(lAdresse) LIKE '%VANNES%'
;

/**
UPPER(NOM)           UPPER(PRENOM)       
-------------------- --------------------
ORVOEN               ROMAIN              
PULSFORD             CAROLYNN            
LE GAL               QUENTIN             
COTHENET             ERWANN              
HAMON                ALIZEE              

*/

--3-
SELECT DISTINCT UPPER (Proprietaire.nom)
FROM Proprietaire , Reservation , Bateau 
WHERE lIdentifiant = leProp
AND leBateau = noImm
;
/**
UPPER(PROPRIETAIRE.N
--------------------
LE GAL
LANDUREIN
BRUNET
ORVOEN
PULSFORD
*/

--4-
SELECT DISTINCT UPPER (Proprietaire.nom)
FROM Proprietaire
MINUS
SELECT DISTINCT UPPER (Proprietaire.nom)
FROM Proprietaire , Reservation , Bateau 
WHERE lIdentifiant = leProp
AND leBateau = noImm
;

/*
SELECT DISTINCT UPPER (Proprietaire.nom)
FROM Proprietaire , Reservation , Bateau 
WHERE lIdentifiant = leProp
AND leBateau = noImm
*/

--5-
SELECT DISTINCT UPPER (nom)
FROM  Reservation , Bateau 
WHERE leBateau = noImm
;
/**
UPPER(NOM)          
--------------------
A BIENTOT
POURQUOI PAS
TIENT BON
CA FLOTTE
MYOSOTIS
TITI 56

6 lignes sélectionnées. 
*/

--6-
SELECT DISTINCT UPPER (proprietaire.nom)
FROM Proprietaire, Bateau
WHERE lIdentifiant = leProp 
GROUP BY UPPER (proprietaire.nom)
HAVING COUNT (leProp) >=2
;

/**
UPPER(PROPRIETAIRE.N
--------------------
LE GAL
LOUVEL
*/

--7-
SELECT DISTINCT UPPER (nom)
FROM Bateau
MINUS
SELECT DISTINCT UPPER (nom)
FROM  Reservation , Bateau 
WHERE leBateau = noImm
;

/**
UPPER(NOM)          
--------------------
LOULOU 56
MANUREVA
PAQUEBOT
PETIT BOUCHON
*/

--8-
SELECT DISTINCT UPPER (nom)
FROM Bateau
WHERE longueur > 5
ORDER BY UPPER (nom)
;

/**
UPPER(NOM)          
--------------------
CA FLOTTE
MANUREVA
MYOSOTIS
PAQUEBOT
PETIT BOUCHON

*/

--9-
SELECT longueurE, COUNT(*)
FROM Emplacement
GROUP BY longueurE
;
/**

 LONGUEURE   COUNT(*)
---------- ----------
         6          3
         5          2
         8          2
        18          2
        12          2

*/

--10-
SELECT DISTINCT UPPER (proprietaire.nom)
FROM Proprietaire, Bateau
WHERE leProp = lIdentifiant 
AND longueur = (SELECT MAX (longueur)
                FROM Bateau
                )
;

/**
UPPER(PROPRIETAIRE.N
--------------------
LE GAL
COTHENET
LOUVEL
*/

--11-

SELECT DISTINCT UPPER (leBateau) 
FROM reservation 
GROUP BY leBateau
WHERE NOT EXISTS= ( SELECT numero
                    FROM Reservation , Emplacement
                    WHERE numeroE = lEmplacementR
                    AND longueurE > 12
                    MINUS
                    SELECT numero
                    FROM Reservation
                    )
                   
;                     
/**
*/

--12- 
SELECT DISTINCT UPPER (proprietaire.nom)
FROM Proprietaire,Bateau
WHERE leProp = lIdentifiant
AND noImm = (
