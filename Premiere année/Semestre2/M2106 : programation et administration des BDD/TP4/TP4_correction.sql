DROP TABLE Reservation;
DROP TABLE Bateau;
DROP TABLE Emplacement;
DROP TABLE Proprietaire;
-- permet d utiliser la commande DBMS_OUTPUT.put_line('.............');
set serveroutput on;
--
CREATE TABLE Proprietaire(
				identifiant NUMBER(4)
				CONSTRAINT pkProprietaire PRIMARY KEY,
				nom VARCHAR2(20)
				CONSTRAINT nnNomProprietaire NOT NULL,
				prenom VARCHAR2(20)
				CONSTRAINT nnPrenomProprietaire NOT NULL,
				adresse VARCHAR2(40)
				CONSTRAINT nnAdresseProprietaire NOT NULL,
				CONSTRAINT uqNomPrenomProprietaire UNIQUE(nom,prenom));
--
CREATE TABLE Emplacement(
				numero NUMBER(4)
				CONSTRAINT pkEmplacement PRIMARY KEY,
				longueur NUMBER(2)
				CONSTRAINT ckLongueurEmplacement
				CHECK((longueur >= 3) AND (longueur <= 20))
				CONSTRAINT nnLongeurEmplacement NOT NULL);
--
CREATE TABLE Bateau(
				noImm NUMBER(4)
				CONSTRAINT pkBateau PRIMARY KEY,
				nom VARCHAR2(20),
				longueur NUMBER(2)
				CONSTRAINT ckLongueurBateau
				CHECK((longueur >= 3) AND (longueur <= 20))
				CONSTRAINT nnlongueurBateau NOT NULL,
				largeur NUMBER(2)
				CONSTRAINT ckLargeurBateau
				CHECK(largeur <= 6),
				leP NUMBER(4)
				CONSTRAINT fkBateauProprietaire REFERENCES Proprietaire(identifiant)
				CONSTRAINT nnLep NOT NULL,
				lEmp NUMBER(4)
				CONSTRAINT fkBateauEmplacement REFERENCES Emplacement(numero)
				CONSTRAINT uqLemp UNIQUE,
				dateD DATE,
				dateF DATE);
--
CREATE TABLE Reservation(
				numero NUMBER(4)
				CONSTRAINT pkReservation PRIMARY KEY,
				dateDebut DATE
				CONSTRAINT nnDateDebut NOT NULL,
				dateFin DATE
				CONSTRAINT nnDateFin NOT NULL,
				leBateau NUMBER(4)
				CONSTRAINT fkReservationBateau REFERENCES Bateau(noImm)
				CONSTRAINT nnLeB NOT NULL,
				lEmplacement NUMBER(4)
				CONSTRAINT fkReservationEmplacement REFERENCES Emplacement(numero)
				CONSTRAINT nnLemplacement NOT NULL);
--
-- la date debut doit preceder la date fin dans Reservation
--
CREATE OR REPLACE TRIGGER DATE_RESA 
BEFORE INSERT OR UPDATE ON Reservation
FOR EACH ROW
BEGIN
	IF (TO_DATE(:NEW.dateDebut,'DD/MM/YYYY') > TO_DATE(:NEW.dateFin,'DD/MM/YYYY')) THEN
--	IF (:NEW.dateDebut,'DD/MM/YYYY' > :NEW.dateFin,'DD/MM/YYYY') THEN
		RAISE_APPLICATION_ERROR(-20002 ,'la date debut doit preceder la dateFin');
	END IF;
END;
/
SHOW ERROR TRIGGER DATE_RESA; -- montre les erreurs eventuelles de compilation
-- test de ce trigger :
INSERT INTO Proprietaire VALUES(1,'Toto','Toto','3 rue des bouleaux 56000 Vannes');
INSERT INTO Emplacement VALUES(1,8);
INSERT INTO Bateau VALUES(1,'Manureva',7,4,1,NULL,NULL,NULL);
INSERT INTO Reservation VALUES(1,TO_DATE('11/12/2017','DD/MM/YYYY'),TO_DATE('10/12/2016','DD/MM/YYYY'),1,1);
INSERT INTO Bateau VALUES(2,'Manureva2',7,4,1,1,TO_DATE('11/12/2017','DD/MM/YYYY'),TO_DATE('10/12/2016','DD/MM/YYYY'));
-- on vide les tables
--TRUNCATE TABLE Reservation;
--TRUNCATE TABLE Bateau;
--TRUNCATE TABLE Emplacement;
--TRUNCATE TABLE Proprietaire;
DELETE FROM Reservation;
DELETE FROM Bateau;
DELETE FROM Emplacement;
DELETE FROM Proprietaire;
--
-- un bateau ne peut stationner que dans un emplacement de longueur >= à celle du bateau
--
CREATE OR REPLACE TRIGGER Stationnement
BEFORE INSERT OR UPDATE ON Reservation
FOR EACH ROW
DECLARE
	tailleEmp Emplacement.longueur%TYPE;
	tailleBat Bateau.longueur%TYPE;
BEGIN
	SELECT longueur INTO tailleEmp
	FROM Emplacement
	WHERE :NEW.lEmplacement = numero;
--
	SELECT longueur INTO tailleBat
	FROM Bateau
	WHERE :NEW.leBateau = noImm;
--
	IF ( tailleEmp < tailleBat ) THEN
		RAISE_APPLICATION_ERROR(-20003,'la longueur du bateau doit etre inferieure ou egale a celle de l emplacement');
	END IF;
END;
/
SHOW ERROR TRIGGER Stationnement; -- montre les erreurs eventuelles de compilation
-- test de ce trigger :
INSERT INTO Proprietaire VALUES(1,'Toto','Toto','2 rue pompidou 35000 Rennes');
INSERT INTO Emplacement VALUES(1,8);
INSERT INTO Bateau VALUES(1,'Manureva',9,4,1,NULL,NULL,NULL);
INSERT INTO Reservation VALUES(1,TO_DATE('11/12/2017','DD/MM/YYYY'),TO_DATE('20/12/2017','DD/MM/YYYY'),1,1);
-- on vide les tables
--TRUNCATE TABLE Reservation;
--TRUNCATE TABLE Bateau;
--TRUNCATE TABLE Emplacement;
--TRUNCATE TABLE Proprietaire;
DELETE FROM Reservation;
DELETE FROM Bateau;
DELETE FROM Emplacement;
DELETE FROM Proprietaire;
--
-- un bateau ne peut stationner que si une reservation correspondante a été effectuée
--
--autre facon : compter le nb de t uples ds reservation qui correspondent et faire if nbResa = 0 then erreur .....
--
CREATE OR REPLACE TRIGGER Station
BEFORE UPDATE ON Bateau
FOR EACH ROW
WHEN (NEW.lEmp IS NOT NULL)
DECLARE 
	nbResa NUMBER;
BEGIN
--
	nbResa := 0;
	SELECT COUNT(*) INTO nbResa
	FROM Reservation
	WHERE leBateau = :NEW.noImm
	AND DateDebut = :NEW.dateD
	AND DateFin = :NEW.dateF
	AND numero = :NEW.lEmp;
--	
	IF ( nbResa = 0 ) THEN 
		RAISE_APPLICATION_ERROR(-20004,'la reservation n a pas ete effectuee');
	END IF;
END;
/
SHOW ERROR TRIGGER Station; -- montre les erreurs eventuelles de compilation
DELETE FROM Reservation;
DELETE FROM Bateau;
DELETE FROM Emplacement;
DELETE FROM Proprietaire;
-- test de ce trigger :
INSERT INTO Proprietaire VALUES(1,'Toto','Toto','4 impasse des violettes 56000 Lorient');
INSERT INTO Emplacement VALUES(1,8);
INSERT INTO Bateau VALUES(1,'Manureva',7,4,1,NULL,NULL,NULL);
INSERT INTO Reservation VALUES(1,TO_DATE('11/12/2017','DD/MM/YYYY'),TO_DATE('20/12/2017','DD/MM/YYYY'),1,1);
-- un update qui passe
UPDATE Bateau SET lEmp = 1,
				  dateD = TO_DATE('11/12/2017','DD/MM/YYYY'),
				  dateF= TO_DATE('20/12/2017','DD/MM/YYYY')
			WHERE noImm = 1;
-- un update qui passe pas : les dates ne conviennent pas
UPDATE Bateau SET lEmp = 1,
				  dateD = TO_DATE('10/12/2016','DD/MM/YYYY'),
				  dateF= TO_DATE('20/12/2017','DD/MM/YYYY')
			WHERE noImm = 1;
-- un update qui passe pas : pas de reservation pour ce bateau et cet emplacement
INSERT INTO Bateau VALUES(2,'Manureva2',7,4,1,NULL,NULL,NULL);
UPDATE Bateau SET lEmp = 1,
				  dateD = TO_DATE('11/12/2017','DD/MM/YYYY'),
				  dateF= TO_DATE('20/12/2017','DD/MM/YYYY')
			WHERE noImm = 2;

select * from bateau;
			
DELETE FROM Reservation;
DELETE FROM Bateau;
DELETE FROM Emplacement;
DELETE FROM Proprietaire;
	

			