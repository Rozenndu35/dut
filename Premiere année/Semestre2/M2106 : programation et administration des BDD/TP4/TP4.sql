/*
25/01/19 15h30
TP2_COSTIOU_rozenn.sql
Costiou Rozenn groupe 1B1
*/

DROP TABLE Reservation;
DROP TABLE Bateau;
DROP TABLE Emplacement ;
DROP TABLE Proprietaire;


CREATE TABLE Proprietaire(
                lIdentifiant  NUMBER(4)
                CONSTRAINT pk_lIdentifiant PRIMARY KEY,
                nom VARCHAR2(20)
                CONSTRAINT nn_nom NOT NULL,
                prenom VARCHAR2(20)
                CONSTRAINT nn_prenom NOT NULL,
                CONSTRAINT uq_Proprietaire_nom_prenom UNIQUE (nom , prenom),
                lAdresse VARCHAR2(50)
                CONSTRAINT nn_lAdresse NOT NULL
                )
;

                
CREATE TABLE Emplacement(
                numeroE NUMBER(4)
                CONSTRAINT pk_Emplacement_numeroE PRIMARY KEY,
                longueurE  NUMBER(2)
                CONSTRAINT nn_Emplacement_longueurE NOT NULL
                CONSTRAINT ck_Emplacement_longueurE CHECK (( longueurE>=3) AND ( longueurE<=20)) 
                 );

CREATE TABLE Bateau(
                noImm  NUMBER(4)
                CONSTRAINT pk_noImm PRIMARY KEY,
                nom VARCHAR2(20),
                longueur  NUMBER(2)
                CONSTRAINT ck_longueurb CHECK (( longueur>=3) AND ( longueur<=20))
                CONSTRAINT nn_longueurb NOT NULL,
                largeur  NUMBER(2)
                CONSTRAINT ck_largeur CHECK ( largeur<=6),
                leProp NUMBER(4)
                CONSTRAINT fk_Bateau_leProp REFERENCES Proprietaire(lIdentifiant ) NOT NULL,
                CONSTRAINT uq_Bateau_leProp UNIQUE (leProp),
                lEmplacements NUMBER(4)
                CONSTRAINT fk_Bateau_leEmplacement REFERENCES Emplacement(numeroE),
                CONSTRAINT uq_Bateau_leEmplacement UNIQUE (lEmplacements),
                dateDebuts DATE,
                dateFins DATE
                );
                            
CREATE TABLE Reservation(
                numero NUMBER(4)
                CONSTRAINT pk_Reservation_numero PRIMARY KEY,
                dateDebutR DATE
                CONSTRAINT nn_dateDebutR NOT NULL,
                dateFinR DATE
                CONSTRAINT nn_dateFinR NOT NULL,
                CONSTRAINT ck_Reservation_dateFinR CHECK ( dateDebutR <= dateFinR), 
                leBateau NUMBER(4)
                CONSTRAINT fk_Reservation_leBateau REFERENCES Bateau(noImm ) NOT NULL,
                lEmplacementR NUMBER(4)
                CONSTRAINT fk_Reservation_lEmplacementR REFERENCES Emplacement(numeroE) NOT NULL
                 );
                 
CREATE OR REPLACE  TRIGGER Bateau_Emplacement
BEFORE INSERT OR UPDATE ON Reservation
FOR EACH ROW
DECLARE
    longBateau NUMBER;
    longEmplacement NUMBER;
BEGIN
    SELECT longueurE INTO longEmplacement
    FROM  Emplacement
    WHERE :NEW.lEmplacementR = numeroE;
    
    SELECT longueur INTO longBateau 
    FROM Bateau
    WHERE :NEW.leBateau = noImm;
    
    IF ( longBateau > longEmplacement)THEN 
        RAISE_APPLICATION_ERROR (- 20000,'bateau trop grand');
    END IF;
END;
/
CREATE OR REPLACE  TRIGGER Bateau_Reservation
BEFORE INSERT OR UPDATE ON Bateau
FOR EACH ROW
DECLARE
    dateD DATE;
    dateF DATE;
   
BEGIN
    SELECT dateDebutR  INTO dateD
    FROM Reservation
    WHERE :NEW.noImm =  leBateau;
    
    SELECT dateFinR  INTO dateF
    FROM Reservation
    WHERE :NEW.noImm =  leBateau;
        
    IF ( (dateD < :NEW.dateDebuts) AND (dateF < :NEW.dateFins) )THEN 
        RAISE_APPLICATION_ERROR (- 200000,'date  reservation et date emplacement invalide');
    END IF;
END;
