/*
25/01/19 15h30
TP2_COSTIOU_rozenn.sql
Costiou Rozenn groupe 1B1
*/

DROP TABLE Ecrit;
DROP TABLE Exemplaire1;
DROP TABLE Livre;
DROP TABLE Lecteur;
DROP TABLE Auteur1 ;
DROP TABLE Editeur ;


CREATE TABLE Editeur(
                noEditeur NUMBER(4)
                CONSTRAINT pk_noEditeur PRIMARY KEY,
                nomEditeur VARCHAR2(10) 
                CONSTRAINT nn_nomEditeur NOT NULL,
                adresseEditeur VARCHAR2(30)
                )
;

CREATE TABLE Auteur1(
                noAuteur NUMBER(4)
                CONSTRAINT pk_noAuteur PRIMARY KEY,
                nomAuteur VARCHAR2(10)
                CONSTRAINT nn_nomAuteur NOT NULL,
                prenomAuteur VARCHAR2(10)
                CONSTRAINT nn_prenomAuteur NOT NULL,
                dateNaissanceAuteur DATE,
                lEditeurA NUMBER(4)
                CONSTRAINT fk_Auteur1_lEditeur REFERENCES Editeur( noEditeur ) NOT NULL
                )
;

CREATE TABLE Lecteur(
                noLecteur NUMBER(4)
                CONSTRAINT pk_noLecteur PRIMARY KEY,
                nomLecteur VARCHAR2(10) 
                CONSTRAINT nn_nomLecteur NOT NULL,
                prenomLecteur VARCHAR2(10)
                CONSTRAINT nn_prenomLecteur NOT NULL,
                dateNaissanceAuteurLecteur DATE,
                argentDePoche NUMBER
                )
;

CREATE TABLE Livre(
                noLivre NUMBER(4)
                CONSTRAINT pk_noLivre PRIMARY KEY,
                titre VARCHAR2(12),
                prix NUMBER(4),
                typeLivre VARCHAR2(6)
                CONSTRAINT ck_typeLivre CHECK (typeLivre IN ('BD','POCHE'))
                )
;

CREATE TABLE Exemplaire1(
                noExemplaire NUMBER(4)
                CONSTRAINT pk_noExemplaire PRIMARY KEY,
                leLivre NUMBER(4)
                CONSTRAINT fk_Exemplaire_leLivre REFERENCES Livre( noLivre ) NOT NULL,
                leLecteur NUMBER(4)
                CONSTRAINT fk_Exemplaire_leLecteur REFERENCES Lecteur( noLecteur ),
                dateAchat DATE DEFAULT SYSDATE 
                )
;

CREATE TABLE Ecrit(
                lAuteur NUMBER(4)
                CONSTRAINT fk_Ecrit_lAuteur REFERENCES Auteur1(noAuteur),
                leLivre NUMBER(4)
                CONSTRAINT fk_Ecrit_leLivre REFERENCES Livre(noLivre),
                CONSTRAINT pk_Ecrit PRIMARY KEY (lAuteur, leLivre)
                )
;

DROP VIEW lExemplaireDe ;
DROP VIEW ecritPar1 ;
DROP VIEW aEcrit ;
DROP VIEW ecritEditeur;

CREATE VIEW lExemplaireDe 
(livreRestant)
AS 
SELECT noLivre
FROM Livre
MINUS
SELECT leLivre
FROM Exemplaire1
;
/*Jeu de test*/


CREATE VIEW ecritPar1
(livreRestant)
AS 
SELECT noLivre
FROM Livre
MINUS
SELECT leLivre
FROM Ecrit
;
/*Jeu de test*/

CREATE VIEW aEcrit
(auteurRestant)
AS 
SELECT noAuteur
FROM Auteur1
MINUS
SELECT lAuteur
FROM Ecrit
;
/*Jeu de test*/

CREATE VIEW ecritEditeur 
(editeur)
AS 
SELECT nomEditeur
FROM Livre , Editeur, Auteur1 , Ecrit
WHERE noLivre = leLivre
AND noAuteur = lAuteur
AND noEditeur = lEditeurA
;

/*Jeu de test*/

CREATE OR REPLACE TRIGGER PrixExemplaireArgentDePoche
BEFORE INSERT OR UPDATE ON  Exemplaire1
FOR EACH ROW
DECLARE
    v_prixL Livre.prix%TYPE;
    v_argent Lecteur.argentDePoche%TYPE;
BEGIN
    SELECT prix INTO v_prixL 
    FROM Livre 
    WHERE noLivre = :NEW.leLivre
    ;
    SELECT argentDePoche INTO v_argent
    FROM Lecteur
    WHERE noLecteur = :NEW.leLecteur
    ;
    
    IF (v_prixL > (v_argent /2)) THEN
        RAISE_APPLICATION_ERROR (-20000,'pas assez d argent de poche');
    END IF;
END
;
/
/*Jeu de test*/    


CREATE OR REPLACE TRIGGER AugmentationPrix
BEFORE UPDATE ON Livre
FOR EACH ROW
BEGIN 
    IF (:NEW.prix > 1.1*:OLD.prix) THEN
        RAISE_APPLICATION_ERROR (-20001,'augmentation trop importante');
    END IF;
END
;
/

/*Jeu de test*/    

CREATE OR REPLACE TRIGGER NomAuteurNomEditeur
BEFORE INSERT OR UPDATE ON  Auteur1
FOR EACH ROW
DECLARE
    v_nomEditeur Editeur.nomEditeur%TYPE;
BEGIN 
    
    SELECT nomEditeur INTO v_nomEditeur 
    FROM Editeur
    WHERE noEditeur = :NEW.lEditeurA
    ;
    IF (v_nomEditeur = :NEW.nomAuteur) THEN
        RAISE_APPLICATION_ERROR (- 20000,'editeur et auteur meme nom');
    END IF;
END
;
/

/*Jeu de test*/    


/* ------------------------------------------------ faire les test----------------------------------------*/
