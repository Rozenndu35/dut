
DROP TABLE Operation1;
DROP TABLE Appartient1;
DROP TABLE Compte1;
DROP TABLE Client1;
DROP TABLE Agent1;
DROP TABLE Agence1;


CREATE TABLE Agence1 (
			idAgence NUMBER(4)
			CONSTRAINT pk_Agence PRIMARY KEY,
			telAgence NUMBER(10),
			adAgence VARCHAR2(40)
			);
			
CREATE TABLE Agent1 (
			idAgent NUMBER(4)
			CONSTRAINT pkAgent PRIMARY KEY,
			nomA VARCHAR2(20),
			prenomA VARCHAR2(20),
            salaire NUMBER(5)
			CONSTRAINT ck_Salaire CHECK (salaire >= 1480),
			estDirecteur NUMBER(1)
            CONSTRAINT ck_estDirecteur CHECK (estDirecteur =1 OR estDirecteur =0),
			lAgence NUMBER(4)
			CONSTRAINT 	fk_LAgence REFERENCES	Agence1(idAgence)
			CONSTRAINT	nn_LAgence NOT NULL
			);
			
CREATE TABLE Client1 (
			idClient1 NUMBER(4)
			CONSTRAINT pk_Client1 PRIMARY KEY,
			nomClient VARCHAR2(20)
			CONSTRAINT nn_NomClient1 NOT NULL,
			prenomClient VARCHAR2(20)
			CONSTRAINT nn_PrenomClient NOT NULL,
			adClient VARCHAR2(40),
			dateNaisClient DATE,
			lAgent NUMBER(4)
			CONSTRAINT fk_LAgent REFERENCES Agent1(idAgent)
			CONSTRAINT nn_LAgent NOT NULL
			);
			
CREATE TABLE Compte1 (
			idCompte1 NUMBER(4)
			CONSTRAINT pk_Compte1 PRIMARY KEY,
            solde NUMBER(10),
			typeCompte VARCHAR2(30)
			CONSTRAINT ck_typeCompte1 CHECK (UPPER(typeCompte) = 'COMPTE EPARGNE' OR UPPER(typeCompte) = 'COMPTE COURANT')
			);

CREATE TABLE Appartient1 (
			unClient NUMBER(4)
			CONSTRAINT fk_UnClient REFERENCES Client1(idClient1),
			unCompte NUMBER(4)
			CONSTRAINT fk_UnCompte REFERENCES Compte1(idCompte1),
			CONSTRAINT pk_Appartient	PRIMARY KEY (unClient, unCompte)
			);

CREATE TABLE Operation1 (
			idOperations NUMBER(4)
			CONSTRAINT pk_Operations PRIMARY KEY,
			dateOpe DATE DEFAULT SYSDATE,
			typeOpe VARCHAR2(20)
			CONSTRAINT ck_Type CHECK (UPPER(typeOpe) = 'RETRAIT' OR UPPER(typeOpe) = 'DEPOT'),
			montant NUMBER(10)
			CONSTRAINT ck_montant CHECK (montant >= 0),
            leCompte NUMBER(4)
			CONSTRAINT fk_LeCompte REFERENCES Compte1(idCompte1)
			CONSTRAINT nn_LeCompte NOT NULL,
			leClient NUMBER(4)
			CONSTRAINT fk_LeClient REFERENCES Client1(idClient1)
			CONSTRAINT nn_LeClient NOT NULL
			);
            
DROP VIEW compteClient ;
DROP VIEW directeurAgence;

CREATE VIEW compteClient 
(compteRestant)
AS 
SELECT idCompte1
FROM Compte1
MINUS
SELECT unCompte
FROM Appartient1
;
CREATE VIEW directeurAgence 
(DirecteurReste)
AS 
SELECT idAgent
FROM Agent1
WHERE estDirecteur = 1
AND  1!= (SELECT COUNT(*) AS nbDirect
            FROM Agent1
            WHERE estDirecteur = 1
            GROUP BY lAgence
            )
;

CREATE OR REPLACE TRIGGER AugmentationSalaire
BEFORE UPDATE ON Agent1
FOR EACH ROW
BEGIN 
    IF (:NEW.salaire > 1.1*:OLD.salaire) AND (:NEW.salaire < 0.08*:OLD.salaire) THEN
        RAISE_APPLICATION_ERROR (-20001,'augmentation ou diminution trop importante');
    END IF;
END
;
/

CREATE OR REPLACE TRIGGER NomClientNomAgent
BEFORE INSERT OR UPDATE ON  Client1
FOR EACH ROW
DECLARE
    v_nomAgent Agent1.nomA%TYPE;
BEGIN 
    
    SELECT nomA INTO v_nomAgent 
    FROM Agent1
    WHERE idAgent = :NEW.lAgent
    ;
    IF (v_nomAgent = :NEW.lAgent) THEN
        RAISE_APPLICATION_ERROR (- 20000,'client et agent même nom');
    END IF;
END
;
/

CREATE OR REPLACE TRIGGER soldeRetrait
BEFORE INSERT OR UPDATE ON  Operation1
FOR EACH ROW
WHEN (NEW.typeOpe = 'RETRAIT')
DECLARE
    v_solde Compte1.solde%TYPE;
BEGIN
    SELECT solde INTO v_solde 
    FROM Compte1 
    WHERE idCompte1 = :NEW.leCompte
    ;
    IF (v_solde < :NEW.montant) THEN
        RAISE_APPLICATION_ERROR (-20000,'pas assez d argent dans le solde');
    END IF;
END
;
/


CREATE OR REPLACE TRIGGER AppartientRetrait
BEFORE INSERT OR UPDATE ON  Operation1
FOR EACH ROW
WHEN (NEW.typeOpe = 'RETRAIT')
DECLARE
    v_comptecli Appartient1.unClient%TYPE;
    v_client Client1.idClient%TYPE;
    
BEGIN
    SELECT unClient INTO v_comptecli 
    FROM Compte1 , Appartient
    WHERE idCompte1 = :NEW.leCompte
    AND unCompte = idCompte1 
    ;
    SELECT idClient FROM v_client
    FROM Client1
    WHERE idClient = :NEW.leClient
    ;
    IF (v_client != v_comptecli ) THEN
        RAISE_APPLICATION_ERROR (-20000,'l opperation n est pas sur le conte du client');
    END IF;
END
;
/