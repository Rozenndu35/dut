/* -------------------------------- Insertion table Editeur---------------------------------*/

INSERT INTO Editeur VALUES (1,'wattpad','Plouermel');
INSERT INTO Editeur VALUES (2,'flammarion','Paris');
INSERT INTO Editeur VALUES (3,'allia','guichen');
INSERT INTO Editeur VALUES (4,'aureas','Lanion');
INSERT INTO Editeur VALUES (5,'Benoit','restigne');
INSERT INTO Editeur VALUES (6,'Camion Blanc','Paris');
INSERT INTO Editeur VALUES (7,'caroline durand','Brest');
INSERT INTO Editeur VALUES (8,'hachette','Paris');
INSERT INTO Editeur VALUES (9,'clementine','havranche');
INSERT INTO Editeur VALUES (10,'damiani','Paris');
INSERT INTO Editeur VALUES (11,'eres','Paris');
INSERT INTO Editeur VALUES (12,'grego','cherbourg');
INSERT INTO Editeur VALUES (13,'gwinzegal','Paris');
INSERT INTO Editeur VALUES (14,'nimrod','Bagnolet');
INSERT INTO Editeur VALUES (15,'julliard','Paris');
INSERT INTO Editeur VALUES (16,'jules verne','Paris');
INSERT INTO Editeur VALUES (17,'klopp','st mere l eglise');
INSERT INTO Editeur VALUES (18,'louison','Paris');
INSERT INTO Editeur VALUES (19,'nathan','Lorient');
INSERT INTO Editeur VALUES (20,'romance','Nante');
INSERT INTO Editeur VALUES (21,'rozel','Rennes');
INSERT INTO Editeur VALUES (22,'theatrale','Vannes');
INSERT INTO Editeur VALUES (23,'victoria','Montreuil');

/* -------------------------------- Insertion table Auteur1---------------------------------*/

INSERT INTO Auteur1 VALUES (1,'Bautrais','Klervia','18/07/2000',1);
INSERT INTO Auteur1 VALUES (2,'Vadeleau','Alan','02/10/1997',23);
INSERT INTO Auteur1 VALUES (3,'Carrer','Aurelien', '20/01/2000',6);
INSERT INTO Auteur1 VALUES (4,'Brunet','Benoit','25/01/2000',8);
INSERT INTO Auteur1 VALUES (5,'Le St Quinio','Camille','13/02/2000',10);
INSERT INTO Auteur1 VALUES (6,'Carolunn','Pulsford', '09/12/2000',19);
INSERT INTO Auteur1 VALUES (7,'W','Clement','//',5);
INSERT INTO Auteur1 VALUES (8,'Leon','Damien','//',7);
INSERT INTO Auteur1 VALUES (9,'','Achild','07/07/2000',15);
INSERT INTO Auteur1 VALUES (10,'','','//',4);
INSERT INTO Auteur1 VALUES (11,'','','//',18);
INSERT INTO Auteur1 VALUES (12,'','','//', 16);
INSERT INTO Auteur1 VALUES (13,'','','//', 22);
INSERT INTO Auteur1 VALUES (14,'','', '//',20);
INSERT INTO Auteur1 VALUES (15,'','','//', 21);
INSERT INTO Auteur1 VALUES (16,'','','//', 8);
INSERT INTO Auteur1 VALUES (17,'','','//', 16);
INSERT INTO Auteur1 VALUES (18,'','','//',12);
INSERT INTO Auteur1 VALUES (19,'','','//', 11);
INSERT INTO Auteur1 VALUES (20,'','','//', 19);
INSERT INTO Auteur1 VALUES (21,'','','//', 13);
INSERT INTO Auteur1 VALUES (22,'','','//', 11);
INSERT INTO Auteur1 VALUES (23,'','','//', 15);