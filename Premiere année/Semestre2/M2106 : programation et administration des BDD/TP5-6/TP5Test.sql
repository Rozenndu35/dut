/* --------------------------------------Jeu de test :CREATE VIEW ecritPer1 et de aEcrit------------------------------ */
/* _____________________________________POUR OK___________________________________________ */

DELETE FROM Ecrit;
DELETE FROM Auteur1;
DELETE FROM Livre;
DELETE FROM Editeur;



INSERT INTO Livre VALUES(1,'Luna Elaheh', 9,'POCHE');
INSERT INTO Editeur VALUES (1,'Wattpad','9 rue St Thomas 56800');
INSERT INTO Auteur1 VALUES (1,'Bautrais','Klervia','18/07/2000',1);
INSERT INTO Ecrit VALUES (1,1);
INSERT INTO Livre VALUES(2,'Tintin', 5,'BD');
INSERT INTO Editeur VALUES (2,'Flammarion','Paris');
INSERT INTO Auteur1 VALUES (2,'Remi','Hergé', '22/5/1907',2);
INSERT INTO Ecrit VALUES (2,2);
SELECT livreRestant FROM  ecritPar1;
SELECT auteurRestant FROM  aEcrit;
/* ____________________________________POUR ERREUR _________________________________________*/

DELETE FROM Ecrit;
DELETE FROM Auteur1;
DELETE FROM Livre;
DELETE FROM Editeur;

INSERT INTO Livre VALUES(1,'Luna Elaheh', 9,'POCHE');
INSERT INTO Editeur VALUES (2,'Flammarion','Paris');
INSERT INTO Auteur1 VALUES (2,'Remi','Hergé', '22/5/1907',2);

SELECT livreRestant FROM  ecritPar1;
SELECT auteurRestant FROM  aEcrit;

/* --------------------------------------Jeu de test :CREATE VIEW ecritEditeur------------------------------ */

DELETE FROM Ecrit;
DELETE FROM Auteur1;
DELETE FROM Livre;
DELETE FROM Editeur;

INSERT INTO Livre VALUES(1,'Luna Elaheh', 9,'POCHE');
INSERT INTO Editeur VALUES (1,'Wattpad','9 rue St Thomas 56800');
INSERT INTO Auteur1 VALUES (1,'Bautrais','Klervia','18/07/2000',1);
INSERT INTO Ecrit VALUES (1,1);
INSERT INTO Livre VALUES(2,'Tintin', 5,'BD');
INSERT INTO Editeur VALUES (2,'Flammarion','Paris');
INSERT INTO Auteur1 VALUES (2,'Remi','Hergé', '22/5/1907',2);
INSERT INTO Ecrit VALUES (2,2);

SELECT editeur FROM  ecritEditeur;


/* --------------------------------------Jeu de test :CREATE VIEW PrixExemplaireArgentDePoche------------------------------ */
/* _____________________________________POUR OK___________________________________________ */

DELETE FROM Ecrit;
DELETE FROM Exemplaire1;
DELETE FROM Livre;
DELETE FROM Lecteur;
DELETE FROM Auteur1 ;
DELETE FROM Editeur ;



INSERT INTO Livre VALUES(1,'Luna Elaheh', 9,'POCHE');
INSERT INTO Livre VALUES(2,'Tintin', 5,'BD');
INSERT INTO Lecteur VALUES(1, 'Costiou', 'Rozenn' ,'25/08/2000', 50);
INSERT INTO Lecteur VALUES(2, 'Davail', 'Brieuc' ,'25/08/2005', 60);
INSERT INTO Exemplaire1 VALUES(1,1,1,'03/09/2018');
INSERT INTO Exemplaire1 VALUES(2,2,2,'29/03/2016');
/* ____________________________________POUR ERREUR _________________________________________*/
DELETE FROM Ecrit;
DELETE FROM Exemplaire1;
DELETE FROM Livre;
DELETE FROM Lecteur;
DELETE FROM Auteur1 ;
DELETE FROM Editeur ;

INSERT INTO Livre VALUES (1,'Luna Elaheh', 9,'POCHE');
INSERT INTO Lecteur VALUES(2, 'Davail', 'Brieuc' ,TO_DATE('25/08/2005','DD/MM/YYYY'), 10);
INSERT INTO Exemplaire1 VALUES(1,1,2, TO_DATE('25/06/2018','DD/MM/YYYY'));

/* --------------------------------------Jeu de test :CREATE VIEW AugmentationPrix------------------------------ */
/* _____________________________________POUR OK___________________________________________ */

DELETE FROM Ecrit;
DELETE FROM Exemplaire1;
DELETE FROM Livre;
DELETE FROM Lecteur;
DELETE FROM Auteur1 ;
DELETE FROM Editeur ;



INSERT INTO Livre VALUES(1,'Luna Elaheh', 10,'POCHE');
UPDATE Livre SET prix=11 WHERE noLivre =1; 

/* ____________________________________POUR ERREUR _________________________________________*/
DELETE FROM Ecrit;
DELETE FROM Exemplaire1;
DELETE FROM Livre;
DELETE FROM Lecteur;
DELETE FROM Auteur1 ;
DELETE FROM Editeur ;

INSERT INTO Livre VALUES(1,'Luna Elaheh', 9,'POCHE');
UPDATE Livre SET prix=10 WHERE noLivre =1;

/* --------------------------------------Jeu de test :CREATE VIEW PrixExemplaireArgentDePoche------------------------------ */
/* _____________________________________POUR OK___________________________________________ */

DELETE FROM Ecrit;
DELETE FROM Exemplaire1;
DELETE FROM Livre;
DELETE FROM Lecteur;
DELETE FROM Auteur1 ;
DELETE FROM Editeur ;



INSERT INTO Livre VALUES(1,'Luna Elaheh', 9,'POCHE');
INSERT INTO Editeur VALUES (1,'Wattpad','9 rue St Thomas 56800');
INSERT INTO Auteur1 VALUES (1,'Bautrais','Klervia','18/07/2000',1);
INSERT INTO Ecrit VALUES (1,1);
INSERT INTO Livre VALUES(2,'Tintin', 5,'BD');
INSERT INTO Editeur VALUES (2,'Flammarion','Paris');
INSERT INTO Auteur1 VALUES (2,'Remi','Hergé', '22/5/1907',2);
INSERT INTO Ecrit VALUES (2,2);

/* ____________________________________POUR ERREUR _________________________________________*/
DELETE FROM Ecrit;
DELETE FROM Exemplaire1;
DELETE FROM Livre;
DELETE FROM Lecteur;
DELETE FROM Auteur1 ;
DELETE FROM Editeur ;

INSERT INTO Livre VALUES(1,'Luna Elaheh', 9,'POCHE');
INSERT INTO Editeur VALUES (1,'Bautrais','9 rue St Thomas 56800');
INSERT INTO Auteur1 VALUES (1,'Bautrais','Klervia','18/07/2000',1);
