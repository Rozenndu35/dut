-- Agence([numAgence](1),telAgence,adresseAgence)
-- Agent([numAgent](1),nomAgent,prenomAgent,salaire, directeur,lAgence=@Agence.numAgence (NN))
-- Client([numClient](1),nomClient(NN),prenomClient(NN),adresseClient,dateNaissanceClient,lAgent=@Agent.numAgent(NN))
-- Appartient([unCompte=@Compte.numCompte,unClient=@Client.numClient](1))
-- Compte([numCompte](1),solde,typeCompte (NN))
-- Operation([numOperation](1),dateOperation,typeOperation,montant,leClient=@Client.numClient(NN), leCompte=@Compte.numCompte(NN))

-- ATTENTION :
-- utilisez DELETE avec l'ordre correct !!!!!
DELETE FROM Operation;
DELETE FROM Appartient;
DELETE FROM Compte;
DELETE FROM Client;
DELETE FROM Agence;
DELETE FROM Agent;

------------------------------------------------------------------------------------------------------------------------------------
--Insertion d'agences
------------------------------------------------------------------------------------------------------------------------------------
--
-- ATTENTION : le numero de tel EST UNE CHAINE
--
INSERT ALL
  INTO Agence VALUES (1, '06 00 00 00 01','2 rue info Vannes')
  INTO Agence VALUES (2, '06 00 00 00 02','5 boulevard des roses Vannes')
  INTO Agence VALUES (3, '06 00 00 00 03','7 squarre des chenes Vannes')
  INTO Agence VALUES (4, '06 00 00 00 04','15 rue victor Hugo Vannes')
  SELECT * FROM DUAL;

------------------------------------------------------------------------------------------------------------------------------------
--Insertion d'agents
------------------------------------------------------------------------------------------------------------------------------------
INSERT ALL
  -- un agent de l'agence 1
  INTO Agent VALUES (1,'Guillemot','Clement',1500,0,1)
  -- le directeur de l'agence 1
  INTO Agent VALUES (2,'Lacroix','Benoit',3000,1,1)
  -- trois agents de l'agence 2 dont un directeur
  INTO Agent VALUES (3,'Jeaunot','Louis',4000,0,2)
  INTO Agent VALUES (4,'Baron','Patrice',1750,0,2)
  INTO Agent VALUES (5,'Le Marechal','Adeline',4500,1,2)
  -- le directeur de l'agence 4
  INTO Agent VALUES (6,'Le Breton','Matthieu',2500,1,4)
  -- agents de l'agence 3 dont un directeur
  INTO Agent VALUES (7,'Marigot','Samuel',1600,0,3)
  INTO Agent VALUES (8,'Moliere','Nicolas',1600,0,3)
  INTO Agent VALUES (9,'Olivier','Aurore',1600,0,3)
  INTO Agent VALUES (10,'Pellerin','Louise',1600,0,3)
  INTO Agent VALUES (11,'Caro','Herve',1600,0,3)
  INTO Agent VALUES (12,'Boutin','Clementine',2605,1,3)
  SELECT * FROM DUAL;

------------------------------------------------------------------------------------------------------------------------------------
--Insertion de clients
------------------------------------------------------------------------------------------------------------------------------------
INSERT ALL
  -- 2 clients conseillés par l'agent 1
  INTO Client VALUES (1,'Villaume','Clement','1 rue Michel de Montaigne','01/01/1978',1)
  INTO Client VALUES (2,'Salun','prenomClient2','2 rue Michel de Montaigne','02/02/1990',1)
  -- un client conseillé par l'agent 2 (directeur)
  INTO Client VALUES (3,'Colin','prenomClient3','3 rue Michel de Montaigne','03/03/1978',2)
  -- 3 clients conseillés par l'agent 3
  INTO Client VALUES (4,'Le Gall','prenomClient4','4 rue Michel de Montaigne','04/04/1978',3)
  INTO Client VALUES (5,'Deschamp','prenomClient5','5 rue Michel de Montaigne','05/05/1978',3)
  INTO Client VALUES (6,'Cloarec','prenomClient6','6 rue Michel de Montaigne','06/06/1978',3)
  -- 3 clients conseillés par l'agent 6
  INTO Client VALUES (7,'Cadic','prenomClient7','7 rue Michel de Montaigne', SYSDATE-365*31,6)
  INTO Client VALUES (8,'Rocheteau','prenomClient8','8 rue Michel de Montaigne','08/08/1978',6)
  INTO Client VALUES (9,'Lenon','prenomClient9','9 rue Michel de Montaigne','09/09/1978',6)
  -- 4 clients conseillés par l'agent 12 (directeur)
  INTO Client VALUES (10,'Picard','prenomClient10','10 rue Michel de Montaigne','10/10/1978',12)
  INTO Client VALUES (11,'Le Roullec','CLEMENTINE','11 rue Michel de Montaigne','11/11/1978',12)
  INTO Client VALUES (12,'Le neveu','prenomClient12','12 rue Michel de Montaigne','12/12/1978',12)
  INTO Client VALUES (13,'Debost','prenomClient13','13 rue Michel de Montaigne','13/01/1978',12)
SELECT * FROM DUAL
;


------------------------------------------------------------------------------------------------------------------------------------
--Insertion dans Compte : Compte([numCompte](1),solde,numTypeCompte=@TypeCompte.numTypeCompte(NN))
------------------------------------------------------------------------------------------------------------------------------------
INSERT ALL
  INTO Compte VALUES (1,50,'COURANT')
  INTO Compte VALUES (2,13000,'EPARGNE')
  INTO Compte VALUES (3,1000,'COURANT')
  INTO Compte VALUES (4,500,'COURANT')
  INTO Compte VALUES (5,100000,'EPARGNE')
  INTO Compte VALUES (6,50,'EPARGNE')
  INTO Compte VALUES (7,5000,'COURANT')
  INTO Compte VALUES (8,5005,'COURANT')
  INTO Compte VALUES (9,500,'COURANT')
  INTO Compte VALUES (10,14000,'EPARGNE')
  INTO Compte VALUES (11,50,'COURANT')
  INTO Compte VALUES (12,8500,'EPARGNE')
  INTO Compte VALUES (13,18500,'EPARGNE')
  INTO Compte VALUES (14,2548,'COURANT')
  INTO Compte VALUES (15,85000,'EPARGNE')
  SELECT * FROM DUAL;


------------------------------------------------------------------------------------------------------------------------------------
--Insertion dans Appartient : Appartient([numCompte=@Compte.numCompte,numClient=@Client.numClient](1))
------------------------------------------------------------------------------------------------------------------------------------
INSERT ALL
  INTO Appartient VALUES (1,1)
  INTO Appartient VALUES (1,2)
  INTO Appartient VALUES (2,3)
  INTO Appartient VALUES (3,4)
  INTO Appartient VALUES (4,5)
  INTO Appartient VALUES (5,6)
  INTO Appartient VALUES (6,7)
  INTO Appartient VALUES (7,8)
  INTO Appartient VALUES (8,9)
  INTO Appartient VALUES (9,10)
  INTO Appartient VALUES (10,11)
  INTO Appartient VALUES (11,12)
  INTO Appartient VALUES (12,13)
  INTO Appartient VALUES (13,1)
  INTO Appartient VALUES (14,2)
--  INTO Appartient VALUES (15,3)
  SELECT * FROM DUAL;


--------------------------------------------------------------------------------------------------------------------------------------------------------------
--Insertion dans Operation : Operation([numOperation](1),dateOperation,typeOperation,montant,client=@Client.numClient(NN), numCompte=@Compte.numCompte(NN))
--------------------------------------------------------------------------------------------------------------------------------------------------------------
INSERT ALL
  INTO Operation(numOperation,typeOperation,montant,leClient,leCompte) VALUES (1,'RETRAIT',10,10,9) -- la date sera celle proposée par defaut
  INTO Operation(numOperation,typeOperation,montant,leClient,leCompte) VALUES (2,'RETRAIT',500,8,7) -- la date sera celle proposée par defaut
  INTO Operation(numOperation,typeOperation,montant,leClient,leCompte) VALUES (3,'DEPOT',5005,9,8) -- la date sera celle proposée par defaut
  INTO Operation(numOperation,typeOperation,montant,leClient,leCompte) VALUES (4,'DEPOT',200,9,8) -- la date sera celle proposée par defaut
  INTO Operation VALUES (5,SYSDATE-1,'RETRAIT',100,9,8)
  INTO Operation VALUES (6,SYSDATE-1,'RETRAIT',400,9,8)
  INTO Operation(numOperation,typeOperation,montant,leClient,leCompte) VALUES (7,'DEPOT',10000,9,8) -- la date sera celle proposée par defaut
  INTO Operation(numOperation,typeOperation,montant,leClient,leCompte) VALUES (8,'RETRAIT',100,9,8) -- la date sera celle proposée par defaut
  INTO Operation(numOperation,typeOperation,montant,leClient,leCompte) VALUES (9,'DEPOT',400,9,8) -- la date sera celle proposée par defaut
  INTO Operation(numOperation,typeOperation,montant,leClient,leCompte) VALUES (10,'DEPOT',500,8,7) -- la date sera celle proposée par defaut
  INTO Operation VALUES (11,SYSDATE-4,'RETRAIT',600,8,7)
  INTO Operation VALUES (12,SYSDATE-10,'RETRAIT',700,8,7)
  INTO Operation(numOperation,typeOperation,montant,leClient,leCompte) VALUES (13,'DEPOT',750,8,7) -- la date sera celle proposée par defaut
  SELECT * FROM DUAL
;

