/*
25/01/19 15h30
TP2_COSTIOU_rozenn.sql
Costiou Rozenn groupe 1B1
*/



DROP TABLE Reservation;
DROP TABLE Emplacement ;
DROP TABLE Bateau;
DROP TABLE Proprietaire;


CREATE TABLE Proprietaire(
                lIdentifiant  NUMBER(4)
                CONSTRAINT pk_lIdentifiant PRIMARY KEY,
                nom VARCHAR2(20),
                prenom VARCHAR2(20),
                lAdresse VARCHAR2(50)
                CONSTRAINT nn_lAdresse NOT NULL
                )
;

                
CREATE TABLE Emplacement(
                numeroE NUMBER(4)
                CONSTRAINT pk_Emplacement_numeroE PRIMARY KEY,
                longueurE  NUMBER(2)
                CONSTRAINT nn_Emplacement_longueurE NOT NULL
                CONSTRAINT ck_Emplacement_longueurE CHECK (( longueurE>=3) AND ( longueurE<=20)) 
                 );

CREATE TABLE Bateau(
                noImm  NUMBER(4)
                CONSTRAINT pk_noImm PRIMARY KEY,
                nom VARCHAR2(20),
                longueur  NUMBER(2)
                CONSTRAINT ck_longueurb CHECK (( longueur>=3) AND ( longueur<=20))
                CONSTRAINT nn_longueurb NOT NULL,
                largeur  NUMBER(2)
                CONSTRAINT ck_largeur CHECK ( largeur<=6),
                leProp NUMBER(4)
                CONSTRAINT fk_Bateau_leProp REFERENCES Proprietaire(lIdentifiant ) NOT NULL,
                lEmplacements NUMBER(4)
                CONSTRAINT fk_Bateau_leEmplacement REFERENCES Emplacement(numeroE ),
                dateDebuts DATE,
                dateFins DATE
                );
                 
CREATE TABLE Reservation(
                numero NUMBER(4)
                CONSTRAINT pk_Reservation_numero PRIMARY KEY,
                dateDebutR DATE
                CONSTRAINT nn_Reservation_dateDebutR NOT NULL,
                dateFinR DATE
                CONSTRAINT nn_Reservation_dateFinR NOT NULL,
                CONSTRAINT ck_Reservation_dateFinR CHECK ( dateDebutR <= dateFinR),
                leBateau NUMBER(4)
                CONSTRAINT fk_Reservation_leBateau REFERENCES Bateau(noImm ) NOT NULL,
                lEmplacementR NUMBER(4)
                CONSTRAINT fk_Reservation_lEmplacementR REFERENCES Emplacement(numeroE) NOT NULL
                 );
                 
INSERT INTO Proprietaire VALUES(1,'COSTIOU','Rozenn','Acigne');
INSERT INTO Proprietaire VALUES(2,'DAVAIL','MALO','Paris');
INSERT INTO Proprietaire VALUES(3,'COIC','Patrice','Vannes');
INSERT INTO Proprietaire VALUES(4,'TRASSAR','Nicolas','Toulouse');


INSERT INTO Emplacement VALUES(1,6);
INSERT INTO Emplacement VALUES(2,10);
INSERT INTO Emplacement VALUES(3,15);
INSERT INTO Emplacement VALUES(4,9);


INSERT INTO Bateau VALUES(0001,'Loup des mers', 10 , 5 , 1, 2, '10/05/2018', '25/05/2018');
INSERT INTO Bateau VALUES(0002,'Loiseau', 15 , 6 , 4, 3, '10/08/2018', '25/12/2018');
INSERT INTO Bateau VALUES(0003,'Titanic', 9 , 3 , 2, 4, '9/05/2018', '25/08/2018');
INSERT INTO Bateau VALUES(0004,'Renard', 5 , 3 , 3, 1, '15/05/2018', '25/01/2019');

INSERT INTO Reservation VALUES(0010,'10/05/2018', '25/05/2018', 0003, 4);
INSERT INTO Reservation VALUES(0020,'10/08/2018', '25/12/2018', 0002, 3);
INSERT INTO Reservation VALUES(0030,'10/05/2018', '25/11/2018', 0004, 1);
INSERT INTO Reservation VALUES(0040,'9/05/2018', '25/08/2018', 0001, 2);

DELETE FROM Reservation;
DELETE FROM Bateau;
DELETE FROM Proprietaire;
DELETE FROM Emplacement;


