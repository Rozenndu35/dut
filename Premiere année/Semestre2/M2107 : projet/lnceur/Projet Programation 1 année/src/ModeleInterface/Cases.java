/**
 * classe pour les cases du tableau
 * @author rozenn/Klervia
 * @version1
 */
package ModeleInterface;

import java.io.Serializable;

public class Cases implements Serializable {

	private Couleur maCouleur;

	/**
	 * retourne sa couleur
	 * @return la couleur
	 */
	public Couleur getCouleur() {
		return this.maCouleur;
	}

	/**
	 * change sa couleur
	 * @param newCouleur : la nouvelle couleur
	 */
	public void setCouleur(Couleur newCouleur) {
		this.maCouleur = newCouleur;
	}	
}