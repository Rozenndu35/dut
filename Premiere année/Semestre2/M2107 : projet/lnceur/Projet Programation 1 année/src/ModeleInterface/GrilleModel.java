/**
 * La classe contiendra un tableau a deux dimensions d'entier
 * @author rozenn
 * @version 1
 */

package ModeleInterface;

import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;

import Vue.Accueil;
import Vue.Fenetre;

public class GrilleModel extends AbstractTableModel {
	private Fenetre fenetre;
	Couleur[][] grilleDuJeu;
	/**
	 * le constructeur
	 * @param f : la fenetre
	 * @param cases : le tableau de case
	 */
    public GrilleModel(Fenetre f, Cases[][] cases) {
    	grilleDuJeu = new Couleur[19][19];
    	for(int i=0;i<cases.length;i++) {
    		for(int j=0; j<cases.length;j++) {
    			grilleDuJeu[i][j]=cases[i][j].getCouleur();
    		}
    	}
    }
    
    

    /**
     * modifie la grille
     * @param plat : le nouveau plateau
     */
   public  void setgrille(Cases[][] plat) {
	   this.grilleDuJeu = new Couleur[19][19];
	   for (int i = 0 ; i<19 ; i++) {
		   for ( int j = 0 ; j<19 ; j++) {
			   grilleDuJeu[i][j] = plat[i][j].getCouleur();
		   }
	   }
	   this.fireTableDataChanged();
   }
   
   /**
    * Retourne le nombre de colomne
    * @return le numero
    */
	public int getColumnCount() {
		return this.grilleDuJeu.length;
	}


	/**
	 * retourne le numero de la ligne
	 * @return le numero
	 */
	public int getRowCount() {
		return this.grilleDuJeu[0].length;
	}




	/**
	 * Retourne le contenu de notre tableau a x et y 
	 * @return l'objet
	 */
	public Object getValueAt(int x, int y) {
		Object result =  new Object();
		Couleur coul = this.grilleDuJeu[x][y];
	    
	    if (coul==Couleur.LIBREB) { result= new ImageIcon("donnee/BarriereVide.png");
	    }else if (coul == Couleur.LIBREP){result= new ImageIcon("donnee/CaseVide.png");
	    }else if (coul == Couleur.BLEU) {result = new ImageIcon("donnee/PionBleu.png");
	    }else if (coul == Couleur.NOIR) {result = new ImageIcon("donnee/PionNoir.png");
	    }else if (coul == Couleur.ROUGE) {result = new ImageIcon("donnee/PionRouge.png");
	    }else if (coul == Couleur.VERT) {result = new ImageIcon("donnee/PionVert.png");
	    }else if (coul == Couleur.MUR) {result = new ImageIcon("donnee/Barriere.png");
	    }else if (coul == Couleur.BOMBE) {result = new ImageIcon("donnee/Bombe.png");
	    }else if (coul == Couleur.ETOILE) {result = new ImageIcon("donnee/Etoile.png");
	    }else if (coul == Couleur.ARRIVER) {result = new ImageIcon("donnee/Arrivée.png");
	    }else if (coul == Couleur.MAISON) {result = new ImageIcon("donnee/DiagHG.jpg");
	    }else if (coul == Couleur.FAKEMUR) {result = new ImageIcon("donnee/fakebarriere.png");
	    }else if (coul == Couleur.FAKEPION) {result = new ImageIcon("donnee/fakepion.png");
	    }
	    
	    return result;
	    
	    
		
	}
	
	/**
	 * modifie la valeur de la case
	 * @param result le nouveau objet
	 * @param x la coordonee
	 * @param y la coordonee
	 */
	public void setValueAt(Object result, int x, int y) {
		this.grilleDuJeu[x][y]=(Couleur) result;
		this.fireTableDataChanged();
	}
	  /**
	   * renvoie le nom de la colonne
	   * @param c : le nombre de la colone
	   * @return retourne le nom
	   */
	  public String getColumnName(int c){
	    return (new Integer(c).toString());
	  }
	  
	   /**
	   * renvoie le nom de la colone
	   * @param c : le nombre de la colone
	   * @return retourne l'objet
	   */
	   public Class getColumnClass(int c) {
	      return this.getValueAt(0, c).getClass();
	   }

}
