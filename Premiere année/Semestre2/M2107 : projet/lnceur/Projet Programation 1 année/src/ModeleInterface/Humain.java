/**
 * classe pour les actions du joueur humain
 * @author rozenn/Klervia
 * @version1
 */
package ModeleInterface;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Humain extends Joueur {
	private boolean gangnerLab;
	private int etoile;

	/**
	 * constructeur de la classe humain
	 * @param nom
	 * @param numero
	 * @param nbJouer
	 */
	public Humain(String nom, int numero, int nbJouer, Plateau plat) {
		super(nom, numero, nbJouer,plat);
		gangnerLab = false;
		etoile= 0;
	}
//__________________________________________________________________________________________________________________________ 
	
	/**
	 * methode qui propose a l'utilisateur de choisir sont deplacement dans le labyrinthe
	 * @return : return false si il est mort par une bombe
	 */
	public boolean choixLab(int x , int y) {
		boolean marche = false;	
		int nX = x;
		int nY = y;
		if (super.lePlateau.getCases(nX,nY).getCouleur() != Couleur.BOMBE) {
			super.lePlateau.verifPionLab(super.monPion.getX(),super.monPion.getY());
			HashMap xpossible = super.lePlateau.getlistX();
			HashMap ypossible = super.lePlateau.getlistY();
			for (int i = 0; i< xpossible.size() && !marche; i++) {
				if (nX == (int)xpossible.get(i) && nY == (int)ypossible.get(i) ) {
					marche= true;
					if (this.lePlateau.getCases(nX,nY).getCouleur() == Couleur.ETOILE) {this.etoile ++;}
					if (this.lePlateau.getCases(nX,nY).getCouleur() == Couleur.ARRIVER) {marche = true; this.gangnerLab = true;}
					super.lePlateau.changeCouleurDeplacerPion(super.monPion.getX(), super.monPion.getY(), nX , nY);
					super.monPion.setX(nX);
					super.monPion.setY(nY);
				}
			}
		}
		return marche;
	}
	//__________________________________________________________________________________________________________________________	
	/**
	 * methode qui permet au joueur de deplacer son pion
	 */
	public boolean deplacePion(int x , int y) {
		boolean marche = false;
		int nX = x ;
		int nY =y;
		super.lePlateau.verifPion(super.monPion.getX(),super.monPion.getY());
		HashMap xpossible = super.lePlateau.getlistX();
		HashMap ypossible = super.lePlateau.getlistY();
		for (int i = 0; i< xpossible.size() && !marche; i++) {
			if (nX == (int)xpossible.get(i) && nY == (int)ypossible.get(i) ) {
				marche= true;
				super.lePlateau.changeCouleurDeplacerPion(super.monPion.getX(), super.monPion.getY(), nX , nY);
				super.monPion.setX(nX);
				super.monPion.setY(nY);
			}
		}
		return marche;
	}
	/**
	 * methode qui permet au joueur de placer une barriere
	 */
	public boolean placerBarriere(int x , int y , boolean sens) {
		boolean ret = false;
		if (super.lePlateau.verifBarriere(x,y,sens , this)) {
			ret = true; 
			super.lePlateau.ajouteCouleurBarriere(x,y,sens);
		}
		return ret;
	}
	//__________________________________________________________________________________________________________________________
	/**
	 * retourne le nombre d'etoile recuperer
	 * @return :le nombre d'etoile
	 */
	public int getEtoile() {
		return this.etoile;
	}
	/**
	 * retourne vrai si il a gagner au labyrinthe
	 * @return : si il a gagner ou non
	 */
	public boolean getgagnerLab() {
		return this.gangnerLab;
	}

}