import javax.swing.text.html.Option;

import ModeleInterface.Partie;
import Vue.Fenetre;
import Vue.Options;


public class LanceurInterface {
	/**
	 * methode de lancement de la partie
	 * @param args : les arguments
	 */
	public static void main(String[] args) {
		java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            	new Fenetre();
            	
            	
                }
        });
	}

} 