/**
 * test pour afficher le tableau
 * @author rozenn
 */
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;

import ModeleConsole.Couleur;
import ModeleConsole.Mode;
import ModeleConsole.Partie;

public class test {
	public static void main(String[] arg) {
		Partie part = new Partie(Mode.HHHH);
		part.afficheConsole();
		part.plateau.ajouteCouleurBarriere(5,6, true);
		part.afficheConsole();
	}
	
	boolean marche = false;
	Scanner sc = new Scanner (System.in);
	System.out.println("Saisir x");
	int nX ;
	try {
		nX = sc.nextInt();
	}
	catch(InputMismatchException e) {
		System.out.println("Resaisir x (un entier)");
		nX = sc.nextInt();
	}

	System.out.println("Saisir y");
	int nY ;
	try {
		nY = sc.nextInt();
	}
	catch(InputMismatchException e) {
		System.out.println("Resaisir y (un entier)");
		nY = sc.nextInt();
	}
	super.lePlateau.verifPionLab(super.monPion.getX(),super.monPion.getY());
	HashMap xpossible = super.lePlateau.getlistX();
	HashMap ypossible = super.lePlateau.getlistY();
	for (int i = 0; i< xpossible.size() && !marche; i++) {
		if (nX == (int)xpossible.get(i) && nY == (int)ypossible.get(i) ) {
			marche= true;
				
			}
		}
		while (!marche ) {
			System.out.println("Saisie incorrecte");
			System.out.println("Resaisir x");
			try {
				nX = sc.nextInt();
			}
			catch(InputMismatchException e) {
				System.out.println("Resaisir x (un entier)");
				nX = sc.nextInt();
			}

			System.out.println("Resaisir y");
			try {
				nY = sc.nextInt();
			}
			catch(InputMismatchException e) {
				System.out.println("Resaisir y (un entier)");
				nY = sc.nextInt();
			}
			super.lePlateau.verifPionLab(super.monPion.getX(),super.monPion.getY());
			xpossible = super.lePlateau.getlistX();
			ypossible = super.lePlateau.getlistY();
			for (int i = 0; i< xpossible.size() && !marche; i++) {
				if (nX == (int)xpossible.get(i) && nY == (int)ypossible.get(i) ) {
					marche= true;
				}
			}
		}
		super.monPion.setX(nX);
		super.monPion.setY(nY);
	}
}
