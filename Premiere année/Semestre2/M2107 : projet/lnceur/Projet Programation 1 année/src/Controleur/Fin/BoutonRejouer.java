/**
 * controleur pour les bouton rejouer une partie
 * @author rozenn/Klervia
 * @version1
 */

package Controleur.Fin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Vue.*;

public class BoutonRejouer  implements ActionListener{

	private Fenetre laFenetre;
	
	/**
	 * le constructeur
	 * @param f : la fenetre 
	 */
	public BoutonRejouer(Fenetre f) {
		this.laFenetre=f;
	}
	/**
	 * cree les action du bouton
	 * @param e : l'evenement
	 */
	public void actionPerformed(ActionEvent e) {
		this.laFenetre.setPanel(new Accueil(laFenetre));
	}

}