package Controleur.Option;

/**
 * le controleur pour le bouton option de la fenetre
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Vue.Accueil;
import Vue.Fenetre;
import Vue.Options;

public class OptionButtonListener implements ActionListener {
	private Fenetre f;
	
	/**
	 * le constructeur
	 * @param f : la fenetre
	 */
	public OptionButtonListener(Fenetre f) {
		this.f = f;
	}

	/**
	 *  cree les action du bouton
	 * @param e : l'evenement
	 */
	public void actionPerformed(ActionEvent arg0) {
		
		Options op = new Options(this.f);
	}

}
