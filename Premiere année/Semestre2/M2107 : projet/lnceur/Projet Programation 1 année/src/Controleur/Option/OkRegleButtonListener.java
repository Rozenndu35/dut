package Controleur.Option;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import Vue.Accueil;
import Vue.Debut;
import Vue.Fenetre;
import Vue.Regles;

/**
 * le controleur pour le bouton option de la fenetre
 */
public class OkRegleButtonListener implements ActionListener {

	private Regles fenetre;
	/**
	 * le constructeur
	 * @param f : la fenetre
	 */
	public OkRegleButtonListener(Regles f) {
		this.fenetre=f;
	}
	
	/**
	 *  cree les action du bouton
	 * @param e : l'evenement
	 */
	public void actionPerformed(ActionEvent e) {
		fenetre.dispose();
	}

}
