package Controleur.Option;
/**
 * le controleur pouvoir jouer depuis l'acceuil
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import Vue.Debut;
import Vue.Fenetre;

public class JouerButtonListener  implements ActionListener{
	private Fenetre fenetre;

	/**
	 * le constructeur
	 * @param d : la fenetre de debut
	 */
	public JouerButtonListener(Fenetre f ) {
		this.fenetre = f;
	}
	/**
	 *  cree les action du bouton
	 * @param e : l'evenement
	 */
	public void actionPerformed(ActionEvent e) {
		this.fenetre.setPanel(new Debut(fenetre));
	}
}
