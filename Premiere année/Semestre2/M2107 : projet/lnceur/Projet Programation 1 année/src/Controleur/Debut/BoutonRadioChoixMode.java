/**
 * controleur pour les bouton choisir le mode
 * @author rozenn/Klervia
 * @version1
 */

package Controleur.Debut;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JRadioButton;
import Vue.*;

public class BoutonRadioChoixMode implements ActionListener{
	private Debut debut;

	/**
	 * le constructeur
	 * @param d : la fenetre de debut
	 */
	public BoutonRadioChoixMode(Debut d ) {
		this.debut = d;
	}

	/**
	 *  cree les action du bouton
	 * @param e : l'evenement
	 */
	public void actionPerformed(ActionEvent e) {
		if (this.debut.gethJoueur2().isSelected() ){
				this.debut.setPanelJoueur2(true);
		}
		else if(this.debut.getoJoueur2().isSelected()) {
			this.debut.setPanelJoueur2(false);
		}
		if (this.debut.gethJoueur3().isSelected() ){
			this.debut.setPanelJoueur3("humain");
			this.debut.gethJoueur4().setEnabled(true);
			this.debut.getoJoueur4().setEnabled(true);
			if(this.debut.getnJoueur4().isSelected()) {
				this.debut.getnJoueur4().setEnabled(false);
				this.debut.getoJoueur4().setSelected(true);
			}
		}
		else if(this.debut.getoJoueur3().isSelected() ) {
			this.debut.setPanelJoueur3("ordinateur");
			this.debut.gethJoueur4().setEnabled(true);
			this.debut.getoJoueur4().setEnabled(true);
			if(this.debut.getnJoueur4().isSelected()) {
				this.debut.getnJoueur4().setEnabled(false);
				this.debut.getoJoueur4().setSelected(true);
			}
		}
		else if(this.debut.getnJoueur3().isSelected()) {
			this.debut.setPanelJoueur3("aucun");
			this.debut.getnJoueur4().setSelected(true);
			this.debut.gethJoueur4().setEnabled(false);
			this.debut.getoJoueur4().setEnabled(false);
			this.debut.getnJoueur4().setEnabled(false);
			this.debut.getnJoueur4().setSelected(true);
		}
		if (this.debut.gethJoueur4().isSelected() ){
			this.debut.setPanelJoueur4(true);
		}
		else if(this.debut.getoJoueur4().isSelected()) {
			this.debut.setPanelJoueur4(false);
		}
		if (this.debut.getnJoueur4().isSelected()) {
			this.debut.getj4p().removeAll();
			this.debut.getj4p().updateUI();
		}
	}
}