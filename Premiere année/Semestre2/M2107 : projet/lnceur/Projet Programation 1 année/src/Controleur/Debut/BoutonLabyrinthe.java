package Controleur.Debut;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import ModeleInterface.Mode;
import ModeleInterface.Partie;
import Vue.*;

public class BoutonLabyrinthe implements ActionListener {

	private Fenetre fenetre;
	private Pedagogique peda;
	/**
	 * le constructeur
	 * @param d : la fenetre de debut
	 */
	public BoutonLabyrinthe(Fenetre f, Pedagogique p) {
		this.fenetre = f;
		this.peda = p;
	}

	/**
	 * cree les action du bouton
	 * @param e : l'evenement
	 */
	public void actionPerformed(ActionEvent e) {
		Mode mode =(Mode) this.peda.getMode().getSelectedItem();
		int niveau = (int)this.peda.getNiveau().getSelectedItem();
		String nom = this.peda.getNom(); 
		this.fenetre.setPanel(new  JeuxLab(mode , niveau, nom,this.fenetre));
		//Partie part = new Partie(mode, niveau, nom);
	}
}