/**
 * controleur pour le bouton valider les coix
 * @author rozenn/Klervia
 * @version1
 */
package Controleur.Debut;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ModeleInterface.Difficulte;
import ModeleInterface.Mode;
import ModeleInterface.Partie;
import Vue.Accueil;
import Vue.Debut;
import Vue.Fenetre;
import Vue.Jeux;



public class BoutonValider  implements ActionListener{
	private Fenetre fenetre;
	private Debut d;

	/**
	 * le constructeur
	 * @param d : la fenetre de debut
	 */
	public BoutonValider(Fenetre f , Debut debut) {
		this.fenetre = f;
		this.d = debut;
	}
	/**
	 *  cree les action du bouton
	 * @param e : l'evenement
	 */

	public void actionPerformed(ActionEvent arg0) {
		Mode mode = this.d.getMode();
		String nom1 = this.d.getNom1();
		String nom2 = null;
		String nom3 = null;
		String nom4 = null;
		Difficulte dif2 = null;
		Difficulte dif3 = null;
		Difficulte dif4 = null;
		if (mode == Mode.HO) {
			dif2 = this.d. getDif2();
		}
		else if (mode == Mode.HH) {
			nom2 = this.d.getNom2();
		}
		else if (mode == Mode.HOOO) {
			dif2 = this.d. getDif2();
			dif3 = this.d. getDif3();
			dif4 = this.d. getDif4();
		}
		else if (mode == Mode.HHOO) {
			nom2 = this.d.getNom2();
			dif3 = this.d. getDif3();
			dif4 = this.d. getDif4();
		}
		else if (mode == Mode.HHHO) {
			nom2 = this.d.getNom2();
			nom3 = this.d.getNom3();
			dif4 = this.d. getDif4();
		}
		else if (mode == Mode.HHHH) {
			nom2 = this.d.getNom2();
			nom3 = this.d.getNom3();
			nom4 = this.d.getNom4();
		}
		else if (mode == Mode.HHOO) {
			nom2 = this.d.getNom2();
			dif3 = this.d. getDif3();
			dif4 = this.d. getDif4();
		}
		else if (mode == Mode.HOOH) {
			dif2 = this.d. getDif2();
			dif3 = this.d. getDif3();
		}
		else if (mode == Mode.HOHH) {
			dif2 = this.d. getDif2();
			nom3 = this.d.getNom3();
			nom4 = this.d.getNom4();
		}
		else if (mode == Mode.HHOH) {
			nom2 = this.d.getNom2();
			dif3 = this.d. getDif3();
			nom4 = this.d.getNom4();
		}
		
		this.fenetre.setPanel(new Jeux(mode ,nom1 ,nom2,nom3,nom4,dif2 ,dif3 , dif4 ,this.fenetre));
	}
}