package Vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import Controleur.Option.AccueilButtonListener;
import Controleur.Option.OptionButtonListener;
import Controleur.Option.RegleButtonListener;
import ModeleInterface.Audio;

public class Fenetre extends JFrame{
	private JPanel milieu;
	private JButton accueil;
	private JButton regle;
	private JButton option;
	private Audio son;
	
	public Fenetre(){
		
		//musique
		son = new Audio();
		son.start();
		
		
		this.milieu= new Accueil(this);
		this.accueil = new JButton ("Accueil");
		this.regle = new JButton ("Règles");
		this.option = new JButton ("Options");
		
		Color c = new Color(255,244,235); //beige couleur bois
		accueil.setBackground(c);
		regle.setBackground(c);
		option.setBackground(c);
		
        Color col = new Color (139,69,19);
		accueil.setBorder(null);
		regle.setBorder(null);
		option.setBorder(null);
		
		//disposition
		JPanel haut = new JPanel();
		haut.setLayout(new GridLayout(0,3));
		haut.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(col,2), BorderFactory.createLineBorder(c,12)));
		haut.add(accueil);
		haut.add(regle);
		haut.add(option);
		
		this.setLayout(new BorderLayout());
		this.add(haut , BorderLayout.NORTH);
		this.add(milieu , BorderLayout.CENTER);
		this.pack();
		
		//affichage de la fenetre
		this.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setSize(1200,720);
		this.setTitle("Quoridor");
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		
		
		// les actions
		this.regle.addActionListener(new  RegleButtonListener());
		this.accueil.addActionListener(new  AccueilButtonListener(this));
		this.option.addActionListener(new  OptionButtonListener(this));
		
		this.setResizable(false);
		
	}
	
	public void setPanel(JPanel nPanel) {
		
		this.milieu.removeAll();
		this.milieu.validate();//valider le suppression
       
        this.milieu.add(nPanel);
 
        this.milieu.revalidate();
        this.milieu.repaint();
		
		//this.milieu.removeAll();
		//this.milieu.add(nPanel);
		//this.milieu.repaint();;
	}
	public JPanel getPanel() {
		return this.milieu;
	}
	
	public Audio getSon() {
		return this.son;
	}
	
	
	public void setSon(Audio son) {
		this.son.interrupt();
	}
		
		
}
