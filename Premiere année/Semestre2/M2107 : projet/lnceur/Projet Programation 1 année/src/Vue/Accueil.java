package Vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Controleur.Option.JouerButtonListener;
import Controleur.Option.PedaButtonListener; 

public class Accueil extends JPanel{
	private JPanel milieu;
	private BufferedImage buffImage;
	private Image image;
	private BufferedImage buffImage2;
	private Image image2;
	private JButton jouer;
	private JButton charger;
	private JButton peda;
	private JLabel labelImage;
	private JLabel labelImage2;
	private Fenetre laF;
	
	public Accueil(Fenetre f) {
		this.laF = f;
		//declaration des composants
		jouer = new JButton("Jouer");
		charger = new JButton("Charger");
		peda = new JButton("Pédagogique");
		milieu = new JPanel();
		
		//initialisation des composants
		Color col = new Color (139,69,19); //marron
		buffImage = Accueil.LectureImage("./donnee/Fond.jpg");
		image = Toolkit.getDefaultToolkit().createImage(buffImage.getSource());
		image = image.getScaledInstance(1200, 720, Image.SCALE_DEFAULT);
		labelImage = new JLabel(new ImageIcon(image));		
        labelImage.setBounds( 0, 0, 1200, 720);
        buffImage2 = Accueil.LectureImage("./donnee/gigamic_glqo_quoridor-deluxe_facing.png");
		image2 = Toolkit.getDefaultToolkit().createImage(buffImage2.getSource());
		image2 = image2.getScaledInstance(150, 150, Image.SCALE_DEFAULT);
		labelImage2 = new JLabel(new ImageIcon(image2));		
        labelImage2.setBounds( 525, 60, 150, 150);
        labelImage2.setBorder(BorderFactory.createLineBorder(col,2));
        jouer.setBounds(475, 250, 250, 80);
        charger.setBounds(475, 350, 250, 80);
        peda.setBounds(475, 450, 250, 80);
        Color c = new Color(255,244,235); //beige couleur bois      
        jouer.setBackground(c);
        charger.setBackground(c);
        peda.setBackground(c);
        jouer.setBorder(BorderFactory.createLineBorder(col,2));
        charger.setBorder(BorderFactory.createLineBorder(col,2));
        peda.setBorder(BorderFactory.createLineBorder(col,2));
		//disposition panel milieu
		milieu.setLayout(null);
		milieu.add(jouer);	
		milieu.add(charger);
		milieu.add(peda);
		milieu.add(labelImage2);
		milieu.add(labelImage);
		
		//disposition sur la fenetre
		this.setLayout(new BorderLayout());
		this.add(milieu);
		
		//Action
		this.jouer.addActionListener(new JouerButtonListener(this.laF));
		this.peda.addActionListener(new PedaButtonListener(this.laF));
		
	}
	
	public static BufferedImage LectureImage(String Filename) // fonction de lecture d'une image dont le nom est Filename. Cette methode retourne un objet de type Image
	{     
		BufferedImage buffImage;
		buffImage = null;
		try 
        {
			File file   = new File(Filename);  // Creation d'un objet File afin de lire le fichier dont le nom est Filename
			buffImage   = ImageIO.read(file);  // Le fichier file est interprete comme etant une image que l'on nomme image
        } 
       	catch (IOException e) 
		{		}
		return(buffImage);	
	}
	public void setPanel(JPanel nPanel) {
		this.milieu= nPanel;
		this.milieu.updateUI();
	}

}
