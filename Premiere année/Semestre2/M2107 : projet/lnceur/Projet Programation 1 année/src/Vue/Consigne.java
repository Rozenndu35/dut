package Vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.text.JTextComponent;

import Controleur.Debut.BoutonConsigne;
import Controleur.Option.OkRegleButtonListener;

public class Consigne extends JFrame{

	private JButton ok;
	private BufferedImage buffImage;
	private Image image;
	private JLabel labelImage;
	private JPanel p;
	private JPanel panelOp;
	private JTextComponent texte1;
	
	/**
	 * le constructeur
	 */
	public Consigne() {
		//initialisation des composants
		
		texte1 = new JTextArea("Créer une partie : "
				+ 
				"\n" 
				+ "Saisir votre nom"
				+
				"\n"
				+ 
				"   -> choisir votre mode"
				+ 
				"\n" 
				+ 
				"   -> choisir votre niveau"
				+ 
				"\n"
				+
				" Lancer la partie avec le bouton labyrinthe."
				+ 
				"\n"
				+ 
				"\n" 
				+ 
				"Choisir votre direction avec le pad "+
				"\n"
				+ 
				"\n" 
				+ 
				"pour les labyrinthe "+ 
				"\n"
				+" attention si vous touvher une bombe vous avez perdu\n" + 
				"\n" + 
				"vous ne pouvez pas traverser les mur"+ 
				"\n"
				+
				"les etoiles sont a ramasser lorsu'il y en a si vous voulez etre dans les record"+ 
				"\n"
				+
				"\n"
				+ 
				"\n" 
				+ 
				"pour le gider "+ 
				"\n"
				+" suiver les instruction doner \n");
		texte1.setBounds(75, 50, 500,250);
		buffImage = Accueil.LectureImage("./donnee/Fond.jpg");
		image = Toolkit.getDefaultToolkit().createImage(buffImage.getSource());
		image = image.getScaledInstance(1200, 720, Image.SCALE_DEFAULT);
		labelImage = new JLabel(new ImageIcon(image));		
        labelImage.setBounds( 0, 0, 1200, 720);
        p = new JPanel();
        panelOp = new JPanel();
        panelOp.setBounds(25, 50, 950,480);
        ok = new JButton("Ok");
        ok.setBounds(880,550,100,30);
        Color c = new Color(255,244,235); //beige couleur bois
        panelOp.setBackground(c);
        ok.setBackground(c);
        texte1.setBackground(c);
        Color col = new Color (139,69,19); //marron
        ok.setBorder(BorderFactory.createLineBorder(col,2));
        panelOp.setBorder(BorderFactory.createLineBorder(col,2));
        
        //Disposition des composants
       panelOp.setLayout(new FlowLayout());
       panelOp.add(texte1);
       
       p.setLayout(null);
       p.add(ok);
       p.add(panelOp);
       p.add(labelImage);
       
       this.setLayout(new BorderLayout());
       this.add(p);
       
       //Actions
       
      ok.addActionListener(new BoutonConsigne(this));
       
      //affichage de la fenetre
       this.setTitle("Règles");
       this.setVisible(true);
       int longueur=1000;
       int largeur=630;
       this.setSize(longueur, largeur);
       Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
       this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
	}
	
}

