/**
 * classe pour la fenetre de jeu
 * @author rozenn/Klervia
 * @version1
 */

package Vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.MouseInputAdapter;

import Controleur.Jeux.BoutonQuitter;
import Controleur.Jeux.GrilleController;
import Controleur.Option.RegleButtonListener;
import ModeleInterface.Cases;
import ModeleInterface.Couleur;
import ModeleInterface.Difficulte;
import ModeleInterface.GrilleModel;
import ModeleInterface.Humain;
import ModeleInterface.Mode;
import ModeleInterface.Partie;
import ModeleInterface.Plateau;

public class Jeux extends JPanel {
	
	private JLabel pion;
	private JLabel pionCouleur;
	private JButton quitter;
	
	private JRadioButton horizontal;
	private JRadioButton vertical;
	private JTextField xBarriere;
	private JTextField yBarriere;
	private BufferedImage buffImage;
	private Image image;
	private JLabel labelImage;
	private Fenetre fenetre;
	public Partie part;
	private JLabel nbBarriereLabel;
	private ButtonGroup sens;
	private JTable tab;
	private GrilleModel model;
	/**
	 * le constructeur
	 */
	public Jeux(Mode mode , String nom1 , String nom2 , String nom3, String nom4, Difficulte dif2 , Difficulte dif3 , Difficulte dif4, Fenetre f) {
		this.fenetre = f;
		
		part = new Partie(mode ,nom1 ,nom2,nom3,nom4,dif2 ,dif3 , dif4 ,this.fenetre);
		JPanel tableau = new JPanel();
		JPanel milieu = new JPanel();
		Color c = new Color(255,244,235); //beige couleur bois
		Color col = new Color (139,69,19);
		
		buffImage = Accueil.LectureImage("./donnee/Fond.jpg");
		image = Toolkit.getDefaultToolkit().createImage(buffImage.getSource());
		image = image.getScaledInstance(1200, 720, Image.SCALE_DEFAULT);
		labelImage = new JLabel(new ImageIcon(image));		
        labelImage.setBounds( 0, 0, 1200, 720);
		
        quitter = new JButton("Quitter");
        quitter.setBackground(c);
		quitter.setBorder(BorderFactory.createLineBorder(col,1));
		quitter.setBounds(1050,600,100,30);
		
		horizontal = new JRadioButton("Horizontale");
		horizontal.setBackground(c);
		horizontal.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				updateTab();
				
			}
				});
		vertical = new JRadioButton("Verticale");
		vertical.setBackground(c);
		vertical.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				updateTab();
				
			}
				});
		
		sens = new ButtonGroup();
		sens.add(this.vertical);
		sens.add (this.horizontal);
		
		JPanel choixSens = new JPanel();
		choixSens.setLayout(new GridLayout(2,0));
		choixSens.add(horizontal);
		choixSens.add(vertical);
		
		choixSens.setBackground(c);
		choixSens.setBorder(BorderFactory.createLineBorder(col,2));
		choixSens.setBounds(755, 450, 100, 75);
		
		model = new GrilleModel(this.fenetre, part.plateau.getPlateauJeux());
	    tab = new JTable(model);
	    
	    tab.setBounds(50, 40, 570, 570);
	    tab.setFillsViewportHeight(true);
	    tab.setRowHeight(tab.getWidth()/tab.getColumnCount());
	    tab.setShowGrid(true);
	    tab.setBackground(c);
	    //tab.setEnabled(false);
	    tab.addMouseMotionListener(new mouseMotionGridController(tab, this));
	    tab.addMouseWheelListener(new mouseWheelController(this));
	    


	    tableau.add(tab);
		
		pion = new JLabel("Au tour de J"+this.part.getAuTourDe()+"("+this.part.getAuTourDeStr()+")");
		
		pion.setBounds(800, 110, 300, 30);
		pion.setForeground(c);
		pion.setFont(new Font("Arial",Font.BOLD,16));
		
		pionCouleur = new JLabel(new ImageIcon("donnee/PionRouge.png"));
		pionCouleur.setBounds(755, 105, 40, 40);
		pionCouleur.setBorder(BorderFactory.createLineBorder(col,2));
		
		nbBarriereLabel = new JLabel("Barrieres restantes : "+this.part.getBarriereDujoueur());
		nbBarriereLabel.setBounds(755, 150, 300, 30);
		nbBarriereLabel.setForeground(c);
		nbBarriereLabel.setFont(new Font("Arial",Font.BOLD,16));
		
		
		JLabel barriere = new JLabel("Orientation Barriere");
		barriere.setBounds(755, 400, 200, 30);
		barriere.setForeground(c);
		barriere.setFont(new Font("Arial",Font.BOLD,16));
	
		milieu.setLayout(null);
		milieu.add(quitter);
		milieu.add(pion);
		milieu.add(pionCouleur);
		milieu.add(nbBarriereLabel);
		milieu.add(barriere);
		milieu.add(tab);
		milieu.add(choixSens,BorderLayout.SOUTH );
		milieu.add(new JLabel("Plateau"),BorderLayout.WEST );
		milieu.add(labelImage);
		
		vertical.setSelected(true);
		this.setLayout(new BorderLayout());
		this.add(milieu);
		
		//action
		tab.addMouseListener(new GrilleController(tab,this,model,this.fenetre ));
		this.quitter.addActionListener(new  BoutonQuitter(this.fenetre ));
		
	}
	
	private class mouseMotionGridController extends MouseInputAdapter {

		private JTable tab;
		private Jeux jeux;
		public mouseMotionGridController(JTable t, Jeux j) {
			this.tab=t;
			this.jeux=j;
		}
		
		int prevX=1;
		int prevY=1;
		Couleur prevColor;
		// Permet de mettre des pr�visualisation des pions/barri�res
		public void mouseMoved(MouseEvent e) {
			
			int xx = this.tab.rowAtPoint(e.getPoint());
			int yy = this.tab.columnAtPoint(e.getPoint());
			
			if (this.tab.getModel().getValueAt(xx, yy).toString().equals("donnee/CaseVide.png")
					
					
					
					
					
					) {
				if(this.tab.getModel().getValueAt(this.prevX, this.prevY).toString().equals("donnee/fakebarriere.png")||
						this.tab.getModel().getValueAt(this.prevX, this.prevY).toString().equals("donnee/fakepion.png")
						) {
					if(this.prevColor==Couleur.LIBREB) {
						if(this.jeux.vertical.isSelected()) {
							this.tab.getModel().setValueAt(this.prevColor,this.prevX-1, this.prevY);
							this.tab.getModel().setValueAt(this.prevColor,this.prevX, this.prevY);
							this.tab.getModel().setValueAt(this.prevColor,this.prevX+1, this.prevY);
						}else {
							this.tab.getModel().setValueAt(this.prevColor,this.prevX, this.prevY-1);
							this.tab.getModel().setValueAt(this.prevColor,this.prevX, this.prevY);
							this.tab.getModel().setValueAt(this.prevColor,this.prevX, this.prevY+1);
						}
					
					}else {
						this.tab.getModel().setValueAt(this.prevColor,this.prevX, this.prevY);
					}
				}
				this.prevX=xx;
				this.prevY=yy;
				this.prevColor=Couleur.LIBREP;
				this.tab.getModel().setValueAt(Couleur.FAKEPION,xx, yy);
				//Si on est dans une case de barri�re vide
			}else if(this.tab.getModel().getValueAt(xx, yy).toString().equals("donnee/BarriereVide.png")){
				//Si les coordon�es sont possibles pour la pose de barri�res
				if (xx%2==0 &&yy%2==0 &&xx!=0 &&xx!=18 &&yy!=0&&yy!=18) {
					
					// Si une barri�re n'est pas pr�sente � c�t� (�viter l'�crasement)
					if(jeux.vertical.isSelected()) {
						if(!this.tab.getModel().getValueAt(xx+1, yy).toString().equals("donnee/Barriere.png")
								&&!this.tab.getModel().getValueAt(xx-1, yy).toString().equals("donnee/Barriere.png")){
							
							
							if(this.tab.getModel().getValueAt(this.prevX, this.prevY).toString().equals("donnee/fakebarriere.png")||
									this.tab.getModel().getValueAt(this.prevX, this.prevY).toString().equals("donnee/fakepion.png")
									) {
								if(this.prevColor==Couleur.LIBREB) {
									if(this.jeux.vertical.isSelected()) {
										this.tab.getModel().setValueAt(this.prevColor,this.prevX-1, this.prevY);
										this.tab.getModel().setValueAt(this.prevColor,this.prevX, this.prevY);
										this.tab.getModel().setValueAt(this.prevColor,this.prevX+1, this.prevY);
									}else {
										this.tab.getModel().setValueAt(this.prevColor,this.prevX, this.prevY-1);
										this.tab.getModel().setValueAt(this.prevColor,this.prevX, this.prevY);
										this.tab.getModel().setValueAt(this.prevColor,this.prevX, this.prevY+1);
									}
								
								}else {
									this.tab.getModel().setValueAt(this.prevColor,this.prevX, this.prevY);
								}
								
							}
							this.prevX=xx;
							this.prevY=yy;
							this.prevColor=Couleur.LIBREB;
							
							
							if(this.jeux.vertical.isSelected()) {
								this.tab.getModel().setValueAt(Couleur.FAKEMUR,xx, yy);
								this.tab.getModel().setValueAt(Couleur.FAKEMUR,xx-1, yy);
								this.tab.getModel().setValueAt(Couleur.FAKEMUR,xx+1, yy);
							}else {
								this.tab.getModel().setValueAt(Couleur.FAKEMUR,xx, yy+1);
								this.tab.getModel().setValueAt(Couleur.FAKEMUR,xx, yy);
								this.tab.getModel().setValueAt(Couleur.FAKEMUR,xx, yy-1);
							}
						
						}

					}else {
						if(!this.tab.getModel().getValueAt(xx, yy+1).toString().equals("donnee/Barriere.png")
								&&!this.tab.getModel().getValueAt(xx, yy-1).toString().equals("donnee/Barriere.png")){
						
							if(this.tab.getModel().getValueAt(this.prevX, this.prevY).toString().equals("donnee/fakebarriere.png")||
									this.tab.getModel().getValueAt(this.prevX, this.prevY).toString().equals("donnee/fakepion.png")
									) {
								if(this.prevColor==Couleur.LIBREB) {
									if(this.jeux.vertical.isSelected()) {
										this.tab.getModel().setValueAt(this.prevColor,this.prevX-1, this.prevY);
										this.tab.getModel().setValueAt(this.prevColor,this.prevX, this.prevY);
										this.tab.getModel().setValueAt(this.prevColor,this.prevX+1, this.prevY);
									}else {
										this.tab.getModel().setValueAt(this.prevColor,this.prevX, this.prevY-1);
										this.tab.getModel().setValueAt(this.prevColor,this.prevX, this.prevY);
										this.tab.getModel().setValueAt(this.prevColor,this.prevX, this.prevY+1);
									}
								
								}else {
									this.tab.getModel().setValueAt(this.prevColor,this.prevX, this.prevY);
								}
								
							}
							this.prevX=xx;
							this.prevY=yy;
							this.prevColor=Couleur.LIBREB;
							
							
							if(this.jeux.vertical.isSelected()) {
								this.tab.getModel().setValueAt(Couleur.FAKEMUR,xx, yy);
								this.tab.getModel().setValueAt(Couleur.FAKEMUR,xx-1, yy);
								this.tab.getModel().setValueAt(Couleur.FAKEMUR,xx+1, yy);
							}else {
								this.tab.getModel().setValueAt(Couleur.FAKEMUR,xx, yy+1);
								this.tab.getModel().setValueAt(Couleur.FAKEMUR,xx, yy);
								this.tab.getModel().setValueAt(Couleur.FAKEMUR,xx, yy-1);
							}
						
						}
					}

					

					
					
				}

			}

			
		}

	}
	//Permet de controller les radio Button plus facilement
	private class mouseWheelController implements MouseWheelListener {

		private JTable tab;
		private Jeux jeux;

		public mouseWheelController(Jeux j) {
			this.jeux=j;
		}

		@Override
		public void mouseWheelMoved(MouseWheelEvent e) {
			this.jeux.model.setgrille(this.jeux.part.plateau.getPlateauJeux());
			if(this.jeux.vertical.isSelected()) {
				this.jeux.vertical.setSelected(false);
				this.jeux.horizontal.setSelected(true);
			}else {
				this.jeux.vertical.setSelected(true);
				this.jeux.horizontal.setSelected(false);
			}
			
			
		}
		
	}
	public void setCorBarriere(int x, int y) {
		this.xBarriere.setText(String.valueOf(x));
		this.yBarriere.setText(String.valueOf(y));
	}
	
	public int getXBarriere() {
		return  Integer.parseInt(this.xBarriere.getText()) ;
	}
	public int getYBarriere() {
		return  Integer.parseInt(this.yBarriere.getText()) ;
	}
	public boolean getSensBarriere() {
		return  vertical.isSelected() ;
	}
	
	public void updateTab() {
		model.setgrille(this.part.plateau.getPlateauJeux());
	}

	public void changeLebelJoueur() {
		this.pion.setText("Au tour de J"+this.part.getAuTourDe()+"("+this.part.getAuTourDeStr()+")");
		ImageIcon icon = new ImageIcon("donnee/PionRouge.png");
		switch(this.part.getAuTourDe()) {
		case 1:
			icon = new ImageIcon("donnee/PionRouge.png");
			break;
		case 2:
			icon = new ImageIcon("donnee/PionNoir.png");
			break;
		case 3:
			icon = new ImageIcon("donnee/PionBleu.png");
			break;
		case 4:
			icon = new ImageIcon("donnee/PionVert.png");
			break;
		}
		
		
		
		this.pionCouleur.setIcon(icon);
		
		this.nbBarriereLabel.setText("Barri�res Restantes : "+this.part.getBarriereDujoueur());
		
		if(this.part.getBarriereDujoueur()==0) {
			this.vertical.setEnabled(false);
			this.horizontal.setEnabled(false);
		}else {
			this.vertical.setEnabled(true);
			this.horizontal.setEnabled(true);
		}
		
		
		
	}
}
	