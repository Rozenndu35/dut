package Vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.text.JTextComponent;

import Controleur.Option.OkRegleButtonListener;

public class Regles extends JFrame{

	
	private JButton ok;
	private BufferedImage buffImage;
	private Image image;
	private JLabel labelImage;
	private JPanel p;
	private JPanel panelOp;
	private JTextComponent texte1;
	
	/**
	 * le constructeur
	 */
	public Regles() {
		//initialisation des composants
		
		texte1 = new JTextArea("Fonctionnement : "
				+ 
				"\n" 
				+ 
				"\n" 
				+ "A tour de rôle, chaque joueur choisit :"
				+
				"\n"
				+ 
				"   -> de déplacer son pion d'une case verticalement ou horizontalement, en avant ou en arrière."
				+ 
				"\n" 
				+ 
				"   -> de poser une de ses barrières. Une barrière doit être posée exactement entre deux blocs de 2 cases."
				+ 
				"\n"
				+ 
				"\n" 
				+
				"Lorsqu'il n'a plus de barrières, un joueur est obligé de déplacer son pion."
				+ 
				"\n"
				+ 
				"\n" 
				+ 
				"La pose des barrières a pour but de se créer son propre chemin ou de ralentir d'adversaire,"
				+ 
				"\n"
				+"mais il est interdit de lui fermer totalement l'accès à sa ligne de but.\n" + 
				"\n" + 
				"Quand 2 pions se retrouvent en vis-à-vis sur 2 cases voisines non séparées par une barrière,"
				+ 
				"\n"
				+
				"le joueur dont c'est le tour peut sauter par-dessus son adversaire et se retrouver derrière lui."
				+ 
				"\n"
				+
				"Si une barrière se trouve derrière le pion sauté, le joueur peut choisir de bifurquer à droite ou à gauche du pion sauté. ");
		texte1.setBounds(75, 50, 500,250);
		texte1.setEditable(false);
		texte1.setFont(new Font("Arial",Font.BOLD,13));
		buffImage = Accueil.LectureImage("./donnee/Fond.jpg");
		image = Toolkit.getDefaultToolkit().createImage(buffImage.getSource());
		image = image.getScaledInstance(1200, 720, Image.SCALE_DEFAULT);
		labelImage = new JLabel(new ImageIcon(image));		
        labelImage.setBounds( 0, 0, 1200, 720);
        p = new JPanel();
        panelOp = new JPanel();
        panelOp.setBounds(25, 150, 950,280);
        ok = new JButton("Ok");
        ok.setBounds(880,550,100,30);
        Color c = new Color(255,244,235); //beige couleur bois
        panelOp.setBackground(c);
        ok.setBackground(c);
        texte1.setBackground(c);
        Color col = new Color (139,69,19); //marron
        ok.setBorder(BorderFactory.createLineBorder(col,2));
        panelOp.setBorder(BorderFactory.createLineBorder(col,2));
        
        //Disposition des composants
       panelOp.setLayout(new FlowLayout());
       panelOp.add(texte1);
       
       p.setLayout(null);
       p.add(ok);
       p.add(panelOp);
       p.add(labelImage);
       
       this.setLayout(new BorderLayout());
       this.add(p);
       
       //Actions
       
      ok.addActionListener(new OkRegleButtonListener(this));
       
      //affichage de la fenetre
       this.setTitle("Règles");
       this.setVisible(true);
       int longueur=1000;
       int largeur=630;
       this.setSize(longueur, largeur);
       Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
       this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
	}
	
}
