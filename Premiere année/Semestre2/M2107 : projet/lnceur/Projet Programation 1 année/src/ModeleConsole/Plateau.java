/**
 * classe pour le plateau de jeux et ces actions
 * @author rozenn/Klervia
 * @version1
 */

package ModeleConsole;

import java.io.Serializable;
import java.util.*;

public class Plateau implements Serializable {

	private Cases[][] plateauJeux; // le plateau de jeu
	private HashMap listX; // la liste des X possibles apres verif piont (cle , x)
	private HashMap listY; // la liste des Y possibles apres verif piont (cle , y)
	private int nbJ; // le nombre de joueur
	private HashMap listPionx; // la liste des pion avec le x ou il sont situer
	private HashMap listPiony; // la liste des pion avec le y ou il sont situer

	// pour le parcour
	private HashMap saut; // la liste des saut lorsque on est revenus en arriere
	private ArrayList listXFait; // la liste des x deja passe (l'indice permet de trouver sont y assosier et danc
									// avoir la case
	private ArrayList listYFait; // la liste des y deja passe

	/**
	 * le constructeur pour les tests
	 * 
	 * @param plateauTest : le plateau qui permet les tests
	 */
	public Plateau(Cases[][] plateauTest, boolean test) {
		this.plateauJeux = plateauTest;
		this.listX = new HashMap<String, Integer>();
		this.listY = new HashMap<String, Integer>();
		this.listPionx = new HashMap<Couleur, Integer>();
		this.listPiony = new HashMap<Couleur, Integer>();
		this.listPionx.put(Couleur.ROUGE, 1);
		this.listPiony.put(Couleur.ROUGE, 9);
		this.listPionx.put(Couleur.NOIR, 17);
		this.listPiony.put(Couleur.NOIR, 9);
		this.listPionx.put(Couleur.BLEU, 9);
		this.listPiony.put(Couleur.BLEU, 1);
		this.listPionx.put(Couleur.VERT, 9);
		this.listPiony.put(Couleur.VERT, 17);
		System.out.println(toString());
	}

	/**
	 * constructeur pour le labyrinthe
	 */

	public Plateau(Cases[][] plateau) {
		this.plateauJeux = plateau;
		this.listX = new HashMap<String, Integer>();
		this.listY = new HashMap<String, Integer>();
		this.listPionx = new HashMap<Couleur, Integer>();
		this.listPiony = new HashMap<Couleur, Integer>();
	}

	/**
	 * le constructeur
	 */
	public Plateau(int nbJoueur) {
		this.plateauJeux = new Cases[19][19];
		this.nbJ = nbJoueur;
		this.listPionx = new HashMap<Couleur, Integer>();
		this.listPiony = new HashMap<Couleur, Integer>();
		for (int i = 0; i < 19; i++) {
			for (int j = 0; j < 19; j++) {
				this.plateauJeux[i][j] = new Cases();
				if ((i % 2 == 0) || (j % 2 == 0)) {
					this.plateauJeux[i][j].setCouleur(Couleur.LIBREB);
				} else {
					this.plateauJeux[i][j].setCouleur(Couleur.LIBREP);
				}
			}
		}
		if (nbJoueur == 2) {
			this.listPionx.put(Couleur.ROUGE, 1);
			this.listPiony.put(Couleur.ROUGE, 9);
			this.listPionx.put(Couleur.NOIR, 17);
			this.listPiony.put(Couleur.NOIR, 9);
			this.plateauJeux[1][9].setCouleur(Couleur.ROUGE);
			this.plateauJeux[17][9].setCouleur(Couleur.NOIR);
		} else {
			this.listPionx.put(Couleur.ROUGE, 1);
			this.listPiony.put(Couleur.ROUGE, 9);
			this.listPionx.put(Couleur.NOIR, 17);
			this.listPiony.put(Couleur.NOIR, 9);
			this.listPionx.put(Couleur.BLEU, 9);
			this.listPiony.put(Couleur.BLEU, 1);
			this.listPionx.put(Couleur.VERT, 9);
			this.listPiony.put(Couleur.VERT, 17);
			this.plateauJeux[1][9].setCouleur(Couleur.ROUGE);
			this.plateauJeux[17][9].setCouleur(Couleur.NOIR);
			this.plateauJeux[9][1].setCouleur(Couleur.BLEU);
			this.plateauJeux[9][17].setCouleur(Couleur.VERT);
		}
		this.listX = new HashMap<String, Integer>();
		this.listY = new HashMap<String, Integer>();
	}

	// _______________________________________________________________________________________________
	/**
	 * methode qui verifie si c est un pion en face
	 * @param x : le x a verifier
	 * @param y : a verifier
	 * @return true si c est un pion
	 */
	public boolean verifPion(int x, int y) {
		boolean ret = false;
		listX.clear();
		listY.clear();
		if (x > 0 && x < 18 && y > 0 && y < 18) {
			boolean d = verifDroite(x, y);
			boolean g = verifGauche(x, y);
			boolean b = verifBas(x, y);
			boolean h = verifHaut(x, y);
			if (d || g || b || h) {

				ret = true;
			}
		}
		return ret;
	}

	/**
	 * methode qui verifie si a droite c est libre
	 * @param y : a verifier
	 * @param x : a verifier
	 * @return true si bon
	 */
	public boolean verifDroite(int x, int y) {
		boolean ret = false;
		int xpion = x;
		int ypion = y;
		int xou = x;
		int you = y + 2;
		int xb = x;
		int yb = y + 1;
		if (xou > 0 && xou < 18 && you > 0 && you < 18) {
			if (plateauJeux[xb][yb].getCouleur() == Couleur.MUR) {
				ret = false;
			} else if (plateauJeux[xou][you].getCouleur() == Couleur.LIBREP
					|| plateauJeux[xou][you].getCouleur() == Couleur.ETOILE) {
				ret = true;
				int cle = listX.size();
				listX.put(cle, xou);
				listY.put(cle, you);
			} else {
				String d = verifsautDroit(xpion, ypion);
				if (d.equals("barriere") || d.equals("pion")) {
					boolean hD = verifDiagonaleHD(xpion, ypion, true);
					boolean bD = verifDiagonaleBD(xpion, ypion, true);
				}

			}
		}
		return ret;
	}

	/**
	 * methode qui verifie si a gauche c est libre
	 * @param y : a verifier
	 * @param x : a verifier
	 * @return true si bon
	 */
	public boolean verifGauche(int x, int y) {
		boolean ret = false;
		int xpion = x;
		int ypion = y;
		int xou = x;
		int you = y - 2;
		int xb = x;
		int yb = y - 1;
		if (xou > 0 && xou < 18 && you > 0 && you < 18) {
			if (plateauJeux[xb][yb].getCouleur() == Couleur.MUR) {
				ret = false;
			} else if (plateauJeux[xou][you].getCouleur() == Couleur.LIBREP
					|| plateauJeux[xou][you].getCouleur() == Couleur.ETOILE) {
				ret = true;
				int cle = listX.size();
				listX.put(cle, xou);
				listY.put(cle, you);
			} else {
				String g = verifsautGauche(xpion, ypion);
				if (g.equals("barriere") || g.equals("pion")) {
					boolean hG = verifDiagonaleHG(xpion, ypion, true);
					boolean bG = verifDiagonaleBG(xpion, ypion, true);
				}
			}
		}
		return ret;
	}

	/**
	 * methode qui verifie si en bas c est libre
	 * @param y : a verifier
	 * @param x : a verifier
	 * @return true si bon
	 */
	public boolean verifBas(int x, int y) {
		boolean ret = false;
		int xpion = x;
		int ypion = y;
		int xou = x + 2;
		int you = y;
		int xb = x + 1;
		int yb = y;
		if (xou > 0 && xou < 18 && you > 0 && you < 18) {
			if (plateauJeux[xb][yb].getCouleur() == Couleur.MUR) {
				ret = false;
			} else if (plateauJeux[xou][you].getCouleur() == Couleur.LIBREP
					|| plateauJeux[xou][you].getCouleur() == Couleur.ETOILE) {
				ret = true;
				int cle = listX.size();
				listX.put(cle, xou);
				listY.put(cle, you);
			} else {
				String b = verifsautBas(xpion, ypion);
				if (b.equals("barriere") || b.equals("pion")) {
					boolean bD = verifDiagonaleBD(xpion, ypion, false);
					boolean bG = verifDiagonaleBG(xpion, ypion, false);
				}
			}
		}
		return ret;
	}

	/**
	 * methode qui verifie si en haut c est libre
	 * @param y : a verifier
	 * @param x : a verifier
	 * @return true si bon
	 */
	public boolean verifHaut(int x, int y) {
		boolean ret = false;
		int xpion = x;
		int ypion = y;
		int xou = x - 2;
		int you = y;
		int xb = x - 1;
		int yb = y;
		if (xou > 0 && xou < 18 && you > 0 && you < 18) {
			if (plateauJeux[xb][yb].getCouleur() == Couleur.MUR) {
				ret = false;
			} else if (plateauJeux[xou][you].getCouleur() == Couleur.LIBREP
					|| plateauJeux[xou][you].getCouleur() == Couleur.ETOILE) {
				ret = true;
				int cle = listX.size();
				listX.put(cle, xou);
				listY.put(cle, you);
			} else {
				String h = verifsautHaut(xpion, ypion);
				if (h.equals("barriere") || h.equals("pion")) {
					boolean hD = verifDiagonaleHD(xpion, ypion, false);
					boolean hG = verifDiagonaleHG(xpion, ypion, false);
				}
			}
		}
		return ret;
	}

	/**
	 * methode qui verifie si en diagonal en haut a droite c est libre
	 * @param y : a verifier
	 * @param x : a verifier 
	 * @return true si bon
	 */
	public boolean verifDiagonaleHD(int x, int y, boolean cote) {
		boolean ret = false;
		int xpion = x;
		int ypion = y;
		int xou = x - 2;
		int you = y + 2;
		int xb = x - 1;
		int yb = y + 2;
		if (cote) {
			xb = x - 2;
			yb = y + 1;
		}
		if (xou > 0 && xou < 18 && you > 0 && you < 18) {
			if (plateauJeux[xou][you].getCouleur() == Couleur.LIBREP) {
				ret = true;
				int cle = listX.size();
				listX.put(cle, xou);
				listY.put(cle, you);
			}
		}
		return ret;
	}

	/**
	 * methode qui verifie si en diagonal en haut a gauche c est libre
	 * @param y : a verifier
	 * @param x : a verifier 
	 * @return true si bon
	 */
	public boolean verifDiagonaleHG(int x, int y, boolean cote) {
		boolean ret = false;
		int xpion = x;
		int ypion = y;
		int xou = x - 2;
		int you = y - 2;
		int xb = x - 1;
		int yb = y - 2;
		if (cote) {
			xb = x - 2;
			yb = y - 1;
		}
		if (xou > 0 && xou < 18 && you > 0 && you < 18) {
			if (plateauJeux[xou][you].getCouleur() == Couleur.LIBREP) {
				ret = true;
				int cle = listX.size();
				listX.put(cle, xou);
				listY.put(cle, you);
			}
		}
		return ret;
	}

	/**
	 * methode qui verifie si en diagonal en bas a gauche c est libre
	 * @param y : a verifier
	 * @param x : a verifier 
	 * @return true si bon
	 */
	public boolean verifDiagonaleBG(int x, int y, boolean cote) {
		boolean ret = false;
		int xpion = x;
		int ypion = y;
		int xou = x - 2;
		int you = y - 2;
		int xb = x + 1;
		int yb = y - 2;
		if (cote) {
			xb = x + 2;
			yb = y - 1;
		}
		if (xou > 0 && xou < 18 && you > 0 && you < 18) {
			if (plateauJeux[xou][you].getCouleur() == Couleur.LIBREP) {
				ret = true;
				int cle = listX.size();
				listX.put(cle, xou);
				listY.put(cle, you);
			}
		}
		return ret;
	}

	/**
	 * methode qui verifie si en diagonal en bas a droite c est libre
	 * @param y : a verifier
	 * @param x : a verifier 
	 * @return true si bon
	 */
	public boolean verifDiagonaleBD(int x, int y, boolean cote) {
		boolean ret = false;
		int xpion = x;
		int ypion = y;
		int xou = x + 2;
		int you = y + 2;
		int xb = x + 1;
		int yb = y + 2;
		if (cote) {
			xb = x + 2;
			yb = y + 1;
		}
		if (xou > 0 && xou < 18 && you > 0 && you < 18) {
			if (plateauJeux[xou][you].getCouleur() == Couleur.LIBREP) {
				ret = true;
				int cle = listX.size();
				listX.put(cle, xou);
				listY.put(cle, you);
			}
		}
		return ret;
	}

	/**
	 * methode qui verifie si le saut en haut est possible
	 * @param y : a verifier
	 * @param x : a verifier 
	 * @return true si bon
	 */
	public String verifsautHaut(int x, int y) {
		String ret = "sort";
		int xpion = x;
		int ypion = y;
		int xou = x - 4;
		int you = y;
		int xb = x - 3;
		int yb = y;
		if (xou > 0 && xou < 18 && you > 0 && you < 18) {
			if (plateauJeux[xb][yb].getCouleur() == Couleur.MUR) {
				ret = "barriere";
			} else if (plateauJeux[xou][you].getCouleur() == Couleur.LIBREP) {
				ret = "marche";
				int cle = listX.size();
				listX.put(cle, xou);
				listY.put(cle, you);
			} else {
				ret = "pion";
			}
		}
		return ret;
	}

	/**
	 * methode qui verifie si le saut en bas est possible
	 * @param y : a verifier
	 * @param x : a verifier 
	 * @return true si bon
	 */
	public String verifsautBas(int x, int y) {
		String ret = "sort";
		int xpion = x;
		int ypion = y;
		int xou = x + 4;
		int you = y;
		int xb = x + 3;
		int yb = y;
		if (xou > 0 && xou < 18 && you > 0 && you < 18) {
			if (plateauJeux[xb][yb].getCouleur() == Couleur.MUR) {
				ret = "barriere";
			} else if (plateauJeux[xou][you].getCouleur() == Couleur.LIBREP) {
				ret = "marche";
				int cle = listX.size();
				listX.put(cle, xou);
				listY.put(cle, you);
			} else {
				ret = "pion";
			}
		}
		return ret;
	}

	/**
	 * methode qui verifie si le saut a droite est possible
	 * @param y : a verifier
	 * @param x : a verifier 
	 * @return true si bon
	 */
	public String verifsautDroit(int x, int y) {
		String ret = "sort";
		int xpion = x;
		int ypion = y;
		int xou = x;
		int you = y + 4;
		int xb = x;
		int yb = y + 3;
		if (xou > 0 && xou < 18 && you > 0 && you < 18) {
			if (plateauJeux[xb][yb].getCouleur() == Couleur.MUR) {
				ret = "barriere";

			} else if (plateauJeux[xou][you].getCouleur() == Couleur.LIBREP) {
				ret = "marche";
				int cle = listX.size();
				listX.put(cle, xou);
				listY.put(cle, you);
			} else {
				ret = "pion";
			}
		}
		return ret;
	}

	/**
	 * methode qui verifie si le saut a gauche est possible
	 * @param y : a verifier
	 * @param x : a verifier 
	 * @return true si bon
	 */
	public String verifsautGauche(int x, int y) {
		String ret = "sort";
		int xpion = x;
		int ypion = y;
		int xou = x;
		int you = y - 4;
		int xb = x;
		int yb = y - 3;
		if (xou > 0 && xou < 18 && you > 0 && you < 18) {
			if (plateauJeux[xb][yb].getCouleur() == Couleur.MUR) {
				ret = "barriere";
			} else if (plateauJeux[xou][you].getCouleur() == Couleur.LIBREP) {
				ret = "marche";
				int cle = listX.size();
				listX.put(cle, xou);
				listY.put(cle, you);
			} else {
				ret = "pion";
			}
		}
		return ret;
	}

	// _______________________________________________________________________________________________
	/**
	 * methode qui verifie si les barrieres sont correctes
	 * @param y : a verifier
	 * @param x : a verifier 
	 * @return true si c est bon
	 */
	public boolean verifBarriere(int x, int y, boolean verticale, Joueur joueur) {
		boolean ret = false;
		listXFait = new ArrayList<Integer>();
		listYFait = new ArrayList<Integer>();
		saut = new HashMap<Integer, Integer>();
		if (joueur.moinsBarriere()) {
			if (verticale && y > 2 && y < 18 && x < 18 && x > 0) {
				if (plateauJeux[x][y].getCouleur() == Couleur.LIBREB
						&& plateauJeux[x - 1][y].getCouleur() == Couleur.LIBREB
						&& plateauJeux[x - 2][y].getCouleur() == Couleur.LIBREB) {
					this.plateauJeux = this.plateauJeux;
					this.plateauJeux[x - 1][y].setCouleur(Couleur.MUR);
					this.plateauJeux[x - 2][y].setCouleur(Couleur.MUR);
					this.plateauJeux[x][y].setCouleur(Couleur.MUR);
					boolean j1 = parcours(Couleur.ROUGE);
					listXFait.clear();
					listYFait.clear();
					saut.clear();
					boolean j2 = parcours(Couleur.NOIR);
					boolean ja4 = true;
					if (nbJ == 4) {
						listXFait.clear();
						listYFait.clear();
						saut.clear();
						boolean j3 = parcours(Couleur.VERT);
						listXFait.clear();
						listYFait.clear();
						saut.clear();
						boolean j4 = parcours(Couleur.BLEU);
						if (!j3 || !j4) {
							ja4 = false;
						}
					}
					if (j1 && j2 && ja4) {
						ret = true;
					}
					if (!ret) {
						this.plateauJeux[x - 1][y].setCouleur(Couleur.LIBREP);
						this.plateauJeux[x - 2][y].setCouleur(Couleur.LIBREP);
						this.plateauJeux[x][y].setCouleur(Couleur.LIBREP);
					}
				}
			} else if (!verticale && y > 0 && y < 16 && x < 18 && x > 0) {
				if (this.plateauJeux[x][y].getCouleur() == Couleur.LIBREB
						&& this.plateauJeux[x][y + 1].getCouleur() == Couleur.LIBREB
						&& plateauJeux[x][y + 2].getCouleur() == Couleur.LIBREB) {
					this.plateauJeux = this.plateauJeux;
					this.plateauJeux[x][y].setCouleur(Couleur.MUR);
					this.plateauJeux[x][y + 1].setCouleur(Couleur.MUR);
					this.plateauJeux[x][y + 2].setCouleur(Couleur.MUR);
					boolean j1 = parcours(Couleur.ROUGE);
					boolean j2 = parcours(Couleur.NOIR);
					boolean ja4 = true;
					if (nbJ == 4) {
						boolean j3 = parcours(Couleur.VERT);
						boolean j4 = parcours(Couleur.BLEU);
						if (!j3 || !j4) {
							ja4 = false;
						}
					}
					if (j1 && j2 && ja4) {
						ret = true;
					}
					if (!ret) {
						this.plateauJeux[x][y].setCouleur(Couleur.LIBREP);
						this.plateauJeux[x][y + 1].setCouleur(Couleur.LIBREP);
						this.plateauJeux[x][y + 2].setCouleur(Couleur.LIBREP);
					}
				}
			}
		}
		return ret;
	}

	/**
	 * Verifie qu il y a toujours un parcours possible
	 * @param y : la couleur de celui a verifier
	 * @return true si c est bon
	 */
	public boolean parcours(Couleur pionVerif) {
		boolean ret = true;
		// On cree notre matrice etant un tableau d'entier pour etre
		// analysee avec d'autre methodes (isPath/isSafe)

		// On met 0 quand il y a une barriere
		// On met 3 quand on peut traverser
		// On met 1 pour la source (pion)
		// On met 2 pour les cases d'arrivees

		int matrice[][] = new int[19][19];
		
		switch (pionVerif) {
			case BLEU:
				matrice = new int[19][19];
				for (int i = 0; i < this.plateauJeux.length; i++) {
					for (int j = 0; j < this.plateauJeux.length; j++) {
						switch (this.plateauJeux[i][j].getCouleur()) {
						case MUR:
							matrice[i][j] = 0;
							break;
						case BLEU:
							matrice[i][j] = 1;
							break;
						default:
							matrice[i][j] = 3;
						}
					}
				}
				// On rempli les Destinations
				matrice[1][17] = 2;
				matrice[3][17] = 2;
				matrice[5][17] = 2;
				matrice[7][17] = 2;
				matrice[9][17] = 2;
				matrice[11][17] = 2;
				matrice[13][17] = 2;
				matrice[15][17] = 2;
				matrice[17][17] = 2;
				break;
			case VERT:
				matrice = new int[19][19];
				for (int i = 0; i < this.plateauJeux.length; i++) {
					for (int j = 0; j < this.plateauJeux.length; j++) {
						switch (this.plateauJeux[i][j].getCouleur()) {
						case MUR:
							matrice[i][j] = 0;
							break;
						case VERT:
							matrice[i][j] = 1;
							break;
						default:
							matrice[i][j] = 3;
						}
					}
				}
				// On rempli les Destinations
				matrice[1][1] = 2;
				matrice[3][1] = 2;
				matrice[5][1] = 2;
				matrice[7][1] = 2;
				matrice[9][1] = 2;
				matrice[11][1] = 2;
				matrice[13][1] = 2;
				matrice[15][1] = 2;
				matrice[17][1] = 2;
	
				break;
			case ROUGE:
				matrice = new int[19][19];
				for (int i = 0; i < this.plateauJeux.length; i++) {
					for (int j = 0; j < this.plateauJeux.length; j++) {
						switch (this.plateauJeux[i][j].getCouleur()) {
						case MUR:
							matrice[i][j] = 0;
							break;
						case ROUGE:
							matrice[i][j] = 1;
							break;
						default:
							matrice[i][j] = 3;
						}
					}
				}
				// On rempli les Destinations
				matrice[17][1] = 2;
				matrice[17][3] = 2;
				matrice[17][5] = 2;
				matrice[17][7] = 2;
				matrice[17][9] = 2;
				matrice[17][11] = 2;
				matrice[17][13] = 2;
				matrice[17][15] = 2;
				matrice[17][17] = 2;
				break;
			case NOIR:
				matrice = new int[19][19];
				for (int i = 0; i < this.plateauJeux.length; i++) {
					for (int j = 0; j < this.plateauJeux.length; j++) {
						switch (this.plateauJeux[i][j].getCouleur()) {
						case MUR:
							matrice[i][j] = 0;
							break;
						case NOIR:
							matrice[i][j] = 1;
							break;
						default:
							matrice[i][j] = 3;
						}
					}
				}
				// On rempli les Destination
				matrice[1][1] = 2;
				matrice[1][3] = 2;
				matrice[1][5] = 2;
				matrice[1][7] = 2;
				matrice[1][9] = 2;
				matrice[1][11] = 2;
				matrice[1][13] = 2;
				matrice[1][15] = 2;
				matrice[1][17] = 2;
				break;
		}

		// On efface les contours

		for (int i = 0; i < this.plateauJeux.length; i++) {
			matrice[0][i] = 0;
			matrice[18][i] = 0;
			matrice[i][0] = 0;
			matrice[i][18] = 0;
		}
		// On bloque les diagonales dans la matrice
		for (int i = 2; i <= 16; i = i + 2) {
			for (int j = 2; j <= 16; j = j + 2) {
				matrice[i][j] = 0;
			}
		}
		// Affichage si besoin de la matrice
		
		ret = isPath(matrice, 18);
		
		return ret;
		
	}

	/**
	 * Regarde si il y a chemin 
	 * @param matrix : la matrice a regarder
	 * @param n : la taille
	 * @return si il en trouve un
	 */
	public boolean isPath(int matrix[][], int n) {
		boolean visited[][] = new boolean[n][n];
		boolean flag = false;

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (matrix[i][j] == 1 && !visited[i][j])
					if (isPath(matrix, i, j, visited)) {
						flag = true; 
						break;
					}
			}
		}
		return flag;
	}

	/**
	 * verifie les limites
	 * @param i: 
	 * @param j
	 * @param matrix : la matrice
	 * @return : 
	 */
	public boolean isSafe(int i, int j, int matrix[][]) {

		if (i >= 0 && i < matrix.length && j >= 0 && j < matrix[0].length)
			return true;
		return false;
	}

	/**
	 * retoiurne si il peut aller a une nouvelle destination
	 * @param matrix : la matrice
	 * @param i
	 * @param j
	 * @param visited tableau de si ca a etait visiter
	 * @return
	 */
	public boolean isPath(int matrix[][], int i, int j, boolean visited[][]) {

		if (isSafe(i, j, matrix) && matrix[i][j] != 0 && !visited[i][j]) {
			visited[i][j] = true;
			if (matrix[i][j] == 2)
				return true;
			boolean up = isPath(matrix, i - 1, j, visited);

			if (up)
				return true;

			boolean left = isPath(matrix, i, j - 1, visited);

			if (left)
				return true;

			boolean down = isPath(matrix, i + 1, j, visited);

			if (down)
				return true;
			boolean right = isPath(matrix, i, j + 1, visited);

			if (right)
				return true;
		}
		return false; 
	}

	// _______________________________________________________________________________________________
	/**
	 * retourne une case demander
	 * @param x : la coordonne x de la case voulu
	 * @param y : la coordonne y de la case voulu
	 * @return : la cases voulu
	 */
	public Cases getCases(int x, int y) {
		return plateauJeux[x][y];
	}

	/**
	 * retourne la liste de tout les x
	 * 
	 * @return la liste des x
	 */
	public HashMap getlistX() {
		return this.listX;
	}

	/**
	 * retourne la liste de tout les y
	 * 
	 * @return la liste des y
	 */
	public HashMap getlistY() {
		return this.listY;
	}

	/**
	 * met dans un string pour pouvoir afficher le tableau
	 * 
	 * @return le tableau dans le String
	 */
	public String toString() {
		String ret = "";
		ret += "    0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 " + "\n";
		// ret+="
		// —————————————————————————————————————————————————————————"+"\n";
		for (int a = 0; a < 19; a++) {
			if (a < 10) {
				ret += a + " |";
			} else {
				ret += a + "|";
			}

			for (int aC = 0; aC < 19; aC++) {
				ret += this.plateauJeux[a][aC].toString();
			}
			ret += "|" + "\n";

		}

		// ret+="
		// —————————————————————————————————————————————————————————";
		return ret;

	}

	/**
	 * change la couleur lors du deplacement d'un piont
	 * 
	 * @param xe : les cordonnee du pion
	 * @param ye : les cordonnee du pion
	 * @param xn : les nouvelle cordonnee du pion
	 * @param yn : les nouvelle cordonnee du pion
	 */
	public void changeCouleurDeplacerPion(int xe, int ye, int xn, int yn) {
		Couleur nCouleur = this.plateauJeux[xe][ye].getCouleur();
		this.plateauJeux[xe][ye].setCouleur(Couleur.LIBREP);
		this.plateauJeux[xn][yn].setCouleur(nCouleur);
		this.listPionx.put(this.plateauJeux[xe][ye].getCouleur(), xn);
		this.listPiony.put(this.plateauJeux[xe][ye].getCouleur(), yn);
	}

	/**
	 * change la couleur lors de l'ajout d'une barriere
	 * 
	 * @param x         : les cordonne de la barriere
	 * @param y         : les cordonne de la barriere
	 * @param verticale : le sens de la barriere
	 */
	public void ajouteCouleurBarriere(int x, int y, boolean verticale) {
		if (verticale) {
			this.plateauJeux[x][y].setCouleur(Couleur.MUR);
			this.plateauJeux[x - 1][y].setCouleur(Couleur.MUR);
			this.plateauJeux[x - 2][y].setCouleur(Couleur.MUR);
		} else {
			this.plateauJeux[x][y].setCouleur(Couleur.MUR);
			this.plateauJeux[x][y + 1].setCouleur(Couleur.MUR);
			this.plateauJeux[x][y + 2].setCouleur(Couleur.MUR);
		}
	}

	public void ajouterObjet(int x, int y, Couleur couleur) {
		this.plateauJeux[x][y].setCouleur(couleur);
	}

	/**
	 * verification pour le labyrinyhe
	 * @param x : les cordonnée
	 * @param y : les cordonnée
	 */
	public void verifPionLab(int x, int y) {
		listX.clear();
		listY.clear();
		if (x >= 0 && x <= 18 && y >= 0 && y <= 18) {
			if (y + 1 <= 18) {
				if (plateauJeux[x][y + 1].getCouleur() != Couleur.MUR) {
					int cle = listX.size();
					listX.put(cle, x);
					listY.put(cle, y + 1);
				}
			}
			if (y - 1 >= 0) {
				if (plateauJeux[x][y - 1].getCouleur() != Couleur.MUR) {
					int cle = listX.size();
					listX.put(cle, x);
					listY.put(cle, y - 1);
				}
			}
			if (x - 1 >= 0) {
				if (plateauJeux[x - 1][y].getCouleur() != Couleur.MUR) {
					int cle = listX.size();
					listX.put(cle, x - 1);
					listY.put(cle, y);
				}
			}
			if (x + 1 <= 18) {
				if (plateauJeux[x + 1][y].getCouleur() != Couleur.MUR) {
					int cle = listX.size();
					listX.put(cle, x + 1);
					listY.put(cle, y);
				}
			}

		}
	}

	/**
	 * change la couleur des pions lorsque l'on les deplace
	 * @param xe : les enciene cordonee
	 * @param ye : les enciene cordonee
	 * @param xn : les nouvelle cordonee
	 * @param yn : les nouvelle cordonee
	 * @param couleur : la couleur du pion
	 */
	public void changeCouleurDeplacerPionGidder(int xe, int ye, int xn, int yn, Couleur couleur) {
		this.plateauJeux[xe][ye].setCouleur(Couleur.LIBREP);
		this.plateauJeux[xn][yn].setCouleur(couleur);
	}
}