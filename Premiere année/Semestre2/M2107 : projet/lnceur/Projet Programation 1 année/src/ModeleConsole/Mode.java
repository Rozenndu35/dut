/**
 * les differents modes du jeux
 * @author rozenn/Klervia
 * @version1
 */
package ModeleConsole;

import java.io.Serializable;

public enum Mode implements Serializable{
	OH,
	HH,
	HOOO,
	HHOO,
	HHHO,
	HHHH,
	LAB,
	LABETOILE,
	LABBOMBE,
	LABTOUT,
	MATHEMATIQUE,
	GIDER;
}