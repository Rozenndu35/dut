package ModeleConsole;
/**
 * le mode ou l'on nous gide
 */
import java.util.ArrayList;

import ModeleInterface.Plateau;

public class Gider {
	private ModeleConsole.Plateau plat;
	private ArrayList<String> list1Bleu;
	private ArrayList<String> list2Bleu;
	private ArrayList<String> list1Rouge;
	private ArrayList<String> list2Rouge;
	private ArrayList<String> list1Noir;
	private ArrayList<String> list2Noir;
	
	/**
	 * le constructeur
	 * @param level : le niveau
	 */
	public Gider(int level) {
		Cases[][] tableau = new Cases[19][19];
		for (int i = 0; i< 19; i++) {
			for (int j = 0; j< 19; j++) {
				tableau[i][j]= new Cases();
				tableau[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		if (level == 1 ) {
			tableau[5][0].setCouleur(Couleur.BLEU);
			tableau[9][18].setCouleur(Couleur.MAISON);
			tableau[11][0].setCouleur(Couleur.ROUGE);
			tableau[3][18].setCouleur(Couleur.MAISON);
			tableau[15][0].setCouleur(Couleur.NOIR);
			tableau[15][18].setCouleur(Couleur.MAISON);
			list1Bleu = new ArrayList();
			for (int i = 0 ; i<5 ; i++) {
				list1Bleu.add("Droite");		
			}
			for (int i = 0 ; i<4 ; i++) {
				list1Bleu.add("Bas");		
			}
			for (int i = 0 ; i<12 ; i++) {
				list1Bleu.add("Droite");		
			}
			list1Rouge = new ArrayList();
			for (int i = 0 ; i<3 ; i++) {
				list1Rouge.add("Droite");		
			}
			for (int i = 1 ; i<5 ; i++) {
				list1Rouge.add("Haut");		
			}
			for (int i = 1 ; i<7 ; i++) {
				list1Rouge.add("Droite");		
			}
			for (int i = 1 ; i<5 ; i++) {
				list1Rouge.add("Haut");		
			}
			for (int i = 1 ; i<9 ; i++) {
				list1Rouge.add("Droite");		
			}
			list1Noir = new ArrayList();
			for (int i = 1 ; i<3 ; i++) {
				list1Noir.add("Droite");		
			}
			for (int i = 1 ; i<4 ; i++) {
				list1Noir.add("Haut");		
			}
			for (int i = 1 ; i<4 ; i++) {
				list1Noir.add("Droite");		
			}
			for (int i = 1 ; i<6 ; i++) {
				list1Noir.add("Bas");		
			}
			for (int i = 1 ; i<6 ; i++) {
				list1Noir.add("Droite");		
			}
			for (int i = 1 ; i<5 ; i++) {
				list1Noir.add("Haut");		
			}
			for (int i = 1 ; i<9 ; i++) {
				list1Noir.add("Droite");		
			}
			for (int i = 1 ; i<2 ; i++) {
				list1Noir.add("Bas");		
			}
		}
		else if (level == 2 ) {
			tableau[2][0].setCouleur(Couleur.BLEU);
			tableau[16][18].setCouleur(Couleur.MAISON);
			tableau[8][0].setCouleur(Couleur.ROUGE);
			tableau[2][18].setCouleur(Couleur.MAISON);
			tableau[13][0].setCouleur(Couleur.NOIR);
			tableau[8][18].setCouleur(Couleur.MAISON);
			list2Bleu = new ArrayList();
			for (int i = 1 ; i<6 ; i++) {
				list2Bleu.add("Droite");		
			}
			for (int i = 1 ; i<3 ; i++) {
				list2Bleu.add("Haut");		
			}
			for (int i = 1 ; i<5 ; i++) {
				list2Bleu.add("Droite");		
			}
			for (int i = 1 ; i<8 ; i++) {
				list2Bleu.add("Bas");		
			}
			for (int i = 1 ; i<7 ; i++) {
				list2Bleu.add("Droite");		
			}
			for (int i = 1 ; i<12 ; i++) {
				list2Bleu.add("Haut");		
			}
			for (int i = 1 ; i<4 ; i++) {
				list2Bleu.add("Droite");		
			}
			list2Rouge = new ArrayList();
			for (int i = 1 ; i<5 ; i++) {
				list2Rouge.add("Droite");		
			}
			for (int i = 1 ; i<10 ; i++) {
				list2Rouge.add("Bas");		
			}
			for (int i = 1 ; i<3 ; i++) {
				list2Rouge.add("Droite");		
			}
			for (int i = 1 ; i<4 ; i++) {
				list2Rouge.add("Haut");		
			}
			for (int i = 1 ; i<7; i++) {
				list2Rouge.add("Droite");		
			}
			for (int i = 1 ; i<8 ; i++) {
				list2Rouge.add("Haut");		
			}
			for (int i = 1 ; i<5 ; i++) {
				list2Rouge.add("Droite");		
			}
			for (int i = 1 ; i<17 ; i++) {
				list2Rouge.add("Bas");		
			}
			for (int i = 1 ; i<3; i++) {
				list2Rouge.add("Droite");		
			}
			list2Noir = new ArrayList();
			for (int i = 1 ; i<3 ; i++) {
				list2Noir.add("Droite");		
			}
			for (int i = 1 ; i<4 ; i++) {
				list2Noir.add("Bas");		
			}
			for (int i = 1 ; i<6 ; i++) {
				list2Noir.add("Droite");		
			}
			for (int i = 1 ; i<12 ; i++) {
				list2Noir.add("Haut");		
			}
			for (int i = 1 ; i<13 ; i++) {
				list2Noir.add("Droite");		
			}
			for (int i = 1 ; i<3 ; i++) {
				list2Noir.add("Bas");		
			}
		}	
		this.plat = new Plateau (tableau) ;
		
	}
	/**
	 * retourne le plateau
	 * @return : le plateau
	 */
	public ModeleConsole.Plateau getPlateau() {
		return this.plat;
	}
	/**
	 * retourne la liste de direction
	 * @return la liste
	 */
	public ArrayList getlist1Bleu() {
		return this.list1Bleu;
	}
	/**
	 * retourne la liste de direction
	 * @return la liste
	 */
	public ArrayList getlist2Bleu() {
		return this.list2Bleu;
	}
	/**
	 * retourne la liste de direction
	 * @return la liste
	 */
	public ArrayList getlist1Rouge() {
		return this.list1Rouge;
	}
	/**
	 * retourne la liste de direction
	 * @return la liste
	 */
	public ArrayList getlist2Rouge() {
		return this.list2Rouge;
	}
	/**
	 * retourne la liste de direction
	 * @return la liste
	 */
	public ArrayList getlist1Noir() {
		return this.list1Noir;
	}
	/**
	 * retourne la liste de direction
	 * @return la liste
	 */
	public ArrayList getlist2Noir() {
		return this.list2Noir;
	}
}
