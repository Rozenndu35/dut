/**
 * classe abstraite pour les caracteristiques des joueurs
 * @author rozenn/Klervia
 * @version1
 */

package ModeleConsole;

import java.io.Serializable;
import java.util.*;

public abstract class Joueur implements Serializable {

	private ArrayList<String> stockBarriere;
	protected Pion monPion;
	private String nom;
	private int numero;
	protected Plateau lePlateau;

	/**
	 * Constructeur de la classe joueur
	 * @param nom : son nom
	 * @param numero : son numero
	 * @param nbJouer : le nombre de joueur
	 */
	public Joueur(String nom, int numero, int nbJoueur, Plateau plat) {
		if ((nom !=null && numero>0 && numero< 5 )&& (nbJoueur == 2 || nbJoueur == 4 )) {
			this.nom=nom;
			this.numero=numero;
			this.lePlateau = plat;
			if(numero == 1) {
				this.monPion = new Pion(Couleur.ROUGE);
				this.monPion.setX(1);
				this.monPion.setY(9);
			}
			else if(numero == 2) {
				this.monPion = new Pion(Couleur.NOIR);
				this.monPion.setX(17);
				this.monPion.setY(9);
			}
			else if(numero == 3) {
				this.monPion = new Pion(Couleur.BLEU);
				this.monPion.setX(9);
				this.monPion.setY(1);
			}
			else {
				this.monPion = new Pion(Couleur.VERT);
				this.monPion.setX(9);
				this.monPion.setY(17);
			}
			if (nbJoueur == 2) {
				this.stockBarriere = new ArrayList<String> ();
				for(int i = 0 ; i<10 ; i++) {
					this.stockBarriere.add("bariere");
				}
			}
			if (nbJoueur == 4) {
				this.stockBarriere= new ArrayList<String> ();
				for(int i = 0 ; i<5 ; i++) {
					this.stockBarriere.add("bariere");
				}
			}
		}
		else {
			this.nom="";
			this.numero=0;
			this.stockBarriere = new ArrayList<String> ();
			this.monPion = new Pion(Couleur.LIBREP);
		}
		
		
	}
	/**
	 * Methode qui permet d obtenir de nom du joueur
	 * @return nom : le nom du joueur
	 */
	
	public String getNom() {
		return this.nom;
	}

	/**
	 * Methode qui en leve une barriere de ca liste de stockage
	 * @return si ca a marcher
	 */
	public boolean moinsBarriere() {
		boolean ret = false;
		if (this.stockBarriere.size() > 0) {
			this.stockBarriere.remove(this.stockBarriere.size()-1);	
			ret = true;
		}
		return ret;
	}
	
	/**
	 * renvois le nombre de barriere restante
	 * @return le nombre de barierre restante
	 */
	public int getNbBarriere() {
		int ret = 0;
		if (this.stockBarriere.size() > 0) {
			ret = this.stockBarriere.size();
		}
		return ret;
	}
	
	public abstract boolean choix();

}