/**
 * les differentes couleurs des cases
 * @author rozenn/Klervia
 * @version1
 */
package ModeleConsole;

import java.io.Serializable;

public enum Couleur implements Serializable{
	
	// Les quatres pions
	BLEU,
	VERT,
	ROUGE,
	NOIR,
	// Emplacement libre de pion
	LIBREP,
	//Divers bonus singulier
	BOMBE,
	ETOILE,
	ARRIVER,
	MAISON,
	// Barriere 
	MUR,
	// Emplacement libre de barriere
	LIBREB;
	
}