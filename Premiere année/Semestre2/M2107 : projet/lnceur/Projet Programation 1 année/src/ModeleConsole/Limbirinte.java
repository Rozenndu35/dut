package ModeleConsole;
/**
 * le labyrinthe
 * @author rozenn
 * @version 1
 */
public class Limbirinte {
	private Plateau plat;
	
	/**
	 * le constructeur
	 * @param level : le niveau
	 */public Limbirinte(int level) {
		Cases[][] tableau = new Cases[19][19];
		for (int i = 0; i< 19; i++) {
			for (int j = 0; j< 19; j++) {
				tableau[i][j]= new Cases();
				tableau[i][j].setCouleur(Couleur.LIBREP);
				if (i== 0) {tableau[i][j].setCouleur(Couleur.MUR);}
				if (j== 0) {tableau[i][j].setCouleur(Couleur.MUR);}
				if (i== 18) {tableau[i][j].setCouleur(Couleur.MUR);}
				if (j== 18) {tableau[i][j].setCouleur(Couleur.MUR);}
				
			}
		}
		if (level == 1 ) {
			tableau[0][10].setCouleur(Couleur.ARRIVER);
			tableau[18][5].setCouleur(Couleur.ROUGE);
			tableau[1][17].setCouleur(Couleur.MUR);
			tableau[2][2].setCouleur(Couleur.MUR);
			tableau[2][3].setCouleur(Couleur.MUR);
			tableau[2][4].setCouleur(Couleur.MUR);
			tableau[3][3].setCouleur(Couleur.MUR);
			tableau[2][7].setCouleur(Couleur.MUR);
			tableau[3][7].setCouleur(Couleur.MUR);
			tableau[2][9].setCouleur(Couleur.MUR);
			tableau[3][9].setCouleur(Couleur.MUR);
			tableau[2][11].setCouleur(Couleur.MUR);
			tableau[3][11].setCouleur(Couleur.MUR);
			tableau[2][13].setCouleur(Couleur.MUR);
			tableau[3][13].setCouleur(Couleur.MUR);
			tableau[2][15].setCouleur(Couleur.MUR);
			tableau[3][15].setCouleur(Couleur.MUR);
			tableau[3][17].setCouleur(Couleur.MUR);
			tableau[4][1].setCouleur(Couleur.MUR);
			tableau[5][3].setCouleur(Couleur.MUR);
			tableau[5][3].setCouleur(Couleur.MUR);
			tableau[5][4].setCouleur(Couleur.MUR);
			tableau[5][6].setCouleur(Couleur.MUR);
			tableau[5][7].setCouleur(Couleur.MUR);
			tableau[5][8].setCouleur(Couleur.MUR);
			tableau[5][17].setCouleur(Couleur.MUR);
			tableau[5][11].setCouleur(Couleur.MUR);
			tableau[6][11].setCouleur(Couleur.MUR);
			tableau[5][13].setCouleur(Couleur.MUR);
			tableau[6][13].setCouleur(Couleur.MUR);
			tableau[5][15].setCouleur(Couleur.MUR);
			tableau[6][15].setCouleur(Couleur.MUR);
			tableau[7][17].setCouleur(Couleur.MUR);
			tableau[9][17].setCouleur(Couleur.MUR);
			tableau[11][17].setCouleur(Couleur.MUR);
			tableau[13][17].setCouleur(Couleur.MUR);
			tableau[15][17].setCouleur(Couleur.MUR);
			tableau[17][17].setCouleur(Couleur.MUR);
			tableau[7][1].setCouleur(Couleur.MUR);
			tableau[7][2].setCouleur(Couleur.MUR);
			tableau[7][3].setCouleur(Couleur.MUR);
			tableau[7][4].setCouleur(Couleur.MUR);
			tableau[8][1].setCouleur(Couleur.MUR);
			tableau[11][2].setCouleur(Couleur.MUR);
			tableau[11][3].setCouleur(Couleur.MUR);
			tableau[15][4].setCouleur(Couleur.MUR);
			for(int i= 5 ; i<16 ; i++) {
				tableau[16][i].setCouleur(Couleur.MUR);
			}
			tableau[8][8].setCouleur(Couleur.MUR);
			tableau[8][9].setCouleur(Couleur.MUR);
			tableau[8][10].setCouleur(Couleur.MUR);
			tableau[8][11].setCouleur(Couleur.MUR);
			tableau[9][4].setCouleur(Couleur.MUR);
			tableau[9][5].setCouleur(Couleur.MUR);
			tableau[9][6].setCouleur(Couleur.MUR);
			tableau[9][13].setCouleur(Couleur.MUR);
			tableau[9][14].setCouleur(Couleur.MUR);
			tableau[9][15].setCouleur(Couleur.MUR);
			tableau[11][5].setCouleur(Couleur.MUR);
			tableau[12][5].setCouleur(Couleur.MUR);
			tableau[11][6].setCouleur(Couleur.MUR);
			tableau[12][6].setCouleur(Couleur.MUR);
			tableau[11][8].setCouleur(Couleur.MUR);
			tableau[11][9].setCouleur(Couleur.MUR);
			tableau[11][11].setCouleur(Couleur.MUR);
			tableau[11][12].setCouleur(Couleur.MUR);
			tableau[11][14].setCouleur(Couleur.MUR);
			tableau[11][15].setCouleur(Couleur.MUR);
			tableau[12][14].setCouleur(Couleur.MUR);
			tableau[12][15].setCouleur(Couleur.MUR);
			tableau[13][8].setCouleur(Couleur.MUR);
			tableau[13][10].setCouleur(Couleur.MUR);
			tableau[13][11].setCouleur(Couleur.MUR);
			tableau[14][1].setCouleur(Couleur.MUR);
			tableau[15][1].setCouleur(Couleur.MUR);
			tableau[17][1].setCouleur(Couleur.MUR);
			tableau[15][3].setCouleur(Couleur.MUR);
			tableau[14][13].setCouleur(Couleur.MUR);
			tableau[14][14].setCouleur(Couleur.MUR);
			tableau[14][15].setCouleur(Couleur.MUR);
			tableau[14][4].setCouleur(Couleur.MUR);
			tableau[10][2].setCouleur(Couleur.MUR);
			tableau[13][3].setCouleur(Couleur.MUR);
			tableau[14][3].setCouleur(Couleur.MUR);
			tableau[7][6].setCouleur(Couleur.MUR);
			tableau[8][6].setCouleur(Couleur.MUR);
			tableau[7][8].setCouleur(Couleur.MUR);
			tableau[8][13].setCouleur(Couleur.MUR);
			tableau[8][15].setCouleur(Couleur.MUR);
			tableau[10][9].setCouleur(Couleur.MUR);
			tableau[10][11].setCouleur(Couleur.MUR);
			tableau[14][6].setCouleur(Couleur.MUR);
			tableau[15][6].setCouleur(Couleur.MUR);
			tableau[15][8].setCouleur(Couleur.MUR);
			tableau[15][9].setCouleur(Couleur.MUR);
			tableau[15][10].setCouleur(Couleur.MUR);
		}
		else if (level == 2 ) {
			System.out.println ("lab2");
			tableau[0][2].setCouleur(Couleur.ROUGE);
			tableau[0][18].setCouleur(Couleur.ARRIVER);
			tableau[1][18].setCouleur(Couleur.LIBREP);
			tableau[1][3].setCouleur(Couleur.MUR);
			for (int i =1 ; i<18 ; i++) {	
				if(i!=1 && i!=4) {tableau[2][i].setCouleur(Couleur.MUR);}
			}
			tableau[2][2].setCouleur(Couleur.MUR);
			tableau[2][3].setCouleur(Couleur.MUR);
			tableau[2][7].setCouleur(Couleur.MUR);
			tableau[2][8].setCouleur(Couleur.MUR);
			tableau[2][9].setCouleur(Couleur.MUR);
			tableau[2][10].setCouleur(Couleur.MUR);
			tableau[2][11].setCouleur(Couleur.MUR);
			tableau[3][2].setCouleur(Couleur.MUR);
			tableau[3][3].setCouleur(Couleur.MUR);
			tableau[3][7].setCouleur(Couleur.MUR);
			tableau[3][8].setCouleur(Couleur.MUR);
			tableau[3][9].setCouleur(Couleur.MUR);
			tableau[3][10].setCouleur(Couleur.MUR);
			tableau[3][11].setCouleur(Couleur.MUR);
			for (int i =0 ; i<19 ; i++) {	
				if(i!=1 && i!=6 && i!= 12 && i!= 17) {tableau[4][i].setCouleur(Couleur.MUR);}
			}
			tableau[5][2].setCouleur(Couleur.MUR);
			tableau[5][3].setCouleur(Couleur.MUR);
			tableau[5][4].setCouleur(Couleur.MUR);
			tableau[5][13].setCouleur(Couleur.MUR);
			tableau[5][15].setCouleur(Couleur.MUR);
			tableau[5][16].setCouleur(Couleur.MUR);
			for (int i =5 ; i<13 ; i++) {	
				tableau[6][i].setCouleur(Couleur.MUR);
			}
			tableau[6][16].setCouleur(Couleur.MUR);
			tableau[7][2].setCouleur(Couleur.MUR);
			tableau[7][3].setCouleur(Couleur.MUR);
			tableau[7][4].setCouleur(Couleur.MUR);
			tableau[7][14].setCouleur(Couleur.MUR);
			tableau[7][16].setCouleur(Couleur.MUR);
			for (int i =5 ; i<12 ; i++) {	
				tableau[8][i].setCouleur(Couleur.MUR);
			}
			tableau[8][13].setCouleur(Couleur.MUR);
			tableau[8][16].setCouleur(Couleur.MUR);
			for (int i =2 ; i<10 ; i++) {	
				tableau[9][i].setCouleur(Couleur.MUR);
			}
			tableau[9][13].setCouleur(Couleur.MUR);
			tableau[9][15].setCouleur(Couleur.MUR);
			tableau[9][16].setCouleur(Couleur.MUR);
			tableau[10][2].setCouleur(Couleur.MUR);
			tableau[10][10].setCouleur(Couleur.MUR);
			tableau[10][11].setCouleur(Couleur.MUR);
			tableau[10][16].setCouleur(Couleur.MUR);
			for (int i =10 ; i<17; i++) {	
				tableau[11][i].setCouleur(Couleur.MUR);
			}
			tableau[11][2].setCouleur(Couleur.MUR);
			tableau[11][4].setCouleur(Couleur.MUR);
			tableau[11][5].setCouleur(Couleur.MUR);
			tableau[11][7].setCouleur(Couleur.MUR);
			tableau[11][8].setCouleur(Couleur.MUR);
			tableau[12][2].setCouleur(Couleur.MUR);
			tableau[12][4].setCouleur(Couleur.MUR);
			tableau[12][6].setCouleur(Couleur.MUR);
			tableau[12][8].setCouleur(Couleur.MUR);
			tableau[13][2].setCouleur(Couleur.MUR);
			for (int i = 8; i<18; i++) {	
				tableau[13][i].setCouleur(Couleur.MUR);
			}
			for (int i = 3; i < 7; i++) {	
				tableau[14][i].setCouleur(Couleur.MUR);
			}
			tableau[15][2].setCouleur(Couleur.MUR);
			tableau[15][7].setCouleur(Couleur.MUR);
			tableau[15][9].setCouleur(Couleur.MUR);
			tableau[15][10].setCouleur(Couleur.MUR);
			tableau[15][12].setCouleur(Couleur.MUR);
			tableau[15][14].setCouleur(Couleur.MUR);
			tableau[15][15].setCouleur(Couleur.MUR);
			tableau[15][16].setCouleur(Couleur.MUR);
			tableau[16][2].setCouleur(Couleur.MUR);
			tableau[16][4].setCouleur(Couleur.MUR);
			tableau[16][5].setCouleur(Couleur.MUR);
			tableau[16][7].setCouleur(Couleur.MUR);
			tableau[16][8].setCouleur(Couleur.MUR);
			tableau[16][10].setCouleur(Couleur.MUR);
			tableau[16][11].setCouleur(Couleur.MUR);
			tableau[16][13].setCouleur(Couleur.MUR);
			tableau[16][15].setCouleur(Couleur.MUR);
			tableau[16][16].setCouleur(Couleur.MUR);
			
		}
		else if (level == 3 ) {
			System.out.println ("lab3");
			tableau[0][4].setCouleur(Couleur.ROUGE);
			tableau[9][18].setCouleur(Couleur.ARRIVER);
			tableau[2][2].setCouleur(Couleur.MUR);
			tableau[1][3].setCouleur(Couleur.MUR);
			tableau[1][11].setCouleur(Couleur.MUR);
			for (int i = 4; i< 10; i++) {	
				tableau[2][i].setCouleur(Couleur.MUR);
			}
			tableau[2][11].setCouleur(Couleur.MUR);
			tableau[2][13].setCouleur(Couleur.MUR);
			tableau[2][14].setCouleur(Couleur.MUR);
			tableau[2][16].setCouleur(Couleur.MUR);
			tableau[3][11].setCouleur(Couleur.MUR);
			tableau[3][13].setCouleur(Couleur.MUR);
			tableau[3][14].setCouleur(Couleur.MUR);
			tableau[3][16].setCouleur(Couleur.MUR);
			for (int i = 2; i<11; i++) {	
				tableau[4][i].setCouleur(Couleur.MUR);
			}
			tableau[4][13].setCouleur(Couleur.MUR);
			tableau[4][14].setCouleur(Couleur.MUR);
			tableau[4][16].setCouleur(Couleur.MUR);
			tableau[5][2].setCouleur(Couleur.MUR);
			tableau[5][3].setCouleur(Couleur.MUR);
			tableau[5][9].setCouleur(Couleur.MUR);
			tableau[5][10].setCouleur(Couleur.MUR);
			tableau[5][12].setCouleur(Couleur.MUR);
			tableau[5][14].setCouleur(Couleur.MUR);
			tableau[5][16].setCouleur(Couleur.MUR);
			tableau[6][2].setCouleur(Couleur.MUR);
			tableau[6][3].setCouleur(Couleur.MUR);
			tableau[6][5].setCouleur(Couleur.MUR);
			tableau[6][6].setCouleur(Couleur.MUR);
			tableau[6][7].setCouleur(Couleur.MUR);
			tableau[6][9].setCouleur(Couleur.MUR);
			tableau[6][10].setCouleur(Couleur.MUR);
			tableau[6][14].setCouleur(Couleur.MUR);
			tableau[6][16].setCouleur(Couleur.MUR);
			tableau[7][2].setCouleur(Couleur.MUR);
			tableau[7][3].setCouleur(Couleur.MUR);
			tableau[7][7].setCouleur(Couleur.MUR);
			tableau[7][13].setCouleur(Couleur.MUR);
			tableau[7][14].setCouleur(Couleur.MUR);
			tableau[7][16].setCouleur(Couleur.MUR);
			for (int i = 4; i<15; i++) {	
				tableau[8][i].setCouleur(Couleur.MUR);
			}
			tableau[8][16].setCouleur(Couleur.MUR);
			tableau[9][2].setCouleur(Couleur.MUR);
			tableau[9][4].setCouleur(Couleur.MUR);
			tableau[9][16].setCouleur(Couleur.MUR);
			tableau[10][2].setCouleur(Couleur.MUR);
			tableau[10][4].setCouleur(Couleur.MUR);
			for (int i = 6; i<18; i++) {	
				tableau[10][i].setCouleur(Couleur.MUR);
			}
			tableau[11][2].setCouleur(Couleur.MUR);
			tableau[11][4].setCouleur(Couleur.MUR);
			tableau[12][2].setCouleur(Couleur.MUR);
			for (int i = 5; i<17; i++) {	
				tableau[12][i].setCouleur(Couleur.MUR);
			}
			tableau[13][2].setCouleur(Couleur.MUR);
			tableau[13][3].setCouleur(Couleur.MUR);
			tableau[13][4].setCouleur(Couleur.MUR);
			tableau[13][6].setCouleur(Couleur.MUR);
			tableau[13][15].setCouleur(Couleur.MUR);
			tableau[13][16].setCouleur(Couleur.MUR);
			tableau[14][2].setCouleur(Couleur.MUR);
			tableau[14][4].setCouleur(Couleur.MUR);
			tableau[14][9].setCouleur(Couleur.MUR);
			tableau[14][10].setCouleur(Couleur.MUR);
			tableau[14][11].setCouleur(Couleur.MUR);
			tableau[14][13].setCouleur(Couleur.MUR);
			tableau[14][15].setCouleur(Couleur.MUR);
			tableau[14][16].setCouleur(Couleur.MUR);
			tableau[15][2].setCouleur(Couleur.MUR);
			tableau[15][6].setCouleur(Couleur.MUR);
			tableau[15][7].setCouleur(Couleur.MUR);
			tableau[15][13].setCouleur(Couleur.MUR);
			tableau[15][16].setCouleur(Couleur.MUR);
			for (int i =0 ; i<17 ; i++) {	
				if(i!=10) {tableau[16][i].setCouleur(Couleur.MUR);}
			}
		}
		else if (level == 4 ) {
			System.out.println ("lab4");
			for (int i =0 ; i<19 ; i++) {	
				if(i!= 8) {tableau[0][i].setCouleur(Couleur.MUR);}
				else {tableau[0][i].setCouleur(Couleur.ROUGE);}
				if(i!= 0 && i != 1) {tableau[i][18].setCouleur(Couleur.MUR);}
				else if (i == 0){tableau[0][i].setCouleur(Couleur.ARRIVER);}
				}
			for (int i =0 ; i<19 ; i++) {	
				tableau[i][18].setCouleur(Couleur.MUR);
				tableau[i][0].setCouleur(Couleur.MUR);
			}
			for (int i =2; i<17 ; i++) {	
				tableau[2][i].setCouleur(Couleur.MUR);
			}
			tableau[3][2].setCouleur(Couleur.MUR);
			tableau[3][10].setCouleur(Couleur.MUR);
			tableau[3][16].setCouleur(Couleur.MUR);
			for (int i =2; i<17 ; i++) {	
				if ( i != 3 && i!=9 && i!= 11 && i!= 15) {tableau[4][i].setCouleur(Couleur.MUR);}
			}
			tableau[5][4].setCouleur(Couleur.MUR);
			tableau[5][8].setCouleur(Couleur.MUR);
			tableau[5][10].setCouleur(Couleur.MUR);
			tableau[5][12].setCouleur(Couleur.MUR);
			tableau[5][13].setCouleur(Couleur.MUR);
			tableau[5][14].setCouleur(Couleur.MUR);
			tableau[5][16].setCouleur(Couleur.MUR);
			for (int i =1; i<9 ; i++) {	
				if ( i != 5 && i!=7) {tableau[6][i].setCouleur(Couleur.MUR);}
			}
			tableau[6][12].setCouleur(Couleur.MUR);
			tableau[6][16].setCouleur(Couleur.MUR);
			for (int i =4; i<17 ; i++) {	
				if ( i != 5 && i!=7 && i!=9 && i!= 11) {tableau[7][i].setCouleur(Couleur.MUR);}
			}
			for (int i =2; i<11 ; i++) {	
				if ( i != 3 && i!=7 && i!=9) {tableau[8][i].setCouleur(Couleur.MUR);}
			}
			tableau[9][2].setCouleur(Couleur.MUR);
			for (int i =8; i<18 ; i++) {	
				if ( i != 9 && i!=14) {tableau[9][i].setCouleur(Couleur.MUR);}
			}
			for (int i =2; i<8 ; i++) {	
				if ( i != 4) {tableau[10][i].setCouleur(Couleur.MUR);}
			}
			tableau[10][11].setCouleur(Couleur.MUR);
			for (int i =3; i<17 ; i++) {	
				if ( i != 4 && i!=8 && i!= 9) {tableau[11][i].setCouleur(Couleur.MUR);}
			}
			tableau[12][1].setCouleur(Couleur.MUR);
			tableau[12][3].setCouleur(Couleur.MUR);
			tableau[12][8].setCouleur(Couleur.MUR);
			tableau[12][10].setCouleur(Couleur.MUR);
			tableau[12][16].setCouleur(Couleur.MUR);
			tableau[13][1].setCouleur(Couleur.MUR);
			tableau[13][3].setCouleur(Couleur.MUR);
			tableau[13][4].setCouleur(Couleur.MUR);
			for (int i =8; i<17 ; i++) {	
				if ( i != 11 && i!=15) {tableau[13][i].setCouleur(Couleur.MUR);}
			}
			tableau[14][1].setCouleur(Couleur.MUR);
			tableau[14][5].setCouleur(Couleur.MUR);
			tableau[14][6].setCouleur(Couleur.MUR);
			tableau[14][12].setCouleur(Couleur.MUR);
			tableau[14][16].setCouleur(Couleur.MUR);
			for (int i =3; i<13 ; i++) {	
				if ( i != 4 && i!=7 && i!= 11) {tableau[15][i].setCouleur(Couleur.MUR);}
			}
			tableau[15][16].setCouleur(Couleur.MUR);
			tableau[16][2].setCouleur(Couleur.MUR);
			tableau[16][3].setCouleur(Couleur.MUR);
			tableau[16][5].setCouleur(Couleur.MUR);
			tableau[16][10].setCouleur(Couleur.MUR);
			for (int i =12; i<17 ; i++) {	
				tableau[16][i].setCouleur(Couleur.MUR);
			}
			tableau[17][2].setCouleur(Couleur.MUR);
			for (int i =7; i<11 ; i++) {	
				tableau[17][i].setCouleur(Couleur.MUR);
			}
		}
		else if (level == 5 ) {
			System.out.println ("lab5");
			tableau[0][0].setCouleur(Couleur.ROUGE);
			tableau[0][1].setCouleur(Couleur.LIBREP);
			tableau[16][18].setCouleur(Couleur.ARRIVER);
			tableau[1][4].setCouleur(Couleur.MUR);
			tableau[1][10].setCouleur(Couleur.MUR);
			tableau[1][11].setCouleur(Couleur.MUR);
			tableau[1][12].setCouleur(Couleur.MUR);
			tableau[2][1].setCouleur(Couleur.MUR);
			tableau[2][2].setCouleur(Couleur.MUR);
			tableau[2][4].setCouleur(Couleur.MUR);
			tableau[2][6].setCouleur(Couleur.MUR);
			tableau[2][7].setCouleur(Couleur.MUR);
			tableau[2][8].setCouleur(Couleur.MUR);
			tableau[2][14].setCouleur(Couleur.MUR);
			tableau[2][15].setCouleur(Couleur.MUR);
			tableau[2][16].setCouleur(Couleur.MUR);
			tableau[3][2].setCouleur(Couleur.MUR);
			tableau[3][4].setCouleur(Couleur.MUR);
			tableau[3][8].setCouleur(Couleur.MUR);
			tableau[3][9].setCouleur(Couleur.MUR);
			tableau[3][10].setCouleur(Couleur.MUR);
			tableau[3][11].setCouleur(Couleur.MUR);
			tableau[3][13].setCouleur(Couleur.MUR);
			tableau[3][14].setCouleur(Couleur.MUR);
			tableau[3][15].setCouleur(Couleur.MUR);
			tableau[3][16].setCouleur(Couleur.MUR);
			tableau[4][2].setCouleur(Couleur.MUR);
			tableau[4][4].setCouleur(Couleur.MUR);
			tableau[4][5].setCouleur(Couleur.MUR);
			tableau[4][6].setCouleur(Couleur.MUR);
			tableau[4][8].setCouleur(Couleur.MUR);
			tableau[4][13].setCouleur(Couleur.MUR);
			tableau[5][2].setCouleur(Couleur.MUR);
			tableau[5][4].setCouleur(Couleur.MUR);
			tableau[5][8].setCouleur(Couleur.MUR);
			tableau[5][10].setCouleur(Couleur.MUR);
			tableau[5][11].setCouleur(Couleur.MUR);
			tableau[5][13].setCouleur(Couleur.MUR);
			tableau[5][15].setCouleur(Couleur.MUR);
			tableau[5][16].setCouleur(Couleur.MUR);
			tableau[5][17].setCouleur(Couleur.MUR);
			tableau[6][2].setCouleur(Couleur.MUR);
			tableau[6][6].setCouleur(Couleur.MUR);
			tableau[6][7].setCouleur(Couleur.MUR);
			tableau[6][13].setCouleur(Couleur.MUR);
			for (int i =2 ; i<8 ; i++) {	
				tableau[7][i].setCouleur(Couleur.MUR);
			}
			for (int i =9 ; i<13 ; i++) {	
				tableau[7][i].setCouleur(Couleur.MUR);
			}
			tableau[8][7].setCouleur(Couleur.MUR);
			tableau[8][9].setCouleur(Couleur.MUR);
			tableau[8][14].setCouleur(Couleur.MUR);
			tableau[8][15].setCouleur(Couleur.MUR);
			tableau[8][16].setCouleur(Couleur.MUR);
			tableau[9][1].setCouleur(Couleur.MUR);
			tableau[9][3].setCouleur(Couleur.MUR);
			tableau[9][4].setCouleur(Couleur.MUR);
			tableau[9][5].setCouleur(Couleur.MUR);
			tableau[9][9].setCouleur(Couleur.MUR);
			tableau[9][11].setCouleur(Couleur.MUR);
			tableau[9][13].setCouleur(Couleur.MUR);
			tableau[9][15].setCouleur(Couleur.MUR);
			tableau[9][16].setCouleur(Couleur.MUR);
			tableau[10][8].setCouleur(Couleur.MUR);
			tableau[10][13].setCouleur(Couleur.MUR);
			tableau[10][15].setCouleur(Couleur.MUR);
			tableau[11][2].setCouleur(Couleur.MUR);
			for (int i =11 ; i<18 ; i++) {	
				if(i!=14) {tableau[11][i].setCouleur(Couleur.MUR);}
			}
			for (int i =4 ; i<8 ; i++) {	
				tableau[11][i].setCouleur(Couleur.MUR);
			}
			tableau[12][2].setCouleur(Couleur.MUR);
			tableau[12][8].setCouleur(Couleur.MUR);
			tableau[12][9].setCouleur(Couleur.MUR);
			tableau[12][11].setCouleur(Couleur.MUR);
			for (int i =2 ; i<17 ; i++) {	
				if(i!=7 && i!=10 && i!= 12) {tableau[13][i].setCouleur(Couleur.MUR);}
			}
			tableau[14][2].setCouleur(Couleur.MUR);
			tableau[14][8].setCouleur(Couleur.MUR);
			tableau[14][11].setCouleur(Couleur.MUR);
			for (int i =13 ; i<17 ; i++) {	
				tableau[14][i].setCouleur(Couleur.MUR);
			}
			for (int i =2 ; i<14 ; i++) {	
				if(i!=3 && i!=12) {tableau[15][i].setCouleur(Couleur.MUR);}
			}
			tableau[16][2].setCouleur(Couleur.MUR);
			tableau[16][13].setCouleur(Couleur.MUR);
			tableau[16][15].setCouleur(Couleur.MUR);
			tableau[16][16].setCouleur(Couleur.MUR);
			for (int i =3 ; i<14 ; i++) {	
				tableau[17][i].setCouleur(Couleur.MUR);
			}
		}
		else if (level == 6) {
			System.out.println ("lab6");
			tableau[18][4].setCouleur(Couleur.ARRIVER);
			tableau[15][0].setCouleur(Couleur.ROUGE);
			tableau[1][2].setCouleur(Couleur.MUR);
			tableau[1][3].setCouleur(Couleur.MUR);
			tableau[1][8].setCouleur(Couleur.MUR);
			tableau[1][13].setCouleur(Couleur.MUR);
			tableau[1][16].setCouleur(Couleur.MUR);
			tableau[2][5].setCouleur(Couleur.MUR);
			tableau[2][6].setCouleur(Couleur.MUR);
			tableau[2][7].setCouleur(Couleur.MUR);
			tableau[2][10].setCouleur(Couleur.MUR);
			tableau[2][11].setCouleur(Couleur.MUR);
			tableau[2][13].setCouleur(Couleur.MUR);
			tableau[2][16].setCouleur(Couleur.MUR);
			tableau[3][1].setCouleur(Couleur.MUR);
			tableau[3][2].setCouleur(Couleur.MUR);
			tableau[3][3].setCouleur(Couleur.MUR);
			tableau[3][5].setCouleur(Couleur.MUR);
			tableau[3][9].setCouleur(Couleur.MUR);
			tableau[3][15].setCouleur(Couleur.MUR);
			tableau[3][16].setCouleur(Couleur.MUR);
			tableau[4][5].setCouleur(Couleur.MUR);
			tableau[4][7].setCouleur(Couleur.MUR);
			tableau[4][8].setCouleur(Couleur.MUR);
			tableau[4][9].setCouleur(Couleur.MUR);
			tableau[4][11].setCouleur(Couleur.MUR);
			tableau[4][12].setCouleur(Couleur.MUR);
			tableau[5][2].setCouleur(Couleur.MUR);
			tableau[5][3].setCouleur(Couleur.MUR);
			tableau[5][7].setCouleur(Couleur.MUR);
			tableau[5][13].setCouleur(Couleur.MUR);
			tableau[5][14].setCouleur(Couleur.MUR);
			tableau[5][15].setCouleur(Couleur.MUR);
			for (int i =3 ; i<7 ; i++) {	
				tableau[6][i].setCouleur(Couleur.MUR);
			}
			tableau[6][9].setCouleur(Couleur.MUR);
			tableau[6][10].setCouleur(Couleur.MUR);
			tableau[6][13].setCouleur(Couleur.MUR);
			tableau[7][1].setCouleur(Couleur.MUR);
			for (int i =5 ; i<9 ; i++) {	
				tableau[7][i].setCouleur(Couleur.MUR);
			}
			for (int i =12 ; i<18 ; i++) {	
				if ( i != 15) {tableau[7][i].setCouleur(Couleur.MUR);}
			}
			tableau[8][1].setCouleur(Couleur.MUR);
			tableau[8][2].setCouleur(Couleur.MUR);
			tableau[8][3].setCouleur(Couleur.MUR);
			tableau[8][6].setCouleur(Couleur.MUR);
			tableau[8][10].setCouleur(Couleur.MUR);
			tableau[8][14].setCouleur(Couleur.MUR);
			for (int i =4 ; i<17 ; i++) {	
				if ( i != 5 && i != 7 && i != 10 && i!=14 && i!= 15) {tableau[9][i].setCouleur(Couleur.MUR);}
			}
			tableau[10][2].setCouleur(Couleur.MUR);
			tableau[10][6].setCouleur(Couleur.MUR);
			tableau[10][8].setCouleur(Couleur.MUR);
			tableau[10][10].setCouleur(Couleur.MUR);
			tableau[10][11].setCouleur(Couleur.MUR);
			tableau[10][15].setCouleur(Couleur.MUR);
			tableau[10][16].setCouleur(Couleur.MUR);
			for (int i =13 ; i<18 ; i++) {	
				tableau[i][3].setCouleur(Couleur.MUR);
			}
			tableau[11][4].setCouleur(Couleur.MUR);
			tableau[11][5].setCouleur(Couleur.MUR);
			tableau[11][7].setCouleur(Couleur.MUR);
			tableau[11][9].setCouleur(Couleur.MUR);
			tableau[11][14].setCouleur(Couleur.MUR);
			tableau[12][2].setCouleur(Couleur.MUR);
			tableau[12][4].setCouleur(Couleur.MUR);
			for (int i =9 ; i<17 ; i++) {	
				if ( i != 13 && i != 15) {tableau[12][i].setCouleur(Couleur.MUR);}
			}
			tableau[14][1].setCouleur(Couleur.MUR);
			tableau[16][1].setCouleur(Couleur.MUR);
			tableau[13][6].setCouleur(Couleur.MUR);
			tableau[13][7].setCouleur(Couleur.MUR);
			tableau[13][9].setCouleur(Couleur.MUR);
			tableau[13][12].setCouleur(Couleur.MUR);
			tableau[13][14].setCouleur(Couleur.MUR);
			tableau[13][15].setCouleur(Couleur.MUR);
			tableau[14][5].setCouleur(Couleur.MUR);
			tableau[14][10].setCouleur(Couleur.MUR);
			tableau[14][16].setCouleur(Couleur.MUR);
			tableau[14][17].setCouleur(Couleur.MUR);
			tableau[15][6].setCouleur(Couleur.MUR);
			tableau[15][9].setCouleur(Couleur.MUR);
			tableau[15][11].setCouleur(Couleur.MUR);
			tableau[15][12].setCouleur(Couleur.MUR);
			tableau[16][5].setCouleur(Couleur.MUR);
			tableau[16][7].setCouleur(Couleur.MUR);
			tableau[16][8].setCouleur(Couleur.MUR);
			tableau[16][12].setCouleur(Couleur.MUR);
			tableau[16][14].setCouleur(Couleur.MUR);
			tableau[16][15].setCouleur(Couleur.MUR);
			tableau[16][16].setCouleur(Couleur.MUR);
		}
		
		this.plat = new Plateau (tableau) ;
	}
	public Plateau getPlateau() {
		return this.plat;
	}
}
