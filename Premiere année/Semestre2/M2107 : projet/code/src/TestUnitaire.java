/**
 * class pour les test de l'application
 * @author rozenn/Klervia
 * @version1
 */

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import Modele.Cases;
import Modele.Couleur;
import Modele.Plateau;

class TestUnitaire {
	

	@Test
	void test() {
		fail("Not yet implemented");
		testDeplacerPion();
		testDeplacerBarriere();
		
		
	}
	/**
	 * les test pour deplacer un piont
	 */
	public void testDeplacerPion() {
		// verif des deplacement droite / gauche / bas / haut quand il y a pas de pion 
		System.out.println("____________________Test des deplacements d'un pion_________________________");
		Cases[][] test = new Cases[19][19];
		System.out.println("------------Deplacement a droite sans barriere et sans pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		Plateau plat = new Plateau (test);
		assertTrue (plat.verifDroite());
		System.out.println("------------Deplacement a droite avec barriere et sans pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][6].setCouleur(Couleur.MARRON);
		test[6][6].setCouleur(Couleur.MARRON);
		test[7][6].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		System.out.println("------------Deplacement a droite a l'exterieur-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][17].setCouleur(Couleur.BLEU);
		plat = new Plateau (test);
		assertFalse (plat.verifDroite());	
		
		System.out.println("------------Deplacement a gauche sans barriere et sans pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		plat = new Plateau (test);
		assertTrue (plat.verifGauche());
		System.out.println("------------Deplacement a gauche avec barriere et sans pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][4].setCouleur(Couleur.MARRON);
		test[6][4].setCouleur(Couleur.MARRON);
		test[7][4].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifGauche());	
		System.out.println("------------Deplacement a gauche a l'esterieur-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][1].setCouleur(Couleur.BLEU);
		plat = new Plateau (test);
		assertFalse (plat.verifGauche());
		
		System.out.println("--------------Deplacement a bas sans barriere et sans pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		plat = new Plateau (test);
		assertTrue (plat.verifBas());
		System.out.println("--------------Deplacement a bas avec barriere et sans pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[4][5].setCouleur(Couleur.MARRON);
		test[4][4].setCouleur(Couleur.MARRON);
		test[4][4].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifBas());	
		System.out.println("--------------Deplacement a bas a l'esterieur-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[17][5].setCouleur(Couleur.BLEU);
		plat = new Plateau (test);
		assertFalse (plat.verifBas());
		
		System.out.println("-------------Deplacement a haut sans barriere et sans pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		plat = new Plateau (test);
		assertTrue (plat.verifHaut());
		System.out.println("-------------Deplacement a haut avec barriere et sans pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[4][5].setCouleur(Couleur.MARRON);
		test[4][6].setCouleur(Couleur.MARRON);
		test[4][7].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifHaut());	
		System.out.println("-------------Deplacement a haut a l'exterieur-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[1][5].setCouleur(Couleur.BLEU);
		plat = new Plateau (test);
		assertFalse (plat.verifHaut());
		
		// ___________________________verif des diagonales et des sauts
		System.out.println("-------------Deplacement a haut sans barriere et pion donc diagonale droite-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[3][5].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertTrue(plat.verifDiagonaleHD());	
		System.out.println("-------------Deplacement a haut sans barriere et pion donc diagonale gauche-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[3][5].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertTrue  (plat.verifDiagonaleHG());		
		System.out.println("-------------Deplacement a haut sans barriere et pion donc saute le pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[3][5].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertTrue (plat.verifsautHaut());	
		System.out.println("-------------Deplacement a haut avec pion donc diagonale droite mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[3][5].setCouleur(Couleur.VERT);
		test[4][7].setCouleur(Couleur.MARRON);
		test[4][8].setCouleur(Couleur.MARRON);
		test[4][9].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleHD());
		System.out.println("-------------Deplacement a haut avec pion donc diagonale gauche mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[3][5].setCouleur(Couleur.VERT);
		test[4][3].setCouleur(Couleur.MARRON);
		test[4][2].setCouleur(Couleur.MARRON);
		test[4][1].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse  (plat.verifDiagonaleHG());	
		System.out.println("-------------Deplacement a haut avec pion donc saute le pion mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[3][5].setCouleur(Couleur.VERT);
		test[4][5].setCouleur(Couleur.MARRON);
		test[4][6].setCouleur(Couleur.MARRON);
		test[4][7].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifsautHaut());		
		System.out.println("-------------Deplacement a haut avec pion donc diagonale droite mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][17].setCouleur(Couleur.BLEU);
		test[3][17].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleHD());	
		System.out.println("-------------Deplacement a haut avec pion donc diagonale gauche mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][1].setCouleur(Couleur.BLEU);
		test[3][1].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleHG());	
		System.out.println("-------------Deplacement a haut avec pion donc saute le pion mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[3][5].setCouleur(Couleur.BLEU);
		test[1][5].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertFalse (plat.verifsautHaut());
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		System.out.println("-------------Deplacement a droite sans barriere et pion donc diagonale haut-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][7].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertTrue(plat.verifDiagonaleHD());	
		System.out.println("-------------Deplacement a droite sans barriere et pion donc diagonale bas-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][7].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertTrue (plat.verifDiagonaleBD());	
		System.out.println("-------------Deplacement a droite sans barriere et pion donc saute le pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][7].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertTrue  (plat.verifsautDroit());
		System.out.println("-------------Deplacement a droite avec pion donc diagonale haut mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][7].setCouleur(Couleur.VERT);
		test[1][6].setCouleur(Couleur.MARRON);
		test[2][6].setCouleur(Couleur.MARRON);
		test[3][6].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleHD());
		System.out.println("-------------Deplacement a droite avec pion donc diagonale bas mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][7].setCouleur(Couleur.VERT);
		test[7][6].setCouleur(Couleur.MARRON);
		test[8][6].setCouleur(Couleur.MARRON);
		test[9][6].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleBD());	
		System.out.println("-------------Deplacement a droite avec pion donc saute le pion mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][7].setCouleur(Couleur.VERT);
		test[3][6].setCouleur(Couleur.MARRON);
		test[4][6].setCouleur(Couleur.MARRON);
		test[5][6].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifsautDroit());
		System.out.println("-------------Deplacement a droite avec pion donc diagonale haut mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[1][5].setCouleur(Couleur.BLEU);
		test[1][7].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleHD());	
		System.out.println("-------------Deplacement a droite avec pion donc diagonale bas mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[17][5].setCouleur(Couleur.BLEU);
		test[17][7].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleBD());	
		System.out.println("-------------Deplacement a droite avec pion donc saute le pion mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][15].setCouleur(Couleur.BLEU);
		test[5][17].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertFalse (plat.verifsautDroit());
		
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		System.out.println("-------------Deplacement a gauche sans barriere et pion donc diagonale haut-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][3].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertTrue  (plat.verifDiagonaleHG());	
		System.out.println("-------------Deplacement a gauche sans barriere et pion donc diagonale bas-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][3].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertTrue (plat.verifDiagonaleBG());		
		System.out.println("-------------Deplacement a gauche sans barriere et pion donc saute le pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][3].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertTrue (plat.verifsautGauche());
		System.out.println("-------------Deplacement a gauche avec pion donc diagonale haut mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][3].setCouleur(Couleur.VERT);
		test[1][4].setCouleur(Couleur.MARRON);
		test[2][4].setCouleur(Couleur.MARRON);
		test[3][4].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleHG());	
		System.out.println("-------------Deplacement a gauche avec pion donc diagonale bas mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][3].setCouleur(Couleur.VERT);
		test[7][4].setCouleur(Couleur.MARRON);
		test[8][4].setCouleur(Couleur.MARRON);
		test[9][4].setCouleur(Couleur.MARRON);
		
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleBG());		
		System.out.println("-------------Deplacement a gauche avec pion donc saute le pion mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][3].setCouleur(Couleur.VERT);
		test[3][4].setCouleur(Couleur.MARRON);
		test[5][4].setCouleur(Couleur.MARRON);
		test[6][4].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifsautGauche());
		System.out.println("-------------Deplacement a gauche avec pion donc diagonale haut mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[17][5].setCouleur(Couleur.BLEU);
		test[17][3].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleHG());	
		System.out.println("-------------Deplacement a gauche avec pion donc diagonale bas mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[1][5].setCouleur(Couleur.BLEU);
		test[1][3].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleBG());	
		System.out.println("-------------Deplacement a gauche avec pion donc saute le pion mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][3].setCouleur(Couleur.BLEU);
		test[5][1].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertFalse (plat.verifsautGauche());
		
		System.out.println("-------------Deplacement a bas sans barriere et pion donc diagonale gauche-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[7][5].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertTrue (plat.verifDiagonaleBG());	
		System.out.println("-------------Deplacement a bas sans barriere et pion donc diagonale droite-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[7][5].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertTrue (plat.verifDiagonaleBD());	
		System.out.println("-------------Deplacement a bas sans barriere et pion donc saute le pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[7][5].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertTrue (plat.verifsautBas());	
		System.out.println("-------------Deplacement a bas avec et pion donc diagonale gauche mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[7][5].setCouleur(Couleur.VERT);
		test[6][3].setCouleur(Couleur.MARRON);
		test[6][2].setCouleur(Couleur.MARRON);
		test[6][1].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleBG());	
		System.out.println("-------------Deplacement a bas avec et pion donc diagonale droite mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[7][5].setCouleur(Couleur.VERT);
		test[6][7].setCouleur(Couleur.MARRON);
		test[6][8].setCouleur(Couleur.MARRON);
		test[6][9].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleBD());	
		System.out.println("-------------Deplacement a bas avec et pion donc saute le pion mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[7][5].setCouleur(Couleur.VERT);
		test[6][3].setCouleur(Couleur.MARRON);
		test[6][4].setCouleur(Couleur.MARRON);
		test[6][5].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifsautBas());
		System.out.println("-------------Deplacement a bas avec et pion donc diagonale gauche mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][1].setCouleur(Couleur.BLEU);
		test[7][1].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleBG());	
		System.out.println("-------------Deplacement a bas avec et pion donc diagonale droite mais  sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][17].setCouleur(Couleur.BLEU);
		test[7][17].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleBD());	
		System.out.println("-------------Deplacement a bas avec et pion donc saute le pion mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[15][5].setCouleur(Couleur.BLEU);
		test[17][5].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertFalse (plat.verifsautBas());
		
		// ___________________________verif des possibilité
		System.out.println("-------------tout est possible-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][3].setCouleur(Couleur.BLEU);
		plat = new Plateau (test);
		assertTrue (plat.verifPion());	
		System.out.println("-------------pas de possibiliter-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[5][3].setCouleur(Couleur.BLEU);
		test[3][4].setCouleur(Couleur.MARRON);
		test[4][1].setCouleur(Couleur.MARRON);
		test[4][2].setCouleur(Couleur.MARRON);
		test[4][3].setCouleur(Couleur.MARRON);
		test[4][4].setCouleur(Couleur.MARRON);
		test[5][2].setCouleur(Couleur.MARRON);
		test[5][4].setCouleur(Couleur.MARRON);
		test[6][2].setCouleur(Couleur.MARRON);
		test[6][3].setCouleur(Couleur.MARRON);
		test[6][4].setCouleur(Couleur.MARRON);
		test[6][5].setCouleur(Couleur.MARRON);
		test[7][2].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifPion());		
	}
	
	/**
	 * les test pour placer une barriere
	 */
	
	public void testDeplacerBarriere() {
		System.out.println("____________________Test deplacement barriere_____________________________________");
		Cases[][] test = new Cases[19][19];
		Plateau plat = new Plateau (test);
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		//__________________________ test parcours_______________________
		System.out.println("------------cas avec un passege normal---------------------");
		plat = new Plateau (test);
		assertTrue (plat.parcours());
		System.out.println("------------cas sans possibiliter---------------------");
		test = new Cases[19][19];
		plat = new Plateau (test);
		assertFalse (plat.parcours());

		System.out.println("------------cas avec possibiliter mais obliger de faire demi tour---------------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		
		plat = new Plateau (test);
		assertTrue (plat.parcours());
		
		//__________________________test deplacer barriere
		System.out.println("-------------possede des barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		plat = new Plateau (test);
		assertTrue (plat.verifBarriere());	
		System.out.println("-------------n'a pas de barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		plat = new Plateau (test);
		assertFalse (plat.verifBarriere());
		
		System.out.println("-------------posse une barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		plat = new Plateau (test);
		assertTrue (plat.verifBarriere());	
		System.out.println("-------------pose une barriere sur une autre-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		test[3][4].setCouleur(Couleur.MARRON);
		test[4][1].setCouleur(Couleur.MARRON);
		test[4][2].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifBarriere());
		System.out.println("-------------cordonée x y valide horizontale-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		plat = new Plateau (test);
		assertTrue (plat.verifBarriere());	
		System.out.println("-------------cordonné x invalide (sort coter 0 (x<0) ) et y OK horizontale-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		plat = new Plateau (test);
		assertFalse (plat.verifBarriere());
		System.out.println("-------------cordonné x invalide (sort coter 18 (y>18)) et y OK horizontale-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		plat = new Plateau (test);
		assertFalse (plat.verifBarriere());
		System.out.println("-------------cordonné y invalide (sort coter 0 ) et x OK horizontale-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		plat = new Plateau (test);
		assertFalse (plat.verifBarriere());
		System.out.println("-------------cordonné y invalide (sort coter 18  (y>14)) et x OK horizontale-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		plat = new Plateau (test);
		assertFalse (plat.verifBarriere());
		System.out.println("-------------cordonée x y valide verticale-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		plat = new Plateau (test);
		assertTrue (plat.verifBarriere());
		System.out.println("-------------cordonné x invalide (sort coter 0 (x<4) ) et y OK verticale-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		plat = new Plateau (test);
		assertFalse (plat.verifBarriere());
		System.out.println("-------------cordonné x invalide (sort coter 18 (x>19) et y OK verticale-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		plat = new Plateau (test);
		assertFalse (plat.verifBarriere());
		System.out.println("-------------cordonné y invalide (sort coter 0 (y<0) ) et x OK verticale-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		plat = new Plateau (test);
		assertFalse (plat.verifBarriere());
		System.out.println("-------------cordonné y invalide (sort coter 18 (y>18) ) et x OK verticale-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases(Couleur.LIBRE);
			}
		}
		plat = new Plateau (test);
		assertFalse (plat.verifBarriere());
	}

}
