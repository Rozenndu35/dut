/**
 * les differents modes du jeux
 * @author rozenn/Klervia
 * @version1
 */
package Modele;

public enum Mode {
	OH,
	HH,
	HOOO,
	HHOO,
	HHHO,
	HHHH
}