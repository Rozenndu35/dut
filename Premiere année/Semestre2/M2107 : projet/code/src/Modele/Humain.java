/**
 * classe pour les actions du joueur humain
 * @author rozenn/Klervia
 * @version1
 */
package Modele;

public class Humain extends Joueur {

	/**
	 * constructeur de la classe humain
	 * @param nom
	 * @param numero
	 * @param nbJouer
	 */
	public Humain(String nom, int numero, int nbJouer) {
		super(nom, numero, nbJouer);
		// TODO Auto-generated constructor stub
	}

	/**
	 * methode qui regroupe tous les choix possibles du joueur
	 */
	public void choix() {
		// TODO - implement Humain.choix
		throw new UnsupportedOperationException();
	}

	/**
	 * methode qui permet au joueur de deplacer son pion
	 */
	public void deplacePion() {
		// TODO - implement Humain.deplacePion
		throw new UnsupportedOperationException();
	}

	/**
	 * methode qui permet au joueur de placer une barriere
	 */
	public void placerBarriere() {
		// TODO - implement Humain.placerBarriere
		throw new UnsupportedOperationException();
	}

}