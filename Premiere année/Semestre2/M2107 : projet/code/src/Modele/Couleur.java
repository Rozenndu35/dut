/**
 * les differentes couleurs des cases
 * @author rozenn/Klervia
 * @version1
 */
package Modele;

public enum Couleur {
	BLEU,
	VERT,
	ROUGE,
	NOIR,
	MARRON,
	LIBRE;
}