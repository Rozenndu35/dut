/**
 * classe pour la partie
 * @author rozenn/Klervia
 * @version1
 */

package Modele;

import java.util.*;

public class Partie {

	private Joueur joueur1;
	private Joueur joueur2;
	private Joueur joueur3;
	private Joueur joueur4;
	private Plateau plateau;
	private Mode mode;

	/**
	 * Constructeur de la classe partie
	 * @param mode : le mode de jeux
	 */
	public Partie(Mode mode) {
		// TODO - implement Partie.Partie
		throw new UnsupportedOperationException();
	}
	/**
	 * methode qui fait jouer les joueurs a tour de role
	 */

	public void faireJouer() {
		// TODO - implement Partie.faireJouer
		throw new UnsupportedOperationException();
	}
	/**
	 * Methode qui arrete la partie avec 2 joueurs
	 */
	public void finAvecJ2() {
		// TODO - implement Partie.finAvecJ2
		throw new UnsupportedOperationException();
	}
	/**
	 * Methode qui arrete la partie avec 4 joueurs
	 */
	public void finAvecJ4() {
		// TODO - implement Partie.finAvecJ4
		throw new UnsupportedOperationException();
	}
	/**
	 * Methode qui permet d enregistrer une partie en cours
	 */
	public void enregistrerPartie() {
		// TODO - implement Partie.enregistrerPartie
		throw new UnsupportedOperationException();
	}
	/**
	 * Methode qui permet de charger une partie precedemment enregistree
	 */
	public void chargerPartie() {
		// TODO - implement Partie.chargerPartie
		throw new UnsupportedOperationException();
	}

}