/**
 * classe abstraite pour les caracteristiques des joueurs
 * @author rozenn/Klervia
 * @version1
 */

package Modele;

import java.util.*;

public abstract class Joueur {

	private ArrayList<Barriere> stockBarriere;
	private Pion monPion;
	private String nom;
	private int numero;

	/**
	 * Constructeur de la classe joueur
	 * @param nom : son nom
	 * @param numero : son numero
	 * @param nbJouer : le nombre de joueur
	 */
	public Joueur(String nom, int numero, int nbJouer) {
		// TODO - implement Joueur.Joueur
		throw new UnsupportedOperationException();
	}
	/**
	 * Methode qui permet d obtenir de nom du joueur
	 * @return nom : le nom du joueur
	 */
	
	public String getNom() {
		return this.nom;
	}

	/**
	 * Methode qui en leve une barriere de ca liste de stockage
	 */
	public void moinsBarriere() {
		// TODO - implement Joueur.moinsBarriere
		throw new UnsupportedOperationException();
	}

	

}