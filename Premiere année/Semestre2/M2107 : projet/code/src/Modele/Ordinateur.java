/**
 * classe pour les actions de l'ordinateur (IA)
 * @author rozenn/Klervia
 * @version1
 */

package Modele;

public class Ordinateur extends Joueur {
	private Difficulte dif;
	
	/**
	 * Constructeur de la classe ordinateur
	 * @param dif : la difficulter
	 * @param num : son numero
	 * @param nbJ : le nombre de joueur
	 * @param nom : son nom
	 */
	public Ordinateur(Difficulte dif, int num , int nbJ, String nom) {
		super (nom , num, nbJ);
		this.dif = dif;
	}
	
	/**
	 * Methode qui regroupe tous les choix de l ordinateur
	 */
	public void choix() {
		// TODO - implement Ordinateur.choix
		throw new UnsupportedOperationException();
	}

	/**
	 * Methode qui permet a l ordinateur de deplacer son pion
	 */
	public void deplacerPion() {
		// TODO - implement Ordinateur.deplacerPion
		throw new UnsupportedOperationException();
	}

	/**
	 * Methode qui permet a l ordinateur de placer une barriere
	 */
	public void placerBarriere() {
		// TODO - implement Ordinateur.placerBarriere
		throw new UnsupportedOperationException();
	}

}