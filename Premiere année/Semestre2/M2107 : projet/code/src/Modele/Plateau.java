/**
 * classe pour le plateau de jeux et ces actions
 * @author rozenn/Klervia
 * @version1
 */

package Modele;

import java.util.*;

public class Plateau {

	private Cases[][] plateauJeux;
	private int taille;
	private HashMap listX;
	private HashMap listY;
	private HashMap attribute;
	private HashMap attribute2;

	/**
	 * le constructeur pour les tests
	 * @param plateauTest : le plateau qui permet les tests
	 */
	public Plateau (Cases[][] plateauTest) {
		
	}
	/**
	 * methode qui verifie si c est un pion en face
	 * @return true si c est un pion
	 */
	public boolean verifPion() {
		// TODO - implement Plateau.verifPion
		throw new UnsupportedOperationException();
	}
	/**
	 * methode qui verifie si a droite c est libre
	 * @return true si bon
	 */
	public boolean verifDroite() {
		// TODO - implement Plateau.verifDroite
		throw new UnsupportedOperationException();
	}
	/**
	 * methode qui verifie si a gauche c est libre
	 * @return true si bon
	 */
	public boolean verifGauche() {
		// TODO - implement Plateau.verifGauche
		throw new UnsupportedOperationException();
	}
	/**
	 * methode qui verifie si en bas c est libre
	 * @return true si bon
	 */
	public boolean verifBas() {
		// TODO - implement Plateau.verifBas
		throw new UnsupportedOperationException();
	}
	/**
	 * methode qui verifie si en haut c est libre
	 * @return true si bon
	 */
	public boolean verifHaut() {
		// TODO - implement Plateau.verifHaut
		throw new UnsupportedOperationException();
	}
	/**
	 * methode qui verifie si en diagonal en haut a droite c est libre
	 * @return true si bon
	 */
	public boolean verifDiagonaleHD() {
		// TODO - implement Plateau.verifDiagonaleHD
		throw new UnsupportedOperationException();
	}
	/**
	 * methode qui verifie si en diagonal en haut a gauche c est libre
	 * @return true si bon
	 */
	public boolean verifDiagonaleHG() {
		// TODO - implement Plateau.verifDiagonaleHG
		throw new UnsupportedOperationException();
	}
	/**
	 * methode qui verifie si en diagonal en bas a gauche c est libre
	 * @return true si bon
	 */
	public boolean verifDiagonaleBG() {
		// TODO - implement Plateau.verifDiagonaleBG
		throw new UnsupportedOperationException();
	}
	/**
	 * methode qui verifie si en diagonal en bas a droite c est libre
	 * @return true si bon
	 */
	public boolean verifDiagonaleBD() {
		// TODO - implement Plateau.verifDiagonaleBD
		throw new UnsupportedOperationException();
	}
	/**
	 * methode qui verifie si le saut en haut est possible 
	 * @return true si bon
	 */
	public boolean verifsautHaut() {
		// TODO - implement Plateau.verifsautHaut
		throw new UnsupportedOperationException();
	}
	/**
	 * methode qui verifie si le saut en bas est possible 
	 * @return true si bon
	 */

	public boolean verifsautBas() {
		// TODO - implement Plateau.verifsautBas
		throw new UnsupportedOperationException();
	}
	/**
	 * methode qui verifie si le saut a droite est possible 
	 * @return true si bon
	 */
	public boolean verifsautDroit() {
		// TODO - implement Plateau.verifsautDroit
		throw new UnsupportedOperationException();
	}
	/**
	 * methode qui verifie si le saut a gauche est possible 
	 * @return true si bon
	 */
	public boolean verifsautGauche() {
		// TODO - implement Plateau.verifsautGauche
		throw new UnsupportedOperationException();
	}
	/**
	 * methode qui verifie si les barrieres sont correctes
	 * @return true si bon
	 */
	public boolean verifBarriere() {
		// TODO - implement Plateau.veriBarriiere
		throw new UnsupportedOperationException();
	}
	/**
	 * ??
	 */
	public void operation() {
		// TODO - implement Plateau.operation
		throw new UnsupportedOperationException();
	}
	/**
	 * Verifie qu il y a toujours un parcours possible
	 * @return true si c est bon
	 */
	public boolean parcours() {
		// TODO - implement Plateau.parcours
		throw new UnsupportedOperationException();
	}


}