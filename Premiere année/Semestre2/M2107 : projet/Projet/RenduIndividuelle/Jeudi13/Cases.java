/**
 * classe pour les cases du tableau
 * @author rozenn/Klervia
 * @version1
 */
package ModeleConsole;

public class Cases {

	private Couleur maCouleur;

	/**
	 * retourne sa couleur
	 * @return la couleur
	 */
	public Couleur getCouleur() {
		return this.maCouleur;
	}

	/**
	 * change sa couleur
	 * @param newCouleur : la nouvelle couleur
	 */
	public void setCouleur(Couleur newCouleur) {
		this.maCouleur = newCouleur;
	}
	/**
	 * pour afficher les cases
	 * @return la valeur de la case en string
	 */
	public String toString(){
		String ret ="";
		if (getCouleur() == Couleur.BLEU) {
			ret= "B";
		}
		if (getCouleur() == Couleur.VERT) {
			ret= "V";
		}
		if (getCouleur() == Couleur.ROUGE) {
			ret= "R";
		}
		if (getCouleur() == Couleur.NOIR) {
			ret= "N";
		}
		if (getCouleur() == Couleur.MARRON) {
			ret= "M";
		}
		if (getCouleur() == Couleur.LIBREB) {
			ret= ".";// -------------------------------------------------------------A trouver
		}
		if (getCouleur() == Couleur.LIBREP) {
			ret= "-";// -------------------------------------------------------------A trouver
		}
		return ret;
	}
	
}
