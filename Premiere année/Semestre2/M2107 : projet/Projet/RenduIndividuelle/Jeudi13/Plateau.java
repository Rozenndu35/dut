/**
 * classe pour le plateau de jeux et ces actions
 * @author rozenn/Klervia
 * @version1
 */

package ModeleConsole;

import java.util.*;

public class Plateau {

	private Cases[][] plateauJeux;
	private int taille;
	private HashMap listX;
	private HashMap listY;

	/**
	 * le constructeur pour les tests
	 * @param plateauTest : le plateau qui permet les tests
	 */
	public Plateau (Cases[][] plateauTest) {
		this.plateauJeux = plateauTest;
		listX = new HashMap<String, Integer>();
		listY = new HashMap<String, Integer>();
		
	}
	/**
	 * le constructeur 
	 */
	public Plateau (int nbJoueur) {
		this.plateauJeux = new Cases[19][19];
		for (int i = 0; i< 19; i++) {
			for (int j = 0; j< 19; j++) {
				this.plateauJeux[i][j]= new Cases();
				if( (i%2 == 0 ) || (j%2 == 0)) {
					this.plateauJeux[i][j].setCouleur(Couleur.LIBREB);
				}
				else {
					this.plateauJeux[i][j].setCouleur(Couleur.LIBREP);
				}
			}
		}
		if (nbJoueur == 2) {
			this.plateauJeux[1][9].setCouleur(Couleur.ROUGE);
			this.plateauJeux[17][9].setCouleur(Couleur.NOIR);
		}
		else {
			this.plateauJeux[1][9].setCouleur(Couleur.ROUGE);
			this.plateauJeux[17][9].setCouleur(Couleur.NOIR);
			this.plateauJeux[9][1].setCouleur(Couleur.BLEU);
			this.plateauJeux[9][17].setCouleur(Couleur.VERT);
		}
		this.listX = new HashMap<String, Integer>();
		this.listY = new HashMap<String, Integer>();
	}
	/**
	 * methode qui verifie si c est un pion en face
	 * @return true si c est un pion
	 */
	public boolean verifPion(int x , int y) {
		boolean ret = false;
		listX.clear();
		listY.clear();
		if ( x > 0 && x < 18 && y > 0 && y < 18) {
			boolean d = verifDroite(x, y);
			boolean g = verifGauche(x, y);
			boolean b =verifBas(x, y);
			boolean h =verifHaut(x,y);	
			if (d || g || b || h) {
				
				ret = true;
			}
		}
		return ret;
	}
	/**
	 * methode qui verifie si a droite c est libre
	 * @param y 
	 * @param x 
	 * @return true si bon
	 */
	public boolean verifDroite(int x, int y) {
		boolean ret = false;
		int xpion = x;
		int ypion = y;
		int xou = x;
		int you = y+2;
		int xb = x;
		int yb = y+1;
		if ( xou > 0 && xou < 18 && you > 0 && you < 18 ) {
			if (plateauJeux[xb][yb].getCouleur()== Couleur.MARRON) {
				ret = false;
			}
			else if (plateauJeux[xou][you].getCouleur()== Couleur.LIBREP) {
				ret = true;
				int cle = listX.size();
				listX.put( cle , xou );
				listY.put( cle , you );
			}
			else {
				String d = verifsautDroit(xpion , ypion);
				if (d.equals("barriere") || d.equals("pion") ) {
					boolean hD = verifDiagonaleHD(xpion , ypion ,true );
					boolean bD = verifDiagonaleBD(xpion , ypion , true);
				}
				
			}
		}
		return ret;
	}
	/**
	 * methode qui verifie si a gauche c est libre
	 * @param y 
	 * @param x 
	 * @return true si bon
	 */
	public boolean verifGauche(int x, int y) {
		boolean ret = false;
		int xpion = x;
		int ypion = y;
		int xou = x;
		int you = y-2;
		int xb = x;
		int yb = y-1;
		if ( xou > 0 && xou < 18 && you > 0 && you < 18 ) {
			if (plateauJeux[xb][yb].getCouleur()== Couleur.MARRON) {
				ret = false;
			}
			else if (plateauJeux[xou][you].getCouleur()== Couleur.LIBREP) {
				ret = true;
				int cle = listX.size();
				listX.put( cle , xou );
				listY.put( cle , you );
			}
			else {
				String g = verifsautGauche(xpion , ypion);
				if (g.equals("barriere") || g.equals("pion") ) {
					boolean hG = verifDiagonaleHG(xpion , ypion, true);
					boolean bG = verifDiagonaleBG(xpion , ypion , true);
				}
			}
		}
		return ret;
	}
	/**
	 * methode qui verifie si en bas c est libre
	 * @param y 
	 * @param x 
	 * @return true si bon
	 */
	public boolean verifBas(int x, int y) {
		boolean ret = false;
		int xpion = x;
		int ypion = y;
		int xou = x+2;
		int you = y;
		int xb = x+1;
		int yb = y;
		if ( xou > 0 && xou < 18 && you > 0 && you < 18 ) {
			if (plateauJeux[xb][yb].getCouleur()== Couleur.MARRON) {
				ret = false;
			}
			else if (plateauJeux[xou][you].getCouleur()== Couleur.LIBREP) {
				ret = true;
				int cle = listX.size();
				listX.put( cle , xou );
				listY.put( cle , you );
			}
			else {
				String b = verifsautBas(xpion , ypion);
				if (b.equals("barriere") || b.equals("pion") ) {
					boolean bD = verifDiagonaleBD(xpion , ypion , false);
					boolean bG = verifDiagonaleBG(xpion , ypion , false);
				}
			}
		}
		return ret;
	}
	/**
	 * methode qui verifie si en haut c est libre
	 * @param y 
	 * @param x 
	 * @return true si bon
	 */
	public boolean verifHaut(int x, int y) {
		boolean ret = false;
		int xpion = x;
		int ypion = y;
		int xou = x-2;
		int you = y;
		int xb = x-1;
		int yb = y;
		if ( xou > 0 && xou < 18 && you > 0 && you < 18 ) {
			if (plateauJeux[xb][yb].getCouleur()== Couleur.MARRON) {
				ret = false;
			}
			else if (plateauJeux[xou][you].getCouleur()== Couleur.LIBREP) {
				ret = true;
				int cle = listX.size();
				listX.put( cle , xou );
				listY.put( cle , you );
			}
			else {
				String h = verifsautHaut(xpion , ypion);
				if (h.equals("barriere") || h.equals("pion") ) {
					boolean hD = verifDiagonaleHD(xpion , ypion , false);
					boolean hG = verifDiagonaleHG(xpion , ypion , false);
				}
			}
		}
		return ret;
	}
	/**
	 * methode qui verifie si en diagonal en haut a droite c est libre
	 * @return true si bon
	 */
	public boolean verifDiagonaleHD(int x , int y, boolean cote) {
		boolean ret = false;
		int xpion = x;
		int ypion = y;
		int xou = x-2;
		int you = y+2;
		int xb = x-1;
		int yb = y+2;
		if (cote) {
			xb = x-2;
			yb = y+1;
		}
		if ( xou > 0 && xou < 18 && you > 0 && you < 18 ) {
			if (plateauJeux[xb][yb].getCouleur()== Couleur.MARRON) {
				ret = false;
			}
			else if (plateauJeux[xou][you].getCouleur()== Couleur.LIBREP) {
				ret = true;
				int cle = listX.size();
				listX.put( cle , xou );
				listY.put( cle , you );
			}
		}
		return ret;
	}
	/**
	 * methode qui verifie si en diagonal en haut a gauche c est libre
	 * @return true si bon
	 */
	public boolean verifDiagonaleHG(int x , int y , boolean cote) {
		boolean ret = false;
		int xpion = x;
		int ypion = y;
		int xou = x-2;
		int you = y-2;
		int xb = x-1;
		int yb = y-2;
		if(cote) {
			xb = x-2;
			yb = y-1;
		}
		if ( xou > 0 && xou < 18 && you > 0 && you < 18 ) {
			if (plateauJeux[xb][yb].getCouleur()== Couleur.MARRON) {
				ret = false;
			}
			else if (plateauJeux[xou][you].getCouleur()== Couleur.LIBREP) {
				ret = true;
				int cle = listX.size();
				listX.put( cle , xou );
				listY.put( cle , you );
			}
		}
		return ret;
	}
	/**
	 * methode qui verifie si en diagonal en bas a gauche c est libre
	 * @return true si bon
	 */
	public boolean verifDiagonaleBG(int x , int y, boolean cote) {
		boolean ret = false;
		int xpion = x;
		int ypion = y;
		int xou = x-2;
		int you = y-2;
		int xb = x+1;
		int yb = y-2;
		if (cote) {
			xb = x+2;
			yb = y-1;
		}
		if ( xou > 0 && xou < 18 && you > 0 && you < 18 ) {
			if (plateauJeux[xb][yb].getCouleur()== Couleur.MARRON) {
				ret = false;
			}
			else if (plateauJeux[xou][you].getCouleur()== Couleur.LIBREP) {
				ret = true;
				int cle = listX.size();
				listX.put( cle , xou );
				listY.put( cle , you );
			}
		}
		return ret;
	}
	/**
	 * methode qui verifie si en diagonal en bas a droite c est libre
	 * @return true si bon
	 */
	public boolean verifDiagonaleBD(int x , int y, boolean cote) {
		boolean ret = false;
		int xpion = x;
		int ypion = y;
		int xou = x+2;
		int you = y+2;
		int xb = x+1;
		int yb = y+2;
		if(cote) {
			xb = x+2;
			yb = y+1;
		}
		if ( xou > 0 && xou < 18 && you > 0 && you < 18 ) {
			if (plateauJeux[xb][yb].getCouleur()== Couleur.MARRON) {
				ret = false;
			}
			else if (plateauJeux[xou][you].getCouleur()== Couleur.LIBREP) {
				ret = true;
				int cle = listX.size();
				listX.put( cle , xou );
				listY.put( cle , you );
			}
		}
		return ret;
	}
	/**
	 * methode qui verifie si le saut en haut est possible 
	 * @return true si bon
	 */
	public String verifsautHaut(int x , int y) {
		String ret = "sort";
		int xpion = x;
		int ypion = y;
		int xou = x-4;
		int you = y;
		int xb = x-3;
		int yb = y;
		if ( xou > 0 && xou < 18 && you > 0 && you < 18 ) {
			if (plateauJeux[xb][yb].getCouleur()== Couleur.MARRON) {
				ret = "barriere";
			}
			else if (plateauJeux[xou][you].getCouleur()== Couleur.LIBREP) {
				ret = "marche";
				int cle = listX.size();
				listX.put( cle , xou );
				listY.put( cle , you );
			}
			else {
				ret = "pion";
			}
		}
		return ret;
	}
	/**
	 * methode qui verifie si le saut en bas est possible 
	 * @return true si bon
	 */

	public String verifsautBas(int x , int y) {
		String ret = "sort";
		int xpion = x;
		int ypion = y;
		int xou = x+4;
		int you = y;
		int xb = x+ 3;
		int yb = y;
		if ( xou > 0 && xou < 18 && you > 0 && you < 18 ) {
			if (plateauJeux[xb][yb].getCouleur()== Couleur.MARRON) {
				ret = "barriere";
			}
			else if (plateauJeux[xou][you].getCouleur()== Couleur.LIBREP) {
				ret = "marche";
				int cle = listX.size();
				listX.put( cle , xou );
				listY.put( cle , you );
			}
			else {
				ret = "pion";
			}
		}
		return ret;
	}
	/**
	 * methode qui verifie si le saut a droite est possible 
	 * @return true si bon
	 */
	public String verifsautDroit(int x , int y) {
		String ret = "sort";
		int xpion = x;
		int ypion = y;
		int xou = x;
		int you = y+4;
		int xb = x;
		int yb = y+3;
		if ( xou > 0 && xou < 18 && you > 0 && you < 18 ) {
			if (plateauJeux[xb][yb].getCouleur()== Couleur.MARRON) {
				ret = "barriere";
				
			}
			else if (plateauJeux[xou][you].getCouleur()== Couleur.LIBREP) {
				ret = "marche";
				int cle = listX.size();
				listX.put( cle , xou );
				listY.put( cle , you );
			}
			else {
				ret = "pion";
			}
		}
		return ret;
	}
	/**
	 * methode qui verifie si le saut a gauche est possible 
	 * @return true si bon
	 */
	public String verifsautGauche(int x , int y) {
		String ret = "sort";
		int xpion = x;
		int ypion = y;
		int xou = x;
		int you = y-4;
		int xb = x;
		int yb = y-3;
		if ( xou > 0 && xou < 18 && you > 0 && you < 18 ) {
			if (plateauJeux[xb][yb].getCouleur()== Couleur.MARRON) {
				ret = "barriere";
			}
			else if (plateauJeux[xou][you].getCouleur()== Couleur.LIBREP) {
				ret = "marche";
				int cle = listX.size();
				listX.put( cle , xou );
				listY.put( cle , you );
			}
			else {
				ret = "pion";
			}
		}
		return ret;
	}
	/**
	 * methode qui verifie si les barrieres sont correctes
	 * @return true si c est bon
	 */
	public boolean verifBarriere(int x , int y) {
		boolean ret = false;
		System.out.println("Voila ce qu'il faut faire je pense, possiblement ajouter un tableau contenant toutes les coordonnees des barrieres");
		//verifie si les coordonnes rentrees ne sont pas sur une barriere
		
			// verifie si il existe la place suffisante pour placer une barriere
		
				//verifie si il existe un parcours pour tous les pions
				if(parcours()){
					ret=true;
				}
		return ret;
	}
	
	/**
	 * Verifie qu il y a toujours un parcours possible
	 * @return true si c est bon
	 */
	public boolean parcours() {
		return false;
		
	}

	public String toString() {
		String ret ="";
		for (int a = 0 ; a<19 ; a++) {
			for (int aC = 0 ; aC <19; aC++ ) {
			   ret += this.plateauJeux[a][aC].toString();
			}
			ret += "\n";
		}
		return ret;
		
	}

}
