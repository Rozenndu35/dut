/**
 * classe pour le pion d'un joueur
 * @author rozenn/Klervia
 * @version1
 */

package ModeleConsole;

public class Pion {

	private Couleur maCouleur;

	/**
	 * Constructeur de la classe pion
	 * @param maCouleur : la couleur du pion (du joueur)
	 */
	public Pion(Couleur maCouleur) {
		this.maCouleur = maCouleur;
	}

}
