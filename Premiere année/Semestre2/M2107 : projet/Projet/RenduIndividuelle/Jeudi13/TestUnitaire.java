/**
 * classe pour les tests de l'application
 * @author rozenn/Klervia
 * @version1
 */

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import ModeleConsole.Cases;
import ModeleConsole.Couleur;
import ModeleConsole.Humain;
import ModeleConsole.Joueur;
import ModeleConsole.Plateau;

class TestUnitaire {
	

	@Test
	void test() {
		testJoueur();
		testDeplacerPion();
		//testDeplacerBarriere();
		
		
	}
	private void testJoueur() {
		System.out.println("____________________Test des methode de joueur_________________________");
		System.out.println("------------getNom quand nom existe-----------------");
		Joueur joueur = new Humain("test", 1, 2);
		assertEquals(joueur.getNom(), "test");
		System.out.println("------------getNom quand nom existe pas-----------------");
		joueur = new Humain(null, 1, 2);
		assertEquals(joueur.getNom(), "");
		System.out.println("------------getNbBarriere avec deux joueur-----------------");
		joueur = new Humain("test", 1, 2);
		assertEquals(joueur.getNbBarriere(), 10);
		System.out.println("------------getNbBarriere avec quatre joueur-----------------");
		joueur = new Humain("test", 1, 4);
		assertEquals (joueur.getNbBarriere(), 5);
		System.out.println("------------enlever barriere avec quatre joueur-----------------");
		joueur = new Humain("test", 1, 4);
		assertTrue(joueur.moinsBarriere());
		assertEquals(joueur.getNbBarriere(), 4);
		assertTrue(joueur.moinsBarriere());
		assertTrue(joueur.moinsBarriere());
		assertTrue(joueur.moinsBarriere());		
		System.out.println("------------enlever barriere avec plus qu'une-----------------");
		assertTrue(joueur.moinsBarriere());
		assertEquals(joueur.getNbBarriere(), 0);
		System.out.println("------------enlever barriere avec aucune-----------------");
		assertFalse(joueur.moinsBarriere());
		assertEquals (joueur.getNbBarriere(), 0);
		
	}
	/**
	 * les tests pour deplacer un pion
	 */
	public void testDeplacerPion() {
		// verif des deplacement droite / gauche / bas / haut quand il y a pas de pion 
		System.out.println("____________________Test des deplacements d'un pion_________________________");
		Cases[][] test = new Cases[19][19];
		System.out.println("------------Deplacement a droite sans barriere et sans pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		Plateau plat = new Plateau (test);
		assertTrue (plat.verifDroite(5,5));
		System.out.println("------------Deplacement a droite avec barriere et sans pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][6].setCouleur(Couleur.MARRON);
		test[6][6].setCouleur(Couleur.MARRON);
		test[7][6].setCouleur(Couleur.MARRON);
		assertFalse (plat.verifDroite(5,5));
		System.out.println("------------Deplacement a droite a l'exterieur-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][17].setCouleur(Couleur.BLEU);
		plat = new Plateau (test);
		assertFalse (plat.verifDroite(5,17));	
		
		System.out.println("------------Deplacement a gauche sans barriere et sans pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		plat = new Plateau (test);
		assertTrue (plat.verifGauche(5,5));
		System.out.println("------------Deplacement a gauche avec barriere et sans pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][4].setCouleur(Couleur.MARRON);
		test[6][4].setCouleur(Couleur.MARRON);
		test[7][4].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifGauche(5,5));	
		System.out.println("------------Deplacement a gauche a l'esterieur-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][1].setCouleur(Couleur.BLEU);
		plat = new Plateau (test);
		assertFalse (plat.verifGauche(5,1));
		
		System.out.println("--------------Deplacement a bas sans barriere et sans pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		plat = new Plateau (test);
		assertTrue (plat.verifBas(5,5));
		System.out.println("--------------Deplacement a bas avec barriere et sans pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[6][5].setCouleur(Couleur.MARRON);
		test[6][4].setCouleur(Couleur.MARRON);
		test[6][4].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifBas(5,5));	
		System.out.println("--------------Deplacement a bas a l'esterieur-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[17][5].setCouleur(Couleur.BLEU);
		plat = new Plateau (test);
		assertFalse (plat.verifBas(17,5));
		
		System.out.println("-------------Deplacement a haut sans barriere et sans pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		plat = new Plateau (test);
		assertTrue (plat.verifHaut(5,5));
		System.out.println("-------------Deplacement a haut avec barriere et sans pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[4][5].setCouleur(Couleur.MARRON);
		test[4][6].setCouleur(Couleur.MARRON);
		test[4][7].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifHaut(5,5));	
		System.out.println("-------------Deplacement a haut a l'exterieur-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[1][5].setCouleur(Couleur.BLEU);
		plat = new Plateau (test);
		assertFalse (plat.verifHaut(1,5));
		
		// ___________________________verif des diagonales et des sauts
		System.out.println("-------------Deplacement a haut sans barriere et pion donc diagonale droite-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[3][5].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertTrue(plat.verifDiagonaleHD(5,5, false));	
		System.out.println("-------------Deplacement a haut sans barriere et pion donc diagonale gauche-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[3][5].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertTrue  (plat.verifDiagonaleHG(5,5, false));		
		System.out.println("-------------Deplacement a haut sans barriere et pion donc saute le pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[3][5].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertEquals (plat.verifsautHaut(5,5),"marche");	
		System.out.println("-------------Deplacement a haut avec pion donc diagonale droite mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[3][5].setCouleur(Couleur.VERT);
		test[4][7].setCouleur(Couleur.MARRON);
		test[4][8].setCouleur(Couleur.MARRON);
		test[4][9].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleHD(5,5, false));
		System.out.println("-------------Deplacement a haut avec pion donc diagonale gauche mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[3][5].setCouleur(Couleur.VERT);
		test[4][3].setCouleur(Couleur.MARRON);
		test[4][2].setCouleur(Couleur.MARRON);
		test[4][1].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse  (plat.verifDiagonaleHG(5,5, false));	
		System.out.println("-------------Deplacement a haut avec pion donc saute le pion mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[3][5].setCouleur(Couleur.VERT);
		test[2][5].setCouleur(Couleur.MARRON);
		test[2][6].setCouleur(Couleur.MARRON);
		test[2][7].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertEquals (plat.verifsautHaut(5,5), "barriere");		
		System.out.println("-------------Deplacement a haut avec pion donc saute le pion mais pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[3][5].setCouleur(Couleur.VERT);
		test[1][5].setCouleur(Couleur.ROUGE);
		plat = new Plateau (test);
		assertEquals (plat.verifsautHaut(5,5), "pion");	
		System.out.println("-------------Deplacement a haut avec pion donc diagonale droite mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][17].setCouleur(Couleur.BLEU);
		test[3][17].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleHD(5,17, false));	
		System.out.println("-------------Deplacement a haut avec pion donc diagonale gauche mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][1].setCouleur(Couleur.BLEU);
		test[3][1].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleHG(5,1, false));	
		System.out.println("-------------Deplacement a haut avec pion donc saute le pion mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[3][5].setCouleur(Couleur.BLEU);
		test[1][5].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertEquals (plat.verifsautHaut(3,5),"sort");
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		System.out.println("-------------Deplacement a droite sans barriere et pion donc diagonale haut-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][7].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertTrue(plat.verifDiagonaleHD(5,5, true));	
		System.out.println("-------------Deplacement a droite sans barriere et pion donc diagonale bas-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][7].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertTrue (plat.verifDiagonaleBD(5,5, true));	
		System.out.println("-------------Deplacement a droite sans barriere et pion donc saute le pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][7].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertEquals (plat.verifsautDroit(5,5),"marche");
		System.out.println("-------------Deplacement a droite avec pion donc diagonale haut mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][7].setCouleur(Couleur.VERT);
		test[1][6].setCouleur(Couleur.MARRON);
		test[2][6].setCouleur(Couleur.MARRON);
		test[3][6].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleHD(5,5, true));
		System.out.println("-------------Deplacement a droite avec pion donc diagonale bas mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][7].setCouleur(Couleur.VERT);
		test[7][6].setCouleur(Couleur.MARRON);
		test[8][6].setCouleur(Couleur.MARRON);
		test[9][6].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleBD(5,5, true));	
		System.out.println("-------------Deplacement a droite avec pion donc saute le pion mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][7].setCouleur(Couleur.VERT);
		test[5][8].setCouleur(Couleur.MARRON);
		test[6][8].setCouleur(Couleur.MARRON);
		test[7][8].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertEquals (plat.verifsautDroit(5,5), "barriere");
		System.out.println("-------------Deplacement a droite avec pion donc saute le pion mais pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][7].setCouleur(Couleur.VERT);
		test[5][9].setCouleur(Couleur.ROUGE);
		plat = new Plateau (test);
		assertEquals (plat.verifsautDroit(5,5), "pion");
		System.out.println("-------------Deplacement a droite avec pion donc diagonale haut mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[1][5].setCouleur(Couleur.BLEU);
		test[1][7].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleHD(1,5, true));	
		System.out.println("-------------Deplacement a droite avec pion donc diagonale bas mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[17][5].setCouleur(Couleur.BLEU);
		test[17][7].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleBD(17,5, true));	
		System.out.println("-------------Deplacement a droite avec pion donc saute le pion mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][15].setCouleur(Couleur.BLEU);
		test[5][17].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertEquals (plat.verifsautDroit(5,15),"sort");
		
		//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		System.out.println("-------------Deplacement a gauche sans barriere et pion donc diagonale haut-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][3].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertTrue  (plat.verifDiagonaleHG(5,5, true));	
		System.out.println("-------------Deplacement a gauche sans barriere et pion donc diagonale bas-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][3].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertTrue (plat.verifDiagonaleBG(5,5, true));		
		System.out.println("-------------Deplacement a gauche sans barriere et pion donc saute le pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][3].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertEquals (plat.verifsautGauche(5,5),"marche");
		System.out.println("-------------Deplacement a gauche avec pion donc diagonale haut mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][3].setCouleur(Couleur.VERT);
		test[1][4].setCouleur(Couleur.MARRON);
		test[2][4].setCouleur(Couleur.MARRON);
		test[3][4].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleHG(5,5, true));	
		System.out.println("-------------Deplacement a gauche avec pion donc diagonale bas mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][3].setCouleur(Couleur.VERT);
		test[7][4].setCouleur(Couleur.MARRON);
		test[8][4].setCouleur(Couleur.MARRON);
		test[9][4].setCouleur(Couleur.MARRON);
		
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleBG(5,5, true));		
		System.out.println("-------------Deplacement a gauche avec pion donc saute le pion mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][3].setCouleur(Couleur.VERT);
		test[5][2].setCouleur(Couleur.MARRON);
		test[5][3].setCouleur(Couleur.MARRON);
		test[5][4].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertEquals (plat.verifsautGauche(5,5), "barriere");
		System.out.println("-------------Deplacement a gauche avec pion donc saute le pion mais pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[5][3].setCouleur(Couleur.VERT);
		test[5][1].setCouleur(Couleur.ROUGE);
		plat = new Plateau (test);
		assertEquals (plat.verifsautGauche(5,5), "pion");
		System.out.println("-------------Deplacement a gauche avec pion donc diagonale haut mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[3][1].setCouleur(Couleur.BLEU);
		test[1][1].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleHG(3,1, true));	
		System.out.println("-------------Deplacement a gauche avec pion donc diagonale bas mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[1][5].setCouleur(Couleur.BLEU);
		test[1][3].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleBG(1,5, true));	
		System.out.println("-------------Deplacement a gauche avec pion donc saute le pion mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][3].setCouleur(Couleur.BLEU);
		test[5][1].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertEquals(plat.verifsautGauche(5,3),"sort");
		
		System.out.println("-------------Deplacement a bas sans barriere et pion donc diagonale gauche-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[7][5].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertTrue (plat.verifDiagonaleBG(5,5, false));	
		System.out.println("-------------Deplacement a bas sans barriere et pion donc diagonale droite-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[7][5].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertTrue (plat.verifDiagonaleBD(5,5, false));	
		System.out.println("-------------Deplacement a bas sans barriere et pion donc saute le pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[7][5].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertEquals(plat.verifsautBas(5,5),"marche");	
		System.out.println("-------------Deplacement a bas avec et pion donc diagonale gauche mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[7][5].setCouleur(Couleur.VERT);
		test[6][3].setCouleur(Couleur.MARRON);
		test[6][2].setCouleur(Couleur.MARRON);
		test[6][1].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleBG(5,5, false));	
		System.out.println("-------------Deplacement a bas avec et pion donc diagonale droite mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[7][5].setCouleur(Couleur.VERT);
		test[6][7].setCouleur(Couleur.MARRON);
		test[6][8].setCouleur(Couleur.MARRON);
		test[6][9].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleBD(5,5, false));	
		System.out.println("-------------Deplacement a bas avec et pion donc saute le pion mais barriere-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[7][5].setCouleur(Couleur.VERT);
		test[8][5].setCouleur(Couleur.MARRON);
		test[8][6].setCouleur(Couleur.MARRON);
		test[8][7].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertEquals(plat.verifsautBas(5,5), "barriere");
		System.out.println("-------------Deplacement a bas avec et pion donc saute le pion mais pion-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][5].setCouleur(Couleur.BLEU);
		test[7][5].setCouleur(Couleur.VERT);
		test[9][5].setCouleur(Couleur.ROUGE);
		plat = new Plateau (test);
		assertEquals(plat.verifsautBas(5,5), "pion");
		System.out.println("-------------Deplacement a bas avec et pion donc diagonale gauche mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][1].setCouleur(Couleur.BLEU);
		test[7][1].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleBG(5,1, false));	
		System.out.println("-------------Deplacement a bas avec et pion donc diagonale droite mais  sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[15][17].setCouleur(Couleur.BLEU);
		test[17][17].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertFalse (plat.verifDiagonaleBD(15,17, false));	
		System.out.println("-------------Deplacement a bas avec et pion donc saute le pion mais sort-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[15][5].setCouleur(Couleur.BLEU);
		test[17][5].setCouleur(Couleur.VERT);
		plat = new Plateau (test);
		assertEquals(plat.verifsautBas(15,5),"sort");
		
		// ___________________________verif des possibilité
		System.out.println("-------------tout est possible-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][3].setCouleur(Couleur.BLEU);
		assertTrue (plat.verifPion(5,3));	
		System.out.println("-------------pas de possibiliter-----------------");
		for (int i = 0 ; i<19; i++) {
			for (int j = 0 ; j<19; j++) {
				test[i][j] = new Cases();
				test[i][j].setCouleur(Couleur.LIBREP);
			}
		}
		test[5][3].setCouleur(Couleur.BLEU);
		test[3][4].setCouleur(Couleur.MARRON);
		test[4][1].setCouleur(Couleur.MARRON);
		test[4][2].setCouleur(Couleur.MARRON);
		test[4][3].setCouleur(Couleur.MARRON);
		test[4][4].setCouleur(Couleur.MARRON);
		test[5][2].setCouleur(Couleur.MARRON);
		test[5][4].setCouleur(Couleur.MARRON);
		test[6][2].setCouleur(Couleur.MARRON);
		test[6][3].setCouleur(Couleur.MARRON);
		test[6][4].setCouleur(Couleur.MARRON);
		test[6][5].setCouleur(Couleur.MARRON);
		test[7][2].setCouleur(Couleur.MARRON);
		plat = new Plateau (test);
		assertFalse (plat.verifPion(5,3));		
	}
