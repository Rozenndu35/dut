/**
 * classe abstraite pour les caracteristiques des joueurs
 * @author rozenn/Klervia
 * @version1
 */

package ModeleConsole;

import java.util.*;

public abstract class Joueur {

	private ArrayList<Barriere> stockBarriere;
	private Pion monPion;
	private String nom;
	private int numero;

	/**
	 * Constructeur de la classe joueur
	 * @param nom : son nom
	 * @param numero : son numero
	 * @param nbJouer : le nombre de joueur
	 */
	public Joueur(String nom, int numero, int nbJoueur) {
		if ((nom !=null && numero>0 && numero< 5 )&& (nbJoueur == 2 || nbJoueur == 4 )) {
			this.nom=nom;
			this.numero=numero;
			if(numero == 1) {
				this.monPion = new Pion(Couleur.ROUGE);
			}
			else if(numero == 1) {
				this.monPion = new Pion(Couleur.NOIR);
			}
			else if(numero == 1) {
				this.monPion = new Pion(Couleur.BLEU);
			}
			else {
				this.monPion = new Pion(Couleur.VERT);
			}
			if (nbJoueur == 2) {
				stockBarriere = new ArrayList<Barriere> ();
				for(int i = 0 ; i<10 ; i++) {
					stockBarriere.add(new Barriere());
				}
			}
			if (nbJoueur == 4) {
				stockBarriere = new ArrayList<Barriere> ();
				for(int i = 0 ; i<5 ; i++) {
					stockBarriere.add(new Barriere());
				}
			}
		}
		else {
			this.nom="";
			this.numero=0;
			stockBarriere = new ArrayList<Barriere> ();
			this.monPion = new Pion(Couleur.LIBREP);
		}
		
		
	}
	/**
	 * Methode qui permet d obtenir de nom du joueur
	 * @return nom : le nom du joueur
	 */
	
	public String getNom() {
		return this.nom;
	}

	/**
	 * Methode qui en leve une barriere de ca liste de stockage
	 * @return si ca a marcher
	 */
	public boolean moinsBarriere() {
		boolean ret = false;
		if (stockBarriere.size() > 0) {
			stockBarriere.remove(stockBarriere.size()-1);	
			ret = true;
		}
		return ret;
	}
	
	/**
	 * renvois le nombre de barriere restante
	 * @return le nombre de barierre restante
	 */
	public int getNbBarriere() {
		int ret = 0;
		if (stockBarriere.size() > 0) {
			ret = stockBarriere.size();
		}
		return ret;
	}

	

}
