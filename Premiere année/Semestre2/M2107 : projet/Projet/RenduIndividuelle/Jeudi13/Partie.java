/**
 * classe pour la partie
 * @author rozenn/Klervia
 * @version1
 */

package ModeleConsole;

import java.util.*;

public class Partie {

	private Joueur joueur1;
	private Joueur joueur2;
	private Joueur joueur3;
	private Joueur joueur4;
	private Plateau plateau;
	private Mode mode;
	private Scanner scann;

	/**
	 * Constructeur de la classe partie
	 * @param mode : le mode de jeux
	 */
	public Partie(Mode mode) {
		scann = new Scanner(System.in);
		if (mode == Mode.OH ) {
			this.plateau = new Plateau(2);
			System.out.println("Saisir le nom du joeur1");
			String nom= scann.nextLine();
			this.joueur1 =  new Humain(nom, 1, 2);
			this.joueur2 =  new Ordinateur(null, 2, 2, "Ordinateur2" );
			
		}
		else if (mode == Mode.HH ) {
			this.plateau = new Plateau(2);
			System.out.println("Saisir le nom du joeur1");
			String nom1= scann.nextLine();
			this.joueur1 =  new Humain(nom1, 1, 2);
			System.out.println("Saisir le nom du joeur2");
			String nom2= scann.nextLine();
			this.joueur1 =  new Humain(nom2, 2, 2);
		}
		else if (mode == Mode.HOOO ) {
			this.plateau = new Plateau(4);
			System.out.println("Saisir le nom du joeur1");
			String nom= scann.nextLine();
			this.joueur1 =  new Humain(nom, 1, 4);
			this.joueur2 =  new Ordinateur(null, 2, 4, "Ordinateur2" );
			this.joueur3 =  new Ordinateur(null, 3, 4, "Ordinateur3" );
			this.joueur4 =  new Ordinateur(null, 4, 4, "Ordinateur4" );
		}
		else if (mode == Mode.HHOO ) {
			this.plateau = new Plateau(4);
			System.out.println("Saisir le nom du joeur1");
			String nom1= scann.nextLine();
			this.joueur1 =  new Humain(nom1, 1, 4);
			System.out.println("Saisir le nom du joeur2");
			String nom2= scann.nextLine();
			this.joueur2 =  new Humain(nom2, 2, 4);
			this.joueur3 =  new Ordinateur(null, 3, 4, "Ordinateur3" );
			this.joueur4 =  new Ordinateur(null, 4, 4, "Ordinateur4" );
		}
		else if (mode == Mode.HHHO ) {
			this.plateau = new Plateau(4);
			System.out.println("Saisir le nom du joeur1");
			String nom1= scann.nextLine();
			this.joueur1 =  new Humain(nom1, 1, 4);
			System.out.println("Saisir le nom du joeur2");
			String nom2= scann.nextLine();
			this.joueur2 =  new Humain(nom2, 2, 4);
			System.out.println("Saisir le nom du joeur3");
			String nom3= scann.nextLine();
			this.joueur3 =  new Humain(nom3, 3, 4);
			this.joueur4 =  new Ordinateur(null, 4, 4, "Ordinateur4" );
		}
		else if (mode == Mode.HHHH ) {
			this.plateau = new Plateau(4);
			System.out.println("Saisir le nom du joeur1");
			String nom1= scann.nextLine();
			this.joueur1 =  new Humain(nom1, 1, 4);
			System.out.println("Saisir le nom du joeur2");
			String nom2= scann.nextLine();
			this.joueur2 =  new Humain(nom2, 2, 4);
			System.out.println("Saisir le nom du joeur3");
			String nom3= scann.nextLine();
			this.joueur3 =  new Humain(nom3, 3, 4);
			System.out.println("Saisir le nom du joeur4");
			String nom4= scann.nextLine();
			this.joueur4 =  new Humain(nom4, 4, 4);
		}
	}
//suite pas fait encore
}
