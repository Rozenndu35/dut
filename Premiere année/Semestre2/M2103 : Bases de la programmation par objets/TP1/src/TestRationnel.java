/**
 * The test for the class Rationnel
 *@author Rozenn Costiou 
 */
public class TestRationnel {
	/**
 	 * The instance of classe
 	*/
	public static void main(String[] args){
		System.out.println("----Test du constructeur et toString ----");
		System.out.println("pour rationnel 6/5  si cela OK:");
		Rationnel r = new Rationnel(6,5);
		System.out.println(r.toString());

		System.out.println("pour rationnel 0/5  si cela OK:");
        r = new Rationnel(0,5);
		System.out.println(r.toString());

		System.out.println("pour rationnel -6/5 si cela OK:");
        r = new Rationnel(-6,5);
		System.out.println(r.toString());
		
        System.out.println("pour rationnel 6/-5 si -6/5 OK:");
        r = new Rationnel(6,-5);
		System.out.println(r.toString());
		
        System.out.println("pour rationnel -5/-4 si 5/4 OK :");
        r = new Rationnel(-5,-4);
		System.out.println(r.toString());
		
        System.out.println("---- Cas d'erreur----");
		System.out.println("Pour rationnel 1/0 :");
        r = new Rationnel(1,0);
		
		System.out.println();
        System.out.println("---- Test de getNumerateur et getDenominateur----");
		System.out.println("Pour rationnel 7/5:");
        r = new Rationnel(7,5);
		if ((r.getNumerateur() == 7) && (r.getDenominateur() == 5)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}


		System.out.println("---- Test de setNumerateur et getDenominateur----");
		System.out.println("Pour rationnel 7/5, change :");
        r = new Rationnel(7,5);
		r.setNumerateur(9);
		r.setDenominateur(10);
		if ((r.getNumerateur() == 9) && (r.getDenominateur() == 10)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		
		System.out.println("---- Test de inverse----");
		System.out.println("Pour rationnel 1/6");
		r = new Rationnel(1,6);
		if ((r.inverse().getNumerateur() == 6) && (r.inverse().getDenominateur() == 1)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour rationnel -7/6 si -6/7 OK");
		r = new Rationnel(-7,6);
		if ((r.inverse().getNumerateur() == -6) && (r.inverse().getDenominateur() == 7)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("---- Cas d'erreur----");
		System.out.println("Pour rationnel 0/6");
		r = new Rationnel(0,6);
		r = r.inverse();
		

		System.out.println("---- Test de ajoute ----");
		System.out.println("Pour rationnel 1/6 + 7/6");
		r = new Rationnel(1,6);
		Rationnel a = new Rationnel(7,6);
		
		r=r.ajoute(a);
		if ((r.getNumerateur() == 8) && (r.getDenominateur() == 6)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour rationnel -1/6 + 7/6");
		r = new Rationnel(-1,6);
		a = new Rationnel(7,6);
		r=r.ajoute(a);
		if ((r.getNumerateur() == 6) && (r.getDenominateur() == 6)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		
		System.out.println("Pour rationnel 1/6 + -7/6 si -1/6 OK");
		r = new Rationnel(1,6);
		a = new Rationnel(-7,6);
		r = r.ajoute(a);
		if ((r.getNumerateur() == -6) && (r.getDenominateur() == 6)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		
		System.out.println("Pour rationnel 0/6 + 7/6");
		r = new Rationnel(0,6);
		a = new Rationnel(7,6);
		r = r.ajoute(a);
		if ((r.getNumerateur() == 7) && (r.getDenominateur() == 6)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour rationnel 5/6 + 0/6");
		r = new Rationnel(5,6);
		a = new Rationnel(0,6);
		r = r.ajoute(a);
		if ((r.getNumerateur() == 5) && (r.getDenominateur() == 6)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour rationnel 5/6 + 0/3");
		r = new Rationnel(5,6);
		a = new Rationnel(0,6);
		r = r.ajoute(a);
		if ((r.getNumerateur() == 5) && (r.getDenominateur() == 6)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		
		System.out.println("Pour rationnel 5/6 + -2/3 si 3/18 OK");
		r = new Rationnel(5,6);
		a = new Rationnel(-2,3);
		r = r.ajoute(a);
		if ((r.getNumerateur() == 3) && (r.getDenominateur() == 18)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		
		System.out.println("Pour rationnel 5/6 + 2/5");
		r = new Rationnel(5,6);
		a = new Rationnel(2,5);
		r = r.ajoute(a);
		if ((r.getNumerateur() == 37) && (r.getDenominateur() == 30)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}

		System.out.println("---- Test de soustrait ----");
		System.out.println("Pour rationnel 1/6 - 7/6 si -6/6 OK");
		r = new Rationnel(1,6);
		a = new Rationnel(7,6);
		r = r.soustrait(a);
		if ((r.getNumerateur() == -6) && (r.getDenominateur() == 6)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour rationnel -1/6 - 7/6:");
		r = new Rationnel(-1,6);
		a = new Rationnel(7,6);
		r = r.soustrait(a);
		if ((r.getNumerateur() == -8) && (r.getDenominateur() == 6)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour rationnel 1/6 - -7/6:");
		r = new Rationnel(1,6);
		a = new Rationnel(-7,6);
		r = r.soustrait(a);
		if ((r.getNumerateur() == 8) && (r.getDenominateur() == 6)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour rationnel 0/6 - 7/6");
		r = new Rationnel(0,6);
		a = new Rationnel(7,6);
		r = r.soustrait(a);
		if ((r.getNumerateur() == -7) && (r.getDenominateur() == 6)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour rationnel 5/6 - 0/6 ");
		r = new Rationnel(5,6);
		a = new Rationnel(0,6);
		r = r.soustrait(a);
		if ((r.getNumerateur() == 5) && (r.getDenominateur() == 6)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour rationnel 5/6 - 0/3 si 15/18 OK");
		r = new Rationnel(5,6);
		a = new Rationnel(0,3);
		r = r.soustrait(a);
		if ((r.getNumerateur() == 15) && (r.getDenominateur() == 18)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour rationnel 5/6 - -2/3 si 27/18 OK");
		r = new Rationnel(5,6);
		a = new Rationnel(-2,3);
		r = r.soustrait(a);
		if ((r.getNumerateur() == 27) && (r.getDenominateur() == 18)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour rationnel 5/6 - 2/5 si 13/30 OK");
		r = new Rationnel(5,6);
		a = new Rationnel(2,5);
		r = r.soustrait(a);
		if ((r.getNumerateur() == 13) && (r.getDenominateur() == 30)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}

		System.out.println("---- Test de multiplie ----");
		System.out.println("Pour rationnel 1/6 * 7/6");
		r = new Rationnel(1,6);
		a = new Rationnel(7,6);
		r = r.multiplie(a);
		if ((r.getNumerateur() == 7) && (r.getDenominateur() == 36)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour rationnel -1/6 * 7/6");
		r = new Rationnel(-1,6);
		a = new Rationnel(7,6);
		r = r.multiplie(a);
		if ((r.getNumerateur() == -7) && (r.getDenominateur() == 36)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour rationnel 1/6 * -7/6");
		r = new Rationnel(1,6);
		a = new Rationnel(-7,6);
		r = r.multiplie(a);
		if ((r.getNumerateur() == -7) && (r.getDenominateur() == 36)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour rationnel 0/6 * 7/6 ");
		r = new Rationnel(0,6);
		a = new Rationnel(7,6);
		r = r.multiplie(a);
		if ((r.getNumerateur() == 0) && (r.getDenominateur() == 36)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour rationnel 5/6 * 0/6");
		r = new Rationnel(5,6);
		a = new Rationnel(0,6);
		r = r.multiplie(a);
		if ((r.getNumerateur() == 0) && (r.getDenominateur() == 36)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour rationnel 5/6 * 2/5");
		r = new Rationnel(5,6);
		a = new Rationnel(2,5);
		r = r.multiplie(a);
		if ((r.getNumerateur() == 10) && (r.getDenominateur() == 30)){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}

		System.out.println("---- Test de egales et reduce----");
		System.out.println("Pour rationnel 1/6 et 7/6");
		boolean eq = false;
		r = new Rationnel(1,6);
		a = new Rationnel(7,6);
		eq = r.equals(a);
		if (eq == false){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour rationnel 7/6 et 7/6 ");
		eq = false;
		r = new Rationnel(7,6);
		a = new Rationnel(7,6);
		eq = r.equals(a);
		if (eq == true){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour rationnel 5/12 et 7/6 ");
		eq = false;
		r = new Rationnel(5,12);
		a = new Rationnel(7,6);
		eq = r.equals(a);
		if (eq == false){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}		
		System.out.println("Pour rationnel 14/12 et 7/6 ");
		eq = false;
		r = new Rationnel(14,12);
		a = new Rationnel(7,6);
		eq = r.equals(a);
		if (eq == true){
			System.out.println("Test OK");
		}
		else{
			System.out.println("erreur");
		}	
    }
}