/**
 * manipulates the rationals (operation, equality, simplifies ...)
 *@author Rozenn Costiou 
 */
public class Rationnel {
    private int numerateur; // the numerator of methode
    private int denominateur;// the denominator of methode

    /**
     * Create a rational number with a numerator and a denominator
     * @param n : the numerator
     * @param d : the denominateur
    */
    public Rationnel(int n, int d){

        if (d > 0){
            this.numerateur = n;
            this.denominateur = d;
        }
        else if (d == 0) {
            System.err.println ("Rationnel: denominateur = 0 impossible");
        }
        else{
            this.numerateur = 0 - n;
            this.denominateur = 0- d;
        }
    }
    
    /**
     * Get the numerator
     * @return the numerator
     */
    public int getNumerateur (){
        return this.numerateur;
    }

    /**
     * set the numerator
     * @param numerateur : the numerator
     */
    public void setNumerateur (int numerateur){
        this.numerateur = numerateur;
        
    }

    /**
     * Get the denominator
     * @return the denominator
     */
    public int getDenominateur (){
        return this.denominateur;
    }

    /**
     * set the denominator
     * @param denominateur :  the denominator
     */
    public void setDenominateur (int denominateur){
        this.denominateur = denominateur;
    }

    /**
     * method who allow to reduce the rational number
     */
    private void reduce (){
        int i=0; // the actress of the loop
        boolean simp = false; // the and of the loop if is reduce
        if (this.numerateur == 0){
            this.denominateur = 1;
        }
        else{
            
            if (this.numerateur < this.denominateur){
                i = this.numerateur;
            }
            else if(this.numerateur >= this.denominateur){
                i= this.denominateur;
            }
            while ((i>0) && (simp == false)){
                if (((this.denominateur%i)==0) && ((this.numerateur%i)==0)){
                    simp = true;
                }
                else{
                    i--;
                }
            }
            if(i==0){
                this.numerateur = this.numerateur;
                this.denominateur = this.denominateur;
            }
            else{
                this.numerateur = (int) this.numerateur/i;
                this.denominateur = (int) this.denominateur/i;
            }
        }
    }

    /**
     * give the inverse of the rational number
	 * @return the inverse of the rational number 
     */
    public Rationnel inverse (){
        Rationnel inv = new Rationnel(this.numerateur, this.denominateur); //the new retional for the inverse
        if(this.numerateur != 0){
			inv = new Rationnel(this.denominateur, this.numerateur);
        }
		else{
			System.out.println("Un nombre nul n'a pas d'inverse");
		}
		return inv;
    }

    /**
     * give a sum of the rational with another on parameter
     * @param unNR : the rationel who is added
     * @return the sum of the addition
    */
    public Rationnel ajoute (Rationnel unNR){
        Rationnel somme = new Rationnel(this.numerateur, this.denominateur);//the new retional for the somme
        int a,b;
        if (this.denominateur == unNR.denominateur){
            this.numerateur = this.numerateur + unNR.numerateur;
            somme = new Rationnel(this.numerateur, this.denominateur);
        }
        else{
            this.numerateur = this.numerateur * unNR.denominateur + unNR.numerateur * this.denominateur ; 
            this.denominateur = this.denominateur * unNR.denominateur;
            somme = new Rationnel(this.numerateur, this.denominateur);
        }
        return somme;
    }

    /**
     * give the substract of the rational number with another on parameter
     * @param unNR : number who'll substract THE rational number
     * @return the result of the substract
     */
    public Rationnel soustrait (Rationnel unNR){
        Rationnel difference = new Rationnel(this.numerateur , this.denominateur);//the new retional for the difference
        if (this.denominateur == unNR.denominateur){
            this.numerateur = this.numerateur - unNR.numerateur;
            difference = new Rationnel(this.numerateur , this.denominateur);
        }
        else{
            this.numerateur = this.numerateur * unNR.denominateur - unNR.numerateur * this.denominateur;
            this.denominateur = this.denominateur * unNR.denominateur;          
            difference = new Rationnel(this.numerateur , this.denominateur);
        }
        return difference;
    }

    /**
     * give the multiplication of a rational with another on parameter
     * @param unNR :  is the rational who multiplicate the rational number
     * @return the result of the multiplication
     */
    public Rationnel multiplie (Rationnel unNR){
        this.denominateur = this.denominateur * unNR.denominateur;
        this.numerateur = this.numerateur * unNR.numerateur;
        Rationnel produit = new Rationnel(this.numerateur , this.denominateur);//the new retional for the produit
        return produit;
    }

    /**
     * compare two rational to find out if there are equal
     * @param nr2 : is the rational whocompare the rational number
     * @return aives true if he are equal
    */
    public boolean equals (Rationnel nr2){
        boolean equal = false; // the answer for the question itis equals
        this.reduce();
        nr2.reduce();
        if ((this.denominateur == nr2.denominateur) && (this.numerateur == nr2.numerateur)){
            equal= true;
        }
        return equal;
    }

    /**
     * create the rational
    */
    public String toString(){
        return (this.numerateur + "/" + this.denominateur);
    }
}
