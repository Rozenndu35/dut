/**
 * The scenario for the mail
 *@author Rozenn Costiou 
*/
import mail.*;
import java.util.ArrayList;

public class MailScenario {
     
    /**
 	 * The instance of classe
	 * @param args :  
 	*/
	public static void main(String[] args){
        Scenario1();
        Scenario2();

    }

    public static void Scenario1(){
        String from;
        String to;
        String message;
        MailServeur serveur;
        int nbMail1;
        int nbMail2;
        MailItem mail1;
        MailItem mail2;
        from = "Klervia";
        to = "Rozenn";
        message = "Salut ca vas";
        mail1 = new MailItem(from ,to ,  message);
        from = "Brieuc";
        to = "Rozenn";
        message = "Bonjour , est ce que je doit garder ton chien Samedi? ";
        mail2 = new MailItem(from ,to ,  message);
        serveur = new MailServeur();
        serveur.post(mail1);
        serveur.post(mail2);
        nbMail1 = serveur.howManyMailltems("Rozenn");
        nbMail2 = serveur.howManyMailltems("Klervia");
        if ((nbMail1 == 2) && (nbMail2 == 0)){
            System.out.println("test de howManyMailItem ok");
        }
        else{
            System.out.println("test de howManyMailItem erreur");
        }
        System.out.println("test de NextMailItem");
        System.out.println("Mail un pour Rozenn");
        mail1 = serveur.getNexMailItem("Rozenn");
        System.out.println("Mail deux pour Rozenn");
        mail2 = serveur.getNexMailItem("Rozenn");
    }

    public static void Scenario2(){
        String from;
        String to;
        String message;
        MailServeur serveur;
        int nbMail;
        MailItem mail1;
        from = "Klervia";
        to = "Rozenn";
        message = "Salut ca vas";
        mail1 = new MailItem(from ,to ,  message);
        System.out.println("Scenario d'un serveur avec 2 mail ajouterpas dedans demander n'est  et le client");
        serveur = new MailServeur();
        serveur.post(mail1);
        nbMail = serveur.howManyMailltems("Klervia");
        if (nbMail == 0){
            System.out.println("test de howManyMailItem ok");
        }
        else{
            System.out.println("test de howManyMailItem erreur");
        }
        System.out.println("test de NextMailItem ou il n' y a pas le client");
        System.out.println("Mail un pour Klervia");
        mail1 = serveur.getNexMailItem("Klervia");
        if (mail1 == null){
            System.out.println("test OK");
        }
        else {System.out.println("erreur");}
    }
}