/**
 * create the amail
 *@author Rozenn Costiou 
 */
package mail;
import java.util.ArrayList;

public class MailItem {
    private String from;
    private String to;
    private String message;
    private MailServeur items;

    /**
     * build the email
     * @param from : the sender
     * @param to : the receiver
     * @param message : the text of the mail
    */
    public MailItem(String from , String to , String message){
        if ( (from != null) && (to != null) && (message != null)){
            this.from = from;
            this.to = to;
            this.message = message;
        }
        else{
            this.from = "";
            this.to = "";
            this.message = "";
            System.err.println("MailItem : MailItem : from , to or message have not been initialized");
        }
    }

    /**
     * return the sender
     *@return the sender
    */
    public String getFrom(){
        return this.from;
    }

    /**
     * return the receiver
     *@return the receiver
    */
    public String getTo(){
        return this.to;
    }

    /**
     * return the text of the mail
     *@return the text of the mail
    */
    public String getMessage(){
        return message;
    }

    /**
     * print the message and the sender and recipient
    */
    public void print(){
        System.out.println("From :  " + getFrom());
        System.out.println ("To :   " + getTo());
        System.out.println (getMessage());
    }

}