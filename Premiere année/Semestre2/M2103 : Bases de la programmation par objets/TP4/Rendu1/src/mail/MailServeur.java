/**
 * allows to create a mail server
 *@author Rozenn Costiou 
*/

package mail;
import java.util.ArrayList;

public class MailServeur{
    private ArrayList <MailItem> items;

    /**
     * build the server
    */
    public MailServeur(){
        this.items = new ArrayList <MailItem>();
    }

    /**
     * calculate the number of mail send to a person
     * @param who : the poeple to shearch the number of mail 
     *@return the number of mail
    */
    public int howManyMailltems( String who){
        int ret = 0;
        for ( int i = 0 ; i< items.size(); i++){
            if ( this.items.get(i).getTo().equals(who)){ 
                ret ++;
            }
        }
        return ret;
    }

    /**
     * return the client's last email and delete it
     * @param who : the poeple to shearch the number of mail
     * @return the client last email
    */
    public MailItem getNexMailItem(String who){
        MailItem ret = null;
        boolean find = false;
        for ( int i = 0 ;(i< items.size() && find == false); i++){
            if ( this.items.get(i).getTo().equals(who)){ 
                find = true;
                ret = this.items.get(i);
                this.items.get(i).print();
                this.items.remove(i);
            }
        }
        return ret;
    }

    /**
     * add an email to the server
     * @param item : the email to add
    */
    public void post (MailItem item){
        this.items.add(item);
    }

}