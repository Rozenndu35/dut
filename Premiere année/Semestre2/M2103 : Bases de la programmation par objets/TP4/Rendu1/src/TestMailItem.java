/**
 * The test for the class MailItem
 *@author Rozenn Costiou 
*/
import mail.*;
import java.util.ArrayList;

public class TestMailItem {
	static int nbTest =0;
	static int nbTestOK =0;
	/**
 	 * The instance of classe
	 * @param args :  
 	*/
	public static void main(String[] args){
        testMailItem();
        testGetFrom();
        testGetTo();
        testGetMessage();
        System.out.println ("\n"+"nombre de test reussi : "+ nbTestOK + "\n" + "pour "+ nbTest + " effectuer");
    }
    
    /**
     * The test for the builder and print
    */
    public static void testMailItem(){
        String from;
        String to;
        String message;
        MailItem mail;
        System.out.println ("test du constructeur et de print");
        System.out.println ("pour tout ok");
        from = "Klervia";
        to = "Rozenn";
        message = "Salut ca vas";
        mail = new MailItem(from ,to ,  message);
        mail.print();

        System.out.println ("pour from invalide");
        from = null;
        to = "Rozenn";
        message = "Salut ca vas";
        mail = new MailItem(from ,to ,  message);
        mail.print();

        System.out.println ("pour to invalide");
        from = "Klervia";
        to = null;
        message = "Salut ca vas";
        mail = new MailItem(from ,to ,  message);
        mail.print();

        System.out.println ("pour message invalide");
        from = "Klervia";
        to = "Rozenn";
        message = null;
        mail = new MailItem(from ,to ,  message);
        mail.print();
    }

    /**
     * The test for the getFrom
    */
    public static void testGetFrom(){
        String from;
        String to;
        String message;
        MailItem mail;
        System.out.println ("test de getFrom");
        System.out.println ("pour tout ok");
        nbTest ++;
        from = "Klervia";
        to = "Rozenn";
        message = "Salut ca vas";
        mail = new MailItem(from ,to ,  message);
        if (mail.getFrom() == "Klervia"){
            System.out.println("test ok");
            nbTestOK ++;
        }
        else {
            System.out.println("erreur");
        }

        System.out.println ("pour from invalide");
        nbTest ++;
        from = null;
        to = "Rozenn";
        message = "Salut ca vas";
        mail = new MailItem(from ,to ,  message);
        if (mail.getFrom() == ""){
            System.out.println("test ok");
            nbTestOK ++;
        }
        else {
            System.out.println("erreur");
        }
    }

    /**
     * The test for the getTo
    */ 
    public static void testGetTo(){
        String from;
        String to;
        String message;
        MailItem mail;
        System.out.println ("test de getTo");
        System.out.println ("pour tout ok");
        nbTest ++;
        from = "Klervia";
        to = "Rozenn";
        message = "Salut ca vas";
        mail = new MailItem(from ,to ,  message);
        if (mail.getTo() == "Rozenn"){
            System.out.println("test ok");
            nbTestOK ++;
        }
        else {
            System.out.println("erreur");
        }

        System.out.println ("pour to invalide");
        nbTest ++;
        from = "Klervia";
        to = null;
        message = "Salut ca vas";
        mail = new MailItem(from ,to ,  message);
        if (mail.getTo() == ""){
            System.out.println("test ok");
            nbTestOK ++;
        }
        else {
            System.out.println("erreur");
        }
    }

    /**
     * The test getMessage
    */
    public static void testGetMessage(){
        String from;
        String to;
        String message;
        MailItem mail;
        System.out.println ("test de getMessage");
        System.out.println ("pour tout ok");
        nbTest ++;
        from = "Klervia";
        to = "Rozenn";
        message = "Salut ca vas";
        mail = new MailItem(from ,to ,  message);
        if (mail.getMessage() == "Salut ca vas"){
            System.out.println("test ok");
            nbTestOK ++;
        }
        else {
            System.out.println("erreur");
        }

        System.out.println ("pour message invalide");
        nbTest ++;
        from = "Klervia";
        to = "Rozenn";
        message = null;
        mail = new MailItem(from ,to ,  message);
        if (mail.getMessage() == ""){
            System.out.println("test ok");
            nbTestOK ++;
        }
        else {
            System.out.println("erreur");
        }
    }
}