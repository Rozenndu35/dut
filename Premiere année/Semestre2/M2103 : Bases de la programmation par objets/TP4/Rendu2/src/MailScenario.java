/**
 * The scenario for the mail
 *@author Rozenn Costiou 
*/
import mail.*;
import java.util.ArrayList;

public class MailScenario {
     
    /**
 	 * The instance of classe
	 * @param args :  
 	*/
	public static void main(String[] args){
        Scenario1();
        Scenario2();
        Scenario3();
        Scenario4();
        Scenario5();
        scenario6(); 
        scenario7();

    }

    public static void Scenario1(){
        String from;
        String to;
        String message;
        MailServeur serveur;
        int nbMail1;
        int nbMail2;
        MailItem mail1;
        MailItem mail2;
        from = "Klervia";
        to = "Rozenn";
        message = "Salut ca vas";
        mail1 = new MailItem(from ,to ,  message);
        from = "Brieuc";
        to = "Rozenn";
        message = "Bonjour , est ce que je doit garder ton chien Samedi? ";
        mail2 = new MailItem(from ,to ,  message);
        serveur = new MailServeur();
        serveur.post1(mail1);
        serveur.post1(mail2);
        nbMail1 = serveur.howManyMailltems("Rozenn");
        nbMail2 = serveur.howManyMailltems("Klervia");
        if ((nbMail1 == 2) && (nbMail2 == 0)){
            System.out.println("test de howManyMailItem ok");
        }
        else{
            System.out.println("test de howManyMailItem erreur");
        }
        System.out.println("test de NextMailItem");
        System.out.println("Mail un pour Rozenn");
        mail1 = serveur.getNexMailItem("Rozenn");
        System.out.println("Mail deux pour Rozenn");
        mail2 = serveur.getNexMailItem("Rozenn");
    }

    public static void Scenario2(){
        String from;
        String to;
        String message;
        MailServeur serveur;
        int nbMail;
        MailItem mail1;
        from = "Klervia";
        to = "Rozenn";
        message = "Salut ca vas";
        mail1 = new MailItem(from ,to ,  message);
        System.out.println("Scenario d'un serveur avec 2 mail a ajouter pas dedans demander n'est  et le client");
        serveur = new MailServeur();
        serveur.post1(mail1);
        nbMail = serveur.howManyMailltems("Klervia");
        if (nbMail == 0){
            System.out.println("test de howManyMailItem ok");
        }
        else{
            System.out.println("test de howManyMailItem erreur");
        }
        System.out.println("test de NextMailItem ou il n' y a pas le client");
        System.out.println("Mail un pour Klervia");
        mail1 = serveur.getNexMailItem("Klervia");
        if (mail1 == null){
            System.out.println("test OK");
        }
        else {System.out.println("erreur");}
    }
    public static void Scenario3(){
        ArrayList <String> filters;
        String mots;
        AntiSpam antispam;
        System.out.println("créer une liste anti spam et ajouter un mot");
        filters = new ArrayList <String>();
        filters.add("gagner");
        filters.add("perdu");
        mots = "piratage";
        antispam = new AntiSpam(filters);
        antispam.add(mots);       
    }

    public static void Scenario4(){
        ArrayList <String> filters;
        String mots;
        AntiSpam antispam;
        System.out.println("créer une liste anti spam invalide et ajouter un mot");
        mots = "piratage";
        filters = null;
        antispam = new AntiSpam(filters);
        antispam.add(mots);       
    }

    public static void Scenario5(){
        ArrayList <String> filters;
        String mots;
        AntiSpam antispam;
        System.out.println("créer une liste anti spam et ajouter un mot invalide");
        filters = new ArrayList <String>();
        mots = null;
        filters.add("gagner");
        filters.add("perdu");
        antispam = new AntiSpam(filters);
        antispam.add(mots);       
    }

    public static void scenario6(){
        ArrayList <String> filters;
        AntiSpam antispam;
        MailServeur serveur;
        MailItem mail; 
        String to;
        String from;
        String message;
        System.out.println(" ajout de mail OK");
        filters = new ArrayList <String>();
        filters.add("gagner");
        filters.add("perdu");
        serveur = new MailServeur(filters);
        to = "Briand";
        from = "Bautrais";
        message = "Salut rendez vous 8h a l'IUT ";
        mail = new MailItem(from , to , message);
        serveur.post(mail);
    }

    public static void scenario7(){
        ArrayList <String> filters;
        AntiSpam antispam;
        MailServeur serveur;
        MailItem mail; 
        String to;
        String from;
        String message;
        System.out.println(" ajout de mail avec un spam");
        filters = new ArrayList <String>();
        filters.add("gagner");
        filters.add("perdu");
        serveur = new MailServeur(filters);
        to = "Briand";
        from = "Bautrais";
        message = " Salut rendez vous 8h pour gagner ";
        mail = new MailItem(from , to , message);
        serveur.post(mail);
    }

}