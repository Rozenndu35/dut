/**
 * Create the Mail of the client
 *@author Rozenn Costiou 
*/
package mail;
import java.util.ArrayList;

public class MailClient{
    private String user;
    private MailServeur server;

    /**
     * build the MailClient
     * @param server : the server
     * @param user : the user
    */
    public MailClient(MailServeur server , String user){
       if ((user != null)&& (server != null)){
           this.user = user;
        }
        else {
            System.err.println ("MailClient : MailClient : server or user invalibel");
            this.user = "";
            this.server = null;
        }
    }

    /**
     * return a Email to the server
     *@return a Email
    */
    public MailItem getNextMailItem(){
        MailItem mail;
        if ((server!= null)&& (user != null)){
            mail = server.getNexMailItem(user);
            if (mail == null){
                System.err.println ("MailClient:getNextMailItem: user not in the server");
            }
        }
        else{
            mail= null;
            System.err.println ("MailClient:getNextMailItem: user or server invalibel");
        }
        
        return mail;
    }

    /**
     * print the mail of the user
    */
    public void printNextMailItem(){
        MailItem mail;
        if ((this.server!= null)&&(this.user!= null)){
            mail= this.server.getNexMailItem(this.user);
            if(mail!=null){
                mail.print();
            }
            else {
                System.out.println("No next mail");
            }
        }
        else{
            System.err.println("MailClient:printNextMailItem: user and server");
        }
    }

    /**
     * send a new mailItem in the server
     * @param to : the client
     * @param message : the message of client
    */
    public void sendMailItem( String to , String message){
        MailItem item;
        if (( to!= null) && (server!=null) && (user!=null) && (message!= null)){
            item = new MailItem(this.user, to , message);
            this.server.post(item);
        }
        else{
            System.err.println("MailClient:sendMailItem: invalid value");
        }
        
    }
}