/**
 * Create the Mail of the client
 *@author Rozenn Costiou 
*/
package mail;
import java.util.ArrayList;

public class AntiSpam{
    private ArrayList <String> filters;
    
    /**
     * build the AntiSpam
     * @param filters : the liste of the word spam
    */
    public AntiSpam(ArrayList<String> filters){
        if(filters!= null){
            this.filters = filters;
        }
        else {
            this.filters = null;
            System.err.println ( "AntiSpam: AntiSpam : filters not valibel");
        }
    }

    /**
     * scan the message for know if it is a spam
     * @param message : the message to analyse
     * @return true if tne message contain a word spam
    */
    public boolean scan (String message) {
        boolean contient;
        int trouver;
        int i = 0;
        String[] liste = message.split(" ");
        contient = false;
        if ((this.filters != null)&&(message != null)){
            while ((contient == false)&& (i < liste.length)){
                contient= filters.contains(liste[i]);
                i++;
            }
        }
        else{
            System.err.println ("AntiSpam: scan : filters or message invalibel");
        }
        return contient;
    }

    /**
     * add a new word in the spam
     *@param f : the word to add 
    */
    public void add ( String f){
        if ((f!= null)&& (this.filters!= null)){
            this.filters.add(f);
            System.out.println("word add");
        }
        else {
            System.err.println ("AntiSpam: add : filters or f invalibel");
        }
    }
}