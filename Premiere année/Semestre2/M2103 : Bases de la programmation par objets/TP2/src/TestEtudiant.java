/**
 * The test for the class Etudiant
 *@author Rozenn Costiou 
*/
public class TestEtudiant {
	static int nbTest =0;
	static int nbTestOK =0;
	/**
 	 * The instance of classe
	 * @param args :  
 	*/
	public static void main(String[] args){
		//testEtudiant();
		//testGetNom();
		//testSetNom();
		//testGetNbMatiere();
		//testGetUneNote();
		//testMoyenneMatiere();
		//testMoyenneGenerale();
		testMeilleurNote();
		System.out.println ("\n"+"nombre de test reussi : "+ nbTestOK + "\n" + "pour "+ nbTest + " effectuer");
    }
    public static void testEtudiant (){
		System.out.println("----Test du constructeur, initialisation et toString ----");
		
		String[] matieres = new String [5];
		double[] coeff = new double[5];
		matieres[0] = "Mathematique";
		matieres[1] = "Anglais";
		matieres[2] = "Economie";
		matieres[3] = "Gestion";
		matieres[4] = "Programation";
		coeff[0] = 2;
		coeff[1] = 3;
		coeff[2] = 1.5;
		coeff[3] = 2;
		coeff[4] = 3;
		Etudiant e1 = new Etudiant("Costiou", matieres, coeff,3);
		System.out.println (e1.toString());
		System.out.println ("verifier si un buletin s'affiche bien avec des notes");
	}

	public static void testGetNom(){
		String[] matieres = new String [5];
		double[] coeff = new double[5];
		System.out.println("---- Test de GetNom----");	
		System.out.println("Pour Costiou:");
		nbTest = nbTest+1;
		Etudiant e1 = new Etudiant("Costiou", matieres, coeff,3);
		if (e1.getNom() == "Costiou"){
			System.out.println("Test OK");
			nbTestOK = nbTestOK+1;
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour vide :");
		nbTest = nbTest+1;
		e1 = new Etudiant("", matieres, coeff,3);
		if (e1.getNom() == ""){
			System.out.println("Test OK");
			nbTestOK = nbTestOK+1;
		}
		else{
			System.out.println("erreur");
		}
	}

	public static void testSetNom(){
		String[] matieres = new String [10];
		double[] coeff = new double[10];
		System.out.println("---- Test de setNom----");
		System.out.println("Pour Costiou change  par Bautrais :");
		nbTest = nbTest+1;
        Etudiant e1 = new Etudiant("Costiou", matieres, coeff,6);
		e1.setNom("Bautrais");
		if (e1.getNom() == "Bautrais"){
			System.out.println("Test OK");
			nbTestOK = nbTestOK+1;
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour Costiou chage par vide :");
		nbTest = nbTest+1;
		e1 = new Etudiant("Costiou", matieres, coeff,3);
		e1.setNom("");
		if (e1.getNom() == ""){
			System.out.println("Test OK");
			nbTestOK = nbTestOK+1;
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour vide chage par costiou :");
		nbTest = nbTest+1;
		e1 = new Etudiant("", matieres, coeff,3);
		e1.setNom("Costiou");
		if (e1.getNom() == "Costiou"){
			System.out.println("Test OK");
			nbTestOK = nbTestOK+1;
		}
		else{
			System.out.println("erreur");
		}
	}

	public static void testGetNbMatiere(){
		String[] matieres = new String [10];
		double[] coeff = new double[10];
		System.out.println("---- Test de setNomGetNbMatiere----");
		System.out.println("Pour 10 matiere:");
		nbTest = nbTest+1;
        Etudiant e1 = new Etudiant("Costiou", matieres, coeff,6);
		if (e1.getNbMatiere() == 10){
			System.out.println("Test OK");
			nbTestOK = nbTestOK+1;
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour 0 matiere :");
		nbTest = nbTest+1;
		matieres = new String [0];
		coeff = new double[0];
        e1 = new Etudiant("Costiou", matieres, coeff,6);
		if (e1.getNbMatiere() == 0){
			System.out.println("Test OK");
			nbTestOK = nbTestOK+1;

		}
		else{
			System.out.println("erreur");
		}
	}

	public static void testGetUneNote(){
		String[] matieres = new String [10];
		double[] coeff = new double[10];
		System.out.println("---- Test de setNomGetUneNote (faire en elevent l'initialisation dans le constructure----");
		System.out.println("Pour iMatiere et i compris dans le tableau :");
		nbTest = nbTest+1;
        Etudiant e1 = new Etudiant("Costiou", matieres, coeff,10);
		if (e1.getUneNote(5,5) == 0){
			System.out.println("Test OK");
			nbTestOK = nbTestOK+1;
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour iMatiere compris dans le tableau et i egale 0 :");
		nbTest = nbTest+1;
		matieres = new String [10];
		coeff = new double[10];
        e1 = new Etudiant("Costiou", matieres, coeff,10);
		if (e1.getUneNote(5,0) == 0){
			System.out.println("Test OK");
			nbTestOK = nbTestOK+1;
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour i compris dans le tableau et iMatiere egale 0 :");
		nbTest = nbTest+1;
		matieres = new String [10];
		coeff = new double[10];
        e1 = new Etudiant("Costiou", matieres, coeff,10);
		if (e1.getUneNote(0,5) == 0){
			System.out.println("Test OK");
			nbTestOK = nbTestOK+1;
		}
		else{
			System.out.println("erreur");
		}

		System.out.println("Pour i compris dans le tableau et iMatiere inferieur 0 :");
		nbTest = nbTest+1;
		matieres = new String [10];
		coeff = new double[10];
        e1 = new Etudiant("Costiou", matieres, coeff,10);
		if (e1.getUneNote(-5,5) == -1){
			System.out.println("Test OK");
			nbTestOK = nbTestOK+1;

		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour iMatiere compris dans le tableau et i inferieur 0 :");
		nbTest = nbTest+1;
		matieres = new String [10];
		coeff = new double[10];
        e1 = new Etudiant("Costiou", matieres, coeff,10);
		if (e1.getUneNote(5,-5) == -1){
			System.out.println("Test OK");
			nbTestOK = nbTestOK+1;

		}
		else{
			System.out.println("erreur");
		}
	}
	public static void testMoyenneMatiere(){
		String[] matieres = new String [10];
		double[] coeff = new double[10];
		Etudiant e1 = new Etudiant("Costiou", matieres, coeff,10);
		System.out.println("---- Test de MoyenneMatiere (faire en enlevent l' initialisatiion dans le constructeur)----");
		System.out.println("Pour iMatiere compris dans le tableau :");
		nbTest = nbTest+1;
        if (e1.moyenneMatiere (5) == 0){
			System.out.println("Test OK");
			nbTestOK = nbTestOK+1;
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour iMatiere egale a 0:");
		nbTest = nbTest+1;
        if (e1.moyenneMatiere (0) == 0){
			System.out.println("Test OK");
			nbTestOK = nbTestOK+1;
		}
		else{
			System.out.println("erreur");
		}
		System.out.println("Pour iMatiere a l'exterieur du tableau :");
		nbTest = nbTest+1;
        if (e1.moyenneMatiere (-5) == -1){
			System.out.println("Test OK");
			nbTestOK = nbTestOK+1;
		}
		else{
			System.out.println("erreur");
		}
	}
	public static void testMoyenneGenerale(){
		String[] matieres = new String [10];
		double[] coeff = new double[10];
		coeff[0] = 3;
		coeff[1] = 2;
		coeff[2] = 1;
		coeff[3] = 5;
		coeff[4] = 9;

		Etudiant e1 = new Etudiant("Costiou", matieres, coeff,10);
		System.out.println("---- Test de MoyenneMatiere (faire en enlevent l' initialisatiion dans le constructeur)----");
		nbTest = nbTest+1;
        if (e1.moyenneGenerale () == 0){
			System.out.println("Test OK");
			nbTestOK = nbTestOK+1;
		}
		else{
			System.out.println("erreur");
		}
	}
	public static void testMeilleurNote(){
		System.out.println("---- Test de MeilleurNote----");
		String[] matieres = new String [5];
		double[] coeff = new double[5];
		matieres[0] = "Mathematique";
		matieres[1] = "Anglais";
		matieres[2] = "Economie";
		matieres[3] = "Gestion";
		matieres[4] = "Programation";
		coeff[0] = 2;
		coeff[1] = 3;
		coeff[2] = 1.5;
		coeff[3] = 2;
		coeff[4] = 3;
		Etudiant e1 = new Etudiant("Costiou", matieres, coeff,3);
		System.out.println (e1.toString());
		System.out.println ("regarder si meilleur note est bien celle ci : ");
		System.out.println (e1.meilleurNote());
	}
}