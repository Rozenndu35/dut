/**
 * Create a bulletin
 *@author Rozenn Costiou 
 */
import java.util.Arrays;

public class Etudiant {
    private String nom; // the name
    private double[][] bulletin; // the tables whith name of subject and the gate
    private final String[] MATIERE; // contains the titles of the subject
    private final double[] COEFFICIENT; //the tables with the coefficient


    /**
     * Create a bulletin with a name, subjects, coeffivients and the number of the gate
     * @param nom : the name
     * @param matiere : the subjects
     * @param coeff : the coefficients
     * @param nbNotes : the number of the gate
    */
    public Etudiant(String nom, String[] matiere, double[] coeff, int nbNotes){
        if (nom != null){
            this.nom=nom;
            if ((matiere != null) && (matiere.length > 0) && (matiere.length == coeff.length)){
                this.MATIERE = matiere;
                this.COEFFICIENT = coeff;
                if(nbNotes >0){
                    this.bulletin = new double [matiere.length][nbNotes];
                    this.initialisation ();
                }
                else{
                    System.err.println("Etudiant: Etudiant: nbNotes invalide ");
                    this.bulletin = new double [0][0];
                }
            }
            else{
                System.err.println("Etudiant: Etudiant: nbNotes invalide ");
                this.bulletin = new double [0][0];
                this.MATIERE = new String [0];
                this.COEFFICIENT = new double [0];
            }
        }
        else{
            System.err.println("Etudiant: Etudiant: nom invalide ");
            this.nom="";
            this.bulletin = new double [0][0];
            this.MATIERE = new String [0];
            this.COEFFICIENT = new double [0];
        }
    }

    /**
     * set the name
     * @param nom : the name
     */
    public void setNom (String nom){
        this.nom = nom;
    }

    /**
     * Get the name
     * @return the name
    */
    public String getNom (){
        return this.nom;
    }

    /**
     * Get the number of matiere
     * @return the number of matiere
    */
    public int getNbMatiere (){
        int ret; // value to return
        ret = MATIERE.length;
        return ret;
    }

    /**
     * Get the grade
     * @param iMatiere : the column of subject
     * @param i : the line of the note
     * @return the grade
    */
    public double getUneNote (int iMatiere , int i){
        double ret; // value to return
        if ((iMatiere >= 0) && (i >= 0) && (iMatiere < bulletin.length) && (i < bulletin.length)){
            ret = this.bulletin[iMatiere][i];
        }
        else{
            System.err.println("Etudiant: getUneNote: i ou iMatiere invalide ");
            ret = -1;
        }
        return ret;
    }
    /**
     * Fill the bulletin of a student with real random values ​​between 0 and 20
    */
    private void initialisation (){
        int i=0; // value for while
        int j; // value for while
        while (i< MATIERE.length){
            j=0;
            while (j< bulletin[0].length){
                bulletin [i][j] =  (double) Math.round((Math.random() * 20) * 100) / 100;
                j++;
            }
            i++;
        }
        
    }

    /**
     * calculates and returns the average of a subject
     * @param iMatiere : the subject we want the average
     * @return the average grade in a subject
    */
    public double moyenneMatiere (int iMatiere){
        double ret = 0 ; // value to return
        int i=0; // value for while
        if (iMatiere  >=0){
            while (i < bulletin[0].length){
                ret = ret + bulletin[iMatiere][i];
                i++;
            }
            ret =  (double) Math.round( (ret / bulletin[0].length) *  100) / 100;
        }
        else{
            ret =-1;
        }
        return ret;
    }
   
    /**
     * calculates and returns the average
     * @return the average
    */
    public double moyenneGenerale (){
        double ret = 0; // value to return
        double nbCoef=0; // the divisor to have the average
        int i=0; // value for while

        while (i < MATIERE.length){
            ret = ret + ( moyenneMatiere (i) * COEFFICIENT[i]);
            nbCoef = nbCoef + COEFFICIENT[i];

            i++;
        }
        ret =  (double) Math.round( (ret/nbCoef) *  100) / 100;
        return ret;
    }

    /**
     * returns a string containing the subject and the best grade
     * @return a string containing the subject and the best grade
    */
    public String meilleurNote (){
        int i = 0; // value for while
        int j; // value for while
        String ret; // value to return
        double note = 0; // the best gate
        int iMatiere= 0; // the subject number wher they have the best gate
        String matiere;
        while (i< MATIERE.length){
            j=0;
            while (j< bulletin[0].length){
                if (getUneNote (i,j) > note){
                    note = getUneNote (i,j);
                    iMatiere = i;
                }
                j++;
            }
            i++;
        }
        matiere = MATIERE[iMatiere];
        ret =  ("la meilleur note est " + matiere + " : " + note);
        return ret;
    }

    /**
     * returns a string representing the data of a student that we can post later.
     * @return a string representing the data of a student that we can post later.
    */

    public String toString(){
        String ret = new String("Bulletin vide");
        int nbMatiere = this.getNbMatiere();
        int nbNotes = this.bulletin[0].length;;
        int i= 0;
        int j = 0;
        if (nbMatiere > 0){
            ret = "bulletin de : " + this.nom + "\n";
            ret = ret + "Matieres" + "\t" + "Coeff" + "\t" ;
            while (i < nbNotes){
                ret = ret + "note" + (i+1) + "\t";
                i++;
            }
            ret = ret + "moyenne" + "\n";
            i=0;
            while (i < nbMatiere){
                ret = ret +this.MATIERE[i] + " " + "\t" + this.COEFFICIENT[i] + "\t";
                j= 0;
                while (j < nbNotes){
                    ret = ret+ this.bulletin[i][j] + "\t" ;
                    j++;
                }
                ret = ret + this.moyenneMatiere(i) + "\n";
                i++;
            }
            ret = ret + "moyenne generale : " + this.moyenneGenerale() + "\n" + this.meilleurNote();
        }
            
        else{
            System.err.println("Etudiant: toString: nb de matiere invalide ");
        }
        
        return ret ;
    }

}