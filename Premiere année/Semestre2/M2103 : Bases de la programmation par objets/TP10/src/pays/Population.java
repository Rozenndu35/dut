package pays;

import java.util.*;

import tri.TriParSelectionAlpha;
import utilitaire.RWFile;
/**
 * The class Population
 * @author rozenn
 * @version 3
 */
public class Population {
	private HashMap <String , Double> popMap;
	private HashMap <String , Double> areaMap;
	private ArrayList<Pays> listePays;
	
	/**
	 * the constructor of the population
	 * @param popFileName the file with the information of population
	 * @param areaFileName the file withe the information of area
	 */
	public Population (String popFileName , String areaFileName) {
		this.popMap = new HashMap <String , Double>();
		this.areaMap = new HashMap <String , Double>();
		this.listePays = new  ArrayList<Pays>();
		if ((popFileName != null) && (areaFileName != null)) {
			initializePopMap( popFileName);
			initializeAreaMap( areaFileName);
			initializeListPays();	
		}
	}

	/**
	 * get the HashMap population
	 * @return the popMap
	 */
	public HashMap <String , Double> getPopMap() {
		return this.popMap;
	}

	/**
	 * get the HashMap area
	 * @return the areaMap
	 */
	public HashMap <String , Double> getAreaMap() {
		return this.areaMap;
	}

	/**
	 * get the list of pays
	 * @return the listePays
	 */
	public ArrayList<Pays> getListePays() {
		return this.listePays;
	}
	/**
	 * init the HashMap of popuation with a file
	 * @param popFile the file with the information of population
	 */
	private void initializePopMap( String popFile) {
		ArrayList<String> recup = new ArrayList<String>();
		recup = RWFile.readFile (popFile);
		this.popMap = asMap (recup);
	}
	
	/**
	 * init the HashMap of area with a file
	 * @param areaFile the file with the information of area
	 */
	private void initializeAreaMap( String areaFile) {
		ArrayList<String> recup = new ArrayList<String>();
		recup = RWFile.readFile (areaFile);
		this.areaMap = asMap (recup);
	}
	/**
	 * take the contry in a line withh other information
	 * @param line : a line of the file
	 * @return the country
	 */
	private String extractContry(String line) {
		String ret = "";
		for ( int i =0 ; ((i<line.length()) && (Character.isDigit(line.charAt(i)) == false)) ; i++ ){
			ret += line.charAt(i);
		}
		ret = ret.trim();
		return ret;
	}
	
	/**
	 * take the value (population or area) in a line withh other information
	 * @param line : a line of the file
	 * @return the value
	 */
	private double extractValue(String line) {
		double ret= 0;
		int indice = 0;
		for ( int i =0 ; ((i<line.length()) && (Character.isDigit(line.charAt(i)) == false)) ; i++ ){
			indice = i;
		}
		String chaine = line.substring (indice);
		chaine = chaine.trim();
		ret = Double.parseDouble(chaine);
		return ret;
	}
	/**
	 * extract and put the information in a HashMap
	 * @param liste : a liste with the line of file for element
	 * @return the HashMap withe the contry and the value
	 */
	private HashMap<String,Double> asMap (ArrayList<String> liste){
		HashMap<String,Double> ret = new HashMap<String,Double>();
		for (String e : liste) {
			String pays =extractContry(e);
			double pop = extractValue(e);
			ret.put (pays, new Double (pop));
		}
		return ret;
		
	}
	/**
	 * Initilize the list of the contry
	 */
	private void initializeListPays() {
		Pays pays;
		Set<String> nomPays = popMap.keySet();
		if (popMap.size() == areaMap.size()) {
			for (String nom : nomPays) {
				pays =new Pays (nom , popMap.get(nom), areaMap.get(nom));
				this.listePays.add(pays);
			}
		}
	}
	/**
	 * create a HashMap with the density
	 * @return the new HashMap with the density
	 */
	public HashMap<String , Double> computeDensity(){
		HashMap<String, Double> ret = new HashMap<String, Double>();
		double den;
		for(int i = 0; i < this.listePays.size(); i++) {
			popMap.get(areaMap.get(listePays.get(i).getNom()));
			double area = areaMap.get(listePays.get(i).getNom());
			if (area == 0) {
				den = -1;
			}
			else{den = (popMap.get(listePays.get(i).getNom())/area);}
			ret.put(listePays.get(i).getNom(), new Double (den));
		}
		return ret;
		
	}
	/**
	 * create a HashMap with the density and return the country with the biggest density
	 * @return the country with the biggest density
	 */
	public Pays getMaxDensity() {
		Pays ret = null;
		HashMap<String, Double> denc = computeDensity();
		double den = 0;
		for(int i = 0; i < this.listePays.size(); i++) {
			if ((den < denc.get(listePays.get(i).getNom())) && (denc.get(listePays.get(i).getNom())!= -1)) {
				den = denc.get(listePays.get(i).getNom());
				ret = listePays.get(i);
			}			
		}
		return ret;
	}
	
	/**
	 * save the density in "data/densityFile"
	 * @param denc : the HashMap of density to want save
	 */
	public void enregistrerDencité(HashMap<String, Double> denc) {
		Pays[] tPays = new Pays[listePays.size()];
		ArrayList<String> listEcrire = new ArrayList<String>();
       	for (int i = 0; i<listePays.size() ; i++) {
       		tPays[i] = listePays.get(i);
       	}
       	TriParSelectionAlpha tri = new TriParSelectionAlpha(tPays);
        tri.trier();
        for (int i = 0; i<tPays.length ; i++) {
        	String aj= tPays[i].getNom()+ denc.get(tPays[i].getNom());
        	listEcrire.add(aj);
       	}
        System.out.println(listEcrire.toString());
        RWFile write = new RWFile();
        write.writeFile(listEcrire , "data/densityFile");
        System.out.println("Fichier Enregistrer!");
	}

}
