package tri;

import pays.Pays;

/**
 * Class to make a sort by selection
 * @author Rozenn
 * @version 3
 */
public class triRapideRecAlpha implements ITri {
	private Pays[] tab;
	/**
	 * the constructor
	 * @param tab : the tab to want a sort
	 */
	public triRapideRecAlpha(Pays[] tab) {
		if (tab != null) {
			this.tab=tab;
		}
		else {
			this.tab= new Pays[0];
		}
	}
	/**
	 * Recursive sorting method according to the separation principle. The method is called itself on the left and right tables with respect to a pivot.
	 * @param indL end of table left index
	 * @param indR end of table right index
	 */
	private void recursive(int indL, int indR){
		int indS;
		indS = separer(indL, indR );
		if (indS >0){
			if ( (indS-1) > indL ) {
				recursive ( indL, (indS-1) );
			}
		}
		if ( indS < this.tab.length){
			if ( (indS+1) < indR ) {
				recursive ((indS+1), indR );
			}
		}
	}
	/**
	 * This method returns the separation index of the table in two parts by placing the pivot in the correct box.
	 * @param indL end of table left index
	 * @param indR end of table right index
	 * @return the separation index of the table
	 */
	public int separer( int indL, int indR){
		  String pivot;
		  int ret;
		  ret =indL;
		  pivot = this.tab[indL].getNom();
		  while (this.tab[indR].getSurface() != this.tab[indL].getSurface()){
			  while (this.tab[indR].getNom().compareTo(pivot)> 0){
				  indR--;
			  }
			  if (this.tab[indR].getNom().compareTo(pivot)< 0){
				  swap(indR , indL );
			  }
			  while (this.tab[indL].getNom().compareTo(pivot)< 0){
					  indL++;
			  }
			  if (this.tab[indL].getNom().compareTo(pivot)> 0){
				  swap(indR , indL );
			  }		  
		  }
		  ret = indL;
		  return ret;
	  }


	/**
	 * swap to value
	 * @param i : first value 
	 * @param j : second value
	 */
	private void swap (int i , int j) {
		Pays change = this.tab[i];
		this.tab[i] = this.tab[j];
		this.tab[j] = change;
	}
	
	/**
	 * sort the Pays array by arrays
	 */
	public void trier() {
		recursive(0, this.tab.length-1 );

	}

}
