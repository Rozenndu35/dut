package Recherche;

import pays.*;
/**
 * recherche  Dichotomie
 * @author Rozenn
 * @version 3
 */
public class BinarySearcher {
	private Pays[] tab;
	private int nbElem;
	private String aChercher;
	/**
	 * The constructor
	 * @param leTab : the country table where you want to search
	 * @param nb : the number of country
	 * @param aChercher : the name of country to want search
	 */
	public BinarySearcher(Pays[] leTab , int nb , String aChercher){
		this.tab = leTab;
		this.nbElem = nb;
		this.aChercher = aChercher;
	}

	/**
	 * search the country in the table
	 * @return the index of the country search or -1 if is not in the list
	 */
	public int rechercher() {	
		int indD = 0; //debut
		int indF = this.nbElem -1; //fin
		int indM; // millieu
		int ret = -1;
		while (indD != indF) {
			indM =(indD+indF)/2;
			if (aChercher.compareTo(this.tab[indM].getNom()) >0 ) {
				indD= indM+1;
				
			}
			else {
				indF = indM;				
			}
		}
		if (this.aChercher.compareTo(this.tab[indD].getNom()) == 0) {
			ret = indD;
		}
		return ret;
		
	}
}
