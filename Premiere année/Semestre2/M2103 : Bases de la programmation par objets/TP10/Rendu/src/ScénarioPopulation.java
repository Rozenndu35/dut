import pays.*;
import tri.*;
import Recherche.*;
import utilitaire.*;
import java.util.*;

/**
 * The scenario of the TP
 * @author rozenn
 * @version 3
 */
public class ScénarioPopulation {
	public static void main (String[] args){
	    //ScenarioComparePays();
	    //ScenarioSetPays();
	    //ScenarioTestTriParSelection();
		//ScenarioPopulation();
		//ScenarioPopulationRanger();
		//ScenarioPopulationDecite();
		//ScenarioPopulationMaxDecite();
		//ScenarioPopulationRangerNom();
		//ScenarioFichierDencite();
		//ScenarioCherchePays();
		//ScenarioPopulationRangerNomRapide();
		//ScenarioPopulationCompareTri();
		ScenarioCherchePaysSelectionner();
    }
	/* test de compare de pays*/
    public static void ScenarioComparePays(){
        System.out.println ( "Test CompareTo");
        Pays pays1  = new Pays ("Chile" , 16454143, 756950);
        Pays pays2 = new Pays ("Cuba" , 11423952, 110860);
        int resultat;
        int marche=0;
        int nbTest=0;
        resultat = pays1.compareTo(pays2);
        nbTest ++;
        if ( resultat == -1){
            marche ++;
        }
        else{
            System.err.println ("erreur : inferieur");
        }
        resultat = pays1.compareTo(pays1);
        nbTest ++;
        if ( resultat == 0){
            marche ++;
        }
        else{
            System.err.println ("erreur : pour egale");
        }
        resultat = pays2.compareTo(pays1);
        nbTest ++;
        if ( resultat == 1){
            marche ++;
        }
        else{
            System.err.println ("erreur : pour superieur");
        }
        System.out.println ( "nombre de test reussi : " + marche + "/" + nbTest);
    }
    /* test des set de pays*/
    public static void ScenarioSetPays(){
        System.out.println ( "Test set...");
        Pays pays = new Pays ("Chile" , 16454143, 756950);
        int marche=0;
        int nbTest=0;
        nbTest ++;
        pays.setNom("Cuba");
        if (pays.getNom() == "Cuba"){
            marche ++;
        }
        else{
            System.err.println ("erreur : setNom");
        }
        nbTest ++;
        pays.setSurface(1);
        if (pays.getSurface() == 1){
            marche ++;
        }
        else{
            System.err.println ("erreur : setSurface");
        }
        nbTest ++;
        pays.setPopulation(100);
        if (pays.getPopulation() == 100){
            marche ++;
        }
        else{
            System.err.println ("erreur : setPopulation");
        }
        System.out.println ( "nombre de test reussi : " + marche + "/" + nbTest);
    }
    /* test du tri par selection  par leur superficie*/
    public static void ScenarioTestTriParSelection(){
        System.out.println ( "Test TriParSelection");
        Pays[] tabPays = new Pays[10];
        tabPays [0] = new Pays ("Cuba" , 11423952, 110860);
        tabPays [1] = new Pays ("Chile" , 16454143, 756950);
        tabPays [2] = new Pays ("Russia" , 140702094, 17075200);
        tabPays [3] = new Pays ("Norway" , 4644457, 323802);
        tabPays [4] = new Pays ("Nigeria" , 138283240, 923768);
        tabPays [5] = new Pays ("Paraguay" , 6831306, 406750);
        tabPays [6] = new Pays ("Oman" , 3311640, 212460);
        tabPays [7] = new Pays ("Yemen" , 23013376, 406750);
        tabPays [8] = new Pays ("Togo" , 5858673, 56785);
        tabPays [9] = new Pays ("France" , 64057790 , 643427);
        TriParSelection tri = new TriParSelection(tabPays);
        tri.trier();
        System.out.println (Arrays.toString(tabPays));
    }
    /* test du de laa classe population*/
    public static void ScenarioPopulation(){
    	Population pop = new Population("../data/worldpop.txt" ,"../data/worldarea.txt");
    	System.out.println ("population : "+pop.getPopMap().toString());
    	System.out.println ("taille : "+pop.getAreaMap().toString());
    	System.out.println ("Pays : "+pop.getListePays().toString());
    }
    /* test du tri par selection  par leur superficie*/
    public static void ScenarioPopulationRanger(){
    	Population pop = new Population("../data/worldpop.txt" ,"../data/worldarea.txt");
    	ArrayList<Pays> pays = pop.getListePays();
       	Pays[] tPays = new Pays[pays.size()];
       	for (int i = 0; i<pays.size() ; i++) {
       		tPays[i] = pays.get(i);
       	}
       	TriParSelection tri = new TriParSelection(tPays);
        tri.trier();
        System.out.println (Arrays.toString(tPays));

    }
    /* test de la methode computeDensity */
    public static void ScenarioPopulationDecite() {
    	Population pop = new Population("../data/worldpop.txt" ,"../data/worldarea.txt");
    	HashMap<String, Double> dencite = pop.computeDensity();
    	System.out.println(dencite.toString());
    }
    /* test de la methode getMaxDensity */
    public static void ScenarioPopulationMaxDecite() {
    	Population pop = new Population("../data/worldpop.txt" ,"../data/worldarea.txt");
    	Pays pays = pop.getMaxDensity();
    	System.out.println(pays.toString());
    }
    /* test de la methode du tri par selection par leur nom */
    public static void ScenarioPopulationRangerNom() {
    	Population pop = new Population("../data/worldpop.txt" ,"../data/worldarea.txt");
    	ArrayList<Pays> pays = pop.getListePays();
       	Pays[] tPays = new Pays[pays.size()];
       	for (int i = 0; i<pays.size() ; i++) {
       		tPays[i] = pays.get(i);
       	}
       	TriParSelectionAlpha tri = new TriParSelectionAlpha(tPays);
        tri.trier();
        System.out.println (Arrays.toString(tPays));
    }
    /* test d'enregistrer la dencite dans data le fichier */
    public static void ScenarioFichierDencite() {
    	Population pop = new Population("../data/worldpop.txt" ,"../data/worldarea.txt");
    	HashMap<String, Double> dencite = pop.computeDensity();
    	pop.enregistrerDencité(dencite);
    }
    /* test rechercher un pays dans un tableau*/
    public static void ScenarioCherchePays() {
    	Population pop = new Population("../data/worldpop.txt" ,"../data/worldarea.txt");
    	ArrayList<Pays> pays = pop.getListePays();
       	Pays[] tPays = new Pays[pays.size()];
       	for (int i = 0; i<pays.size() ; i++) {
       		tPays[i] = pays.get(i);
       	}
       	TriParSelectionAlpha tri = new TriParSelectionAlpha(tPays);
        tri.trier();
       	BinarySearcher cherche = new BinarySearcher(tPays , pays.size() , "Afghanistan");
       	int in = cherche.rechercher();
       	if ( tPays[in].getNom().compareTo("Afghanistan") == 0) {System.out.println("test reussi");}
       	else {System.err.println("erreur recherche");} 
       	BinarySearcher cher = new BinarySearcher(tPays , pays.size() , "Vannes");
       	int i = cher.rechercher();
       	if (i == -1) {System.out.println("test reussi");}
       	else {System.err.println("erreur recherche");}  	
    }
    /* test de la methode du tri Rapide  par leur nom */
    public static void ScenarioPopulationRangerNomRapide() {
    	Population pop = new Population("../data/worldpop.txt" ,"../data/worldarea.txt");
    	ArrayList<Pays> pays = pop.getListePays();
       	Pays[] tPays = new Pays[pays.size()];
       	for (int i = 0; i<pays.size() ; i++) {
       		tPays[i] = pays.get(i);
       	}
       	triRapideRecAlpha tri = new triRapideRecAlpha(tPays);
        tri.trier();
        System.out.println (Arrays.toString(tPays));
    }
    /* Comparer les tri par leur nom */
    public static void ScenarioPopulationCompareTri() {
    	long t1;
		long t2;
		long diffT1;
		long diffT2;
    	Population pop = new Population("../data/worldpop.txt" ,"../data/worldarea.txt");
    	ArrayList<Pays> pays = pop.getListePays();
       	Pays[] tPays = new Pays[pays.size()];
       	for (int i = 0; i<pays.size() ; i++) {
       		tPays[i] = pays.get(i);
       	}
       	t1 = System.nanoTime();
       	TriParSelectionAlpha tri = new TriParSelectionAlpha(tPays);
        tri.trier();
        t2 = System.nanoTime();
		diffT1= (t2 - t1);
    	Population pop2 = new Population("../data/worldpop.txt" ,"../data/worldarea.txt");
    	ArrayList<Pays> pays2 = pop2.getListePays();
       	Pays[] tPays2 = new Pays[pays2.size()];
       	for (int i = 0; i<pays2.size() ; i++) {
       		tPays2[i] = pays2.get(i);
       	}
       	t1 = System.nanoTime();
       	triRapideRecAlpha tri2 = new triRapideRecAlpha(tPays);
        tri2.trier();
        t2 = System.nanoTime();
		diffT2= (t2 - t1);
		if (diffT1>diffT2) { System.out.println ("le Tri Rapide et meilleur que le tri par selection (" +diffT2 +" ns contre " +diffT1 +" ns)" );}
		else {System.out.println ("le tri par selection et meilleur que  le Tri Rapide(" +diffT1 +" ns contre " +diffT2 +" ns)" );}
    }
    /* trouver un pays selectionner*/
    public static void ScenarioCherchePaysSelectionner() {
    	Population pop = new Population("../data/worldpop.txt" ,"../data/worldarea.txt");
    	ArrayList<Pays> pays = pop.getListePays();
       	Pays[] tPays = new Pays[pays.size()];
       	for (int i = 0; i<pays.size() ; i++) {
       		tPays[i] = pays.get(i);
       	}
       	TriParSelectionAlpha tri = new TriParSelectionAlpha(tPays);
        tri.trier();
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez saisir un mot :");
        String choix = sc.nextLine();
       	BinarySearcher cherche = new BinarySearcher(tPays , pays.size() , choix);
       	int in = cherche.rechercher();
       	if (in != -1) {
	       	if ( tPays[in].getNom().compareTo(choix) == 0) {
	       		System.out.println (tPays[in].toString());
	       		Scanner sc2 = new Scanner(System.in);
	            System.out.println("Voulez vous la dencité ? (oui ou non)");
	            String choix2 = sc2.nextLine();
	            while (choix2.compareTo("oui") != 0 && choix2.compareTo("non") != 0 ) {
	            	System.out.println("Veullier resaisir");
	                choix2 = sc2.nextLine();
	            }
	            if (choix2.compareTo("oui") == 0) {
	            	HashMap<String, Double> dencite = pop.computeDensity();
	            	System.out.println("dencité :" + dencite.get(choix));
	            }
	       	}
       	}
       	else {System.err.println("Le pays saisie n'est pas dans notre liste");} 
       	  	
    }
}
