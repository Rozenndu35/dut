/**
 * The interface for the sort
 * @author Rozenn
 * @version 3
 */

package tri;

public interface ITri {
	public void trier();
}
