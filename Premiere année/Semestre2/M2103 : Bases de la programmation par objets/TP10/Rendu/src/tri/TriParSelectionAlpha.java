/**
 * Class to make a sort by selection
 * @author Rozenn
 * @version 3
 */

package tri;
import pays.Pays;

public class TriParSelectionAlpha implements ITri {

	private Pays[] tab;
	
	/**
	 * the constructor
	 * @param tab : the tab to want a sort
	 */
	public TriParSelectionAlpha(Pays[] tab) {
		if (tab != null) {
			this.tab=tab;
		}
		else {
			this.tab= new Pays[0];
		}
	}
	/**
	 * looking for the smallest value
	 * @param debut : the start of the research
	 * @return the index
	 */
	private int mininumPosition(int debut) {
		int ret = debut;
		int plusGrand;
		for (int i = debut+1 ; i < this.tab.length ; i++){
			plusGrand = tab[ret].compareTo(tab[i], "oui");
			if (plusGrand == -1){
				ret = i;
			}
		}
		return ret;
	}
	
	/**
	 * swap to value
	 * @param i : first value 
	 * @param j : second value
	 */
	private void swap (int i , int j) {
		Pays change = tab[i];
		tab[i] = tab[j];
		tab[j] = change;
	}
	
	/**
	 * sort the Pays array by arrays
	 */
	public void trier() {
		int min;
		for(int i=0;i< tab.length ; i++){
			min=this.mininumPosition(i);
			swap(i, min);
		}

	}

}
