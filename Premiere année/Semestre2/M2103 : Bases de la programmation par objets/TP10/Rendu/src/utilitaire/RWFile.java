package utilitaire;


import java.io.*;
import java.util.*;
/**
 * 
 * @author rozenn
 * @version 3
 */
public class RWFile {
	/**
	 * create a list with the information in a file
	 * @param fileName : the name of the list
	 * @return a new list with information
	 */
	public static ArrayList<String> readFile (String fileName){
		ArrayList<String> ret = new ArrayList<String>();
		try {
			Scanner in = new Scanner (new FileReader (fileName));
			while (in.hasNextLine()) {
				ret.add(in.nextLine());
			}
			in.close();
		} 
		catch (FileNotFoundException e) {
			System.out.println ("readFile- Fichier non trouve: " + fileName);
		}
		return ret;
		
	}
	
	/**
	 * save the list in the file Name in parameter
	 * @param liste : the list to want write in a file
	 * @param fileName : the file where you want save the list
	 */
	public static void writeFile (ArrayList<String> liste , String fileName) {
		try {
			PrintWriter out = new PrintWriter (fileName);
			for( String ligne : liste) {
				out.println (ligne);
			}
			out.close();
		}
		catch (FileNotFoundException e) {
			System.out.println (e.getMessage());
		}
	}
		
}
