/*
 * The scenario
 *@author Rozenn Costiou 
*/
import question.*;
import java.util.*;
import java.lang.*;


public class ScenarioChoiseQuestion {
    /**
 	 * The instance of classe
	 * @param args :  
 	*/
    private Questionnaire questionnaire;

	public static void main(String[] args){
        Scanner jouer;
        Question question;
        Scanner reponse;
        Scanner oui = new Scanner ("oui");
        boolean check;
        int cpt = 0;
        int gagner = 0;
        initialiseQuesionnaire();
        jouer = new Scanner ("Vouler vous jouer? " + System.in );
        while (jouer == oui){ 
            cpt ++;
            question = questionnaire.pickAtRandom();
            question.display();
            reponse = new Scanner ("Votre reponse : " + System.in );
            check = question.checkAnswer(reponse);
            if (check == true){
                System.out.println("Vous avez gagner");
                gagner ++;
            }
            jouer = new Scanner ("Vouler vous jouer? " + System.in );
        }
        System.out.println("Vous avez gagner " + gagner + " sur " + cpt + " partie jouer" );
    }
    public static void initialiseChoise(){ 
        String reponse1;
        String reponse2;
        String reponse3;
        String reponse4;
        Question question1;
        ChoiceQuestion choixlist;
        reponse1 = " le roi";
        reponse2 = " la dame";
        reponse3 = " le fou";
        reponse4 = " le cavalier";
        question1 = new Question("Quelle pièce est absolument à protéger dans un jeu d’échec ?","");
        choixList =  new ChoiceQuestion();
        choixList.addChoise (reponse1 , true);
        choixList.addChoise (reponse2 , false);
        choixList.addChoise (reponse2 , false);
        choixList.addChoise (reponse2 , false);
        
    } 
}