/*
 * The scenario
 *@author Rozenn Costiou 
*/
import question.*;
import java.util.*;
import java.lang.*;


public class ScenarioQuestion {
    /**
 	 * The instance of classe
	 * @param args :  
 	*/
    private Questionnaire questionnaire;

	public static void main(String[] args){
        Scanner jouer;
        Question question;
        Scanner reponse;
        Scanner oui = new Scanner ("oui");
        boolean check;
        int cpt = 0;
        int gagner = 0;
        initialiseQuesionnaire();
        jouer = new Scanner ("Vouler vous jouer? " + System.in );
        while (jouer == oui){ 
            cpt ++;
            question = questionnaire.pickAtRandom();
            question.display();
            reponse = new Scanner ("Votre reponse : " + System.in );
            check = question.checkAnswer(reponse);
            if (check == true){
                System.out.println("Vous avez gagner");
                gagner ++;
            }
            jouer = new Scanner ("Vouler vous jouer? " + System.in );
        }
        System.out.println("Vous avez gagner " + gagner + " sur " + cpt + " partie jouer" );
    }
    public static void initialiseQuesionnaire(){ 
        Question question1;
        Question question2;
        Question question3;
        Question question4;
        Question question5;
        Question question6;
        Question question7;
        Question question8;
        Question question9;
        Question question10;
        question1 = new Question("Quelle pièce est absolument à protéger dans un jeu d’échec ?","Le roi");
        question2 = new Question("Quel est la capitale de l’Australie ?","Canberra");
        question3 = new Question("Quelle année a suivi l’an 1 avant JC ? ", "L’an 1 après JC");
        question4 = new Question("Qui sont les animateurs du festival « juste pour rire » ?","Stéphane rousseau, franck dubosc");
        question5 = new Question("Combien de nouvelles chaînes sont apparus grâce à la TNT ?", "12");
        question6 = new Question("Combien y a-t-il de signes astrologiques chinois ? ","12");
        question7 = new Question("Quel est le 2ème nom de l’hippocampe ?","Le cheval de mer");
        question8 = new Question("En quelle année est mort JFK ? ", "1963");
        question9 = new Question("Combien de dieu trône a l’Olympe ?", "12");
        question10 = new Question("Quel l’équivalent du pape au Tibet ?","Le dallai lama");
        questionnaire = new Questionnaire();
        questionnaire.add(question1);
        questionnaire.add(question2);
        questionnaire.add(question3);
        questionnaire.add(question4);
        questionnaire.add(question5);
        questionnaire.add(question6);
        questionnaire.add(question7);
        questionnaire.add(question8);
        questionnaire.add(question9);
        questionnaire.add(question10);
    } 
}