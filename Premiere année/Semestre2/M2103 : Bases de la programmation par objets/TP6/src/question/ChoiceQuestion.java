/**
 * Create the choises answer
 *@author Rozenn Costiou 
*/
package question;
import java.util.ArrayList;
import java.lang.*;


public class ChoiceQuestion extends Question{
    private ArrayList <String> choices;

    /**
     * Create the choices
    */
    public ChoiceQuestion(){
        this.choices = new ArrayList<String> (); 
    }

    /**
     * add a choise in the list
     * @param choice : the choise
     * @param correct : true if the good answer of a question
    */
    public void addChoise (String choice , boolean correct){
        if (choice != null){
            choices.add(choice);
            if(correct == true){
                super.setAnswer(Integer.toString(choices.size()));
            }
        }
    }
    /**
     * display on the screen the question and the choises
     */
    public void display () {
        super.display();
        int i =0;
        for ( String s : choices){
            System.out.println( i+1 + " - " + s );
            i++;
        }
    }
}