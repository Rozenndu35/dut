package reversi;
/**
 * the different mode of game
 * @author rozenn
 * version 1
 */
public enum Mode {
  HH, // humain avec humain
  HA, // humain avec automatique
  AH, // humain avec automatique
  AA; // automatique aec automatique
	
}