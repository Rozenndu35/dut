package reversi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Scanner;

import view.EndGameFrame;
import view.SelectJoueur;

/**
 * class to order the game
 * @author rozenn
 * @version 1
 */
public class Game implements IGame , Serializable {
	private boolean aJ1 = true;
	private Board board;
	private Player player1;
	private Player player2;
	private int nbCoinRestant;
	
	/**
	 * the constructor
	 * @param mode : the mode of the gamme
	 * @param taille : the length of the table
	 * @param nbCoin : tne numbur of the coin
	 */
	public Game ( Mode mode, int taille, int nbCoin)  {
		System.out.println (description());

		this.nbCoinRestant =nbCoin;

		if (mode ==  Mode.HH) {

			SelectJoueur sj = new SelectJoueur(taille, nbCoin, mode);
			while(sj.waiting()) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
				}
			}
			sj.dispose();
			if ( !sj.charger) {
				initializeBoard(taille);
				this.player1 = new HumanPlayer(sj.getJ1(), this.board,1);
				this.player2 = new HumanPlayer(sj.getJ2(), this.board,2);
			}
			else {
				try {
					lectureFileEnregistrer(sj.getLoadingPath());
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
			
		}
		else if (mode ==  Mode.HA) {

			SelectJoueur sj = new SelectJoueur(taille, nbCoin, mode);
			sj.disableJ2();
			while(sj.waiting()) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
				}
			}
			sj.dispose();
			initializeBoard(taille);
			if ( !sj.charger) {
				this.player1 = new HumanPlayer(sj.getJ1(), this.board,1);
				Difficulter dif = sj.getDifJ2();
				this.player2 = new AutoPlayer(sj.getJ2(), this.board,2, dif);
			}
			else {
				try {
					lectureFileEnregistrer(sj.getLoadingPath());
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
		else if (mode ==  Mode.AA) {
			SelectJoueur sj = new SelectJoueur(taille, nbCoin, mode);
			sj.disableJ1J2();

			while(sj.waiting()) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
				}
			}
			sj.dispose();
			if ( !sj.charger) {
				initializeBoard(taille);
				Difficulter dif1 = sj.getDifJ1();
				this.player1 = new AutoPlayer(sj.getJ1(), this.board,1, dif1);
				Difficulter dif2 = sj.getDifJ2();
				this.player2 = new AutoPlayer(sj.getJ2(), this.board,2, dif2);
			}
			else {
				try {
					lectureFileEnregistrer(sj.getLoadingPath());
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
		else if (mode ==  Mode.AH) {

			SelectJoueur sj = new SelectJoueur(taille, nbCoin, mode);
			sj.disableJ1();
			while(sj.waiting()) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
				}
			}

			sj.dispose();
			if ( !sj.charger) {
				initializeBoard(taille);
				Difficulter dif = sj.getDifJ1();
				this.player1 = new AutoPlayer(sj.getJ1(), this.board,1, dif);
				this.player2 = new HumanPlayer(sj.getJ2(), this.board,2);
			}
			else {
				try {
					lectureFileEnregistrer(sj.getLoadingPath());
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
		else {
			initializeBoard(taille);
			this.player1 = new AutoPlayer("Ordi1", this.board,1, Difficulter.FACILE);
			this.player2 = new AutoPlayer("Ordi2", this.board,2,Difficulter.FACILE);
			System.err.println("reversi.Game : Game : le mode n'a pas été rentrer deux ordinateur jouerons");
		}
		System.out.println(board.toString());
		start();
	}

	/**
	 * initilize the table for the game
	 * @param taille : the length of the table
	 */
	private void initializeBoard(int taille) {
		board = new Board(taille);
	}

	/**
	 * the rule of the game
	 */
	public String description() {
		String ret = "\n" + "Saisir votre nom au debut de la partie ";
		ret += "\n" + "Saisir les coordonnées ou vous voulez poser votre poin en clican sur la case dans le tableau et valider avec le bouton";
		ret += "\n" + "respecter cela :";
		ret += "\n" +"\t"+ "vous devez obligatoirement manger un pion de l'adversaire dans les diagonale ou la colonne";
		ret += "\n" ;
		ret += "\n" + "Le premier qui gagne est celui qui :";
		ret +="\n" + "\t" + "a manger tout les pion adverse";
		ret += "\n"+ "\t"+ "ou  a le plus de jeton lorsque persone peut jouer ou que les pion sont tout tombé" ;
		ret += "\n"+ "bonne partie";
		ret += "\n";
		return ret;
	}

	/**
	 * the course of the games
	 */
	public void start() {
		aJ1 = true;
		int past1 = 0;
		int past2 = 0;
		boolean arret = false;
		String end  = end(1);
		while(!arret && end != "end") { // si ce n'est pas la fin
			//____________________________________________________________Joueur 1
			if(aJ1) {
				end = end(1);
				aJ1 = true;
				if (end != "past") {  // si il peut jouer
					this.board.otframe.setImgTour(true);
					player1.play(board.otframe);
					if(board.otframe.getHaveToSave()) {
						this.ecritureFileEnregistrer();
					}
					this.nbCoinRestant --;
					System.out.println(board.toString());
					past2 = 0;
				}else if (end == "past" && past2 == 1) { // si il doit passer et le joeur avant la fait
					int nbB =0;
					int nbN =0;
					Square[][] tab = board.getGrid();
					for (int i = 0; i < tab.length  ; i++) {
						for (int j = 0 ; j< tab[0].length ; j++) {
							if (tab[i][j].getColor() == CoinColor.WHITE) {
								nbB ++;
							}
							else if (tab[i][j].getColor() == CoinColor.BLACK) {
								nbN++;
							}
						}
					}
					if (nbB < nbN) { // si il reste aucun blanc
						endOfGame("joueur " + player1.getName() + " a gagner!");
					}
					else { // Si il reste aucun noir
						endOfGame("joueur " + player2.getName() + " a gagner!");
					}
					arret = true;
				}else if(end == "past") {// si il doit passer
					System.out.println("vous n'avez pas de possibilité c'est a l'autre jour de jouer");
					past1 = 1;
				}
			}
			System.out.println("____________________________________________________________Joueur 2");
			end = end(2);
			if (end == "end") {
				arret = true; 
			}else if(end != "past") { // si il peut jouer
				this.board.otframe.setImgTour(false);
				player2.play(board.otframe);
				if(board.otframe.getHaveToSave()) {
					this.ecritureFileEnregistrer();
				}
				this.nbCoinRestant --;
				System.out.println(board.toString());
				past1 = 0;
			}else if (end == "past" && past1 == 1) {// si il doit passer et le joeur avant la fait
				int nbB =0;
				int nbN =0;
				Square[][] tab = board.getGrid();
				for (int i = 0; i < tab.length  ; i++) {
					for (int j = 0 ; j< tab[0].length ; j++) {
						if (tab[i][j].getColor() == CoinColor.WHITE) {
							nbB ++;
						}
						else if (tab[i][j].getColor() == CoinColor.BLACK) {
							nbN++;
						}
					}
				}
				if (nbB < nbN) { // si il reste aucun blanc
					endOfGame("joueur " + player1.getName() + " a gagner!");
				}
				else { // Si il reste aucun noir
					endOfGame("joueur " + player2.getName() + " a gagner!");
				}
				arret = true;
			}else if(end == "past") { // si il doit passer
				System.out.println("vous n'avez pas de possibilité c'est a l'autre jour de jouer");
				past2 = 1;
			}
			end = end(1);
			aJ1=true;
		}				
	}

	/**
	 * write the winner of the game
	 * @param term : the player win
	 */
	public void endOfGame(String term) {
		System.out.println("partie fini : " + term);
		EndGameFrame windo = new EndGameFrame("partie fini : " + term);
		board.otframe.dispose();
		
	}

	/**
	 * check if is the end
	 * @param jou : the number of player play
	 * @return end if is the the end , past if the player can't play; cont if is good
	 */
	public String end(int jou) {
		String ret = "cont";
		int nbB =0;
		int nbN = 0;
		int nbF=0;
		int posibiliteN = 0;
		int posibiliteB = 0;
		Square[][] tab = board.getGrid();
		for (int i = 0; i < tab.length  ; i++) {
			for (int j = 0 ; j< tab[0].length ; j++) {
				if (tab[i][j].getColor() == CoinColor.WHITE) {
					nbB ++;
					boolean ret1 = board.checkAlignment(CoinColor.WHITE, CoinColor.BLACK , i, j, true, false); // Si dans la colone en haut
					boolean ret2 = board.checkHAlignement(CoinColor.WHITE, CoinColor.BLACK , i, j, true, false); // Si dans la diagonale haut gauche
					boolean ret3 = board.checkLAlignement(CoinColor.WHITE, CoinColor.BLACK , i, j, true, false); // Si dans la diagonale haut droite
					boolean ret4 = board.checkWAlignement(CoinColor.WHITE, CoinColor.BLACK , i, j, true, false); // Si dans la diagonale bas gauche
					boolean ret5 = board.checkVAlignement(CoinColor.WHITE, CoinColor.BLACK , i, j, true, false); // Si dans la diagonale bas droite
					boolean ret6 = board.checkXAlignement(CoinColor.WHITE, CoinColor.BLACK , i, j, true, false); // Si dans la colone en bas
					if (ret1 == true || ret2 == true || ret3 == true || ret4 == true || ret5 == true || ret6 == true) {
						posibiliteB = 1;
					}
				}
				else if (tab[i][j].getColor() == CoinColor.BLACK) {
					nbN++;
					boolean ret1 = board.checkAlignment(CoinColor.BLACK, CoinColor.WHITE , i, j, true, false); // Si dans la colone en haut
					boolean ret2 = board.checkHAlignement(CoinColor.BLACK, CoinColor.WHITE , i, j, true, false); // Si dans la diagonale haut gauche
					boolean ret3 = board.checkLAlignement(CoinColor.BLACK, CoinColor.WHITE , i, j, true, false); // Si dans la diagonale haut droite
					boolean ret4 = board.checkWAlignement(CoinColor.BLACK, CoinColor.WHITE , i, j, true, false); // Si dans la diagonale bas gauche
					boolean ret5 = board.checkVAlignement(CoinColor.BLACK, CoinColor.WHITE , i, j, true, false); // Si dans la diagonale bas droite
					boolean ret6 = board.checkXAlignement(CoinColor.BLACK, CoinColor.WHITE , i, j, true, false); // Si dans la colone en bas
					if (ret1 == true || ret2 == true || ret3 == true || ret4 == true || ret5 == true || ret6 == true) {
						posibiliteN = 1;
					}
				}
				else if (tab[i][j].getColor() == CoinColor.FREE) {
					nbF++;
				}
			}
		}
		if ( nbB != 0 && nbN !=0) { // verifie si il reste des jeton de chaque couleur
			if (posibiliteB == 1 && jou == 2 ) { // verifie si il y a une possibilité
				ret = "cont";
			}
			else if (posibiliteN == 1 && jou ==1 ) { // verifie si il y a une possibilité
				ret = "cont";
			}
			else {
				ret = "past";
			}

		}else {
			ret = "end";
			if (nbB == 0) { // si il reste aucun blanc
				endOfGame( player1.getName() + " a gagner!");
			}
			else { // Si il reste aucun noir
				endOfGame( player2.getName() + " a gagner!");
			}
		}	
		if (nbF == 0) {
			ret = "end";
			for (int i = 0; i < tab.length  ; i++) {
				for (int j = 0 ; j< tab[0].length ; j++) {
					if (tab[i][j].getColor() == CoinColor.WHITE) {
						nbB ++;
					}
					else if (tab[i][j].getColor() == CoinColor.BLACK) {
						nbN++;
					}
				}
			}
			if (nbB < nbN) { // si il y a plus de noir
				endOfGame( player1.getName() + " a gagner!");
			}
			else if (nbB == nbN)  { // si egalite
				endOfGame(" egalite");
			}
			else { // si il y a plus de blanc
				endOfGame(player2.getName() + " a gagner!");
			}
		}
		return ret;	
	}

	/**
	 * write the information in the file object
	 */
	public void ecritureFileEnregistrer() {
		try {
			// Store Serialized User Object in File
			FileOutputStream fileOutputStream = new FileOutputStream(this.board.otframe.getSavingPath());
			ObjectOutputStream output = new ObjectOutputStream(fileOutputStream);
			output.writeObject(this);
			System.out.println ("--------------------------partie enregistrer---------------------");
			output.close();
			

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
		}

		this.board.otframe.setHaveToSave(false);
	}

	/**
	 * read the information in the file object
	 * @param path : the path of the file with have a saving game
	 * @throws ClassNotFoundException : the exeption
	 */
	public void lectureFileEnregistrer(String path) throws ClassNotFoundException {
		try {

			//Read from the stored file
			FileInputStream fileInputStream = new FileInputStream(new File(path));
			ObjectInputStream input = new ObjectInputStream(fileInputStream);
			// ______________________________ reprendre les objet un part un
			Game recup = (Game) input.readObject();
			this.aJ1 = recup.aJ1;
			this.board = recup.board;
			this.player1 = recup.player1;
			this.player2 = recup.player2;
			this.nbCoinRestant = recup.nbCoinRestant;
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		System.out.println ("partie charger");

	}
}
