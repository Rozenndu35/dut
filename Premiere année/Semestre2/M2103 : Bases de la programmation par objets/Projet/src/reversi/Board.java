package reversi;
import java.io.Serializable;

import view.GridTableFrame;

/**
 * the class to make the board and to check the change of the coin
 * @author rozenn
 * @version 1
 */
public class Board implements Serializable{
	private int width; // largeur
	private int height; // hauteur
	private Square[][] grid;
	public GridTableFrame otframe;
	public int nbCoinChange;
	
	/**
	 * the constructor of the table
	 * @param taille : the length of the table
	 */
	public Board(int taille) {
		//creer la grille
		int i = 0;
		int free = taille;
		this.height = (taille *2);
		this.width = (taille *2) + (((taille -2)*2)+1);
		int m = this.width -1;
		this.grid = new Square[this.width][this.height];
		// creer la partie gauche de la grille jusqu'a taille
	    while (i <= m) {
	    	int j = 0;
	      	while (j < free) {
	      		grid[i][j] = new Square();
	      		grid[m][j] = new Square();
	      		grid[i][j].setColor(CoinColor.NONE);
		    	grid[m][j].setColor(CoinColor.NONE);
		    	
	      		
	      		grid[i][j].setX(i);
	      		grid[i][j].setY(j);
	      		
	      		grid[m][j].setX(m);
	      		grid[m][j].setY(j);
		    	
		    	
		    	j++;
		    }
		    while(j <= taille) {
		    	if (free == taille) {
		    		grid[i][j] = new Square();
		      		grid[m][j] = new Square();
		      		grid[i][j].setColor(CoinColor.FREE);
		    		grid[m][j].setColor(CoinColor.FREE);
		    		
		      		grid[i][j].setX(i);
		      		grid[i][j].setY(j);
		      		
		      		grid[m][j].setX(m);
		      		grid[m][j].setY(j);
		      		
		    		
		    		j++;
		    	}
		    	else {
		    		grid[i][j] = new Square();
		      		grid[m][j] = new Square();
		      		grid[i][j].setColor(CoinColor.FREE);
			    	grid[m][j].setColor(CoinColor.FREE);
			    	
		      		grid[i][j].setX(i);
		      		grid[i][j].setY(j);
		      		
		      		grid[m][j].setX(m);
		      		grid[m][j].setY(j);
		    		
			    	j++;
			    	grid[i][j] = new Square();
		      		grid[m][j] = new Square();
		      		grid[i][j].setColor(CoinColor.NONE);
		    		grid[m][j].setColor(CoinColor.NONE);
		    		
		    		
		      		grid[i][j].setX(i);
		      		grid[i][j].setY(j);
		      		
		      		grid[m][j].setX(m);
		      		grid[m][j].setY(j);
			    	
		    		j++;
		    	}
		    }
		    
		    
		    if (free != 1) {
		    	free --;
		    }
		    else if (free == 1) {
		    	free ++;
		    }
		    i++;
		    m--;
		    
	    }
	    
	    // creer la partie droit de la grille a partir taille + 1
	    i =0;
	    m = this.width -1;
	    free = taille;
	    while (i <= m) {
	    	int j = this.height-1;
	    	while (j > free) {
	      		grid[i][j] = new Square();
	      		grid[m][j] = new Square();
		    	grid[i][j].setColor(CoinColor.NONE);
		    	grid[m][j].setColor(CoinColor.NONE);
		    	
		    	grid[i][j].setX(i);
	      		grid[i][j].setY(j);
	      		
	      		grid[m][j].setX(m);
	      		grid[m][j].setY(j);
		    	
		    	j--;
		    }
		    while(j >= taille +1) {
		    	if (free == taille) {
		    		grid[i][j] = new Square();
		      		grid[m][j] = new Square();
		    		grid[i][j].setColor(CoinColor.FREE);
		    		grid[m][j].setColor(CoinColor.FREE);
		    		
		    		grid[i][j].setX(i);
		      		grid[i][j].setY(j);
		      		
		      		grid[m][j].setX(m);
		      		grid[m][j].setY(j);
		      		
		    		j--;
		    		grid[i][j] = new Square();
		      		grid[m][j] = new Square();
			    	grid[i][j].setColor(CoinColor.NONE);
		    		grid[m][j].setColor(CoinColor.NONE);
		    		
		    		grid[i][j].setX(i);
		      		grid[i][j].setY(j);
		      		
		      		grid[m][j].setX(m);
		      		grid[m][j].setY(j);
		      		
		    		j--;
		    	}
		    	else {
		    		grid[i][j] = new Square();
		      		grid[m][j] = new Square();
		    		grid[i][j].setColor(CoinColor.FREE);
			    	grid[m][j].setColor(CoinColor.FREE);
			    	
			    	grid[i][j].setX(i);
		      		grid[i][j].setY(j);
		      		
		      		grid[m][j].setX(m);
		      		grid[m][j].setY(j);
		      		
			    	j--;
			    	grid[i][j] = new Square();
		      		grid[m][j] = new Square();
			    	grid[i][j].setColor(CoinColor.NONE);
		    		grid[m][j].setColor(CoinColor.NONE);
		    		
		    		grid[i][j].setX(i);
		      		grid[i][j].setY(j);
		      		
		      		grid[m][j].setX(m);
		      		grid[m][j].setY(j);
		      		
		    		j--;
		    	}
		    }
		    
		    i++;
		    m--;
		    if (free != this.height-1) {
		    	free ++;
		    }
		    else if (free == this.height-1) {
		    	free --;
		    }
		    
	    }
	    
	    //create and show the graphical grid
		otframe = new GridTableFrame(this.getGrid());
		otframe.showIt("Reversi");
		
	    changeCoin((this.width/2), taille, CoinColor.BLACK);
	    changeCoin(((this.width/2)-1), (taille-1), CoinColor.BLACK);
	    changeCoin(((this.width/2)-1), (taille+1), CoinColor.BLACK);
	    changeCoin(((this.width/2)-2), taille, CoinColor.WHITE);
	    changeCoin(((this.width/2)+1), (taille-1), CoinColor.WHITE);
	    changeCoin(((this.width/2)+1), (taille+1), CoinColor.WHITE);
		
	   
	}
	
	/**
	 * get the table
	 * @return the table
	 */
	public Square[][] getGrid() {
		return this.grid;
	}

	/**
	 * Check if you can put or check if you can and change the color when they have a pawn in below
	 * @param color : the color of the player
	 * @param couleur : the color of the other player
	 * @param x : the coordinated
	 * @param y : the coordinated
	 * @param verif : true if you want check possibility or false if you want play
	 * @param conte if is conte the number of coin change
	 * @return true if you can or it's possible
	 */
	public boolean checkAlignment(CoinColor color, CoinColor couleur , int x, int y, boolean verif, boolean conte) {
		boolean ret = false;
		if (x<this.width-2 && (grid[x+2][y].getColor() == couleur)&& !verif) { // Si dans la colone en bas
			int xe = x+4;
			boolean stop = false;
			boolean arret = false;
			while (xe<this.width && grid[xe][y].getColor() != CoinColor.NONE && grid[xe][y].getColor() != CoinColor.FREE &&   stop == false && arret == false) { // cherche si il y a au dessous
				if (grid[xe][y].getColor() == color) { 
					ret = true;
					if(!conte) {
						changeCoin(x,y,color);
					}
					else {
						nbCoinChange ++;
					}
					for ( int i = x; i < xe ; i= i+2) {
						if(!conte) {
							changeCoin(i,y,color);
						}
						else {
							nbCoinChange ++;
						}
					}
					stop = true;
				}
				xe = xe+2;
			}		}
		if (x<this.width-2 && (grid[x+2][y].getColor() == couleur)&& verif) { // Si dans la colone en bas
			int xe = x+4;
			boolean stop = false;
			boolean arret = false;
			while (xe<this.width && grid[xe][y].getColor() != CoinColor.NONE && grid[xe][y].getColor() != color &&   stop == false && arret == false) { // cherche si il y a au dessous
				if (grid[xe][y].getColor() == CoinColor.FREE) { 
					ret = true;
					stop = true;
				}
				xe = xe+2;
			}
		}
		return ret;
	}
	
	/**
	 * Check if you can put or check if you can and change the color when they have a pawn in diagonal top left
	 * @param color : the color of the player
	 * @param couleur : the color of the other player
	 * @param x : the coordinated
	 * @param y : the coordinated
	 * @param verif : true if you want check possibility or false if you want play
	 * @param conte if is conte the number of coin change
	 * @return true if you can or it's possible
	 */
	public boolean checkHAlignement(CoinColor color, CoinColor couleur , int x, int y, boolean verif, boolean conte) {
		boolean ret = false;
		if (x > 0 && y> 0 && (grid[x-1][y-1].getColor() == couleur)&& !verif) {// Si dans la diagonale haut gauche
			int xe = x-2;
			int ye = y-2;
			boolean stop = false;
			while (xe>=0 && ye>=0 && grid[xe][ye].getColor() != CoinColor.NONE && grid[xe][ye].getColor() != CoinColor.FREE &&  stop == false ) {
				if (grid[xe][ye].getColor() == color ) { 
					ret = true;
					if(!conte) {
						changeCoin(x,y,color);
					}
					else {
						nbCoinChange ++;
					}
					int i = x-1;
					int j = y-1;
					while ( i > xe && j> ye ) {
						if(!conte) {
							changeCoin(i,j,color);
						}
						else {
							nbCoinChange ++;
						}
						i--;
						j--;
					}
					stop = true;
				}
				xe --;
				ye --;
			}
		}
		else if (x > 0 && y> 0 && (grid[x-1][y-1].getColor() == couleur)&& verif) {// Si dans la diagonale haut gauche
			int xe = x-2;
			int ye = y-2;
			boolean stop = false;
			while (xe>=0 && ye>=0 && grid[xe][ye].getColor() != CoinColor.NONE && grid[xe][ye].getColor() != color  &&  stop == false ) {
				if (grid[xe][ye].getColor() == CoinColor.FREE) { 
					ret = true;
					stop = true;
				}
				xe --;
				ye --;
			}
		}
		return ret;
			
	}
	
	/**
	 * Check if you can put or check if you can and change the color when they have a pawn in diagonal top right
	 * @param color : the color of the player
	 * @param couleur : the color of the other player
	 * @param x : the coordinated
	 * @param y : the coordinated
	 * @param verif : true if you want check possibility or false if you want play
	 * @param conte if is conte the number of coin change
	 * @return true if you can or it's possible
	 */
	public boolean checkLAlignement(CoinColor color, CoinColor couleur , int x, int y, boolean verif, boolean conte) {
		boolean ret = false;
		if (x> 0 && y < this.height-1 && (grid[x-1][y+1].getColor() == couleur) && !verif) {// Si dans la diagonale haut droite
			int xe = x-1;
			int ye = y+1;
			boolean stop = false;
			while (xe >=0 && ye < this.height && grid[xe][ye].getColor() != CoinColor.NONE && grid[xe][ye].getColor() != CoinColor.FREE && stop == false ) {
				if (grid[xe][ye].getColor() == color   ) { 
					ret = true;
					if(!conte) {
						changeCoin(x,y,color);
					}
					else {
						nbCoinChange ++;
					}
					int i = x-1;
					int j = y + 1;
					while ( i > xe && j<ye) {
						if(!conte) {
							changeCoin(i,j,color);
						}
						else {
							nbCoinChange ++;
						}
						i--;
						j++;
					}
					stop = true;
				}
				xe --;
				ye ++;
			}
		}
		else if (x> 0 && y < this.height-1 && (grid[x-1][y+1].getColor() == couleur) && verif ) {// Si dans la diagonale haut droite
			int xe = x-1;
			int ye = y+1;
			boolean stop = false;
			while (xe >=0 && ye < this.height && grid[xe][ye].getColor() != CoinColor.NONE && grid[xe][ye].getColor() != color  && stop == false ) {
				if (grid[xe][ye].getColor() == CoinColor.FREE ) { 
					ret = true;
					stop = true;
				}
				xe --;
				ye ++;
			}
		}
		return ret;
	}
	
	/**
	 * Check if you can put or check if you can and change the color when they have a pawn in diagonal low left
	 * @param color : the color of the player
	 * @param couleur : the color of the other player
	 * @param x : the coordinated
	 * @param y : the coordinated
	 * @param verif : true if you want check possibility or false if you want play
	 * @param conte if is conte the number of coin change
	 * @return true if you can or it's possible
	 */
	public boolean checkWAlignement(CoinColor color, CoinColor couleur , int x, int y, boolean verif, boolean conte) {
		boolean ret = false;
		if (x<this.width-1 && y> 0 && (grid[x+1][y-1].getColor() == couleur) && !verif) { // Si dans la diagonale bas gauche 
			int xe = x+1;
			int ye = y -1;
			boolean stop = false;
			
			while (ye>=0 && xe <this.width && grid[xe][ye].getColor() != CoinColor.NONE && grid[xe][ye].getColor() != CoinColor.FREE &&  stop == false ) { 
				if (grid[xe][ye].getColor() == color   ) { 
					ret = true;
					if(!conte) {
						changeCoin(x,y,color);
					}
					else {
						nbCoinChange ++;
					}
					int i = x+1;
					int j = y - 1;
					while (i < xe && j> ye) {
						if(!conte) {
							changeCoin(i,j,color);
						}
						else {
							nbCoinChange ++;
						}
						i++;
						j--;
					}
					stop = true;
				}
				xe ++;
				ye --;
			}
		}
		else if (x<this.width-1 && y> 0 && (grid[x+1][y-1].getColor() == couleur) && verif) { // Si dans la diagonale bas gauche 
			int xe = x+1;
			int ye = y -1;
			boolean stop = false;
			
			while (ye>=0 && xe <this.width && grid[xe][ye].getColor() != CoinColor.NONE && grid[xe][ye].getColor() != color &&  stop == false ) { 
				if (grid[xe][ye].getColor() == CoinColor.FREE    ) { 
					ret = true;
					stop = true;
				}
				xe ++;
				ye --;
			}
		}
		
		return ret;
	}
	
	/**
	 * Check if you can put or check if you can and change the color when they have a pawn in diagonal low right
	 * @param color : the color of the player
	 * @param couleur : the color of the other player
	 * @param x : the coordinated
	 * @param y : the coordinated
	 * @param verif : true if you want check possibility or false if you want play
	 * @param conte : if is conte the number of coin change
	 * @return true if you can or it's possible
	 */
	public boolean checkVAlignement(CoinColor color, CoinColor couleur , int x, int y,boolean verif, boolean conte) {
		boolean ret = false;
		if (x<this.width-1 && y < this.height-1 && (grid[x+1][y+1].getColor() == couleur)&& !verif) {// Si dans la diagonale bas droite
			int xe = x+2;
			int ye = y+2;
			boolean stop = false;
			while (xe <this.width && ye<this.height && grid[xe][ye].getColor() != CoinColor.NONE && grid[xe][ye].getColor() != CoinColor.FREE && stop == false ) {
				if (grid[xe][ye].getColor() == color ) { 
					ret = true;
					if(!conte) {
						changeCoin(x,y,color);
					}
					else {
						nbCoinChange ++;
					}
					int i = x+1;
					int j = y + 1;
					while ( i < xe && j < ye ) {
						if(!conte) {
							changeCoin(i,j,color);
						}
						else {
							nbCoinChange ++;
						}
						i++;
						j++;
					}
					stop = true;
				}
				xe ++;
				ye ++;
			}
		}
		else if (x<this.width-1 && y < this.height-1 && (grid[x+1][y+1].getColor() == couleur)&& verif) {// Si dans la diagonale bas droite
			int xe = x+2;
			int ye = y+2;
			boolean stop = false;
			while (xe <this.width && ye<this.height && grid[xe][ye].getColor() != CoinColor.NONE && grid[xe][ye].getColor() != color && stop == false ) {
				if (grid[xe][ye].getColor() == CoinColor.FREE ) { 
					ret = true;
					stop = true;
				}
				xe ++;
				ye ++;
			}
		}
		return ret;
	}
	
	/**
	 * Check if you can put or check if you can and change the color when they have a pawn in top
	 * @param color : the color of the player
	 * @param couleur : the color of the other player
	 * @param x : the coordinated
	 * @param y : the coordinated
	 * @param verif : true if you want check possibility or false if you want play
	 * @param conte : if is conte the number of coin change
	 * @return true if you can or it's possible
	 */
	public boolean checkXAlignement(CoinColor color, CoinColor couleur , int x, int y, boolean verif, boolean conte) {
		boolean ret = false;
		if (x>1 && (grid[x-2][y].getColor() == couleur)&& !verif) {// Si dans la colone en haut
			int xe = x-4;
			boolean stop = false;
			while ( xe>= 0 && grid[xe][y].getColor() != CoinColor.NONE && grid[xe][y].getColor() != CoinColor.FREE && stop == false ) {
				if (grid[xe][y].getColor() == color  ) { 
					ret = true;
					if(!conte) {
						changeCoin(x,y,color);
					}
					else {
						nbCoinChange ++;
					}
					for ( int i = xe; i < x ; i= i+2 ) {
						if(!conte) {
							changeCoin(i,y,color);
						}
						else {
							nbCoinChange ++;
						}
					}
					stop = true;
				}
				xe = xe-2;
			}
		}
		else if (x>1 && (grid[x-2][y].getColor() == couleur)&& verif) {// Si dans la colone en haut
			int xe = x-4;
			boolean stop = false;
			while ( xe>= 0 && grid[xe][y].getColor() != CoinColor.NONE && grid[xe][y].getColor() != color && stop == false ) {
				if (grid[xe][y].getColor() == CoinColor.FREE ) { 
					ret = true;
					stop = true;
				}
				xe = xe-2;
			}
		}
		return ret;
	}
	
	/**
	 * get the table in a String
	 * @return the table in a string
	 */
	public String toString() {
		String ret ="";
		for (int a = 0 ; a<this.width ; a++) {
			for (int aC = 0 ; aC <height  ; aC++ ) {
			   ret += grid[a][aC].toString();
			}
			ret += "\n";
		}
		return ret;
		
	}
	
	/**
	 * set the color in the alignment
	 * @param x : the coordinated
	 * @param y : the coordinated
	 * @param color : the color of the player
	 * @return true if the player can put their color
	 */
	public boolean setCoin(int x, int y, CoinColor color) {
		boolean ret = false;
		CoinColor couleur = CoinColor.WHITE;
		if (color == CoinColor.WHITE){couleur = CoinColor.BLACK;}
		if (x<this.width && y<height && grid[x][y].isFree()) {
			boolean ret1 = checkAlignment(color, couleur , x, y, false, false); // Si dans la colone en haut
			boolean ret2 = checkHAlignement(color,couleur ,x, y, false, false); // Si dans la diagonale haut gauche
			boolean ret3 = checkLAlignement(color, couleur , x,y, false, false); // Si dans la diagonale haut droite
			boolean ret4 = checkWAlignement(color, couleur , x,y, false, false); // Si dans la diagonale bas gauche
			boolean ret5 = checkVAlignement(color, couleur , x,y, false, false); // Si dans la diagonale bas droite
			boolean ret6 = checkXAlignement(color, couleur , x,y, false, false); // Si dans la colone en bas
			if (ret1 == true || ret2 == true || ret3 == true || ret4 == true || ret5 == true || ret6 == true) {
				ret = true;	
			}
		}
		return ret;
	}
	
	/**
	 * calculate how many pieces change
	 * @param x : the coordinated
	 * @param y : the coordinated
	 * @param color : the color of the player
	 * @return : the number of color to change if you chose y and y
	 */
	public int countCoin( int x, int y, CoinColor color) {
		nbCoinChange = 0;
		CoinColor couleur;
		if (color == CoinColor.WHITE){
			couleur = CoinColor.BLACK;
		}
		else {couleur = CoinColor.WHITE;}
		if (x<this.width && y<height && grid[x][y].isFree()) {
			boolean ret1 = checkAlignment(color, couleur , x, y, false , true); // Si dans la colone en haut
			boolean ret2 = checkHAlignement(color,couleur ,x, y, false , true); // Si dans la diagonale haut gauche
			boolean ret3 = checkLAlignement(color, couleur , x,y, false , true); // Si dans la diagonale haut droite
			boolean ret4 = checkWAlignement(color, couleur , x,y, false , true); // Si dans la diagonale bas gauche
			boolean ret5 = checkVAlignement(color, couleur , x,y, false , true); // Si dans la diagonale bas droite
			boolean ret6 = checkXAlignement(color, couleur , x,y, false , true); // Si dans la colone en bas
		}
		return nbCoinChange;
	}
	
	/**
	 * change the color of the square and refresh the Frame
	 * @param x : the cordinates 
	 * @param y : the coordinates
	 * @param color : the new color 
	 */
	public void changeCoin(int x, int y, CoinColor color) {
		grid[x][y].setColor(color);
		otframe.hideIt();
		otframe.showIt("Reversi");
		}
	
	/**
	 * changed the message show on the window
	 * @param aff : the message to display
	 */
	public void setAffichage(String aff) {
		otframe.setTour(aff);
	}
}