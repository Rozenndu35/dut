package reversi;
/**
 * the interface of a game
 * @author rozenn
 * @version 1
 */
public interface IGame {
	public String description();
	public void start();
	public void endOfGame(String term);
}
