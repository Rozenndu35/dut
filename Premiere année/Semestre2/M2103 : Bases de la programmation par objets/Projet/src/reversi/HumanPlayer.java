package reversi;

import java.awt.Color;
import java.io.Serializable;
import java.util.Scanner;

import view.GridTableFrame;

/**
 *  the class for the Igame of humain
 * @author rozenn
 * @version 1
 */
public class HumanPlayer extends Player implements Serializable{
	/**
	 * the constructor
	 * @param name : the name
	 * @param board : the table of the game
	 * @param nbJ : the number of player
	 */
	public HumanPlayer(String name, Board board, int nbJ) {
		super(name, board, nbJ);
	}
	
	/**
	 *  make the game of the humain
	 *  @param otframe : the frame whith the table
	 */
	public void play(GridTableFrame otframe) {
		
		System.out.println(this.name+" Saisir les cordonnées de la case ou vous voulez poser");
		board.setAffichage(this.name+" Saisir les cordonnées de la case ou vous voulez poser");
		
		while(!otframe.isHasPlayed()) {
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
			}
		}
		
		otframe.setHasPlayed(false);
		String x = otframe.gettheX();
		String y = otframe.gettheY();
		
		System.out.println("recupere x et y : "+ x+" ; "+ y);
		int xv;
		int yv;
		boolean correctS = false;
				
		if ( this.nuJ == 1) {
			while(!correctS) {
				try{
					while(!board.setCoin(Integer.parseInt(x),Integer.parseInt(y), CoinColor.BLACK) ) {
						
						
						System.out.println(this.name+" Resaisir les cordonnées de la case ou vous voulez poser");
						board.setAffichage(this.name+" Resaisir les cordonnées de la case ou vous voulez poser");
												
						while(!otframe.isHasPlayed()) {
							try {
								Thread.sleep(200);
							} catch (InterruptedException e) {
							}
						}
						
						otframe.setHasPlayed(false);
						x = otframe.gettheX();
						y = otframe.gettheY();
						xv =Integer.parseInt(x);	
						yv =Integer.parseInt(y);
						}
					correctS = true;
				}
				catch(NumberFormatException e){
					System.out.println("vous vous ete tromper dans la saisie d'un nombre");
					board.setAffichage("vous vous ete tromper dans la saisie d'un nombre");
					System.out.println(this.name+" Resaisir les cordonnées de la case ou vous voulez poser");
					board.setAffichage(this.name+" Resaisir les cordonnées de la case ou vous voulez poser");
					System.out.print( "x : ");
					Scanner scan = new Scanner(System.in);
					x = scan.nextLine();
					System.out.print( "y : "); 
					y = scan.nextLine();
					xv =Integer.parseInt(x);	
					yv =Integer.parseInt(y);
					
				}
			}
		}
		else {
			while(!correctS) {
				try{
					while(!board.setCoin(Integer.parseInt(x),Integer.parseInt(y), CoinColor.WHITE)) {
						otframe.changeCouleur(Color.RED);
						System.out.println(this.name+" Resaisir les cordonnées de la case ou vous voulez poser");
						board.setAffichage(this.name+" Resaisir les cordonnées de la case ou vous voulez poser");
						
						while(!otframe.isHasPlayed()) {
							try {
								Thread.sleep(200);
							} catch (InterruptedException e) {
							}
						}
						
						otframe.setHasPlayed(false);
						x = otframe.gettheX();
						y = otframe.gettheY();
						
						/*System.out.print( "x : ");
						x = scan.nextLine();
						System.out.print( "y : "); 
						y = scan.nextLine();*/
						xv =Integer.parseInt(x);	
						yv =Integer.parseInt(y);
					}
					correctS = true;
				}
				catch(NumberFormatException e){
					System.out.println("vous vous ete tromper dans la saisie d'un nombre");
					board.setAffichage("vous vous ete tromper dans la saisie d'un nombre");
					System.out.println(this.name+" Resaisir les cordonnées de la case ou vous voulez poser");
					board.setAffichage(this.name+" Resaisir les cordonnées de la case ou vous voulez poser");
					System.out.print( "x : ");
					Scanner scan = new Scanner(System.in);
					x = scan.nextLine();
					System.out.print( "y : "); 
					y = scan.nextLine();
					xv =Integer.parseInt(x);	
					yv =Integer.parseInt(y);
					
				}
			}
		}		
	}

}
