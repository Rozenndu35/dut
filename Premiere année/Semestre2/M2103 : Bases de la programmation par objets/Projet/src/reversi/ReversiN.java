package reversi;

import java.io.*;
import java.util.*;

/**
 * 
 * @author rozenn
 * @version 1
 */
public class ReversiN {
	private int size;
	private int nbCoin;
	private final int MINSIZE = 4;
	private Mode mode;
	private Game gameplay;
	
	/**
	 * the constructor
	 * @param fileName :the file for initialize
	 */
	public ReversiN (String fileName ) {
		configure(fileName);
		printConfiguration();
		System.out.println("Demarage de la partie");
		gameplay = new Game(this.mode , ((this.size+2) /2), nbCoin);		
	}
	
	/**
	 * initilize the information with a file
	 * @param fileName : the file with the information
	 */
	public void configure (String fileName) {
		ArrayList<String> list = new ArrayList<String>();
		FileReader file = null;
		// recupere les informations
		try {
			file = new FileReader (fileName);
	
			BufferedReader in = new BufferedReader(file);
			String s = null;
			try {
				s = in.readLine();
				while(s!=null){
					list.add(s);
					s=in.readLine();
				}
				in.close();	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// initialise les valeurs
		for (int i = 0; i< list.size() ; i++) {
			if (i == 0) {
				String line = (String) list.get(i);
				String chaine1 = line.substring (line.indexOf(":") +1);
				int sizeF = Integer.parseInt(chaine1.trim());
				if (sizeF >= this.MINSIZE) {
					this.size = sizeF;
				}
				else {
					System.err.println("reversi.ReversiN : configure : la taille est pas autorize ");
				}
			}
			if (i == 1) {
				String line = (String) list.get(i);
				String chaine1 = line.substring (line.indexOf(":") +1);
				int nbPieceF = Integer.parseInt(chaine1.trim());
				if (nbPieceF == (size*size)-(((size+1)/2) *((size-1)/2))) {
					this.nbCoin = nbPieceF;
				}
				else {
					System.err.println("reversi.ReversiN : configure : le nombre de piece n'est pas correct ");
					this.nbCoin = (size*size)-(((size+1)/2) *((size-1)/2));
				}
			}
			if (i == 2) {
				String line = (String) list.get(i);
				String chaine1 = line.substring (line.indexOf(":") +1);
				String modeF = chaine1.trim();
				if (modeF.compareTo("HH") == 0 ) {this.mode = Mode.HH;}
				else if (modeF.compareTo("HA") == 0 ) {this.mode = Mode.HA;}
				else if (modeF.compareTo("AA") == 0 ) {this.mode = Mode.AA;}
				else if (modeF.compareTo("AH") == 0 ) {this.mode = Mode.AH;}
				else {System.err.println("reversi.ReversiN : configure : le nombre de piece n'est pas correct ");}
			}
		}
		System.out.println ("initialiser avec le fichier : " +fileName);
	}
	
	/**
	 * print the configuration in the console for checking
	 */
	public void printConfiguration() {
		String modeP = null;
		if (this.mode.equals(Mode.HH)) {modeP = "HH";}
		else if (this.mode.equals(Mode.HA)) {modeP = "HA";}
		else if (this.mode.equals(Mode.AA)) {modeP = "AA";}
		else if (this.mode.equals(Mode.AH)) {modeP = "AH";}
		System.out.println("taille de la grille : " + size + "\n" + "nombre de pièces dans le jeu : " + nbCoin + "\n" + "mode joueur : " + modeP);
	}
	
	
}
