package reversi;

import java.io.Serializable;

/**
 * 
 * @author rozenn
 * @version 1
 */
public class Square implements Serializable{
	private int x;
	private int y;
	private CoinColor color;
	
	/**
	 * put with the square a string for the consol
	 */
	public String toString() {
		String ret =" ";
		if (this.color==CoinColor.FREE) {
			ret = "_";
		}
		if (this.color==CoinColor.BLACK) {
			ret = "●";
		}
		if (this.color==CoinColor.WHITE) {
			ret = "○";
		}
		return ret;
	}
	
	/**
	 * return true if it is free or false
	 * @return true if it is free or false
	 */
	public boolean isFree() {
		return this.color==CoinColor.FREE;
	}
	
	/**
	 * return true if it is none or false
	 * @return true if it is none or false
	 */
	public boolean isForbidden() {
		return this.color==CoinColor.NONE;
	}
	
	/**
	 * get the color of the scare
	 * @return the color of the scare
	 */
	public CoinColor getColor() {
		return color;
	}
	
	/**
	 * set the color of a square
	 * @param couleur : the new color
	 */
	public void setColor(CoinColor couleur) {
		this.color = couleur;
		}

	/**
	 * get the x
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * set the x
	 * @param x : the new x
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * get the y
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * set the y
	 * @param y : the new y
	 */
	public void setY(int y) {
		this.y = y;
	}

	
	
}