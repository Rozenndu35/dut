package reversi;

import java.io.Serializable;
import java.util.HashMap;

import view.GridTableFrame;

/**
 * the class for the IA of the game
 * @author rozenn
 * @version 1
 */
public class AutoPlayer extends Player implements Serializable{
	Difficulter difficulte;

	/**
	 * the constructor
	 * @param name : the name
	 * @param board : the table of the game
	 * @param nbJ : the nuber of the player
	 * @param dif the difficulter of the computeur
	 */
	public AutoPlayer(String name, Board board, int nbJ, Difficulter dif) {
		super(name, board, nbJ);
		this.difficulte = dif;
	}
	
	/**
	 * make the game of the computer with different difficult
	 * @param otframe : the frame whith the table
	 */
	public void play(GridTableFrame otframe) {
		otframe.disableVali();
		if (this.difficulte ==  Difficulter.FACILE) {
			System.out.println("C'est au tour de "+ this.name);
			board.setAffichage("C'est au tour de "+ this.name);
			try {
				System.out.print("\n"+this.name+" Réfléchi");
				board.setAffichage(this.name+" Réfléchi");
				   Thread.sleep(500);
				   System.out.print(".");
				   board.setAffichage(this.name+" Réfléchi.");
				   Thread.sleep(500);
				   System.out.print(".");
				   board.setAffichage(this.name+" Réfléchi..");
				   Thread.sleep(500);
				   System.out.print(".");
				   board.setAffichage(this.name+" Réfléchi...");
				   System.out.println("");
				   Thread.sleep(1500);
				}  catch (InterruptedException e) {
				}
			CoinColor maCouleur = CoinColor.BLACK;;
			if (this.nuJ == 2) {maCouleur = CoinColor.WHITE;}
			boolean marche = false;
			Square[][] tab = this.board.getGrid();
			for (int i = 0; i < tab.length && !marche ; i++) {
				for (int j = 0 ; j< tab[0].length && !marche; j++) {
					if (tab[i][j].getColor() == CoinColor.FREE) {
						marche = board.setCoin(i, j, maCouleur);
					}
				}
			}
		}
		else if (this.difficulte ==  Difficulter.ALEATOIRE) {
			System.out.println("C'est au tour de "+ this.name);
			board.setAffichage("C'est au tour de "+ this.name);
			try {
				System.out.print("\n"+this.name+" Réfléchi");
				board.setAffichage(this.name+" Réfléchi");
				   Thread.sleep(500);
				   System.out.print(".");
				   board.setAffichage(this.name+" Réfléchi.");
				   Thread.sleep(500);
				   System.out.print(".");
				   board.setAffichage(this.name+" Réfléchi..");
				   Thread.sleep(500);
				   System.out.print(".");
				   board.setAffichage(this.name+" Réfléchi...");
				   System.out.println("");
				   Thread.sleep(1500);
				}  catch (InterruptedException e) {
				}
			CoinColor maCouleur = CoinColor.BLACK;;
			if (this.nuJ == 2) {maCouleur = CoinColor.WHITE;}
			boolean trouver = false;
			Square[][] tab = this.board.getGrid();
			HashMap xH = new HashMap();
			HashMap yH = new HashMap();
			xH.clear();
			yH.clear();
			int nbP=0;
			for (int i = 0; i < tab.length; i++) {
				for (int j = 0 ; j< tab[0].length; j++) {
					if (tab[i][j].isFree()) {
						if (this.board.countCoin(i, j ,maCouleur)>0) {
							xH.put(nbP, i);
							yH.put(nbP, j);
							nbP++;
						}
					}
				}
			}
			System.out.println(xH.entrySet());
			System.out.println(yH.entrySet());
			while (!trouver) {
				int x = (int) (Math.random() * ( xH.size()));
				int y = (int) (Math.random() * ( yH.size()));
				x = (int) xH.get(x);
				y = (int) yH.get(y);
				trouver = board.setCoin(x, y, maCouleur);
			}
		}
		else if (this.difficulte ==  Difficulter.DIFFICILE) {
			System.out.println("C'est au tour de "+ this.name);
			board.setAffichage("C'est au tour de "+ this.name);
			try {
				System.out.print("\n"+this.name+" Réfléchi");
				board.setAffichage(this.name+" Réfléchi");
				   Thread.sleep(500);
				   System.out.print(".");
				   board.setAffichage(this.name+" Réfléchi.");
				   Thread.sleep(500);
				   System.out.print(".");
				   board.setAffichage(this.name+" Réfléchi..");
				   Thread.sleep(500);
				   System.out.print(".");
				   board.setAffichage(this.name+" Réfléchi...");
				   System.out.println("");
				   Thread.sleep(1500);
				}  catch (InterruptedException e) {}
			System.out.println("_________________________________________________________");
			CoinColor maCouleur = CoinColor.BLACK;;
			if (this.nuJ == 2) {maCouleur = CoinColor.WHITE;}
			int nbCoinChanger = 0;
			int x = 0;
			int y = 0;
			boolean trouver = false;
			Square[][] tab = this.board.getGrid();
			for (int i = 0; i < tab.length ; i++) {
				for (int j = 0 ; j< tab[0].length ; j++) {
					if (tab[i][j].getColor() == CoinColor.FREE) {
						if (nbCoinChanger < board.countCoin(i, j, maCouleur) ) {
							nbCoinChanger = board.countCoin(i, j, maCouleur);
							x = i;
							y = j;
						}
						
					}
				}
			}
			System.out.println("_________________________________________________________");
			trouver = board.setCoin(x, y, maCouleur);
		}
		
		otframe.enableVali();
	}

}
