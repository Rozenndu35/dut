package reversi;
/**
 * the enumeration of the color for the square
 * @author rozenn
 * @version 1
 */
public enum CoinColor {
	WHITE, // les jeton du joueur blanc
	BLACK, // les jeton du joueur noir
	FREE, // les emplacement libre du plateau
	NONE; // exterieur du plateau
}
