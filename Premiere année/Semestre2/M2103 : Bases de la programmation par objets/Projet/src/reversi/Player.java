package reversi;

import java.io.Serializable;

import view.GridTableFrame;

/**
 * the abstract class for make a player
 * @author rozenn
 * @version 1
 */
public abstract class Player implements Serializable {
	protected String name;
	protected Board board;
	protected int nuJ;
	
	/**
	 * the constructor of the class Player
	 * @param name : the name of the player
	 * @param board : the board of the game
	 * @param nbJ : the number of player
	 */
	public Player (String name, Board board, int nbJ) {
		if(name != null && board != null && (nbJ == 1 || nbJ == 2)) {
			this.name = name;
			this.board = board;
			this.nuJ = nbJ;
		}
		else {
			System.err.println("reversi : Player : Player : name , board or numPlayer are invalibel!");
		}		
	}
	/**
	 * get the name
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * the method for play the player 
	 * @param otframe : the frame of the grid
	 */
	public abstract void play(GridTableFrame otframe);
}
