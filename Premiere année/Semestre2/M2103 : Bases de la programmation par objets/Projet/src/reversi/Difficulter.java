package reversi;
/**
 * the enumeration of the different difficult
 * @author rozenn
 * @version 1
 */
public enum Difficulter {
	FACILE,
	ALEATOIRE,
	DIFFICILE;
}
