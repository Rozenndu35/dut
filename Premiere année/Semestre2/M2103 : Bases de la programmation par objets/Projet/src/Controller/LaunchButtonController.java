package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.SelectJoueur;
/**
 * make the action for the button launcher (lancer)
 * @author rozenn
 * @version 1
 */
public class LaunchButtonController implements ActionListener{
	SelectJoueur sj;
	/**
	 * the constructor
	 * @param sjj : the freme when you select the player
	 */
	public LaunchButtonController(SelectJoueur sjj) {
		this.sj=sjj;
	}
	/**
	 * the action when you click on lancer
	 * @param arg0 : the evnet
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.sj.finish();
		
	}

}
