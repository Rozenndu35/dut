package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import view.GridTableFrame;
import view.SelectJoueur;
/**
 * make the action when you click on the button load (charger) 
 * @author rozenn
 * @version 1
 */
public class loadButtonListener implements ActionListener {
	SelectJoueur otf;
	
	/**
	 * the constructor
	 * @param otf : the frame when you select player
	 */
	public loadButtonListener(SelectJoueur otf) {
		this.otf=otf;
	}

	/**
	 * the action and load the game when your click on the button
	 * @param arg0 : the event
	 */
	public void actionPerformed(ActionEvent arg0) {
		JFileChooser dialogue = new JFileChooser();
		dialogue.setName("charger");
		dialogue.setSelectedFile(new File("~/Documents/ReversiSave1.rvsi"));
		JButton charger = new JButton();
		if(dialogue.showOpenDialog(charger)==JFileChooser.APPROVE_OPTION){
			this.otf.setLoadingPath(dialogue.getSelectedFile().getAbsolutePath());
			this.otf.charger = true;
			System.out.println("charger");
			
			
			JOptionPane.showMessageDialog(null, "La partie vas ce lancer");
			this.otf.finish();
		}
		
	}

}

