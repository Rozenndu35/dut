package Controller;

import java.awt.Color;
import java.awt.TextField;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JTable;

import view.GridTableFrame;
/**
 * make the event when you click on the table
 * @author rozenn
 * @version 1
 */
public class grilleController implements MouseListener{
	JLabel x;
	JLabel y;
	JTable table;
	GridTableFrame gTF;
	/**
	 * the constructor
	 * @param x : the label for the x
	 * @param y : the label for the y
	 * @param table : jtable
	 * @param gTF : the frame
	 */
	public grilleController(JLabel x, JLabel y, JTable table, GridTableFrame gTF) {
		this.x=x;
		this.y=y;
		this.table=table;
		this.gTF = gTF;
	}

	/**
	 * make the event
	 * @param event: the event whith the mousse
	 */
	@Override
	public void mouseClicked(MouseEvent event) {
	    this.x.setText(Integer.toString(table.rowAtPoint(event.getPoint())));
	    this.y.setText(Integer.toString(table.columnAtPoint(event.getPoint())));
	    this.gTF.changeCouleur(Color.BLACK);
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
