package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import view.GridTableFrame;
/**
 * make the save on the game when you click in the button save
 * @author rozenn
 * @version 1
 */
public class saveButtonListener implements ActionListener {
	GridTableFrame otf;
	
	/**
	 * the constructor
	 * @param otf : the frame where is
	 */
	public saveButtonListener(GridTableFrame otf) {
		this.otf=otf;
	}

	/**
	 * make the avtion of saving
	 * @param arg0 : the event
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		JFileChooser dialogue = new JFileChooser();
		dialogue.setName("enregister");
		dialogue.setSelectedFile(new File("~/Documents/ReversiSave1.rvsi"));
		JButton save = new JButton();
		if(dialogue.showSaveDialog(save)==JFileChooser.APPROVE_OPTION){
			this.otf.setSavingPath(dialogue.getSelectedFile().getAbsolutePath());
			this.otf.setHaveToSave(true);
			System.out.println("saved");
			
			JOptionPane.showMessageDialog(null, "La partie sera sauvegardee au prochain coup effectue");
		}
		
		
		
	}

}
