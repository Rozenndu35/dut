package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
/**
 * make a new frame with the rule of the game
 * @author rozenn
 * @version 1
 */
public class RuleButtonListener implements ActionListener{

	/**
	 * make the action when you click on the button rule
	 * @param arg0 : the event
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		JOptionPane.showMessageDialog(null, "respecter cela :\r\n" + 
				"	vous devez obligatoirement manger un pion de l'adversaire dans les diagonale ou la colonne\r\n" + 
				"\r\n" + 
				"Le premier qui gagne est celui qui :\r\n" + 
				"	a manger tout les pion adverse\r\n" + 
				"	ou  a le plus de jeton lorsque persone peut jouer ou que les pion sont tout tombé\r\n" + 
				"bonne partie");
		
	}
	

}
