package Controller;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import reversi.ReversiN;
import view.EndGameFrame;
import view.GridTableFrame;
import view.SelectJoueur;

/**
 * make the action for the buton replay
 * @author rozenn
 * @version 1
 */
public class RejouerButtonListener implements ActionListener{
	EndGameFrame gtf;
	/**
	 * the constructor
	 * @param gtf : the frame where is the button
	 */
	public RejouerButtonListener(EndGameFrame gtf) {
		this.gtf = gtf;
	}
	/**
	 * make the action when you want replay
	 * @param arg0 : the event
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		Thread t = new Thread() {
			public void run() {
				ReversiN jeux = new ReversiN("data/lanceur1.txt");
			}
		};
		t.start();
		this.gtf.dispose();
		
	}
}