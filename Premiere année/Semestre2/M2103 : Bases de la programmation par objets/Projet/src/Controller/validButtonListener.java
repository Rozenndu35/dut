package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.GridTableFrame;
import view.SelectJoueur;
/**
 * * make the action fors the buton valider
 * @author rozenn
 * @version 1
 */
public class validButtonListener implements ActionListener{
	GridTableFrame gtf;
	
	/**
	 * the constructor
	 * @param gridTableFrame : the frame where is
	 */
	public validButtonListener(GridTableFrame gridTableFrame) {
		this.gtf=gridTableFrame;
	}

	/**
	 * tells the game that the player has chosen
	 * @param arg0 : the event
	 */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.gtf.setHasPlayed(true);
		
	}
}
