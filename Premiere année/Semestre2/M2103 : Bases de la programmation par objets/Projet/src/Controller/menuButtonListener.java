package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import reversi.ReversiN;
import view.GridTableFrame;
/**
 * make the action fors the buton menu (quitter)
 * @author rozenn
 * @version 1
 */
public class menuButtonListener implements ActionListener{
	GridTableFrame gtf;
	/**
	 * the constructor
	 * @param gridTableFrame : the fram where is the button
	 */
	public menuButtonListener(GridTableFrame gridTableFrame) {
		this.gtf=gridTableFrame;
	}

	/**
	 * make the event when you leave
	 * @param e : the event
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		Thread t = new Thread() {
			public void run() {
				ReversiN jeux = new ReversiN("data/lanceur1.txt");
			}
		};
		t.start();
		System.exit(0);
	}

}
