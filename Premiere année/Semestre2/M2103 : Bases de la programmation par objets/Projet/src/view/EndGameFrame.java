package view;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Controller.RejouerButtonListener;
import Controller.RuleButtonListener;
import reversi.Mode;

/**
 * 
 * @author rozenn
 * @version 1
 */
public class EndGameFrame extends JFrame{
	
	private JLabel info;
	private JButton rejouer;
	
	
	/**
	 * the constructor
	 * @param inform : the information of the winner
	 */
	public EndGameFrame(String inform) {
		JPanel pane = new JPanel();
		pane.setLayout(new GridLayout(0,1));
		this.info=new JLabel(inform); // mettre le joueur qui a gagner
		this.rejouer =new JButton("Rejouer");
		rejouer.addActionListener(new RejouerButtonListener(this));
		pane.add(info);
		pane.add(rejouer);
		this.add(pane);
		this.pack();
		this.setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
