package view;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import Controller.RuleButtonListener;
import Controller.grilleController;
import Controller.menuButtonListener;
import Controller.saveButtonListener;
import Controller.validButtonListener;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Insets;

import reversi.Square;
/**
 * GridTableFrame : frame for GridTable
 * @author rozenn
 * @version 1
 */
public class GridTableFrame extends SimpleFrame {
	private final int rowHeight = 40;  //en pixel
	private JPanel bas;
	private JButton regle;
	private JLabel x;
	private JLabel y;
	private JLabel tour;
	private JLabel xe;
	private JLabel ye;
	private JButton valid;
	private JPanel hautD ;
	private JPanel milieuD;
	private static final String PATH = "../data/images/";

	private JLabel tourde;
	private JLabel tourdeCircle;
	
	private JButton save;
	private JButton menu;
	
	private boolean hasPlayed=false;
	private Boolean haveToSave=false;
	private String savingPath;

	/**
	 * Constructor
	 * It creates the GridTableModel
	 * @param grid : the data table to display
	 */
	public GridTableFrame(Square[][] grid) {
		// set the grid size
		this.setSize(rowHeight*grid[0].length+(3*rowHeight)+30,(rowHeight*grid[0].length*2)-rowHeight);
		GridTableModel otmodel = new GridTableModel(grid);
		JTable tab = new JTable(otmodel);

		// to adjust some parameters
		tab.setShowGrid(true);
		// color for the grid lines
		tab.setGridColor(Color.BLACK);
		tab.setRowHeight(rowHeight);

		JScrollPane SP = new JScrollPane(tab);
		tab.setBackground(new Color(214,214,214));

		regle= new JButton("Regles du jeu");
		regle.addActionListener(new RuleButtonListener());
		x = new JLabel("X : ");
		y = new JLabel("Y : ");
		xe = new JLabel("0");
		ye = new JLabel("0");

		valid = new JButton ("Valider");
		valid.addActionListener(new validButtonListener(this));
		tour = new JLabel ("Tour de");
		save = new JButton ("Sauvegarder");
		save.addActionListener(new saveButtonListener(this));
		menu = new JButton ("Quitter");
		menu.addActionListener(new menuButtonListener(this));
		save.setMaximumSize(regle.getMaximumSize());
		menu.setMaximumSize(regle.getMaximumSize());
		regle.setMaximumSize(menu.getMaximumSize());

		milieuD = new JPanel();
		milieuD.setLayout(new BoxLayout(milieuD, BoxLayout.Y_AXIS));
		JPanel xPane = new JPanel();
		JPanel yPane = new JPanel();
		xPane.add(x);xPane.add(xe); 
		yPane.add(y); yPane.add(ye); 


		JPanel blankPanel = new JPanel();
		blankPanel.setPreferredSize(new Dimension(50,650));

		tourde = new JLabel("Tour de :");
		tourdeCircle = new JLabel(new ImageIcon(PATH +"pion-noir.png"));
		tourdeCircle.setPreferredSize(new Dimension(100,100));
		
		milieuD.add(tourde);
		milieuD.add(tourdeCircle);
		milieuD.add(xPane);
		milieuD.add(yPane);
		milieuD.add(valid);
		milieuD.add(blankPanel);
		//milieuD.setBorder(new EmptyBorder(0, 0, 100, 0));

		JPanel partieChangementPane = new JPanel();
		partieChangementPane.setLayout(new BoxLayout(partieChangementPane, BoxLayout.Y_AXIS));
		partieChangementPane.add(save);
		partieChangementPane.add(new JLabel(" "));
		partieChangementPane.add(menu);

		bas = new JPanel();
		bas.setLayout(new BorderLayout(20, 20));
		bas.add(BorderLayout.NORTH ,regle );
		bas.add(BorderLayout.CENTER,milieuD); 
		bas.add(BorderLayout.SOUTH,partieChangementPane);
		bas.setBorder(new EmptyBorder(10, 10, 10, 10));

		this.getContentPane().setLayout( new BorderLayout());
		this.getContentPane().add(BorderLayout.NORTH, tour);
		this.getContentPane().add(BorderLayout.WEST, bas);
		this.getContentPane().add(BorderLayout.EAST, SP);   

		tab.addMouseListener(new grilleController(xe, ye, tab, this));

	}

	/**
	 * set the message that must play
	 * @param msg : the new message
	 */
	public void setTour(String msg) {
		this.tour.setText(msg);
	}
	
	/**
	 * set the image that must play
	 * @param noir : true if is black
	 */
	public void setImgTour(boolean noir) {
		if(noir) {
			tourdeCircle.setIcon(new ImageIcon(PATH +"pion-noir.png"));
		}else {
			tourdeCircle.setIcon(new ImageIcon(PATH +"pion-blanc.png"));
		}
	}
	
	/**
	 * get the x choose 
	 * @return the x
	 */
	public String gettheX() {
		return this.xe.getText();
	}
	
	/**
	 * get the y choose 
	 * @return the y
	 */
	public String gettheY() {
		return this.ye.getText();
		
	}

	/**
	 * get if the player have play
	 * @return true if the player have play
	 */
	public boolean isHasPlayed() {
		return hasPlayed;
	}

	/**
	 * set if the player have play
	 * @param hasPlayed : true if the player have play
	 */
	public void setHasPlayed(boolean hasPlayed) {
		this.hasPlayed = hasPlayed;
	}
	
	/**
	 * disable the button valid
	 */
	public void disableVali() {
		this.valid.setEnabled(false);
	}
	
	/**
	 * enable the button valid 
	 */
	public void enableVali() {
		this.valid.setEnabled(true);
	}

	/**
	 * get if haveToSave when you want save
	 * @return true if you have save
	 */
	public boolean getHaveToSave() {
		return haveToSave;
	}
	
	/**
	 * set if haveToSave when you want save
	 * @param t : true if you want save
	 */
	public void setHaveToSave(boolean t) {
		this.haveToSave=t;
	}

	/**
	 * set the path when you save
	 * @param absolutePath : the new path when you want  save
	 */
	public void setSavingPath(String absolutePath) {
		this.savingPath=absolutePath;
		
	}
	
	/**
	 * set the path when you save
	 * @return the path when you save
	 */
	public String getSavingPath() {
		return this.savingPath;
	}

	/**
	 * change the color of the text if the player have make a error
	 * @param couleur : the new color of the text
	 */
	public void changeCouleur(Color couleur) {
		this.x.setForeground(couleur);
		this.y.setForeground(couleur);
		this.xe.setForeground(couleur);
		this.ye.setForeground(couleur);
	}
	
}