package view;

import java.awt.GridLayout;
import java.awt.event.WindowEvent;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import Controller.LaunchButtonController;
import Controller.loadButtonListener;
import reversi.Difficulter;
import reversi.HumanPlayer;
import reversi.Mode;

/**
 * Windows for initialize the game
 * @author rozenn
 * @version 1
 */
public class SelectJoueur extends JFrame {
	private JLabel info;
	private JTextField tfJ1;
	private JTextField tfJ2;
	private JComboBox<Difficulter> dificulter1;
	private JComboBox<Difficulter> dificulter2;
	private JLabel largeurGrille;
	private JLabel nbPieceJeu;
	private JLabel mode;
	private JButton go;
	
	volatile Boolean waiting=true;
	private JLabel menu;
	private JButton load;
	private Difficulter[] list =new Difficulter[] {Difficulter.FACILE, Difficulter.ALEATOIRE,Difficulter.DIFFICILE};
	public boolean charger = false;
	private String loadingPath;
	
	
	/**
	 * the constructor
	 * @param largeur : the size of the table
	 * @param nbPieceJeu : nb coins
	 * @param mode : the mode of the game
	 */
	public SelectJoueur(int largeur, int nbPieceJeu, Mode mode) {
		

		JPanel pane = new JPanel();
		pane.setLayout(new GridLayout(0,1));
		this.menu=new JLabel("MENU");
		this.info=new JLabel("(Edit the .txt)");
		this.largeurGrille= new JLabel("Largeur : "+largeur);
		this.nbPieceJeu= new JLabel("Nombre de piéces : "+nbPieceJeu);
		this.mode= new JLabel("Mode : "+mode.toString());
		this.go=new JButton("Lancer");
		this.load=new JButton("Charger");
		this.load.addActionListener(new loadButtonListener(this));
		if (mode ==  Mode.HH) {
			this.tfJ1=new JTextField("Joueur1");
			this.tfJ2=new JTextField("Joueur2");
			this.go.addActionListener(new LaunchButtonController(this));
			
			
			pane.add(menu);
			pane.add(info);
			pane.add(this.largeurGrille);
			pane.add(this.nbPieceJeu);
			pane.add(this.mode);
			pane.add(tfJ1);
			pane.add(tfJ2);
			pane.add(go);
			pane.add(load);
		}
		else if (mode ==  Mode.HA) {
			this.tfJ1=new JTextField("Joueur1");
			this.tfJ2=new JTextField("Ordi12");
			this.dificulter2 = new JComboBox<Difficulter>(list);
			this.go.addActionListener(new LaunchButtonController(this));
			
			
			pane.add(menu);
			pane.add(info);
			pane.add(this.largeurGrille);
			pane.add(this.nbPieceJeu);
			pane.add(this.mode);
			pane.add(tfJ1);
			pane.add(tfJ2);
			pane.add(dificulter2);
			pane.add(go);
			pane.add(load);
		}
		else if (mode ==  Mode.AA) {
			this.tfJ1=new JTextField("Ordi1");
			this.dificulter1 = new JComboBox<Difficulter>(list);
			this.tfJ2=new JTextField("Ordi2");
			this.dificulter2 = new JComboBox<Difficulter>(list);
			
			this.go.addActionListener(new LaunchButtonController(this));
			
			
			pane.add(menu);
			pane.add(info);
			pane.add(this.largeurGrille);
			pane.add(this.nbPieceJeu);
			pane.add(this.mode);
			pane.add(tfJ1);
			pane.add(dificulter1);
			pane.add(tfJ2);
			pane.add(dificulter2);
			pane.add(go);
			pane.add(load);
		}
		else if (mode ==  Mode.AH) {
			this.tfJ1=new JTextField("Ordi1");
			this.dificulter1 = new JComboBox<Difficulter>(list);
			this.tfJ2=new JTextField("Joueur2");
			
			this.go.addActionListener(new LaunchButtonController(this));
			
			
			pane.add(menu);
			pane.add(info);
			pane.add(this.largeurGrille);
			pane.add(this.nbPieceJeu);
			pane.add(this.mode);
			pane.add(tfJ1);
			pane.add(dificulter1);
			pane.add(tfJ2);
			pane.add(go);
			pane.add(load);
			

		}
		pane.setBorder(new EmptyBorder(10, 10, 10, 10));
		this.add(pane);
		this.pack();
		this.setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	/**
	 * get the name of the player 1
	 * @return : the name
	 */
	public String getJ1() {
		return this.tfJ1.getText();
	}
	
	/**
	 * get the name of the player 2
	 * @return : the name
	 */
	public String getJ2() {
		return this.tfJ2.getText();
	}

	/**
	 * get the waiting
	 * @return the waitting
	 */
	public boolean waiting() {
		return this.waiting;
	}
	
	/**
	 * change the waiting when the player have finish to select the information
	 */
	public void finish() {
		this.waiting=false;
	}
	
	/**
	 * disable the JtextField for the name when J1 is a IA
	 */
	public void disableJ1() {
		this.tfJ1.setEnabled(false);
		this.tfJ1.setText("Ordi 1");
	}
	
	/**
	 * disable the JtextField for the name when J2 is a IA
	 */
	public void disableJ2() {
		this.tfJ2.setEnabled(false);
		this.tfJ2.setText("Ordi 2");
	}
	
	/**
	 * disable the JtextField for the name when J2 is a IA and J1 is a IA
	 */
	public void disableJ1J2() {
		this.tfJ1.setEnabled(false);
		this.tfJ2.setEnabled(false);
		this.tfJ1.setText("Ordi 1");
		this.tfJ2.setText("Ordi 2");
	}
	/**
	 * get the difficulterJ1 choose with the player
	 * @return the difficulter
	 */
	public Difficulter getDifJ2() {
		return (Difficulter) this.dificulter2.getSelectedItem();
	}
	
	/**
	 * get the difficulterJ2 choose with the player
	 * @return the difficulter
	 */
	public Difficulter getDifJ1() {
		return (Difficulter) this.dificulter1.getSelectedItem();
	}

	/**
	 * change the path for the load file to whant
	 * @param absolutePath : the new path when you load
	 */
	public void setLoadingPath(String absolutePath) {
		this.loadingPath = absolutePath;
		
	}

	/**
	 * get the path of the file to want load
	 * @return the path of the file to want load
	 */
	public String getLoadingPath() {
		return loadingPath;
	}

	
	
}
