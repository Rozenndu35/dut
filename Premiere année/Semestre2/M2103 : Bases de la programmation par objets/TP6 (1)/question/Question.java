/**
 * Create the question
 *@author Rozenn Costiou 
*/
package question;
import java.util.ArrayList;
import java.lang.*;

public class Question{
    private String text;
    private String answer;

    /**
     * Create a question with an answer
    */
    public Question(){
        this.text = "";
        this.answer = "";
         System.out.println("Question cr�e sans parametre");
    }
    /**
     * Create a question with an answer
     * @param text : the question
     * @param answer : the answer

    */
    public Question (String text , String answer){
        this();
        if ( (text != null) && (answer != null)){
            this.text = text;
            this.answer = answer;
            System.out.println("Question cr�e avec parametre");
        }
    }

    /**
     * Get the question
     * @return the question
    */
    public String getText(){
        String ret;
        ret = this.text;
        return ret;
    }

    /**
     * set the question
     * @param text : the question
     */
    public void setText(String text){
        this.text = text;
    }

    /**
     * Get the answer
     * @return the answer
    */
    public String getAnswer (){
        String ret;
        ret = this.answer;
        return ret;
    }

    /**
     * set the answer
     * @param answer : the answer
     */
    public void setAnswer(String answer){
        this.answer = answer;
    }

    /**
     * check if it's the right answer
     * @param reponse : the answer
     * @return true if the good answer
     */
    public boolean checkAnswer (String reponse){
        boolean ret = false;
        if (reponse != null){
            if (this.getAnswer().equalsIgnoreCase(reponse)){
                ret = true;
            }
        }
        return ret;
    }
    /**
     * display on the screen the text
     */
    public void display(){
        System.out.println("Question : " + this.getText());
    }
}