/**
 * Create the questionnaire
 *@author Rozenn Costiou 
*/
package question;
import java.util.ArrayList;
import java.lang.*;


public class Questionnaire{
        private ArrayList <Question> questionList;
    /**
     * create the list for the question
    */
    public Questionnaire(){
        this.questionList = new ArrayList<Question> (); 
        System.out.println("Questionnaire cr�e");
    }

    /**
     * create the list of the question
     * @param list : the list of question
    */
    public Questionnaire( ArrayList<Question> list){
        this();
        if (list != null){
            this.questionList = list;
            System.out.println("Questionnaire cr�e avec liste");
        }
    }

    /*
     * choise a random question in the list 
     * @return the question
    */
    public Question pickAtRandom(){
        Question ret = null;
        int i;
        if (this.questionList != null){
            i = (int)(Math.random() * (this.questionList.size()));
            ret = this.questionList.get(i);
        }
        return ret;
    }

    /**
     * Get the question
     * @param index : the index of the question
     * @return the question
    */
    public String getQuestionText (int index){
        String ret = null;
        Question question;
        if ( (index >=0)&& (index<this.questionList.size()) ){
            question = this.questionList.get(index);
            ret = question.getText();
        }
        return ret;
    }

    /*
     * get the number of the question
     * @return the nomber of the question
    */
    public int getQNumber(){
        int ret;
        ret = this.questionList.size();
        return ret;
    }

    /* 
     * add a question in the list
    */
    public void add(Question q ){
        if (q != null){
            this.questionList.add(q);
            System.out.println("Question ajoute");
        }
        else{
            System.err.println ("Questionnaire : add : value invalid");
        }
    }
    /**
     * display on the screen the list of question
     */
    public void display(){
        int i =0;
        String t;
        for ( Question q : questionList){
            t = q.getText();
            System.out.println( i+1 + " - " + t );
            i++;
        }
    }

}