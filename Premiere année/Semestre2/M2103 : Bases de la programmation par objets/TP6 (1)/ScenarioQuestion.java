/*
 * The scenario
 *@author Rozenn Costiou 
*/
import question.*;
import java.util.*;
import java.lang.*;


public class ScenarioQuestion {
    /**
 	 * The instance of classe
	 * @param args :  
 	*/
    private static Questionnaire questionnaire;

	public static void main(String[] args){
        int nbGame = 0;
        int nbGameWin = 0;
        
        initialiseQuesionnaire();
        
        Scanner inputPlay = new Scanner(System.in);
        System.out.println("Voulez vous jouer ? ('o' pour oui 'n' pour non)");
        String strPlay = inputPlay.nextLine();
        
        
        while (strPlay.equals("o")){
        	nbGame++;
        	Question question = questionnaire.pickAtRandom();
            question.display();
            Scanner inputAnswer = new Scanner(System.in);
            String strAnswer = inputAnswer.nextLine();
            
            if (question.checkAnswer(strAnswer)==true){
                System.out.println("R�ponse correct");
                nbGameWin++;
            }else {
            	System.out.println("R�ponse incorrect, coup dur");
            }
            System.out.println("Voulez vous rejouer ? ('o' pour oui 'n' pour non)");
            strPlay = inputPlay.next();
        }
        System.out.println("Votre score : "+nbGameWin+"/"+nbGame);
        System.out.println("Aurevoir !");
    }
	
    public static void initialiseQuesionnaire(){
        questionnaire = new Questionnaire();
        questionnaire.add(new Question("Quelle pi�ce est absolument � prot�ger dans le jeu des �checs ?","Le roi"));
        questionnaire.add(new Question("Quel est la capitale de l'Australie ?","Canberra"));
        questionnaire.add(new Question("Quelle ann�e a suivi l'ann�e 1 avant JC ? ", "l'an 1 apr�s JC"));
        questionnaire.add(new Question("Qui sont les animateurs du festival � �juste pour rire� ?","St�phanne rousseau, franck dubosc"));
        questionnaire.add(new Question("Combien de nouvelles cha�nes sont apparu gr�ce � la TNT ?", "12"));
        questionnaire.add(new Question("Combien y a-t-il de signes astrologiques chinois ? ","12"));
        questionnaire.add(new Question("Quel est le 2�me nom de l'hippocampe ?","Le cheval de mer"));
        questionnaire.add(new Question("En quelle ann�e est mort JFK ? ", "1963"));
        questionnaire.add(new Question("Combien de dieu tra�ne � l'Olympe ?", "12"));
        questionnaire.add(new Question("Quel est l'�quivalent du pape au Tibet ?","Le dallai lama"));
    } 
}