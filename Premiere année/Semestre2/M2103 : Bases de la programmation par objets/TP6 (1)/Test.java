/*
 * The scenario
 *@author Rozenn Costiou 
*/
import question.*;
import java.util.ArrayList;
import java.lang.*;


public class Test {
     
    /**
 	 * The instance of classe
	 * @param args :  
 	*/
	public static void main(String[] args){
        //ScenarioQuestion1();
        ScenarioQuestion2();
        //ScenarioQuestionnaire1();
        //ScenarioQuestionnaire2();

    }
    /*
     * the scenario for the class Question but the constructor don't have parametre
    */
    public static void ScenarioQuestion1(){
        System.out.println ("");
        System.out.println("______________________________________ Creer une question avec constructeur sans parametre _________________________________________");
        Question question;
        question = new Question ();
        System.out.println ("");
        System.out.println ("---------------------- modifier la question----------------------");
        System.out.println ("");
        String textM;
        String textR;
        textM = "Quel pays a remporté la coupe du monde de football en 2014 ?";
        question.setText(textM);
        textR = question.getText();
        if (textM.equalsIgnoreCase(textR)){
            System.out.println ("methode setText ok");
        }
        else{
            System.out.println ("methode setText erreur");
        }
        System.out.println ("");
        System.out.println ("---------------------- modifie la reponse ----------------------");
        System.out.println ("");
        String answerM;
        String answerR;
        answerM = "Allemagne";
        question.setAnswer(answerM);
        answerR = question.getAnswer();
        if (answerM.equalsIgnoreCase(answerR)){
            System.out.println ("methode setAnswer ok");
        }
        else{
            System.out.println ("methode setAnswer erreur");
        }
        System.out.println ("---------------------- afficher la question ----------------------");
        System.out.println ("");
        textR = question.getText();
        if (textM.equalsIgnoreCase(textR)){
            System.out.println ("methode getText ok");
        }
        else{
            System.out.println ("methode getText erreur");
        }
        System.out.println ("");
        System.out.println ("---------------------- afficher la reponse ----------------------");
        System.out.println ("");
        answerR = question.getAnswer();
        if (answerM.equalsIgnoreCase(answerR)){
            System.out.println ("methode getAnswer ok");
        }
        else{
            System.out.println ("methode getAnswer erreur");
        }
        System.out.println ("");
        System.out.println ("---------------------- verifie la reponse ----------------------");
        System.out.println ("");
        boolean reponse;
        reponse = question.checkAnswer(answerM);
        if (reponse == true){
            System.out.println ("methode checkAnswer pour vrai ok");
        }
        else{
            System.out.println ("methode checkAnswer pour vrai erreur");
        }
        String answerF = "France";
        reponse = question.checkAnswer(answerF);
        if (reponse == false){
            System.out.println ("methode checkAnswer pour faux ok");
        }
        else{
            System.out.println ("methode checkAnswer pour faux erreur");
        }
        System.out.println ("");
        System.out.println ("---------------------- affiche la question ----------------------");
        System.out.println ("");
        question.display();
    }

    /*
     * the scenario for the class Question but the constructor don't have parametre
    */
    public static void ScenarioQuestion2(){
        System.out.println ("");
        System.out.println("______________________________________ Creer une question avec constructeur sans parametre _________________________________________");
        System.out.println ("");
        Question question;
        String text;
        String answer;
        text = "Quel célèbre dictateur dirigea l’URSS du milieu des années 1920 à 1953 ?";
        answer = "Staline";
        System.out.println ("---------------------- creer la question tout ok----------------------");
        System.out.println ("");
        question = new Question (text , answer);
        System.out.println ("");
        System.out.println ("---------------------- creer la question  avec parametre null---------------------");
        System.out.println ("");
        Question questione;
        String texte = null;
        String answere = null;
        questione = new Question (texte , answere);
        System.out.println ("");
        System.out.println ("---------------------- afficher la question ----------------------");
        System.out.println ("");
        String textR;
        textR = question.getText();
        if (text.equalsIgnoreCase(textR)){
            System.out.println ("methode getText ok");
        }
        else{
            System.out.println ("methode getText erreur");
        }
        System.out.println ("");
        System.out.println ("---------------------- afficher la reponse ----------------------");
        System.out.println ("");
        String answerR;
        answerR = question.getAnswer();
        if (answer.equalsIgnoreCase(answerR)){
            System.out.println ("methode getAnswer ok");
        }
        else{
            System.out.println ("methode getAnswer erreur");
        }
        System.out.println ("---------------------- modifier la question----------------------");
        System.out.println ("");
        String textM;
        textM = "Quel pays a remporté la coupe du monde de football en 2014 ?";
        question.setText(textM);
        textR = question.getText();
        if (textM.equalsIgnoreCase(textR)){
            System.out.println ("methode setText ok");
        }
        else{
            System.out.println ("methode setText erreur");
        }
        System.out.println ("");
        System.out.println ("---------------------- modifie la reponse ----------------------");
        System.out.println ("");
        String answerM;
        answerM = "Allemagne";
        question.setAnswer(answerM);
        answerR = question.getAnswer();
        if (answerM.equalsIgnoreCase(answerR)){
            System.out.println ("methode setAnswer ok");
        }
        else{
            System.out.println ("methode setAnswer erreur");
        }
        System.out.println ("");
        System.out.println ("---------------------- verifie la reponse ----------------------");
        System.out.println ("");
        boolean reponse;
        reponse = question.checkAnswer(answerM);
        if (reponse == true){
            System.out.println ("methode checkAnswer pour vrai ok");
        }
        else{
            System.out.println ("methode checkAnswer pour vrai erreur");
        }
        String answerF = "France";
        reponse = question.checkAnswer(answerF);
        if (reponse == false){
            System.out.println ("methode checkAnswer pour faux ok");
        }
        else{
            System.out.println ("methode checkAnswer pour faux erreur");
        }
        System.out.println ("");
        System.out.println ("---------------------- affiche la question ----------------------");
        System.out.println ("");
        question.display();
    }

    /*
     * the scenario for the class Questionnaire but the constructor don't have parametre
    */
    public static void ScenarioQuestionnaire1(){
        System.out.println ("");
        System.out.println("______________________________________ Creer un questionnaire avec constructeur sans parametre _________________________________________");
        System.out.println ("");
        Questionnaire ques;
        ques = new Questionnaire();
        System.out.println ("");
        System.out.println ("---------------------- ajouter des questions ----------------------");
        System.out.println ("");
        Question question1;
        String text1;
        String answer1;
        text1 = "Quel célèbre dictateur dirigea l’URSS du milieu des années 1920 à 1953 ?";
        answer1 = "Staline";
        question1 = new Question (text1 , answer1);
        ques.add(question1);
        Question question2;
        String text2;
        String answer2;
        text2 = "Dans quel pays peut-on trouver la Catalogne, l’Andalousie et la Castille ? ";
        answer2 = "Espagne";
        question2 = new Question (text2 , answer2);
        ques.add(question2);
        Question question3;
        String text3;
        String answer3;
        text3 = "Qui a dit “ Le sort en est jeté” (Alea jacta est)?";
        answer3 = "Jules Cesar";
        question3 = new Question (text3 , answer3);
        ques.add(question3);
        System.out.println ("");
        System.out.println ("---------------------- afficher les questions ----------------------");
        System.out.println ("");
        ques.display();
        System.out.println ("");
        System.out.println ("---------------------- nombre de questions ----------------------");
        System.out.println ("");
        int nbQ;
        nbQ = ques.getQNumber();
        if (nbQ == 3){
            System.out.println ("methode getQNumber ok");
        }
        else{
            System.out.println ("methode getQNumber erreur");
        }
        System.out.println ("");
        System.out.println ("---------------------- affiche la premiere questions ----------------------");
        System.out.println ("");
        String text;
        text = ques.getQuestionText(0);
        if (text.equalsIgnoreCase(text1)){
            System.out.println ("methode getQuestionText ok");
        }
        else{
            System.out.println ("methode getQuestionText erreur");
        }
        System.out.println ("");
        System.out.println ("---------------------- tire au hazard questions ----------------------");
        System.out.println ("");
        Question choixHazard;
        choixHazard = ques.pickAtRandom();
        choixHazard.display();
        Question choixHazard2;
        choixHazard2 = ques.pickAtRandom();
        choixHazard2.display();        
        Question choixHazard3;
        choixHazard3 = ques.pickAtRandom();
        choixHazard3.display();
    }

    /*
     * the scenario for the class Questionnaire
    */
    public static void ScenarioQuestionnaire2(){
        System.out.println ("");
        System.out.println("______________________________________ Creer un questionnaire avec constructeur avec parametre _________________________________________");
        System.out.println ("");
        Questionnaire ques;
        ArrayList <Question> questionList;
        questionList = new ArrayList<Question> (); 
        Question question1;
        String text1;
        String answer1;
        text1 = "Quel célèbre dictateur dirigea l’URSS du milieu des années 1920 à 1953 ?";
        answer1 = "Staline";
        question1 = new Question (text1 , answer1);
        questionList.add(question1);
        Question question2;
        String text2;
        String answer2;
        text2 = "Dans quel pays peut-on trouver la Catalogne, l’Andalousie et la Castille ? ";
        answer2 = "Espagne";
        question2 = new Question (text2 , answer2);
        questionList.add(question2);
        Question question3;
        String text3;
        String answer3;
        text3 = "Qui a dit “ Le sort en est jeté” (Alea jacta est)?";
        answer3 = "Jules Cesar";
        question3 = new Question (text3 , answer3);
        questionList.add(question3);
        ques = new Questionnaire(questionList);
        System.out.println ("");
        System.out.println ("---------------------- ajouter des questions ----------------------");
        System.out.println ("");
        Question question4;
        String text4;
        String answer4;
        text4 = "À qui doit-on la chanson “ I Shot the Sheriff” ?";
        answer4 = "Bob Marley";
        question4 = new Question (text4 , answer4);
        ques.add(question4);
        System.out.println ("");
        System.out.println ("---------------------- afficher les questions ----------------------");
        System.out.println ("");
        ques.display();
        System.out.println ("");
        System.out.println ("---------------------- nombre de questions ----------------------");
        System.out.println ("");
        int nbQ;
        nbQ = ques.getQNumber();
        if (nbQ == 4){
            System.out.println ("methode getQNumber ok");
        }
        else{
            System.out.println ("methode getQNumber erreur");
        }
        System.out.println ("");
        System.out.println ("---------------------- affiche la premiere questions ----------------------");
        System.out.println ("");
        String text;
        text = ques.getQuestionText(1);
        if (text.equalsIgnoreCase(text2)){
            System.out.println ("methode getQuestionText ok");
        }
        else{
            System.out.println ("methode getQuestionText erreur");
        }
        System.out.println ("");
        System.out.println ("---------------------- tire au hazard questions ----------------------");
        System.out.println ("");
        Question choixHazard1;
        choixHazard1 = ques.pickAtRandom();
        choixHazard1.display();
        Question choixHazard2;
        choixHazard2 = ques.pickAtRandom();
        choixHazard2.display();        
        Question choixHazard3;
        choixHazard3 = ques.pickAtRandom();
        choixHazard3.display();
    }

}