/*
 * The scenario
 *@author Rozenn Costiou 
*/
import question.*;
import java.util.*;
import java.lang.*;


public class ScenarioChoiseQuestion {
    /**
 	 * The instance of classe
	 * @param args :  
 	*/
    private static Questionnaire questionnaire;

	public static void main(String[] args){
        int nbGame = 0;
        int nbGameWin = 0;
        
        initialiseQuesionnaire();
        
        Scanner inputPlay = new Scanner(System.in);
        System.out.println("Voulez vous jouer ? ('o' pour oui 'n' pour non)");
        String strPlay = inputPlay.nextLine();
        
        
        while (strPlay.equals("o")){
        	nbGame++;
        	Question question = questionnaire.pickAtRandom();
            question.display();
            Scanner inputAnswer = new Scanner(System.in);
            String strAnswer = inputAnswer.nextLine();
            
            if (question.checkAnswer(strAnswer)==true){
                System.out.println("R�ponse correct");
                nbGameWin++;
            }else {
            	System.out.println("R�ponse incorrect, coup dur");
            }
            System.out.println("Voulez vous rejouer ? ('o' pour oui 'n' pour non)");
            strPlay = inputPlay.next();
        }
        System.out.println("Votre score : "+nbGameWin+"/"+nbGame);
        System.out.println("Aurevoir !");
    }
	
    public static void initialiseQuesionnaire(){
    	questionnaire = new Questionnaire();
    	
        ChoiceQuestion choiceQuestion1 = new ChoiceQuestion();
        choiceQuestion1.setText("Quelle pi�ce est absolument � prot�ger dans un jeu d'�chec ?");
        choiceQuestion1.addChoice ("la dame" , false);
        choiceQuestion1.addChoice ("le fou" , false);
        choiceQuestion1.addChoice ("le roi" , true);
        choiceQuestion1.addChoice ("le cavalier" , false);
        
        ChoiceQuestion choiceQuestion2 = new ChoiceQuestion();
        choiceQuestion2.setText("Combien de jours comporte une ann�e bissextile ?");
        choiceQuestion2.addChoice ("364" , false);
        choiceQuestion2.addChoice ("29" , false);
        choiceQuestion2.addChoice ("365" , false);
        choiceQuestion2.addChoice ("366" , true);
        
        questionnaire.add(choiceQuestion1);
        questionnaire.add(choiceQuestion2);
        
    } 
}