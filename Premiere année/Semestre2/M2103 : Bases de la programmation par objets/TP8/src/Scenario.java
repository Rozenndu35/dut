/**
 * 
 * @author Rozenn
 * @version 1
 */
import java.util.Arrays;
public class Scenario{
    public static void main (String[] args){
        //ScenarioComparePays();
        //ScenarioSetPays();
        ScenarioTriParSelection();
        ScenariotriRapideRec();
        
    }
    public static void ScenarioComparePays(){
        System.out.println ( "Test CompareTo");
        Pays pays1  = new Pays ("Chile" , 16454143, 756950);
        Pays pays2 = new Pays ("Cuba" , 11423952, 110860);
        int resultat;
        int marche=0;
        int nbTest=0;
        resultat = pays1.compareTo(pays2);
        nbTest ++;
        if ( resultat == -1){
            marche ++;
        }
        else{
            System.err.println ("erreur : inferieur");
        }
        resultat = pays1.compareTo(pays1);
        nbTest ++;
        if ( resultat == 0){
            marche ++;
        }
        else{
            System.err.println ("erreur : pour egale");
        }
        resultat = pays2.compareTo(pays1);
        nbTest ++;
        if ( resultat == 1){
            marche ++;
        }
        else{
            System.err.println ("erreur : pour superieur");
        }
        System.out.println ( "nombre de test reussi : " + marche + "/" + nbTest);
    }
    public static void ScenarioSetPays(){
        System.out.println ( "Test set...");
        Pays pays = new Pays ("Chile" , 16454143, 756950);
        int marche=0;
        int nbTest=0;
        nbTest ++;
        pays.setNom("Cuba");
        if (pays.getNom() == "Cuba"){
            marche ++;
        }
        else{
            System.err.println ("erreur : setNom");
        }
        nbTest ++;
        pays.setSurface(1);
        if (pays.getSurface() == 1){
            marche ++;
        }
        else{
            System.err.println ("erreur : setSurface");
        }
        nbTest ++;
        pays.setPopulation(100);
        if (pays.getPopulation() == 100){
            marche ++;
        }
        else{
            System.err.println ("erreur : setPopulation");
        }
        System.out.println ( "nombre de test reussi : " + marche + "/" + nbTest);
    }
    public static void ScenarioTriParSelection(){
        System.out.println ( "Test TriParSelection");
        Pays[] tabPays = new Pays[10];
        tabPays [0] = new Pays ("Cuba" , 11423952, 110860);
        tabPays [1] = new Pays ("Chile" , 16454143, 756950);
        tabPays [2] = new Pays ("Russia" , 140702094, 17075200);
        tabPays [3] = new Pays ("Norway" , 4644457, 323802);
        tabPays [4] = new Pays ("Nigeria" , 138283240, 923768);
        tabPays [5] = new Pays ("Paraguay" , 6831306, 406750);
        tabPays [6] = new Pays ("Oman" , 3311640, 212460);
        tabPays [7] = new Pays ("Yemen" , 23013376, 406750);
        tabPays [8] = new Pays ("Togo" , 5858673, 56785);
        tabPays [9] = new Pays ("France" , 64057790 , 643427);
        TriParSelection tri = new TriParSelection(tabPays);
        tri.trier();
        System.out.println (Arrays.toString(tabPays));
    }
    public static void ScenariotriRapideRec(){
        System.out.println ( "Test TriParSelection");
        Pays[] tabPays = new Pays[10];
        tabPays [0] = new Pays ("Cuba" , 11423952, 110860);
        tabPays [1] = new Pays ("Chile" , 16454143, 756950);
        tabPays [2] = new Pays ("Russia" , 140702094, 17075200);
        tabPays [3] = new Pays ("Norway" , 4644457, 323802);
        tabPays [4] = new Pays ("Nigeria" , 138283240, 923768);
        tabPays [5] = new Pays ("Paraguay" , 6831306, 406750);
        tabPays [6] = new Pays ("Oman" , 3311640, 212460);
        tabPays [7] = new Pays ("Yemen" , 23013376, 406750);
        tabPays [8] = new Pays ("Togo" , 5858673, 56785);
        tabPays [9] = new Pays ("France" , 64057790 , 643427);
        triRapideRec tri = new triRapideRec(tabPays);
        tri.trier();
        System.out.println (Arrays.toString(tabPays));
    }
}