/**
 * Class make a country
 * @author Rozenn
 * @version 1
 */
public class Pays implements Comparable<Pays> {
	private String nom;
	private double surface;
	private double population;
	
	/**
	 * the constructor of the country
	 * @param nom : the name
	 * @param population :  the population
	 * @param surface : the area
	 */
	public Pays(String nom , double population , double surface) {
		if ((nom !=null) &&(population>=0) &&(surface>=0)) {
			this.nom = nom;
			this.population = population;
			this.surface = surface;
		}
		else {
			this.nom = "";
			this.population = 0;
			this.surface = 0;
			System.err.println("Pays:Pays: nom ou population ou surface invalide ");
		}
	}
	/**
	 * get the name of the country
	 * @return the name
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * set the name of the country
	 * @param nom : the name of the country
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * get the area of the country
	 * @return the area
	 */
	public double getSurface() {
		return surface;
	}
	
	/**
	 * set the area of the country
	 * @param surface : the area
	 */
	public void setSurface(double surface) {
		this.surface = surface;
	}
	
	/**
	 * get the population of the country
	 * @return the population
	 */
	public double getPopulation() {
		return population;
	}
	
	/**
	 * set the population of the country
	 * @param population : the population
	 */
	public void setPopulation(double population) {
		this.population = population;
	}
	
	/**
	 * String a pays with the information
	 * @return a String of information
	 */
	public String toString() {
		String ret;
		ret = "le nom : " + this.nom + "\t" +"\t"+"la surface : " + this.surface + "\t" + "\t" + "la population : " + this.population + "\n";
		return ret;
		
	}

	@Override
	/**
	 * compare the area of two countries
	 * @param the contrie to want compare
	 * @return 0 if is equals , 1 if is superior or -1 if is inferior
	 */
	public int compareTo(Pays o) {
		int ret = 0;
		if (this.surface < o.surface){
			ret = 1;
		}
		else if (this.surface > o.surface){
			ret = -1;
		}
		return ret;
	}
}
