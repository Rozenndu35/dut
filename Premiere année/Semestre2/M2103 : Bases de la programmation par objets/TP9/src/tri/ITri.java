/**
 * The interface for the sort
 * @author Rozenn
 * @version 1
 */

package tri;

public interface ITri {
	public void trier();
}
