package utilitaire;


import java.io.*;
import java.util.*;

public class RWFile {
	/**
	 * put in the list the line of the file in a event
	 * @param fileName the file to sequence
	 * @return the list of the line
	 */
	public static ArrayList<String> readFile (String fileName){
		ArrayList<String> ret = new ArrayList<String>();
		try {
			Scanner in = new Scanner (new FileReader (fileName));
			while (in.hasNextLine()) {
				ret.add(in.nextLine());
			}
			in.close();
		} 
		catch (FileNotFoundException e) {
			System.out.println ("readFile- Fichier non trouve: " + fileName);
		}
		return ret;
		
	}
	
	/**
	 * write in a file the element in the list
	 * @param liste the list
	 * @param fileName the file
	 */
	public static void writeFile (ArrayList<String> liste , String fileName) {
		try {
			PrintWriter out = new PrintWriter (fileName);
			for( String ligne : liste) {
				out.println (ligne);
				out.close();
			}
		}
		catch (FileNotFoundException e) {
			System.out.println (e.getMessage());
		}
	}
		
}
