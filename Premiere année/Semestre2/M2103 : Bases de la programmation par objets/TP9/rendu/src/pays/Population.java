package pays;

import java.util.*;
import utilitaire.RWFile;

public class Population {
	private HashMap <String , Double> popMap;
	private HashMap <String , Double> areaMap;
	private ArrayList<Pays> listePays;
	
	/**
	 * the constructor of the population
	 * @param popFileName the file with the information of population
	 * @param areaFileName the file withe the information of area
	 */
	public Population (String popFileName , String areaFileName) {
		this.popMap = new HashMap <String , Double>();
		this.areaMap = new HashMap <String , Double>();
		this.listePays = new  ArrayList<Pays>();
		if ((popFileName != null) && (areaFileName != null)) {
			initializePopMap( popFileName);
			initializeAreaMap( areaFileName);
			initializeListPays();	
		}
	}

	/**
	 * get the HashMap population
	 * @return the popMap
	 */
	public HashMap <String , Double> getPopMap() {
		return this.popMap;
	}

	/**
	 * get the HashMap area
	 * @return the areaMap
	 */
	public HashMap <String , Double> getAreaMap() {
		return this.areaMap;
	}

	/**
	 * get the list of pays
	 * @return the listePays
	 */
	public ArrayList<Pays> getListePays() {
		return this.listePays;
	}
	/**
	 * init the HashMap of popuation with a file
	 * @param popFile the file with the information of population
	 */
	private void initializePopMap( String popFile) {
		ArrayList<String> recup = new ArrayList<String>();
		recup = RWFile.readFile (popFile);
		this.popMap = asMap (recup);
	}
	
	/**
	 * init the HashMap of area with a file
	 * @param areaFile the file with the information of area
	 */
	private void initializeAreaMap( String areaFile) {
		ArrayList<String> recup = new ArrayList<String>();
		recup = RWFile.readFile (areaFile);
		this.areaMap = asMap (recup);
	}
	/**
	 * take the contry in a line withh other information
	 * @param line a line of the file
	 * @return the country
	 */
	private String extractContry(String line) {
		String ret = "";
		for ( int i =0 ; ((i<line.length()) && (Character.isDigit(line.charAt(i)) == false)) ; i++ ){
			ret += line.charAt(i);
		}
		ret = ret.trim();
		return ret;
	}
	
	/**
	 * take the value (population or area) in a line withh other information
	 * @param line a line of the file
	 * @return the value
	 */
	private double extractValue(String line) {
		double ret= 0;
		int indice = 0;
		for ( int i =0 ; ((i<line.length()) && (Character.isDigit(line.charAt(i)) == false)) ; i++ ){
			indice = i;
		}
		String chaine = line.substring (indice);
		chaine = chaine.trim();
		ret = Double.parseDouble(chaine);
		return ret;
	}
	/**
	 * extract and put the information in a HashMap
	 * @param liste a liste with the line of file for element
	 * @return the HashMap withe the contry and the value
	 */
	private HashMap<String,Double> asMap (ArrayList<String> liste){
		HashMap<String,Double> ret = new HashMap<String,Double>();
		for (String e : liste) {
			String pays =extractContry(e);
			double pop = extractValue(e);
			ret.put (pays, new Double (pop));
		}
		return ret;
		
	}
	/**
	 * initialize the list of the contry
	 */
	private void initializeListPays() {
		Pays pays;
		Set<String> nomPays = popMap.keySet();
		if (popMap.size() == areaMap.size()) {
			for (String nom : nomPays) {
				pays =new Pays (nom , popMap.get(nom), areaMap.get(nom));
				this.listePays.add(pays);
			}
		}
	}
}
