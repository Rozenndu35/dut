/*
 * the staff service of the company
 * @author Rozenn Costiou 
*/

package staff;
import java.util.ArrayList;
import java.lang.*;

public class Staff{
    private ArrayList <StaffMember> staffList;
    /*
     * The constructeur
    */
    public Staff(){
       this.staffList= new ArrayList<StaffMember>();
    }
    /*
     * add a member in the staff list
     * @param menber : the member
    */
    public void addNewMember(StaffMember member){
        if (member != null){
            staffList.add(member);
            System.out.println ("membre ajouter");
        }
    }
    /*
     * get the member to cherche in the compani
     * @param ind : the index for the member to cherche
    */
    public StaffMember getMember(int ind){
        StaffMember ret = null;
        if ((ind >=0) && (ind <= this.staffList.size())){
            ret=this.staffList.get(ind);
        }
        else {
            System.err.println("Staff : getMember : ind hotside of the list");
        } 
        return ret;
    }

    /*
     * Print the member with the paid in the compani
    */
    public void payday(){
        StaffMember member; 
        double pay;
        String information;
        for (StaffMember sM : staffList){
            information = sM.toString();
            System.out.println(information);
            pay= sM.pay();
            if (pay == 0 ){
                System.out.println("Thanks for your help");
            }
            else{
                System.out.println("the paid : " + pay);
            }
            System.out.println ("------------------------------------------------");
        }
    }

}