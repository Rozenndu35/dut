/*
 * Create a Executive
 * @author Rozenn Costiou 
*/

package staff;
import java.util.ArrayList;
import java.lang.*;

public class Executive extends Employee{
    private double bonus;
    
    /*
     * the constructeur
     * @param eName : the name of the employee
     * @param eAdresse : the adresse of the employee
     * @param ePhone : the phone of the employee
     * @param socSecurityNumber : the  social security number of the employee
     * @param rate : the salry of the employee
    */
    public Executive(String eName , String eAdresse , String ePhone , String socSecNumber , double rate){
        super (eName , eAdresse , ePhone , socSecNumber , rate);
        this.bonus = 0;
    }

    /*
     * give the value of tha bonus
     * @param execBonus : the value of the bonus
    */
    public void awardBonus(double execBonus){
        this.bonus = execBonus;
    }
    /*
     * calculating the new salary
     * @returne the paid
    */
    public double pay(){
        double ret;
        ret = super.payRate + bonus;
        return ret;
    }

}
