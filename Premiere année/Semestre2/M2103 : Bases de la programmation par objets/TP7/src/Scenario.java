/*
 * The scenario
 * @author Rozenn Costiou 
*/

import staff.*;
import java.util.ArrayList;
import java.lang.*;

public class Scenario{
    /**
 	 * The instance of classe
	 * @param args :  
 	*/
    public static void main(String[] args){
        //testEmployee();
        //testHourly();
        //testExecutive();
        //testVolunteer();
        testStaff();
        
    }

    private static void testStaff() {
		Staff staff1 = new Staff();
        Executive exec1 = new Executive ("Rozenn", "Acigné 35690" , " 02.99.62.53.39" , "1234567890" , 5);
        Hourly hourly1 = new Hourly ("Ewen", "Concarneau" , " 02.98.62.53.39" , "1234567890" , 5);
        Hourly hourly2 = new Hourly ("Klervia", "Ploermel" , " 02.97.62.53.39" , "1234567890" , 5);
        Volunteer vol1 = new Volunteer ("Brieuc", "Grenoble" , " 09.99.62.53.39");
        exec1.awardBonus(16.0);
        hourly1.addHours(12);
        hourly2.addHours(8);
		staff1.addNewMember(exec1);
		staff1.addNewMember(hourly1);
		staff1.addNewMember(hourly2);
		staff1.addNewMember(vol1);
		if(staff1.getMember(1).equals(hourly1)) {
			System.out.println("methode Staff getMember OK");
		}
		else {
			System.out.println("methode Staff getMember erreur");
		}
		
		staff1.payday();
	}

	private static void testEmployee(){
        Employee employee;
        String nom = "Rozenn";
        String adresse = " Acigné 35690";
        String phone = " 02.99.62.53.39";
        String socSecuritynumber = "1234567890";
        double rate = 5;
        double pay;
        String affiche;
        System.out.println("--------------------------pour rate ok------------------------------");
        employee = new Employee (nom, adresse , phone , socSecuritynumber , rate);
        pay = employee.pay();
        if (pay == 5){
            System.out.println(" methode pay Employee OK");
        }
        else {
            System.out.println(" methode pay Employee erreur");
        }
        affiche = employee.toString();
        System.out.println (affiche);
        System.out.println("---------------------------pour rate inferieur a 0-----------------------");
        rate = -5 ;
        employee = new Employee (nom, adresse , phone , socSecuritynumber , rate);
        pay = employee.pay();
        if (pay == -1){
            System.out.println(" methode pay Employee OK");
        }
        else {
            System.out.println(" methode pay Employee erreur");
        }
        affiche = employee.toString();
        System.out.println (affiche);

        
    }

    private static void testHourly(){
        Hourly employeH;
        String nom = "Rozenn";
        String adresse = " Acigné 35690";
        String phone = " 02.99.62.53.39";
        String socSecuritynumber = "1234567890";
        double rate = 5;
        double pay;
        int heuresup = 10;
        String affiche;
        employeH = new Hourly (nom, adresse , phone , socSecuritynumber , rate);
        employeH.addHours (heuresup);
        pay = employeH.pay();
        if (pay == 50){
            System.out.println(" methode pay Hourly OK");
        }
        else {
            System.out.println(" methode pay Hourly erreur");
        }
        affiche = employeH.toString();
        System.out.println (affiche);
    }

    private static void testExecutive(){
        Executive employeExe;
        String nom = "Rozenn";
        String adresse = " Acigné 35690";
        String phone = " 02.99.62.53.39";
        String socSecuritynumber = "1234567890";
        double rate = 5;
        double pay;
        double bonus = 10;
        employeExe = new Executive (nom, adresse , phone , socSecuritynumber , rate);
        employeExe.awardBonus(bonus);
        pay = employeExe.pay();
        if (pay == 15){
            System.out.println(" methode pay Executive OK");
        }
        else {
            System.out.println(" methode pay Executive erreur");
        }
    }

    private static void testVolunteer(){
        Volunteer volunteer;
        String nom = "Rozenn";
        String adresse = " Acigné 35690";
        String phone = " 02.99.62.53.39";
        double pay;
        volunteer = new Volunteer (nom, adresse , phone );
        pay = volunteer.pay();
        if (pay == 0){
            System.out.println(" methode pay Volunteer OK");
        }
        else {
            System.out.println(" methode pay  volunteer erreur");
        }
    }

}
