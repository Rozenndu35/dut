/*
 * A mamber of the society
 * @author Rozenn Costiou 
*/

package staff;
import java.util.ArrayList;
import java.lang.*;

public abstract class StaffMember{
    protected String name;
    protected String address;
    protected String phone;
    /*
     * the constructeur of StaffMember
     * @param eName : the name of the member
     * @param eAddress : the adresse of the member
     * @param ePhone : the phone of the member
    */
    public StaffMember (String eName, String eAddress, String ePhone){
        if ((eName != null)&& (eAddress!= null)&&(ePhone != null)){
            this.name= eName;
            this.address = eAddress;
            this.phone = ePhone;
        }
        else{
            this.name = "";
            this.address = "";
            this.phone = "";
            System.err.println("StaffMember: StaffMember : variable invalibel");
        }
    }

    /*
     * print the information of the member
     * @return the information
    */
    public String toString(){
        String ret;
        ret = "nom : " + this.name + "\n" + "adresse : " + this.address + "\n" +"numero telephone : " + this.phone;
        return ret;
    }

    /*
     * the paid of the member
    */
    public abstract double pay();

    
}