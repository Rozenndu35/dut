/*
 * Create the a hourly employee
 * @author Rozenn Costiou 
*/

package staff;
import java.util.ArrayList;
import java.lang.*;

public class Hourly extends Employee{
    private int hoursWorked;
    
    /*
     * the constructeur
     * @param eName : the name of the employee
     * @param eAdresse : the adresse of the employee
     * @param ePhone : the phone of the employee
     * @param socSecurityNumber : the  social security number of the employee
     * @param rate : the salry of the employee
    */
    public Hourly(String eName , String eAdresse , String ePhone , String socSecNumber , double rate){
        super (eName , eAdresse , ePhone , socSecNumber , rate);
        this.hoursWorked = 0;
    }

    /*
     * add the number of hours of work
     * @param moreHours : the hours to work the employee 
    */ 
    public void addHours(int moreHours){
        this.hoursWorked += moreHours;
    }

    /*
     * calculating the new salary
     * @returne the paid
    */
    public double pay(){
        double ret;
        ret = super.payRate * this.hoursWorked;
        return ret;
    }

    /*
     * print the paid of a hourly employee
     * @return the information
    */
    public String toString (){
        String ret;
        ret = super.toString() + "\n" + "nombre d'heure travailler : " + this.hoursWorked + "\n" + "nouveau salaire : " + pay() ;
        return ret;
    }
}
