/*
 * Create a Salarie
 * @author Rozenn Costiou 
*/
package staff;
import java.util.ArrayList;
import java.lang.*;

public class Employee extends StaffMember{
    protected String socialSecurityNumber;
    protected double payRate;
    /*
     * the constructeur
     * @param eName : the name of the employee
     * @param eAdresse : the adresse of the employee
     * @param ePhone : the phone of the employee
     * @param socSecurityNumber : the  social security number of the employee
     * @param rate : the salry of the employee
    */
    public Employee (String eName , String eAdresse , String ePhone , String socSecurityNumber , double rate){
        super(eName, eAdresse , ePhone);
        this.socialSecurityNumber = socSecurityNumber;
        if ( rate >= 0){
            this.payRate = rate;
        }
        else {
            this.payRate = -1;
            System.err.println("Employee : Employee : payRate  are not valibel");
        }
        
    }

    /*
     * print the information of the employee
     * @return the information
    */
    public String toString(){
        String ret;
        if (payRate >= 0){
            ret = super.toString() + "\n"+ "numero de seurite sociale : " + this.socialSecurityNumber + "\n" + "salaire : " + this.payRate;
        }
        else{
            ret = super.toString() + "\n"+ "numero de seurite sociale : " + this.socialSecurityNumber + "\n" + "salaire : erreur";
        }
        return ret;
    }
    /*
     * give the paid of the employee
     * @return the pay
    */
    public double pay(){
        double ret = payRate;
        if (payRate < 0){
            System.err.println("Employee : pay : payRate are not valibel");
            ret = -1;
        }
        return ret;
    }
}
