/*
 * Create a Volunteer
 * @author Rozenn Costiou 
*/

package staff;
import java.util.ArrayList;
import java.lang.*;

public class Volunteer extends StaffMember{

    /*
     * the constructeur 
     * @param eName : the name of the employee
     * @param eAdresse : the adresse of the employee
     * @param ePhone : the phone of the employee
    */
    public Volunteer (String eName , String eAdresse , String ePhone){
        super(eName, eAdresse , ePhone);
    }

    /*
     * give the paid of the employee
     * @return the pay
    */
    public double pay(){
        double ret = 0.00;
        return ret;
    }
}
