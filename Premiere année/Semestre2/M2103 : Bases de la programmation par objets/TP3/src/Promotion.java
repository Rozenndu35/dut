/**
 * Create the promotion
 *@author Rozenn Costiou 
 */

 public class Promotion {
     private String nom; // nom de la promo
     private Etudiant[] listeEtudiants; // liste des etudiant de la promo
     private int nbEtudiant; // nombre d'etudiant dans la promo

    /**
     * Create a promotion
     * @param nom : the name
     * @param liste : the list of studient
    */
    public Promotion(String nom, Etudiant[] liste){
        if ((nom != null) && (liste != null)){
            this.nom = nom;
            this.listeEtudiants = liste;
        }
        else{
            System.err.println ("Promotion : promotion nom ou liste invalide");
            this.nom = "";
            this.listeEtudiants = new Etudiant[0];
        }
    }
    /**
     * Get the name
     * @return the name
    */
    public String getNom (){
        return this.nom;
    }
    /**
     * set the name
     * @param nom : the name
     */
    public void setNom (String nom){
        this.nom = nom;
    }
    /**
     * Get the average mark of the promotion
     * @return the average if invalide return -1
    */
    public double moyenne() {
        double ret = -1;
        int nbEtudiant = listeEtudiants.length;
        int i =0;
        double moyenne = 0;
        if (nbEtudiant != 0){
            while (i< nbEtudiant){
                moyenne = moyenne + this.listeEtudiants [i].moyenneGenerale();
                i ++;
            }
            ret = moyenne / nbEtudiant;
            ret = (double) Math.round( (ret) *  100) / 100;
        }
        return ret;
    }

    /**
     * searched the average best
     * @return the best average
    */
    public double moyenneMax (){
        double ret = -1;
        int nbEtudiant = listeEtudiants.length;
        int i =0;
        if (nbEtudiant != 0){
            while (i< nbEtudiant){
                if (ret <this.listeEtudiants [i].moyenneGenerale()){
                    ret = this.listeEtudiants [i].moyenneGenerale();
                }
                i ++;
            }
        }
        ret = (double) Math.round( (ret) *  100) / 100;
        return ret;
    }

    /**
     * searched the average worst
     * @return the worst average
    */
    public double moyenneMin (){
        double ret = -1;
        int nbEtudiant = listeEtudiants.length;
        int i =0;
        if (nbEtudiant != 0){
            ret = this.listeEtudiants[i].moyenneGenerale();
            i++;
            while (i < nbEtudiant){
                if (ret > this.listeEtudiants[i].moyenneGenerale()){
                    ret = this.listeEtudiants[i].moyenneGenerale();
                }
                i ++;
            }
        }
        ret = (double) Math.round( (ret) *  100) / 100;
        return ret;
    }

    /**
     * calculate he average mark for a subject
     * @param i : the index of the subject 
     * @return the average if invalide return -1
    */

    public double moyenneMatiere(int i){
        double ret = -1;
        int nbEtudiant = listeEtudiants.length;
        int j =0;
        double moyenne = 0;
        if (nbEtudiant != 0){
            while (j< nbEtudiant){
                moyenne = moyenne + this.listeEtudiants [j].moyenneMatiere(i);
                j ++;
            }
            ret = moyenne / nbEtudiant;
        }
        ret = (double) Math.round( (ret) *  100) / 100;
        return ret;
    }

    /**
     * searched the best of the promotion
     * @return the malor
    */
    public Etudiant getMajor(){
        Etudiant ret = null;
        double moyenneMajor;
        int i = 0; 
        boolean trouver = false;
        moyenneMajor = moyenneMax();
        if (moyenneMajor != -1){
            while ((trouver == false) && (i<listeEtudiants.length)){
                if (moyenneMajor == this.listeEtudiants[i].moyenneGenerale()){
                    trouver = true;
                    ret = this.listeEtudiants[i];
                }
                i++;
            }
        }
        else {
            System.err.println("Promotion : getMajor : pas de moyenne");
        }
        return ret;
    }
    /**
     * search the student in the list
     * @param nom : the name of the student to look for
     * @return the student
    */
    public Etudiant getEtudiant ( String nom ){
        Etudiant ret = null;
        int i = 0; 
        boolean trouver;
        if (nom != null){
            while ((trouver = false) && (i<listeEtudiants.length)){
                if (nom == this.listeEtudiants[i].getNom()){
                    trouver = true;
                    ret = this.listeEtudiants[i];
                }
                i++;
            }
        }
        else {
            System.err.println ("Promotion : getEtudient : nom donner invalide ");
        }
        return ret;
    }
    
    /**
     * returns a string representing the promotion.
     * @return string representing the promotion.
    */
    public String toString(){
        String ret = new String("Promotion invalide");
        Etudiant major;
        int nbEtudiant = listeEtudiants.length;
        String[] matiere ;
        boolean egale = true;
        int i= 0;
        int j = 0;
        if ( listeEtudiants.length > 0){
            major = getMajor();
            matiere = listeEtudiants[0].getMATIERES();
            while (( i < nbEtudiant -1) && (egale == true)){
                if (listeEtudiants[i].getMATIERES() != listeEtudiants[i+1].getMATIERES()){
                    egale = false;
                }
                i++;
            }
            i=0;
            if ((nbEtudiant > 0) && (egale == true)){
                ret = "Promotion : " + "\t" + this.nom + "\n";
                ret = ret + "nombre d'etudient :" + nbEtudiant +"\t" + "Moyenne de la promotion : " + moyenne() + "\n" ;
                ret = ret + "moyenne max : " + moyenneMax() + "\t" + "moyenne min : " + moyenneMin() + "\n";
                ret += "majeur de promo : " + major.getNom() + "\n";
                ret += "\n" + "nom" +  "\t" + "moyenne generale" + "\t" ;
                while ( j < matiere.length){
                    ret += matiere[j]+ "\t"  ;
                    j++;
                }
                ret += "\n" + "classe" + "\t" + moyenne() + "\t" ;
                j=0;
                while (j< matiere.length){ 
                        ret += "\t" + moyenneMatiere(j) + "\t";
                        j++;
                    }
                while (i< nbEtudiant){
                    ret +=  "\n" + listeEtudiants[i].getNom() + "\t" + listeEtudiants[i].moyenneGenerale();
                    j=0;
                    while (j< matiere.length){ 
                        ret +="\t" + listeEtudiants[i].moyenneMatiere(j) + "\t";
                        j++;
                    }
                    i++;
                } 
            }            
            else{
                System.err.println("Promotion: toString: nb d'etudiant invalide ou nombre de matiere differente entre les etudiants ");
            }
        }
        else {
            System.err.println( "Promotion : toString : nb d'etudient invaide ");
        }
        return ret ;
    }

    /**
     * add a new student in the promotion
     * @param etudiant : the new student
    */
    public void addEtudiant(Etudiant etudiant){
        if (nbEtudiant< (listeEtudiants.length - 1)){
            listeEtudiants[nbEtudiant] = etudiant;
            nbEtudiant ++;
            System.out.println("etudiant ajouter" + nbEtudiant);
        }
        else{
            System.err.println("Promotion : addEtudiant : liste deja complete");
        }
    }

    /**
     * Create a promotion
     * @param nom : the name
     * @param tailleMax : the
    */
    public Promotion(String nom, int tailleMax){
        if ((nom != null) && (tailleMax > 0)){
            this.nom = nom;
            this.listeEtudiants = new Etudiant[tailleMax]; 
            this.nbEtudiant =0;
            System.out.println("promotion créer");

        }
        else{
            System.err.println ("Promotion : promotion nom ou tailleMax invalide");
            this.nom = "";
            this.listeEtudiants = new Etudiant[0];
        }
    }
 }
