/**
 * The test for the class Promotion
 *@author Rozenn Costiou 
*/
public class TestPromotion {
	static int nbTest =0;
	static int nbTestOK =0;
	/**
 	 * The instance of classe
	 * @param args :  
 	*/
	public static void main(String[] args){
		testPromotion();
		testGetNom();
		testSetNom();
		testPromotion2();
		testAddEtudiant();
		System.out.println ("\n"+"nombre de test reussi : "+ nbTestOK + "\n" + "pour "+ nbTest + " effectuer");
    }
    public static void testPromotion(){
		System.out.println("----Test du constructeur et toString (verifier les moyenne si elle semble corecte et le major----");
        System.out.println("----Pour tout les parametre OK----");
		Etudiant[] liste = new Etudiant [4];
        String[] matieres = new String [5];
		double[] coeff = new double[5];
		matieres[0] = "Mathematique";
		matieres[1] = "Anglais";
		matieres[2] = "Economie";
		matieres[3] = "Gestion";
		matieres[4] = "Programation";
        coeff[0] = 2;
		coeff[1] = 3;
		coeff[2] = 1.5;
		coeff[3] = 2;
		coeff[4] = 3;
        Etudiant e1 = new Etudiant("Costiou", matieres, coeff,3);
        Etudiant e2 = new Etudiant("Bautrais", matieres, coeff,3);
        Etudiant e3 = new Etudiant("Bouder", matieres, coeff,3);
        Etudiant e4 = new Etudiant("Davail", matieres, coeff,3);
        liste[0] = e1;
        liste[1] = e2;
        liste[2] = e3;
        liste[3] = e4;
		Promotion p1 = new Promotion("Info 1", liste);
		System.out.println (p1.toString());

		System.out.println("----Pour liste Etudiant = 0 , pas remplis----");
		Etudiant[] liste1 = new Etudiant [0];
		p1 = new Promotion("Info 1", liste1);
		System.out.println (p1.toString());
    }
	public static void testGetNom(){
		System.out.println("----Test de getNom ----");
        Etudiant[] liste = new Etudiant [4];
        String[] matieres = new String [5];
		double[] coeff = new double[5];
		Etudiant e1 = new Etudiant("Costiou", matieres, coeff,3);
        Etudiant e2 = new Etudiant("Bautrais", matieres, coeff,3);
        Etudiant e3 = new Etudiant("Bouder", matieres, coeff,3);
        Etudiant e4 = new Etudiant("Davail", matieres, coeff,3);
        liste[0] = e1;
        liste[1] = e2;
        liste[2] = e3;
        liste[3] = e4;
		Promotion p1 = new Promotion("Info 1", liste);
		nbTest ++;
		if (p1.getNom() == "Info 1"){
			System.out.println("Test Ok");
			nbTestOK ++;
		}
		else{
			System.out.println("erreur");
		}
	}

	public static void testSetNom(){
		System.out.println("----Test de setNom ----");
        Etudiant[] liste = new Etudiant [4];
        String[] matieres = new String [5];
		double[] coeff = new double[5];
		Etudiant e1 = new Etudiant("Costiou", matieres, coeff,3);
        Etudiant e2 = new Etudiant("Bautrais", matieres, coeff,3);
        Etudiant e3 = new Etudiant("Bouder", matieres, coeff,3);
        Etudiant e4 = new Etudiant("Davail", matieres, coeff,3);
        liste[0] = e1;
        liste[1] = e2;
        liste[2] = e3;
        liste[3] = e4;
		Promotion p1 = new Promotion("Info 1", liste);
		nbTest ++;
		p1.setNom("Info 2");
		if (p1.getNom() == "Info 2"){
			System.out.println("Test Ok");
			nbTestOK ++;
		}
		else{
			System.out.println("erreur");
		}
	}

	public static void testPromotion2(){
		System.out.println("----Test du constructeur ----");
		System.out.println("----Pour taille max = 5 , nom OK, ----");
		Promotion p1 = new Promotion("Info 1", 5);

		System.out.println("----Pour taille max = 0 , nom OK, ----");
		p1 = new Promotion("Info 1", 0);

		System.out.println("----Pour taille max = -6 , nom OK, ----");
		p1 = new Promotion("Info 1", -6);

		System.out.println("----Pour taille max = 5 , nom invalide, ----");
		p1 = new Promotion(null, 5);
    }

	public static void testAddEtudiant(){
		String[] matieres = new String [5];
		double[] coeff = new double[5];
		matieres[0] = "Mathematique";
		matieres[1] = "Anglais";
		matieres[2] = "Economie";
		matieres[3] = "Gestion";
		matieres[4] = "Programation";
        coeff[0] = 2;
		coeff[1] = 3;
		coeff[2] = 1.5;
		coeff[3] = 2;
		coeff[4] = 3;
        Etudiant e1 = new Etudiant("Costiou", matieres, coeff,3);
        Etudiant e2 = new Etudiant("Bautrais", matieres, coeff,3);
		Promotion p1 = new Promotion("Info 1", 5);
		System.out.println("----Test du constructeur ----");
		System.out.println("----Pour taille max = 5 avec de la place ----");
		p1 = new Promotion("Info 1", 5);
		System.out.println("----Premier etudiant ----");
		p1.addEtudiant(e1);
		System.out.println("----Deuxieme etudiant ----");
		p1.addEtudiant(e2);
		
		System.out.println("----Pour taille max = 1 avec  pas de place ----");
		p1 = new Promotion("Info 1", 1);
		System.out.println("----Premier etudiant ----");
		p1.addEtudiant(e1);
		System.out.println("----Deuxieme etudiant ----");
		p1.addEtudiant(e2);

		System.out.println("----Pour taille max = 0 ----");
		p1 = new Promotion("Info 1", 0);
		p1.addEtudiant(e1);
	}
}