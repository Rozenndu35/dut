/**
 * The scenario for the class Promotion
 *@author Rozenn Costiou 
*/
public class ScenarioPromotion {
    public static void main(String[] args){
		promotion1();
		promotion2();
		promotion3();
		promotion4();
		promotion5();
    }
    public static void promotion1(){
		System.out.println("----avec 4 etudient 5matiere et trois note----");
		Etudiant[] liste = new Etudiant [4];
        String[] matieres = new String [5];
		double[] coeff = new double[5];
		matieres[0] = "Mathematique";
		matieres[1] = "Anglais";
		matieres[2] = "Economie";
		matieres[3] = "Gestion";
		matieres[4] = "Programation";
        coeff[0] = 2;
		coeff[1] = 3;
		coeff[2] = 1.5;
		coeff[3] = 2;
		coeff[4] = 3;
        Etudiant e1 = new Etudiant("Costiou", matieres, coeff,3);
        Etudiant e2 = new Etudiant("Bautrais", matieres, coeff,3);
        Etudiant e3 = new Etudiant("Bouder", matieres, coeff,3);
        Etudiant e4 = new Etudiant("Davail", matieres, coeff,3);
        liste[0] = e1;
        liste[1] = e2;
        liste[2] = e3;
        liste[3] = e4;
		Promotion p1 = new Promotion("Info 1", liste);
		System.out.println (p1.toString());
    }
    public static void promotion2(){
		System.out.println("----avec 0 etudiant ----");
		Etudiant[] liste = new Etudiant [0];
        Promotion p1 = new Promotion("Info 1", liste);
		System.out.println (p1.toString());
    }

	public static void promotion3(){
		String[] matieres = new String [5];
		double[] coeff = new double[5];
		matieres[0] = "Mathematique";
		matieres[1] = "Anglais";
		matieres[2] = "Economie";
		matieres[3] = "Gestion";
		matieres[4] = "Programation";
        coeff[0] = 2;
		coeff[1] = 3;
		coeff[2] = 1.5;
		coeff[3] = 2;
		coeff[4] = 3;
        Etudiant e1 = new Etudiant("Costiou", matieres, coeff,3);
        Etudiant e2 = new Etudiant("Bautrais", matieres, coeff,3);
		Promotion p1 = new Promotion("Info 1", 5);
		System.out.println("----Test du constructeur ----");
		System.out.println("----Pour taille max = 5 avec de la place ----");
		p1 = new Promotion("Info 1", 5);
		System.out.println("----Premier etudiant ----");
		p1.addEtudiant(e1);
		System.out.println("----Deuxieme etudiant ----");
		p1.addEtudiant(e2);
    }
	public static void promotion4(){ 
		String[] matieres = new String [5];
		double[] coeff = new double[5];
		matieres[0] = "Mathematique";
		matieres[1] = "Anglais";
		matieres[2] = "Economie";
		matieres[3] = "Gestion";
		matieres[4] = "Programation";
        coeff[0] = 2;
		coeff[1] = 3;
		coeff[2] = 1.5;
		coeff[3] = 2;
		coeff[4] = 3;
        Etudiant e1 = new Etudiant("Costiou", matieres, coeff,3);
        Etudiant e2 = new Etudiant("Bautrais", matieres, coeff,3);
		System.out.println("----Pour taille max = 2 avec  pas de place ----");
		Promotion p1 = new Promotion("Info 1", 2);
		System.out.println("----Premier etudiant ----");
		p1.addEtudiant(e1);
		System.out.println("----Deuxieme etudiant ----");
		p1.addEtudiant(e2);
	}
	public static void promotion5(){ 
		String[] matieres = new String [5];
		double[] coeff = new double[5];
		matieres[0] = "Mathematique";
		matieres[1] = "Anglais";
		matieres[2] = "Economie";
		matieres[3] = "Gestion";
		matieres[4] = "Programation";
        coeff[0] = 2;
		coeff[1] = 3;
		coeff[2] = 1.5;
		coeff[3] = 2;
		coeff[4] = 3;
        Etudiant e1 = new Etudiant("Costiou", matieres, coeff,3);
        Etudiant e2 = new Etudiant("Bautrais", matieres, coeff,3);
		System.out.println("----Pour taille max = 0 ----");
		Promotion p1 = new Promotion("Info 1", 0);
		p1.addEtudiant(e1);
	}
}
