# -*- coding: utf-8 -*-
"""
Created on Thu Mar 14 11:08:42 2019

@author: rozenn
"""
import numpy as np

# variable 
A = np.array([[1,2,3,4],[3,4,1,2], [2,3,4,1]])
B = np.array([[1,2,3,1],[1,3,-1,2],[2,-1,2,-1],[0,1,-1,2]])
C = np.array([[1,-1,0,1],[-1,1,1,1],[1,0,-1,-1]])
D = np.array([[2],[1],[-1],[0]])
E = np.array([[1],[3],[5],[7]])
I = np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]])
#Exercice 1
# question 1
print ("operation  A*B")
print (np.dot (A,B))
print ("operation  B*A : marche pas")
#print (np.dot (B,A)) 
print ("operation  A*D")
print (np.dot (A,D))
print ("operation  A*C : marche pas")
#print (np.dot (A,C))
print ("operation  C*E")
print (np.dot (C,E))
print ("operation  A+B : marche pas")
#print (A+B)
print ("operation  A+C")
print (A+C)
print ("operation  B - 2I")
print (B - 2*I)
print ("")

#Question 2
print ("dimention de A")
print (np.shape(A))
print ("dimention de B")
print (np.shape(B))
print ("dimention de C")
print (np.shape(C))
print ("dimention de D")
print (np.shape(D))
print ("dimention de E")
print (np.shape(E))
print ("nombre de ligne de C")
print (np.shape(C)[0])
print ("nombre de colone de C")
print (np.shape(C)[1])
print ("")

#Question 3
print ("Rang(C) : " ,  np.linalg.matrix_rank(C))
print ("")

#Question 4
print ("transposer de A" )
tA = np.transpose(A)
print (tA)
print ("operation tA * A")
print (np.dot (tA,A))
print ("tA*A est elle symetrique?")
X = np.dot (tA,A)
print (np.transpose (X) == X)
print ("                                                     oui")
print ("")

#Question 5
print ("determinant de tA*A =" , np.linalg.det(X))
print ("                                                     on en pense : ................")
print ("rang de tA*A =" , np.linalg.matrix_rank(X))
print ("inverse de tA*A = ")
iA = np.linalg.inv(X)
print ( iA)
print ("                                                     on en pense : ................")
print ("")

#Question 6
print ("determinant de B =" , np.linalg.det(B))
print ("inverse de B = ")
iB = np.linalg.inv(B)
print ( iB)
print ("")

#Question 7
print ("resolution de BX=E")
X1 = np.dot (iB,E)
print ("premiere solution X= iB * E")
print (X1)
print ("deuxieme solution" )
X2 = np.linalg.solve(B,E)
print (X2)
