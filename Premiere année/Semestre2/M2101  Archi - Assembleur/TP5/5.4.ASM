;----------------------------------------------------------------------------------------------------;
;	Permet le calcul et l'affichage du pgcd de 2 nombres rentr�s par l'utilisateur 		     ;
;----------------------------------------------------------------------------------------------------;

MAIN:
	call 34	; appel le sous programme permettant la saisie de l'entier
	call 34 ; appel le sous programme permettant la saisie de l'entier
	call 54	; appel du sous programme permettant le calcul du pgcd
	call 74 ; appel du sous programme permettant l'affichage de l'entier

;&&&&&&&&& debut de la saisie de l'entier &&&&&&&&&&&&&&&&&&&;

RENTRERNOMBRE:
	org 34
	pop dl		; stock l'adresse du retour du programme appelant
	mov bl,0	; initialise bl qui servira � stocker l'entier

NOMBRE:
	in 00		; chaque chiffre de l'entier est stock� dans al
	cmp al,0D	; la saisie se termine lorsque l'utilisateur appuie sur entr�e
	jz FINNOMBRE
	and al,0F	; permet de convertir le caract�re ASCII du chiffre en entier
	mul bl,A
	add bl,al
	jmp NOMBRE

FINNOMBRE:
	push bl
	push dl
	ret

;&&&&&&&&&&&&&&&&&&&&&&&& d�but du calcul du pgcd &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&;

CALCULPGCD:
	org 54
	pop dl			; stock l'adresse du retour du programme appelant
	pop al
	pop bl 			; les 2 nombres seront stock�s dans al et bl
PGCD:
	cmp al,bl		; tant que al et bl sont diff�rent, on a pas trouver le pgcd
	jz FINPGCD
	js INF
	sub al,bl		; si bl>al alors on fait al = al - bl
	jmp FIN

	INF:
		sub bl,al 	; sinon on fait bl = bl - al
	
	FIN:
	jmp PGCD

FINPGCD:
	push al
	push dl
	ret

;&&&&&&&&&&&&&&&&&& d�but de l'affichage de l'entier &&&&&&&&&&&&&&&&&&&&&;

AFFICHERNOMBRE:
	org 74
	pop dl		; stock l'adresse du retour du programme appelant
	pop al
	push al
	pop bl		; al et bl contiennent l'entier
	mov cl,0
	push cl		; met un identifiant de fin de cha�ne dans la pile
	mov cl,C0	; positionne cl afin d'afficher l'entier � l'�cran


STRING:
	mod al,A	; al servira � d�composer l'entier par chiffres
	cmp al,bl
	jz FINSTRING
	add al,30
	push al
	sub al,30
	sub bl,al
	div bl,A	; bl gardera en m�moire l'entier avec son dernier chiffre en moins
	push bl
	pop al
	jmp STRING

FINSTRING:
	add al,30
	push al

AFFICHAGE:	
	pop al		; al contiendra chaque chiffre � afficher
	cmp al,00	; on affiche chaque caract�res diff�rent de l'identifiant de fin de cha�ne
	jz FINAFFICHAGE
	mov [cl],al
	inc cl
	jmp AFFICHAGE

FINAFFICHAGE:
	push dl
	ret
	end