 	CLO
MAIN:
	CALL 20 	; Appel du sous programme de saisie  (entier saisie dans AL)
	CALL 60 	; Appel du sous programme d'affichage  (entier afficher dans AL)
; ----------------- Role : saisie d'un entier retourne dans AL------------------
SPROSAIDIE:
	ORG 20
	POP CL 		; sauvegarde retour dans CL
	MOV BL , 0
BOUCLE:
	IN 00 		; chiffre saisir dans AL
	CMP AL , 0D 	; est ce que c'est entre
	JZ FIN		; Si AL  = entr�
	AND AL , 0F	; AL = AL AND 0F convertion ASCII
	MUL BL , 0A	; nombe = nombre * 10
	ADD BL,AL 	; nombre = nombre + entier saisie
	JMP BOUCLE
FIN: 
	PUSH BL
	PUSH CL 	; retauration de l'adresse de retour dans AL 
	RET
; ----------------- Role : Affiche l'entier dans AL-----------------------------
	ORG 60
	POP DL 		;adresse de retour
	POP AL 		; parametre
	MOV BL ,00
	PUSH BL
DECOMP:
	PUSH AL 	; AL -> BL
	POP AL 		; equivalent a MOV BL , AL (inexistant)
	MOD BL , 0A 	; on recupere le reste dans BL
	OR BL , 30	; transformation reste entier en caractere ASCII
	PUSH BL 	; on empile le reste (code ASCII)
	DIV AL , 0A	; obtenir le quotient qui sera nouveau nombre a d�composer
	CMP AL , 00 	; AL = 0? quotient null
	JNZ DECOMP	; si AL dif de 0 on reboucle sinon on vas afficher
	MOV CL , C0 	; adresse du terminal virtuel
BOUCLE: 
	POP AL		; on d�pile u reste
	CMP AL, 00	; AL = 0?
	JZ FIND
	MOV [CL] , AL 	; on affiche
	INC CL		; pour ce deplacer sur le terminal
	JMP BOUCLE
FIND:	
	PUSH DL 	restaure l'adresse de retour
	RET
	END