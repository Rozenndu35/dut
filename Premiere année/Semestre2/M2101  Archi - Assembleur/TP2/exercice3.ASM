	CLO
	MOV AL, [64]		; premier nombre
	MOV BL , [65]		; deuxieme nombre

Tantque:			; tant que AL est different de BL
	CMP AL,BL
	JZ Fin			
	JNS Sinon
	SUB BL, AL
	JMP Tantque
Sinon:				; BL est inferieur a AL
	SUB AL, BL
	JMP Tantque
Fin:
	MOV [66] , BL		; retourne le pgcd
	
	ORG 64
	DB 15
	DB 9
	END
