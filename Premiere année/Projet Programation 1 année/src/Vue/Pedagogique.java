package Vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


import Controleur.Debut.BoutonLabyrinthe;
import Controleur.Debut.ConsigneBoutonListener;
import ModeleInterface.Mode;

public class Pedagogique extends JPanel{
	private JPanel milieu;
	private JPanel pan;
	private JPanel pa;
	private JPanel p;
	private BufferedImage buffImage;
	private Image image;
	private JButton laby;
	private JButton maths;
	private JButton consigne;
	private JComboBox<Mode> mode;
	private JComboBox<Integer> niveau;
	private JComboBox<Integer> niveau2;
	private JLabel labelImage;
	private JLabel m;
	private JLabel n;
	private JLabel n1;
	private JLabel nomm;
	private JTextField nom;

	public Pedagogique(Fenetre f){
		//declaration des composants
		laby = new JButton("Labyrinthe");
		maths = new JButton("Mathématiques");
		consigne = new JButton("Consigne");
		mode = new JComboBox<Mode>();
		niveau = new JComboBox<Integer>();
		niveau2 = new JComboBox<Integer>();
		milieu = new JPanel();
		pan = new JPanel();
		pa = new JPanel();
		p = new JPanel();
		m = new JLabel("Mode : ");
		n = new JLabel("Niveau : ");
		n1 = new JLabel("Niveau : ");
		nomm = new JLabel("Nom : ");
		nom = new JTextField();
		
		//initialisation des composants
		buffImage = Accueil.LectureImage("./donnee/Fond.jpg");
		image = Toolkit.getDefaultToolkit().createImage(buffImage.getSource());
		image = image.getScaledInstance(1200, 720, Image.SCALE_DEFAULT);
		labelImage = new JLabel(new ImageIcon(image));		
        labelImage.setBounds( 0, 0, 1200, 720);
        laby.setBounds(275, 150, 250, 80);
        maths.setBounds(275, 250, 250, 80);
        consigne.setBounds(275, 350, 250, 80);
        mode.setBounds(800, 150, 100, 80);
        pan.setBounds(575, 150, 200, 80);
        pa.setBounds(575, 350, 200, 80);
        p.setBounds(825, 150, 200, 80);
        Color c = new Color(255,244,235); //beige couleur bois      
        laby.setBackground(c);
        maths.setBackground(c);
        consigne.setBackground(c);
        mode.setBackground(c);
        niveau.setBackground(c);
        niveau2.setBackground(c);
        pan.setBackground(c);
        pa.setBackground(c);
        p.setBackground(c);
        Color col = new Color (139,69,19); //marron
        laby.setBorder(BorderFactory.createLineBorder(col,2));
        maths.setBorder(BorderFactory.createLineBorder(col,2));
        consigne.setBorder(BorderFactory.createLineBorder(col,2));
        pan.setBorder(BorderFactory.createLineBorder(col,2));
        pa.setBorder(BorderFactory.createLineBorder(col,2));
        p.setBorder(BorderFactory.createLineBorder(col,2));
        Mode[] list = new Mode[]{Mode.LAB, Mode.LABETOILE, Mode.LABBOMBE , Mode.LABTOUT};
        Integer[] listn =new Integer[] {1, 2 , 3, 4, 5, 6};
        Integer[] listn2 =new Integer[] {1, 2};
        this.mode= new JComboBox<Mode>(list);
        this.niveau =  new JComboBox<Integer>(listn);
        this.niveau2 =  new JComboBox<Integer>(listn2);
        //disposition panel pan
      	pan.setLayout(new GridLayout(0,2));
      	pan.add(m);
      	pan.add(mode);
      	
      	pa.setLayout(new GridLayout(0,2));
      	pa.add(n1);
      	pa.add(niveau2);
      	
      	p.setLayout(new GridLayout(0,2));
      	p.add(n);
      	p.add(niveau);
        
		//disposition panel milieu
		milieu.setLayout(null);
		milieu.add(pan);
		milieu.add(pa);
		milieu.add(p);
		milieu.add(laby);	
		milieu.add(maths);
		milieu.add(consigne);
		milieu.add(labelImage);
		
		//disposition sur la fenetre
		
		this.setLayout(new BorderLayout());
		this.add(milieu);
		
		//Action
		this.laby.addActionListener(new BoutonLabyrinthe(f,this));
		this.consigne.addActionListener(new ConsigneBoutonListener(f, this));
	}

	public JComboBox<Mode> getMode() {
		return (JComboBox<Mode>) this.mode;
	}
	public JComboBox<Integer> getNiveau() {
		return (JComboBox<Integer>) this.niveau;
	}

	public String getNom() {
		return this.nom.getText();
	}

	public JComboBox<Integer>  getNiveauConsi() {
		return (JComboBox<Integer>) this.niveau2;
	}
}
