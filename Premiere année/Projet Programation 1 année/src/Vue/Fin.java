/**
 * classe pour la fenetre de la fin du jeux
 * @author rozenn/Klervia
 * @version1
 */

package Vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Controleur.Fin.BoutonRejouer;
import Controleur.Jeux.QuiterDef;

public class Fin extends JPanel {

	private JLabel textGagnant;
	private JButton rejouer;
	private JButton quitter;
	private JPanel p;
	private JPanel milieu;
	private JLabel labelImage;
	private Image image;
	private BufferedImage buffImage;
	private Fenetre fenetre;
	
	/**
	 * le constructeur de la classe fin
	 */
	public Fin(Fenetre f, String joueurGagnant) {
		this.fenetre = f;
		Color c = new Color(255,244,235); //beige couleur bois
		Color col = new Color (139,69,19);
		
		buffImage = Accueil.LectureImage("./donnee/Fond.jpg");
		image = Toolkit.getDefaultToolkit().createImage(buffImage.getSource());
		image = image.getScaledInstance(1200, 720, Image.SCALE_DEFAULT);
		labelImage = new JLabel(new ImageIcon(image));		
        labelImage.setBounds( 0, 0, 1200, 720);
		p = new JPanel();
		p.setBackground(c);
		p.setBorder(BorderFactory.createLineBorder(col,2));
		milieu = new JPanel();
		//recuperer le vainqueur
		textGagnant = new JLabel("Le vainqueur est : " + joueurGagnant);
		p.setBounds(500, 300, 200, 80);
		rejouer = new JButton("Rejouer");
		rejouer.setBackground(c);
		rejouer.setBorder(BorderFactory.createLineBorder(col,1));
		quitter = new JButton("Quitter");
		quitter.setBackground(c);
		quitter.setBorder(BorderFactory.createLineBorder(col,1));
		p.setLayout(new FlowLayout());
		p.add(textGagnant);
		p.add(rejouer);p.add(quitter);
		
		milieu.setLayout(null);
		milieu.add(p);
		milieu.add(labelImage);
		
		this.setLayout(new BorderLayout());
		this.add(milieu);
		
		//action
		this.quitter.addActionListener(new QuiterDef(this.fenetre, this.fenetre) );
		this.rejouer.addActionListener(new BoutonRejouer(this.fenetre) );
	}

	
}