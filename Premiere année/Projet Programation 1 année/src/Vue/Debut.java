/**
 * classe pour la fenetre de debut de partie 
 * @author rozenn/Klervia
 * @version1
 */

package Vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import Controleur.Debut.BoutonRadioChoixMode;
import Controleur.Debut.BoutonValider;
import ModeleInterface.Difficulte;
import ModeleInterface.Mode;

public class Debut extends JPanel {

	public JRadioButton hJoueur2;
	private JRadioButton hJoueur3;
	private JRadioButton hJoueur4;
	private JRadioButton oJoueur2;
	private JRadioButton oJoueur3;
	private JRadioButton oJoueur4;
	private JRadioButton nJoueur3;
	private JRadioButton nJoueur4;
	private JComboBox<?> dOrdi2;
	private JComboBox<?> dOrdi3;
	private JComboBox<?> dOrdi4;
	private JButton valider;
	private JLabel entete;
	private JLabel j1;
	private JLabel j2;
	private JLabel j3;
	private JLabel j4;
	private JPanel j1p;
	private JPanel j2p;
	private JPanel j3p;
	private JPanel j4p;
	private JPanel p1;
	private JPanel p2;
	private JPanel p3;
	private JPanel p4;
	private JPanel milieu;
	private JPanel groupe;
	private JPanel pan1;
	private JPanel pan2;
	private JPanel pan3;
	private JPanel pan4;
	private JPanel pv;
	private JTextField nom1;
	private JTextField nom2;
	private JTextField nom3;
	private JTextField nom4;
	private BufferedImage buffImage;
	private Image image;
	private JLabel labelImage;
	private BufferedImage buffImage2;
	private Image image2;
	private JLabel labelImage2;
	private Fenetre laF;

	/**
	 * le constructeur de la classe
	 */
	public Debut(Fenetre f) {
		this.laF = f;
		//initialisation des composants
		valider = new JButton("Valider");
		j1p = new JPanel();
		j2p = new JPanel();
		j3p = new JPanel();
		j4p = new JPanel();
		milieu = new JPanel();
		groupe = new JPanel();
		p1 = new JPanel();
		p2 = new JPanel();
		p3 = new JPanel();
		p4 = new JPanel();
		pan1 = new JPanel();
		pan2 = new JPanel();
		pan3 = new JPanel();
		pan4 = new JPanel();
		pv = new JPanel();
		j1 = new JLabel ("Joueur 1 : humain");
		j2 = new JLabel ("Joueur 2 : ");
		j3 = new JLabel ("Joueur 3 : ");
		j4 = new JLabel ("Joueur 4 : ");
		entete = new JLabel("Bienvenue dans Quoridor veuillez choisir les parametres de jeux");
		nom1 = new JTextField();
		nom2= new JTextField();
		nom3= new JTextField();
		nom4= new JTextField();
		hJoueur2 = new JRadioButton("Humain", false);
		hJoueur3 = new JRadioButton("Humain", false);
		hJoueur4 = new JRadioButton("Humain", false);
		hJoueur4.setEnabled(false);
		oJoueur2 = new JRadioButton("Ordinateur",true);
		oJoueur3 = new JRadioButton("Ordinateur", false);
		oJoueur4 = new JRadioButton("Ordinateur", false);
		oJoueur4.setEnabled(false);
		nJoueur3 = new JRadioButton("Aucun", true);
		nJoueur4 = new JRadioButton("Aucun", true);
		nJoueur4.setEnabled(false);
		ButtonGroup radioButtonGroup2 = new ButtonGroup();
		ButtonGroup radioButtonGroup3 = new ButtonGroup();
		ButtonGroup radioButtonGroup4 = new ButtonGroup();
		dOrdi2 = new JComboBox<Object>();
		dOrdi3 = new JComboBox<Object>();
		dOrdi4 = new JComboBox<Object>();
		
		nom1.setBounds(0, 40, 80, 30);
		nom2.setBounds(0, 40, 80, 30);
		nom3.setBounds(0, 40, 80, 30);
		nom4.setBounds(0, 40, 80, 30);
		buffImage = Accueil.LectureImage("./donnee/Fond.jpg");
		image = Toolkit.getDefaultToolkit().createImage(buffImage.getSource());
		image = image.getScaledInstance(1200, 720, Image.SCALE_DEFAULT);
		labelImage = new JLabel(new ImageIcon(image));		
        labelImage.setBounds( 0, 0, 1200, 720);
        
        buffImage2 = Accueil.LectureImage("./donnee/Option.png");
        image2 = Toolkit.getDefaultToolkit().createImage(buffImage2.getSource());
		image2 = image2.getScaledInstance(80, 80, Image.SCALE_DEFAULT);
		labelImage2 = new JLabel(new ImageIcon(image2));		
        labelImage2.setBounds( 880, 100, 80, 80);
        
        valider.setBounds(1050,600,100,30);
        groupe.setBounds(180, 80, 820,500);
        Color c = new Color(255,244,235); //beige couleur bois 
        valider.setBackground(c);
        groupe.setBackground(c);
        p1.setBackground(c);
        p2.setBackground(c);
        p3.setBackground(c);
        p4.setBackground(c);
        j1p.setBackground(c);
        j2p.setBackground(c);
        j3p.setBackground(c);
        j4p.setBackground(c);
        pan1.setBackground(c);
        pan2.setBackground(c);
        pan3.setBackground(c);
        pan4.setBackground(c);
        hJoueur2.setBackground(c);
        hJoueur3.setBackground(c);
        hJoueur4.setBackground(c);
        oJoueur2.setBackground(c);
        oJoueur3.setBackground(c);
        oJoueur4.setBackground(c);
        nJoueur3.setBackground(c);
        nJoueur4.setBackground(c);
        dOrdi2.setBackground(c);
        nom1.setBackground(c);
        nom2.setBackground(c);
        nom3.setBackground(c);
        nom4.setBackground(c);
        pv.setBackground(c);
        Color col = new Color (139,69,19); //marron
        valider.setBorder(BorderFactory.createLineBorder(col,2));
        groupe.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(col,2), BorderFactory.createLineBorder(c,12)));
        
		//disposition des boutons radio
		radioButtonGroup2.add(this.hJoueur2);
		radioButtonGroup2.add(this.oJoueur2);

		radioButtonGroup3.add(this.hJoueur3);
		radioButtonGroup3.add(this.oJoueur3);
		radioButtonGroup3.add(this.nJoueur3);
		
		radioButtonGroup4.add(this.hJoueur4);
		radioButtonGroup4.add(this.oJoueur4);
		radioButtonGroup4.add(this.nJoueur4);
		
		// disposition des joueurs
		
		pan1.setLayout(null);
		pan1.add(nom1);
		
		pan2.setLayout(null);
		pan2.add(nom2);
		
		pan3.setLayout(null);
		pan3.add(nom3);
		
		pan4.setLayout(null);
		pan4.add(nom4);
		
		
		j1p.setLayout(new GridLayout(0,2));
		j1p.add(new JLabel("Nom : "));
		j1p.add(pan1);
		
		j2p.setLayout(new BorderLayout());
		j2p.add(new JLabel("Difficulte : "), java.awt.BorderLayout.WEST);
		j2p.add(this.dOrdi2 , java.awt.BorderLayout.EAST);
		
		//disposition des groupes
		
		p1.setLayout(new GridLayout(0,3));
		p1.add(j1);
		p1.add(j1p);
		
		p2.setLayout(new GridLayout(0,5));
		p2.add(j2);
		p2.add(this.hJoueur2);
		p2.add(this.oJoueur2);
		p2.add(this.pv);
		p2.add(this.j2p);
		
		p3.setLayout(new GridLayout(0,5));
		p3.add(this.j3); 
		p3.add(this.hJoueur3); 
		p3.add(this.oJoueur3);
		p3.add(this.nJoueur3);
		p3.add(this.j3p);
		
		p4.setLayout(new GridLayout(0,5));
		p4.add(this.j4);
		p4.add(this.hJoueur4);
		p4.add(this.oJoueur4);
		p4.add(this.nJoueur4);
		p4.add(this.j4p);
		
		groupe.setLayout(new GridLayout(5,0));
		groupe.add(this.entete);
		groupe.add(p1);
		groupe.add(p2);
		groupe.add(p3);
		groupe.add(p4);
		
		//disposition panel milieu
		milieu.setLayout(null);
		milieu.add(labelImage2);
		milieu.add(groupe);
		milieu.add(valider);
		milieu.add(labelImage);
		
		//diposition sur la fenetre
		this.setLayout(new BorderLayout());
		this.add(milieu);
		
		
		// action
		hJoueur2.addActionListener(new BoutonRadioChoixMode(this));
		oJoueur2.addActionListener(new BoutonRadioChoixMode(this));
	
		hJoueur3.addActionListener(new BoutonRadioChoixMode(this));
		oJoueur3.addActionListener(new BoutonRadioChoixMode(this));
		nJoueur3.addActionListener(new BoutonRadioChoixMode(this));
		
		hJoueur4.addActionListener(new BoutonRadioChoixMode(this));
		oJoueur4.addActionListener(new BoutonRadioChoixMode(this));
		
		valider.addActionListener(new BoutonValider(laF, this));

	}
	
	public void setPanelJoueur2(boolean humain) {
		this.j2p.removeAll();
		if (humain) {
			this.j2p.setLayout(new GridLayout(0,2));
			this.j2p.add(new JLabel("Nom : "));
			this.j2p.add(this.pan2);
			
		}
		else {
			this.dOrdi2 = new JComboBox<Object>();
			this.j2p.setLayout(new BorderLayout());
			this.j2p.add(new JLabel("Difficulte : "), java.awt.BorderLayout.WEST);
			this.j2p.add(this.dOrdi2 , java.awt.BorderLayout.EAST);
		}
		this.j2p.updateUI();
	}
	
	public void setPanelJoueur3(String personne) {
		this.j3p.removeAll();
		if (personne.equals("humain")) {
			this.j3p.setLayout(new GridLayout(0,2));
			this.j3p.add(new JLabel("Nom : "));
			this.j3p.add(this.pan3);
		}
		else if (personne.equals("ordinateur")){
			this.dOrdi2 = new JComboBox<Object>();
			this.j3p.setLayout(new BorderLayout());
			this.j3p.add(new JLabel("Difficulte : "), java.awt.BorderLayout.WEST);
			this.j3p.add(this.dOrdi2 , java.awt.BorderLayout.EAST);
			Color c = new Color(255,244,235);
			dOrdi3.setBackground(c);
		       
		}
		else if (personne.equals("aucun")) {
			
		}
		this.j3p.updateUI();
	}
	
	public void setPanelJoueur4(boolean humain) {
		this.j4p.removeAll();
		if (humain) {
			this.j4p.setLayout(new GridLayout(0,2));
			this.j4p.add(new JLabel("Nom : "));
			this.j4p.add(this.pan4);
			
		}
		else if (!humain){
			this.dOrdi2 = new JComboBox<Object>();
			this.j4p.setLayout(new BorderLayout());
			this.j4p.add(new JLabel("Difficulte : "), java.awt.BorderLayout.WEST);
			this.j4p.add(this.dOrdi2 , java.awt.BorderLayout.EAST);
			Color c = new Color(255,244,235);
			dOrdi4.setBackground(c);
		}
		this.j4p.repaint();
	}
	
	public JRadioButton gethJoueur2() {
		return this.hJoueur2;
	}
	
	public JRadioButton gethJoueur3() {
		return this.hJoueur3;
	}
	
	public JRadioButton gethJoueur4() {
		return this.hJoueur4;
	}
	
	public JRadioButton getoJoueur2() {
		return this.oJoueur2;
	}
	public JRadioButton getoJoueur3() {
		return this.oJoueur3;
	}
	public JRadioButton getoJoueur4() {
		return this.oJoueur4;
	}
	
	public JRadioButton getnJoueur3() {
		return this.nJoueur3;
	}
	
	public JRadioButton getnJoueur4() {
		return this.nJoueur4;
	}
	
	public JPanel getj4p() {
		return this.j4p;
	}
	
	//_______________________________________________________________________Getter des informations
	
	public Mode getMode() {
		Mode ret = Mode.HO;
		if (nJoueur3.isSelected() &&  hJoueur2.isSelected()) {
			ret = Mode.HH;
		}
		if (oJoueur3.isSelected() &&  oJoueur2.isSelected() && oJoueur4.isSelected() ) {
			ret = Mode.HOOO;
		}
		if (oJoueur3.isSelected() &&  hJoueur2.isSelected() && oJoueur4.isSelected() ) {
			ret = Mode.HHOO;
		}
		if (hJoueur3.isSelected() &&  oJoueur2.isSelected() && oJoueur4.isSelected() ) {
			ret = Mode.HOHO;
		}
		if (oJoueur3.isSelected() &&  oJoueur2.isSelected() && hJoueur4.isSelected() ) {
			ret = Mode.HOOH;
		}
		if (hJoueur3.isSelected() &&  hJoueur2.isSelected() && oJoueur4.isSelected() ) {
			ret = Mode.HHHO;
		}
		if (hJoueur3.isSelected() &&  oJoueur2.isSelected() && hJoueur4.isSelected() ) {
			ret = Mode.HOHH;
		}
		if (oJoueur3.isSelected() &&  hJoueur2.isSelected() && hJoueur4.isSelected() ) {
			ret = Mode.HHOH;
		}
		if (hJoueur3.isSelected() &&  hJoueur2.isSelected() && hJoueur4.isSelected() ) {
			ret = Mode.HHHH;
		}
		return ret;
	}
	public String getNom1() {
		return nom1.getText();
	}
	public String getNom2() {
		String nom = null; 
		if (hJoueur2.isSelected()) {
			nom = nom2.getText();
		}
		return nom;
	}
	public String getNom3() {
		String nom = null; 
		if (hJoueur3.isSelected()) {
			nom = nom3.getText();
		}
		return nom;
	}
	public String getNom4() {
		String nom = null; 
		if (hJoueur4.isSelected()) {
			nom = nom4.getText();
		}
		return nom;
	}
	public Difficulte getDif2() {
		Difficulte dif = null; 
		if (oJoueur2.isSelected()) {
			dif = (Difficulte) dOrdi2.getSelectedItem();
		}
		return dif;
	}
	public Difficulte getDif3() {
		Difficulte dif = null; 
		if (oJoueur3.isSelected()) {
			dif = (Difficulte) dOrdi3.getSelectedItem();
		}
		return dif;
	}
	public Difficulte getDif4() {
		Difficulte dif = null; 
		if (oJoueur4.isSelected()) {
			dif = (Difficulte) dOrdi4.getSelectedItem();
		}
		return dif;
	}
}