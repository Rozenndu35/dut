/**
 * classe pour la fenetre de jeu
 * @author rozenn/Klervia
 * @version1
 */

package Vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTextField;

import Controleur.Jeux.BoutonQuitter;
import Controleur.Jeux.GrilleControleurLab;
import Controleur.Option.RegleButtonListener;
import ModeleInterface.Cases;
import ModeleInterface.Couleur;
import ModeleInterface.Difficulte;
import ModeleInterface.GrilleModel;
import ModeleInterface.Mode;
import ModeleInterface.Partie;
import ModeleInterface.Plateau;

public class JeuxLab extends JPanel {
	
	private JLabel pion;
	private JLabel pionCouleur;
	private JButton valider;
	private JButton quitter;
	private JButton droite;
	private JButton gauche;
	private JButton bas;
	private JButton haut;
	private JButton diagHautDroit;
	private JButton diagHautGauche;
	private JButton diagBasDroit;
	private JButton diagBasGauche;
	private JButton sautD;
	private JButton sautB;
	private JButton sautG;
	private JButton sautH;
	private JRadioButton horizontal;
	private JRadioButton vertical;
	private JTextField xBarriere;
	private JTextField yBarriere;
	private BufferedImage buffImage;
	private Image image;
	private JLabel labelImage;
	private Fenetre fenetre;
	public Partie part;
	private JLabel nbBarriereLabel;
	private ButtonGroup sens;
	private long tempsDebut;
	/**
	 * le constructeur
	 */
	public JeuxLab(Mode mode, int niveau , String nom1 , Fenetre f) {
		this.fenetre = f;
		
		part = new Partie(mode , niveau,nom1 );
		this.tempsDebut = System.currentTimeMillis();
		
		JPanel tableau = new JPanel();
		JPanel milieu = new JPanel();
		JPanel b = new JPanel();
		Color c = new Color(255,244,235); //beige couleur bois
		Color col = new Color (139,69,19);
		
		quitter = new JButton("Quitter");
        quitter.setBackground(c);
		quitter.setBorder(BorderFactory.createLineBorder(col,1));
		quitter.setBounds(1050,600,100,30);
		
		buffImage = Accueil.LectureImage("./donnee/Fond.jpg");
		image = Toolkit.getDefaultToolkit().createImage(buffImage.getSource());
		image = image.getScaledInstance(1200, 720, Image.SCALE_DEFAULT);
		labelImage = new JLabel(new ImageIcon(image));		
        labelImage.setBounds( 0, 0, 1200, 720);
				
		GrilleModel model = new GrilleModel(this.fenetre, part.plateau.getPlateauJeux());
	    JTable tab = new JTable(model);
	    
	    tab.setBounds(50, 40, 570, 570);
	    tab.setFillsViewportHeight(true);
	    tab.setRowHeight(tab.getWidth()/tab.getColumnCount());
	    tab.setShowGrid(true);
	    tab.setBackground(c);
	    tab.setEnabled(false);
	    tableau.add(tab);
	    
		milieu.setLayout(null);
		milieu.add(quitter);
		milieu.add(tab);
		milieu.add(new JLabel("Plateau"),BorderLayout.WEST );
		milieu.add(labelImage);
		
		this.setLayout(new BorderLayout());
		this.add(milieu);
		
		//action
		quitter.addActionListener(new BoutonQuitter(this.fenetre));
		tab.addMouseListener(new  GrilleControleurLab(tab,this,model,this.fenetre ));
		
	}
	
	/**
	 * retourne le temps
	 * @return le tezmps
	*/
	public double temps() {
		long tempsFin = System.currentTimeMillis();
		return  (tempsFin - this.tempsDebut) / 1000F;
	}

}
	