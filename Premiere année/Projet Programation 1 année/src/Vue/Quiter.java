package Vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.text.JTextComponent;

import Controleur.Jeux.QuiterDef;
import Controleur.Option.OkRegleButtonListener;

public class Quiter extends JFrame {

	private JLabel text;
	private JButton quiter;
	private JButton sauvegarder;
	private Fenetre fenetre;
	/**
	 * le constructeur
	 */
	public Quiter(Fenetre f){
		//initialisation des composants
		this.fenetre = f;
		this.text = new JLabel("Voulez-vous... ?");
		this.quiter = new JButton("Quitter");
		this.sauvegarder = new JButton("Sauvegarder");
		Color col = new Color (139,69,19); //marron
		this.quiter.setBorder(BorderFactory.createLineBorder(col,2));
		this.sauvegarder.setBorder(BorderFactory.createLineBorder(col,2));
		text.setFont(new Font("Arial",Font.BOLD,13));
		Color c = new Color(255,244,235); //beige couleur bois
		this.quiter.setBackground(c);
		this.sauvegarder.setBackground(c);
		this.text.setBackground(c);
		text.setBounds(90, 50, 120, 50);
		quiter.setBounds(90, 250, 120, 50);
		sauvegarder.setBounds(90, 150, 120, 50);
		//Disposition des composants
		JPanel pan = new JPanel();
		pan.setBackground(c);
		//pan.setBounds(80, 20, 140, 110);
		pan.setLayout(null);
		pan.add(text);
		pan.add(quiter);
	    pan.add(sauvegarder);
	  
	       
		 this.setLayout(new BorderLayout());
	     this.add(pan);
		//Actions
		this.quiter.addActionListener(new QuiterDef(this.fenetre, this) );   
		//affichage de la fenetre
		this.setTitle("Quitter");
		this.setVisible(true);
		int longueur=300;
		int largeur=400;
		this.setSize(longueur, largeur);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
}
	
}
