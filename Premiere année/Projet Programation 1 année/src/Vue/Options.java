/**
 * classe pour la fenetre des options
 * @author rozenn/Klervia
 * @version1
 */
package Vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import Controleur.Option.ValiderMusique;
import ModeleInterface.Difficulte;

public class Options extends JFrame{

	private JButton valider;
	private JRadioButton musiqueOui;
	private JRadioButton musiqueNon;
	private JComboBox<?> listMusique;
	private JLabel musique;
	private JLabel changerMusique;
	private BufferedImage buffImage;
	private Image image;
	private JLabel labelImage;
	private JPanel p;
	private Fenetre fenetre;
	private JPanel panelOp;
	
	/**
	 * le constructeur
	 */
	public Options(Fenetre f) {
		this.fenetre = f;
		//initialisation des composants
		musiqueOui = new JRadioButton("oui");
		musiqueNon = new JRadioButton("non");
		ButtonGroup radioButtonGroup = new ButtonGroup();
		radioButtonGroup.add(this.musiqueOui);
        radioButtonGroup.add(this.musiqueNon);
        String[] list = new String[] { "musique 1" , "musique 2" , "musique 3"};
		listMusique = new JComboBox<Object>(list);
		musique = new JLabel("Musique");
		changerMusique = new JLabel("Changer la musique");
		buffImage = Accueil.LectureImage("./donnee/Fond.jpg");
		image = Toolkit.getDefaultToolkit().createImage(buffImage.getSource());
		image = image.getScaledInstance(1200, 720, Image.SCALE_DEFAULT);
		labelImage = new JLabel(new ImageIcon(image));		
        labelImage.setBounds( 0, 0, 1200, 720);
        p = new JPanel();
        panelOp = new JPanel();
        panelOp.setBounds(175, 130, 300, 80);
        valider= new JButton("Valider");
        valider.setBounds(520,350,100,30);
        Color c = new Color(255,244,235); //beige couleur bois
        panelOp.setBackground(c);
        listMusique.setBackground(c);
        musiqueOui.setBackground(c);
        musiqueNon.setBackground(c);
        valider.setBackground(c);
        Color col = new Color (139,69,19);
        valider.setBorder(BorderFactory.createLineBorder(col,2));
        panelOp.setBorder(BorderFactory.createLineBorder(col,2));
        //Disposition des composants
        
        
        
        panelOp.setLayout(new FlowLayout());
        panelOp.add(musique) ; panelOp.add(musiqueOui);panelOp.add(musiqueNon);
        panelOp.add(changerMusique ) ; panelOp.add(listMusique);
        
        p.setLayout(null);
        p.add(valider);
        p.add(panelOp);
        p.add(labelImage);
        	// ______________________________________________________________n'affiche pas les information
        
        this.setLayout(new BorderLayout());
        this.add(p);
        //Actions
       this.valider.addActionListener(new ValiderMusique(this, fenetre));
        
      //affichage de la fenetre
        this.setTitle("Option");
  		this.setVisible(true);
  		int longueur=640;
  		int largeur=430;
  		this.setSize(longueur, largeur);
  		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
	}
	
	public boolean musiqueOui() {
		boolean ret = musiqueOui.isSelected();
		return ret;
	}
	public boolean musiqueNon() {
		boolean ret = musiqueNon.isSelected();
		return ret;
	}
	
	public String musique() {
		String ret = (String) listMusique.getSelectedItem();
		return ret;
	}

}