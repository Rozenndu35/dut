/**
 * classe pour la partie
 * @author rozenn/Klervia
 * @version1
 */

package ModeleInterface;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.*;

import Vue.Accueil;
import Vue.Fenetre;
import Vue.Jeux;

public class Partie implements Serializable{
	private Fenetre laFenetre;
	private Joueur joueur1;
	private Joueur joueur2;
	private Joueur joueur3;
	private Joueur joueur4;
	public ModeleInterface.Plateau plateau;
	private Mode mode;
	private Humain humain;
	private ArrayList<String> listeRecord;
	private int niveau;
	private int auTourDe;
	private String joueurGagnant;
	//_______________________________________________________________________________________
	/**
	 * Constructeur de la classe partie
	 * @param mode : le mode de jeux
	 */
	public Partie(Mode mode , String nom1 , String nom2 , String nom3, String nom4, Difficulte dif2 , Difficulte dif3 , Difficulte dif4, Fenetre f) {
		this.laFenetre = f;
		this.auTourDe=1;
		this.mode=mode;
		if (mode == Mode.HO ) {
			this.plateau = new Plateau(2);
			this.joueur1 =  new Humain(nom1, 1, 2, this.plateau);
			this.joueur2 =  new Ordinateur(dif2, 2, 2, "Ordinateur2", this.plateau );
		}
		else if (mode == Mode.HH ) {
			this.plateau = new Plateau(2);
			this.joueur1 =  new Humain(nom1, 1, 2, this.plateau);
			this.joueur2 =  new Humain(nom2, 2, 2, this.plateau);
		}
		else if (mode == Mode.HOOO ) {
			this.plateau = new Plateau(4);
			this.joueur1 =  new Humain(nom1, 1, 4, this.plateau);
			this.joueur2 =  new Ordinateur(dif2, 2, 4, "Ordinateur2", this.plateau );
			this.joueur3 =  new Ordinateur(dif3, 3, 4, "Ordinateur3" , this.plateau);
			this.joueur4 =  new Ordinateur(dif4, 4, 4, "Ordinateur4" ,this.plateau);
		}
		else if (mode == Mode.HHOO ) {
			this.plateau = new Plateau(4);
			this.joueur1 =  new Humain(nom1, 1, 4, this.plateau);
			this.joueur2 =  new Humain(nom2, 2, 4 , this.plateau);
			this.joueur3 =  new Ordinateur(dif3, 3, 4, "Ordinateur3" , this.plateau);
			this.joueur4 =  new Ordinateur(dif4, 4, 4, "Ordinateur4", this.plateau );
		}
		else if (mode == Mode.HHHO ) {
			this.plateau = new Plateau(4);
			this.joueur1 =  new Humain(nom1, 1, 4, this.plateau);
			this.joueur2 =  new Humain(nom2, 2, 4, this.plateau);
			this.joueur3 =  new Humain(nom3, 3, 4, this.plateau);
			this.joueur4 =  new Ordinateur(dif4, 4, 4, "Ordinateur4", this.plateau );
		}
		else if (mode == Mode.HHHH ) {
			this.plateau = new Plateau(4);
			this.joueur1 =  new Humain(nom1, 1, 4, this.plateau);
			this.joueur2 =  new Humain(nom2, 2, 4, this.plateau);
			this.joueur3 =  new Humain(nom3, 3, 4, this.plateau);
			this.joueur4 =  new Humain(nom4, 4, 4, this.plateau);
		}
		else if (mode == Mode.HOHO ) {
			this.plateau = new Plateau(4);
			this.joueur1 =  new Humain(nom1, 1, 4, this.plateau);
			this.joueur2 =  new Ordinateur(dif2, 2, 4, "Ordinateur2", this.plateau );
			this.joueur3 =  new Humain(nom3, 3, 4, this.plateau);
			this.joueur4 =  new Ordinateur(dif4, 4, 4, "Ordinateur4" ,this.plateau);
		}
		else if (mode == Mode.HOOH ) {
			this.plateau = new Plateau(4);
			this.joueur1 =  new Humain(nom1, 1, 4, this.plateau);
			this.joueur2 =  new Ordinateur(dif2, 2, 4, "Ordinateur2", this.plateau );
			this.joueur3 =  new Ordinateur(dif3, 3, 4, "Ordinateur3" , this.plateau);
			this.joueur4 =  new Humain(nom4, 4, 4, this.plateau);
		}
		else if (mode == Mode.HOHH ) {
			this.plateau = new Plateau(4);
			this.joueur1 =  new Humain(nom1, 1, 4, this.plateau);
			this.joueur2 =  new Ordinateur(dif2, 2, 4, "Ordinateur2", this.plateau );
			this.joueur3 =  new Humain(nom3, 3, 4, this.plateau);
			this.joueur4 =  new Humain(nom4, 4, 4, this.plateau);
		}
		else if (mode == Mode.HOHH ) {
			this.plateau = new Plateau(4);
			this.joueur1 =  new Humain(nom1, 1, 4, this.plateau);
			this.joueur2 =  new Humain(nom2, 2, 4, this.plateau);
			this.joueur3 =  new Ordinateur(dif3, 3, 4, "Ordinateur3" , this.plateau);
			this.joueur4 =  new Humain(nom4, 4, 4, this.plateau);
		}
	}
	/**
	 * Le constructeur pour les mode pedagogique
	 * @param mode : le mode de jeux pedagogique
	 * @param niveau : le niveau de la partie
	 */
	public Partie (Mode mode , int niveau , String nom) {
		this.niveau = niveau;
		int xPion = 0;
		int yPion = 0;
		if (mode == Mode.LAB) {
			labyrinthe lab = new labyrinthe(niveau);
			this.plateau = lab.getPlateau();
			if(niveau ==1) {
				xPion = 18;
				yPion = 5;
			}
			else if(niveau ==2) {
				xPion = 0;
				yPion = 2;
			}
			else if(niveau ==3) {
				xPion = 9;
				yPion = 18;
			}
			else if(niveau ==4) {
				xPion = 0;
				yPion = 8;
			}
			else if(niveau ==5) {
				xPion = 0;
				yPion = 0;
			}
			else if(niveau ==6) {
				xPion = 15;
				yPion = 0;
			}
			this.humain =  new Humain(nom, 1, 2, this.plateau);
			this.humain.monPion.setX(xPion);
			this.humain.monPion.setY(yPion);
		}
		if (mode == Mode.LABETOILE) {
			labyrinthe lab = new labyrinthe(niveau);
			this.plateau = lab.getPlateau();
			if(niveau ==1) {
				this.plateau.ajouterObjet(3 , 5 , Couleur.ETOILE);
				this.plateau.ajouterObjet(4 , 14 , Couleur.ETOILE);
				this.plateau.ajouterObjet(9 , 12 , Couleur.ETOILE);
				this.plateau.ajouterObjet(14 , 9 , Couleur.ETOILE);
				this.plateau.ajouterObjet(17 , 2 , Couleur.ETOILE);
				xPion = 18;
				yPion = 5;
			}
			else if(niveau ==2) {
				this.plateau.ajouterObjet(5 , 14 , Couleur.ETOILE);
				this.plateau.ajouterObjet(8 , 4 , Couleur.ETOILE);
				this.plateau.ajouterObjet(9 , 10 , Couleur.ETOILE);
				this.plateau.ajouterObjet(10 , 15 , Couleur.ETOILE);
				this.plateau.ajouterObjet(15 , 4 , Couleur.ETOILE);		
				xPion = 0;
				yPion = 2;
			}
			else if(niveau ==3) {
				this.plateau.ajouterObjet(5 , 13 , Couleur.ETOILE);
				this.plateau.ajouterObjet(7 , 6 , Couleur.ETOILE);
				this.plateau.ajouterObjet(12 , 4 , Couleur.ETOILE);
				this.plateau.ajouterObjet(14 , 3 , Couleur.ETOILE);
				this.plateau.ajouterObjet(15 , 15 , Couleur.ETOILE);
				xPion = 9;
				yPion = 18;
			}
			else if(niveau ==4) {
				this.plateau.ajouterObjet( 6 , 13 , Couleur.ETOILE);
				this.plateau.ajouterObjet(7 , 5 , Couleur.ETOILE);
				this.plateau.ajouterObjet(9 , 4 , Couleur.ETOILE);
				this.plateau.ajouterObjet(11 , 8 , Couleur.ETOILE);
				this.plateau.ajouterObjet(14 , 13 , Couleur.ETOILE);
				xPion = 0;
				yPion = 8;
			}
			else if(niveau ==5) {
				this.plateau.ajouterObjet(3 , 1 , Couleur.ETOILE);
				this.plateau.ajouterObjet(9 , 14 , Couleur.ETOILE);
				this.plateau.ajouterObjet(10 , 12 , Couleur.ETOILE);
				this.plateau.ajouterObjet(11 , 8 , Couleur.ETOILE);
				this.plateau.ajouterObjet(17 , 2 , Couleur.ETOILE);
				xPion = 0;
				yPion = 0;
			}
			else if(niveau ==6) {
				this.plateau.ajouterObjet(1 , 17 , Couleur.ETOILE);
				this.plateau.ajouterObjet(6 , 7 , Couleur.ETOILE);
				this.plateau.ajouterObjet(13 , 16 , Couleur.ETOILE);
				this.plateau.ajouterObjet(14 , 6 , Couleur.ETOILE);
				this.plateau.ajouterObjet(16 , 4 , Couleur.ETOILE);
				xPion = 15;
				yPion = 0;
			}
			this.humain =  new Humain(nom, 1, 2, this.plateau);
			this.humain.monPion.setX(xPion);
			this.humain.monPion.setY(yPion);
		}
		if (mode == Mode.LABBOMBE) {
			
			labyrinthe lab = new labyrinthe(niveau);
			this.plateau = lab.getPlateau();
			if(niveau ==1) {
				this.plateau.ajouterObjet(6 , 5 , Couleur.BOMBE);
				this.plateau.ajouterObjet(6 , 10 , Couleur.BOMBE);
				this.plateau.ajouterObjet(9 , 8 , Couleur.BOMBE);
				this.plateau.ajouterObjet(12 , 4 , Couleur.BOMBE);
				this.plateau.ajouterObjet(12, 13 , Couleur.BOMBE);
				xPion = 18;
				yPion = 5;
			}
			else if(niveau ==2) {	
				this.plateau.ajouterObjet(8 , 12 , Couleur.BOMBE);
				this.plateau.ajouterObjet(16 , 6 , Couleur.BOMBE);
				xPion = 0;
				yPion = 2;
			}
			else if(niveau ==3) {
				this.plateau.ajouterObjet(7 , 12 , Couleur.BOMBE);
				this.plateau.ajouterObjet(15 , 11 , Couleur.BOMBE);
				xPion = 9;
				yPion = 18;
			}
			else if(niveau ==4) {
				this.plateau.ajouterObjet(3 , 9 , Couleur.BOMBE);
				this.plateau.ajouterObjet(7 , 1 , Couleur.BOMBE);
				this.plateau.ajouterObjet(11 , 9 , Couleur.BOMBE);
				xPion = 0;
				yPion = 8;
			}
			else if(niveau ==5) {
				this.plateau.ajouterObjet(7 , 15 , Couleur.BOMBE);
				this.plateau.ajouterObjet(8 , 6 , Couleur.BOMBE);
				this.plateau.ajouterObjet(9 , 10 , Couleur.BOMBE);
				this.plateau.ajouterObjet(12, 10 , Couleur.BOMBE);
				this.plateau.ajouterObjet(17 , 16 , Couleur.BOMBE);
				xPion = 0;
				yPion = 0;
			}
			else if(niveau ==6) {
				this.plateau.ajouterObjet(5 , 11 , Couleur.BOMBE);
				this.plateau.ajouterObjet(5 , 17 , Couleur.BOMBE);
				this.plateau.ajouterObjet(7 , 10 , Couleur.BOMBE);
				this.plateau.ajouterObjet(15 , 8 , Couleur.BOMBE);
				this.plateau.ajouterObjet(16, 13 , Couleur.BOMBE);
				xPion = 15;
				yPion = 0;
			}
			this.humain =  new Humain(nom, 1, 2, this.plateau);
			this.humain.monPion.setX(xPion);
			this.humain.monPion.setY(yPion);
		}
		if (mode == Mode.LABTOUT) {
			labyrinthe lab = new labyrinthe(niveau);
			this.plateau = lab.getPlateau();
			if(niveau ==1) {
				this.plateau.ajouterObjet(3 , 5 , Couleur.ETOILE);
				this.plateau.ajouterObjet(4 , 14 , Couleur.ETOILE);
				this.plateau.ajouterObjet(9 , 12 , Couleur.ETOILE);
				this.plateau.ajouterObjet(14 , 9 , Couleur.ETOILE);
				this.plateau.ajouterObjet(17 , 2 , Couleur.ETOILE);
				this.plateau.ajouterObjet(6 , 5 , Couleur.BOMBE);
				this.plateau.ajouterObjet(6 , 10 , Couleur.BOMBE);
				this.plateau.ajouterObjet(9 , 8 , Couleur.BOMBE);
				this.plateau.ajouterObjet(12 , 4 , Couleur.BOMBE);
				this.plateau.ajouterObjet(12, 13 , Couleur.BOMBE);
				xPion = 18;
				yPion = 5;
			}
			else if(niveau ==2) {
				this.plateau.ajouterObjet(5 , 14 , Couleur.ETOILE);
				this.plateau.ajouterObjet(8 , 4 , Couleur.ETOILE);
				this.plateau.ajouterObjet(9 , 10 , Couleur.ETOILE);
				this.plateau.ajouterObjet(10 , 15 , Couleur.ETOILE);
				this.plateau.ajouterObjet(15 , 4 , Couleur.ETOILE);	
				this.plateau.ajouterObjet(8 , 12 , Couleur.BOMBE);
				this.plateau.ajouterObjet(15 , 11 , Couleur.BOMBE);
				xPion = 0;
				yPion = 2;
			}
			else if(niveau ==3) {
				this.plateau.ajouterObjet(5 , 13 , Couleur.ETOILE);
				this.plateau.ajouterObjet(7 , 6 , Couleur.ETOILE);
				this.plateau.ajouterObjet(12 , 4 , Couleur.ETOILE);
				this.plateau.ajouterObjet(14 , 3 , Couleur.ETOILE);
				this.plateau.ajouterObjet(15 , 15 , Couleur.ETOILE);
				this.plateau.ajouterObjet(7 , 12 , Couleur.BOMBE);
				this.plateau.ajouterObjet(15 , 11 , Couleur.BOMBE);
				xPion = 9;
				yPion = 18;
			}
			else if(niveau ==4) {
				this.plateau.ajouterObjet( 6 , 13 , Couleur.ETOILE);
				this.plateau.ajouterObjet(7 , 5 , Couleur.ETOILE);
				this.plateau.ajouterObjet(9 , 4 , Couleur.ETOILE);
				this.plateau.ajouterObjet(11 , 8 , Couleur.ETOILE);
				this.plateau.ajouterObjet(14 , 13 , Couleur.ETOILE);
				this.plateau.ajouterObjet(3 , 9 , Couleur.BOMBE);
				this.plateau.ajouterObjet(7 , 1 , Couleur.BOMBE);
				this.plateau.ajouterObjet(11 , 9 , Couleur.BOMBE);
				xPion = 0;
				yPion = 8;
			}
			else if(niveau ==5) {
				this.plateau.ajouterObjet(3 , 1 , Couleur.ETOILE);
				this.plateau.ajouterObjet(9 , 14 , Couleur.ETOILE);
				this.plateau.ajouterObjet(10 , 12 , Couleur.ETOILE);
				this.plateau.ajouterObjet(11 , 8 , Couleur.ETOILE);
				this.plateau.ajouterObjet(17 , 2 , Couleur.ETOILE);
				this.plateau.ajouterObjet(7 , 15 , Couleur.BOMBE);
				this.plateau.ajouterObjet(8 , 6 , Couleur.BOMBE);
				this.plateau.ajouterObjet(9 , 10 , Couleur.BOMBE);
				this.plateau.ajouterObjet(12, 10 , Couleur.BOMBE);
				this.plateau.ajouterObjet(17 , 16 , Couleur.BOMBE);
				xPion = 0;
				yPion = 0;
			}
			else if(niveau ==6) {
				this.plateau.ajouterObjet(1 , 17 , Couleur.ETOILE);
				this.plateau.ajouterObjet(6 , 7 , Couleur.ETOILE);
				this.plateau.ajouterObjet(13 , 16 , Couleur.ETOILE);
				this.plateau.ajouterObjet(14 , 6 , Couleur.ETOILE);
				this.plateau.ajouterObjet(16 , 4 , Couleur.ETOILE);
				this.plateau.ajouterObjet(5 , 11 , Couleur.BOMBE);
				this.plateau.ajouterObjet(5 , 17 , Couleur.BOMBE);
				this.plateau.ajouterObjet(7 , 10 , Couleur.BOMBE);
				this.plateau.ajouterObjet(15 , 8 , Couleur.BOMBE);
				this.plateau.ajouterObjet(16, 13 , Couleur.BOMBE);
				xPion = 15;
				yPion = 0;
				
			}
			this.humain =  new Humain(nom, 1, 2, this.plateau);
			this.humain.monPion.setX(xPion);
			this.humain.monPion.setY(yPion);
		}
		
	}
	
	
	//_______________________________________________________________________________________
	/**
	 * regarde si la partie est fini pour 2 joueur
	 * @return true si la parti est fini
	 */
	public boolean finAvecJ2() {
		boolean ret =false;
		for (int j= 0; j< 19 ; j++) {
			if ( plateau.getCases(17,j).getCouleur() == Couleur.ROUGE ) {
				ret = true;
				this.joueurGagnant = new String("Joueur 1 : "+joueur1.getNom());
			}
		}
		for (int j= 0; j< 19 ; j++) {
			if ( plateau.getCases(1,j).getCouleur() == Couleur.NOIR ) {
				ret = true;
				this.joueurGagnant = new String("Joueur 2 : "+joueur2.getNom());
			}
		}
		return ret;
	}
	/**
	 * regarde si la partie est fini pour 4 joueur
	 * @return true si la parti est fini
	 */
	public boolean finAvecJ4() {
		boolean ret =false;
		for (int j= 0; j< 19 ; j++) {
			if ( plateau.getCases(17,j).getCouleur() == Couleur.ROUGE ) {
				ret = true;
				this.joueurGagnant = new String("Joueur 1 : "+joueur1.getNom());
			}
		}
		for (int j= 0; j< 19 ; j++) {
			if ( plateau.getCases(1,j).getCouleur() == Couleur.NOIR ) {
				ret = true;
				this.joueurGagnant = new String("Joueur 2 : "+joueur2.getNom());
			}
		}
		for (int i= 0; i< 19 ; i++) {
			if ( plateau.getCases(i,17).getCouleur() == Couleur.BLEU ) {
				ret = true;
				this.joueurGagnant = new String("Joueur 3 : "+joueur3.getNom());
			}
		}
		for (int i= 0; i< 19 ; i++) {
			if ( plateau.getCases(i,1).getCouleur() == Couleur.VERT ) {
				ret = true;
				this.joueurGagnant = new String("Joueur 4 : "+joueur4.getNom());
			}
		}
		return ret;
	}
	//_______________________________________________________________________________________
	/**
	 * Methode qui permet d enregistrer une partie en cours
	 */
	public void enregistrerPartie() {
		try {
			FileOutputStream fos = new FileOutputStream("DonneesPartie");
			ObjectOutputStream oos = new ObjectOutputStream (fos);
			oos.writeObject(this);
			oos.close();
			fos.close();
		}
		catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	/**
	 * Methode qui permet de charger une partie precedemment enregistree
	 */
	public void chargerPartie() {
		Partie partie;
		try {
			FileInputStream fis = new FileInputStream("DonneesPartie");
			ObjectInputStream ois = new ObjectInputStream (fis);
			partie = (Partie)ois.readObject();
			ois.close();
			fis.close();
		}
		catch (IOException ioe) {
			ioe.printStackTrace();
			return;
		}
		catch (ClassNotFoundException c) {
			c.printStackTrace();
			return;
		}
		this.joueur1=partie.joueur1;
		this.joueur2=partie.joueur2;
		this.joueur3=partie.joueur3;
		this.joueur4=partie.joueur4;
	}
	
	/**
	 * lit le fichier des records
	 * @return la list des personne avec leur score
	 */
	public ArrayList<String> lireFichierRecord (String fichier){
		ArrayList<String> ret = new ArrayList<String>();
		try {
			Scanner in = new Scanner (new FileReader ("./donnee/"+fichier+".txt"));
			while (in.hasNextLine()) {
				ret.add(in.nextLine());
			}
			in.close();
		} 
		catch (FileNotFoundException e) {}
		return ret;
		
	}
	
	/**
	 * ecrit le fichier des record
	 */
	public void ecrireFichierRecord (String fichier) {
		try {
			PrintWriter out = new PrintWriter ("./donnee/"+fichier+".txt");
			for( String ligne : this.listeRecord) {
				out.println (ligne);
				out.close();
			}
		}
		catch (FileNotFoundException e) {
			System.out.println (e.getMessage());
		}
	}
	
	/**
	 * Creer la liste des record
	 */
	public String listeRecord(String nNom , Double nTemps, String fichier) {
		String resultat = "";
		this.listeRecord = lireFichierRecord (fichier);
		HashMap record = new HashMap<String , Double>();
		String[] classement = new String[3];
		classement[0]= "";
		classement[1]= "";
		classement[2]= "";
		boolean perdu = false;
		for (String e : this.listeRecord) {
			String nom = "";
			for ( int i =0 ; ((i<e.length()) && (Character.isDigit(e.charAt(i)) == false)) ; i++ ){
				nom += e.charAt(i);
			}
			nom = nom.trim();
			Double temps = (double) 0;
			double ret= 0;
			int indice = 0;
			for ( int i =0 ; ((i<e.length()) && (Character.isDigit(e.charAt(i)) == false)) ; i++ ){
				indice = i;
			}
			String chaine = e.substring (indice);
			chaine = chaine.trim();
			temps = Double.parseDouble(chaine);
			record.put(nom,temps);
			for (int i = 0; i<3 ; i++) {
				if (classement[i] != "") {
					if ((Double)record.get(classement[i])>(Double)record.get(nom)) {
						String rec = classement[i];
						classement[i] = nom;
						nom =rec;
					}
				}
				else {classement[i] = nom;}
			}
		}
		if (record.containsKey(nNom)) { 
			if ((Double)record.get(nNom)>nTemps) {
				record.put(nNom,nTemps);
				boolean dedans= false;
				int position = Arrays.binarySearch( classement, nNom );
				for (int i = 0; i<position ; i++) {
					if ((Double)record.get(classement[i])>nTemps) {
						String rec = classement[i];
						classement[i] = nNom;
						dedans = true;
						if (i == 0) {
							classement[2] = classement[1];
							classement[1] = classement[0];
						}
						else if (i == 1) {
							classement[1] = classement[0];
						}
					}
				}
			}
			else {
				perdu = true;
			}
		}
		else {
			record.put(nNom,nTemps);
			boolean dedans= false;
			boolean sort = false;
			for (int i = 0; i<3 && !sort; i++) {
				if (classement[i] != "") {
					if ((Double)record.get(classement[i])>nTemps) {
						String rec = classement[i];
						classement[i] = nNom;
						dedans = true;
						if (i == 0) {
							classement[2] = classement[1];
							classement[1] = classement[0];
						}
						else if (i == 1) {
							classement[1] = classement[0];
						}
					}
				}
				else {classement[i] = nNom;dedans = true; sort = true;}
			}
			if(!dedans){
				perdu = true;
			}
		}
		this.listeRecord = new ArrayList<String>();
		resultat = "Liste des records";
		resultat += "/n" + "1er : "+ classement[0] +" " +record.get(classement[0] )+ "s" ;
		this.listeRecord.add(classement[0] +" " + record.get(classement[0]));
		if (classement[1]!= "") {
			resultat += "/n" +"2eme : "+ classement[1] +" " +record.get(classement[1] ) + "s";
			this.listeRecord.add(classement[1] +" " + record.get(classement[1]));
			if (classement[2]!= "") {
				resultat += "/n" +"3eme : "+ classement[2] +" " +record.get(classement[2] ) + "s";
				this.listeRecord.add(classement[2] +" " + record.get(classement[2]));
			}
		}
		if (perdu) { resultat ="Desoler vous nete pas dans les meilleur avec " + nTemps + "s" ;}
		ecrireFichierRecord (fichier);
		return resultat;
	}
	//_______________________________________________________________________________________

	/**
	 * renvoie a qui de jouer
	 * @return le numero du joueur
	 */
		public int getAuTourDe() {
			return auTourDe;
		}
		/**
		 * modifie a qui de jouer
		 * @param auTourDe : le numero du joueur
		 */
		public void setAuTourDe(int auTourDe) {
			this.auTourDe = auTourDe;
		}
		/**
		 * renvoie a qui de jouer
		 * @return le nom du joueur
		 */
		public String getAuTourDeStr() {
			String ret = "";
			switch(this.auTourDe) {
			case 1:
				ret=this.joueur1.getNom();
				break;
			case 2:
				ret=this.joueur2.getNom();
				break;
			case 3:
				ret=this.joueur3.getNom();
				break; 
			case 4:
				ret=this.joueur4.getNom();
				break;
			}
		return ret;
		}
		/**
		 * renvoie a qui de jouer
		 * @return le  joueur
		 */
		public Humain getAuTourDeJoueur() {
			Humain ret = null;
			switch(this.auTourDe) {
			
			case 1:
				ret=(Humain) this.joueur1;
				break;
			case 2:
				if (mode.equals(Mode.HH)  || mode == Mode.HHOO || mode ==Mode.HHHO || mode ==Mode.HHHH ) {
					ret=(Humain) this.joueur2;
				}else {
					this.joueur2.choix();
				}
				
				break;
			case 3:
				
				if (mode == Mode.HHHO || mode == Mode.HOHH ||mode == Mode.HOHO ||mode == Mode.HHHH ) {
					ret=(Humain) this.joueur3;
				}else {
					this.joueur3.choix();
				}
				break; 
			case 4:
				if (mode == Mode.HHHH ||mode == Mode.HOOH ||mode == Mode.HOHH  || mode == Mode.HHOH  ) {
					ret=(Humain) this.joueur4;
				}else {
					this.joueur4.choix();
				}
				break;
			}
		return ret;
		}
		/**
		 * change le joueur de jouer
		 */
		public void auJoueurSuivant() {
			switch(this.plateau.getNbJ()) {
			case 2:
				switch(this.auTourDe) {
				case 1:
					this.auTourDe++;

					if(!finAvecJ2() && this.mode.equals(mode.HO)) {
						this.joueur2.choix();
						auTourDe=1;
					}
					break;
				case 2:
					this.auTourDe=1;
					break;
				};
				finAvecJ2();
				break;
			case 4:
				switch(this.auTourDe) {
				case 1:
					this.auTourDe++;
					if(!finAvecJ4() && this.mode.equals(mode.HOHH) || this.mode.equals(mode.HOOH) || this.mode.equals(mode.HOOO) || this.mode.equals(mode.HOHO)) {
						this.joueur2.choix();
						auTourDe=3;
						if(!finAvecJ4() && this.mode.equals(mode.HOOO) || this.mode.equals(mode.HOOH)) {
							this.joueur3.choix();
							auTourDe=4;
							if(!finAvecJ4() && this.mode.equals(mode.HOOO)) {
								this.joueur4.choix();
								finAvecJ4();
								auTourDe=1;
							}
						}
						
					}
					break;
				case 2:
					this.auTourDe++;
					if(!finAvecJ4() && this.mode.equals(mode.HHOO) || this.mode.equals(mode.HHOH)) {
						this.joueur3.choix();
						auTourDe=4;
						if(!finAvecJ4() && this.mode.equals(mode.HHOO)) {
							this.joueur4.choix();
							auTourDe=1;
						}
					}
					
					break;
				case 3:
					this.auTourDe++;
					if(!finAvecJ4() && this.mode.equals(mode.HOHO) || this.mode.equals(mode.HHHO)|| this.mode.equals(mode.HOHH)) {
						this.joueur4.choix();
						auTourDe=1;
					}
					break; 
				case 4:
					this.auTourDe=1;
					finAvecJ4();
					break;
				};
				finAvecJ4();
				break;
			};
			
		}
		/**
		 * retourne le nombre de barriere du joueur
		 * @return le nombre de barriere
		 */
		public int getBarriereDujoueur() {
			int ret=10;
			switch(this.auTourDe) {
			case 1:
				ret=this.joueur1.getNbBarriere();
				break;
			case 2:
				ret=this.joueur2.getNbBarriere();
				break;
			case 3:
				ret=this.joueur3.getNbBarriere();
				break; 
			case 4:
				ret=this.joueur4.getNbBarriere();
				break;
			}
			
			return ret;
		}
		
		/**
		 * retourne le jouer gagnant
		 * @return le jeur gagnant
		 */
		public String getJoueurGagnant() {
			return joueurGagnant;
		}
		
		/**
		 * retourne le joueur du labyrinthe
		 * @return le joueur
		 */
		public Humain getAuTourDeJoueurLab() {return this.humain;}
		
		/**
		 * retourne les resultat
		 * @return resultat
		 */
		public String getJoueurGagnantLab(double seconds) {
			String ret = "";
			if (this.humain.getgagnerLab()) {
				String etoile = "";
				for (int i = 0 ; i< this.humain.getEtoile() ; i++) {
					etoile += "★";
				}
				if (this.mode == Mode.LAB && this.niveau ==1) {
					ret = listeRecord(this.humain.getNom(),seconds, "recordL1");
				}
				if (this.mode == Mode.LAB && this.niveau ==2) {
					listeRecord(this.humain.getNom(),seconds, "recordL2");
				}
				if (this.mode == Mode.LAB && this.niveau ==3) {
					listeRecord(this.humain.getNom(),seconds, "recordL3");
				}
				if (this.mode == Mode.LAB && this.niveau ==4) {
					listeRecord(this.humain.getNom(),seconds, "recordL4");
				}
				if (this.mode == Mode.LAB && this.niveau ==5) {
					listeRecord(this.humain.getNom(),seconds , "recordL5");
				}
				if (this.mode == Mode.LAB && this.niveau ==6) {
					listeRecord(this.humain.getNom(),seconds, "recordL6");
				}
				if (this.mode == Mode.LABETOILE && this.niveau ==1) {
					listeRecord(this.humain.getNom(),seconds, "recordLE1");
				}
				if (this.mode == Mode.LABETOILE && this.niveau ==2) {
					listeRecord(this.humain.getNom(),seconds, "recordLE2");
				}
				if (this.mode == Mode.LABETOILE && this.niveau ==3) {
					listeRecord(this.humain.getNom(),seconds, "recordLE3");
				}
				if (this.mode == Mode.LABETOILE && this.niveau ==4) {
					listeRecord(this.humain.getNom(),seconds, "recordLE4");
				}
				if (this.mode == Mode.LABETOILE && this.niveau ==5) {
					listeRecord(this.humain.getNom(),seconds, "recordLE5");
				}
				if (this.mode == Mode.LABETOILE && this.niveau ==6) {
					listeRecord(this.humain.getNom(),seconds, "recordLE6");
				}
				if (this.mode == Mode.LABBOMBE && this.niveau ==1) {
					listeRecord(this.humain.getNom(),seconds, "recordLB1");
				}
				if (this.mode == Mode.LABBOMBE && this.niveau ==2) {
					listeRecord(this.humain.getNom(),seconds, "recordLB2");
				}
				if (this.mode == Mode.LABBOMBE && this.niveau ==3) {
					listeRecord(this.humain.getNom(),seconds, "recordLB3");
				}
				if (this.mode == Mode.LABBOMBE && this.niveau ==4) {
					listeRecord(this.humain.getNom(),seconds, "recordLB4");
				}
				if (this.mode == Mode.LABBOMBE && this.niveau ==5) {
					listeRecord(this.humain.getNom(),seconds, "recordLB5");
				}
				if (this.mode == Mode.LABBOMBE && this.niveau ==6) {
					listeRecord(this.humain.getNom(),seconds, "recordLB6");
				}
				if (this.mode == Mode.LABTOUT && this.niveau ==1) {
					listeRecord(this.humain.getNom(),seconds, "recordLEB1");
				}
				if (this.mode == Mode.LABTOUT && this.niveau ==2) {
					listeRecord(this.humain.getNom(),seconds, "recordLEB2");
				}
				if (this.mode == Mode.LABTOUT && this.niveau ==3) {
					listeRecord(this.humain.getNom(),seconds, "recordLEB3");
				}
				if (this.mode == Mode.LABTOUT && this.niveau ==4) {
					listeRecord(this.humain.getNom(),seconds, "recordLEB4");
				}
				if (this.mode == Mode.LABTOUT && this.niveau ==5) {
					listeRecord(this.humain.getNom(),seconds, "recordLEB5");
				}
				if (this.mode == Mode.LABTOUT && this.niveau ==6) {
					listeRecord(this.humain.getNom(),seconds, "recordLEB6");
				}
			}
			else {
				ret = "Vous avez perdu";
			}
			return ret;
		}
		
}