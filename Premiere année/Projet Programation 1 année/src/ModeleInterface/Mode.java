/**
 * les differents modes du jeux
 * @author rozenn/Klervia
 * @version1
 */
package ModeleInterface;

import java.io.Serializable;

public enum Mode implements Serializable{
	HO,
	HH,
	HOOO,
	HHOO,
	HHHO,
	HHHH,
	HOHO,
	HOOH,
	HOHH,
	HHOH,
	LAB,
	LABETOILE,
	LABBOMBE,
	LABTOUT,
	MATHEMATIQUE,
	GIDER;
}