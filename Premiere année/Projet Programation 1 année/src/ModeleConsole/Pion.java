/**
 * classe pour le pion d'un joueur
 * @author rozenn/Klervia
 * @version1
 */

package ModeleConsole;

public class Pion {

	private Couleur maCouleur;
	private int monX;
	private int monY;

	/**
	 * Constructeur de la classe pion
	 * @param maCouleur : la couleur du pion (du joueur)
	 */
	public Pion(Couleur maCouleur) {
		this.maCouleur = maCouleur;
	}
	
	/**
	 * retourne le x ou se situe le pion a ce moment
	 * @return la position
	 */
	public int getX() {
		return this.monX;
	}
	/**
	 * retourne le y ou se situe le pion a ce moment
	 * @return la position
	 */
	public int getY() {
		return this.monY;
	}
	/**
	 * modifie le x ou se situe le pion a ce moment
	 * @param nouvX : le nouveau x
	 */
	public void setX(int nouvX) {
		monX = nouvX;
	}
	/**
	 * modifie le y ou se situe le pion a ce moment
	 * @param nouvY : le nouveau y
	 */
	public void setY(int nouvY) {
		monY= nouvY;
	}
}