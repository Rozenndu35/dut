/**
 * classe pour la partie
 * @author rozenn/Klervia
 * @version1
 */

package ModeleConsole;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.*;

public class Partie implements Serializable{

	private Joueur joueur1;
	private Joueur joueur2;
	private Joueur joueur3;
	private Joueur joueur4;
	public ModeleConsole.Plateau plateau;
	private Mode mode;
	private Humain humain;
	private ArrayList<String> listeRecord;
	private int niveau ;
	//_______________________________________________________________________________________
	/**
	 * Constructeur de la classe partie
	 * @param mode : le mode de jeux
	 */
	public Partie(Mode mode) {
		Scanner scann = new Scanner(System.in);
		if (mode == Mode.OH ) {
			this.plateau = new Plateau(2);
			System.out.println("Saisir le nom du joeur1");
			String nom= scann.nextLine();
			this.joueur1 =  new Humain(nom, 1, 2, this.plateau);
			this.joueur2 =  new Ordinateur(null, 2, 2, "Ordinateur2", this.plateau );
			faireJouerJ2();
		}
		else if (mode == Mode.HH ) {
			this.plateau = new Plateau(2);
			System.out.println("Saisir le nom du joeur1");
			String nom1= scann.nextLine();
			this.joueur1 =  new Humain(nom1, 1, 2, this.plateau);
			System.out.println("Saisir le nom du joeur2");
			String nom2= scann.nextLine();
			this.joueur1 =  new Humain(nom2, 2, 2, this.plateau);
			faireJouerJ2();
		}
		else if (mode == Mode.HOOO ) {
			this.plateau = new Plateau(4);
			System.out.println("Saisir le nom du joeur1");
			String nom= scann.nextLine();
			this.joueur1 =  new Humain(nom, 1, 4, this.plateau);
			this.joueur2 =  new Ordinateur(null, 2, 4, "Ordinateur2", this.plateau );
			this.joueur3 =  new Ordinateur(null, 3, 4, "Ordinateur3" , this.plateau);
			this.joueur4 =  new Ordinateur(null, 4, 4, "Ordinateur4" ,this.plateau);
			faireJouerJ4();
		}
		else if (mode == Mode.HHOO ) {
			this.plateau = new Plateau(4);
			System.out.println("Saisir le nom du joeur1");
			String nom1= scann.nextLine();
			this.joueur1 =  new Humain(nom1, 1, 4, this.plateau);
			System.out.println("Saisir le nom du joeur2");
			String nom2= scann.nextLine();
			this.joueur2 =  new Humain(nom2, 2, 4 , this.plateau);
			this.joueur3 =  new Ordinateur(null, 3, 4, "Ordinateur3" , this.plateau);
			this.joueur4 =  new Ordinateur(null, 4, 4, "Ordinateur4", this.plateau );
			faireJouerJ4();
		}
		else if (mode == Mode.HHHO ) {
			this.plateau = new Plateau(4);
			System.out.println("Saisir le nom du joeur1");
			String nom1= scann.nextLine();
			this.joueur1 =  new Humain(nom1, 1, 4, this.plateau);
			System.out.println("Saisir le nom du joeur2");
			String nom2= scann.nextLine();
			this.joueur2 =  new Humain(nom2, 2, 4, this.plateau);
			System.out.println("Saisir le nom du joeur3");
			String nom3= scann.nextLine();
			this.joueur3 =  new Humain(nom3, 3, 4, this.plateau);
			this.joueur4 =  new Ordinateur(null, 4, 4, "Ordinateur4", this.plateau );
			faireJouerJ4();
		}
		else if (mode == Mode.HHHH ) {
			this.plateau = new Plateau(4);
			System.out.println("Saisir le nom du joeur1");
			String nom1= scann.nextLine();
			this.joueur1 =  new Humain(nom1, 1, 4, this.plateau);
			System.out.println("Saisir le nom du joeur2");
			String nom2= scann.nextLine();
			this.joueur2 =  new Humain(nom2, 2, 4, this.plateau);
			System.out.println("Saisir le nom du joeur3");
			String nom3= scann.nextLine();
			this.joueur3 =  new Humain(nom3, 3, 4, this.plateau);
			System.out.println("Saisir le nom du joeur4");
			String nom4= scann.nextLine();
			this.joueur4 =  new Humain(nom4, 4, 4, this.plateau);
			faireJouerJ4();
		}
	}
	/**
	 * Le constructeur pour les mode pedagogique
	 * @param mode : le mode de jeux pedagogique
	 * @param niveau : le niveau de la partie
	 */
	public Partie (Mode mode , int niveau) {
		Scanner scann = new Scanner(System.in);
		this.niveau = niveau;
		this.mode = mode;
		int xPion = 0;
		int yPion = 0;
		System.out.println("Saisir le nom du joueur");
		String nom= scann.nextLine();
		if (this.mode == Mode.LAB) {
			Limbirinte lab = new Limbirinte(niveau);
			this.plateau = lab.getPlateau();
			if(this.niveau ==1) {
				xPion = 18;
				yPion = 5;
			}
			else if(this.niveau ==2) {
				xPion = 0;
				yPion = 2;
			}
			else if(this.niveau ==3) {
				xPion = 9;
				yPion = 18;
			}
			else if(this.niveau ==4) {
				xPion = 0;
				yPion = 8;
			}
			else if(this.niveau ==5) {
				xPion = 0;
				yPion = 0;
			}
			else if(this.niveau ==6) {
				xPion = 15;
				yPion = 0;
			}
			this.humain =  new Humain(nom, 1, 2, this.plateau);
			this.humain.monPion.setX(xPion);
			this.humain.monPion.setY(yPion);
			faireJouerLabyrinte();
		}
		if (this.mode == Mode.LABETOILE) {
			Limbirinte lab = new Limbirinte(niveau);
			this.plateau = lab.getPlateau();
			if(this.niveau ==1) {
				this.plateau.ajouterObjet(3 , 5 , Couleur.ETOILE);
				this.plateau.ajouterObjet(4 , 14 , Couleur.ETOILE);
				this.plateau.ajouterObjet(9 , 12 , Couleur.ETOILE);
				this.plateau.ajouterObjet(14 , 9 , Couleur.ETOILE);
				this.plateau.ajouterObjet(17 , 2 , Couleur.ETOILE);
				xPion = 18;
				yPion = 5;
			}
			else if(this.niveau ==2) {
				this.plateau.ajouterObjet(5 , 14 , Couleur.ETOILE);
				this.plateau.ajouterObjet(8 , 4 , Couleur.ETOILE);
				this.plateau.ajouterObjet(9 , 10 , Couleur.ETOILE);
				this.plateau.ajouterObjet(10 , 15 , Couleur.ETOILE);
				this.plateau.ajouterObjet(15 , 4 , Couleur.ETOILE);		
				xPion = 0;
				yPion = 2;
			}
			else if(this.niveau ==3) {
				this.plateau.ajouterObjet(5 , 13 , Couleur.ETOILE);
				this.plateau.ajouterObjet(7 , 6 , Couleur.ETOILE);
				this.plateau.ajouterObjet(12 , 4 , Couleur.ETOILE);
				this.plateau.ajouterObjet(14 , 3 , Couleur.ETOILE);
				this.plateau.ajouterObjet(15 , 15 , Couleur.ETOILE);
				xPion = 9;
				yPion = 18;
			}
			else if(this.niveau ==4) {
				this.plateau.ajouterObjet( 6 , 13 , Couleur.ETOILE);
				this.plateau.ajouterObjet(7 , 5 , Couleur.ETOILE);
				this.plateau.ajouterObjet(9 , 4 , Couleur.ETOILE);
				this.plateau.ajouterObjet(11 , 8 , Couleur.ETOILE);
				this.plateau.ajouterObjet(14 , 13 , Couleur.ETOILE);
				xPion = 0;
				yPion = 8;
			}
			else if(this.niveau ==5) {
				this.plateau.ajouterObjet(3 , 1 , Couleur.ETOILE);
				this.plateau.ajouterObjet(9 , 14 , Couleur.ETOILE);
				this.plateau.ajouterObjet(10 , 12 , Couleur.ETOILE);
				this.plateau.ajouterObjet(11 , 8 , Couleur.ETOILE);
				this.plateau.ajouterObjet(17 , 2 , Couleur.ETOILE);
				xPion = 0;
				yPion = 0;
			}
			else if(this.niveau ==6) {
				this.plateau.ajouterObjet(1 , 17 , Couleur.ETOILE);
				this.plateau.ajouterObjet(6 , 7 , Couleur.ETOILE);
				this.plateau.ajouterObjet(13 , 16 , Couleur.ETOILE);
				this.plateau.ajouterObjet(14 , 6 , Couleur.ETOILE);
				this.plateau.ajouterObjet(16 , 4 , Couleur.ETOILE);
				xPion = 15;
				yPion = 0;
			}
			this.humain =  new Humain(nom, 1, 2, this.plateau);
			this.humain.monPion.setX(xPion);
			this.humain.monPion.setY(yPion);
			faireJouerLabyrinte();
		}
		if (this.mode == Mode.LABBOMBE) {
			
			Limbirinte lab = new Limbirinte(this.niveau);
			this.plateau = lab.getPlateau();
			if(this.niveau ==1) {
				this.plateau.ajouterObjet(6 , 5 , Couleur.BOMBE);
				this.plateau.ajouterObjet(6 , 10 , Couleur.BOMBE);
				this.plateau.ajouterObjet(9 , 8 , Couleur.BOMBE);
				this.plateau.ajouterObjet(12 , 4 , Couleur.BOMBE);
				this.plateau.ajouterObjet(12, 13 , Couleur.BOMBE);
				xPion = 18;
				yPion = 5;
			}
			else if(this.niveau ==2) {	
				this.plateau.ajouterObjet(8 , 12 , Couleur.BOMBE);
				this.plateau.ajouterObjet(16 , 6 , Couleur.BOMBE);
				xPion = 0;
				yPion = 2;
			}
			else if(this.niveau ==3) {
				this.plateau.ajouterObjet(7 , 12 , Couleur.BOMBE);
				this.plateau.ajouterObjet(15 , 11 , Couleur.BOMBE);
				xPion = 9;
				yPion = 18;
			}
			else if(this.niveau ==4) {
				this.plateau.ajouterObjet(3 , 9 , Couleur.BOMBE);
				this.plateau.ajouterObjet(7 , 1 , Couleur.BOMBE);
				this.plateau.ajouterObjet(11 , 9 , Couleur.BOMBE);
				xPion = 0;
				yPion = 8;
			}
			else if(this.niveau ==5) {
				this.plateau.ajouterObjet(7 , 15 , Couleur.BOMBE);
				this.plateau.ajouterObjet(8 , 6 , Couleur.BOMBE);
				this.plateau.ajouterObjet(9 , 10 , Couleur.BOMBE);
				this.plateau.ajouterObjet(12, 10 , Couleur.BOMBE);
				this.plateau.ajouterObjet(17 , 16 , Couleur.BOMBE);
				xPion = 0;
				yPion = 0;
			}
			else if(this.niveau ==6) {
				this.plateau.ajouterObjet(5 , 11 , Couleur.BOMBE);
				this.plateau.ajouterObjet(5 , 17 , Couleur.BOMBE);
				this.plateau.ajouterObjet(7 , 10 , Couleur.BOMBE);
				this.plateau.ajouterObjet(15 , 8 , Couleur.BOMBE);
				this.plateau.ajouterObjet(16, 13 , Couleur.BOMBE);
				xPion = 15;
				yPion = 0;
			}
			this.humain =  new Humain(nom, 1, 2, this.plateau);
			this.humain.monPion.setX(xPion);
			this.humain.monPion.setY(yPion);
			faireJouerLabyrinte();
		}
		if (this.mode == Mode.LABTOUT) {
			Limbirinte lab = new Limbirinte(this.niveau);
			this.plateau = lab.getPlateau();
			if(this.niveau ==1) {
				this.plateau.ajouterObjet(3 , 5 , Couleur.ETOILE);
				this.plateau.ajouterObjet(4 , 14 , Couleur.ETOILE);
				this.plateau.ajouterObjet(9 , 12 , Couleur.ETOILE);
				this.plateau.ajouterObjet(14 , 9 , Couleur.ETOILE);
				this.plateau.ajouterObjet(17 , 2 , Couleur.ETOILE);
				this.plateau.ajouterObjet(6 , 5 , Couleur.BOMBE);
				this.plateau.ajouterObjet(6 , 10 , Couleur.BOMBE);
				this.plateau.ajouterObjet(9 , 8 , Couleur.BOMBE);
				this.plateau.ajouterObjet(12 , 4 , Couleur.BOMBE);
				this.plateau.ajouterObjet(12, 13 , Couleur.BOMBE);
				xPion = 18;
				yPion = 5;
			}
			else if(this.niveau ==2) {
				this.plateau.ajouterObjet(5 , 14 , Couleur.ETOILE);
				this.plateau.ajouterObjet(8 , 4 , Couleur.ETOILE);
				this.plateau.ajouterObjet(9 , 10 , Couleur.ETOILE);
				this.plateau.ajouterObjet(10 , 15 , Couleur.ETOILE);
				this.plateau.ajouterObjet(15 , 4 , Couleur.ETOILE);	
				this.plateau.ajouterObjet(8 , 12 , Couleur.BOMBE);
				this.plateau.ajouterObjet(15 , 11 , Couleur.BOMBE);
				xPion = 0;
				yPion = 2;
			}
			else if(this.niveau ==3) {
				this.plateau.ajouterObjet(5 , 13 , Couleur.ETOILE);
				this.plateau.ajouterObjet(7 , 6 , Couleur.ETOILE);
				this.plateau.ajouterObjet(12 , 4 , Couleur.ETOILE);
				this.plateau.ajouterObjet(14 , 3 , Couleur.ETOILE);
				this.plateau.ajouterObjet(15 , 15 , Couleur.ETOILE);
				this.plateau.ajouterObjet(7 , 12 , Couleur.BOMBE);
				this.plateau.ajouterObjet(15 , 11 , Couleur.BOMBE);
				xPion = 9;
				yPion = 18;
			}
			else if(this.niveau ==4) {
				this.plateau.ajouterObjet( 6 , 13 , Couleur.ETOILE);
				this.plateau.ajouterObjet(7 , 5 , Couleur.ETOILE);
				this.plateau.ajouterObjet(9 , 4 , Couleur.ETOILE);
				this.plateau.ajouterObjet(11 , 8 , Couleur.ETOILE);
				this.plateau.ajouterObjet(14 , 13 , Couleur.ETOILE);
				this.plateau.ajouterObjet(3 , 9 , Couleur.BOMBE);
				this.plateau.ajouterObjet(7 , 1 , Couleur.BOMBE);
				this.plateau.ajouterObjet(11 , 9 , Couleur.BOMBE);
				xPion = 0;
				yPion = 8;
			}
			else if(this.niveau ==5) {
				this.plateau.ajouterObjet(3 , 1 , Couleur.ETOILE);
				this.plateau.ajouterObjet(9 , 14 , Couleur.ETOILE);
				this.plateau.ajouterObjet(10 , 12 , Couleur.ETOILE);
				this.plateau.ajouterObjet(11 , 8 , Couleur.ETOILE);
				this.plateau.ajouterObjet(17 , 2 , Couleur.ETOILE);
				this.plateau.ajouterObjet(7 , 15 , Couleur.BOMBE);
				this.plateau.ajouterObjet(8 , 6 , Couleur.BOMBE);
				this.plateau.ajouterObjet(9 , 10 , Couleur.BOMBE);
				this.plateau.ajouterObjet(12, 10 , Couleur.BOMBE);
				this.plateau.ajouterObjet(17 , 16 , Couleur.BOMBE);
				xPion = 0;
				yPion = 0;
			}
			else if(this.niveau ==6) {
				this.plateau.ajouterObjet(1 , 17 , Couleur.ETOILE);
				this.plateau.ajouterObjet(6 , 7 , Couleur.ETOILE);
				this.plateau.ajouterObjet(13 , 16 , Couleur.ETOILE);
				this.plateau.ajouterObjet(14 , 6 , Couleur.ETOILE);
				this.plateau.ajouterObjet(16 , 4 , Couleur.ETOILE);
				this.plateau.ajouterObjet(5 , 11 , Couleur.BOMBE);
				this.plateau.ajouterObjet(5 , 17 , Couleur.BOMBE);
				this.plateau.ajouterObjet(7 , 10 , Couleur.BOMBE);
				this.plateau.ajouterObjet(15 , 8 , Couleur.BOMBE);
				this.plateau.ajouterObjet(16, 13 , Couleur.BOMBE);
				xPion = 15;
				yPion = 0;
				
			}
			this.humain =  new Humain(nom, 1, 2, this.plateau);
			this.humain.monPion.setX(xPion);
			this.humain.monPion.setY(yPion);
			faireJouerLabyrinte();
		}
		if (this.mode == Mode.GIDER) {
			Gider gid = new Gider(this.niveau);
			this.plateau = gid.getPlateau();
			this.humain =  new Humain(nom, 1, 2, this.plateau);
			int result = this.humain.choixLabGide(this.niveau);
			if (result ==3) {
				System.out.println("bravo vous avcez gagner");
			}
			else {
				System.out.println("Desoler vous avez perdu "+( 3 - result) + " animaux se sont perdu");
			}
		}
		
		
	}
	//_______________________________________________________________________________________
	/**
	 * methode qui fait jouer les joueurs a tour de role lorsqu'il y a 2 joueur
	 */
	public void faireJouerJ2() {
		afficheConsole();
		boolean arret = true;
		while (!finAvecJ2() && arret) { 
			System.out.println("Au tour de "+ this.joueur1.getNom());
			arret = this.joueur1.choix();
			afficheConsole();
			if (!finAvecJ2() && arret) {
				System.out.println("Au tour de "+ this.joueur2.getNom());
				arret = this.joueur2.choix();
				afficheConsole();
			}
		}
		System.out.println ("merci d'avoir jouer");
	}
	/**
	 * methode qui fait jouer les joueurs a tour de role lorsqu'il y a 4 joueur
	 */
	public void faireJouerJ4() {
		afficheConsole();
		boolean arret = true;
		while (!finAvecJ4() && arret) { 
			System.out.println("Au tour de "+ this.joueur1.getNom());
			arret =this.joueur1.choix();
			afficheConsole();
			if (!finAvecJ4() && arret) {
				System.out.println("Au tour de "+ this.joueur3.getNom());
				arret = this.joueur3.choix();
				afficheConsole();
			}
			if (!finAvecJ4()&& arret) {
				System.out.println("Au tour de "+ this.joueur2.getNom());
				arret =this.joueur2.choix();
				afficheConsole();
			}
			if (!finAvecJ4()&& arret) {
				System.out.println("Au tour de "+ this.joueur4.getNom());
				arret =this.joueur4.choix();
				afficheConsole();
			}
		}
		System.out.println ("merci d'avoir jouer");
	}
	/**
	 * methode qui fait jouer les joueurs au labyrinthe
	 */
	public void faireJouerLabyrinte() {
		boolean continu = true;
		long tempsDebut = System.currentTimeMillis();
		afficheConsole(); 
		    
		while (continu ) { 
			System.out.println("Choisis une direction");
			continu = this.humain.choixLab();
			afficheConsole();
		}
		long tempsFin = System.currentTimeMillis();
	    double seconds = (tempsFin - tempsDebut) / 1000F;
		if (this.humain.getgagnerLab()) {
			String etoile = "";
			for (int i = 0 ; i< this.humain.getEtoile() ; i++) {
				etoile += "★";
			}
			if (this.mode == Mode.LAB && this.niveau ==1) {
				listeRecord(this.humain.getNom(),seconds, "recordL1");
			}
			if (this.mode == Mode.LAB && this.niveau ==2) {
				listeRecord(this.humain.getNom(),seconds, "recordL2");
			}
			if (this.mode == Mode.LAB && this.niveau ==3) {
				listeRecord(this.humain.getNom(),seconds, "recordL3");
			}
			if (this.mode == Mode.LAB && this.niveau ==4) {
				listeRecord(this.humain.getNom(),seconds, "record4");
			}
			if (this.mode == Mode.LAB && this.niveau ==5) {
				listeRecord(this.humain.getNom(),seconds , "recordL5");
			}
			if (this.mode == Mode.LAB && this.niveau ==6) {
				listeRecord(this.humain.getNom(),seconds, "recordL6");
			}
			if (this.mode == Mode.LABETOILE && this.niveau ==1) {
				System.out.println ( "Vous avez gagner avec " + etoile );	
				if ( this.humain.getEtoile() == 5) {
					listeRecord(this.humain.getNom(),seconds, "recordLE1");
				}
			}
			if (this.mode == Mode.LABETOILE && this.niveau ==2) {
				System.out.println ( "Vous avez gagner avec " + etoile );
				if ( this.humain.getEtoile() == 5) {
					listeRecord(this.humain.getNom(),seconds, "recordLE1");
				}
			}
			if (this.mode == Mode.LABETOILE && this.niveau ==3) {
				System.out.println ( "Vous avez gagner avec " + etoile );
				if ( this.humain.getEtoile() == 5) {
					listeRecord(this.humain.getNom(),seconds, "recordLE1");
				}
			}
			if (this.mode == Mode.LABETOILE && this.niveau ==4) {
				System.out.println ( "Vous avez gagner avec " + etoile );
				if ( this.humain.getEtoile() == 5) {
					listeRecord(this.humain.getNom(),seconds, "recordLE1");
				}
			}
			if (this.mode == Mode.LABETOILE && this.niveau ==5) {
				System.out.println ( "Vous avez gagner avec " + etoile );
				if ( this.humain.getEtoile() == 5) {
					listeRecord(this.humain.getNom(),seconds, "recordLE1");
				}
			}
			if (this.mode == Mode.LABETOILE && this.niveau ==6) {
				System.out.println ( "Vous avez gagner avec " + etoile );
				if ( this.humain.getEtoile() == 5) {
					listeRecord(this.humain.getNom(),seconds, "recordLE1");
				}
			}
			if (this.mode == Mode.LABBOMBE && this.niveau ==1) {
				listeRecord(this.humain.getNom(),seconds, "recordLB1");
			}
			if (this.mode == Mode.LABBOMBE && this.niveau ==2) {
				listeRecord(this.humain.getNom(),seconds, "recordLB2");
			}
			if (this.mode == Mode.LABBOMBE && this.niveau ==3) {
				listeRecord(this.humain.getNom(),seconds, "recordLB3");
			}
			if (this.mode == Mode.LABBOMBE && this.niveau ==4) {
				listeRecord(this.humain.getNom(),seconds, "recordLB4");
			}
			if (this.mode == Mode.LABBOMBE && this.niveau ==5) {
				listeRecord(this.humain.getNom(),seconds, "recordLB5");
			}
			if (this.mode == Mode.LABBOMBE && this.niveau ==6) {
				listeRecord(this.humain.getNom(),seconds, "recordLB6");
			}
			if (this.mode == Mode.LABTOUT && this.niveau ==1) {
				System.out.println ( "Vous avez gagner avec " + etoile );
				if ( this.humain.getEtoile() == 5) {
					listeRecord(this.humain.getNom(),seconds, "recordLE1");
				}
			}
			if (this.mode == Mode.LABTOUT && this.niveau ==2) {
				System.out.println ( "Vous avez gagner avec " + etoile );
				if ( this.humain.getEtoile() == 5) {
					listeRecord(this.humain.getNom(),seconds, "recordLE1");
				}
			}
			if (this.mode == Mode.LABTOUT && this.niveau ==3) {
				System.out.println ( "Vous avez gagner avec " + etoile );
				if ( this.humain.getEtoile() == 5) {
					listeRecord(this.humain.getNom(),seconds, "recordLE1");
				}
			}
			if (this.mode == Mode.LABTOUT && this.niveau ==4) {
				System.out.println ( "Vous avez gagner avec " + etoile );
				if ( this.humain.getEtoile() == 5) {
					listeRecord(this.humain.getNom(),seconds, "recordLE1");
				}
			}
			if (this.mode == Mode.LABTOUT && this.niveau ==5) {
				System.out.println ( "Vous avez gagner avec " + etoile );
				if ( this.humain.getEtoile() == 5) {
					listeRecord(this.humain.getNom(),seconds, "recordLE1");
				}
			}
			if (this.mode == Mode.LABTOUT && this.niveau ==6) {
				System.out.println ( "Vous avez gagner avec " + etoile );
				if ( this.humain.getEtoile() == 5) {
					listeRecord(this.humain.getNom(),seconds, "recordLE1");
				}
			}
			System.out.println ( "Vous avez gagner avec " + etoile + " en " + Double.toString(seconds) +" seconde.");
		}
		else {
			System.out.println ( "Vous avez perdu");
		}
	}
	//_______________________________________________________________________________________
	/**
	 * regarde si la partie est fini pour 2 joueur
	 * @return true si la parti est fini
	 */
	public boolean finAvecJ2() {
		boolean ret =false;
		for (int j= 0; j< 19 ; j++) {
			if ( plateau.getCases(17,j).getCouleur() == Couleur.ROUGE ) {
				ret = true;
				System.out.println (joueur1.getNom() + " a gagner");
			}
		}
		for (int j= 0; j< 19 ; j++) {
			if ( plateau.getCases(1,j).getCouleur() == Couleur.NOIR ) {
				ret = true;
				System.out.println (joueur2.getNom() + " a gagner");
			}
		}
		return ret;
	}
	/**
	 * regarde si la partie est fini pour 4 joueur
	 * @return true si la parti est fini
	 */
	public boolean finAvecJ4() {
		boolean ret =false;
		for (int j= 0; j< 19 ; j++) {
			if ( plateau.getCases(17,j).getCouleur() == Couleur.ROUGE ) {
				ret = true;
				System.out.println (joueur1.getNom() + " a gagner");
			}
		}
		for (int j= 0; j< 19 ; j++) {
			if ( plateau.getCases(1,j).getCouleur() == Couleur.NOIR ) {
				ret = true;
				System.out.println (joueur2.getNom() + " a gagner");
			}
		}
		for (int i= 0; i< 19 ; i++) {
			if ( plateau.getCases(i,17).getCouleur() == Couleur.BLEU ) {
				ret = true;
				System.out.println (joueur3.getNom() + " a gagner");
			}
		}
		for (int i= 0; i< 19 ; i++) {
			if ( plateau.getCases(i,1).getCouleur() == Couleur.VERT ) {
				ret = true;
				System.out.println (joueur4.getNom() + " a gagner");
			}
		}
		return ret;
	}
	//_______________________________________________________________________________________
	/**
	 * Methode qui permet d enregistrer une partie en cours
	 */
	public void enregistrerPartie() {
		try {
			FileOutputStream fos = new FileOutputStream("DonneesPartie");
			ObjectOutputStream oos = new ObjectOutputStream (fos);
			oos.writeObject(this);
			oos.close();
			fos.close();
		}
		catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	/**
	 * Methode qui permet de charger une partie precedemment enregistree
	 */
	public void chargerPartie() {
		Partie partie;
		try {
			FileInputStream fis = new FileInputStream("DonneesPartie");
			ObjectInputStream ois = new ObjectInputStream (fis);
			partie = (Partie)ois.readObject();
			ois.close();
			fis.close();
		}
		catch (IOException ioe) {
			ioe.printStackTrace();
			return;
		}
		catch (ClassNotFoundException c) {
			System.out.println("Classe non trouvee");
			c.printStackTrace();
			return;
		}
		this.joueur1=partie.joueur1;
		this.joueur2=partie.joueur2;
		this.joueur3=partie.joueur3;
		this.joueur4=partie.joueur4;
	}
	
	/**
	 * lit le fichier des records
	 * @return la list des personne avec leur score
	 */
	public ArrayList<String> lireFichierRecord (String fichier){
		ArrayList<String> ret = new ArrayList<String>();
		try {
			Scanner in = new Scanner (new FileReader ("./donnee/"+fichier+".txt"));
			while (in.hasNextLine()) {
				ret.add(in.nextLine());
			}
			in.close();
		} 
		catch (FileNotFoundException e) {}
		return ret;
		
	}
	
	/**
	 * ecrit le fichier des record
	 */
	public void ecrireFichierRecord (String fichier) {
		try {
			PrintWriter out = new PrintWriter ("./donnee/"+fichier+".txt");
			for( String ligne : this.listeRecord) {
				out.println (ligne);
				out.close();
			}
		}
		catch (FileNotFoundException e) {
			System.out.println (e.getMessage());
		}
	}
	
	/**
	 * Creer la liste des record
	 */
	public void listeRecord(String nNom , Double nTemps, String fichier) {
		this.listeRecord = lireFichierRecord (fichier);
		HashMap record = new HashMap<String , Double>();
		String[] classement = new String[3];
		classement[0]= "";
		classement[1]= "";
		classement[2]= "";
		boolean perdu = false;
		for (String e : this.listeRecord) {
			String nom = "";
			for ( int i =0 ; ((i<e.length()) && (Character.isDigit(e.charAt(i)) == false)) ; i++ ){
				nom += e.charAt(i);
			}
			nom = nom.trim();
			Double temps = (double) 0;
			double ret= 0;
			int indice = 0;
			for ( int i =0 ; ((i<e.length()) && (Character.isDigit(e.charAt(i)) == false)) ; i++ ){
				indice = i;
			}
			String chaine = e.substring (indice);
			chaine = chaine.trim();
			temps = Double.parseDouble(chaine);
			record.put(nom,temps);
			for (int i = 0; i<3 ; i++) {
				if (classement[i] != "") {
					if ((Double)record.get(classement[i])>(Double)record.get(nom)) {
						String rec = classement[i];
						classement[i] = nom;
						nom =rec;
					}
				}
				else {classement[i] = nom;}
			}
		}
		if (record.containsKey(nNom)) { 
			if ((Double)record.get(nNom)>nTemps) {
				record.put(nNom,nTemps);
				boolean dedans= false;
				int position = Arrays.binarySearch( classement, nNom );
				for (int i = 0; i<position ; i++) {
					if ((Double)record.get(classement[i])>nTemps) {
						String rec = classement[i];
						classement[i] = nNom;
						dedans = true;
						if (i == 0) {
							classement[2] = classement[1];
							classement[1] = classement[0];
						}
						else if (i == 1) {
							classement[1] = classement[0];
						}
					}
				}
			}
			else {
				perdu = true;
			}
		}
		else {
			record.put(nNom,nTemps);
			boolean dedans= false;
			boolean sort = false;
			for (int i = 0; i<3 && !sort; i++) {
				if (classement[i] != "") {
					if ((Double)record.get(classement[i])>nTemps) {
						String rec = classement[i];
						classement[i] = nNom;
						dedans = true;
						if (i == 0) {
							classement[2] = classement[1];
							classement[1] = classement[0];
						}
						else if (i == 1) {
							classement[1] = classement[0];
						}
					}
				}
				else {classement[i] = nNom;dedans = true; sort = true;}
			}
			if(!dedans){
				perdu = true;
			}
		}
		this.listeRecord = new ArrayList<String>();
		System.out.println(record.toString());
		System.out.println("Liste des records");
		System.out.println("1er : "+ classement[0] +" " +record.get(classement[0] )+ "s" );
		this.listeRecord.add(classement[0] +" " + record.get(classement[0]));
		if (classement[1]!= "") {
			System.out.println("2eme : "+ classement[1] +" " +record.get(classement[1] ) + "s");
			this.listeRecord.add(classement[1] +" " + record.get(classement[1]));
			if (classement[2]!= "") {
				System.out.println("3eme : "+ classement[2] +" " +record.get(classement[2] ) + "s");
				this.listeRecord.add(classement[2] +" " + record.get(classement[2]));
			}
		}
		if (!perdu) { System.out.println("desoler vous nete pas dans les meilleur avec " + nTemps + "s" );}
		ecrireFichierRecord (fichier);
	}
	//_______________________________________________________________________________________
	/**
	 * affiche la plateau sur la console
	 */
	public void afficheConsole() {
		System.out.println(plateau.toString());
	}
}