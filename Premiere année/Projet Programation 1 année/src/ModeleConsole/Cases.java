/**
 * classe pour les cases du tableau
 * @author rozenn/Klervia
 * @version1
 */
package ModeleConsole;

import java.io.Serializable;

public class Cases implements Serializable {

	private Couleur maCouleur;

	/**
	 * retourne sa couleur
	 * @return la couleur
	 */
	public Couleur getCouleur() {
		return this.maCouleur;
	}
	/**
	 * change sa couleur
	 * @param newCouleur : la nouvelle couleur
	 */
	public void setCouleur(Couleur newCouleur) {
		this.maCouleur = newCouleur;
	}
	/**
	 * pour afficher les cases
	 * @return la valeur de la case en string
	 */
	public String toString(){
		String ret ="";
		if (getCouleur() == Couleur.BLEU) {
			ret= " 🐶";
		}
		if (getCouleur() == Couleur.VERT) {
			ret= " 🦌";
		}
		if (getCouleur() == Couleur.ROUGE) {
			ret= " 🦍";
		}
		if (getCouleur() == Couleur.NOIR) {
			ret= " 🦁";
		}
		if (getCouleur() == Couleur.MUR) {
			ret= " ▆ ";
		}
		if (getCouleur() == Couleur.LIBREB) {
			ret= "   ";
		}
		if (getCouleur() == Couleur.LIBREP) {
			ret= "[ ]";
		}
		if (getCouleur() == Couleur.BOMBE) {
			ret= " 💣";
		}
		if (getCouleur() == Couleur.ETOILE) {
			ret= " ⭐";
		}
		if (getCouleur() == Couleur.ARRIVER) {
			ret= " 🏁";
		}
		if (getCouleur() == Couleur.MAISON) {
			ret= " 🏠";
		}

		
		
		return ret;
	}
	
}