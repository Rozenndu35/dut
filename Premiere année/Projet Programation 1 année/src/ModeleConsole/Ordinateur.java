/**
 * classe pour les actions de l'ordinateur (IA)
 * @author rozenn/Klervia
 * @version1
 */

package ModeleConsole;

import java.util.HashMap;

public class Ordinateur extends Joueur {
	private Difficulte dif;
	
	/**
	 * Constructeur de la classe ordinateur
	 * @param dif : la difficulter
	 * @param num : son numero
	 * @param nbJ : le nombre de joueur
	 * @param nom : son nom
	 */
	public Ordinateur(Difficulte dif, int num , int nbJ, String nom , Plateau plat) {
		super (nom , num, nbJ, plat);
		this.dif = Difficulte.ALEATOIRE;
	}
	
	/**
	 * Methode qui regroupe tous les choix de l ordinateur
	 */
	public boolean choix() {
		if ( this.dif == Difficulte.ALEATOIRE) {
			if (getNbBarriere()>0) {
				int i = (int)(Math.random() * ((10) + 1));
				if (i<= 7) {
					deplacerPion();
				}
				else {
					placerBarriere();
				}	
			}
		}
		return true;
	}

	/**
	 * Methode qui permet a l ordinateur de deplacer son pion
	 */
	public void deplacerPion() {
		if ( this.dif == Difficulte.ALEATOIRE) {
			super.lePlateau.verifPion(super.monPion.getX() ,super. monPion.getY());
			HashMap listex= super.lePlateau.getlistX();
			HashMap listey= super.lePlateau.getlistY();
			int taille = listex.size();
			int i = (int)Math.random() * taille;
			super.lePlateau.changeCouleurDeplacerPion(super.monPion.getX() ,super. monPion.getY() , (int)listex.get(i) ,(int) listey.get(i));
			super.monPion.setX((int)listex.get(i));
			super.monPion.setY((int)listey.get(i));
		}
	}

	/**
	 * Methode qui permet a l ordinateur de placer une barriere
	 */
	public void placerBarriere() {
		if ( this.dif == Difficulte.ALEATOIRE) {
			int i = (int)Math.random();
			if (i == 0) {
				//Horizontale
				int x = (int)(2+Math.random() * (16 - 2));
				int y = (int)(Math.random()* 7);
				y = y+y+1;
				boolean marche = super.lePlateau.verifBarriere(x , y, false, this);
				while (!marche) {
					x = (int)(2+Math.random() * (16 - 2));
					y = (int)(Math.random()* 7);
					y = y+y+1;
				}
				super.lePlateau.ajouteCouleurBarriere(x, y, false);
			}
			else {
				//verticale
				int x = (int)(Math.random() * (7));
				x = x+x+1;
				int y = (int)(2+Math.random()* (16-2));
				
				boolean marche = super.lePlateau.verifBarriere(x , y ,true, this);
				while (!marche) {
					x = (int)(Math.random() * (7));
					x = x+x+1;
					y = (int)(2+Math.random()* (16-2));
				}
				super.lePlateau.ajouteCouleurBarriere(x, y, true);
			}	
		}
	}

}