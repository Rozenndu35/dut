/**
 * classe pour les actions du joueur humain
 * @author rozenn/Klervia
 * @version1
 */
package ModeleConsole;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Humain extends Joueur {
	private boolean gangnerLab;
	private int etoile;

	/**
	 * constructeur de la classe humain
	 * @param nom
	 * @param numero
	 * @param nbJouer
	 */
	public Humain(String nom, int numero, int nbJouer, Plateau plat) {
		super(nom, numero, nbJouer,plat);
		gangnerLab = false;
		etoile= 0;
	}
//__________________________________________________________________________________________________________________________ 
	/**
	 * methode qui regroupe tous les choix possibles du joueur pour jouer au koridor
	 */
	public boolean choix() {
		boolean ret= true;
		boolean marche = false;
		if (getNbBarriere()>0) {
			Scanner sc = new Scanner(System.in);
			while (!marche ) {
				System.out.println("Saisir b pour placer une barriere et saisir p pour deplacer le pion dit quiter si vous voulez arreter la partie");
				String ch= sc.nextLine();
				if (ch.equals("b")) {
					marche = true;
					placerBarriere();
				}
				
				else if (ch.equals("p")) {
					marche = true;
					deplacePion();
				}
				else if (ch.equals("quiter")) {
					marche = true;
					ret = false;
				}
			}
		}
		else {
			deplacePion();
		}
		return ret;
	}
	/**
	 * methode qui propose a l'utilisateur de choisir sont deplacement dans le labyrinthe
	 * @return : return false si il est mort par une bombe
	 */
	public boolean choixLab() {
		boolean ret =true;
		boolean marche = false;	
		int nX = -1 ;
		boolean chiffre = false;
		while (!chiffre ||( nX>18 || nX<0 )) {
			try {
				System.out.println("Saisir x");
				Scanner sc = new Scanner (System.in);
				nX = sc.nextInt();
				chiffre =true;
			}
			catch(InputMismatchException e) {}
		}
		chiffre =false;
		
		int nY =-1;
		while (!chiffre || (nY>18 || nY<0)) {
			try {
				System.out.println("Saisir y");
				Scanner sc = new Scanner (System.in);
				nY = sc.nextInt();
				chiffre = true;
			}
			catch(InputMismatchException e) {}
		}
		if (super.lePlateau.getCases(nX,nY).getCouleur() != Couleur.BOMBE) {
			super.lePlateau.verifPionLab(super.monPion.getX(),super.monPion.getY());
			HashMap xpossible = super.lePlateau.getlistX();
			HashMap ypossible = super.lePlateau.getlistY();
			for (int i = 0; i< xpossible.size() && !marche; i++) {
				if (nX == (int)xpossible.get(i) && nY == (int)ypossible.get(i) ) {
					marche= true;
					
				}
			}
			while (!marche || !ret ) {
				System.out.println("Saisie incorrecte");
				nX = -1 ;
				chiffre = false;
				while (!chiffre ||( nX>18 || nX<0 )) {
					try {
						System.out.println("Saisir x");
						Scanner sc = new Scanner (System.in);
						nX = sc.nextInt();
						chiffre =true;
					}
					catch(InputMismatchException e) {}
				}
				chiffre =false;
				
				nY =-1;
				while (!chiffre || (nY>18 || nY<0)) {
					try {
						System.out.println("Saisir y");
						Scanner sc = new Scanner (System.in);
						nY = sc.nextInt();
						chiffre = true;
					}
					catch(InputMismatchException e) {}
				}
				super.lePlateau.verifPionLab(super.monPion.getX(),super.monPion.getY());
				xpossible = super.lePlateau.getlistX();
				ypossible = super.lePlateau.getlistY();
				for (int i = 0; i< xpossible.size() && !marche; i++) {
					System.out.println (xpossible.toString());
					System.out.println (ypossible.toString());
					if (nX == (int)xpossible.get(i) && nY == (int)ypossible.get(i) ) {
						marche= true;
						if (this.lePlateau.getCases(nX,nY).getCouleur() == Couleur.BOMBE) {ret = false;}
						if (this.lePlateau.getCases(nX,nY).getCouleur() == Couleur.ARRIVER) {ret = false;}
					}
				}
			}
			if (this.lePlateau.getCases(nX,nY).getCouleur() == Couleur.ETOILE) {this.etoile ++;}
			if (this.lePlateau.getCases(nX,nY).getCouleur() == Couleur.ARRIVER) {ret = false; this.gangnerLab = true;}
			super.lePlateau.changeCouleurDeplacerPion(super.monPion.getX(), super.monPion.getY(), nX , nY);
			super.monPion.setX(nX);
			super.monPion.setY(nY);
		}
		else {ret = false;}
		return ret;
	}
	/**
	 * methode qui propose a l'utilisateur de choisir sont deplacement et indique le lieu ou il doit alles
	 * @param niveau : le niveau du plateau
	 * @return le nombre de maison qu'il a rejoint
	 */
	public int choixLabGide(int niveau) {
		System.out.println(super.lePlateau.toString());
		int point= 0;
		int oX =5;
		int oY =0;
		Gider gider = new Gider(niveau);
		System.out.println("Pour le chien");
		if (niveau == 1) {
			for (int i=0 ; i < gider.getlist1Bleu().size(); i++) {
				System.out.println(gider.getlist1Bleu().get(i));	
				int nX = -1 ;
				boolean chiffre = false;
				while (!chiffre ||( nX>18 || nX<0 )) {
					try {
						System.out.println("Saisir x");
						Scanner sc = new Scanner (System.in);
						nX = sc.nextInt();
						chiffre =true;
					}
					catch(InputMismatchException e) {}
				}
				chiffre =false;
				
				int nY =-1;
				while (!chiffre || (nY>18 || nY<0)) {
					try {
						System.out.println("Saisir y");
						Scanner sc = new Scanner (System.in);
						nY = sc.nextInt();
						chiffre = true;
					}
					catch(InputMismatchException e) {}
				}
				super.lePlateau.changeCouleurDeplacerPionGidder(oX , oY, nX,nY,Couleur.BLEU); 
				System.out.println(super.lePlateau.toString());
				oX = nX;
				oY = nY;
			}
			if(oX== 9 && oY == 17) {
				point ++;
			}
			oX =11;
			oY =0;
			System.out.println("Pour le singe");
			for (int i=0 ; i < gider.getlist1Rouge().size(); i++) {
				System.out.println(gider.getlist1Rouge().get(i));
				int nX = -1 ;
				boolean chiffre = false;
				while (!chiffre ||( nX>18 || nX<0 )) {
					try {
						System.out.println("Saisir x");
						Scanner sc = new Scanner (System.in);
						nX = sc.nextInt();
						chiffre =true;
					}
					catch(InputMismatchException e) {}
				}
				chiffre =false;
				
				int nY =-1;
				while (!chiffre || (nY>18 || nY<0)) {
					try {
						System.out.println("Saisir y");
						Scanner sc = new Scanner (System.in);
						nY = sc.nextInt();
						chiffre = true;
					}
					catch(InputMismatchException e) {}
				}
				super.lePlateau.changeCouleurDeplacerPionGidder(oX , oY, nX,nY,Couleur.ROUGE); 
				System.out.println(super.lePlateau.toString());
				oX = nX;
				oY = nY;
			}
			if(oX== 3 && oY == 17) {
				point ++;
			}
			oX =15;
			oY =0;
			System.out.println("Pour le lion");
			for (int i=0 ; i < gider.getlist1Noir().size(); i++) {
				System.out.println(gider.getlist1Noir().get(i));
				int nX = -1 ;
				boolean chiffre = false;
				while (!chiffre ||( nX>18 || nX<0 )) {
					try {
						System.out.println("Saisir x");
						Scanner sc = new Scanner (System.in);
						nX = sc.nextInt();
						chiffre =true;
					}
					catch(InputMismatchException e) {}
				}
				chiffre =false;
				
				int nY =-1;
				while (!chiffre || (nY>18 || nY<0)) {
					try {
						System.out.println("Saisir y");
						Scanner sc = new Scanner (System.in);
						nY = sc.nextInt();
						chiffre = true;
					}
					catch(InputMismatchException e) {}
				}
				super.lePlateau.changeCouleurDeplacerPionGidder(oX , oY, nX,nY,Couleur.NOIR); 
				System.out.println(super.lePlateau.toString());
				oX = nX;
				oY = nY;
			}
			if(oX== 14 && oY == 18) {
				point ++;
			}
		}
		else {
			oX =2;
			oY =0;
			for (int i=0 ; i < gider.getlist2Rouge().size(); i++) {
				System.out.println(gider.getlist2Rouge().get(i));
				int nX = -1 ;
				boolean chiffre = false;
				while (!chiffre ||( nX>18 || nX<0 )) {
					try {
						System.out.println("Saisir x");
						Scanner sc = new Scanner (System.in);
						nX = sc.nextInt();
						chiffre =true;
					}
					catch(InputMismatchException e) {}
				}
				chiffre =false;
				
				int nY =-1;
				while (!chiffre || (nY>18 || nY<0)) {
					try {
						System.out.println("Saisir y");
						Scanner sc = new Scanner (System.in);
						nY = sc.nextInt();
						chiffre = true;
					}
					catch(InputMismatchException e) {}
				}
				super.lePlateau.changeCouleurDeplacerPionGidder(oX , oY, nX,nY,Couleur.BLEU); 
				System.out.println(super.lePlateau.toString());
				oX = nX;
				oY = nY;
			}
			if(oX== 2 && oY == 17) {
				point ++;
			}
			oX =8;
			oY =0;
			System.out.println("Pour le singe");
			for (int i=0 ; i < gider.getlist2Bleu().size(); i++) {
				System.out.println(gider.getlist2Bleu().get(i));
				int nX = -1 ;
				boolean chiffre = false;
				while (!chiffre ||( nX>18 || nX<0 )) {
					try {
						System.out.println("Saisir x");
						Scanner sc = new Scanner (System.in);
						nX = sc.nextInt();
						chiffre =true;
					}
					catch(InputMismatchException e) {}
				}
				chiffre =false;
				
				int nY =-1;
				while (!chiffre || (nY>18 || nY<0)) {
					try {
						System.out.println("Saisir y");
						Scanner sc = new Scanner (System.in);
						nY = sc.nextInt();
						chiffre = true;
					}
					catch(InputMismatchException e) {}
				}
				super.lePlateau.changeCouleurDeplacerPionGidder(oX , oY, nX,nY,Couleur.ROUGE); 
				System.out.println(super.lePlateau.toString());
				oX = nX;
				oY = nY;
			}
			if(oX== 17 && oY == 18) {
				point ++;
			}
			oX =13;
			oY =0;
			System.out.println("Pour le lion");
			for (int i=0 ; i < gider.getlist2Noir().size(); i++) {
				System.out.println(gider.getlist2Noir().get(i));
				int nX = -1 ;
				boolean chiffre = false;
				while (!chiffre ||( nX>18 || nX<0 )) {
					try {
						System.out.println("Saisir x");
						Scanner sc = new Scanner (System.in);
						nX = sc.nextInt();
						chiffre =true;
					}
					catch(InputMismatchException e) {}
				}
				chiffre =false;
				
				int nY =-1;
				while (!chiffre || (nY>18 || nY<0)) {
					try {
						System.out.println("Saisir y");
						Scanner sc = new Scanner (System.in);
						nY = sc.nextInt();
						chiffre = true;
					}
					catch(InputMismatchException e) {}
				}
				super.lePlateau.changeCouleurDeplacerPionGidder(oX , oY, nX,nY,Couleur.NOIR); 
				System.out.println(super.lePlateau.toString());
				oX = nX;
				oY = nY;
			}
			if(oX== 7 && oY == 18) {
				point ++;
			}
		}
		return point;
	}
	//__________________________________________________________________________________________________________________________	
	/**
	 * methode qui permet au joueur de deplacer son pion
	 */
	public void deplacePion() {
		boolean marche = false;
		int nX = -1 ;
		boolean chiffre = false;
		while (!chiffre ||( nX>18 || nX<0 )) {
			try {
				System.out.println("Saisir x");
				Scanner sc = new Scanner (System.in);
				nX = sc.nextInt();
				chiffre =true;
			}
			catch(InputMismatchException e) {}
		}
		chiffre =false;
		
		int nY =-1;
		while (!chiffre || (nY>18 || nY<0)) {
			try {
				System.out.println("Saisir y");
				Scanner sc = new Scanner (System.in);
				nY = sc.nextInt();
				chiffre = true;
			}
			catch(InputMismatchException e) {}
		}
		super.lePlateau.verifPion(super.monPion.getX(),super.monPion.getY());
		HashMap xpossible = super.lePlateau.getlistX();
		HashMap ypossible = super.lePlateau.getlistY();
		for (int i = 0; i< xpossible.size() && !marche; i++) {
			if (nX == (int)xpossible.get(i) && nY == (int)ypossible.get(i) ) {
				marche= true;
			}
		}
		while (!marche ) {
			System.out.println("Saisie incorrecte");
			nX = -1 ;
			chiffre = false;
			while (!chiffre ||( nX>18 || nX<0 )) {
				try {
					System.out.println("Saisir x");
					Scanner sc = new Scanner (System.in);
					nX = sc.nextInt();
					chiffre =true;
				}
				catch(InputMismatchException e) {}
			}
			chiffre =false;
			
			nY =-1;
			while (!chiffre || (nY>18 || nY<0)) {
				try {
					System.out.println("Saisir y");
					Scanner sc = new Scanner (System.in);
					nY = sc.nextInt();
					chiffre = true;
				}
				catch(InputMismatchException e) {}
			}	
			super.lePlateau.verifPion(super.monPion.getX(),super.monPion.getY());
			xpossible = super.lePlateau.getlistX();
			ypossible = super.lePlateau.getlistY();
			for (int i = 0; i< xpossible.size() && !marche; i++) {
				if (nX == (int)xpossible.get(i) && nY == (int)ypossible.get(i) ) {
					marche= true;
				}
			}
		}
		super.lePlateau.changeCouleurDeplacerPion(super.monPion.getX(), super.monPion.getY(), nX , nY);
		super.monPion.setX(nX);
		super.monPion.setY(nY);
	}
	/**
	 * methode qui permet au joueur de placer une barriere
	 */
	public void placerBarriere() {
		boolean sens=false;
			
		int nX = -1 ;
		boolean chiffre = false;
		while (!chiffre) {
			try {
				System.out.println("Saisir x");
				Scanner sc = new Scanner (System.in);
				nX = sc.nextInt();
				chiffre =true;
			}
			catch(InputMismatchException e) {}
		}
		chiffre =false;
		
		int nY =-1;
		while (!chiffre) {
			try {
				System.out.println("Saisir y");
				Scanner sc = new Scanner (System.in);
				nY = sc.nextInt();
				chiffre = true;
			}
			catch(InputMismatchException e) {}
		}
		System.out.println("Saisir sens : v ou h");
		Scanner st = new Scanner (System.in);
		String s = st.nextLine(); 
		if(s.equals("v")) {
			sens=true;
		}
		while (!super.lePlateau.verifBarriere(nX,nY,sens , this)) {
			System.out.println("Saisie incorrecte");
			chiffre = false;
			while (!chiffre) {
				try {
					System.out.println("Saisir x");
					Scanner sc = new Scanner (System.in);
					nX = sc.nextInt();
					chiffre =true;
				}
				catch(InputMismatchException e) {}
			}
			chiffre =false;
			
			while (!chiffre) {
				try {
					System.out.println("Saisir y");
					Scanner sc = new Scanner (System.in);
					nY = sc.nextInt();
					chiffre = true;
				}
				catch(InputMismatchException e) {}
			}
			System.out.println("Saisir sens : v ou h");
			Scanner str = new Scanner (System.in);
			s = str.nextLine();
			if(s.equals("v")) {
				sens=true;
			}
		}
		super.lePlateau.ajouteCouleurBarriere(nX,nY,sens);
	}
	//__________________________________________________________________________________________________________________________
	/**
	 * retourne le nombre d'etoile recuperer
	 * @return :le nombre d'etoile
	 */
	public int getEtoile() {
		return this.etoile;
	}
	/**
	 * retourne vrai si il a gagner au labyrinthe
	 * @return : si il a gagner ou non
	 */
	public boolean getgagnerLab() {
		return this.gangnerLab;
	}

}