package Controleur.Jeux;
/**
 * le controleur de la grille
 */
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JTable;

import ModeleInterface.Cases;
import ModeleInterface.Couleur;
import ModeleInterface.GrilleModel;
import ModeleInterface.Humain;
import ModeleInterface.Joueur;
import Vue.Fenetre;
import Vue.Fin;
import Vue.JeuxLab;



public class GrilleControleurLab implements MouseListener{
	JTable table;
	private JeuxLab jeux;
	private GrilleModel model;
	private Fenetre fenetre;
	
	public GrilleControleurLab(JTable table, JeuxLab j , GrilleModel m, Fenetre f) {
		this.table=table;
		this.jeux=j;
		this.model = m;
		this.fenetre=f;
	}

	/**
	 * make the event
	 * @param event: the event whith the mousse
	 */
	@Override
	public void mouseClicked(MouseEvent event) {
	
		int xx = table.rowAtPoint(event.getPoint());
		int yy = table.columnAtPoint(event.getPoint());
		Humain personne = this.jeux.part.getAuTourDeJoueurLab();
		if (this.table.getModel().getValueAt(xx, yy).toString().equals("donnee/CaseVide.png")
			|| this.table.getModel().getValueAt(xx, yy).toString().equals("donnee/fakepion.png")||
			this.table.getModel().getValueAt(xx, yy).toString().equals("donnee/Arrivée.png")) {
			if (personne!= null) {
				//boolean marche =this.jeux.part.plateau.verifPionLab(xx, yy);
				boolean marche = personne.choixLab(xx, yy);
				if (marche) { 
					this.model.setgrille(this.jeux.part.plateau.getPlateauJeux());
					
					this.model.setgrille(this.jeux.part.plateau.getPlateauJeux());
				}
			}	
		}		
		if(personne.getgagnerLab()) {
			double temps = this.jeux.temps();
			fenetre.setPanel(new Fin(fenetre, jeux.part.getJoueurGagnantLab(temps)));
		}
	}

	@Override
	// Le reste des methodes peut etre utile ou pas en fonction
	public void mouseEntered(MouseEvent event) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		
	}

}
