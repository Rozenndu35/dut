package Controleur.Jeux;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import Vue.Fenetre;
import Vue.Quiter;

public class BoutonQuitter  implements ActionListener{
	private Fenetre fenetre;
	
	/**
	 * le constructeur
	 * @param j : la fenetre jeux
	 */
	public BoutonQuitter(Fenetre f ) {
		this.fenetre = f;
	}

	/**
	 * cree les action du bouton
	 * @param e : l'evenement
	*/
	public void actionPerformed(ActionEvent e) {
		Quiter q = new Quiter(this.fenetre);
	}

}
