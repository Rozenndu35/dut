package Controleur.Jeux;
/**
 * permet de quiter l'application
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import ModeleInterface.GrilleModel;
import ModeleInterface.Humain;
import Vue.Fenetre;
import Vue.Jeux;

public class QuiterDef  implements ActionListener{
	private Fenetre fenetre;
	private JFrame quit;
	
	/**
	 * le constructeur
	 * @param j : la fenetre jeux
	 */
	public QuiterDef(Fenetre f , JFrame q) {
		this.fenetre = f;
		this.quit = q;
	}

	/**
	 * cree les action du bouton
	 * @param e : l'evenement
	*/
	public void actionPerformed(ActionEvent e) {
		this.fenetre.dispose();
		this.quit.dispose();
	}

}
