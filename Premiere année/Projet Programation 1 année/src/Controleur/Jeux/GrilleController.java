package Controleur.Jeux;
/**
 * le controleur de la grille
 */
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JTable;

import ModeleInterface.Cases;
import ModeleInterface.Couleur;
import ModeleInterface.GrilleModel;
import ModeleInterface.Humain;
import ModeleInterface.Joueur;
import Vue.Fenetre;
import Vue.Fin;
import Vue.Jeux;

public class GrilleController implements MouseListener{
	JTable table;
	private Jeux jeux;
	private GrilleModel model;
	private Fenetre fenetre;
	
	public GrilleController(JTable table, Jeux j , GrilleModel m, Fenetre f) {
		this.table=table;
		this.jeux=j;
		this.model = m;
		this.fenetre=f;
	}

	/**
	 * make the event
	 * @param event: the event whith the mousse
	 */
	@Override
	public void mouseClicked(MouseEvent event) {
		int xx = table.rowAtPoint(event.getPoint());
		int yy = table.columnAtPoint(event.getPoint());
		if (this.table.getModel().getValueAt(xx, yy).toString().equals("donnee/CaseVide.png")
			|| this.table.getModel().getValueAt(xx, yy).toString().equals("donnee/fakepion.png")) {
			Humain personne = this.jeux.part.getAuTourDeJoueur();
			if (personne!= null) {
				boolean marche = personne.deplacePion(xx, yy);
				if (marche) { 
					this.model.setgrille(this.jeux.part.plateau.getPlateauJeux());
					this.jeux.part.auJoueurSuivant();
					
					jeux.changeLebelJoueur();
					this.model.setgrille(this.jeux.part.plateau.getPlateauJeux());
				}
			}	
		}
		else if(this.table.getModel().getValueAt(xx, yy).toString().equals("donnee/BarriereVide.png")
				|| this.table.getModel().getValueAt(xx, yy).toString().equals("donnee/fakebarriere.png")
				) {
			
			Humain personne = this.jeux.part.getAuTourDeJoueur();
			if (personne!= null) {
				boolean marche = personne.placerBarriere(xx, yy,jeux.getSensBarriere());
				if (marche) { 
					this.model.setgrille(this.jeux.part.plateau.getPlateauJeux());
					this.jeux.part.auJoueurSuivant();
					jeux.changeLebelJoueur();
					this.model.setgrille(this.jeux.part.plateau.getPlateauJeux());
				}
			}
		}
		
		if(jeux.part.getJoueurGagnant()!=null) {
			fenetre.setPanel(new Fin(fenetre, jeux.part.getJoueurGagnant()));
		}
	}

	@Override
	// Le reste des m�thodes peut �tre utile ou pas en fonction
	public void mouseEntered(MouseEvent event) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		
	}

}
