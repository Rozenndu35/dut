package Controleur.Option;
/**
 * le controleur pour le bouton option de la fenetre
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Vue.Accueil;
import Vue.Debut;
import Vue.Fenetre;
import Vue.Jeux;

public class AccueilButtonListener implements ActionListener {

	private Fenetre laFenetre;
	/**
	 * le constructeur
	 * @param f : la fenetre
	 */
	public AccueilButtonListener(Fenetre f) {
		this.laFenetre=f;
	}
	/**
	 *  cree les action du bouton
	 * @param e : l'evenement
	 */
	public void actionPerformed(ActionEvent e) {
		this.laFenetre.setPanel(new Accueil(laFenetre));
	}

}
