package Controleur.Option;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import Vue.Fenetre;
import Vue.Pedagogique;

public class PedaButtonListener  implements ActionListener{
	private Fenetre fenetre;

	/**
	 * le constructeur
	 * @param d : la fenetre de debut
	 */
	public PedaButtonListener(Fenetre f ) {
		this.fenetre = f;
	}
	/**
	 *  cree les action du bouton
	 * @param e : l'evenement
	 */
	public void actionPerformed(ActionEvent e) {
		this.fenetre.setPanel(new Pedagogique(fenetre));
	}
}
