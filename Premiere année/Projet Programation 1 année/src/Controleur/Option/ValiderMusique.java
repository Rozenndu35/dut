package Controleur.Option;
/**
 * le controleur du bouton valider d' option
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ModeleInterface.Audio;
import Vue.Fenetre;
import Vue.Options;

public class ValiderMusique implements ActionListener{
		private Fenetre fe;
		private Options op;
		private Audio s;
		/**
		 * le constructeur
		 * @param option : la fenetre ou il est placer
		 */
		public ValiderMusique(Options option, Fenetre f) {
			this.op = option;
			this.fe = f;
			s = new Audio();
			s = this.fe.getSon();
		}

		/**
		 *  cree les action du bouton
		 * @param e : l'evenement
		 */
		public void actionPerformed(ActionEvent e) {
			boolean musique = this.op.musiqueOui();	
			if (musique) {
				String choix = this.op.musique();
				// changer la musique avec le choix fait
			}
			else {
				//eteindre la musique
				fe.setSon(s);
			}
			op.dispose();
		}
		
	}

