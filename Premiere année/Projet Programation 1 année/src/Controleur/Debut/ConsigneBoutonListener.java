/**
 * controleur pour le bouton valider les coix
 * @author rozenn/Klervia
 * @version1
 */

package Controleur.Debut;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ModeleInterface.Mode;
import ModeleInterface.Partie;
import Vue.*;

public class ConsigneBoutonListener  implements ActionListener {
	private Fenetre fenetre;
	private Pedagogique peda;
	/**
	 * le constructeur
	 * @param f : la fenetre 
	 * @param p : la fenetre pedagogique
	 */
	public ConsigneBoutonListener(Fenetre f , Pedagogique p) {
		this.fenetre = f;
		this.peda = p;
	}
	/**
	 * cree les action du bouton
	 * @param e : l'evenement
	 */
	public void actionPerformed(ActionEvent arg0) {
		int niveau = (int)this.peda.getNiveauConsi().getSelectedItem();
		String nom = this.peda.getNom(); ;
		Partie part = new Partie(Mode.GIDER, niveau, nom);
		
	}

}
