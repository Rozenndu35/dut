package Controleur.Debut;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import Vue.Accueil;
import Vue.Consigne;
import Vue.Debut;
import Vue.Fenetre;
import Vue.Regles;

public class BoutonConsigne implements ActionListener {

	private Consigne fenetre;
	public BoutonConsigne(Consigne f) {
		this.fenetre=f;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		fenetre.dispose();
	}

}
