/*
23/11/18 9h30
TP5_COSTIOU_rozenn.sql
Costiou Rozenn groupe 1B1
*/



DROP TABLE Avion
;
DROP TABLE Employe
;
DROP TABLE Modele
;
DROP TABLE Compagnie
;

CREATE TABLE Compagnie(
                nom VARCHAR2(20)
                CONSTRAINT pk_Compagnie PRIMARY KEY,
                nbAvions NUMBER(3)
                CONSTRAINT ck_nbAvions CHECK (nbAvions>1)
                )
;

CREATE TABLE Modele(
                nom VARCHAR2(20)
                CONSTRAINT pk_Modele PRIMARY KEY,
                nbCopilotes NUMBER(2)
                CONSTRAINT ck_nbCopilotes CHECK (nbCopilotes>0)
                )
;
CREATE TABLE Employe(
                noSS NUMBER(20)
                CONSTRAINT pk_NoSS PRIMARY KEY,
                nom VARCHAR2(30),
                prenom VARCHAR2(20),
                statut VARCHAR2(10)
                CONSTRAINT ck_statut CHECK (statut IN ('Commandant','Copilote')),
                laCompagnie VARCHAR2(20)
                CONSTRAINT fk_Employer_Compagnie REFERENCES Compagnie(nom) NOT NULL
                )
;
CREATE TABLE Avion(
                numero NUMBER(10)
                CONSTRAINT pk_avion PRIMARY KEY,
                LeCommandant NUMBER(20)
                CONSTRAINT fk_Avion_Employe REFERENCES Employe(noSS),
                laCompagnie VARCHAR2(20)
                CONSTRAINT fk_Avion_Compagnie REFERENCES Compagnie(nom) NOT NULL,
                leModele VARCHAR2(20)
                CONSTRAINT fk_Avion_Modele REFERENCES Modele(nom) NOT NULL
                )
;

INSERT INTO Modele VALUES('A380',2);
INSERT INTO Modele VALUES('A330',1);
INSERT INTO Modele VALUES('A350',1);
INSERT INTO Modele VALUES('A320',1);
INSERT INTO Modele VALUES('A320 neo',1);
INSERT INTO Modele VALUES('Boeing 747',1);
INSERT INTO Modele VALUES('Boeing 777',1);
INSERT INTO Modele VALUES('Boeing 787',1);
INSERT INTO Modele VALUES('Boeing 737',1);
					

INSERT INTO Compagnie VALUES('Air France',232);
INSERT INTO Compagnie VALUES('British Airways',252);
INSERT INTO Compagnie VALUES('Lufthansa',182);
INSERT INTO Compagnie VALUES('Cathay Pacific',312);
INSERT INTO Compagnie VALUES('Easy Jet',152);


insert into Employe values(1,'ANDRE','William','Commandant','Air France');
insert into Employe values(2,'BRISSON','Bastien','Commandant','Air France');
insert into Employe values(3,'CHARRIER AUBERTOT','Ethan','Commandant','Air France');
insert into Employe values(4,'CHARRON','Maxime','Commandant','Air France');
insert into Employe values(5,'CINAR','Mikail','Commandant','Air France');
insert into Employe values(6,'CORMIER','Aymeric','Commandant','Air France');
insert into Employe values(7,'DANIEL','Maelvin','Commandant','Air France');
insert into Employe values(8,'FERNANDEZ','Yoann','Copilote','Air France');
insert into Employe values(9,'FORESTIER','Juliette','Copilote','Air France');
insert into Employe values(10,'GASSINE','Alan','Copilote','Air France');
insert into Employe values(11,'GUILHE LA COMBE DE VILLERS','Pierre-Louis','Copilote','Air France');
insert into Employe values(12,'HASSANI','Nada','Copilote','Air France');
insert into Employe values(13,'JAMBOU','Fabien','Copilote','Air France');
insert into Employe values(14,'LANDUREIN','Enora','Copilote','Air France');
insert into Employe values(15,'LE BAGOUSSE','Adrien','Copilote','Air France');

insert into Employe values(30,'BAUTRAIS','Klervia','Commandant','British Airways');
insert into Employe values(31,'BLANCHARD','Nicolas','Commandant','British Airways');
insert into Employe values(32,'BRIQUET','Corentin','Commandant','British Airways');
insert into Employe values(33,'BRUNET','Benoit','Commandant','British Airways');
insert into Employe values(34,'CACHIN-BERNARD','Gwendal','Commandant','British Airways');
insert into Employe values(35,'CARRER','Aurelien','Commandant','British Airways');
insert into Employe values(36,'COLLET','Gweltaz','Commandant','British Airways');
insert into Employe values(37,'COSTIOU','Rozenn','Copilote','British Airways');
insert into Employe values(38,'GOHIER','Louis','Copilote','British Airways');
insert into Employe values(39,'JOLY','Mathieu','Copilote','British Airways');
insert into Employe values(40,'LAMIRAULT','Julien','Copilote','British Airways');
insert into Employe values(41,'LE FORESTIER','Erwan','Copilote','British Airways');
insert into Employe values(42,'LE SAINT-QUINIO','Camille','Copilote','British Airways');
insert into Employe values(43,'LEBLAY','Julien','Copilote','British Airways');
insert into Employe values(44,'LÉON','Damien','Copilote','British Airways');
insert into Employe values(45,'LOUVEL','Victor','Copilote','British Airways');


insert into Employe values(62,'BLANDEL LE BRETON','Victorien','Commandant','Cathay Pacific');
insert into Employe values(63,'BOURDIN','Meriadec','Commandant','Cathay Pacific');
insert into Employe values(64,'CASTEL','Clement','Commandant','Cathay Pacific');
insert into Employe values(65,'CHANTREL','Yann','Commandant','Cathay Pacific');
insert into Employe values(66,'CORNEC','Kilian','Commandant','Cathay Pacific');
insert into Employe values(67,'DELB','Clément','Commandant','Cathay Pacific');
insert into Employe values(68,'DOUSSET','Sylvain','Commandant','Cathay Pacific');
insert into Employe values(69,'EDIMBOURG','Pierre','Commandant','Cathay Pacific');
insert into Employe values(70,'GUILLEUX','Lucas','Copilote','Cathay Pacific');
insert into Employe values(71,'KHALIFA','Youssef','Copilote','Cathay Pacific');
insert into Employe values(72,'KISCHEL','Thery','Copilote','Cathay Pacific');
insert into Employe values(73,'LE FEUNTEUN','Florian','Copilote','Cathay Pacific');
insert into Employe values(74,'LE GAL','Quentin','Copilote','Cathay Pacific');
insert into Employe values(75,'LEFEBRE','Raphael','Copilote','Cathay Pacific');
insert into Employe values(76,'LIMA','Pierre','Copilote','Cathay Pacific');
insert into Employe values(77,'MAJANI','Lily','Copilote','Cathay Pacific');
insert into Employe values(78,'MARTIN','Pierre','Copilote','Cathay Pacific');
insert into Employe values(79,'MAZALEYRAT','Erwan','Copilote','Cathay Pacific');


insert into Employe values(83,'PERRUDIN','Baptiste','Commandant','Lufthansa');
insert into Employe values(84,'RAMOS GIQUEL','Joshua','Commandant','Lufthansa');
insert into Employe values(85,'ROBERT','Jolan','Commandant','Lufthansa');
insert into Employe values(86,'SCHLOSSER','Maxime','Commandant','Lufthansa');
insert into Employe values(87,'AUDOIN','Remi','Commandant','Lufthansa');
insert into Employe values(88,'BERNARDIN','Amaury','Commandant','Lufthansa');
insert into Employe values(89,'BIZOUARN','Aymeric','Commandant','Lufthansa');
insert into Employe values(90,'BOISNARD','Noewen','Copilote','Lufthansa');
insert into Employe values(91,'CHARPENTIER','Francois','Copilote','Lufthansa');
insert into Employe values(92,'COCARD','Constance','Copilote','Lufthansa');
insert into Employe values(93,'CORMERAIS','Florent','Copilote','Lufthansa');
insert into Employe values(94,'COTHENET','Erwann','Copilote','Lufthansa');
insert into Employe values(95,'DAUTRICHE','Martin','Copilote','Lufthansa');
insert into Employe values(96,'DE SOUZA','Alois','Copilote','Lufthansa');
insert into Employe values(97,'DELOURMEL','Arnaud','Copilote','Lufthansa');
insert into Employe values(98,'DELVENNE','Jerome','Copilote','Lufthansa');


insert into Employe values(103,'JOUSSET','Alexandre','Commandant','Easy Jet');
insert into Employe values(104,'LEGAL','Leo','Commandant','Easy Jet');
insert into Employe values(105,'LOISEL','Titouan','Commandant','Easy Jet');
insert into Employe values(106,'LORAND','Manon','Commandant','Easy Jet');
insert into Employe values(107,'MAINGUY','Alex','Commandant','Easy Jet');
insert into Employe values(108,'MARCHAND','Aurelien','Copilote','Easy Jet');
insert into Employe values(109,'MOUSSA','Kartoibidine','Copilote','Easy Jet');
insert into Employe values(110,'NAQUET','Pierre Galaad','Copilote','Easy Jet');
insert into Employe values(111,'PESSEL','Antoine','Copilote','Easy Jet');
insert into Employe values(112,'TOULEMONT','Amedee','Copilote','Easy Jet');
insert into Employe values(113,'VANDAMME','Benoit','Copilote','Easy Jet');
insert into Employe values(114,'YAICHE','Yoann','Copilote','Easy Jet');


INSERT INTO Avion VALUES(1,1,'Air France','A380');
INSERT INTO Avion VALUES(3,2,'Air France','A320');
INSERT INTO Avion VALUES(4,3,'Air France','A330');
INSERT INTO Avion VALUES(5,4,'Air France','A320');
INSERT INTO Avion VALUES(6,5,'Air France','A320');
INSERT INTO Avion VALUES(7,6,'Air France','Boeing 737');
INSERT INTO Avion VALUES(8,7,'Air France','A330');
INSERT INTO Avion VALUES(9,1,'Air France','A330');
INSERT INTO Avion VALUES(10,2,'Air France','A320');
INSERT INTO Avion VALUES(11,3,'Air France','A320');
INSERT INTO Avion VALUES(12,4,'Air France','Boeing 737');
INSERT INTO Avion VALUES(13,5,'Air France','A380');
INSERT INTO Avion VALUES(14,6,'Air France','A320');
INSERT INTO Avion VALUES(15,7,'Air France','Boeing 737');
INSERT INTO Avion VALUES(16,1,'Air France','A380');

INSERT INTO Avion VALUES(17,30,'British Airways','A380');
INSERT INTO Avion VALUES(37,31,'British Airways','A330');
INSERT INTO Avion VALUES(47,32,'British Airways','Boeing 777');
INSERT INTO Avion VALUES(57,33,'British Airways','A320');
INSERT INTO Avion VALUES(67,34,'British Airways','A320');
INSERT INTO Avion VALUES(77,35,'British Airways','Boeing 737');
INSERT INTO Avion VALUES(87,36,'British Airways','Boeing 747');
INSERT INTO Avion VALUES(97,30,'British Airways','Boeing 777');
INSERT INTO Avion VALUES(107,31,'British Airways','Boeing 737');
INSERT INTO Avion VALUES(117,32,'British Airways','A320');
INSERT INTO Avion VALUES(127,33,'British Airways','A350');
INSERT INTO Avion VALUES(137,34,'British Airways','A380');
INSERT INTO Avion VALUES(147,35,'British Airways','Boeing 737');
INSERT INTO Avion VALUES(157,36,'British Airways','A320');
INSERT INTO Avion VALUES(167,31,'British Airways','Boeing 787');

INSERT INTO Avion VALUES(18,83,'Lufthansa','A380');
INSERT INTO Avion VALUES(38,84,'Lufthansa','A320');
INSERT INTO Avion VALUES(48,85,'Lufthansa','A330');
INSERT INTO Avion VALUES(58,86,'Lufthansa','Boeing 787');
INSERT INTO Avion VALUES(68,87,'Lufthansa','A320');
INSERT INTO Avion VALUES(78,88,'Lufthansa','A320');
INSERT INTO Avion VALUES(88,89,'Lufthansa','A330');
INSERT INTO Avion VALUES(98,83,'Lufthansa','A330');
INSERT INTO Avion VALUES(108,84,'Lufthansa','Boeing 787');
INSERT INTO Avion VALUES(118,85,'Lufthansa','A320');
INSERT INTO Avion VALUES(128,86,'Lufthansa','A320');
INSERT INTO Avion VALUES(138,87,'Lufthansa','A380');
INSERT INTO Avion VALUES(148,88,'Lufthansa','Boeing 787');
INSERT INTO Avion VALUES(158,89,'Lufthansa','A320');
INSERT INTO Avion VALUES(168,85,'Lufthansa','Boeing 777');

INSERT INTO Avion VALUES(111,62,'Cathay Pacific','Boeing 777');
INSERT INTO Avion VALUES(311,63,'Cathay Pacific','A320');
INSERT INTO Avion VALUES(411,64,'Cathay Pacific','A330');
INSERT INTO Avion VALUES(511,65,'Cathay Pacific','A320');
INSERT INTO Avion VALUES(611,66,'Cathay Pacific','Boeing 777');
INSERT INTO Avion VALUES(711,67,'Cathay Pacific','A320');
INSERT INTO Avion VALUES(811,68,'Cathay Pacific','Boeing 777');
INSERT INTO Avion VALUES(911,69,'Cathay Pacific','Boeing 777');
INSERT INTO Avion VALUES(1112,64,'Cathay Pacific','A320');
INSERT INTO Avion VALUES(1113,65,'Cathay Pacific','Boeing 777');
INSERT INTO Avion VALUES(1114,66,'Cathay Pacific','A320');
INSERT INTO Avion VALUES(1115,67,'Cathay Pacific','A320');
INSERT INTO Avion VALUES(1116,68,'Cathay Pacific','Boeing 777');

INSERT INTO Avion VALUES(122,103,'Easy Jet','A320');
INSERT INTO Avion VALUES(322,104,'Easy Jet','A320');
INSERT INTO Avion VALUES(422,105,'Easy Jet','A330');
INSERT INTO Avion VALUES(522,106,'Easy Jet','A320');
INSERT INTO Avion VALUES(622,107,'Easy Jet','A320');
INSERT INTO Avion VALUES(723,103,'Easy Jet','A320');
INSERT INTO Avion VALUES(822,104,'Easy Jet','A330');
INSERT INTO Avion VALUES(922,105,'Easy Jet','A330');
INSERT INTO Avion VALUES(722,103,'Easy Jet','A320');
INSERT INTO Avion VALUES(823,104,'Easy Jet','A330');
INSERT INTO Avion VALUES(928,105,'Easy Jet','A330');
INSERT INTO Avion VALUES(1220,106,'Easy Jet','Boeing 737');
INSERT INTO Avion VALUES(1221,107,'Easy Jet','Boeing 737');
INSERT INTO Avion VALUES(1222,103,'Easy Jet','Boeing 737');
INSERT INTO Avion VALUES(1223,104,'Easy Jet','Boeing 737');
INSERT INTO Avion VALUES(1224,105,'Easy Jet','Boeing 737');
INSERT INTO Avion VALUES(1225,106,'Easy Jet','Boeing 737');
INSERT INTO Avion VALUES(1226,107,'Easy Jet','Boeing 737');
INSERT INTO Avion VALUES(1227,106,'Easy Jet','Boeing 737');
INSERT INTO Avion VALUES(1228,107,'Easy Jet','Boeing 737');

					
--1-
SELECT DISTINCT UPPER(leModele)
FROM Avion
WHERE laCompagnie = 'Easy Jet'
;

/*
UPPER(LEMODELE)     
--------------------
BOEING 737
A320
A330
*/

--2-
SELECT DISTINCT UPPER(laCompagnie)
FROM Avion
WHERE  leModele = 'A380'
;

/*
UPPER(LACOMPAGNIE)  
--------------------
AIR FRANCE
LUFTHANSA
BRITISH AIRWAYS
*/

--3-
SELECT DISTINCT UPPER (nom), UPPER (prenom)
FROM Employe, Avion
WHERE Employe.laCompagnie = Avion.laCompagnie
AND Employe.laCompagnie = 'Air France'
AND statut = 'Commandant'
AND leModele = 'A380'
;

/*
UPPER(NOM)                     UPPER(PRENOM)       
------------------------------ --------------------
DANIEL                         MAELVIN             
CHARRON                        MAXIME              
CINAR                          MIKAIL              
BRISSON                        BASTIEN             
ANDRE                          WILLIAM             
CHARRIER AUBERTOT              ETHAN               
CORMIER                        AYMERIC             

7 lignes sélectionnées. 
*/

--4-
SELECT DISTINCT UPPER (nom)
FROM Modele
MINUS
SELECT DISTINCT UPPER (leModele)
FROM Avion
;

/*
UPPER(NOM)          
--------------------
A320 NEO
*/

--5-

SELECT laCompagnie, COUNT (*) 
FROM Employe
WHERE statut = 'Commandant'
GROUP BY laCompagnie
;

/*
LACOMPAGNIE            COUNT(*)
-------------------- ----------
Air France                    7
British Airways               7
Easy Jet                      5
Cathay Pacific                8
Lufthansa                     7
*/

--6-
/*SELECT laCompagnie, MAX (nbc)
 FROM (SELECT laCompagnie, COUNT (*)nbc
            FROM Employe
            WHERE statut = 'Copilote'
            GROUP BY laCompagnie
            )
;



*/


--7-
SELECT DISTINCT UPPER(nom), MIN(nbAvions)
FROM Compagnie
;