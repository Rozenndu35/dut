/*
16/11/18 9h30
TP5_COSTIOU_rozenn.sql
Costiou Rozenn groupe 1B1
*/

--1-

/*
CREATE TABLE Companie(
                        CONSTRAINT ck-nbAvions
                        CHECK (nbAvions>1),
                        )
 CREATE TABLE Modele(
                        CONSTRAINT ck-nbCopilotes
                        CHECK (condePostal>0),
                        )
 CREATE TABLE Employé(
                        CONSTRAINT ck-statut
                        CHECK (statut LIKE 'COMMANDANT DE BORD' OR 'COPILOTE'),
                        )
*/


--2-

/*
on creer d'abore les table sans cle etrangere (compagnie et Modele) puis employe et enfin Avion

on vas les detruire dans le sans inverse de la creation
*/

--3-
DROP TABLE Avion
;
DROP TABLE Employe
;
DROP TABLE Modele
;
DROP TABLE Compagnie
;

CREATE TABLE Compagnie(
                nom VARCHAR2(20)
                CONSTRAINT pk_Compagnie PRIMARY KEY,
                nbAvions NUMBER(3)
                CONSTRAINT ck_nbAvions CHECK (nbAvions>1)
                )
;

CREATE TABLE Modele(
                nom VARCHAR2(20)
                CONSTRAINT pk_Modele PRIMARY KEY,
                nbCopilotes NUMBER(2)
                CONSTRAINT ck_nbCopilotes CHECK (nbCopilotes>0)
                )
;
CREATE TABLE Employe(
                noSS NUMBER(20)
                CONSTRAINT pk_NoSS PRIMARY KEY,
                nom VARCHAR2(30),
                prenom VARCHAR2(20),
                statut VARCHAR2(10)
                CONSTRAINT ck_statut CHECK (statut IN ('Commandant','Copilote')),
                laCompagnie VARCHAR2(20)
                CONSTRAINT fk_Employer_Compagnie REFERENCES Compagnie(nom) NOT NULL
                )
;
CREATE TABLE Avion(
                numero NUMBER(10)
                CONSTRAINT pk_avion PRIMARY KEY,
                LeCommandant NUMBER(20)
                CONSTRAINT fk_Avion_Employe REFERENCES Employe(noSS),
                laCompagnie VARCHAR2(20)
                CONSTRAINT fk_Avion_Compagnie REFERENCES Compagnie(nom) NOT NULL,
                leModele VARCHAR2(20)
                CONSTRAINT fk_Avion_Modele REFERENCES Modele(nom) NOT NULL
                )
;

--4-

