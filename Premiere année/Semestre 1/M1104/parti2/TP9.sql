DROP TABLE Emprunt;
DROP TABLE  EcritPar;
DROP TABLE Exemplaire;
DROP TABLE Abonne;
DROP TABLE Auteur;
DROP TABLE Ouvrage;





CREATE TABLE Ouvrage(
                cote VARCHAR2(10)
                CONSTRAINT pk_Ouvrage PRIMARY KEY,
                titre VARCHAR2(20),
                nbExemplaire NUMBER(10)
                CONSTRAINT ck_nbExemplaire CHECK ( nbExemplaire>0)
                )
;

CREATE TABLE Auteur(
                nom VARCHAR2(20),
                prenom VARCHAR2(20),
                CONSTRAINT pkAuteur PRIMARY KEY (Nom, Prenom),
                anneeNaissance NUMBER(4),
                natianalite VARCHAR2(20)
                )
;

CREATE TABLE Abonne(
                noAbonne NUMBER(5)
                CONSTRAINT pk_Abonne PRIMARY KEY,
                nomAbonne VARCHAR2(20)
                CONSTRAINT nn_nomAbonne NOT NULL,
                prenomAbonne VARCHAR2(15),
                email VARCHAR2(20) 
                CONSTRAINT nn_email NOT NULL
                CONSTRAINT ck_email CHECK (UPPER (email) LIKE '%'||'@'||'%.FR')
                )
;

CREATE TABLE Exemplaire(
                numero NUMBER(5)
                CONSTRAINT pk_Exemplaire PRIMARY KEY,
                lOuvrage VARCHAR2(20)
                CONSTRAINT fk_Exemplaire_ouvrage REFERENCES Ouvrage(cote) NOT NULL,
                dateAchat DATE
                )
;
  
CREATE TABLE EcritPar(
                unOuvrage VARCHAR2(20)
                CONSTRAINT fk_EcritPar_ouvrage REFERENCES Ouvrage(cote),
                nomA VARCHAR2(20),
                prenomA VARCHAR2(20),
                CONSTRAINT fkEcritPar FOREIGN KEY(nomA,prenomA) REFERENCES Auteur(nom,prenom)
                )
;

CREATE TABLE Emprunt(
                unAbonne NUMBER(5)
                CONSTRAINT fk_Emprunt_Abonne REFERENCES Abonne(noAbonne),
                unExemplaire NUMBER(5)
                CONSTRAINT fk_Emprunt_Exemplaire REFERENCES Exemplaire(numero)NOT NULL,
                dateEmprunt DATE
                )
;
DELETE FROM Emprunt;
DELETE FROM EcritPar;
DELETE FROM Exemplaire;
DELETE FROM Ouvrage;
DELETE FROM Auteur;
DELETE FROM Abonne;


INSERT INTO Abonne VALUES(24,'ORVOEN','Romain','ORVOEN@INFO1.FR');
INSERT INTO Abonne VALUES(25,'POULNAIS','Brieuc','POULNAIS@INFO1.fr');
INSERT INTO Abonne VALUES(26,'QUERE','Hippolyte','QUERE@INFO1.fr');
INSERT INTO Abonne VALUES(27,'ROBILLIARD','Remi','ROBILLIARD@INFO1.FR');
INSERT INTO Abonne VALUES(28,'ROUET','Yoann','ROUET@INFO1.fr');
INSERT INTO Abonne VALUES(53,'SOURGET','Theo','SOURGET@INFO1.fr');
INSERT INTO Abonne VALUES(54,'VADELEAU','Alan','VADELEAU@INFO1.fr');
INSERT INTO Abonne VALUES(55,'VIVION','Achille','VIVION@INFO1.fr');
INSERT INTO Abonne VALUES(56,'VOLMIER','Romain','VOLMIER@INFO1.fr');
INSERT INTO Abonne VALUES(57,'WEINREICH','Clément','WEINREICH@INFO1.fr');
INSERT INTO Abonne VALUES(82,'PAUTREL','Arthur','PAUTREL@INFO1.fr');
INSERT INTO Abonne VALUES(85,'ROBERT','Jolan','ROBERT@INFO1.fr');
INSERT INTO Abonne VALUES(110,'NAQUET','Pierre Galaad','NAQUET@INFO1.fr');
INSERT INTO Abonne VALUES(111,'PESSEL','Antoine','PESSEL@INFO1.fr');

INSERT INTO Auteur VALUES('Ichbiah','Jean',1940,'France');
INSERT INTO Auteur VALUES('Gosling','James',1955,'Canada');
INSERT INTO Auteur VALUES('Naughton','Patrick',1965,'USA');
INSERT INTO Auteur VALUES('Ritchie','Dennis',1941,'USA');
INSERT INTO Auteur VALUES('Van Rossum','Guido',1956,'Pays Bas');
INSERT INTO Auteur VALUES('Backus','John',1924,'USA');
INSERT INTO Auteur VALUES('Rowling','Joanne',1965,'Grande Bretagne');
INSERT INTO Auteur VALUES('Merle','Robert',1908,'France');
INSERT INTO Auteur VALUES('Hugo','Victor',1802,'France');

INSERT INTO Ouvrage VALUES('Ich01','le langage ADA',5);
INSERT INTO Ouvrage VALUES('GosNau01','le langage JAVA',4);
INSERT INTO Ouvrage VALUES('Rit01','le langage C',10);
INSERT INTO Ouvrage VALUES('VanRoss01','le langage Python',5);
INSERT INTO Ouvrage VALUES('Back01','le langage Fortran',3);
INSERT INTO Ouvrage VALUES('Rowl01','Harry Potter',3);
INSERT INTO Ouvrage VALUES('Merle01','Fortune de France',4);
INSERT INTO Ouvrage VALUES('Merle02','week end a Zuydcoote',1);
INSERT INTO Ouvrage VALUES('Hugo01','Quatre vingt treize',3);
INSERT INTO Ouvrage VALUES('Hugo02','Contemplations',4);
INSERT INTO Ouvrage VALUES('Hugo03','les miserables',5);

INSERT INTO Exemplaire VALUES(1,'Ich01',TO_DATE('10/02/1999','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(2,'Ich01',TO_DATE('10/02/1999','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(3,'Ich01',TO_DATE('10/02/1999','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(4,'Ich01',TO_DATE('10/02/1999','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(5,'Ich01',TO_DATE('10/02/1999','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(10,'GosNau01',TO_DATE('10/02/2010','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(11,'GosNau01',TO_DATE('10/02/2010','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(12,'GosNau01',TO_DATE('10/02/2010','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(13,'GosNau01',TO_DATE('10/02/2010','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(20,'Rit01',TO_DATE('10/02/2015','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(21,'Rit01',TO_DATE('10/02/2015','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(22,'Rit01',TO_DATE('10/02/2015','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(23,'Rit01',TO_DATE('10/02/2015','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(24,'Rit01',TO_DATE('10/02/2015','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(30,'VanRoss01',TO_DATE('10/02/2017','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(31,'VanRoss01',TO_DATE('10/02/2017','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(32,'VanRoss01',TO_DATE('10/02/2017','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(33,'VanRoss01',TO_DATE('10/02/2017','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(34,'VanRoss01',TO_DATE('10/02/2017','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(40,'Back01',TO_DATE('10/02/2017','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(41,'Back01',TO_DATE('10/02/2017','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(42,'Back01',TO_DATE('10/02/2017','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(50,'Rowl01',TO_DATE('10/02/2017','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(51,'Rowl01',TO_DATE('10/02/2017','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(52,'Rowl01',TO_DATE('10/02/2017','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(60,'Merle01',TO_DATE('10/02/2008','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(61,'Merle01',TO_DATE('10/02/2008','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(62,'Merle01',TO_DATE('10/02/2008','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(63,'Merle01',TO_DATE('10/02/2008','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(70,'Merle02',TO_DATE('10/02/2008','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(71,'Merle02',TO_DATE('10/02/2008','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(80,'Hugo01',TO_DATE('10/02/2008','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(81,'Hugo01',TO_DATE('10/02/2008','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(82,'Hugo01',TO_DATE('10/02/2008','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(90,'Hugo02',TO_DATE('10/02/2008','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(91,'Hugo02',TO_DATE('10/02/2008','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(92,'Hugo02',TO_DATE('10/02/2008','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(93,'Hugo02',TO_DATE('10/02/2008','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(100,'Hugo03',TO_DATE('10/02/2008','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(101,'Hugo03',TO_DATE('10/02/2008','DD/MM/YYYY'));
INSERT INTO Exemplaire VALUES(102,'Hugo03',TO_DATE('10/02/2008','DD/MM/YYYY'));

INSERT INTO EcritPar VALUES('Ich01','Ichbiah','Jean');
INSERT INTO EcritPar VALUES('GosNau01','Gosling','James');
INSERT INTO EcritPar VALUES('GosNau01','Naughton','Patrick');
INSERT INTO EcritPar VALUES('Rit01','Ritchie','Dennis');
INSERT INTO EcritPar VALUES('VanRoss01','Van Rossum','Guido');
INSERT INTO EcritPar VALUES('Back01','Backus','John');
INSERT INTO EcritPar VALUES('Merle01','Merle','Robert');
INSERT INTO EcritPar VALUES('Merle02','Merle','Robert');
INSERT INTO EcritPar VALUES('Hugo01','Hugo','Victor');
INSERT INTO EcritPar VALUES('Hugo02','Hugo','Victor');
INSERT INTO EcritPar VALUES('Hugo03','Hugo','Victor');

INSERT INTO Emprunt VALUES(24,1,TO_DATE('10/09/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(24,70,TO_DATE('29/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(25,1,TO_DATE('29/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(25,10,TO_DATE('29/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(25,20,TO_DATE('29/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(25,30,TO_DATE('29/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(25,40,TO_DATE('20/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(25,50,TO_DATE('15/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(25,60,TO_DATE('28/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(25,70,TO_DATE('29/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(25,80,TO_DATE('29/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(25,90,TO_DATE('30/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(25,100,TO_DATE('30/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(26,11,TO_DATE('30/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(27,12,TO_DATE('30/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(28,13,TO_DATE('30/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(53,81,TO_DATE('30/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(53,91,TO_DATE('30/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(53,101,TO_DATE('30/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(53,12,TO_DATE('30/09/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(85,82,TO_DATE('30/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(85,92,TO_DATE('30/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(85,102,TO_DATE('30/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(53,82,TO_DATE('30/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(53,92,TO_DATE('30/11/2018','DD/MM/YYYY'));
INSERT INTO Emprunt VALUES(111,63,TO_DATE('30/11/2018','DD/MM/YYYY'));

--1-
SELECT UPPER (titre)
FROM Ouvrage
WHERE ouvrage.nbexemplaire != (SELECT COUNT (*)
                                FROM Exemplaire
                                WHERE lOuvrage = cote
                                )
;

/*
UPPER(TITRE)        
--------------------
LE LANGAGE C
WEEK END A ZUYDCOOTE
LES MISERABLES

*/

--2-

SELECT UPPER (nomAbonne), UPPER (prenomAbonne), UPPER (titre)
FROM Abonne , Emprunt , Exemplaire , Ouvrage
WHERE noAbonne = unAbonne
AND unExemplaire = numero
AND lOuvrage = cote
AND dateEmprunt < '23/11/2018'
;
/*
UPPER(NOMABONNE)     UPPER(PRENOMABO UPPER(TITRE)        
-------------------- --------------- --------------------
ORVOEN               ROMAIN          LE LANGAGE ADA      
SOURGET              THEO            LE LANGAGE JAVA     
POULNAIS             BRIEUC          LE LANGAGE FORTRAN  
POULNAIS             BRIEUC          HARRY POTTER
*/

--3-
SELECT DISTINCT titre
FROM Ouvrage
WHERE NOT EXISTS ( SELECT numero
                    FROM Exemplaire
                    WHERE lOuvrage = cote
                    MINUS
                    SELECT unExemplaire
                    FROM Emprunt
                    )
;

/*
TITRE               
--------------------
Quatre vingt treize
le langage JAVA
les miserables
*/

--4-

SELECT UPPER(nomA) , UPPER (prenomA), nbExemplaire
FROM EcritPar, Ouvrage
WHERE unOuvrage=cote
GROUP BY UPPER(titre)
ORDER BY nbExemplaire DESC
;
