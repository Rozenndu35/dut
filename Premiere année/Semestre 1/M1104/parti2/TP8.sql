/*
16/11/18 9h30
TP5_COSTIOU_rozenn.sql
Costiou Rozenn groupe 1B1
*/
--1-
DROP TABLE Emprunt;
DROP TABLE  EcritPar;
DROP TABLE Exemplaire;
DROP TABLE Abonne;
DROP TABLE Auteur;
DROP TABLE Ouvrage;





CREATE TABLE Ouvrage(
                cote VARCHAR2(10)
                CONSTRAINT pk_Ouvrage PRIMARY KEY,
                titre VARCHAR2(20),
                nbExemplaire NUMBER(10)
                CONSTRAINT ck_nbExemplaire CHECK ( nbExemplaire>0)
                )
;

CREATE TABLE Auteur(
                nom VARCHAR2(20),
                prenom VARCHAR2(20),
                CONSTRAINT pkAuteur PRIMARY KEY (Nom, Prenom),
                anneeNaissance DATE,
                natianalite VARCHAR2(20)
                )
;

CREATE TABLE Abonne(
                noAbonne NUMBER(5)
                CONSTRAINT pk_Abonne PRIMARY KEY,
                nomAbonne VARCHAR2(20)
                CONSTRAINT nn_nomAbonne NOT NULL,
                prenomAbonne VARCHAR2(15),
                email VARCHAR2(20) 
                CONSTRAINT nn_email NOT NULL
                CONSTRAINT ck_email CHECK (email LIKE '%@%.fr')
                )
;

CREATE TABLE Exemplaire(
                numero NUMBER(5)
                CONSTRAINT pk_Exemplaire PRIMARY KEY,
                lOuvrage VARCHAR2(20)
                CONSTRAINT fk_Exemplaire_ouvrage REFERENCES Ouvrage(cote) NOT NULL,
                dateAchat NUMBER(5)
                )
;
  
CREATE TABLE EcritPar(
                unOuvrage VARCHAR2(20)
                CONSTRAINT fk_EcritPar_ouvrage REFERENCES Ouvrage(cote),
                nomA VARCHAR2(20),
                prenomA VARCHAR2(20),
                CONSTRAINT fkEcritPar FOREIGN KEY(nomA,prenomA) REFERENCES Auteur(nom,prenom)
                )
;

CREATE TABLE Emprunt(
                unAbonne NUMBER(5)
                CONSTRAINT fk_Emprunt_Abonne REFERENCES Abonne(noAbonne),
                unExemplaire NUMBER(5)
                CONSTRAINT fk_Emprunt_Exemplaire REFERENCES Exemplaire(numero)NOT NULL,
                dateEmprunt DATE
                )
;

--2-
DELETE FROM Emprunt;
DELETE FROM EcritPar;
DELETE FROM Exemplaire;
DELETE FROM Abonne;
DELETE FROM Auteur;
DELETE FROM Ouvrage;

INSERT INTO Ouvrage VALUES('A','Timide',2);
INSERT INTO Ouvrage VALUES('B','La bicyclette bleu',5);
INSERT INTO Ouvrage VALUES('C','Croc blanc',4);
INSERT INTO Ouvrage VALUES('D','Oui-Oui',9);

INSERT INTO Auteur VALUES('Bautrais','Klervia',2000-07-18,'Francais');
INSERT INTO Auteur VALUES('Davail','Malo',2005-06-29,'Francais');
INSERT INTO Auteur VALUES('Miquel','Lucie',1999-07-18,'Allemand');
INSERT INTO Auteur VALUES('Costiou','Aurelie',1980-02-23,'Belge');

INSERT INTO Abonne VALUES(1,'Lissillour','Anne',1970-03-05,'anne.lissillour@orange.fr');
INSERT INTO Abonne VALUES(2,'Adam','Michel',1968-03-09,'adam.michel@hotmail.fr');
INSERT INTO Abonne VALUES(3,'Raffin','Elise',2008-03-09,'elise.raffin@sfr.fr');
INSERT INTO Abonne VALUES(4,'Mousson','Lou',2004-03-09,'mousson.lou@etud.univ-ubs.fr');

INSERT INTO Exemplaire VALUES(1,1,2018-03-06);
INSERT INTO Exemplaire VALUES(2,4,2018-10-09);
INSERT INTO Exemplaire VALUES(3,1,2017-09-15);
INSERT INTO Exemplaire VALUES(4,2,2017-03-09);

INSERT INTO EcritPar VALUES('A','Bautrais','Klervia');
INSERT INTO EcritPar VALUES('B','Costiou','Aurelie');
INSERT INTO EcritPar VALUES('C','Davail','Malo');
INSERT INTO EcritPar VALUES('D','Costiou','Aurelie');

INSERT INTO Emprunt VALUES(1,2,2018-10-09);
INSERT INTO Emprunt VALUES(1,1,2018-10-09);
INSERT INTO Emprunt VALUES(2,3,2018-11-15);
INSERT INTO Emprunt VALUES(4,2,2018-12-07);
