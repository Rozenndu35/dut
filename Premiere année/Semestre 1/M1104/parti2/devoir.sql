/*
09/11/18 13h00
TP6_COSTIOU_rozenn.sql
Costiou Rozenn groupe 1B1
*/

--1-1-
SELECT UPPER (nomMeeting)
FROM POUIT_BDD.meeting , POUIT_BDD.participe , POUIT_BDD.athlete
WHERE noAthlete = unAthlete
AND UPPER (unMeeting) = UPPER (nomMeeting)
AND UPPER (nom) = 'MAYER'
AND UPPER (prenom) = 'KEVIN'
;

/*
UPPER(NOMMEETING)             
------------------------------
AREVA
LONDON GRAND PRIX
PRIX DE BERLIN
MEETING DE ROME
MEETING DE PONTIVY
*/

--1-2-

SELECT DISTINCT UPPER (leClub)
FROM POUIT_BDD.participe , POUIT_BDD.athlete
WHERE noAthlete = unAthlete
;

/*
UPPER(LECLUB)       
--------------------
ACR
ACN
ACC
ACB
ACV
ACS

6 lignes sélectionnées. 
*/

--1-3-
SELECT noAthlete
FROM POUIT_BDD.athlete
MINUS
SELECT unAthlete
FROM POUIT_BDD.participe
;

/*
 NOATHLETE
----------
         7
        31
        72
        97
*/


--1-4-
SELECT UPPER(A1.nom)
FROM POUIT_BDD.athlete A1
WHERE discipline =(SELECT DISTINCT discipline
                                FROM POUIT_BDD.athlete A2
                                WHERE UPPER(A2.nom) = 'LAVILENIE'
                                AND UPPER(A2.prenom) = 'RENAUD'
                                )
AND UPPER(A1.nom) != 'LAVILENIE'
;

/*
UPPER(A1.NOM)                                     
--------------------------------------------------
GASSINE
BLANCHARD
COSTIOU
MARTIN
FLEURY
HAMON

6 lignes sélectionnées. 
*/

SELECT UPPER(A1.nom) , UPPER (A1.discipline)
FROM POUIT_BDD.athlete A1
WHERE UPPER(A1.nom) != 'LAVILENIE'
INTERSECT
SELECT UPPER (A2.discipline)
FROM POUIT_BDD.athlete A2
WHERE UPPER(A2.nom) = 'LAVILENIE'
;

/*

*/

--1-5-
SELECT UPPER(nom)
FROM POUIT_BDD.athlete , POUIT_BDD.participe P1 ,POUIT_BDD.participe P2 
WHERE noAthlete = P1.unAthlete
AND noAthlete = P2.unAthlete
AND UPPER(P1.unMeeting) = 'AREVA'
AND UPPER(P2.unMeeting) = 'LONDON GRAND PRIX'
;

/*
UPPER(NOM)                                        
--------------------------------------------------
MAYER
LAVILENIE
LE FEUNTEUN
*/

--2-1-
SELECT UPPER(discipline) , UPPER(nom)
FROM POUIT_BDD.athlete 
ORDER BY discipline , nom
;
/*
29 lignes sélectionnées. 
*/

--2-2-
SELECT MAX (datem)
FROM POUIT_BDD.meeting
;

/*
MAX(DATE
--------
12/10/19
*/

--2-3-
SELECT unAthlete
FROM POUIT_BDD.participe 
WHERE unMeeting = (SELECT (nomMeeting)
                    FROM ( SELECT *
                            FROM POUIT_BDD.meeting
                             ORDER BY datem DESC
                            )
                    WHERE ROWNUM <2
                    )
;
/*
 UNATHLETE
----------
         1
         2
        11
        12
        73
        74
        75
       103

8 lignes sélectionnées. 
*/

--2-4
SELECT UPPER(nom) , COUNT(*)
FROM POUIT_BDD.athlete ,POUIT_BDD.participe 
WHERE noAthlete = unAthlete
GROUP BY UPPER(nom)
;

/*
25 lignes sélectionnées. 
*/

--2-5
SELECT UPPER(unMeeting), COUNT(*)
FROM POUIT_BDD.athlete ,POUIT_BDD.participe 
WHERE noAthlete = unAthlete
GROUP BY UPPER(unMeeting)
;

/*
UPPER(UNMEETING)                 COUNT(*)
------------------------------ ----------
MEETING DE PONTIVY                     12
LONDON GRAND PRIX                       8
AREVA                                   7
PRIX DE BERLIN                          8
MEETING DE ROME                        15
*/

--2-6
SELECT UPPER(leClub), COUNT(*)
FROM POUIT_BDD.athlete ,POUIT_BDD.participe 
WHERE noAthlete = unAthlete
GROUP BY UPPER(leClub)
;

/*
UPPER(LECLUB)          COUNT(*)
-------------------- ----------
ACR                           6
ACN                           9
ACC                           6
ACB                           6
ACV                          14
ACS                           9

6 lignes sélectionnées.
*/