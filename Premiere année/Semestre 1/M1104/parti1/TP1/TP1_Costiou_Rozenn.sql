/*
21/09/2018 à 13h00
TP 1 : premières requêtes en SQL Oracle
Rozenn Costiou Groupe 1B1
*/

--3-1-1)

DESCRIBE POUIT_BDD.etudiantinfo;

/*
NOETUDIANT NOT NULL NUMBER(4)    
NOM                 VARCHAR2(40) 
PRENOM              VARCHAR2(30) 
PROMOTION           VARCHAR2(10) 
GROUPE              VARCHAR2(2)  
*/

--3-1-2)

DESCRIBE POUIT_BDD.enseignantinfo;

/*
NOENSEIGNANT NOT NULL NUMBER(4)    
NOM                   VARCHAR2(20) 
PRENOM1               VARCHAR2(20) 
PRENOM2               VARCHAR2(20) 
*/

SELECT NoEtudiant
FROM POUIT_BDD.etudiantinfo;

/*
NOETUDIANT
----------
         1
         2
         3
         4
         5
         6
         7
         8
         9
        10
        11

NOETUDIANT
----------
        12
        13
        14
        15
        16
        17
        18
        19
        20
        21
        22

NOETUDIANT
----------
        23
        24
        25
        26
        27
        28
        29
        30
        31
        32
        33

NOETUDIANT
----------
        34
        35
        36
        37
        38
        39
        40
        41
        42
        43
        44

NOETUDIANT
----------
        45
        46
        47
        48
        49
        50
        51
        52
        53
        54
        55

NOETUDIANT
----------
        56
        57
        58
        59
        60
        61
        62
        63
        64
        65
        66

NOETUDIANT
----------
        67
        68
        69
        70
        71
        72
        73
        74
        75
        76
        77

NOETUDIANT
----------
        78
        79
        80
        81
        82
        83
        84
        85
        86
        87
        88

NOETUDIANT
----------
        89
        90
        91
        92
        93
        94
        95
        96
        97
        98
        99

NOETUDIANT
----------
       100
       101
       102
       103
       104
       105
       106
       107
       108
       109
       110

NOETUDIANT
----------
       111
       112
       113
       114
       115
       116
       117
       118
       119
       120
       121

NOETUDIANT
----------
       122
       123
       124
       125
       126
       127
       128
       129
       130
       131
       132

NOETUDIANT
----------
       133
       134
       135
       136
       137
       138
       139
       140
       141
       142
       143

NOETUDIANT
----------
       144
       145
       146
       147
       148
       149
       150
       151
       152
       153
       154

NOETUDIANT
----------
       155
       156
       157
       158
       159
       160
       161
       162
       163
       164
       165

NOETUDIANT
----------
       166
       167
       168
       169
       170
       171
       172
       173
       174
       175
       176

NOETUDIANT
----------
       177
       178
       179
       180
       181
       182
       183
       184
       185
       186
       187

NOETUDIANT
----------
       188
       189
       190
       191
       192
       193
       194
       195
       200
       201
       202

NOETUDIANT
----------
       203
       204
       205
       206
       207
       208
       209
       210
       211
       212
       213

NOETUDIANT
----------
       214
       215
       216
       217
       218

214 lignes sélectionnées
*/

 --3-1-4)
 
SELECT DISTINCT NoEnseignant
FROM POUIT_BDD.enseignantinfo;

/*
NOENSEIGNANT
------------
          12
          13
          14
          15
          16
          17
          18

18 lignes sélectionnées. 
*/

-- 3_2_1)

SELECT DISTINCT UPPER (Prenom)
From POUIT_BDD.etudiantinfo;

/*
UPPER(PRENOM)                 
------------------------------
WILLIAM
AYMERIC
PIERRE-LOUIS
NAEL
HIPPOLYTE
ERWAN
CAMILLE
ANTONIN
SEBASTIEN
MANON
AMEDEE

UPPER(PRENOM)                 
------------------------------
TOM
EMILIE
CEDRIC
LOAN
GLENN
LOIC
PETER
ELOISE
NADA
PIERRE
GWENDAL

UPPER(PRENOM)                 
------------------------------
DAMIEN
FRANCOIS
ALEXANDRE
JULES
MICKAEL
JONATHAN
SAMI
LORENZO
JEAN RHAPHAEL
BLEUENN
THOMAS

UPPER(PRENOM)                 
------------------------------
LUCAS
XAVIER
ETIENNE
REMI
BENOIT
AURELIEN
LOUIS
GREGOR
YANN
KILIAN
MAELLE

UPPER(PRENOM)                 
------------------------------
ERWANN
ALOIS
KARTOIBIDINE
MAELYS
MELVIN
MAXIME
YOANN
NICOLAS
VICTOR
YOUSSEF
THERY

UPPER(PRENOM)                 
------------------------------
RAPHAEL
LILY
ARNAUD
ALLAN
BASILE
PATRICK
ANTHONY
HÉLOISE
STEVEN
ALAN
ENORA

UPPER(PRENOM)                 
------------------------------
CORENTIN
ROMAIN
PAUL
MARTIN
VICTORIEN
MERIADEC
CLEMENT
SYLVAIN
QUENTIN
BAPTISTE
CONSTANCE

UPPER(PRENOM)                 
------------------------------
ANDY-LOUP
GURVAN
IVAN
BRICE
GAUTHIER
ESTEBAN
MATHIS
LAURENT
MIKAIL
BRIEUC
KLERVIA

UPPER(PRENOM)                 
------------------------------
ROZENN
ALEX
JOSHUA
JOLAN
JEROME
TITOUAN
PIERRE GALAAD
EWEN
YOHAN
NATHAN
HUGO

UPPER(PRENOM)                 
------------------------------
GAETAN
GILLES-ALAIN
THIBAUD
MAEL
LUDOVIC
MALO
BASTIEN
FABIEN
ADRIEN
NATHYS
FRANCOIS LOUIS

UPPER(PRENOM)                 
------------------------------
TANGUY
CLEMENCE
FLORIAN
ANTOINE
ADAM
ARMAND
SULLYVAN
CAN
ETHAN
MAELVIN
JULIETTE

UPPER(PRENOM)                 
------------------------------
THEO
GWELTAZ
MATHIEU
JULIEN
CAROLYNN
ACHILLE
CLÉMENT
ARTHUR
AMAURY
NOEWEN
FLORENT

UPPER(PRENOM)                 
------------------------------
CONSTANTIN
ALIZEE
LEO
SIMON
NOEMIE
FLORETTE
MIKA
ALEXIS
COLIN
GWENN
PIERRIG

UPPER(PRENOM)                 
------------------------------
THIERRY

144 lignes sélectionnées. 

On trouve deux fois car le UPPER ne change pas les accents et clement peut a un accent ou non
Les accent peuvent poser probléme

*/

--3-2-2)

SELECT (NoEtudiant)
FROM POUIT_BDD.etudiantinfo
WHERE UPPER (prenom)='LUCAS';

/*
NOETUDIANT
----------
        18
        70
       140
       181
*/

SELECT (NoEtudiant)
FROM POUIT_BDD.etudiantinfo
WHERE UPPER (prenom)='CLEMENT'
OR UPPER (prenom)='CLÉMENT'
OR UPPER (prenom)='CLÉMENCE'
OR UPPER (prenom)='CLEMENCE';

/*
NOETUDIANT
----------
        57
        61
        64
        67
       148
       191

6 lignes sélectionnées. 

*/

--3-2-4)

SELECT (NoEtudiant)
FROM POUIT_BDD.etudiantinfo
WHERE UPPER (prenom)='ROZENN';

/*
        37

*/

--3-2-5)

/*
Il y a personne d'autre
*/

--3-2-6)

SELECT DISTINCT UPPER (prenom)
FROM POUIT_BDD.etudiantinfo
UNION
SELECT DISTINCT UPPER (prenom1)
FROM POUIT_BDD.enseignantinfo;

/*
UPPER(PRENOM)                 
------------------------------
ACHILLE
ADAM
ADRIEN
ALAN
ALEX
ALEXANDRE
ALEXIS
ALIZEE
ALLAN
ALOIS
AMAURY

UPPER(PRENOM)                 
------------------------------
AMEDEE
ANDY-LOUP
ANTHONY
ANTOINE
ANTONIN
ARMAND
ARNAUD
ARTHUR
AURELIEN
AYMERIC
BAPTISTE

UPPER(PRENOM)                 
------------------------------
BASILE
BASTIEN
BENOIT
BLEUENN
BRICE
BRIEUC
CAMILLE
CAN
CAROLYNN
CEDRIC
CLEMENCE

UPPER(PRENOM)                 
------------------------------
CLEMENT
CLÉMENT
COLIN
CONSTANCE
CONSTANTIN
CORENTIN
DAMIEN
ELOISE
EMILIE
ENORA
ERWAN

UPPER(PRENOM)                 
------------------------------
ERWANN
ESTEBAN
ETHAN
ETIENNE
EWEN
FABIEN
FLORENT
FLORETTE
FLORIAN
FRANCOIS
FRANCOIS LOUIS

UPPER(PRENOM)                 
------------------------------
GAETAN
GAUTHIER
GILLES-ALAIN
GLENN
GREGOR
GURVAN
GWELTAZ
GWENDAL
GWENN
HELENE
HIPPOLYTE

UPPER(PRENOM)                 
------------------------------
HUGO
HÉLOISE
ISABELLE
IVAN
JEAN FRANCOIS
JEAN RHAPHAEL
JEROME
JOLAN
JONATHAN
JOSHUA
JULES

UPPER(PRENOM)                 
------------------------------
JULIEN
JULIETTE
KARTOIBIDINE
KILIAN
KLERVIA
LAURENT
LEO
LILY
LOAN
LOIC
LORENZO

UPPER(PRENOM)                 
------------------------------
LOUIS
LUCAS
LUDOVIC
MAEL
MAELLE
MAELVIN
MAELYS
MALO
MANON
MARTIN
MATHIEU

UPPER(PRENOM)                 
------------------------------
MATHIS
MATTHIEU
MAXIME
MELVIN
MERIADEC
MICHEL
MICKAEL
MIKA
MIKAIL
MURIELLE
NADA

UPPER(PRENOM)                 
------------------------------
NAEL
NATHAN
NATHYS
NICOLAS
NOEMIE
NOEWEN
PASCAL
PATRICK
PAUL
PETER
PHILIPPE

UPPER(PRENOM)                 
------------------------------
PIERRE
PIERRE GALAAD
PIERRE-LOUIS
PIERRIG
PRUNE
QUENTIN
RAPHAEL
REGIS
REMI
ROMAIN
ROZENN

UPPER(PRENOM)                 
------------------------------
SAMI
SEBASTIEN
SIMON
STEVEN
SULLYVAN
SYLVAIN
TANGUY
THEO
THERY
THIBAUD
THIERRY

UPPER(PRENOM)                 
------------------------------
THOMAS
TITOUAN
TOM
VANEA
VICTOR
VICTORIEN
WILLIAM
XAVIER
YANN
YOANN
YOHAN

UPPER(PRENOM)                 
------------------------------
YOUSSEF

155 lignes sélectionnées. 
*/

--3-2-7)
SELECT DISTINCT UPPER (prenom)
FROM POUIT_BDD.etudiantinfo
INTERSECT
SELECT DISTINCT UPPER (prenom1)
FROM POUIT_BDD.enseignantinfo;

/*
UPPER(PRENOM)                 
------------------------------
ANTHONY
FRANCOIS
NICOLAS
SEBASTIEN
XAVIER
*/

--3-2-8)

SELECT DISTINCT UPPER (prenom)
FROM POUIT_BDD.etudiantinfo
MINUS
SELECT DISTINCT UPPER (prenom1)
FROM POUIT_BDD.enseignantinfo;

/*
139 lignes sélectionnées. 
*/

--3-2-9

SELECT DISTINCT UPPER (NoEtudient)
FROM POUIT_BDD.etudiantinfo POUIT_BDD.enseignantinfo
WHERE UPPER prenom = UPPER prenom1;
