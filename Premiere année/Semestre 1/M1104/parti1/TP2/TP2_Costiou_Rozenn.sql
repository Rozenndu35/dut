/* TP2 Algébre relationnelle et SQL
    Rozenn Costiou Groupe 1B1
    28/09/18
*/
--1

SELECT DISTINCT UPPER (titre)
FROM POUIT_BDD.Film;

/*
92 lignes sélectionnées. 
*/

--2
SELECT DISTINCT UPPER (titre)
FROM POUIT_BDD.Film
WHERE anneesortie = 2017;

/*
UPPER(TITRE)                                                                    
--------------------------------------------------------------------------------
DUNKERQUE
PIRATES DES CARAIBES : LA VENGEANCE DE SALAZAR
*/

--3
SELECT DISTINCT UPPER (titre)
FROM POUIT_BDD.Film
WHERE anneeSortie BETWEEN 2016 AND 2018;

/*
UPPER(TITRE)                                                                    
--------------------------------------------------------------------------------
DUNKERQUE
PIRATES DES CARAIBES : LA VENGEANCE DE SALAZAR
CHOCOLAT
MISSION IMPOSSIBLE : FALLOUT
DIVERGENTE 3 : AU DELA DU MUR
*/

--4
SELECT DISTINCT UPPER (titre)
FROM POUIT_BDD.Film, POUIT_BDD.Personne
WHERE leRealisateur = idPersonne
AND UPPER (pays)= 'JAPON';

/*
UPPER(TITRE)                                                                    
--------------------------------------------------------------------------------
LA FORTERESSE CACHEE
LES SEPT SAMOURAIS
DERSOU OUZALA
*/

--5
SELECT DISTINCT UPPER(prenom),UPPER(nom) 
FROM POUIT_BDD.Film, POUIT_BDD.Personne
WHERE leRealisateur = idPersonne
AND UPPER (pays)= 'USA'
AND anneeSortie BETWEEN 2012 AND 2017;

/*
UPPER(PRENOM)        UPPER(NOM)          
-------------------- --------------------
NEIL                 BURGER              
WOODY                ALLEN               
JOSEPH               KOSINSKI            
GARY                 ROSS                
CHRISTOPHER          MCQUARRIE        
*/

--6
SELECT DISTINCT UPPER(titre), (anneeSortie)
FROM POUIT_BDD.Film, POUIT_BDD.Personne
WHERE leRealisateur = idPersonne
AND UPPER (prenom)= 'LUC'
AND UPPER (nom) = 'BESSON';

/*
UPPER(TITRE)                                                                     ANNEESORTIE
-------------------------------------------------------------------------------- -----------
LUCY                                                                                    2014
LES AVENTURES EXTRAORDINAIRES D ADELE BLANC SEC                                         2010
LE GRAND BLEU                                                                           1988
*/

--7

SELECT DISTINCT UPPER(nom), UPPER(prenom)
FROM POUIT_BDD.Film, POUIT_BDD.Acteur , POUIT_BDD.Personne
WHERE unFilm = code
AND idPersonne = unActeur
AND UPPER (titre) = 'HUNGER GAMES';

/*
UPPER(NOM)           UPPER(PRENOM)       
-------------------- --------------------
LAWRENCE             JENNIFER            
HUTCHERSON           JOSHUA              
HEMSWORTH            LIAM           
*/

--8

SELECT DISTINCT UPPER(nom), UPPER(prenom)
FROM POUIT_BDD.Film, POUIT_BDD.Personne
WHERE idPersonne=leRealisateur
AND UPPER (titre) LIKE '%DIVERGENTE%';

/*
UPPER(NOM)           UPPER(PRENOM)       
-------------------- --------------------
BURGER               NEIL                
SCHWENTKE            ROBERT     
*/

--9

SELECT DISTINCT UPPER(titre)
FROM POUIT_BDD.Film, POUIT_BDD.Personne
WHERE idPersonne=leRealisateur
AND UPPER (nom)= 'COLUMBUS'
AND UPPER (prenom)= 'CHRIS';

/*
UPPER(TITRE)                                                                    
--------------------------------------------------------------------------------
HARRY POTTER ET LA CHAMBRE DES SECRETS
HARRY POTTER A L ECOLE DES SORCIERS
*/

--10

SELECT DISTINCT UPPER(titre)
FROM POUIT_BDD.Film, POUIT_BDD.Acteur , POUIT_BDD.Personne
WHERE unFilm = code
AND idPersonne = unActeur
AND UPPER (prenom) = 'TOM'
AND UPPER (nom)= 'CRUISE';

/*
8 lignes sélectionnées. 
*/

--11


SELECT DISTINCT UPPER(P1.nom), UPPER(P1.prenom)
FROM POUIT_BDD.Film , POUIT_BDD.Acteur A1 , POUIT_BDD.Personne P1 , POUIT_BDD.Acteur A2 , POUIT_BDD.Personne P2
WHERE (A1.unFilm) = code
AND (A2.unFilm) = code
AND (P1.idPersonne) = (A1.unActeur)
AND (A2.unActeur)=(P2.idPersonne)
AND UPPER (P2.nom)= 'RADCLIFFE'
AND UPPER (P2.prenom)= 'DANIEL'
AND UPPER (P1.nom) != UPPER(P2.nom);

/*
UPPER(P1.NOM)        UPPER(P1.PRENOM)    
-------------------- --------------------
WATSON               EMMA                
HARRIS               RICHARD             
SMITH                MAGGIE        
*/