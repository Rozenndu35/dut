/*
05/10/18 13h00
TP3_COSTIOU_rozenn.sql
Costiou Rozenn groupe 1B1
*/

--1-

SELECT noEtudiant
FROM POUIT_BDD.etudiantinfo
WHERE UPPER (nom) LIKE '%A'
ORDER BY nom,prenom
;

/*
NOETUDIANT
----------
       142
        96
       153
        71
        76
       170
       109

7 lignes sélectionnées. 
*/

--2-

SELECT DISTINCT UPPER (nom), UPPER (prenom)
FROM POUIT_BDD.etudiantinfo
WHERE UPPER (nom) LIKE 'A%A%'
;

/*

7 lignes sélectionnées. 


UPPER(NOM)                               UPPER(PRENOM)                 
---------------------------------------- ------------------------------
ALIAMUS                                  EWEN                          
*/


--3-

SELECT noEtudiant
FROM POUIT_BDD.etudiantinfo
WHERE promotion = 'INFO2'
AND groupe IS NULL;
;

/*
aucune ligne sélectionnée
*/

--4-

SELECT DISTINCT UPPER (Et.nom)
FROM POUIT_BDD.etudiantinfo Et, POUIT_BDD.enseignantinfo Es
WHERE promotion = 'INFO1'
AND  Et.prenom=ES.prenom1
;

/*

UPPER(ET.NOM)                           
----------------------------------------
BLANCHARD
CHARPENTIER
LESCIEUX
GAVIGNET
*/

--5-

SELECT DISTINCT UPPER (Et.nom)
FROM POUIT_BDD.etudiantinfo Et, POUIT_BDD.enseignantinfo Es
WHERE noEnseignant = 7
AND  Et.prenom=ES.prenom1
;

/*
UPPER(ET.NOM)                           
----------------------------------------
BLANCHARD
*/

--6-

SELECT DISTINCT UPPER (Es.nom)
FROM POUIT_BDD.enseignantinfo Es, POUIT_BDD.enseignantinfo Es1
WHERE Es.noEnseignant = 17
AND  Es1.prenom1=ES.prenom1
AND Es.noEnseignant!=Es1.noEnseignant
;

/*
aucune ligne sélectionnée
*/

--7-

SELECT UPPER (Et.nom), UPPER (Et.prenom)
FROM POUIT_BDD.etudiantinfo Et, POUIT_BDD.etudiantinfo Et1
WHERE  Et.nom = Et1.nom
AND Et.noetudiant!=Et1.noetudiant
;

/*
UPPER(ET.NOM)                            UPPER(ET.PRENOM)              
---------------------------------------- ------------------------------
MORICE                                   PIERRE                        
ROUET                                    GWENN                         
VANDAMME                                 BENOIT                        
COLLET                                   ARNAUD                        
MORICE                                   PIERRE                        
VANDAMME                                 PAUL                          
ROUET                                    YOANN                         
COLLET                                   GWELTAZ                       

8 lignes sélectionnées.  
*/

--8-

SELECT DISTINCT UPPER (nom), UPPER (prenom1)
FROM POUIT_BDD.enseignantinfo 
MINUS 
SELECT DISTINCT UPPER (Es.nom), UPPER (Es.prenom1)
FROM POUIT_BDD.enseignantinfo Es, POUIT_BDD.enseignantinfo Es1
WHERE Es.prenom1=Es1.prenom1
AND Es.noEnseignant!=Es1.noEnseignant
;

/*
UPPER(NOM)           UPPER(PRENOM1)      
-------------------- --------------------
ADAM                 MICHEL              
BEAUDONT             PASCAL              
BORNE                ISABELLE            
CHIPRIANOV           VANEA               
FLEURQUIN            REGIS               
KAMP                 JEAN FRANCOIS       
LE LAIN              MATTHIEU            
LE SOMMER            NICOLAS             
LEFEVRE              SEBASTIEN           
LI                   PRUNE               
MANEVY               MURIELLE            

UPPER(NOM)           UPPER(PRENOM1)      
-------------------- --------------------
RIDARD               ANTHONY             
ROIRAND              XAVIER              
TUFFIGOT             HELENE              

14 lignes sélectionnées. 
*/

--9-

SELECT *
FROM (SELECT DISTINCT (prenom)
        FROM POUIT_BDD.etudiantinfo 
        ORDER BY prenom
        )
WHERE ROWNUM <6
;

/*
PRENOM                        
------------------------------
Achille
Adam
Adrien
Alan
ALEX
*/

--10-

SELECT *
FROM( SELECT *
      FROM (SELECT DISTINCT UPPER(prenom)
             FROM POUIT_BDD.etudiantinfo
             WHERE UPPER (prenom) LIKE 'S%'
             ORDER BY prenom
            )
UNION
      SELECT *
      FROM (SELECT DISTINCT UPPER (prenom1)
            FROM POUIT_BDD.enseignantinfo
            WHERE UPPER (prenom1) LIKE 'S%'
            ORDER BY prenom1
            )
     )
 WHERE ROWNUM <6
;

/*
UPPER(PRENOM)                 
------------------------------
SAMI
SEBASTIEN
SIMON
STEVEN
SULLYVAN

*/

--11-

SELECT *
FROM (SELECT *
        FROM (SELECT DISTINCT UPPER(prenom) pren
              FROM POUIT_BDD.etudiantinfo 
              ORDER BY UPPER(prenom) DESC
              )
        WHERE ROWNUM <6
        ORDER BY pren
        )
;

/*
------------------------------
XAVIER
YANN
YOANN
YOHAN
YOUSSEF
*/

--12-
SELECT DISTINCT UPPER (prenom1), COUNT(*) nombre
FROM POUIT_BDD.enseignantinfo
GROUP BY prenom1
ORDER BY nombre DESC
;

/*
UPPER(PRENOM1)           NOMBRE
-------------------- ----------
FRANCOIS                      2
PHILIPPE                      2
ANTHONY                       1
HELENE                        1
ISABELLE                      1
JEAN FRANCOIS                 1
MATTHIEU                      1
MICHEL                        1
MURIELLE                      1
NICOLAS                       1
PASCAL                        1

UPPER(PRENOM1)           NOMBRE
-------------------- ----------
PRUNE                         1
REGIS                         1
SEBASTIEN                     1
VANEA                         1
XAVIER                        1

16 lignes sélectionnées. 
*/