/*
12/10/18 13h00
TP4_COSTIOU_rozenn.sql
Costiou Rozenn groupe 1B1
*/

--1-
SELECT UPPER (nom), COUNT(unEmp)
FROM POUIT_BDD.employe,POUIT_BDD.intervention
WHERE unEmp = idEmp
GROUP BY UPPER (nom), unEmp
;

/*

UPPER(NOM)           COUNT(UNEMP)
-------------------- ------------
LE BORGNE                       9
GUILLEUX                        2
LOISEL                          6
ORVOEN                          4
LEFEBRE                         2
LEBLAY                          2
BERNARDIN                       2
CASTEL                          1
COCARD                          6
BLANCHARD                       8
FLEURY                          3

UPPER(NOM)           COUNT(UNEMP)
-------------------- ------------
HASSANI                         3
MARCHAND                        5
VADELEAU                        5
PERRUDIN                        8
BERNARDIN                      10
FERNANDEZ                       2

17 lignes sélectionnées. 
*/

--2-
SELECT AVG(nbI)
FROM (SELECT unEmp, COUNT(uneTache)nbI
        FROM POUIT_BDD.Intervention
        GROUP BY unEmp
    )
;

/*
  AVG(NBI)
----------
4,58823529
*/

--3-
SELECT dateIntervention, unEmp, COUNT(*)nbI
FROM POUIT_BDD.Intervention
GROUP BY  dateIntervention, unEmp
;

/*
75 lignes sélectionnées. 
*/

--4-
SELECT unEmp, UPPER (nom), COUNT(unClient) nbClient
FROM POUIT_BDD.Intervention,POUIT_BDD.employe
WHERE unEmp=idEmp
GROUP BY  nom, unEMP
ORDER BY nom
;
/*
     UNEMP UPPER(NOM)             NBCLIENT
---------- -------------------- ----------
         9 BERNARDIN                    10
        16 BERNARDIN                     2
         4 BLANCHARD                     8
         7 CASTEL                        1
        10 COCARD                        6
        21 FERNANDEZ                     2
        13 FLEURY                        3
        11 GUILLEUX                      2
        22 HASSANI                       3
         2 LE BORGNE                     9
         6 LEBLAY                        2

     UNEMP UPPER(NOM)             NBCLIENT
---------- -------------------- ----------
        12 LEFEBRE                       2
        14 LOISEL                        6
        15 MARCHAND                      5
         3 ORVOEN                        4
         8 PERRUDIN                      8
         1 VADELEAU                      5

17 lignes sélectionnées. 
*/

--5-
SELECT AVG(nbI)
FROM (SELECT unEmp, COUNT(DISTINCT uneTache)nbI
        FROM POUIT_BDD.Intervention
        GROUP BY unEmp
    )
;
 
 /*
   AVG(NBI)
----------
3,47058824
*/

--6-
SELECT MAX (nbT)
FROM (SELECT unEmp, COUNT(*)nbT
        FROM POUIT_BDD.intervention
        GROUP BY  unEmp
    )
;
 
/*

  MAX(NBT)
----------
        10
*/


--7-
SELECT  UPPER (nom)
FROM POUIT_BDD.intervention, POUIT_BDD.employe
WHERE unEmp=idEmp
GROUP BY  unEmp, UPPER (nom)
HAVING COUNT(DISTINCT unClient)>3
;

/*

UPPER(NOM)          
--------------------
LE BORGNE
LOISEL
ORVOEN
BLANCHARD
BERNARDIN
PERRUDIN

6 lignes sélectionnées. 
*/

SELECT *
FROM POUIT_BDD.client

;
--8-
SELECT  UPPER (nom), COUNT(*)nbT
FROM POUIT_BDD.intervention, POUIT_BDD.employe
WHERE unEmp=idEmp
AND specialite = 'expert securite'
GROUP BY  unEmp, UPPER (nom)
;
/*
UPPER(NOM)                  NBT
-------------------- ----------
ORVOEN                        4
LEBLAY                        2
VADELEAU                      5
*/

--9-
SELECT   UPPER (nom)
FROM  POUIT_BDD.intervention, POUIT_BDD.client
WHERE unClient=noClient
GROUP BY  unClient, UPPER(nom)
HAVING COUNT(uneTache)= (SELECT MAX (COUNT(uneTache))
                        FROM POUIT_BDD.intervention
                        GROUP BY unClient
                        )
;

/*
UPPER(NOM)                                        
--------------------------------------------------
CGI INFO
*/

--10-
SELECT   uneTache
FROM  POUIT_BDD.intervention
GROUP BY  uneTache
HAVING COUNT(uneTache)= (SELECT MIN (COUNT(uneTache))
                        FROM POUIT_BDD.intervention
                        GROUP BY uneTache
                        )
;
/*
  UNETACHE
----------
         5
*/

--11-
SELECT DISTINCT UPPER(nom)
FROM POUIT_BDD.intervention, POUIT_BDD.client
WHERE unClient=noClient
AND dateIntervention = (SELECT MAX (dateIntervention)
                        FROM POUIT_BDD.intervention, POUIT_BDD.client
                        )
;

/*
UPPER(NOM)                                        
--------------------------------------------------
AUCHAN
*/