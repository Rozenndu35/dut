/*
19/10/18 13h00
TP4_COSTIOU_rozenn.sql
Costiou Rozenn groupe 1B1
*/

--1-
SELECT DISTINCT UPPER (titre)
FROM POUIT_BDD.Film
WHERE duree =( SELECT MAX (duree)
            FROM POUIT_BDD.film
            )
;

/*

UPPER(TITRE)                                                                    
--------------------------------------------------------------------------------
LES SEPT NAINS


*/

--2-
SELECT DISTINCT UPPER (nom)
FROM POUIT_BDD.Film, POUIT_BDD.personne
WHERE leRealisateur = idPersonne
GROUP BY leRealisateur , UPPER (nom)
HAVING COUNT (*) =(SELECT MAX (COUNT(*))
                   FROM POUIT_BDD.Film
                   GROUP BY leRealisateur 
                   )
;

/*
UPPER(NOM)          
--------------------
GLEN
*/


--3-
SELECT DISTINCT UPPER (titre)
FROM POUIT_BDD.personne, POUIT_BDD.acteur, POUIT_BDD.film
WHERE unActeur = idPersonne
AND UPPER (nom) = 'LAWRENCE'
AND UPPER (prenom) = 'JENNIFER'
AND unFilm = code
;

/*
UPPER(TITRE)                                                                    
--------------------------------------------------------------------------------
HUNGER GAMES : LA REVOLTE PARTIE 1
HUNGER GAMES : LA REVOLTE PARTIE 2
HUNGER GAMES : L EMBRASEMENT
HUNGER GAMES
*/

--4-
SELECT DISTINCT UPPER (pays)
FROM POUIT_BDD.personne
GROUP BY UPPER (pays)
HAVING COUNT (*) =(SELECT MAX (COUNT(*))
                   FROM POUIT_BDD.Personne
                   GROUP BY UPPER (pays)
                   )
;

/*
UPPER(PAYS)         
--------------------
USA
GB
*/


--5-
SELECT (titre)
FROM ( SELECT *
        FROM POUIT_BDD.film
        ORDER BY anneeSortie DESC
        )
WHERE ROWNUM <6
;

/*
TITRE                                                                           
--------------------------------------------------------------------------------
Mission impossible : FALLOUT
Dunkerque
Pirates des Caraibes : La Vengeance de Salazar
Divergente 3 : Au dela du mur
Chocolat
*/

--6-
SELECT DISTINCT UPPER (nom)
FROM POUIT_BDD.personne , POUIT_BDD.acteur, POUIT_BDD.film
WHERE idPersonne = unActeur
AND unFilm = code
AND anneeSortie = 2018
;

/*
UPPER(NOM)          
--------------------
CRUISE
RHAMES
MOHNAGAN
PEGG
*/

--8-
CREATE VIEW ActeurReal
(unRealisateur, nomR, unActeur, nomAct,titre)
AS SELECT leRealisateur,P1.nom,  unActeur, P2.nom,titre
FROM POUIT_BDD.personne P1, POUIT_BDD.personne P2, POUIT_BDD.acteur, POUIT_BDD.film
WHERE leRealisateur= P1.idPersonne
AND unActeur = P2.idPersonne
AND unFilm = code
;
SELECT UPPER (nomAct)
FROM ActeurReal
WHERE UPPER (nomR) ='BESSON'
AND UPPER (nomAct) != 'BESSON'
;

/*
UPPER(NOMACT)       
--------------------
TIPTON
FREEMAN
JOHANSSON
*/

--8-
SELECT UPPER (titre)
FROM ActeurReal
WHERE UPPER (nomR) = UPPER (nomAct)
;

/*
UPPER(TITRE)                                                                    
--------------------------------------------------------------------------------
LA FORTERESSE CACHEE
80 CHASSEURS
LA DENT BLEUE
BUCK
TO ROME WITH LOVE
LE GRAND BLEU
HUNGER GAMES : LA REVOLTE PARTIE 2
HUNGER GAMES : LA REVOLTE PARTIE 1
HUNGER GAMES : L EMBRASEMENT

9 lignes sélectionnées. 

*/

DROP VIEW ActeurReal
;

--9- 

SELECT DISTINCT UPPER (nom)
FROM POUIT_BDD.film, POUIT_BDD.acteur, POUIT_BDD.personne
WHERE leRealisateur = unActeur


;