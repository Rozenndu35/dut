DROP TABLE Ticket;
DROP TABLE Participe;
DROP TABLE Concert;
DROP TABLE Client;
DROP TABLE Festival;
DROP TABLE Artiste;

CREATE TABLE Artiste (
				noArtiste NUMBER(3)
				CONSTRAINT pkArtiste PRIMARY KEY,
				nomA VARCHAR2(30),
				prenomA VARCHAR2(30) 
);

CREATE TABLE Festival (
				nomFestival VARCHAR2(30)
				CONSTRAINT pkFestival PRIMARY KEY,
				ville VARCHAR2(30) 
);

CREATE TABLE Client (
				noClient NUMBER(5)
				CONSTRAINT pkClient PRIMARY KEY,
				nomC VARCHAR2(30),
				adresse VARCHAR2(60),
				email VARCHAR2(30) 
);

CREATE TABLE Concert (
				noConcert NUMBER(3)
				CONSTRAINT pkConcert PRIMARY KEY,
				leFestival VARCHAR2(30)
				CONSTRAINT fkConcertFestival REFERENCES Festival(nomFestival)
				CONSTRAINT nnLeFestival NOT NULL,
				dateC DATE,
				prix NUMBER(2) 
);

CREATE TABLE Participe (
				unConcert NUMBER(3)
				CONSTRAINT fkParticipeConcert REFERENCES Concert(noConcert),
				unArtiste NUMBER(3)
				CONSTRAINT fkParticipeArtiste REFERENCES Artiste(noArtiste),
				CONSTRAINT pkParticipe PRIMARY KEY (unConcert,unArtiste)
);

CREATE TABLE Ticket (
				unConcert NUMBER(3)
				CONSTRAINT fkTicketConcert REFERENCES Concert(noConcert),
				unClient NUMBER(5)
				CONSTRAINT fkTicketClient REFERENCES Client(noClient),
				CONSTRAINT pkTicket PRIMARY KEY (unConcert,unClient)
);

INSERT ALL
	INTO Artiste VALUES (1,'Jain','Jain')
	INTO Artiste VALUES (2,'Aubert','Jean Louis')
	INTO Artiste VALUES (3,'Daft punk','Daft punk')
	INTO Artiste VALUES (4,'Guetta','David')
	INTO Artiste VALUES (5,'Solveig','Martin')
	INTO Artiste VALUES (6,'Gregoire','Gregoire')
	INTO Artiste VALUES (7,'coeur de pirate','coeur de pirate')
	INTO Artiste VALUES (8,'mc solaar','mc solaar')
	INTO Artiste VALUES (9,'tremonti','tremonti')
	INTO Artiste VALUES (10,'Red Hot CHILI peppers','Red Hot CHILI peppers')
	INTO Artiste VALUES (11,'Linkin Park','Linkin Park')
SELECT * FROM DUAL;
	
INSERT ALL
	INTO Festival VALUES ('Les vieilles charrues','Carhaix')
	INTO Festival VALUES ('Les nuits de Fourviere','Lyon')
	INTO Festival VALUES ('Le printemps de Bourges','Bourges')
	INTO Festival VALUES ('Rencontres trans musicales','Rennes')
SELECT * FROM DUAL;

INSERT ALL
	INTO Client VALUES(1,'ORVOEN','6 rue des violettes 56000 Vannes','ORVOEN@INFO1.FR')
	INTO Client VALUES(2,'POULNAIS','42 rue du capitaine Hadock 22000 Saint Brieuc','POULNAIS@INFO1.fr')
	INTO Client VALUES(3,'QUERE','18 rue tintin 56000 Lorient','QUERE@INFO1.fr')
	INTO Client VALUES(4,'ROBILLIARD','45 squarre des Roses 35000 Rennes','ROBILLIARD@INFO1.FR')
	INTO Client VALUES(5,'ROUET','18 rue de la galette 29000 Quimper','ROUET@INFO1.fr')
	INTO Client VALUES(6,'SOURGET','3 rue des chenes 56000 Vannes','SOURGET@INFO1.fr')
	INTO Client VALUES(7,'VADELEAU','12 av des bouleaux 35000 Rennes','VADELEAU@INFO1.fr')
	INTO Client VALUES(8,'VIVION','5 imp de la marine 29000 Brest','VIVION@INFO1.fr')
	INTO Client VALUES(9,'VOLMIER','8 rue de la soif 56000 Vannes','VOLMIER@INFO1.fr')
	INTO Client VALUES(10,'WEINREICH','32 rue Emile Zola 29000 Brest','WEINREICH@INFO1.fr')
	INTO Client VALUES(11,'PAUTREL','89 av Lamartine 35000 Rennes','PAUTREL@INFO1.fr')
	INTO Client VALUES(12,'ROBERT','99 av de la republique 35000 Rennes','ROBERT@INFO1.fr')
	INTO Client VALUES(13,'NAQUET','1 square des Lys 56000 Vannes','NAQUET@INFO1.fr')
	INTO Client VALUES(14,'PESSEL','11 rue zouzou 56000 Vannes','PESSEL@INFO1.fr')
	INTO Client VALUES(15,'PIAU','2 rue des bouleaux 56000 Vannes','Piau@INFO.fr')
	INTO Client VALUES(16,'GUYADER','2 rue des chenes 56000 Pontivy','Guyader@INFO2.fr')
	INTO Client VALUES(17,'CARIO','8 rue du lys 56000 Sene','Cario@INFO2.fr')
	INTO Client VALUES(18,'MOINEREAU','22 rue victor Hugo 56000 Lorient','Moinereau@INFO2.fr')
	INTO Client VALUES(19,'PERDEREAU','23 rue Lamartine 56000 Vannes','Perdereau@INFO2.fr')
SELECT * FROM DUAL;

INSERT ALL
	INTO Concert VALUES(1,'Les vieilles charrues',TO_DATE('10/07/2019','DD/MM/YYYY'),32)
	INTO Concert VALUES(2,'Les vieilles charrues',TO_DATE('11/07/2019','DD/MM/YYYY'),30)
	INTO Concert VALUES(3,'Les vieilles charrues',TO_DATE('12/07/2019','DD/MM/YYYY'),40)
	INTO Concert VALUES(4,'Les nuits de Fourviere',TO_DATE('17/06/2019','DD/MM/YYYY'),20)
	INTO Concert VALUES(5,'Les nuits de Fourviere',TO_DATE('18/06/2019','DD/MM/YYYY'),25)
	INTO Concert VALUES(6,'Les nuits de Fourviere',TO_DATE('19/06/2019','DD/MM/YYYY'),25)
	INTO Concert VALUES(7,'Le printemps de Bourges',TO_DATE('18/04/2019','DD/MM/YYYY'),15)
	INTO Concert VALUES(8,'Le printemps de Bourges',TO_DATE('19/04/2019','DD/MM/YYYY'),15)
	INTO Concert VALUES(9,'Rencontres trans musicales',TO_DATE('04/12/2019','DD/MM/YYYY'),15)
	INTO Concert VALUES(10,'Rencontres trans musicales',TO_DATE('05/12/2019','DD/MM/YYYY'),15)
SELECT * FROM DUAL;

INSERT ALL
	INTO Participe VALUES(1,1)
	INTO Participe VALUES(1,2)
	INTO Participe VALUES(1,3)
	INTO Participe VALUES(2,4)
	INTO Participe VALUES(3,5)
	INTO Participe VALUES(3,8)
	INTO Participe VALUES(3,9)
	
	INTO Participe VALUES(4,4)
	INTO Participe VALUES(4,5)
	INTO Participe VALUES(5,8)
	INTO Participe VALUES(5,9)
	INTO Participe VALUES(6,3)
	
	INTO Participe VALUES(7,1)
	INTO Participe VALUES(7,3)
	INTO Participe VALUES(7,8)
	INTO Participe VALUES(8,1)
	INTO Participe VALUES(8,9)
	INTO Participe VALUES(8,10)
	
	INTO Participe VALUES(9,8)
	INTO Participe VALUES(9,5)
	INTO Participe VALUES(9,9)
	INTO Participe VALUES(10,3)
	INTO Participe VALUES(10,11)
SELECT * FROM DUAL;

INSERT ALL
	INTO Ticket VALUES(1,1)
	INTO Ticket VALUES(1,2)
	INTO Ticket VALUES(1,3)
	INTO Ticket VALUES(1,4)
	INTO Ticket VALUES(1,5)
	INTO Ticket VALUES(1,6)
	INTO Ticket VALUES(1,7)
	INTO Ticket VALUES(2,8)
	INTO Ticket VALUES(2,9)
	INTO Ticket VALUES(2,10)
	INTO Ticket VALUES(2,11)
	INTO Ticket VALUES(2,12)
	INTO Ticket VALUES(2,13)
	INTO Ticket VALUES(2,14)
	INTO Ticket VALUES(2,15)
	INTO Ticket VALUES(2,16)
	INTO Ticket VALUES(3,8)
	INTO Ticket VALUES(3,9)
	INTO Ticket VALUES(3,10)
	INTO Ticket VALUES(3,11)
	INTO Ticket VALUES(3,12)
	INTO Ticket VALUES(3,13)
	INTO Ticket VALUES(3,14)
	INTO Ticket VALUES(3,15)
	INTO Ticket VALUES(3,16)
	INTO Ticket VALUES(3,17)
	INTO Ticket VALUES(3,18)
	INTO Ticket VALUES(3,19)
	INTO Ticket VALUES(4,1)
	INTO Ticket VALUES(4,2)
	INTO Ticket VALUES(4,3)
	INTO Ticket VALUES(4,4)
	INTO Ticket VALUES(4,5)
	INTO Ticket VALUES(4,6)
	INTO Ticket VALUES(4,14)
	INTO Ticket VALUES(4,15)
	INTO Ticket VALUES(4,16)
	INTO Ticket VALUES(4,17)
	INTO Ticket VALUES(4,18)
	INTO Ticket VALUES(4,19)
	INTO Ticket VALUES(5,8)
	INTO Ticket VALUES(5,9)
	INTO Ticket VALUES(5,10)
	INTO Ticket VALUES(5,11)
	INTO Ticket VALUES(5,12)
	INTO Ticket VALUES(5,13)
	INTO Ticket VALUES(5,14)
	INTO Ticket VALUES(5,15)
	INTO Ticket VALUES(5,16)
	INTO Ticket VALUES(6,1)
	INTO Ticket VALUES(6,2)
	INTO Ticket VALUES(6,3)
	INTO Ticket VALUES(6,4)
	INTO Ticket VALUES(6,5)
	INTO Ticket VALUES(6,6)
	INTO Ticket VALUES(6,7)
	INTO Ticket VALUES(7,8)
	INTO Ticket VALUES(7,9)
	INTO Ticket VALUES(7,10)
	INTO Ticket VALUES(7,11)
	INTO Ticket VALUES(7,12)
	INTO Ticket VALUES(7,13)
	INTO Ticket VALUES(7,14)
	INTO Ticket VALUES(7,15)
	INTO Ticket VALUES(7,16)
	INTO Ticket VALUES(8,8)
	INTO Ticket VALUES(8,9)
	INTO Ticket VALUES(8,10)
	INTO Ticket VALUES(8,11)
	INTO Ticket VALUES(8,12)
	INTO Ticket VALUES(8,13)
	INTO Ticket VALUES(8,14)
	INTO Ticket VALUES(8,15)
	INTO Ticket VALUES(8,16)
	INTO Ticket VALUES(8,17)
	INTO Ticket VALUES(8,18)
	INTO Ticket VALUES(8,19)
	INTO Ticket VALUES(9,1)
	INTO Ticket VALUES(9,2)
	INTO Ticket VALUES(9,3)
	INTO Ticket VALUES(9,4)
	INTO Ticket VALUES(9,5)
	INTO Ticket VALUES(9,6)
	INTO Ticket VALUES(9,14)
	INTO Ticket VALUES(9,15)
	INTO Ticket VALUES(9,16)
	INTO Ticket VALUES(9,17)
	INTO Ticket VALUES(9,18)
	INTO Ticket VALUES(9,19)
	INTO Ticket VALUES(10,8)
	INTO Ticket VALUES(10,9)
	INTO Ticket VALUES(10,10)
	INTO Ticket VALUES(10,11)
	INTO Ticket VALUES(10,12)
	INTO Ticket VALUES(10,13)
	INTO Ticket VALUES(10,14)
	INTO Ticket VALUES(10,15)
	INTO Ticket VALUES(10,16)
SELECT * FROM DUAL;

--1-
SELECT unArtiste
FROM Participe, Concert
WHERE unConcert = noConcert
AND leFestival = 'Les vieilles charrues'
;
 /**
  UNARTISTE
----------
         1
         2
         3
         4
         5
         8
         9
         
7 lignes sélectionnées. 
*/

--2-
SELECT leFestival
FROM Artiste, Participe, Concert
WHERE UPPER (nomA) = 'JAIN'
AND noArtiste = unArtiste
AND unConcert = noConcert
;

/**
LEFESTIVAL                    
------------------------------
Les vieilles charrues
Le printemps de Bourges
Le printemps de Bourges
*/

--3-
SELECT noArtiste
FROM Artiste
MINUS
SELECT unArtiste
FROM Participe
;
/**
LEFESTIVAL                    
------------------------------
Les vieilles charrues
Le printemps de Bourges
Le printemps de Bourges
*/

--4-
SELECT DISTINCT UPPER (nomA)
FROM Artiste,Participe,Concert
WHERE unArtiste = noArtiste
AND unConcert = noConcert
AND UPPER (nomA)!= 'JAIN'
AND UPPER (leFestival) in (SELECT UPPER (leFestival)
                    FROM Participe , Artiste, Concert
                    WHERE unArtiste = noArtiste
                    AND unConcert = noConcert
                    AND UPPER (nomA) = 'JAIN'
                          )

;

/**
UPPER(NOMA)                   
------------------------------
GUETTA
RED HOT CHILI PEPPERS
DAFT PUNK
SOLVEIG
AUBERT
MC SOLAAR
TREMONTI

7 lignes sélectionnées. 

*/

--1-

SELECT UPPER (nomA), UPPER (leFestival)
FROM Artiste, Participe, Concert
WHERE noArtiste = unArtiste
AND unConcert = noConcert
ORDER BY leFestival, nomA
;

/**
23 lignes sélectionnées. 

*/

--2- 
SELECT MAX (dateC)
FROM Concert
WHERE UPPER (leFestival) = 'LES VIEILLES CHARRUES'
;

/**

MAX(DATE
--------
12/07/19
*/

--3-
SELECT nomA
FROM Artiste , Participe , Concert
WHERE noConcert = unConcert
AND unArtiste = noArtiste
AND UPPER (leFestival) = 'LES VIEILLES CHARRUES'
AND dateC = ( SELECT MAX (dateC)
                    FROM Concert
                    WHERE UPPER (leFestival) = 'LES VIEILLES CHARRUES'
                )
; 

/**
NOMA                          
------------------------------
Solveig
mc solaar
tremonti
*/

--4-
SELECT DISTINCT UPPER (nomA) , COUNT(*)
FROM Artiste , Participe 
WHERE unArtiste = noArtiste
GROUP BY UPPER (nomA)
;
/**
UPPER(NOMA)                      COUNT(*)
------------------------------ ----------
LINKIN PARK                             1
JAIN                                    3
GUETTA                                  2
RED HOT CHILI PEPPERS                   1
DAFT PUNK                               4
SOLVEIG                                 3
AUBERT                                  1
MC SOLAAR                               4
TREMONTI                                4

9 lignes sélectionnées. 
*/

--1-
SELECT DISTINCT UPPER (nomA)
FROM Participe, Artiste , Concert
WHERE noArtiste = unArtiste
AND noConcert = unConcert
AND UPPER (leFestival) = 'LES VIEILLES CHARRUES'
INTERSECT
SELECT DISTINCT UPPER (nomA)
FROM Participe, Artiste , Concert
WHERE noArtiste = unArtiste
AND noConcert = unConcert
AND UPPER (leFestival) = 'LES NUITS DE FOURVIERE'
;
/**
UPPER(NOMA)                   
------------------------------
DAFT PUNK
GUETTA
MC SOLAAR
SOLVEIG
TREMONTI
*/

--2-
SELECT DISTINCT unArtiste
FROM Participe , Concert
WHERE unConcert =  noConcert
AND NOT EXISTS (SELECT DISTINCT nomFestival
                FROM Festival
                MINUS
                SELECT DISTINCT leFestival
                FROM Concert , Participe
                WHERE unConcert = noConcert
                )
;

/*


 UNARTISTE
----------
         1
        11
         2
         4
         5
         8
         3
         9
        10

9 lignes sélectionnées. 
*/

--3-
SELECT leFestival 
FROM Concert, Ticket
WHERE noConcert = unConcert
GROUP BY leFestival
HAVING COUNT (*) = (SELECT MAX (nbClient)
                    FROM (SELECT leFestival, COUNT (*) AS nbClient 
                            FROM Concert, Ticket
                            WHERE noConcert = unConcert
                            GROUP BY leFestival
                            )
                    )
;

/**

LEFESTIVAL                    
------------------------------
Les nuits de Fourviere
Les vieilles charrues
*/

--4-
SELECT leFestival
FROM Concert , Ticket
WHERE noConcert = unConcert
GROUP BY leFestival
HAVING SUM(prix)  = (SELECT MAX(SUM (prix))
                        FROM Concert , Ticket
                        WHERE noConcert = unConcert
                        GROUP BY leFestival
                    )
;
/**

LEFESTIVAL                    
------------------------------
Les vieilles charrues
*/
