/**
 * Trie sur des tableaux
*/

public class TrisTableau{
	 /**
	 * Le point d'entré du programme.
	 */
	 SimplesTableau monSpleTab = new SimplesTableau ();
	 long cpt;
	 
	void principal (){
		/**
		System.out.println("Ca marche");
		System.out.println(" ");
		
		testRemplacerAuHasard();
		System.out.println(" ");
		
		testRechercherSeq();
		System.out.println(" ");

		testRechercherSeqEfficacite();

		testRechercherDicho () ;
		System.out.println(" ");

		testRechercherDichoEfficacite();

		testVerifTri ();
		System.out.println(" ");
		
		testTrieSimple();
		System.out.println(" ");
		 			
		testTriSimpleEfficacite();

		testSeparer();
		System.out.println(" ");

		testTriRapideRec();
		System.out.println(" ");
		
		testTriRapide();
		System.out.println(" ");
		
		testTriRapideEfficacite();
			*/	
		testCreerTabFreq();
		System.out.println(" ");

		testTriParComptageFreq();
		System.out.println(" ");

		testTriParComptageFreqEfficacite();		
		/**
		testTriABulles();
		System.out.println(" ");
		
		testTriABullesEfficacite();
*/
	}
	/**
	 * Remplace dans le tableau passé en parametre ue case tirée au hasard entre 0 et (nbElem-1) par une nouvelle valeur"newInt". On suppose que le tableau passé en parametre est créé et posséde au moins une valeur  (nbElem &gt; 0).
	 * @param leTab : le tableau dont 1 case sera remplacer
	 * @param nbElem : le nombre d'éléments 
	 * @param newInt : le nouvel entier à placer au hasard dans le tableau
	 */
	void remplacerAuHasard (int[] leTab, int nbElem, int newInt){
		int i ;
		i = monSpleTab.tirerAleatoire(0, (nbElem-1));
		leTab[i] = newInt;
		
	}
	/**
	 * Test de la methode remplacerAuHasard ()
	 */
	void testRemplacerAuHasard(){
		int [] tab;
		int nb;
		int val;
		int i;
		
		System.out.println("test de remplacerAuHasard () pour les cas normaux");
		 
		 System.out.println("Pour un tableau ok");
		 tab= new int [5];
		 nb = 4;
		 val = 5;
		 i=0;
		 remplacerAuHasard (tab, nb, val);
		 while (i < nb){
			  System.out.print( tab[i] +" ");
			  i++;
		  }
		 System.out.println("si la valeur 5 une fois  test OK");
		 
		 System.out.println("test de remplacerAuHasard () pour les cas limite");
		 System.out.println("Pour un tableau ok");
		 tab = new int [5];
		 nb = 1;
		 val = 5;
		 i = 0;
		 remplacerAuHasard (tab, nb, val);
		 while (i < nb){
			  System.out.print( tab[i] +" ");
			  i++;
		  }
		 System.out.println("si la valeur 5 une fois  test OK");
	 }
	/**
	 * Recherche séquentielle d'une valeur dans un tableau. La valeur à rechercher peut exister en plusieur exemplaires mais la recherche s'arrête dés qu'une premiere valeur est trouvé.
	 * @param leTab : le tableau dans lequel effectuerla recherche
	 * @param nbElem : le nombre d'entiers que contient le tableau
	 * @param aRech : l'entier à rechercher dans le tableau 
	 * @return l'indice (&gt;= 0) de la position de l'entier dans le tableau ou -1 s'il n'est pas présent
	 */
	 int rechercherSeq(int[] leTab, int nbElem, int aRech){
		 int i;
		 boolean res;
		 int ren;
		 i=0;
		 ren =-1;
		 res = false;
		 while ((i < nbElem) && (res == false)){
			 if (leTab[i] == aRech){
				 res = true;
				 ren = i;
			 }
			 cpt++;
		     i++;
		 }
		 
		 return ren;
	 }
	 /**
	 * Test de la methode rechercherSeq()
	 */
	 void testRechercherSeq(){
		int [] tab;
		int nb;
		int val;
		int i;
		int res;
		
		System.out.println("test de rechercherSeq() pour les cas normaux");
		 
		 System.out.println("Pour un tableau ok et valeur inexistante");
		 tab= new int [5];
		 nb = 4;
		 tab[0]= 1;
		 tab[1]= 2;
		 tab[2]= 4;
		 tab[3]= 5;
		 val = 10;
		 res= rechercherSeq( tab, nb, val);
		 if (res == -1){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("Pour un tableau ok et valeur  presente");
		 tab= new int [5];
		 nb = 4;
		 tab[0]= 1;
		 tab[1]= 10;
		 tab[2]= 4;
		 tab[3]= 5;
		 val = 10;
		 res= rechercherSeq( tab, nb, val);
		 if (res == 1){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("erreur");
		 }
		 
		 System.out.println("test de rechercherSeq() pour les cas limite");
		 System.out.println("Pour un tableau ok et valeur presente");
		 tab = new int [5];
		 nb = 1;
		 tab[0]= 10;
		 val = 10;
		 res= rechercherSeq( tab, nb, val);
		 if (res == 0){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("erreur");
		 }
		 
		 System.out.println("Pour un tableau ok et valeur presente");
		 tab = new int [5];
		 nb = 1;
		 tab[0]= 10;
		 val = 5;
		 res= rechercherSeq( tab, nb, val);
		 if (res == -1){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("erreur");
		 }
	 }
	  /**
	  * recherche de l'eficacite de rechercherSeq
	  */
	  void testRechercherSeqEfficacite(){
		  int [] tab;
		  int n;
		  int indice;
		  long t1;
		  long t2;
		  long diffT;
		  int rech;
		  rech =5;
		  
		  n= 1000;
		  System.out.println("efficacite de RechercherSeq");
		  for (int i = 1 ; i<= 5 ; i++){
			  tab = new int[n];
			  monSpleTab.remplirAleatoire( tab, n, 0, 10000);
			  cpt = 0;
			  
			  t1 = System.nanoTime();
			  rechercherSeq(tab, n, rech);
			  t2 = System.nanoTime();
			  diffT= (t2 - t1);
			  
			  System.out.println("Tps= " + diffT + "ns");
			  System.out.println("cpt/n=" + (double) cpt/n + "constant");
			  
			  n=n*2;
		  }
		  
	  }
	 /**
	 * Recherche dichotomique d'une valeur dans un tableau. On suppose que le tableau est trié par ordre croissant. La valeur à rechercher peut exister en plusieurs exemplaires, dans ce cas, c'est la valeur à l'indice le + faible qui sera trouvé.
	 * @param leTab : le tableau trié par ordre croissant dans lequel effectuer la recherche
	 * @param nbElem : le nombre d'entiers que contient le tableau
	 * @param aRech : l'entier à rechercher dans le tableau
	 * @return l'indice (&gt;=0) de la position de l'entier dans le tableau ou -1 s'il n'est pas présent
	 */
	 int rechercherDicho(int[] leTab, int nbElem, int aRech){
		 int indD;
		 int indM;
		 int indF;
		 int ret;
		 indD = 0;
		 indF = nbElem - 1;
		 while (indD !=indF){
			 cpt ++;
			 indM = (int) ((indD + indF)/2);
			 if (aRech > leTab[indM]){
				 indD = indM + 1;
			 }
			 else {
				 indF=indM;
			 }
		 }
		 if (aRech == leTab [indD]) {
			 ret = indD;
		 }
		 else {
			 ret = -1;
		 }
		 return ret;
	 }
	 /**
	 * Test de la methode rechercherDicho()
	 */
	 void testRechercherDicho () {
		 int [] tab;
		int nb;
		int val;
		int i;
		int res;
		
		System.out.println("test de rechercherDicho() pour les cas normaux");
		 
		 System.out.println("Pour un tableau ok et valeur inexistante");
		 tab= new int [5];
		 nb = 4;
		 tab[0]= 1;
		 tab[1]= 2;
		 tab[2]= 4;
		 tab[3]= 5;
		 val = 10;
		 res= rechercherDicho( tab, nb, val);
		 if (res == -1){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("Pour un tableau ok et valeur  presente");
		 tab= new int [5];
		 nb = 4;
		 tab[0]= 1;
		 tab[1]= 10;
		 tab[2]= 4;
		 tab[3]= 5;
		 val = 10;
		 res= rechercherDicho( tab, nb, val);
		 if (res == 1){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("erreur");
		 }
		 
		 System.out.println("test de rechercherDicho() pour les cas limite");
		 System.out.println("Pour un tableau ok et valeur presente");
		 tab = new int [5];
		 nb = 1;
		 tab[0]= 10;
		 val = 10;
		 res= rechercherDicho( tab, nb, val);
		 if (res == 0){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("erreur");
		 }
		 
		 System.out.println("Pour un tableau ok et valeur presente");
		 tab = new int [5];
		 nb = 1;
		 tab[0]= 10;
		 val = 5;
		 res= rechercherDicho( tab, nb, val);
		 if (res == -1){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("erreur");
		 }
	 }
	  /**
	  * recherche de l'eficacite de rechercherDicho
	  */
	  void testRechercherDichoEfficacite(){
		  int [] tab;
		  int n;
		  int indice;
		  long t1;
		  long t2;
		  long diffT;
		  double log2n;
		  int val;
		  
		  val=5;
		  n= 1000;
		  
		  System.out.println("efficacite de RechercherDicho");
		  for (int i = 1 ; i<= 5; i++){
			  tab = new int[n];
			  monSpleTab.remplirAleatoire( tab, n, 0, 10000);
			  cpt = 0;
			  
			  t1 = System.nanoTime();
			  rechercherDicho(tab, n, val);
			  t2 = System.nanoTime();
			  diffT= (t2 - t1);
			  
			  System.out.println("Tps= " + diffT + "ns");
			  log2n= Math.log10(n)/Math.log10(2);
			  System.out.println("cpt/log2n=" + (double) cpt/log2n + "costant");
			  n=n*2;
		  }
	  }
	 /**
	  * Vérifie que le tableau passé en paramètre est trié par ordre croissant des valeurs.
	  * @param leTab : le tableau à vérifier (trié en ordre croissant)
	  * @param nbElem : le nombre d'entiers présents dans le tableau
	  * @return true si le tableau est trié
	  */
	  boolean verifTri(int[] leTab, int nbElem){
		  boolean ret;
		  int i;
		  i=0;
		  ret = true;
		  while (( i<nbElem - 1) && (ret == true)){
			  if (leTab[i] > leTab[i+1]){
				  ret = false;
			  }
			  i++;
		  }
		  return ret;
	  }
	  /**
	   * Test de la methode verifTri()
	   */
	    void testVerifTri () {
		int [] tab;
		int nb;
		int j;
		boolean res;
		
		System.out.println("test de verifTri() pour les cas normaux");
		 
		 System.out.println("Pour un tableau ok et trier");
		 tab= new int [5];
		 nb = 4;
		 j=0;
		 while (j < nb){
			 tab[j]= j;
			 j++;
		 }
		 res=  verifTri(tab,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("Pour un tableau ok et non trier");
		 tab= new int [5];
		 nb = 4;
		 j=nb-1;
		 while (j >= 0){
			 tab[j]= nb-j;
			 j--;
		 }
		 res=  verifTri(tab,nb);
		 if (res == false){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("Pour un tableau ok et une valeur repeter");
		 tab= new int [5];
		 nb = 4;
		 j=0;
		 while (j < nb){
			 tab[j]= 5;
			 j++;
		 }
		 res=  verifTri(tab,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("test de verifTri() pour les cas limite");
		 System.out.println("Pour un tableau ok et valeur presente");
		 tab = new int [5];
		 nb = 1;
		 tab[0]= 10;
		 res=  verifTri(tab,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("erreur");
		 }
	 }
	 /**
	  * Tri par ordre croissant d'un tableau selon la méthode simple ; l'élément minimum est placé en début de tableau (efficacité en n carré).
	  * @param leTab : le tableau à trier par ordre croissant
	  * @param nbElem :le nombre d'entiers que contient le tableau
	  */
	  void triSimple(int[] leTab, int nbElem){
		int i;
		int k; 
		int p;
		int min;
		for (i=0; i<= (nbElem-2);i++){
			min = leTab[i];
			k=i;
			for ( p = (i+1); p<=(nbElem-1);p++){
				cpt ++;
				if (leTab[p] < min){
					min = leTab[p];
					k=p;
				}
			}
			monSpleTab.echange ( leTab , nbElem , k , i );
		}
	} 
	 /**
	 * Test de la methode triSimple()
	 */
	 void testTrieSimple() {
		int [] tab;
		int nb;
		int j;
		boolean res;
		
		System.out.println("test de triSimple() pour les cas normaux");
		 
		 System.out.println("Pour un tableau ok et trier");
		 tab= new int [5];
		 nb =5;
		 j=0;
		 while (j < nb){
			 tab[j]= j;
			 j++;
		 }
		 triSimple(tab, nb);
		 res=  verifTri(tab,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("Pour un tableau ok et non trier");
		 tab= new int [5];
		 nb = 4;
		 j=nb-1;
		 while (j >= 0){
			 tab[j]= nb-j;
			 j--;
		 }
		 triSimple(tab, nb);
		 res=  verifTri(tab,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("Pour un tableau ok et une valeur repeter");
		 tab= new int [5];
		 nb = 4;
		 j=0;
		 while (j < nb){
			 tab[j]= 5;
			 j++;
		 }
		 triSimple(tab, nb);
		 res=  verifTri(tab,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("test de triSimple() pour les cas limite");
		 System.out.println("Pour un tableau ok");
		 tab = new int [5];
		 nb = 1;
		 tab[0]= 10;
		 triSimple(tab, nb);
		 res=  verifTri(tab,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("erreur");
		 }
	 }
	  /**
	  * recherche de l'eficacite de triSimple()
	  */
	  void testTriSimpleEfficacite(){
		  int [] tab;
		  int n;
		  int indice;
		  long t1;
		  long t2;
		  long diffT;
		  
		  n= 1000;
		  System.out.println("efficacite de TriSimple");
		  for (int i = 1 ; i<= 5 ; i++){
			  tab = new int[n];
			  monSpleTab.remplirAleatoire( tab, n, 0, 10000);
			  cpt = 0;
			  
			  t1 = System.currentTimeMillis();
			  triSimple(tab, n);
			  t2 = System.currentTimeMillis();
			  diffT= (t2 - t1);
			  
			  System.out.println("Tps= " + diffT + "ms");
			  System.out.println("cpt/n²=" + (double) cpt/Math.pow(n, 2)  + "constant");
			  n=n*2;
		  }
	  }
	  /**
	   * Cette méthode renvoie l’indice de séparation du tableau en 2 parties par placement du pivot à la bonne case.
	   * @param tab : le tableau des valeurs
	   * @param indR : indice Right de fin de tableau
	   * @param indL : indice Left de début de tableau
	   * @return l’indice de séparation du tableau
	   */ 
	  int separer(int[] tab, int indL, int indR){
		  int pivot;
		  int ret;
		  int nb;
		  nb= indR +1;
		  ret =indL;
		  pivot = tab[indL];
		  while (tab [indR] != tab [indL]){
			  while (tab[indR] > pivot){
				  indR--;
				  cpt++;
			  }
			  if (tab[indR] < pivot){
				  monSpleTab.echange ( tab , nb , indR , indL );
			  }
			  while (tab[indL]<pivot){
					  indL++;
					  cpt++;
			  }
			  if (tab[indL] > pivot){
				  monSpleTab.echange ( tab , nb, indR , indL );
			  }		  
		  }
		  ret = indL;
		  return ret;
	  }
	   /**
	   * Test de la methode separer()
	   */
	    void testSeparer() {
		int [] tab;
		int indL;
		int indR;
		int res;
		int j;
		int nb;
		
		
		System.out.println("test de separer() pour les cas normaux");
		 
		 System.out.println("Pour un tableau ok et trier");
		 tab= new int [5];
		 nb = 5;
		 j=0;
		 while (j < nb){
			 tab[j]= j;
			 j++;
		 }
		 res = separer(tab,0, (nb-1));
		 if (res == 0){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("Pour un tableau ok et non trier");
		 tab= new int [5];
		 nb = 5;
		 tab[0] = 3;
		 tab[1] = 1;
		 tab[2] = 4;
		 tab[3] = 2;
		 tab[4] = 0;
		 res = separer(tab,0, (nb-1));
		 if (res == 3){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("Pour un tableau ok et une valeur repeter");
		 tab= new int [5];
		 nb = 5;
		 j=0;
		 while (j < nb){
			 tab[j]= 5;
			 j++;
		 }
		 res = separer(tab,0, (nb-1));
		 if (res == 0){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("test de separer() pour les cas limite");
		 System.out.println("Pour un tableau ok");
		 tab = new int [5];
		 nb = 1;
		 tab[0]= 10;
		 res = separer(tab,0, (nb-1));
		 if (res == 0){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("erreur");
		 }
	 }
	 /**
	  * Méthode de tri récursive selon le principe de séparation. La méthode s'appelle elle-même sur les tableaux gauche et droite par rapport à un pivot.
	  * @param tab : le tableau sur lequel est effectué la séparation
	  * @param indL : l'indice gauche de début de tableau
	  * @param indR : l'indice droite de fin de tableau
	  */ 
	 void triRapideRec(int[] tab, int indL, int indR){
		 int indS;
		 indS = separer ( tab, indL, indR );
		 if (indS >0){
			 if ( (indS-1) > indL ) {
				 triRapideRec ( tab, indL, (indS-1) );
			 }
		 }
		 if ( indS < tab.length){
			 if ( (indS+1) < indR ) {
				 triRapideRec ( tab, (indS+1), indR );
			 }
		 }
	 }	 
	/**
	 * test de la methode triRapideRec
	 */ 
	 void testTriRapideRec(){
		int [] tab;
		int nb;
		int j;
		boolean res;
		
		System.out.println("test de triRapideRec() pour les cas normaux");
		 
		 System.out.println("Pour un tableau ok et trier");
		 tab= new int [5];
		 nb =5;
		 j=0;
		 while (j < nb){
			 tab[j]= j;
			 j++;
		 }
		 triRapide(tab, nb);
		 res=  verifTri(tab,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("Pour un tableau ok et non trier");
		 tab= new int [5];
		 nb = 4;
		 j=nb-1;
		 while (j >= 0){
			 tab[j]= nb-j;
			 j--;
		 }
		 triRapide(tab, nb);
		 res=  verifTri(tab,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("Pour un tableau ok et une valeur repeter");
		 tab= new int [5];
		 nb = 4;
		 j=0;
		 while (j < nb){
			 tab[j]= 5;
			 j++;
		 }
		 triRapide(tab, nb);
		 res=  verifTri(tab,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("test de triRapideRec() pour les cas limite");
		 System.out.println("Pour un tableau ok");
		 tab = new int [5];
		 nb = 1;
		 tab[0]= 10;
		 triRapide(tab, nb);
		 res=  verifTri(tab,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("erreur");
		 }
	 } 
	 /**
	  * Tri par ordre croissant d'un tableau selon la méthode du tri rapide (QuickSort). On suppose que le tableau passé en paramètre est créé et possède au moins une valeur (nbElem > 0). Ceci ne doit donc pas être vérifié. Cette méthode appelle triRapideRec(...) qui elle effectue réellement le tri rapide selon la méthode de séparation récursive.
	  * @param leTab : le tableau à trier par ordre croissant
	  * @param nbElem : le nombre d'entiers que contient le tableau
	  */
	 void triRapide(int[] leTab, int nbElem){
		 triRapideRec ( leTab, 0, (nbElem -1) );
	 }
	 /**
	 * test de la methode triRapide
	 */
	 void testTriRapide(){
		int [] tab;
		int nb;
		int j;
		boolean res;
		
		System.out.println("test de triRapide() pour les cas normaux");
		 
		 System.out.println("Pour un tableau ok et trier");
		 tab= new int [5];
		 nb =5;
		 j=0;
		 while (j < nb){
			 tab[j]= j;
			 j++;
		 }
		 triRapide(tab, nb);
		 res=  verifTri(tab,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("Pour un tableau ok et non trier");
		 tab= new int [5];
		 nb = 4;
		 j=nb-1;
		 while (j >= 0){
			 tab[j]= nb-j;
			 j--;
		 }
		 triRapide(tab, nb);
		 res=  verifTri(tab,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("Pour un tableau ok et une valeur repeter");
		 tab= new int [5];
		 nb = 4;
		 j=0;
		 while (j < nb){
			 tab[j]= 5;
			 j++;
		 }
		 triRapide(tab, nb);
		 res=  verifTri(tab,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("test de triRapide() pour les cas limite");
		 System.out.println("Pour un tableau ok");
		 tab = new int [5];
		 nb = 1;
		 tab[0]= 10;
		 triRapide(tab, nb);
		 res=  verifTri(tab,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("erreur");
		 }
	 }
	 /**
	  * efficacite de la methode triRapide
	  */
	  void testTriRapideEfficacite(){
		  int [] tab;
		  int n;
		  int indice;
		  long t1;
		  long t2;
		  long diffT;
		  double log2n;
		  n= 1000;
		  System.out.println("efficacite de TriRapide");
		  for (int i = 1 ; i<= 5 ; i++){
			  tab = new int[n];
			  monSpleTab.remplirAleatoire( tab, n, 0, 10000);
			  cpt = 0;
			  
			  t1 = System.nanoTime();
			  triRapide(tab, n);
			  t2 = System.nanoTime();
			  diffT= (t2 - t1);
			  
			  System.out.println("Tps= " + diffT + "ns");
			  log2n= Math.log10(n)/Math.log10(2);
			  System.out.println("cpt/(n * log2n) =" + (double) cpt/(n * log2n) + "constant");
			  n=n*2;
		  }
	  }
	  /**
	   * A partir d'un tableau initial passé en paramètre "leTab", cette méthode renvoie un nouveau tableau "tabFreq" d'entiers où chaque case contient la fréquence d'apparition des valeurs dans le tableau initial. Pour simplifier, on suppose que le tableau initial ne contient que des entiers compris entre 0 et max (&lt;0). Dès lors le tableau "tabFreq" se compose de (max+1) cases et chaque case "i" (0 &lt;=i &lt;=max) contient le nombre de fois que l'entier "i" apparait dans le tableau initial. On suppose que le tableau passé en paramètre est créé et possède au moins une valeur (nbElem &gt; 0). Ceci ne doit donc pas être vérifié. Par contre, on vérifiera que le max du tableau initial est &gt; 0 et que le min est &gt; = 0. Dans le cas contraire, renvoyer un tableau "null".
	   * @param leTab : le tableau initial
	   * @param nbElem : le nombre d'entiers présents dans le tableau
	   * @return le tableau des fréquences de taille (max+1) ou null si la méthode ne s'applique pas 
	   */
	  int[] creerTabFreq(int[] leTab, int nbElem){
		  int []tab;
		  int min;
		  int max;
		  int i;
		  int j;
		  int c;
		  min = monSpleTab.leMin( leTab, nbElem);
		  max = monSpleTab.leMax( leTab, nbElem);
		  if ((min<0) |(max <=0)){
			  tab = null;
		  }
		  else{
			  tab = new int [max+1];
			  i=0;
			  while (i< (max+1)){
				  j=0;
				  c=0;
				  while (j<nbElem){
					  if (leTab[j]==i){
						  c++;
					  }
					  j++;
					  
				  }
				  tab[i] = c;
				  i++;
			  }
		  }
		  return tab;
	  }
	 /**
	 * test de la methode creerTabFreq
	 */
	 void testCreerTabFreq(){
		int [] leTab;
		int [] tabr;
		int indL;
		int indR;
		int res;
		int j;
		int nb;
		
		
		System.out.println("test de CreerTabFreq() pour les cas normaux");
		 
		 System.out.println("Pour un tableau avec une fois chaque valeur");
		 leTab= new int [5];
		 nb = 5;
		 j=0;
		 while (j <= 4){
			 leTab[j]= j;
			 j++;
		 }
		 tabr=creerTabFreq(leTab, nb);
		 j=0;
		 while (j<=4){
			 if (tabr[j] == 1){
				 System.out.println("test OK");
			 }
			 else{
				 System.out.println("Erreur");
			 }
			 j++;
		 }
		 
		 System.out.println("Pour un tableau ok et une valeur repeter");
		 leTab= new int [5];
		 nb = 4;
		 j=0;
		 while (j < nb){
			 leTab[j]= 5;
			 j++;
		 }
		 tabr=creerTabFreq(leTab, nb);
		 if (tabr[5] == 4){
			 System.out.println("test OK");
		 }
		 else{
			 System.out.println("Erreur");
		 }
		 
		 System.out.println("test de CreerTabFreq() pour les cas limite");
		 System.out.println("Pour un tableau ok");
		 leTab = new int [5];
		 nb = 1;
		 leTab[0]= 10;
		 tabr=creerTabFreq(leTab, nb);
		 if (tabr[10] == 1){
			 System.out.println("test OK");
		 }
		 else{
			 System.out.println("Erreur");
		 }
		 System.out.println("test de CreerTabFreq() pour les cas d'erreur");
		 System.out.println("Pour un tableau ok et min =-1 et max=-1");
		 leTab = new int [5];
		 nb = 1;
		 leTab[0]= -1;
		 tabr=creerTabFreq(leTab, nb);
		 if (tabr == null){
			 System.out.println("test OK");
		 }
		 else{
			 System.out.println("Erreur");
		 }
	 }
	 /**
	  * Tri par ordre croissant d'un tableau selon la méthode du tri par comptage de fréquences. On suppose que le tableau passé en paramètre est créé et possède au moins une valeur (nbElem &gt; 0).
	  * @param leTab : le tableau à trier par ordre croissant
	  * @param nbElem : le nombre d'entiers que contient le tableau
	  * @return le tableau initial trié par ordre croissant ou null si cette méthode de tri ne s'applique pas
	  */
	  int[] triParComptageFreq(int[] leTab, int nbElem){
		  int tab[];
		  int tabf[];
		  int i;
		  int j;
		  int c;
		  i=0;
		  j=0;
		  tabf=creerTabFreq(leTab, nbElem);
		  tab= new int [nbElem];
		  while ((i<nbElem)&& (j<nbElem)){
			  c=0;
			  while ((c!= tabf[j])&& (i<nbElem)){
				  tab[i]=j;
				  c++;
				  i++;
				  cpt ++;
			  }
			  j++;
		  }
		  return tab;
	  }
	  /**
	  * test de la methode triParComptageFreq
	  */
	  void testTriParComptageFreq(){
		int [] tab;
		int [] tabt;
		int nb;
		int j;
		boolean res;
		
		System.out.println("test de triParComptageFreq() pour les cas normaux");
		 
		 System.out.println("Pour un tableau ok et trier");
		 tab= new int [5];
		 nb = 4;
		 j=0;
		 while (j < nb){
			 tab[j]= j;
			 j++;
		 }
		 tabt = triParComptageFreq(tab, nb);
		 res=  verifTri(tabt,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("Pour un tableau ok et non trier");
		 tab= new int [5];
		 nb = 4;
		 j=nb-1;
		 while (j >= 0){
			 tab[j]= nb-j;
			 j--;
		 }
		 tabt = triParComptageFreq(tab, nb);
		 res=  verifTri(tabt,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("Pour un tableau ok et une valeur repeter");
		 tab= new int [5];
		 nb = 4;
		 j=0;
		 while (j < nb){
			 tab[j]= 5;
			 j++;
		 }
		 tabt = triParComptageFreq(tab, nb);
		 res=  verifTri(tabt,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("test de triParComptageFreq() pour les cas limite");
		 System.out.println("Pour un tableau ok");
		 tab = new int [5];
		 nb = 1;
		 tab[0]= 10;
		 tabt = triParComptageFreq(tab, nb);
		 res=  verifTri(tabt,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("erreur");
		 }
	  }
	  /**
	  * efficacite de la methode triParComptageFreq
	  */
	  void testTriParComptageFreqEfficacite(){
		  int [] tab;
		  int [] tabt;
		  int n;
		  long t1;
		  long t2;
		  long diffT;
		  n= 100;
		  System.out.println("efficacite de  triParComptageFreq");
		  for (int i = 1 ; i<= 5 ; i++){
			  tab = new int[(n+1)];
			  monSpleTab.remplirAleatoire( tab, n, 0, 10000);
			  cpt = 0;
			
			  t1 = System.currentTimeMillis();
			  tabt= triParComptageFreq(tab, n);
			  t2 = System.currentTimeMillis();
			  diffT= (t2 - t1);
			  
			  System.out.println("Tps= " + diffT + "ms");
			  System.out.println("cpt/n=" + (double) cpt/n + "constant");
			  n=n*2;
		  }
	  }
	  /**
	  * Tri par ordre croissant d'un tableau selon la méthode du tri à bulles : tant que le tableau n'est pas trié, permuter le contenu de 2 cases successives si leTab[i] &gt; leTab[i+1]. On suppose que le tableau passé en paramètre est créé et possède au moins une valeur (nbElem &gt; 0).
	  * @param leTab : le tableau à trier par ordre croissant
	  * @param nbElem : le nombre d'entiers que contient le tableau
	  */ 
	  void triABulles(int[] leTab, int nbElem){
		   int i;
		   int nb;
		   boolean echange;
		   nb=nbElem;
		   i=0;
		   echange = true;
		   while ((nb>0) && (echange = true)){
			   while ( i < (nb-1)){
				   if ( leTab[i] > leTab [i+1]){
					   monSpleTab.echange(leTab, nbElem, i, (i+1));
					   echange = true;
				   }
				   else{
					   echange = false;
				   }
				   cpt ++;
				   i++;
			   }
			   nb--;
			   i=0;
		   }
	  }
	  /**
	  * test de la methode triABulles
	  */
	  void testTriABulles(){
		int [] tab;
		int nb;
		int j;
		boolean res;
		
		System.out.println("test de triABulles() pour les cas normaux");
		 
		 System.out.println("Pour un tableau ok et trier");
		 tab= new int [5];
		 nb = 4;
		 j=0;
		 while (j < nb){
			 tab[j]= j;
			 j++;
		 }
		 triABulles(tab, nb);
		 res=  verifTri(tab,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("Pour un tableau ok et non trier");
		 tab= new int [5];
		 nb = 4;
		 j=nb-1;
		 while (j >= 0){
			 tab[j]= nb-j;
			 j--;
		 }
		 triABulles(tab, nb);
		 res=  verifTri(tab,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("Pour un tableau ok et une valeur repeter");
		 tab= new int [5];
		 nb = 4;
		 j=0;
		 while (j < nb){
			 tab[j]= 5;
			 j++;
		 }
		 triABulles(tab, nb);
		 res=  verifTri(tab,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("Erreur");
		 }
		 
		 System.out.println("test de tritriABulles() pour les cas limite");
		 System.out.println("Pour un tableau ok");
		 tab = new int [5];
		 nb = 1;
		 tab[0]= 10;
		 triABulles(tab, nb);
		 res=  verifTri(tab,nb);
		 if (res == true){
			 System.out.println("test OK");
		 }
		 else{
			  System.out.println("erreur");
		 }
	  }
	  /**
	  * efficacite de la methode TriABulles
	  */
	  void testTriABullesEfficacite(){
		  int [] tab;
		  int n;
		  int indice;
		  long t1;
		  long t2;
		  long diffT;
		  double log2n;
		  n= 1000;
		  System.out.println("efficacite de  triABulles");
		  for (int i = 1 ; i<= 5 ; i++){
			  tab = new int[n];
			  cpt = 0;
			  monSpleTab.remplirAleatoire( tab, n, 0, 10000);
			  
			  t1 = System.currentTimeMillis();
			  triABulles(tab, n);
			  t2 = System.currentTimeMillis();
			  diffT= (t2 - t1);
			  
			  System.out.println("Tps= " + diffT + "ms");
			  System.out.println("cpt/n²=" + (double) cpt/Math.pow(n, 2)  + "constant");
			  n=n*2;
		  }
		  
	  }
	  
}
