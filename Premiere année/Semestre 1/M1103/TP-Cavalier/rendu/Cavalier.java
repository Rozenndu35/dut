/**
 * @author Rozenn.Costiou decembre2018
 * Ce cavalier, tout en respectant la règle du déplacement d’un cavalier aux échecs, doit impérativement parcourir toutes les cases du damier, chaque case étant visitée une seule fois.
 */


public class Cavalier {
	
	/**
	 * Le point d'entré du programme.
	 */
	 
	int TAILLE_ECHEC = 5; //La taille de l'échiquier en constante (elever final pour pouvoir choisir la tailledans lanceur avencer )
	int[][] damier = new int [TAILLE_ECHEC][TAILLE_ECHEC]; //L'échiquier qui permet de visualiser les déplacements du cavalier
	int numDeplacmt; //Le numéro du déplacement qui permet de mémoriser chaque mouvement du cavalier sur le damier et de savoir si le jeu est terminé

	void principal (){
		//testAfficherDamier();
		//testDonnerSuivants();
		//testEstCeValide();
		//testEssayer();
		//lanceur();
		//testDonnerSuivantsRoi();
		//testEssayerRoi();
		lanceurAvencer();
		
	}
	/**
	 * Affiche le damier à l'écran
	 */
	void afficherDamier(){
		int y;
		int x;
		x=0;
		while (x!= TAILLE_ECHEC){
			y=0;
			while (y!= TAILLE_ECHEC){
				System.out.print(damier[x][y]+"	");
				y++;
			}
			x++;
			System.out.println();
		}	
	}
	/**
	 * test de la methode afficherDamier
	 */
	void testAfficherDamier(){
		 System.out.println("test de afficherDamier() pour les cas normaux");
		 damier[2][0] = 10;
		 damier[3][1] = 3;
		 damier[1][3] = 5;
		 afficherDamier();
		 System.out.println("SI :");
		 System.out.println("0  0 0 0");
		 System.out.println("0  0 0 5");
		 System.out.println("10 0 0 0");
		 System.out.println("0  3 0 0");
		 System.out.println("Test OK");
		 
	}
	/**
	 * A partir des cordonnées (posX, posY), cette méthode remplit le tableau "candidats" des 8 déplacements possibles du cavalier.
	 * @param posX : la position du cavalier en X (horizontale)
	 * @param posY : la position du cavalier en Y (verticale)
	 * @return candidats : le tableau (8 lignes X 2 colonnes) qui mémorisent les 8 déplacements possibles du cavalier
	 */
	int[][] donnerSuivants( int posX, int posY){
		int [][] candidat = new int [8][2];
		
		candidat[0][0]= posX-2;
		candidat[0][1]= posY-1;
		candidat[1][0]= posX-1;
		candidat[1][1]= posY-2;
		candidat[2][0]= posX+1;
		candidat[2][1]= posY-2;
		candidat[3][0]= posX+2;
		candidat[3][1]= posY-1;
		candidat[4][0]= posX+2;
		candidat[4][1]= posY+1;
		candidat[5][0]= posX+1;
		candidat[5][1]= posY+2;
		candidat[6][0]= posX-1;
		candidat[6][1]= posY+2;
		candidat[7][0]= posX-2;
		candidat[7][1]= posY+1;
		return candidat;
	}
	/**
	 * test de la methode donnerSuivants
	 */
	void testDonnerSuivants(){
		System.out.println("test de donnerSuivants() pour les cas normaux");
		System.out.println("Pour x=2 et y=2");
		int i;
		int j;
		int [][] candidat = new int [8][2];
		candidat = donnerSuivants(2,2);
		i=0;
		while (i!=8){
			j=0;
			while (j!= 2){
				System.out.print(candidat[i][j]+"	");
				j++;
			}
			i++;
			System.out.println();
		}
		System.out.println("SI :");
		System.out.println("0 1");
		System.out.println("1 0");
		System.out.println("3 0");
		System.out.println("4 1");
		System.out.println("4 3");
		System.out.println("3 4");
		System.out.println("1 4");
		System.out.println("0 3");
		System.out.println("Test OK");
		 
	}
	/**
	 * Méthode qui renvoie vrai si la nouvelle coordonnée (ou nouveau déplacement) du cavalier est possible
	 * @param newX : la nouvelle coordonnée en X 
	 * @param newY : la nouvelle coordonnée en Y
	 * @return : vrai si la nouvelle coordonnée (newX, newY) est valide
	 */
	boolean estCeValide ( int newX, int newY ){
		boolean ret = true;
		if ((newX < TAILLE_ECHEC) && (newY < TAILLE_ECHEC) && (newX >= 0) && (newY >= 0)){
			ret =true;
			if (damier[newX][newY] != 0){
				ret = false;
			}	
		}
		else {
			System.err.println("estCeValide: taille superieur ou inferieur au tableau ");
			ret = false;
		}		
		return ret;
	}
	/**
	 * test de la methode estCeValide
	 */
	void testEstCeValide(){
		boolean a;
		
		System.out.println("test de estCeValide() pour les cas normaux");
		System.out.println("pour des cordonée dans le tableau et nouvelle valeur");
		a=estCeValide (3,3);
		if (a == true){
			System.out.println("Test OK");
		}
		else {
			System.out.println("Ereur");
		}
		
		System.out.println("pour des cordonée dans le tableau et tombesur une case deja prevu");
		damier [3][3]= 7;
		a=estCeValide (3,3);
		if (a == false){
			System.out.println("Test OK");
		}
		else {
			System.out.println("Ereur");
		}
		damier [3][3]= 0;
		
		System.out.println("pour des cordonée a l'exterieur (négatif) du tableau");
		a=estCeValide (-1,5);
		if (a == false){
			System.out.println("Test OK");
		}
		else {
			System.out.println("Ereur");
		}
		
		System.out.println("pour des cordonée a l'exterieur (superieur) du tableau");
		a=estCeValide (10,5);
		if (a == false){
			System.out.println("Test OK");
		}
		else {
			System.out.println("Ereur");
		}
		

	}
	/**
	 * A partir des coordonnées (posX, posY) du cavalier, calculer les 8 déplacements suivants possibles par appel de la méthode « donnerSuivants(...) ».
	 * @param posX : la position du cavalier en X (horizontale)
	 * @param posY : la position du cavalier en Y (verticale)
	 * @return vrai si le chemin des déplacements est trouvé
	 */
	boolean essayer(int posX, int posY ){
		boolean res;
		int[][] newPos= new int[8][2]; 
		int i;
		int newX, newY;		
		boolean vrai;
		
		newPos=donnerSuivants(posX,posY);
		i=0;
		
		vrai= false;
		res = false;
		while ((i<8) && (res == false)){
			newX=newPos[i][0];
			newY = newPos[i][1];
			vrai = estCeValide ( newX,newY );
			
			if ( vrai == true ){
				numDeplacmt++;
				damier [newX][newY] = numDeplacmt;	
				
				if ( numDeplacmt == TAILLE_ECHEC * TAILLE_ECHEC) {
					res = true;
				}
				
				else {
					res = essayer(newX,newY );
					
					if (res == false){
						damier [newX][newY] =0;
						numDeplacmt--;
						i++;
					}
				}
			}
			i++;
			
		}
		
		return res;
	}
	/**
	 * test de la methode essayer
	 */
	void testEssayer(){
		boolean vrai;
		System.out.println("test de essayer() pour les cas normaux");
		System.out.println("commence dans un angle");
		numDeplacmt = 1;
		damier [0][0] = numDeplacmt;
		vrai = essayer(0,0);
		if ( vrai == true){
			 System.out.println("test OK");
		}
		else {
			  System.out.println("erreur");
		}
		
		System.out.println("commence aumilieu");
		damier = new int [TAILLE_ECHEC][TAILLE_ECHEC];
		numDeplacmt = 1;
		damier [2][2] = numDeplacmt;
		vrai = essayer(2,2);
		if ( vrai == true){
			 System.out.println("test OK");
		}
		else {
			 System.out.println("erreur");
		}
		  
		// Si  constante taille inferieur a 5 affiche errueur a chaque fois
	}
	/**
	 * Lanceur de l'application : crée le damier à la bonne taille et positionne le cavalier sur une première case en X et en Y.
	 */
	void lanceur(){
		int posX;
		int posY;
		boolean succes;

		// Choisir une position de depart pour le cavalier
		posX = 0;
		posY = 0;
		//Premier coup joué en (0,0)
		
		numDeplacmt = 1;
		damier [posX][posY] = numDeplacmt;
		// appeler la fonction récursive de recherche de la solution
		succes = essayer(posX, posY);
		if (succes = true){
			afficherDamier();
		}
		else{
			System.out.println("pas de solution");
		}
	}

	int[][] donnerSuivantsRoi( int posX, int posY){
		int [][] candidat = new int [8][2];
		
		candidat[0][0]= posX-1;
		candidat[0][1]= posY-1;
		candidat[1][0]= posX;
		candidat[1][1]= posY-1;
		candidat[2][0]= posX+1;
		candidat[2][1]= posY-1;
		candidat[3][0]= posX+1;
		candidat[3][1]= posY;
		candidat[4][0]= posX+1;
		candidat[4][1]= posY+1;
		candidat[5][0]= posX;
		candidat[5][1]= posY+1;
		candidat[6][0]= posX-1;
		candidat[6][1]= posY+1;
		candidat[7][0]= posX-1;
		candidat[7][1]= posY;
		return candidat;
	}
	void testDonnerSuivantsRoi(){
		System.out.println("test de donnerSuivants() pour les cas normaux");
		System.out.println("Pour x=2 et y=2");
		int i;
		int j;
		int [][] candidat = new int [8][2];
		candidat = donnerSuivantsRoi(1,1);
		i=0;
		while (i!=8){
			j=0;
			while (j!= 2){
				System.out.print(candidat[i][j]+"	");
				j++;
			}
			i++;
			System.out.println();
		}
		System.out.println("SI :");
		System.out.println("0 0");
		System.out.println("1 0");
		System.out.println("2 0");
		System.out.println("2 1");
		System.out.println("2 2");
		System.out.println("1 2");
		System.out.println("0 2");
		System.out.println("0 1");
		System.out.println("Test OK");
		 
	}
	boolean essayerRoi(int posX, int posY ){
		boolean res;
		int[][] newPos= new int[8][2]; 
		int i;
		int newX, newY;		
		boolean vrai;
		
		newPos=donnerSuivantsRoi(posX,posY);
		i=0;
		
		vrai= false;
		res = false;
		while ((i<8) && (res == false)){
			newX=newPos[i][0];
			newY = newPos[i][1];
			vrai = estCeValide ( newX,newY );
			
			if ( vrai == true ){
				numDeplacmt++;
				damier [newX][newY] = numDeplacmt;	
				
				if ( numDeplacmt == TAILLE_ECHEC * TAILLE_ECHEC) {
					res = true;
				}
				
				else {
					res = essayerRoi(newX,newY );
					
					if (res == false){
						damier [newX][newY] =0;
						numDeplacmt--;
						i++;
					}
				}
			}
			i++;
			
		}
		
		return res;
	}
	void testEssayerRoi(){
		boolean vrai;
		System.out.println("test de essayer() pour les cas normaux");
		System.out.println("commence dans un angle");
		numDeplacmt = 1;
		damier [0][0] = numDeplacmt;
		vrai = essayerRoi(0,0);
		if ( vrai == true){
			 System.out.println("test OK");
		}
		else {
			  System.out.println("erreur");
		}
		
		System.out.println("commence aumilieu");
		damier = new int [TAILLE_ECHEC][TAILLE_ECHEC];
		numDeplacmt = 1;
		damier [2][2] = numDeplacmt;
		vrai = essayerRoi(2,2);
		if ( vrai == true){
			 System.out.println("test OK");
		}
		else {
			 System.out.println("erreur");
		}
		  
	}
	void lanceurAvencer(){
		int posX;
		int posY;
		int choix;
		boolean succes;
		
		choix = SimpleInput.getInt("Saisir 1 pour le cavalier sinon 2 pour le roi");
		TAILLE_ECHEC = SimpleInput.getInt("Taille echec");
		if (TAILLE_ECHEC>8){
			System.out.println("cela peut prendre du temps ou ne pas fonctionner en fonction de votre PC");
		}
		// Choisir une position de depart pour le cavalier
		posX = SimpleInput.getInt("Saisir l'emlacement en X");
		while ((posX>= TAILLE_ECHEC) ||(posX<0)){
			System.out.println("case en dehors de l'echec");
			posX = SimpleInput.getInt("Saisir l'emlacement en X");
		}
		posY = SimpleInput.getInt("Saisir l'emlacement en Y");
		while ((posY>= TAILLE_ECHEC) ||(posY<0)){
			System.out.println("case en dehors de l'echec");
			posX = SimpleInput.getInt("Saisir l'emlacement en X");
		}
		
		
		numDeplacmt = 1;
		damier [posX][posY] = numDeplacmt;
		// appeler la fonction récursive de recherche de la solution
		if (choix == 1){
			succes = essayer(posX, posY);
		}
		else {
			succes= essayerRoi(posX, posY);
		}	
		if (succes = true){
			afficherDamier();
		}
		else{
		System.out.println("pas de solution");
		}
	}
}
	
