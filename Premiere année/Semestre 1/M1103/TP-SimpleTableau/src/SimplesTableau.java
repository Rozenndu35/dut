/**
 * Operation simple sur des tableaux
 */


public class SimplesTableau {
	
	/**
	 * Le point d'entré du programme.
	 */
	 
	void principal (){
		System.out.println("Ca marche");
		System.out.println(" ");
		testVerifTab();
		System.out.println(" ");
		
		testAfficherfTab();
		System.out.println(" ");
		
		testSaisir();
		System.out.println(" ");
		
		testAfficherTablgn();
		System.out.println(" ");
		
		testTirerAleatoire();
		System.out.println(" ");
		
		testRemplirAleatoire();
		System.out.println(" ");
		
		testEgalite();
		System.out.println(" ");
		
		testCopier();
		System.out.println(" ");
		
		testNbOccurence();
		System.out.println(" ");
		
		testLeMin();
		System.out.println(" ");
		
		testLeMax();
		System.out.println(" ");
			
		testInverse();
		System.out.println(" ");
			
		testEchange();
		System.out.println(" ");

		testMelange();
		System.out.println(" ");
		
		testDecalerGch();
		System.out.println(" ");
		
		testSuprimerValeur();
		System.out.println(" ");
		
		testInclusion();
		System.out.println(" ");
		
		testRemplirEtTrier();
		System.out.println(" ");
		
		testRemplirToutesDiff();
		
	}
	
	/**
	 * appéle chaque fois qu'une vérification doit se faire sur la validité d'un tableau. Le tableau n'est pas valide si celui-ci est inexistant ou si le nombre d'éléments qu'il contiendra est incompatible avec sa taille (leTab.length).
	 * @param leTab : le tableau dont vérifier l'existantce (leTab différent de null)
	 * @param nbElem : le nombre d'éléments que contiendra le tableau, doit vérifier la condition 0 < nbElem <= leTab.length
	 * @return vrai si le tableau est valide
	 */
	 boolean verifTab (int[] leTab, int nbElem){
		 boolean ret;
		 ret = true;
		 if (leTab == null){
			 ret = false;
			 }
		 else if (nbElem <= 0 || nbElem > leTab.length){
			 ret = false;
			 }	
		 return ret;
	 }
	 
	 /**
	 * Test de la methode verifTab()
	 */
	 void testVerifTab(){
		 int [] tab;
		 boolean res;
		 int nb;
		 
		 System.out.println("test de verifTab() pour les cas normaus");
		 
		 System.out.println("Pour un tableau inexistant");
		 tab = null;
		 nb = 2;
		 res = verifTab(tab,nb);
		 if (res == false){
			 System.out.println("test OK");
			}
	   	 else {
			System.out.println("test non OK");
			}
			
		 System.out.println("Pour un tableau qui marche");
		 tab = new int [5];
		 nb = 2;
		 res = verifTab(tab,nb);
		 if (res == true){
			 System.out.println("test OK");
			}
	   	 else {
			System.out.println("test non OK");
			}
		
		 System.out.println("Pour un tableau sans element");
		 tab = new int [5];
		 nb = 0;
		 res = verifTab(tab,nb);
		 if (res == false){
			 System.out.println("test OK");
			}
	   	 else {
			System.out.println("test non OK");
			}
		
		 System.out.println("Pour un tableau avecplus d'element que de place");
		 tab = new int [5];
		 nb = 10;
		 res = verifTab(tab,nb);
		 if (res == false){
			 System.out.println("test OK");
			}
	   	 else {
			System.out.println("test non OK");
			}
		
		System.out.println("test de verifTab() pour les cas limites");
		 
		 System.out.println("Pour un tableau avec 0 place");
		 tab = new int [0];
		 nb = 2;
		 res = verifTab(tab,nb);
		 if (res == false){
			 System.out.println("test OK");
			}
	   	 else {
			System.out.println("test non OK");
			}
		}

	/**
	 * Afficher le contenu des nbElem cases d'un tableau une par une
	 * @param leTab : le tableau a afficher
	 * @param nbElem : le nombre d'entier que conte le tableau
	 */
	 void afficherTab (int [] leTab, int nbElem){
		 int i;
		 boolean res;
		 res = verifTab(leTab,nbElem);
		 if (res == true){
			 i=0;
			 System.out.println("le tableau est : ");
			 while (i < nbElem){
				 System.out.println(leTab[i]+ " ");
				 i++;
			 }
		  }
		  
		  else {
				System.err.println("afficherTab: tableau invalide");
		  }
	 }
	
	 /**
	 * Test de la methode afficherTab()
	 */
	 void testAfficherfTab(){
		 int []tab={1,2,3,4,5};
		 int nb;
		 
		 System.out.println("test de afficherTab() pour les cas normaux");
		 
		 System.out.println("Pour un tableau rempli entre 1 et longeur-1");
		 nb=2;
		 afficherTab(tab,nb);
		 System.out.println("Si jusqu'à 2 OK");
		 
		 System.out.println("Pour un tableau rempli a la longueur");
		 nb=tab.length;
		 afficherTab(tab,nb);
		 System.out.println("Si jusqu'à 5 OK");
		 
		 System.out.println("test de afficherTab() pour les cas limites");
		 
		 System.out.println("Pour un tableau avec une valeur");
		 nb=1;
		 afficherTab(tab,nb);
		 System.out.println("Si jusqu'à 1 OK");
		 
		 System.out.println("test de afficherTab() pour les cas d'erreurs");
		 
		 System.out.println("Pour un tableau avec nombre d'element superieur a la longeur");
		 nb=20;
		 afficherTab(tab,nb);
		 System.out.println("Si erreur OK");
		 
		 System.out.println("Pour un tableau avec nombre d'element inferieur ou egale a 0");
		 nb=0;
		 afficherTab(tab,nb);
		 System.out.println("Si erreur OK");
		 
		 System.out.println("Pour un tableau inexistant");
		 tab=null;
		 nb=4;
		 afficherTab(tab,nb);
		 System.out.println("Si erreur OK");
	 }
	 /**
	 * A partir d'un tableau supposé DEJA CREE, saisie par boite de dialogue de nbElem valeurs par l'utilisateur. 
	 * @param leTab : le tableau a remplir par saisies successives de l'utilisateur
	 * @param nbElem : le nombre d'entier que contient le tableau (<= taille)
	 */
	 void saisir(int[] leTab, int nbElem){
		 int i;
		 boolean res;
		 
		 res = verifTab(leTab,nbElem);	
		 if (res == true){
			i=0;
			while (i<nbElem){
				leTab[i]=SimpleInput.getInt("Saisir une valeur a mettre dans le tableau");
			i++;
			}
		 }
		 else {
			 System.err.println	("saisir : tableau invalide");
		 }
	 }
	 /**
	 * Test de la methode saisir()
	 */
	 void testSaisir(){
		 int []tab;
		 int nb;
		 tab= new int [5];
		 
		 System.out.println("test de Saisir() pour les cas normaux");
		 
		 System.out.println("Pour un tableau rempli entre 1 et longeur-1");
		 nb=2;
		 saisir(tab,nb);
		 System.out.println("Si demande deux fois OK");
		 
		 System.out.println("Pour un tableau rempli a la longueur");
		 tab= new int [5];
		 nb=tab.length;
		 saisir(tab,nb);
		 System.out.println("Si demande cinq fois OK");
		 
		 System.out.println("test de Saisir()  pour les cas limites");
		 
		 System.out.println("Pour un tableau avec une valeur");
		 tab= new int [5];
		 nb=1;
		 saisir(tab,nb);
		 System.out.println("Si demande une fois OK");
		 
		 System.out.println("test de Saisir() pour les cas d'erreurs");
		 
		 System.out.println("Pour un tableau avec nombre d'element superieur a la longeur");
		 tab= new int [5];
		 nb=20;
		 saisir(tab,nb);
		 System.out.println("Si erreur OK");
		 
		 System.out.println("Pour un tableau avec nombre d'element inferieur ou egale a 0");
		 tab= new int [5];
		 nb=0;
		 saisir(tab,nb);
		 System.out.println("Si erreur OK");
		 
		 System.out.println("Pour un tableau inexistant");
		 tab=null;
		 nb=4;
		 saisir(tab,nb);
		 System.out.println("Si erreur OK");
	 }
	 /**
	 * Affiche le contenu d'un tableau case par case et par ligne où chaque ligne doit afficher un nombre fixé de "nbParLGN" éléments.
	 * @param nbParLgn : le nombre d'élément par ligne
	 * @param leTab : le tableau a afficher
	 * @param nbElem : le nombre d'entier que contient le tableau
	 */
	 void afficherTabLgn( int[] leTab, int nbElem, int nbParLgn){
		int i;
		int j;
		boolean res;
		
		res = verifTab(leTab,nbElem);	
		if(res == true){
			if ((nbParLgn >0)){
				j=0;
				
				while (j < nbElem){
					i=0;
					System.out.println ("lignes suivante:");
					while (i<nbParLgn && j<nbElem){
						System.out.print(leTab[j]+" ");
						i++;
						j++;
					}
					System.out.println (" ");
					
				}
			}
			else if(nbParLgn <= 0 ){
				System.err.println	("afficherTabLgn : nbParLgn invalide");
			}
		}
		else{
			System.err.println	("afficherTabLgn : tableau invalide");
		}
	}
	
	 /**
	 * Test de la methode afficherTabLgn()
	 */
	void testAfficherTablgn(){
		 int []tab;
		 int nb;
		 int nbparlgn;
		 
		 
		 System.out.println("test de AfficherTabLgn() pour les cas normaux");
		 
		 System.out.println("Pour un tableau rempli entre 1 et longeur-1 et 0<nbparln<nb");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 nb = 3;
		 nbparlgn = 2;
		 afficherTabLgn(tab,nb,nbparlgn);
		 System.out.println("Si affiche 1 et 2 sur la ligne et 3 sur la ligne suivente OK");
		 
		 System.out.println("Pour un tableau rempli entre 1 et longeur-1 et nbparln=nb");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 nb = 3;
		 nbparlgn = nb;
		 afficherTabLgn(tab,nb,nbparlgn);
		 System.out.println("Si affiche 1 et 2 et 3 sur une ligne OK");
		 
		 System.out.println("Pour un tableau rempli a la longeur-1 et 0<nbparln<nb");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 tab [3] = 4;
		 tab [4] = 5;
		 nb = tab.length;
		 nbparlgn = 2;
		 afficherTabLgn(tab,nb,nbparlgn);
		 System.out.println("Si affiche 1 et 2 sur la ligne, 3 et 4 sur la ligne suivente et 5 sur la ligne suivente OK");
		  
		 System.out.println("Pour un tableau rempli a la longeur et nbparln=nb");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 tab [3] = 4;
		 tab [4] = 5;
		 nb = tab.length;
		 nbparlgn = nb;
		 afficherTabLgn(tab,nb,nbparlgn);
		 System.out.println("Si affiche 1 et 2 et 3 et 4 et 5 sur une ligne OK");
		 
		 System.out.println("test de AfficherTabLgn() pour les cas limites");
		 
		 System.out.println("Pour un tableau avec une valeur et nbparlgn=nb");
		 tab= new int [5];
		 tab [0] = 1;
		 nb = 1;
		 nbparlgn = nb;
		 afficherTabLgn(tab,nb,nbparlgn);
		 System.out.println("Si affiche 1 sur une ligne OK");
		 
		 System.out.println("Pour un tableau valide et nbparlgn>nb"); 
		 tab= new int [5];
		 tab [0] = 1;
		 nb = 1;
		 nbparlgn = 3;
		 afficherTabLgn(tab,nb,nbparlgn);
		 System.out.println("Si affiche 1 sur une ligne OK");
		 
		 System.out.println("test de AfficherTabLgn() pour les cas d'erreurs");
		 
		 System.out.println("Pour un tableau avec nombre d'element superieur a la longeur");
		 tab= new int [5];
		 nb = 20;
		 nbparlgn = nb;
		 afficherTabLgn(tab,nb,nbparlgn);
		 System.out.println("Si erreur OK");
		 
		 System.out.println("Pour un tableau avec nombre d'element inferieur ou egale a 0");
		 tab= new int [5];
		 nb = 0;
		 nbparlgn = nb;
		 afficherTabLgn(tab,nb,nbparlgn);
		 System.out.println("Si erreur OK");
		 
		 System.out.println("Pour un tableau valide et nbparlgn<=0"); 
		 tab= new int [5];
		 nb = 3;
		 nbparlgn = -1;
		 afficherTabLgn(tab,nb,nbparlgn);
		 System.out.println("Si erreur OK");
		 
		 System.out.println("Pour un tableau inexistant");
		 tab= null;
		 nb = 3;
		 nbparlgn = 5;
		 afficherTabLgn(tab,nb,nbparlgn);
		 System.out.println("Si erreur OK");
	 }	
	/**
	* A partir d'un tableau créér, remplir aléatoirement ce tableau de nbElem valeurs comprises entre min et max.
	* @param min : la valeur de l'entier minimum
	* @param max: la valeur de l'entier maximum
	* @return l'entier aléatoire
	*/
	int tirerAleatoire( int min, int max){
		int res;
	
		if (min<=max && min>=0){
			res= min + (int)(Math.random()* ((max-min)+1));
		}
		else{
			System.err.println	("tirerAleatoirement : max et min invalide");
			res= -1;
		}
		return res;
	}
	 /**
	 * Test de la methode afficherTirerAleatoire()
	 */
	void testTirerAleatoire(){
		 int min;
		 int max;
		 int v;
		 
		 
		 System.out.println("test de TirerAleatoire() pour les cas normaux");
		 
		 System.out.println("Pour max et min ok");
		 min =5;
		 max= 10;		
		 v=tirerAleatoire( min,max);
		 if ( (min<=v) && (v<=max)){
			System.out.println("Test OK");
		 }
		 else{
			 System.out.println("Erreur");
		 }
		 
		 	
		 System.out.println("test de TirerAleatoire() pour les cas limites");
		 
		 System.out.println("Pour max et min egale");
		 min =5;
		 max= 5	;	
		 v=tirerAleatoire( min,max);
		 if ( (min<=v) && (v<=max)){
			System.out.println("Test OK");
		 }
		 else{
			 System.out.println("Erreur");
		 }
		 
		 System.out.println("test de TirerAleatoire() pour les cas d'erreurs");
		 
		 System.out.println("Pour max <0");
		 min =5;
		 max= -6;	
		 v=tirerAleatoire( min,max);
		 if (v==-1){
			System.out.println("Test OK");
		 }
		 else{
			 System.out.println("Erreur");
		 }
		 
		 System.out.println("Pour min <0");
		 min =-5;
		 max= 7	;
		 v=tirerAleatoire( min,max);
		 if (v==-1){
			System.out.println("Test OK");
		 }
		 else{
			 System.out.println("Erreur");
		 }
		 
		 System.out.println("Pour min >max");
		 min =9;
		 max= 4;
		 v=tirerAleatoire( min,max);
		 if (v==-1){
			System.out.println("Test OK");
		 }
		 else{
			 System.out.println("Erreur");
		 }
	 }	
	 
	 /**
	 * A partir d'un tableau créér, remplir aléaoirement ce tableau  de nbElem valeurs comprises entre min et max.
	 * @param leTab : le tableau à remplir de valeurs tirer aléatoirement
	 * @param nbElem1 : le nombre d'entier que contiendra le tableau
	 * @param min : la valeur de l'entier minimum
	 * @param max: la valeur de l'entier maximum
	 */
	 void remplirAleatoire( int[] leTab, int nbElem, int min, int max){
		boolean a;
		int i;
		int c;
		boolean b;
		a=verifTab (leTab, nbElem);
		if (a==true){
			b= true;
			i=0;
			while ((i< nbElem) && (b==true)){
				c=tirerAleatoire(min,max);
				if (c!=-1){
					leTab [i] = c;
					i++;
				}
				else {
					b=false;
					System.err.println	("remplirAleatoirement : max et min invalide");
				}
			}
		}
		if (a == false){
			System.err.println	("remplirAleatoire: tableau invalide");
		}
	}
	
	/**
	* Test de la methode afficherRemplirAleatoire()
	*/
	void testRemplirAleatoire(){
		int [] tab;
		int nb;
		int max;
		int min;
						
		System.out.println("test de RemplirAleatoire pour les cas normaux");
		 
		 System.out.println("Pour un tableau Ok ,pour un nombre d'element OK, un max et un min ok");
		 tab= new int [5];
		 nb = 3;
		 min =5;
		 max= 10;		
		 remplirAleatoire( tab , nb , min , max );
		 System.out.print(tab[0]+" ");
		 System.out.print(tab[1]+" ");
		 System.out.println(tab[2] +" ");
		 System.out.println("si trois valeur comprise entre 5 et 10 OK" );
		 
		 System.out.println("Pour un tableau Ok ,pour un nombre d'element egale a la longueur, un max et un min ok");
		 tab= new int [5];
		 nb = tab.length;
		 min =5;
		 max= 10;		
		 remplirAleatoire( tab , nb , min , max );
		 System.out.print(tab[0]+" ");
		 System.out.print(tab[1]+" ");
		 System.out.print(tab[2] +" ");
		 System.out.print(tab[3] +" ");
		 System.out.println(tab[4] +" ");
		 System.out.println("si cinq valeur comprise entre 5 et 10 OK" );
		 
		 	
		 System.out.println("test de RemplirAleatoire pour les cas limites");
		 
		 System.out.println("Pour un tableau Ok ,pour un nombre d'element OK, un max et un min egale");
		 tab= new int [5];
		 nb = 3;
		 min =10;
		 max= 10;		
		 remplirAleatoire( tab , nb , min , max );
		 System.out.print(tab[0]+" ");
		 System.out.print(tab[1]+" ");
		 System.out.println(tab[2] +" ");
		 System.out.println("si trois valeur egale a 10 OK" );
		 
		 System.out.println("Pour un tableau Ok ,pour un nombre d'element egale a 1, un max et un min egale");
		 tab= new int [5];
		 nb = 1;
		 min =5;
		 max= 10;		
		 remplirAleatoire( tab , nb , min , max );
		 System.out.println(tab[0]+" ");
		 System.out.println("si une valeur entre 5 et 10 OK" );

		 
		 System.out.println("test de RemplirAleatoire pour les cas d'erreurs");
		 
		 System.out.println("Pour un tableau Ok ,pour un nombre d'element OK, un max<0");
		 tab= new int [5];
		 nb = 3;
		 min =10;
		 max= -1;		
		 remplirAleatoire( tab , nb , min , max );
		 System.out.println("si erreur OK" );

		 
		 System.out.println("Pour un tableau Ok ,pour un nombre d'element OK, un min<0");
		 tab= new int [5];
		 nb = 3;
		 min =-5;
		 max= 20;		
		 remplirAleatoire( tab , nb , min , max );
		 System.out.println("si erreur OK" );

		 
		 System.out.println("Pour un tableau Ok ,pour un nombre d'element OK, un max<min");
		 tab= new int [5];
		 nb = 3;
		 min =10;
		 max= 5;		
		 remplirAleatoire( tab , nb , min , max );
		 System.out.println("si erreur OK" );
		 
		 System.out.println("Pas de tableau ,pour un nombre d'element OK, un max et un min OK");
		 tab= null;
		 nb = 3;
		 min =10;
		 max= 5;		
		 remplirAleatoire( tab , nb , min , max );
		 System.out.println("si erreur OK" );
		 
		 System.out.println("Pour un tableau Ok ,pour un nombre d'element superieur a la longueur, un max et un min ok");
		 tab= new int [5];
		 nb = 10;
		 min =5;
		 max= 10;		
		 remplirAleatoire( tab , nb , min , max );
		 System.out.println("si erreur OK" );
		 
		 System.out.println("Pour un tableau Ok ,pour un nombre d'element inferieur a 0, un max et un min ok");
		 tab= new int [5];
		 nb = -1;
		 min =5;
		 max= 10;		
		 remplirAleatoire( tab , nb , min , max );
		 System.out.println("si erreur OK" );

	 }
	 /**
	 * Renvoi vrai si les 2 tableaux passé en parametre sont exactement les mêmes en nombre d'élément contenu (case par case).
	 * @param tab1 : le premier tableau à comparer
	 * @param tab2 : le deuxieme tableau à comparer
	 * @param nbElem1 : le nombre d'entier present dans le premier tableau
	 * @param nbElem2 : le nombre d'entier present dans le deuxieme tableau
	 * @return true si egalité parfaite sinon false
	 */
	 boolean egalite( int[] tab1, int[] tab2, int nbElem1,int nbElem2){
		 int i;
		 boolean res;
		 boolean a;
		 boolean b;
		 a=verifTab (tab1, nbElem1);
		 b=verifTab (tab2, nbElem2);
		 res = true;
		 if (a== false || b== false){
			 res = false;
		 }
		 else if ( nbElem1 != nbElem2){
			 res = false;
		 }
		 else {
			 i=0;
			 while ((i < nbElem1) && (res == true)){
				 if (tab1[i] == tab2[i]) {
					 i ++;
				 }
				 else{
					 System.err.println("egalite: tableau different");
					 res= false;
				 }
			  }
		 }
		 return res;
	}
	/**
	* Test de la methode Egalite()
	*/
	void testEgalite(){
		int [] tab1;
		int nb1;
		int [] tab2;
		int nb2;
		boolean res;

						
		System.out.println("test de egalite() pour les cas normaux");
		 
		 System.out.println("Pour deux tableau identique");
		 tab1= new int [6];
		 nb1 = 4;
		 tab1[0]= 1;
		 tab1[1]= 2;
		 tab2[2]= 1;
		 tab2[3]= 2;
		 res = egalite( tab1, tab2,nb1,nb2);
		 if (res = true){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }

		 System.out.println("test de egalite() pour les cas d'erreurs");
		 
		 System.out.println("Pour un nombre d'elemnt different");
		 tab1= new int [6];
		 nb1 = 3;
		 tab2= new int [6];
		 nb2 = 2;
		 tab1[0]= 1;
		 tab1[1]= 2;
		 tab1[2] =7;
		 tab2[0]= 1;
		 tab2[1]= 2;
		 res = egalite( tab1, tab2,nb1,nb2);
		 if (res = true){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 
	     System.out.println("Pour un tableau avec des valeur differnte");
		 		 tab1= new int [6];
		 nb1 = 2;
		 tab2= new int [6];
		 nb2 = 2;
		 tab1[0]= 1;
		 tab1[1]= 2;
		 tab2[0]= 5;
		 tab2[1]= 2;
		 res = egalite( tab1, tab2,nb1,nb2);
		 if (res = true){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 
		 System.out.println("Pour un des deux tableau inexistant");
		 tab1= null;
		 nb1 = 2;
		 tab2= new int [6];
		 nb2 = 2;
		 tab2[0]= 1;
		 tab2[1]= 2;
		 res = egalite( tab1, tab2,nb1,nb2);
		 if (res = true){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
	 }
	
	/**
	* renvoie la copie exacte (clone) du tableau passé en parametre
	* @param tabToCopy : le tableau à copier
	* @param nbElem : le nombre d'entier présent dans le tableau
	* @return le nouveau tableau qui est la copie du tableau passé en paramètre
	*/
	
	int[] copier( int[] tabToCopy, int nbElem){
		int [] tab;
		int i;
		boolean a;
		a=verifTab (tabToCopy, nbElem);
		if (a==true){
			i=0;
			tab = new int [nbElem+1];
			while (i<nbElem){
				tab[i]=tabToCopy[i];
				i++;
			}
		
		}
		else{
			tab = null;
			System.err.println("copier: tableau a copier invalide");
		}
		return tab;
	}
	
	/**
	* Test de la methode copier()
	*/
	void testCopier(){
		int []tab;
		int nb;
		int []tabc;
		int i;
		 
		 
		 System.out.println("test de copier() pour les cas normaux");
		 
		 System.out.println("Pour un tableau rempli entre 1 et longeur-1");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 nb = 3;
		 tabc=copier(tab,nb);
		 i=0;
		 while (i<nb){
			  System.out.print( tab[i] +" ");
			  i++;
		  }
		 System.out.println("Si meme tableau (1,2,3)OK");
		 
		 System.out.println("Pour un tableau rempli a la longeur-1");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 tab [3] = 4;
		 tab [4] = 5;
		 nb = 5;
		 tabc=copier(tab,nb);
		 i=0;
		 while (i<nb){
			  System.out.print( tab[i]+ " ");
			  i++;
		  }
		 System.out.println("Si meme tableau (1,2,3,4,5)OK");
		  
		 
		 System.out.println("test de copier() pour les cas limites");
		 
		 System.out.println("Pour un tableau avec une valeur ");
		 tab= new int [5];
		 tab[0] = 1;
		 nb = 1;
		 tabc=copier(tab,nb);
		 i=0;
		 while (i<nb){
			  System.out.print( tab[i] +" ");
			  i++;
		  }
		 System.out.println("Si meme tableau (1)OK");

		 
		 System.out.println("test de copier() pour les cas d'erreurs");
		 
		 System.out.println("Pour un tableau avec nombre d'element superieur a la longeur");
		 tab= new int [5];
		 nb = 20;
		 tabc=copier(tab,nb);
		 System.out.println("Si erreur OK");
		 
		 System.out.println("Pour un tableau avec nombre d'element inferieur ou egale a 0");
		 tab= new int [5];
		 nb = 0;
         tabc=copier(tab,nb);
		 System.out.println("Si erreur OK");
		 
		 System.out.println("Pour un tableau inexistant");
		 tab= null;
		 nb = 3;
         tabc=copier(tab,nb);
		 System.out.println("Si erreur OK");
		
	}
	
	/**
	* Renvoie le nombre d'occurence d'un entier dans un tableau
	* @param leTab : le tableau
	* @param nbElem : le nombre d'entier présents dans le tableau
	* @param : l'entier à rechercher dans le tabeau
	* @return le nombre d'occurence
	*/
	int nbOccurence( int [] leTab, int nbElem, int elem){
		int nb;
		int i;
		boolean a;
		a=verifTab (leTab, nbElem);
		if (a==true){
			nb=0;
			i=0;
			while (i < nbElem){
				if (leTab[i]== elem){
					nb=nb+1;
				}
				i++;
			}
		} 
		else{
			nb=-1;
			System.err.println("nbOccurence: tableau invalide");
		}
		return nb;
	}
	/**
	* Test de la methode nbOccurence()
	*/
	void testNbOccurence(){
		int []tab;
		int nb;
		int elem;
		int res;
		 
		 
		 System.out.println("test de nbOccurence() pour les cas normaux");
		 
		 System.out.println("Pour un tableau rempli entre 1 et longeur-1 et deux fois 2");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 2;
		 nb = 3;
		 elem=2;
		 res=nbOccurence( tab,nb, elem);
		 if (res == 2){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		
		 System.out.println("Pour un tableau rempli a la longeur-1 deux fois le nombre 3");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 tab [3] = 4;
		 tab [4] = 3;
		 nb = 5;
		 elem=3;
		 res=nbOccurence( tab,nb, elem);
		 if (res == 2){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		
		  
		 
		 System.out.println("test de nbOccurence() pour les cas limites");
		 
		 System.out.println("Pour un tableau avec une valeur et ele egale la valeur ");
		 tab= new int [5];
		 tab[0] = 1;
		 nb = 1;
		 elem=1;
		 res=nbOccurence( tab,nb, elem);
		 if (res == 1){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 System.out.println("Pour un tableau rempli a la longeur-1 et avec le nombre 3");
		 tab= new int [5];
		 tab [0] = 3;
		 tab [1] = 3;
		 tab [2] = 3;
		 tab [3] = 3;
		 tab [4] = 3;
		 nb = 5;
		 elem=3;
		 res=nbOccurence( tab,nb, elem);
		 if (res == 5){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 
		 System.out.println("Pour un tableau OK et avec le nombre 6 pas present");
		 tab= new int [5];
		 tab [0] = 3;
		 tab [1] = 3;
		 tab [2] = 3;
		 tab [3] = 3;
		 tab [4] = 3;
		 nb = 5;
		 elem=6;
		 res=nbOccurence( tab,nb, elem);
		 if (res == 0){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }

		 
		 System.out.println("test de nbOccurence() pour les cas d'erreurs");
		 
		 System.out.println("Pour un tableau avec nombre d'element superieur a la longeur");
		 tab= new int [5];
		 nb = 20;
		 elem=6;
		 res=nbOccurence( tab,nb, elem);
		 if (res == -1){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 
		 System.out.println("Pour un tableau avec nombre d'element inferieur ou egale a 0");
		 tab= new int [5];
		 nb = 0;
		 elem=6;
		 res=nbOccurence( tab,nb, elem);
		 if (res == -1){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 
		 System.out.println("Pour un tableau inexistant");
		 tab= null;
		 nb = 3;
      	 elem=6;
		 res=nbOccurence( tab,nb, elem);
		 if (res == -1){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
	}
	
	/**
	* Renvoie le minimum parmi les éléments du tableau
	* @param leTab : le tableau
	* @param nbElem : le nombre d'entier présents dans le tableau
	* @return le minimum des elements du tableau
	*/
	int leMin( int [] leTab, int nbElem){
		int min;
		int i;
		boolean a;
		a=verifTab (leTab, nbElem);
		if (a==true){
			i=0;
			min = leTab[i];
			while (i < nbElem-1){
				if (leTab[i] > leTab[i+1] ){
					min = leTab[i+1];
				}
				i++;
			}
		} 
		else{
			min=-1;
			System.err.println("leMin: tableau invalide");
		}
		return min;
	}
	/**
	* Test de la methode leMin()
	*/
	void testLeMin(){
		int []tab;
		int nb;
		int res;
		 
		 
		 System.out.println("test de leMin() pour les cas normaux");
		 
		 System.out.println("Pour un tableau rempli entre 1 et longeur-1 et avec une fois le min 1");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 2;
		 nb = 3;
		 res=leMin(tab, nb);
		 if (res == 1){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		
		 System.out.println("Pour un tableau rempli a la longeur-1 et avec une fois le min 1");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 tab [3] = 4;
		 tab [4] = 3;
		 nb = 5;
		  res=leMin(tab, nb);
		 if (res == 1){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		
		 System.out.println("Pour un tableau rempli a la longeur-1 et avec deux fois le min 1");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 tab [3] = 4;
		 tab [4] = 3;
		 nb = 5;
		  res=leMin(tab, nb);
		 if (res == 1){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 
		 System.out.println("test de leMin() pour les cas limites");
		 
		 System.out.println("Pour un tableau avec une valeur  ");
		 tab= new int [5];
		 tab[0] = 1;
		 nb = 1;
		 res=leMin(tab, nb);
		 if (res == 1){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 System.out.println("Pour un tableau rempli a la longeur-1 et des 3");
		 tab= new int [5];
		 tab [0] = 3;
		 tab [1] = 3;
		 tab [2] = 3;
		 tab [3] = 3;
		 tab [4] = 3;
		 nb = 5;
		 res=leMin(tab, nb);
		 if (res == 3){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 		 
		 System.out.println("test de leMin() pour les cas d'erreurs");
		 
		 System.out.println("Pour un tableau avec nombre d'element superieur a la longeur");
		 tab= new int [5];
		 nb = 20;
		 res=leMin(tab, nb);
		 if (res == -1){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 
		 System.out.println("Pour un tableau avec nombre d'element inferieur ou egale a 0");
		 tab= new int [5];
		 nb = 0;
		 res=leMin(tab, nb);
		 if (res == -1){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 
		 System.out.println("Pour un tableau inexistant");
		 tab= null;
		 nb = 3;
      	 res=leMin(tab, nb);
		 if (res == -1){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
	}
	
	/**
	* Renvoie le maximum parmi les éléments du tableau
	* @param leTab : le tableau
	* @param nbElem : le nombre d'entier présents dans le tableau
	* @return le maximum des elements du tableau
	*/
	int leMax( int [] leTab, int nbElem){
		int max;
		int i;
		boolean a;
		a=verifTab (leTab, nbElem);
		if (a==true){
			i=0;
			max = leTab[i];
			while (i < nbElem-1){
				if (leTab[i] < leTab[i+1] ){
					max = leTab[i+1];
				}
				i++;
			}
		} 
		else{
			max=-1;
			System.err.println("leMax: tableau invalide");
		}
		return max;
	}
	/**
	* Test de la methode leMax()
	*/
	void testLeMax(){
		int []tab;
		int nb;
		int res;
		 
		 
		 System.out.println("test de leMax() pour les cas normaux");
		 
		 System.out.println("Pour un tableau rempli entre 1 et longeur-1 et avec une fois le max 2");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 1;
		 tab [2] = 2;
		 nb = 3;
		 res=leMax(tab, nb);
		 if (res == 2){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		
		 System.out.println("Pour un tableau rempli a la longeur-1 et vec une fois le max 4");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 tab [3] = 4;
		 tab [4] = 3;
		 nb = 5;
		 res=leMax(tab, nb);
		 if (res == 4){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 
		 System.out.println("Pour un tableau rempli a la longeur-1 et vec deux fois le max 4");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 4;
		 tab [2] = 3;
		 tab [3] = 4;
		 tab [4] = 3;
		 nb = 5;
		 res=leMax(tab, nb);
		 if (res == 4){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		
		  
		 
		 System.out.println("test de leMax() pour les cas limites");
		 
		 System.out.println("Pour un tableau avec une valeur  ");
		 tab= new int [5];
		 tab[0] = 1;
		 nb = 1;
		 res=leMax(tab, nb);
		 if (res == 1){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 System.out.println("Pour un tableau rempli a la longeur-1 et des 3");
		 tab= new int [5];
		 tab [0] = 3;
		 tab [1] = 3;
		 tab [2] = 3;
		 tab [3] = 3;
		 tab [4] = 3;
		 nb = 5;
		 res=leMax(tab, nb);
		 if (res == 3){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 		 
		 System.out.println("test de leMax() pour les cas d'erreurs");
		 
		 System.out.println("Pour un tableau avec nombre d'element superieur a la longeur");
		 tab= new int [5];
		 nb = 20;
		 res=leMax(tab, nb);
		 if (res == -1){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 
		 System.out.println("Pour un tableau avec nombre d'element inferieur ou egale a 0");
		 tab= new int [5];
		 nb = 0;
		 res=leMax(tab, nb);
		 if (res == -1){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 
		 System.out.println("Pour un tableau inexistant");
		 tab= null;
		 nb = 3;
      	 res=leMax(tab, nb);
		 if (res == -1){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }		
	}

	/**
	* Renvoie un nouveau tableau qui est l'inverse de celui passé en parametre Son jeme element est egale au (nbElem+1-j) élément du tableau initiale (dans l'explication, j=1 signifir premier element du tableau).
	* @param leTab : le tableau
	* @param nbElem : le nombre d'entier présents dans le tableau
	* @return le nouveau tableau qui est l'inverse de leTab sur le plage (O...nbElem-1)
	*/
	int[] inverse( int [] leTab, int nbElem){
		int [] tab;
		boolean a;
		int i;
		a=verifTab (leTab, nbElem);
		if (a== true){
			i=0;
			tab = new int [nbElem];
			while (i<nbElem){
				tab[i] = leTab[nbElem -1-i];
				i++;
			}
		}
		else{
			tab = null;
			System.err.println("inverse: tableau invalide");
		}
		return tab;
	}
	/**
	* Test de la methode inverse()
	*/
	void testInverse(){
		int [] tab;
		int [] leTab;
		int nb;
		int i;
		 System.out.println("test de inverse() pour les cas normaux");
		 
		 System.out.println("Pour un tableau rempli entre 1 et longeur-1 ");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 nb = 3;
		 leTab = inverse(tab,nb);
		 i = 0;
		 while (i<nb){
			  System.out.print( leTab[i] +" ");
			  i++;
		  }
		  System.out.println("Si 321 OK");
		 
		
		 System.out.println("Pour un tableau rempli a la longeur-1 ");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 tab [3] = 4;
		 tab [4] = 5;
		 nb = 5;
		 leTab=inverse( tab, nb);
		 i=0;
		 while (i<nb){
			  System.out.print( leTab[i] +" ");
			  i++;
		  }
		  System.out.println("Si 54321 OK");
		  
		 System.out.println("test de inverse() pour les cas limites");
		 
		 System.out.println("Pour un tableau avec une valeur  ");
		 tab= new int [5];
		 tab[0] = 1;
		 nb = 1;
		 leTab=inverse( tab, nb);
		 i=0;
		 while (i<nb){
			  System.out.print( leTab[i] +" ");
			  i++;
		  }
		  System.out.println("Si 1 OK");
		  
		 System.out.println("test de inverse() pour les cas d'erreurs");
		 
		 System.out.println("Pour un tableau avec nombre d'element superieur a la longeur");
		 tab= new int [5];
		 nb = 20;
		 leTab=inverse( tab, nb);
		 System.out.println(" si erreur OK");
		 
		 
		 System.out.println("Pour un tableau avec nombre d'element inferieur ou egale a 0");
		 tab= new int [5];
		 nb = 0;
		 leTab=inverse( tab, nb);
		 System.out.println(" si erreur OK");
		 
		 System.out.println("Pour un tableau inexistant");
		 tab= null;
		 nb = 3;
      	 leTab=inverse( tab, nb);
		 System.out.println(" si erreur OK");	
	}
	
	/**
	* Echange les contenus des cases du tableau passé en paramétre, cases identifiées par les indices ind1 et ind2.
	* @param leTab : le tableau
	* @param nbElem : le nombre d'entier présents dans le tableau
	* @param ind1 : numero de la premiere case à echanger
	* @param ind2 : numero de la deuxieme case à echanger
	*/
	
	void echange ( int [] leTab, int nbElem, int ind1, int ind2){
		int c;
		boolean a;
		a=verifTab (leTab, nbElem);
		if (a== true){
			if (( ind1>=0) && (ind1<nbElem)){
				if (( ind2>=0) && (ind2<nbElem)){
					c =  leTab[ind1];
					leTab[ind1]=leTab[ind2];
					leTab[ind2] = c;
				}
				else {
					System.err.println("echange: ind2 invalide");
				}
			}
			else {
				System.err.println("echange: ind1 invalide");
			}
		}
		else{
			System.err.println("inverse: tableau invalide");
		}
	}
	
	/**
	* Test de la methode echange()
	*/	
	void testEchange(){
		int [] tab;
		int nb;
		int i;
		int ind1;
		int ind2;
		
		 System.out.println("test de Echange() pour les cas normaux");
		 
		 System.out.println("Pour un tableau rempli entre 1 et longeur-1 et indice qui marche");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 nb = 3;
		 ind1 = 0;
		 ind2 = 2;
		 echange ( tab, nb, ind1, ind2);
		 i = 0;
		 while (i<nb){
			  System.out.print( tab[i] +" ");
			  i++;
		  }
		  System.out.println("Si 321 OK");
		 
		
		 System.out.println("Pour un tableau rempli a la longeur-1 et indice qui marche");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 tab [3] = 4;
		 tab [4] = 5;
		 nb = 5;
		 ind1 = 4;
		 ind2 = 2;
		 echange ( tab, nb, ind1, ind2);
		 i=0;
		 while (i<nb){
			  System.out.print( tab[i] +" ");
			  i++;
		  }
		  System.out.println("Si 12543 OK");
		  
		 System.out.println("test de echange() pour les cas limites");
		 
		 System.out.println("Pour un tableau avec une valeur  et indice egale");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 tab [3] = 4;
		 tab [4] = 5;
		 nb = 5;
		 ind1 = 2;
		 ind2 = 2;
		 echange ( tab, nb, ind1, ind2);
		 i=0;
		 while (i<nb){
			  System.out.print( tab[i] +" ");
			  i++;
		  }
		  System.out.println("Si 12345 OK");
		
		 System.out.println("Pour un tableau avec une valeur  et indice ok");
		 tab= new int [5];
		 tab[0] = 1;
		 nb = 1;
		 ind1 = 0;
		 ind2 = 0;
		 echange ( tab, nb, ind1, ind2);
		 i=0;
		 while (i<nb){
			  System.out.print( tab[i] +" ");
			  i++;
		  }
		  System.out.println("Si 1 OK");
		  
		 System.out.println("test de echange() pour les cas d'erreurs");
		 
		 System.out.println("Pour un tableau OK et ind 1 invalide");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 tab [3] = 4;
		 tab [4] = 5;
		 nb = 5;
		 ind1 = 10;
		 ind2 = 2;
		 echange ( tab, nb, ind1, ind2);
		 System.out.println(" si erreur OK");
		 
		 System.out.println("Pour un tableau OK et ind 2 invalide");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 tab [3] = 4;
		 tab [4] = 5;
		 nb = 5;
		 ind1 = 2;
		 ind2 = -5;
		 echange ( tab, nb, ind1, ind2);
		 System.out.println(" si erreur OK");
		 
		 System.out.println("Pour un tableau avec nombre d'element superieur a la longeur");
		 tab= new int [5];
		 nb = 20;
		 ind1 = 2;
		 ind2 = 1;
		 echange ( tab, nb, ind1, ind2);
		 System.out.println(" si erreur OK");
		 
		 
		 System.out.println("Pour un tableau avec nombre d'element inferieur ou egale a 0");
		 tab= new int [5];
		 nb = 0;
		 ind1 = 2;
		 ind2 = 1;
		 echange ( tab, nb, ind1, ind2);
		 System.out.println(" si erreur OK");
		 
		 System.out.println("Pour un tableau inexistant");
		 tab= null;
		 nb = 3;
      	 ind1 = 2;
		 ind2 = 1;
		 echange ( tab, nb, ind1, ind2);
		 System.out.println(" si erreur OK");	
	}
	
			
	/**
	* Retourne n nouveau tableau qui a la même taille et les même occurences d'elements que le tableau passé en paramétre mais ces éléments sont répartis selon des indices aléatoires (0<= indice<= nbElem-1).
	* @param leTab : le tableau
	* @param nbElem : le nombre d'entier présents dans le tableau
	* @return le nouveau tableau qui a le même contenu que le tableau initial mais mélanger
	*/
	int[] melange( int [] leTab, int nbElem){
		boolean a;
		int [] tab;
		int ind1;
		int ind2;
		int i;
		
		a=verifTab (leTab, nbElem);
		if ( a == true){
			tab = copier(leTab, nbElem);
			i=0;
			while (i<100){
				ind1 = tirerAleatoire(0,nbElem-1);
				ind2 = tirerAleatoire(0,nbElem-1);
				echange ( tab, nbElem, ind1, ind2);
				i++;
			}
				
		}
		else{
			tab = null;
			System.err.println("melange: tableau invalide");
		}
		return tab;
	}

	/**
	* Test de la methode melange()
	*/
	void testMelange(){
		int [] tab;
		int nb;
		int i;
		int [] leTab;

		 System.out.println("test de Melange() pour les cas normaux");
		 
		 System.out.println("Pour un tableau rempli entre 1 et longeur-1");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 nb = 3;
		 leTab= melange( tab, nb);
		 i = 0;
		 while (i<nb){
			  System.out.print( leTab[i] +" ");
			  i++;
		  }
		  System.out.println("Si 123 dans le desordre OK");
		 
		
		 System.out.println("Pour un tableau rempli a la longeur-1 ");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 tab [3] = 4;
		 tab [4] = 5;
		 nb = 5;
		 leTab= melange( tab, nb);
		 i = 0;
		 while (i<nb){
			  System.out.print( leTab[i] +" ");
			  i++;
		  }
		  System.out.println("Si 12345 dans le desordre OK");
		 
		 System.out.println("Pour un tableau rempli a la longeur-1 avec des occurence");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 1;
		 tab [3] = 4;
		 tab [4] = 5;
		 nb = 5;
		 leTab= melange( tab, nb);
		 i = 0;
		 while (i<nb){
			  System.out.print( leTab[i] +" ");
			  i++;
		  }
		  System.out.println("Si 11245 dans le desordre OK");
		 
		  
		 System.out.println("test de Melange() pour les cas limites");
		
		 System.out.println("Pour un tableau rempli a la longeur-1 avec une valeur");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 1;
		 tab [2] = 1;
		 tab [3] = 1;
		 tab [4] = 1;
		 nb = 5;
		 leTab= melange( tab, nb);
		 i = 0;
		 while (i<nb){
			  System.out.print( leTab[i] +" ");
			  i++;
		  }
		  System.out.println("Si 11111 dans le desordre OK");
		
		 System.out.println("Pour un tableau avec une valeur");
		 tab= new int [5];
		 tab[0] = 1;
		 nb = 1;
		 leTab= melange( tab, nb);
		 i = 0;
		 while (i<nb){
			  System.out.print( leTab[i] +" ");
			  i++;
		 }
		 System.out.println("Si 1 OK");
		  
		 System.out.println("test de Melange() pour les cas d'erreurs");
		 
		 System.out.println("Pour un tableau avec nombre d'element superieur a la longeur");
		 tab= new int [5];
		 nb = 20;
		 leTab= melange( tab, nb);
		 System.out.println("erreur OK");
		 
		 
		 System.out.println("Pour un tableau avec nombre d'element inferieur ou egale a 0");
		 tab= new int [5];
		 nb = 0;
		 leTab= melange( tab, nb);
		 System.out.println("erreur OK");
		 
		 System.out.println("Pour un tableau inexistant");
		 tab= null;
		 nb = 3;
      	 leTab= melange( tab, nb);
		 System.out.println("erreur OK");	
	}
	
	/**
	* Décale de une case de la droite vers la gauche toutes les cases d'un tableau à partir d'un indice "ind" et jusque nbElem-1 ([ind]<-[ind+1]<-[ind+2]<-...<-[nbElem-2]<-[nbElem-1])
	* @param leTab : le tableau
	* @param nbElem : le nombre d'entier présents dans le tableau
	* @param ind : l'indice à partir duquel commence le décalage à gauche
	*/
	void decalerGch( int [] leTab, int nbElem, int ind){
		boolean a;
		int i;
		a=verifTab (leTab, nbElem);
		if ( a== true){
			if ((ind >=0) &&(ind <nbElem -1)){
				i=ind;
				while ( i <nbElem -1){
					leTab [i] = leTab [i+1];
					i++;
				}
			}
			else{
				System.err.println("decalerGch: indice invalide");
			}
		}
		else{
			System.err.println("decalerGch: tableau invalide");
		}
	}
	/**
	* Test de la methode decalerGch()
	*/
	void testDecalerGch(){
		int [] tab;
		int nb;
		int i;
		int ind;
;
		 System.out.println("test de DecalerGch() pour les cas normaux");
		 
		 System.out.println("Pour un tableau rempli entre 1 et longeur-1 ind = nbElem-2");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 nb = 3;
		 ind = 1;
		 decalerGch( tab, nb,ind);
		 i = 0;
		 while (i<nb-1){
			  System.out.print( tab[i] +" ");
			  i++;
		  }
		 System.out.println("Si 13 OK");
		 
		 System.out.println("Pour un tableau rempli entre 1 et longeur-1 ind = 0");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 nb = 3;
		 ind = 0;
		 decalerGch( tab, nb,ind);
		 i = 0;
		 while (i<nb-1){
			  System.out.print( tab[i] +" ");
			  i++;
		  }
		 System.out.println("Si 23 OK");
		
		 System.out.println("Pour un tableau rempli a la longeur-1 0< ind<nbElem-2");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 tab [3] = 4;
		 tab [4] = 5;
		 nb = 5;
		 ind= 2;
		 decalerGch( tab, nb, ind);
		 i = 0;
		 while (i<nb-1){
			  System.out.print( tab[i] +" ");
			  i++;
		  }
		 System.out.println("Si 1245 OK");
		  
		 System.out.println("test de DecalerGch() pour les cas d'erreurs");
		 
		 System.out.println("Pour un tableau OK et ind superieur a nbElem ");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 tab [3] = 4;
		 tab [4] = 5;
		 nb = 5;
		 ind= -5;
		 decalerGch( tab, nb, ind);
		 System.out.println("Si erreur OK");
		 
		 System.out.println("Pour un tableau OK et ind inferieur a 0");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 tab [3] = 4;
		 tab [4] = 5;
		 nb = 5;
		 ind= -5;
		 decalerGch( tab, nb, ind);
		 System.out.println("Si erreur OK");
		 
		 System.out.println("Pour un tableau avec nombre d'element superieur a la longeur");
		 tab= new int [5];
		 nb = 20;
		 ind= 2;
		 decalerGch( tab, nb, ind);
		 System.out.println("Si erreur ok");
		 		 
		 System.out.println("Pour un tableau avec nombre d'element inferieur ou egale a 0");
		 tab= new int [5];
		 nb = 0;
		 ind= 2;
		 decalerGch( tab, nb, ind);
		 System.out.println("Si erreur ok");
		 
		 System.out.println("Pour un tableau inexistant");
		 tab= null;
		 nb = 3;
      	 ind= 2;
      	 decalerGch( tab, nb, ind);
		 System.out.println("Si erreur ok");
		 	
	}
		

	/**
	* Supprime du tableau la première case rencontrée dont le contenu est égale à "valeur". La case du tableau est supprimée par décalage à gauche des cases du tableau.
	* @param leTab : le tableau
	* @param nbElem : le nombre d'entier présents dans le tableau
	* @param valeur : le contenu de la première case à supprimer
	* @return le nombre d'éléments dans le tableau (éventuellement inchangé)
	*/
	int suprimerValeur( int[] leTab, int nbElem, int valeur){
		int res;
		int i;
		boolean a;
		boolean b;
		a=verifTab (leTab, nbElem);
		if ( a== true){
			i=0;
			b= false;
			while ((i <nbElem) && (b==false)){
				if (leTab[i] ==valeur){
					b=true;
				}
				else{
					i++;
				}
			}
			if ((b==true) && (i>0)){
				decalerGch( leTab, nbElem, i);
				res=nbElem-1;
			}
			else if ((b == true) && (i==0)) {
				leTab = null;
				res = 0;
				System.err.println("suprimerValeur: tableau devien inexistant");
				
			}
			else {
				res = nbElem;
			}
		}
		else {
			res=-1;
			System.err.println("suprimerValeur: tableau invalide");
		}
		return res;
	}
	/**
	* Test de la methode SuprimerValeur()
	*/
	void testSuprimerValeur(){
		int [] tab;
		int nb;
		int i;
		int v;
		int res;
		 System.out.println("test de SuprimerValeur() pour les cas normaux");
		 
		 System.out.println("Pour un tableau rempli entre 1 et longeur-1 valeur presente");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 nb = 3;
		 v= 2;
		 res= suprimerValeur( tab, nb, v);
		 i = 0;
		 while (i<res){
			  System.out.print( tab[i] +" ");
			  i++;
		  }
		  System.out.println("Si 13 OK");
		 if (res == (nb-1)){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }	
		 
		
		 System.out.println("Pour un tableau rempli a la longeur-1 valeur presente");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 2;
		 tab [2] = 3;
		 tab [3] = 4;
		 tab [4] = 5;
		 nb = 5;
		 v= 2;
		 res=suprimerValeur( tab, nb, v);
		 i = 0;
		 while (i<res){
			  System.out.print( tab[i] +" ");
			  i++;
		  }
		  System.out.println("Si 1345 OK");
		  if (res == (nb-1)){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		  
		 System.out.println("test de SuprimerValeur() pour les cas limites");
		 
		 System.out.println("Pour un tableau avec une valeur  et valeur presente");
		 tab= new int [5];
		 tab [0] = 2;
		 nb = 1;
		 v= 2;
		 res=suprimerValeur( tab, nb, v);
		  if (res == (nb-1)){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		
		 System.out.println("Pour un tableau rempli a la longeur-1 valeur inexistante");
		 tab= new int [5];
		 tab [0] = 1;
		 tab [1] = 5;
		 tab [2] = 3;
		 tab [3] = 4;
		 tab [4] = 5;
		 nb = 5;
		 v= 2;
		 res=suprimerValeur( tab, nb, v);
		 i = 0;
		 while (i<res){
			  System.out.print( tab[i] +" ");
			  i++;
		  }
		  System.out.println("Si 12345 OK");
		  if (res == (nb)){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		  
		 System.out.println("test de SuprimerValeur() pour les cas d'erreurs");
		 
		 
		 System.out.println("Pour un tableau avec nombre d'element superieur a la longeur");
		 tab= new int [5];
		 nb = 20;
		 v= 2;
		 res=suprimerValeur( tab, nb, v);
		 if (res == -1){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 
		 
		 System.out.println("Pour un tableau avec nombre d'element inferieur ou egale a 0");
		 tab= new int [5];
		 nb = 0;
		 v= 2;
		 res=suprimerValeur( tab, nb, v);
		 if (res == -1){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 
		 System.out.println("Pour un tableau inexistant");
		 tab= null;
		 nb = 3;
      	 v= 2;
      	 res=suprimerValeur( tab, nb, v);
		 if (res == -1){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 	
	}

	/**
	* Supprime du tableau la première case rencontrée dont le contenu est égale à "valeur". La case du tableau est supprimée par décalage à gauche des cases du tableau.
	* @param tab1 : le premier tableau
	* @param tab2 : le deuxieme tableau
	* @param nbElem1 : le nombre d'entier présents dans le tableau1
	* @param nbElem2 : le nombre d'entier présents dans le tableau2
	* @return vrai si et seulement si tableau1 est inclus dans tableau2
	*/
	boolean inclusion( int[] tab1, int[] tab2, int nbElem1, int nbElem2){
		boolean res;
		boolean a;
		boolean b;	
		boolean d;
		int i;
		int c;
		int j;
		a=verifTab (tab1, nbElem1);
		b=verifTab (tab2, nbElem2);
		if ((a==true) && (b==true)){
			i=0;
			while (i<nbElem1-1){
				j=0;
				while (j<nbElem1-1){
					if (tab1 [i] > tab1 [j]){
						echange ( tab1, nbElem1, i, j);
					}
					if (tab2 [i] > tab2 [j]){
						echange ( tab2, nbElem2, i, j);
					}
					j++;
				}
				
				i++;			
			}	
			
			d=egalite(tab1,tab2,nbElem1,nbElem2);
			if ( d == true){
				res = true;
			}
			else {
				res = false;
			}
		}
		else{
			res=false;
			System.err.println("inclusion: tableau invalide");
		}
		return res;
	}
	/**
	* Test de la methode Iclusion()
	*/
	void testInclusion(){
		int [] tab1;
		int nb1;
		int [] tab2;
		int nb2;
		boolean res;
		 System.out.println("test de SuprimerValeur() pour les cas normaux");
		 
		 System.out.println("Pour deux tableau rempli entre 1 et longeur-1 et egale");
		 tab1 = new int [5];
		 tab1 [0] = 1;
		 tab1 [1] = 2;
		 tab1 [2] = 3;
		 nb1 = 3;
		 tab2= new int [5];
		 tab2 [0] = 2;
		 tab2 [1] = 1;
		 tab2 [2] = 3;
		 nb2 = 3;
		 res= inclusion( tab1, tab2, nb1, nb2);
		 
		 if (res == true){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }	
		 
		 System.out.println("Pour deux tableau rempli entre 1 et longeur-1 et different");
		 tab1 = new int [5];
		 tab1 [0] = 1;
		 tab1 [1] = 2;
		 tab1 [2] = 3;
		 nb1 = 3;
		 tab2= new int [5];
		 tab2 [0] = 2;
		 tab2 [1] = 3;
		 tab2 [2] = 3;
		 nb2 = 3;
		 res= inclusion( tab1, tab2, nb1, nb2);
		 
		 if (res == false){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }	
		
		 System.out.println("Pour un tableau rempli a la longeur-1 egale (avec loccurence)");
		 tab1= new int [5];
		 tab1 [0] = 1;
		 tab1 [1] = 3;
		 tab1 [2] = 3;
		 tab1 [3] = 2;
		 tab1 [4] = 5;
		 nb1 = 5;
		 tab2= new int [5];
		 tab2 [0] = 1;
		 tab2 [1] = 2;
		 tab2 [2] = 3;
		 tab2 [3] = 3;
		 tab2 [4] = 5;
		 nb2 = 5;
		 res=inclusion( tab1, tab2, nb1, nb2);
		 if (res == true){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 
		 System.out.println("Pour deux tableau avec nombre de valeur diferent");
		 tab1 = new int [5];
		 tab1 [0] = 1;
		 tab1 [1] = 2;
		 tab1 [2] = 3;
		 nb1 = 3;
		 tab2= new int [5];
		 tab2 [0] = 2;
		 tab2 [1] = 3;
		 nb2 = 2;
		 res= inclusion( tab1, tab2, nb1, nb2);
		 
		 if (res == false){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 
		 System.out.println("test de SuprimerValeur() pour les cas limites");
		 
		 System.out.println("Pour deux tableau ravec une valeur et egale");
		 tab1= new int [5];
		 tab1 [0] = 1;
		 nb1 = 1;
		 tab2= new int [5];
		 tab2 [0] = 1;
		 nb2 = 1;
		 res= inclusion( tab1, tab2, nb1, nb2);
		 
		 if (res == true){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		  
		 System.out.println("Pour deux tableau avec une valeur et différent");
		 tab1= new int [5];
		 tab1 [0] = 1;
		 nb1 = 1;
		 tab2= new int [5];
		 tab2 [0] = 4;
		 nb2 = 1;
		 res= inclusion( tab1, tab2, nb1, nb2);
		 
		 if (res == false ){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 System.out.println("test de SuprimerValeur() pour les cas d'erreurs");
		 
		 
		 System.out.println("Pour un tableau avec nombre d'element superieur a la longeur");
		 tab1= new int [5];
		 nb1 = 20;
		 tab2= new int [5];
		 tab2 [0] = 2;
		 tab2 [1] = 3;
		 nb2 = 2;
		 res= inclusion( tab1, tab2, nb1, nb2);
		 
		 if (res == false){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 
		 
		 System.out.println("Pour un tableau avec nombre d'element inferieur ou egale a 0");
		 tab1 = new int [5];
		 nb1 = 0;
		 tab2= new int [5];
		 tab2 [0] = 2;
		 tab2 [1] = 3;
		 nb2 = 2;
		 res= inclusion( tab1, tab2, nb1, nb2);
		 
		 if (res == false){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
		 
		 System.out.println("Pour un tableau des deux tableau inexistant");
		 tab1= null;
		 nb1 = 3;
      	 tab2= new int [5];
		 tab2 [0] = 2;
		 tab2 [1] = 3;
		 nb2 = 2;
		 res= inclusion( tab1, tab2, nb1, nb2);
		 
		 if (res == false){
			 System.out.println("test ok");
		 }
		 else {
			 System.out.println("erreur");
		 }
	}
	/**
	*A partir d'un tableau déjà créé, remplir le tableau de nbElem valeurs saisies par l'utilisateur. Au fur et a mesure de la saisie, les valeurs doivent être rangées par ordre croissant.
	* @param leTab : le tableau à remplir d'éléments dans l'ordre croissant
	* @param nbElem : le nombre d'entier que contiendra le tableau
	*/
	void remplirEtTrier( int[] leTab, int nbElem){
		boolean a;
		int i;
		int j;
		boolean v;
		a=verifTab (leTab, nbElem);
		if ( a== true){
			i=0;
			while (	i < nbElem){
				leTab[i]=SimpleInput.getInt("Saisir une valeur a mettre dans le tableau");
				j=i;
				v= true;
				while ((j>= 1) && (v== true)){
					if (leTab[j-1] > leTab[j]){
						echange (leTab, nbElem, (j-1),j);
					}
					else{
						v=false;
					}
					j =j-1;
				}
				i++;
			}	
		}
		else{
			System.err.println("remplirEtTrier: tableau invalide");
		}
	}
	
	/**
	* Test de la methode remplirEtTrier()
	*/
	void testRemplirEtTrier(){
		int [] tab;
		int nb;
		int i;
		System.out.println("test de SuprimerValeur() pour les cas normaux");
		 
		 System.out.println("Pour deux tableau rempli entre 1 et longeur-1 deja trier");
		 tab = new int [5];
		 nb = 3;
		 System.out.println("Saisir 1 2 3");
		 remplirEtTrier( tab, nb);
		 i = 0;
		 while (i<nb){
			  System.out.print( tab[i] +" ");
			  i++;
		  }
		  System.out.println("Si1 2 3 OK");
		 
		 
		 System.out.println("Pour deux tableau rempli entre 1 et longeur-1 et non trier");
		 tab = new int [5];
		 nb = 3;
		 System.out.println("Saisir 1 3 2 OK");
		 remplirEtTrier( tab, nb);
		 i = 0;
		 while (i<nb){
			  System.out.print( tab[i] +" ");
			  i++;
		  }
		  System.out.println("SI 1 2 3 OK");
		
		 System.out.println("Pour un tableau rempli a la longeur-1 et trier");
		 tab = new int [5];
		 nb = 5;
		 System.out.println("Saisir 1 2 3 4 5");
		 remplirEtTrier( tab, nb);
		 i = 0;
		 while (i<nb){
			  System.out.print( tab[i] +" ");
			  i++;
		  }
		  System.out.println("Si 1 2 3 4 5 ok");
		 
		 System.out.println("Pour un tableau rempli a la longeur-1 et non trier");
		 tab = new int [5];
		 nb = 5;
		 System.out.println("Saisir 1 4 3 5 2");
		 remplirEtTrier( tab, nb);
		 i = 0;
		 while (i<nb){
			  System.out.print( tab[i] +" ");
			  i++;
		  }
		  System.out.println("Si 1 2 3 4 5 ok");
		 
		 System.out.println("test de SuprimerValeur() pour les cas limites");
		 
		 System.out.println("Pour deux tableau ravec une valeur ");
		 tab = new int [5];
		 nb = 1;
		 System.out.println("Saisir 1 ");
		 remplirEtTrier( tab, nb);
		 i = 0;
		 while (i<nb){
			  System.out.print( tab[i] +" ");
			  i++;
		  }
		  System.out.println("Si 1 OK");
		 
		 System.out.println("Pour un tableau rempli a la longeur-1 et non trier et avec l'occurence");
		 tab = new int [5];
		 nb = 5;
		 System.out.println("Saisir 1 5 3 2 2");
		 remplirEtTrier( tab, nb);
		 i = 0;
		 while (i<nb){
			  System.out.print( tab[i] +" ");
			  i++;
		  }
		  System.out.println("Si 1 2 2 3 5 OK ");
		  
		 System.out.println("test de SuprimerValeur() pour les cas d'erreurs");
		 
		 
		 System.out.println("Pour un tableau avec nombre d'element superieur a la longeur");
		 tab= new int [5];
		 nb = 20;
		 remplirEtTrier( tab, nb);
		 System.out.println("Si Erreur OK ");
		 
		 
		 System.out.println("Pour un tableau avec nombre d'element inferieur ou egale a 0");
		 tab = new int [5];
		 nb = 0;
		 remplirEtTrier( tab, nb);
		 System.out.println("Si Erreur OK ");
		 
		 System.out.println("Pour un tableau des deux tableau inexistant");
		 tab = null;
		 nb = 3;
      	 remplirEtTrier( tab, nb);
		 System.out.println("Si Erreur OK ");
	}
	
	/**
	*A partir d'un tableau déjà créé, remplir le tableau de nbElem valeurs saisies par l'utilisateur. Au fur et a mesure de la saisie, si la nouvelle valeur saisie existe déja dans le tableau alors ne pas l'insérer et demander de ressaisir.
	* @param leTab : le tableau
	* @param nbElem : le nombre d'entier présents dans le tableau
	*/
	void remplirToutesDiff( int[] leTab, int nbElem){
		int i;
		int j;
		boolean a;
		a=verifTab (leTab, nbElem);
		if ( a== true){
			i=0;
			while (i<nbElem){
				leTab[i]=SimpleInput.getInt("Saisir une valeur a mettre dans le tableau");
				j=0;
				while ((j<i)&& (i!=0)){
					if (leTab[i] == leTab [j]){
						leTab[i]=SimpleInput.getInt("Resaisir une autre valeur a mettre dans le tableau");
					}
					else{
						j++;
					}				
				}
				i++;
			}
		}
		else{
			System.err.println("remplirToutesDiff: tableau invalide");
		}
	}
	/**
	* Test de la methode remplirToutesDiff()
	*/
	void testRemplirToutesDiff(){
		int [] tab;
		int nb;
		int i;
		
		System.out.println("test de SuprimerValeur() pour les cas normaux");
		System.out.println("Pour un tableau rempli entre 1 et longeur-1 avec toute les valeur differente");
		tab = new int [5];
		nb = 3;
		System.out.println("Saisir 1 2 3");
		remplirToutesDiff(tab, nb);
		i = 0;
		while (i<nb){
			  System.out.print( tab[i] +" ");
			  i++;
		 }
		 System.out.println("Si 1 2 3 OK");
		
		System.out.println("Pour un tableau rempli entre 1 et longeur-1 avec toute les valeur parreil");
		tab = new int [5];
		nb = 3;
		System.out.println("Saisir 1 2 2 3 ");
		remplirToutesDiff(tab, nb);
		i = 0;
		while (i<nb){
			  System.out.print( tab[i] +" ");
			  i++;
		 }
		 System.out.println("Si 1 2 3 OK");
		 
		System.out.println("Pour un tableau rempli a longeur-1 avec toute les valeur differente");
		tab = new int [5];
		nb = 5;
		System.out.println("Saisir 1 2 3 4 5");
		remplirToutesDiff(tab, nb);
		i = 0;
		while (i<nb){
			  System.out.print( tab[i] +" ");
			  i++;
		 }
		 System.out.println("Si 1 2 3 4 5 OK");
		
		System.out.println("Pour un tableau rempli a avec toute les valeur parreil");
		tab = new int [5];
		nb = 5;
		System.out.println("Saisir 1 2 2 3 3 4 5");
		remplirToutesDiff(tab, nb);
		i = 0;
		while (i<nb){
			  System.out.print( tab[i] +" ");
			  i++;
		 }
		 System.out.println("Si 1 2 3 4 5 OK");
		 
		 System.out.println("test de SuprimerValeur() pour les cas limites");
		 
		 System.out.println("Pour deux tableau ravec une valeur ");
		 tab = new int [5];
		 nb = 1;
		 System.out.println("Saisir 1 ");
		 remplirToutesDiff( tab, nb);
		 i = 0;
		 while (i<nb){
			  System.out.print( tab[i] +" ");
			  i++;
		  }
		  System.out.println("Si 1 OK");
		
		 System.out.println("test de SuprimerValeur() pour les cas d'erreurs");
		 
		 
		 System.out.println("Pour un tableau avec nombre d'element superieur a la longeur");
		 tab= new int [5];
		 nb = 20;
		 remplirToutesDiff( tab, nb);
		 System.out.println("Si Erreur OK ");
		 
		 
		 System.out.println("Pour un tableau avec nombre d'element inferieur ou egale a 0");
		 tab = new int [5];
		 nb = 0;
		 remplirToutesDiff( tab, nb);
		 System.out.println("Si Erreur OK ");
		 
		 System.out.println("Pour un tableau des deux tableau inexistant");
		 tab = null;
		 nb = 3;
      	remplirToutesDiff( tab, nb);
		 System.out.println("Si Erreur OK ");
	}
}



