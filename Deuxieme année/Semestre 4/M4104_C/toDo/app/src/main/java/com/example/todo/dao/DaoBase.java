package com.example.todo.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.todo.base.DatabaseHandler;


public abstract class DaoBase {
    //version de la base
    protected final static int VERSION = 12;
    // Le nom du fichier qui représente ma base
    protected final static String NOM = "mabase.db";

    protected SQLiteDatabase mDb = null;
    protected DatabaseHandler mHandler = null;

    //constructeur
    public DaoBase(Context pContext) {
        this.mHandler = new DatabaseHandler(pContext, NOM, null, VERSION);
    }

    //ouvre la base
    public SQLiteDatabase open() {
        // Pas besoin de fermer la dernière base puisque getWritableDatabase s'en charge
        mDb = mHandler.getWritableDatabase();
        return mDb;
    }

    //ferme la base
    public void close() {
        mDb.close();
    }

}
