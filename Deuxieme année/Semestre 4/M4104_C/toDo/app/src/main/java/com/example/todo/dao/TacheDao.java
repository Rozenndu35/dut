package com.example.todo.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.example.todo.model.Tache;

import java.util.ArrayList;

public class TacheDao extends DaoBase{
    public static final String TABLE_NAME = "Tache";
    public static final String TACHE_KEY = "id";
    public static final String TACHE_NOM = "nom";
    public static final String TACHE_LONGUEUR = "longueur";
    public static final String TACHE_DESCRIPTION = "description";
    public static final String TACHE_JOUR = "jour";
    public static final String TACHE_MOIS = "mois";
    public static final String TACHE_ANNEE = "annee";
    public static final String TACHE_PROJET = "projet";
    public static final String TACHE_CONTEXT = "context";
    private Context pContext;

    //constructeur
    public TacheDao(Context pContext) {
        super(pContext);
        this.pContext=pContext;
    }

    /**
     * @param t la tache à ajouter à la base
     */
    public void ajouter(Tache t) {
        open();
        ContentValues value = new ContentValues();
        value.put(TacheDao.TACHE_NOM, t.getNom());
        value.put(TacheDao.TACHE_LONGUEUR, t.getDuree());
        value.put(TacheDao.TACHE_DESCRIPTION, t.getDescription());
        value.put(TacheDao.TACHE_JOUR, t.getJour());
        value.put(TacheDao.TACHE_MOIS, t.getMois());
        value.put(TacheDao.TACHE_ANNEE, t.getAnnee());
        value.put(TacheDao.TACHE_PROJET, t.getProjet());
        value.put(TacheDao.TACHE_CONTEXT, t.getContext());

        mDb.insert(TacheDao.TABLE_NAME, null, value);

        close();

    }

    /**
     * @param id l'identifiant de la tache à supprimer
     */
    public void supprimer(long id , String projet , String context) {
        open();
        mDb.delete(TABLE_NAME, TACHE_KEY + " = ?", new String[] {String.valueOf(id)});

        close();
    }

    /**
     * @param t le métier modifié
     */
    public void modifier(Tache t) {
        open();
        ContentValues value = new ContentValues();
        value.put(TacheDao.TACHE_NOM, t.getNom());
        value.put(TacheDao.TACHE_LONGUEUR, t.getDuree());
        value.put(TacheDao.TACHE_DESCRIPTION, t.getDescription());
        value.put(TacheDao.TACHE_JOUR, t.getJour());
        value.put(TacheDao.TACHE_MOIS, t.getMois());
        value.put(TacheDao.TACHE_ANNEE, t.getAnnee());
        value.put(TacheDao.TACHE_PROJET, t.getProjet());
        value.put(TacheDao.TACHE_CONTEXT, t.getContext());

        mDb.update(TABLE_NAME, value, TACHE_KEY  + " = ?", new String[] {String.valueOf(t.getId())});
        close();
    }
    /**
     * @param t le métier modifié
     */
    public void modifierEtat(Tache t) {
        open();
        ContentValues value = new ContentValues();
        value.put(TacheDao.TACHE_NOM, t.getNom());
        value.put(TacheDao.TACHE_LONGUEUR, t.getDuree());
        value.put(TacheDao.TACHE_DESCRIPTION, t.getDescription());
        value.put(TacheDao.TACHE_JOUR, t.getJour());
        value.put(TacheDao.TACHE_MOIS, t.getMois());
        value.put(TacheDao.TACHE_ANNEE, t.getAnnee());
        value.put(TacheDao.TACHE_PROJET, t.getProjet());
        value.put(TacheDao.TACHE_CONTEXT, t.getContext());

        mDb.update(TABLE_NAME, value, TACHE_KEY  + " = ?", new String[] {String.valueOf(t.getId())});
        close();
    }


    /**
     * @param nom le nom de la tache à récupérer
     */
    public Tache selectionnerNom(String nom) {
        open();
        Cursor cursor = mDb.rawQuery("select *  from " + TABLE_NAME + " where "+ TACHE_NOM + " = ?",  new String[]{nom});
        Tache t = null;
        if(cursor.moveToNext()){
            long id = cursor.getLong(0);
            String duree = cursor.getString(2);
            String description = cursor.getString(3);
            String jour = cursor.getString(4);
            String mois = cursor.getString(5);
            String annee = cursor.getString(6);
            String projet = cursor.getString(7);
            String context = cursor.getString(8);
            t = new Tache (id, nom , duree , description , jour , mois , annee , projet , context);
        }

        cursor.close();
        close();
        return t;
    }
    /**
     * @param context le context de la tache à récupérer
     */
    public ArrayList<Tache> selectionnerContext(String context){
        open();
        Cursor cursor = mDb.rawQuery("select *  from " + TABLE_NAME + " where "+ TACHE_CONTEXT + " = ?",  new String[]{context});
        ArrayList list = new ArrayList<Tache>();
        while (cursor.moveToNext()) {
            long id = cursor.getLong(0);
            String nom = cursor.getString(1);
            String duree = cursor.getString(2);
            String description = cursor.getString(3);
            String jour = cursor.getString(4);
            String mois = cursor.getString(5);
            String annee = cursor.getString(6);
            String projet = cursor.getString(7);
            Tache t = new Tache (id, nom , duree , description , jour , mois , annee , projet , context);
            list.add(t);
        }
        cursor.close();
        close();
        return list;
    }

    /**
     * @param projet le projet de la tache à récupérer
     */
    public ArrayList<Tache> selectionnerProjet(String projet) {
        open();
        Cursor cursor = mDb.rawQuery("select *  from " + TABLE_NAME + " where "+ TACHE_PROJET + " = ?",  new String[]{projet});
        ArrayList list = new ArrayList<Tache>();
        while (cursor.moveToNext()) {
            long id = cursor.getLong(0);
            String nom = cursor.getString(1);
            String duree = cursor.getString(2);
            String description = cursor.getString(3);
            String jour = cursor.getString(4);
            String mois = cursor.getString(5);
            String annee = cursor.getString(6);
            String context = cursor.getString(8);
            Tache t = new Tache (id, nom , duree , description , jour , mois , annee , projet , context);
            list.add(t);
        }
        cursor.close();
        close();
        return list;
    }

    /**
     * @param context le context de la tache à récupérer
     * @param projet le projet de la tache à récupérer
     */
    public ArrayList<Tache> selectionnerContextProjet(String context, String projet){
        open();
        Cursor cursor = mDb.rawQuery("select *  from " + TABLE_NAME + " where "+ TACHE_CONTEXT + " = ? AND " + TACHE_PROJET + " = ?;",  new String[]{context, projet});
        ArrayList list = new ArrayList<Tache>();
        while (cursor.moveToNext()) {
            long id = cursor.getLong(0);
            String nom = cursor.getString(1);
            String duree = cursor.getString(2);
            String description = cursor.getString(3);
            String jour = cursor.getString(4);
            String mois = cursor.getString(5);
            String annee = cursor.getString(6);
            Tache t = new Tache (id, nom , duree , description , jour , mois , annee , projet , context);
            list.add(t);
        }
        cursor.close();
        close();
        return list;
    }

    //retourne tout les taches
    public ArrayList<Tache> selectionnerAll() {
        open();
        Cursor cursor = mDb.rawQuery("SELECT * FROM "+TABLE_NAME, new String[]{});
        ArrayList list = new ArrayList<Tache>();
        while (cursor.moveToNext()) {
            long id = cursor.getLong(0);
            String nom = cursor.getString(1);
            String duree = cursor.getString(2);
            String description = cursor.getString(3);
            String jour = cursor.getString(4);
            String mois = cursor.getString(5);
            String annee = cursor.getString(6);
            String projet = cursor.getString(7);
            String context = cursor.getString(8);
            Tache t = new Tache (id, nom , duree , description , jour , mois , annee , projet , context);
            list.add(t);
        }
        close();
        return list;
    }

}
