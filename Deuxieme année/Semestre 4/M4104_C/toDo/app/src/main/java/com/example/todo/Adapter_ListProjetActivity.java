package com.example.todo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.todo.model.Projet;

import java.util.List;


public class Adapter_ListProjetActivity extends BaseAdapter {

    private List<Projet> listProjet;
    private Context context; //etat de l'application
    private LayoutInflater inflater;

    public Adapter_ListProjetActivity(Context context, List<Projet> list) {
        this.context = context;
        this.listProjet = list;
        this.inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return this.listProjet.size();
    }

    @Override
    public Object getItem(int position) {
        return this.listProjet.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        //initialise tache_list_item
        if (convertView == null) {
            view = (View) this.inflater.inflate(R.layout.projet_list_item, parent, false);
        } else {
            view = (View) convertView;
        }

        //initialise les vue du layout

        TextView nom= (TextView) view.findViewById(R.id.nom);

        //modification des vue
        nom.setText((listProjet.get(position).getNom()));


        //on retourne la vue creer
        return view;

    }
}