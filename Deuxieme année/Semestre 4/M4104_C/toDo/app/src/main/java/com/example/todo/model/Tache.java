package com.example.todo.model;

public class Tache {
    private long id;
    private String nom;
    private String duree;
    private String description;
    private String jour;
    private String mois;
    private String annee;
    private String projet;
    private String context;

    public Tache(long id,String nom,String duree,String description,String jour , String mois , String annee,String projet,String context) {
        super();
        this.id = id;
        this.nom = nom;
        this.duree = duree;
        this.description = description;
        this.jour = jour;
        this.mois = mois;
        this.annee = annee;
        this.projet = projet;
        this.context=context;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getProjet() {
        return projet;
    }

    public void setProjet(String projet) {
        this.projet = projet;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getJour() {
        return jour;
    }

    public void setJour(String jour) {
        this.jour = jour;
    }

    public String getMois() {
        return mois;
    }

    public void setMois(String mois) {
        this.mois = mois;
    }

    public String getAnnee() {
        return annee;
    }

    public void setAnnee(String annee) {
        this.annee = annee;
    }


    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

}
