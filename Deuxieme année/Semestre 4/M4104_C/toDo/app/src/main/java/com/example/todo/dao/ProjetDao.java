package com.example.todo.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.example.todo.model.Projet;

import java.util.ArrayList;

public class ProjetDao extends DaoBase{
    public static final String TABLE_NAME = "Projet";
    public static final String PROJET_KEY = "id";
    public static final String PROJET_NOM = "nom";

    //constructeur
    public ProjetDao(Context pContext) {
        super(pContext);
    }

    /**
     * @param p la tache à ajouter à la base
     */
    public void ajouter(Projet p) {
        open();
        ContentValues value = new ContentValues();
        value.put(ProjetDao.PROJET_NOM, p.getNom());
        mDb.insert(ProjetDao.TABLE_NAME, null, value);

        close();

    }

    //retourne tout les projet
    public ArrayList<Projet> selectionnerAllProject() {
        open();
        Cursor cursor = mDb.rawQuery("SELECT * FROM "+TABLE_NAME, new String[]{});
        ArrayList list = new ArrayList<Projet>();
        while (cursor.moveToNext()) {
            long id = cursor.getLong(0);
            String nom = cursor.getString(1);
            Projet p = new Projet (id, nom );
            list.add(p);
        }
        close();
        return list;
    }
    //retourne tout les projet
    public ArrayList<String> selectionnerAllnom() {
        open();
        Cursor cursor = mDb.rawQuery("SELECT * FROM "+TABLE_NAME, new String[]{});
        ArrayList list = new ArrayList<String>();
        while (cursor.moveToNext()) {
            list.add(cursor.getString(1));
        }
        close();
        return list;
    }

}
