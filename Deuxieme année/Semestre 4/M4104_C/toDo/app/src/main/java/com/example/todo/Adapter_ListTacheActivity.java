package com.example.todo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.todo.model.Tache;
import java.util.List;


public class Adapter_ListTacheActivity extends BaseAdapter {
    private List<Tache> listTache;
    private Context context; //etat de l'application
    private LayoutInflater inflater;

    public Adapter_ListTacheActivity(Context context, List<Tache> list) {
        this.context = context;
        this.listTache = list;
        this.inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return this.listTache.size();
    }

    @Override
    public Object getItem(int position) {
        return this.listTache.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        //initialise tache_list_item
        if (convertView == null) {
            view = (View) this.inflater.inflate(R.layout.tache_list_item, parent, false);
        } else {
            view = (View) convertView;
        }

        //initialise les vue du layout


        TextView titre = (TextView) view.findViewById(R.id.nom);
        TextView date = (TextView) view.findViewById(R.id.date);
        TextView durree = (TextView) view.findViewById(R.id.duree);

        //modification des vue
        titre.setText((listTache.get(position).getNom()));
        date.setText(listTache.get(position).getJour() + "/" + listTache.get(position).getMois() + "/" + listTache.get(position).getAnnee());
        durree.setText(listTache.get(position).getDuree());


        //on retourne la vue creer
        return view;

    }
}