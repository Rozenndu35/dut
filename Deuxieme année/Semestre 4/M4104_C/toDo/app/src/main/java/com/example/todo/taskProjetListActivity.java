package com.example.todo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;


import com.example.todo.dao.TacheDao;
import com.example.todo.model.Tache;

import java.util.ArrayList;

public class taskProjetListActivity extends AppCompatActivity {
    private int REQUEST_CODE = 0;
    private ListView list;
    private Adapter_ListTacheActivity adapter;
    private taskProjetListActivity cela = this;
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taskprojetlist);

        TacheDao td = new TacheDao(this);
        Bundle bundle = getIntent().getExtras();
        ArrayList<Tache> tacheArrayList = td.selectionnerProjet(bundle.getString("projet"));

        //initialisation de l addapter pour tache
        this.adapter = new Adapter_ListTacheActivity(this, tacheArrayList);
        //recuperation de la listView
        this.list = (ListView)findViewById(R.id.tacheList);
        //passage des donne a la liste
        list.setAdapter(this.adapter);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Tache selected = (Tache) adapter.getItem(position);
                Intent activiterId = new Intent(cela, taskDescActivity.class);
                activiterId.putExtra("id",Long.toString(selected.getId()));
                activiterId.putExtra("nom", selected.getNom());
                activiterId.putExtra("duree", selected.getDuree());
                activiterId.putExtra("description", selected.getDescription());
                activiterId.putExtra("jour", selected.getJour());
                activiterId.putExtra("mois", selected.getMois());
                activiterId.putExtra("annee", selected.getAnnee());
                activiterId.putExtra("projet", selected.getProjet());
                activiterId.putExtra("context", selected.getContext());
                startActivityForResult(activiterId, REQUEST_CODE);

            }
        });
    }
    public void finish(){
        //retourne les infos
        Intent returnIntent = new Intent();

        setResult(RESULT_OK , returnIntent);
        super.finish();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

}
