package com.example.todo.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.example.todo.model.ContextD;
import java.util.ArrayList;

public class ContextDao extends DaoBase{
    public static final String TABLE_NAME = "Context";
    public static final String CONTEXT_KEY = "id";
    public static final String CONTEXT_NOM = "nom";

    //constructeur
    public ContextDao(Context pContext) {
        super(pContext);
    }

    /**
     * @param c la tache à ajouter à la base
     */
    public void ajouter(ContextD c) {
        open();
        ContentValues value = new ContentValues();
        value.put(ContextDao.CONTEXT_NOM, c.getNom());
        mDb.insert(ContextDao.TABLE_NAME, null, value);

        close();

    }

    //retourne tout les projet
    public ArrayList<String> selectionnerAllnom() {
        open();
        Cursor cursor = mDb.rawQuery("SELECT * FROM "+TABLE_NAME, new String[]{});
        ArrayList list = new ArrayList<String>();
        while (cursor.moveToNext()) {
            list.add(cursor.getString(1));
        }
        close();
        return list;
    }

}
