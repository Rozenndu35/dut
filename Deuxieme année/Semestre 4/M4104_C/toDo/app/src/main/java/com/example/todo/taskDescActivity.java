package com.example.todo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.todo.dao.TacheDao;
import com.example.todo.model.Tache;

import java.util.GregorianCalendar;

public class taskDescActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView nom;
    private TextView duree;
    private TextView description;
    private TextView context;
    private TextView projet;
    private DatePicker date;
    private Button modifier;
    private Button supprimer;
    private long id;
    private Tache tache;
    int REQUEST_CODE = 0;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tastkdescavtivity);

        this.nom = (TextView)findViewById(R.id.nomTache);
        this.duree = (TextView)findViewById(R.id.duree);
        this.context = (TextView)findViewById(R.id.context);
        this.projet = (TextView)findViewById(R.id.projet);
        this.description = (TextView)findViewById(R.id.description);
        this.date = (DatePicker)findViewById(R.id.date);
        this.modifier = (Button)findViewById(R.id.modifier) ;
        this.supprimer = (Button)findViewById(R.id.supprimer);
        this.modifier.setOnClickListener(this);
        this.supprimer.setOnClickListener(this);

        //recuperer les valeurs passer par le main
        Bundle bundle = getIntent().getExtras();
        this.id = Long.parseLong(bundle.getString("id"));
        this.nom.setText(bundle.getString("nom"));
        this.duree.setText(bundle.getString("duree"));
        this.description.setText(bundle.getString("description"));
        this.date.updateDate (Integer.parseInt(bundle.getString("annee")), Integer.parseInt(bundle.getString("mois")), Integer.parseInt(bundle.getString("jour")));
        GregorianCalendar dateT = new GregorianCalendar(Integer.parseInt(bundle.getString("annee")), Integer.parseInt(bundle.getString("mois")), Integer.parseInt(bundle.getString("jour")));
        this.date.setMaxDate(dateT.getTimeInMillis());
        this.date.setMinDate(dateT.getTimeInMillis());
        this.projet.setText(bundle.getString("projet"));
        this.context.setText(bundle.getString("context"));


        this.tache = new Tache(this.id,bundle.getString("nom") , bundle.getString("duree"), bundle.getString("description"), bundle.getString("jour") , bundle.getString("mois"), bundle.getString("annee"), bundle.getString("projet"),bundle.getString("context"));




    }

    public void onClick(View v) {
        if(v.getId() == R.id.supprimer){
            //pour supprimer
            TacheDao td = new TacheDao(this);
            td.supprimer(this.id, this.projet.getText().toString() , this.context.getText().toString());
            Toast.makeText(getApplicationContext(),"Tache supprimer" ,Toast.LENGTH_LONG).show();
            finish();

        } else if (v.getId() == R.id.modifier){
            //pour modifier

            Intent activiterId = new Intent(this, taskModifierActivity.class);
            activiterId.putExtra("id",Long.toString(this.tache.getId()));
            activiterId.putExtra("nom", this.tache.getNom());
            activiterId.putExtra("duree", this.tache.getDuree());
            activiterId.putExtra("description", this.tache.getDescription());
            activiterId.putExtra("jour", this.tache.getJour());
            activiterId.putExtra("mois", this.tache.getMois());
            activiterId.putExtra("annee", this.tache.getAnnee());
            activiterId.putExtra("projet", this.tache.getProjet());
            activiterId.putExtra("context", this.tache.getContext());
            startActivityForResult(activiterId, REQUEST_CODE);
            finish();

        }
    }

    public void finish(){
        //retourne les infos
        Intent returnIntent = new Intent();

        setResult(RESULT_OK , returnIntent);
        super.finish();
    }
    protected void onResume() {
        super.onResume();

    }

}