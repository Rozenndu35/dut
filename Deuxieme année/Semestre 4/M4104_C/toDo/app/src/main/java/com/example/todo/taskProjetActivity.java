package com.example.todo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


import androidx.appcompat.app.AppCompatActivity;

import com.example.todo.dao.ProjetDao;
import com.example.todo.model.Projet;

import java.util.ArrayList;

public class taskProjetActivity extends AppCompatActivity  {
    private int REQUEST_CODE = 0;
    private ListView list;
    private Adapter_ListProjetActivity adapter;
    private taskProjetActivity cela = this;
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projetlist);

        ProjetDao pd = new ProjetDao(this);
        ArrayList<Projet> projetArrayList = pd.selectionnerAllProject();

        //initialisation de l addapter pour tache
        this.adapter = new Adapter_ListProjetActivity(this, projetArrayList);
        //recuperation de la listView
        this.list = (ListView)findViewById(R.id.projetList);
        //passage des donne a la liste
        this.list.setAdapter(this.adapter);

        //on ajoute un listener
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent activiterId = new Intent(cela, taskProjetListActivity.class);
                Projet selected = (Projet)adapter.getItem(position);
                activiterId.putExtra("projet", selected.getNom());
                startActivityForResult(activiterId, REQUEST_CODE);

            }
        });
    }
}
