package com.example.todo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;


import com.example.todo.dao.ContextDao;
import com.example.todo.dao.ProjetDao;
import com.example.todo.dao.TacheDao;
import com.example.todo.model.Tache;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class taskListActivity extends AppCompatActivity implements View.OnClickListener {
    private int REQUEST_CODE = 0;
    private ListView list;
    private FloatingActionButton add;
    private Spinner context;
    private Spinner projet;
    private Button filtrer;
    private Adapter_ListTacheActivity adapter;
    private taskListActivity cela = this;
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasklist);

        TacheDao td = new TacheDao(this);
        ArrayList<Tache> tacheArrayList = td.selectionnerAll();

        //initialisation de l addapter pour tache
        this.adapter = new Adapter_ListTacheActivity(this, tacheArrayList);
        //recuperation de la listView
        this.list = (ListView)findViewById(R.id.tacheList);
        //passage des donne a la liste
        list.setAdapter(this.adapter);
        this.filtrer = (Button)findViewById(R.id.filtrer);
        this.add = (FloatingActionButton)findViewById(R.id.add);
        this.add.setOnClickListener(this);
        this.filtrer.setOnClickListener(this);

        //creation des spinner
        this.context = (Spinner)findViewById(R.id.context);
        ContextDao cd = new ContextDao(this);
        List<String> spinnerArrayContext =  cd.selectionnerAllnom();
        spinnerArrayContext.add("Context aucun");
        ArrayAdapter<String> adapterContext = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArrayContext);
        adapterContext.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.context.setAdapter(adapterContext);
        this.context.setSelection(spinnerArrayContext.size()-1);

        this.projet = (Spinner)findViewById(R.id.projet);
        ProjetDao pd = new ProjetDao(this);
        List<String> spinnerArrayProject =  pd.selectionnerAllnom();
        spinnerArrayProject.add("Projet aucun");
        ArrayAdapter<String> adapterProject = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArrayProject);
        adapterProject.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.projet.setAdapter(adapterProject);
        this.projet.setSelection(spinnerArrayContext.size()-1);


        //on ajoute un listener


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Tache selected = (Tache) adapter.getItem(position);
                Intent activiterId = new Intent(cela, taskDescActivity.class);
                TacheDao tdx = new TacheDao(cela);
                activiterId.putExtra("id",Long.toString(selected.getId()));
                activiterId.putExtra("nom", selected.getNom());
                activiterId.putExtra("duree", selected.getDuree());
                activiterId.putExtra("description", selected.getDescription());
                activiterId.putExtra("jour", selected.getJour());
                activiterId.putExtra("mois", selected.getMois());
                activiterId.putExtra("annee", selected.getAnnee());
                activiterId.putExtra("projet", selected.getProjet());
                activiterId.putExtra("context", selected.getContext());
                startActivityForResult(activiterId, REQUEST_CODE);

            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.add){
            //pour ajouter
            Intent ajouterAct = new Intent(this, taskCreateActivity.class);
            startActivity(ajouterAct);
        }else if(v.getId() == R.id.filtrer){
            Toast.makeText(this, "filtrer", Toast.LENGTH_SHORT).show();
            if( this.projet.getSelectedItem().toString().equals("Projet aucun")){
                if( this.context.getSelectedItem().toString().equals("Context aucun")){
                    //si rien de selectionner
                    TacheDao td = new TacheDao(this);
                    ArrayList<Tache> tacheArrayList = td.selectionnerAll();
                    //initialisation de l addapter pour tache
                    this.adapter = new Adapter_ListTacheActivity(this, tacheArrayList);
                    //recuperation de la listView
                    this.list = (ListView)findViewById(R.id.tacheList);
                    //passage des donne a la liste
                    list.setAdapter(this.adapter);
                }else{
                    //si un context
                    TacheDao td = new TacheDao(this);
                    ArrayList<Tache> tacheArrayList = td.selectionnerContext(this.context.getSelectedItem().toString());

                    //initialisation de l addapter pour tache
                    this.adapter = new Adapter_ListTacheActivity(this, tacheArrayList);
                    //recuperation de la listView
                    this.list = (ListView)findViewById(R.id.tacheList);
                    //passage des donne a la liste
                    list.setAdapter(this.adapter);
                }
            }else {
                if( this.context.getSelectedItem().toString() .equals("Context aucun")){
                    //si un projet selectionner
                    TacheDao td = new TacheDao(this);
                    ArrayList<Tache> tacheArrayList = td.selectionnerProjet(this.projet.getSelectedItem().toString());

                    //initialisation de l addapter pour tache
                    this.adapter = new Adapter_ListTacheActivity(this, tacheArrayList);
                    //recuperation de la listView
                    this.list = (ListView)findViewById(R.id.tacheList);
                    //passage des donne a la liste
                    list.setAdapter(this.adapter);
                }else{
                    TacheDao td = new TacheDao(this);
                    ArrayList<Tache> tacheArrayList = td.selectionnerContextProjet(this.context.getSelectedItem().toString(), this.projet.getSelectedItem().toString());

                    //initialisation de l addapter pour tache
                    this.adapter = new Adapter_ListTacheActivity(this, tacheArrayList);
                    //recuperation de la listView
                    this.list = (ListView)findViewById(R.id.tacheList);
                    //passage des donne a la liste
                    list.setAdapter(this.adapter);
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        TacheDao td = new TacheDao(this);
        ArrayList<Tache> tacheArrayList = td.selectionnerAll();
        //initialisation de l addapter pour tache
        this.adapter = new Adapter_ListTacheActivity(this, tacheArrayList);
        //recuperation de la listView
        this.list = (ListView)findViewById(R.id.tacheList);
        //passage des donne a la liste
        list.setAdapter(this.adapter);
    }
}
