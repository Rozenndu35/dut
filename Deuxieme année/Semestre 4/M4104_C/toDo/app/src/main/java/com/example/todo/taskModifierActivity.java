package com.example.todo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.todo.dao.ContextDao;
import com.example.todo.dao.ProjetDao;
import com.example.todo.dao.TacheDao;
import com.example.todo.model.ContextD;
import com.example.todo.model.Projet;
import com.example.todo.model.Tache;

import java.util.List;


public class taskModifierActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private EditText nom;
    private EditText duree;
    private EditText description;
    private EditText addContext;
    private EditText addProjet;
    private Spinner context;
    private Spinner projet;
    private DatePicker date;
    private Button annuler;
    private Button ajouter;
    private long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.taskcreateactivity);
        this.nom = (EditText)findViewById(R.id.nomTache);
        this.duree = (EditText)findViewById(R.id.duree);
        this.addContext = (EditText)findViewById(R.id.addcontext);
        this.addProjet = (EditText)findViewById(R.id.addprojet);
        this.description = (EditText)findViewById(R.id.description);
        this.annuler = (Button)findViewById(R.id.annuler);
        this.ajouter = (Button)findViewById(R.id.ajouter);
        this.date = (DatePicker)findViewById(R.id.date);

        //creation des spinner
        this.date = (DatePicker)findViewById(R.id.date);
        this.date.setSpinnersShown(false);
        this.context = (Spinner)findViewById(R.id.context);
        ContextDao cd = new ContextDao(this);
        List<String> spinnerArrayContext =  cd.selectionnerAllnom();
        spinnerArrayContext.add("Context aucun");
        ArrayAdapter<String> adapterContext = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArrayContext);
        adapterContext.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.context.setAdapter(adapterContext);


        this.projet = (Spinner)findViewById(R.id.projet);
        ProjetDao pd = new ProjetDao(this);
        List<String> spinnerArrayProject =  pd.selectionnerAllnom();
        spinnerArrayProject.add("Projet aucun");
        ArrayAdapter<String> adapterProject = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArrayProject);
        adapterProject.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.projet.setAdapter(adapterProject);

        //ajoute les action

        this.annuler.setOnClickListener(this);
        this.ajouter.setText("modifier");
        this.ajouter.setOnClickListener(this);
        this.date.setSpinnersShown(false);
        this.context.setOnItemSelectedListener(this);
        this.projet.setOnItemSelectedListener(this);



        //recuperer les valeurs passer par l activite
        Bundle bundle = getIntent().getExtras();
        this.id = Long.parseLong(bundle.getString("id"));
        this.nom.setText(bundle.getString("nom"));
        this.duree.setText(bundle.getString("duree"));
        this.description.setText(bundle.getString("description"));
        this.date.updateDate (Integer.parseInt(bundle.getString("annee")), Integer.parseInt(bundle.getString("mois")), Integer.parseInt(bundle.getString("jour")));
        this.context.setSelection(adapterContext.getPosition(bundle.getString("context")));
        this.projet.setSelection(adapterProject.getPosition(bundle.getString("projet")));
    }


    public void onClick(View v) {
        if(v.getId() == R.id.annuler){
            finish();
        }
        Tache tacheN ;
        String jour = String.valueOf(date.getDayOfMonth());
        String mois = String.valueOf(date.getMonth());
        String annee = String.valueOf(date.getYear());
        ContextDao cd = new ContextDao(this);
        ProjetDao pd = new ProjetDao(this);
        if(v.getId() == R.id.ajouter){
            TacheDao td = new TacheDao(this);
            if( this.projet.getSelectedItem().toString().equals("Projet aucun")){
                if( this.context.getSelectedItem().toString().equals("Context aucun")){
                    cd.ajouter(new ContextD(0,this.addContext.getText().toString() ));
                    pd.ajouter(new Projet(0,this.addProjet.getText().toString() ));
                    tacheN = new Tache(this.id, this.nom.getText().toString() , this.duree.getText().toString() , this.description.getText().toString() , jour , mois , annee ,this.addProjet.getText().toString() , this.addContext.getText().toString() );
                }else{
                    pd.ajouter(new Projet(0,this.addProjet.getText().toString() ));
                    tacheN = new Tache(this.id, this.nom.getText().toString() , this.duree.getText().toString() , this.description.getText().toString() , jour , mois , annee ,this.addProjet.getText().toString() , this.context.getSelectedItem().toString());
                }
            }else {
                if( this.context.getSelectedItem().toString() .equals("Context aucun")){
                    cd.ajouter(new ContextD(0,this.addContext.getText().toString() ));
                    tacheN = new Tache(this.id, this.nom.getText().toString() , this.duree.getText().toString() , this.description.getText().toString() , jour , mois , annee ,this.projet.getSelectedItem().toString() , this.addContext.getText().toString());
                }else{
                    tacheN = new Tache(this.id, this.nom.getText().toString() , this.duree.getText().toString() , this.description.getText().toString() , jour , mois , annee ,this.projet.getSelectedItem().toString() , this.context.getSelectedItem().toString());
                }
            }
            td.modifier(tacheN);
            Toast.makeText(this, "La tache a était modifier", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
    public void onItemSelected(AdapterView<?> parent, View v, int pos, long id) {
        if(parent.getId() == R.id.context){
            if( parent.getItemAtPosition(pos).equals("Context aucun")){
                addContext.setEnabled(true);//_____________________erreur on a toujour le droid de saisir
            }else{
                addContext.setEnabled(false);
            }
        }else if(parent.getId() == R.id.projet) {
            if (parent.getItemAtPosition(pos).equals("Projet aucun")) {
                addProjet.setEnabled(true);
            } else {
                addProjet.setEnabled(false);
            }
        }

        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
    }
    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }
    public void finish(){
        //retourne les infos
        Intent returnIntent = new Intent();

        setResult(RESULT_OK , returnIntent);
        super.finish();
    }


}
