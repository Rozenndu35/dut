package com.example.todo;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.todo.dao.ContextDao;
import com.example.todo.dao.ProjetDao;
import com.example.todo.dao.TacheDao;
import com.example.todo.model.ContextD;
import com.example.todo.model.Projet;
import com.example.todo.model.Tache;

import java.util.List;


public class taskCreateActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private EditText nom;
    private EditText duree;
    private EditText description;
    private EditText addContext;
    private EditText addProjet;
    private Spinner context;
    private Spinner projet;
    private DatePicker date;
    private Button annuler;
    private Button ajouter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.taskcreateactivity);

        //creation des widget
        this.nom = (EditText)findViewById(R.id.nomTache);
        this.duree = (EditText)findViewById(R.id.duree);
        this.addContext = (EditText)findViewById(R.id.addcontext);
        this.addProjet = (EditText)findViewById(R.id.addprojet);
        this.description = (EditText)findViewById(R.id.description);
        this.annuler = (Button)findViewById(R.id.annuler);
        this.ajouter = (Button)findViewById(R.id.ajouter);

        //creation des spinner
        this.date = (DatePicker)findViewById(R.id.date);
        this.date.setSpinnersShown(false);
        this.context = (Spinner)findViewById(R.id.context);
        ContextDao cd = new ContextDao(this);
        List<String> spinnerArrayContext =  cd.selectionnerAllnom();
        spinnerArrayContext.add("Context aucun");
        ArrayAdapter<String> adapterContext = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArrayContext);
        adapterContext.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.context.setAdapter(adapterContext);
        this.context.setSelection(spinnerArrayContext.size()-1);
        this.projet = (Spinner)findViewById(R.id.projet);
        ProjetDao pd = new ProjetDao(this);
        List<String> spinnerArrayProject =  pd.selectionnerAllnom();
        spinnerArrayProject.add("Projet aucun");
        ArrayAdapter<String> adapterProject = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArrayProject);
        adapterProject.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.projet.setAdapter(adapterProject);
        this.projet.setSelection(spinnerArrayContext.size()-1);

        //ajoute les action
        this.projet.setOnItemSelectedListener(this);
        this.context.setOnItemSelectedListener(this);
        this.annuler.setOnClickListener(this);
        this.ajouter.setOnClickListener(this);

    }

    /**
     * lors de clic sur le bouton
     * @param v : vue ouverte
     */
    public void onClick(View v) {
        if(v.getId() == R.id.annuler){
            finish();
        }

        if(v.getId() == R.id.ajouter){
            Tache tacheN ;

            //recupere la date pour l'enregistrer dans la tache
            String jour = String.valueOf(date.getDayOfMonth());
            String mois = String.valueOf(date.getMonth());
            String annee = String.valueOf(date.getYear());

            //recuperation des base
            ContextDao cd = new ContextDao(this);
            ProjetDao pd = new ProjetDao(this);
            TacheDao td = new TacheDao(this);

            if( this.projet.getSelectedItem().toString().equals("Projet aucun")){
                if( this.context.getSelectedItem().toString().equals("Context aucun")){
                    //si on ajoute un context et un projet
                    //ajouter a la base
                    cd.ajouter(new ContextD(0,this.addContext.getText().toString()));
                    pd.ajouter(new Projet(0,this.addProjet.getText().toString() ));
                    //creation de la tache
                    tacheN = new Tache(-1, this.nom.getText().toString() , this.duree.getText().toString() , this.description.getText().toString() , jour , mois , annee ,this.addProjet.getText().toString() , this.addContext.getText().toString());
                }else{
                    //si on ajoute  un projet
                    //ajouter a la base
                    pd.ajouter(new Projet(0,this.addProjet.getText().toString() ));
                    //creation de la tache
                    tacheN = new Tache(-1, this.nom.getText().toString() , this.duree.getText().toString() , this.description.getText().toString() , jour , mois , annee ,this.addProjet.getText().toString() , this.context.getSelectedItem().toString());
                }
            }else {
                if( this.context.getSelectedItem().toString() .equals("Context aucun")){
                    //si on ajoute un context
                    //ajouter a la base
                    cd.ajouter(new ContextD(0,this.addContext.getText().toString() ));
                    //creation de la tache
                    tacheN = new Tache(-1, this.nom.getText().toString() , this.duree.getText().toString() , this.description.getText().toString() , jour , mois , annee ,this.projet.getSelectedItem().toString() , this.addContext.getText().toString());
                }else{
                    //creation de la tache
                    tacheN = new Tache(-1, this.nom.getText().toString() , this.duree.getText().toString() , this.description.getText().toString() , jour , mois , annee ,this.projet.getSelectedItem().toString() , this.context.getSelectedItem().toString());
                }
            }
            //ajoute la tache
            td.ajouter(tacheN);
            Toast.makeText(this, "La tache a était ajouter", Toast.LENGTH_SHORT).show();


            finish();
        }
    }

    /**
     * Action lors des deroulement
     * @param parent
     * @param v : la vue
     * @param pos : la position selectionner
     * @param id : l'id de l'element
     */
    public void onItemSelected(AdapterView<?> parent, View v, int pos, long id) {
        if(parent.getId() == R.id.context){
            if( parent.getItemAtPosition(pos).equals("Context aucun")){
                addContext.setEnabled(true);
            }else{
                addContext.setEnabled(false);
                addContext.setHint("ajouter context");
            }
        }else if(parent.getId() == R.id.projet) {
            if (parent.getItemAtPosition(pos).equals("Projet aucun")) {
                addProjet.setEnabled(true);
            } else {
                addProjet.setEnabled(false);
                addProjet.setHint("ajouter projet");
            }
        }

        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
    }
    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }


}
