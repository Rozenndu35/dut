package com.example.todo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.todo.dao.TacheDao;
import com.example.todo.model.Tache;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView baniere;
    private Button list ;
    private Button projet;
    private Button ajouter;
    private Button rechercher;
    private EditText nomTache;
    int REQUEST_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        baniere = (ImageView) findViewById(R.id.baniere);
        list = (Button)findViewById(R.id.list);
        ajouter = (Button)findViewById(R.id.ajouter);
        projet = (Button)findViewById(R.id.projet);
        rechercher = (Button)findViewById(R.id.rechercher);
        rechercher.setEnabled(false);
        nomTache = (EditText) findViewById(R.id.editText);
        list.setOnClickListener(this);
        projet.setOnClickListener(this);
        ajouter.setOnClickListener(this);
        rechercher.setOnClickListener(this);

        nomTache.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            // action des lors que l'on ecrit un caractere
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                rechercher.setEnabled(s.toString().length() != 0);

            }
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void onClick(View v) {
        if(v.getId() == R.id.list){
            //pour list
            Intent listAct = new Intent(this, taskListActivity.class);
            startActivity(listAct);

        }else if(v.getId() == R.id.ajouter){
            //pour ajouter
            Intent ajouterAct = new Intent(this, taskCreateActivity.class);
            startActivity(ajouterAct);

        } else if (v.getId() == R.id.projet){
            //pour projet
            Intent projetAct = new Intent(this, taskProjetActivity.class);
            startActivity(projetAct);


        } else if (v.getId() == R.id.rechercher){
            //pour rechercher
            TacheDao td = new TacheDao(this);
            String recherche = nomTache.getText().toString();
            Tache tacheRechercher = td.selectionnerNom(recherche);
            if(tacheRechercher!=null){
                Intent activiterId = new Intent(this, taskDescActivity.class);
                activiterId.putExtra("id",Long.toString(tacheRechercher.getId()));
                activiterId.putExtra("nom", tacheRechercher.getNom());
                activiterId.putExtra("duree", "null");
                activiterId.putExtra("description", tacheRechercher.getDescription());
                activiterId.putExtra("jour", tacheRechercher.getJour());
                activiterId.putExtra("mois", tacheRechercher.getMois());
                activiterId.putExtra("annee", tacheRechercher.getAnnee());
                activiterId.putExtra("projet", tacheRechercher.getProjet());
                activiterId.putExtra("context", tacheRechercher.getContext());
                startActivityForResult(activiterId, REQUEST_CODE);
            }else{
                Toast.makeText(getApplicationContext(),"Cette tache n'existe pas" ,Toast.LENGTH_LONG).show();

            }

        }
    }

    //recuperer les reponses


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }
}

