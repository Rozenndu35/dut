package com.example.todo.base;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {
    public static final String TACHE_KEY = "id";
    public static final String TACHE_NOM = "nom";
    public static final String TACHE_LONGUEUR = "longueur";
    public static final String TACHE_DESCRIPTION = "description";
    public static final String TACHE_JOUR = "jour";
    public static final String TACHE_MOIS = "mois";
    public static final String TACHE_ANNEE = "annee";
    public static final String TACHE_PROJET = "projet";
    public static final String TACHE_CONTEXT = "context";
    public static final String PROJET_KEY = "id";
    public static final String PROJET_NOM = "nom";
    public static final String CONTEXT_KEY = "id";
    public static final String CONTEXT_NOM = "nom";


    public static final String TACHE_TABLE_NAME = "Tache";
    public static final String PROJET_TABLE_NAME = "Projet";
    public static final String CONTEXT_TABLE_NAME = "Context";
    public static final String TACHE_TABLE_CREATE =
            "CREATE TABLE " + TACHE_TABLE_NAME + " (" +
                    TACHE_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    TACHE_NOM + " TEXT, " +
                    TACHE_LONGUEUR + " TEXT, " +
                    TACHE_DESCRIPTION + " TEXT, " +
                    TACHE_JOUR + " TEXT, " +
                    TACHE_MOIS + " TEXT, " +
                    TACHE_ANNEE + " TEXT, " +
                    TACHE_PROJET + " TEXT, " +
                    TACHE_CONTEXT + " TEXT);";
    public static final String PROJET_TABLE_CREATE =
            "CREATE TABLE " + PROJET_TABLE_NAME + " (" +
                    PROJET_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    PROJET_NOM + " TEXT);";
    public static final String CONTEXT_TABLE_CREATE =
            "CREATE TABLE " + CONTEXT_TABLE_NAME + " (" +
                    CONTEXT_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    CONTEXT_NOM + " TEXT);";

    public static final String TACHE_TABLE_DROP = "DROP TABLE IF EXISTS " + TACHE_TABLE_NAME + ";";
    public static final String PROJET_TABLE_DROP = "DROP TABLE IF EXISTS " + PROJET_TABLE_NAME + ";";
    public static final String CONTEXT_TABLE_DROP = "DROP TABLE IF EXISTS " + CONTEXT_TABLE_NAME + ";";


    //constructeur
    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    //creer les tables
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TACHE_TABLE_CREATE);
        db.execSQL(PROJET_TABLE_CREATE);
        db.execSQL(CONTEXT_TABLE_CREATE);
    }

    @Override
    //mise a jour des tables
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(TACHE_TABLE_DROP);
        db.execSQL(PROJET_TABLE_DROP);
        db.execSQL(CONTEXT_TABLE_DROP);
        onCreate(db);
    }

}
