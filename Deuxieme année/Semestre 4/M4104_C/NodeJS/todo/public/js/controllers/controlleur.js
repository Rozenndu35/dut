//.run -> lance le bloc quand l'application est prête
// On va définir dans le $rootScope la liste des tâches , les context , les projet , l'id pour les tache a ajouter afin qu'elle puisse être accessible par toutes les vues aisément
//On cherchera d'abord à la charger depuis le localStorage si elle existe, sinon on en crée une nouvelle
todoApp.run(function ($rootScope) {
    $rootScope.tacheSelect = 0;
    $rootScope.tachesRecherches = []
    $rootScope.taches = []
    $rootScope.projets = []
    $rootScope.contexts = []
    $rootScope.id=0;
    if (!(localStorage.getItem("idTache") === null))$rootScope.id = localStorage.getItem("idTache")
    if (!(localStorage.getItem("taches") === null)) $rootScope.taches = JSON.parse(localStorage.getItem("taches"))
    if (!(localStorage.getItem("projets") === null)) $rootScope.projets = JSON.parse(localStorage.getItem("projets"))
    if (!(localStorage.getItem("contexts") === null)) $rootScope.contexts = JSON.parse(localStorage.getItem("contexts"))
});


//les controlleurs

todoApp.controller('acceuil', ['$scope', '$window','$rootScope', acceuil]);
function acceuil($scope, $window,$location,$rootScope) {
    //Fonction de recherche de tâches
    $scope.search = function () {
       if(!($scope.nomTache == undefined || $scope.nomTache =="" )){
        //recupere les taches filtrer ( nom includes bien le champ de recherche )
        $scope.tachesRecherches=$scope.taches.filter(x=>x.nom.includes($scope.nomTache))
       }
    }
    $scope.go = function (index) {
        var tacheTest = $scope.tachesRecherches[index]
        console.log(tacheTest)
        var index2 = $scope.taches.indexOf(tacheTest)
        console.log(index2)
        console.log($rootScope.tacheSelect)
        $rootScope.tacheSelect = index2;
        $location.path("/tache")
    }
}

todoApp.controller('ajouterTache', ['$scope', '$window','$rootScope', ajouterTache]);
function ajouterTache($scope, $window,$rootScope) {
    //pour ajouter
    $scope.add = function () {
        if ($scope.projetAddTache == "Nouveau projet") {
            projet = $scope.newProjetAddTache;
            $scope.projets.push({
                nom: projet
            });
            localStorage.setItem("projets", JSON.stringify($scope.projets));
        } else {
            projet = $scope.projetAddTache;
        }
        if ($scope.contextAddTache == "Nouveau context") {
            context = $scope.newContextAddTache;
            $scope.contexts.push({
                nom: context
            });
            localStorage.setItem("contexts", JSON.stringify($scope.contexts));
        } else {
            context = $scope.contextAddTache;
        }
        $scope.taches.push({
            id: $scope.id,
            nom: $scope.nomAddTache,
            duree: $scope.dureeAddTache,
            date: $scope.dateAddTache,
            description: $scope.descriptionAddTache,
            context: context,
            projet: projet
        });
        $scope.id++;
        localStorage.setItem("taches", JSON.stringify($scope.taches));
        localStorage.setItem("idTache", $scope.id);

        $scope.back();
    }
    //retour
    $scope.back = function () {
        window.history.back();
    }



}

todoApp.controller('listeProjet', ['$scope', '$window','$rootScope', listeProjet]);
function listeProjet($scope, $window,$rootScope) {
    //retour
    $scope.back = function () {
        window.history.back();
    }

    //Voir les taches du projet
    $scope.go = function (index) {
        $rootScope.projetSelectionner=index
        $location.path("/listeTacheProjet")
    }
}


todoApp.controller('listeTache', ['$scope', '$window','$location','$rootScope',listeTache]);
function listeTache($scope, $window,$location,$rootScope) {

    //retour
    $scope.back = function () {
        window.history.back();
    }

    //Voir une tâche
    $scope.go = function (index) {
        $rootScope.tacheSelect=index
        $location.path("/tache")
    }

    //Suprrimer une tâche
    $scope.del = function (index) {
        $scope.taches.splice(index,1)
        localStorage.setItem("taches", JSON.stringify($scope.taches));
    }
    //Supprime toutes les tâches
    $scope.reset = function () {
        $scope.taches.splice(0,$scope.taches.length);
        localStorage.setItem("taches", JSON.stringify($scope.taches));
    }
    $scope.getTachesFilter=function (){
        if($scope.contextFiltre!=undefined && $scope.projetFiltre!=undefined){
            console.log($scope.contextFiltre);
            console.log($scope.projetFiltre);
            
        }else if($scope.contextFiltre!=undefined){
            console.log($scope.contextFiltre);
            $scope.tachesRecherches=$scope.taches.filter(x=>x.context.equals($scope.contextFiltre))
        }else if($scope.contextFiltre!=undefined){
            console.log($scope.projetFiltre);
            $scope.tachesRecherches=$scope.taches.filter(x=>x.projet.equals($scope.projetFiltre))
        }
        console.log($scope.tachesRecherches)
        
        
    }
}

todoApp.controller('listeTacheProjet', ['$scope', '$window', listeTacheProjet]);
function listeTacheProjet($scope, $window) {
    //recupere les taches filtrer ( nom includes bien le champ de recherche )
    $scope.tachesSelect=$scope.taches.filter(x=>x.nom.includes($scope.projet.nom))
    //retour
    $scope.back = function () {
        window.history.back();
    }
}

todoApp.controller('tache', ['$scope', '$window','$location', tache]);
function tache($scope, $window,$location) {
    //retour
    $scope.back = function () {
        window.history.back();
    }
    //Supprime la tâche
    $scope.del = function () {
        $scope.taches.splice($scope.tacheSelect,1)
        localStorage.setItem("taches", JSON.stringify($scope.taches));
        $location.path("/listTache")
    }
}

todoApp.controller('modifierTache', ['$scope', '$window','$location', modifierTache]);
function modifierTache($scope, $window,$location) {
    $scope.nomAddTache= $scope.taches[$scope.tacheSelect].nom
    $scope.dureeAddTache=$scope.taches[$scope.tacheSelect].duree
    $scope.descriptionAddTache=$scope.taches[$scope.tacheSelect].description
    $scope.dateAddTache=$scope.taches[$scope.tacheSelect].date
    $scope.newProjetAddTache=$scope.taches[$scope.tacheSelect].projet
    $scope.newContextAddTache=$scope.taches[$scope.tacheSelect].context
    
    //retour
    $scope.back = function () {
        window.history.back();
    }
    //Modifier tâche
    $scope.modify = function () {
        if ($scope.projetAddTache == "Nouveau projet") {
            projet = $scope.newProjetAddTache;
            /*$scope.projets.push({
                nom: projet
            });*/
        } else {
            projet = $scope.projetAddTache;
        }
        if ($scope.contextAddTache == "Nouveau context") {
            context = $scope.newContextAddTache;
            /*$scope.contexts.push({
                nom: context
            });*/
        } else {
            context = $scope.contextAddTache;
        }
        lID = $scope.taches[$scope.tacheSelect].id
        $scope.taches[$scope.tacheSelect]={
            id: $scope.lID,
            nom: $scope.nomAddTache,
            duree: $scope.dureeAddTache,
            date: $scope.dateAddTache,
            description: $scope.descriptionAddTache,
            context: context,
            projet: projet
        };
        localStorage.setItem("taches", JSON.stringify($scope.taches));
        $scope.back();
    }
}