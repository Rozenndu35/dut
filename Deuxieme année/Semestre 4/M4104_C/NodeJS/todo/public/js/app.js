//Création de l'application Todo List
var todoApp = angular.module("app",["ngRoute" , "ngCookies"]);

//Configuration des routes 
todoApp.config(function($routeProvider, $locationProvider){
  $locationProvider.hashPrefix('');
  $routeProvider
  .when('/',{
      templateUrl : 'acceuil.html',
      controller :'acceuil'
    })
  .when('/ajouter',{
      templateUrl : 'ajouterTache.html',
      controller :'ajouterTache'
    })
  .when('/listProjet',{
    templateUrl : 'listeProjet.html',
    controller :'listeProjet'
    })
  .when('/listTache',{
    templateUrl : 'listeTache.html',
    controller :'listeTache'
    })
  .when('/listTacheProjet',{
    templateUrl : 'listeTacheProjet.html',
    controller :'listeTacheProjet'
    })
  .when('/modifier',{
    templateUrl : 'modifierTache.html',
    controller :'modifierTache'
    })
  .when('/tache',{
    templateUrl : 'tache.html',
    controller :'tache'
    })
});

//Voir controlleur.js pour les controlleurs