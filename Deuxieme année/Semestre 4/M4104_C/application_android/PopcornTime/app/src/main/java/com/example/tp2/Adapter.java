package com.example.tp2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.List;

public class Adapter extends BaseAdapter {
    private List<Cinema> listCine;
    private Context context; //etat de l'application
    private LayoutInflater inflater;

    public Adapter(Context context, List<Cinema> list){
        this.context = context;
        this.listCine = list;
        this.inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return this.listCine.size();
    }

    @Override
    public Object getItem(int position) {
        return this.listCine.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        //initialise cinema_list_item
        if(convertView == null){
            view = (View) this.inflater.inflate(R.layout.cinema_list_item, parent, false);
        }else{
            view = (View) convertView;
        }

        //initialise les vue du layout

        TextView titre = (TextView) view.findViewById(R.id.cinemaTitre);
        TextView realisateur = (TextView) view.findViewById(R.id.cinemaRealisateur);
        TextView durree = (TextView) view.findViewById(R.id.cinemaDuree);
        ImageView affiche = (ImageView) view.findViewById(R.id.affiche);

        //modification des vue
        titre.setText((listCine.get(position).getTitre()));
        realisateur.setText(listCine.get(position).getRealisateur());
        durree.setText(listCine.get(position).getDurree());
        int id = context.getResources().getIdentifier(listCine.get(position).getImage(), "drawable", context.getPackageName());
        affiche.setImageResource(id);

        //on retourne la vue creer
        return view;

    }
}
