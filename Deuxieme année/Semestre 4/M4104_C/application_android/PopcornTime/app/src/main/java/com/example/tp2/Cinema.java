package com.example.tp2;


public class Cinema {
    private String image;
    private String titre;
    private String realisateur;
    private String durree;

    public Cinema(String image , String titre , String realisateur , String durree){
        if (image != null){ this.image = image;
        }else{this.image = "@drawable/ic_image_foreground";}
        if (titre != null){ this.titre = titre;
        }else { this.titre = " ";}
        if(realisateur != null) { this.realisateur = realisateur;
        }else{this.realisateur = " ";}
        if( durree!= null){ this.durree = durree;
        }else{this.durree = " ";}
    }

    public String getImage() {
        return image;
    }

    public String getTitre() {
        return titre;
    }

    public String getRealisateur() {
        return realisateur;
    }

    public String getDurree() {
        return durree;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setRealisateur(String realisateur) {
        this.realisateur = realisateur;
    }

    public void setDurree(String durree) {
        this.durree = durree;
    }
}
