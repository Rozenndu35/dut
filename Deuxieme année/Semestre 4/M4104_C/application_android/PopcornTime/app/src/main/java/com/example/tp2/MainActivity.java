package com.example.tp2;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cinema_list);

        //creation des films
        Cinema HungerGame = new Cinema("@drawable/hungergame", "Hunger Game", "Gary Ross", "2h22");
        Cinema Lahaut = new Cinema("@drawable/lahaut", "Là-haut", "Albert Dupontel", "1h36");
        Cinema LeRoiLion = new Cinema("@drawable/leroilion", "Le roi lion", "Jon Favreau", "1h58");
        Cinema Annabelle = new Cinema("@drawable/annabelle", "Annabelle", "Jon R. Leonetti", "1h39");
        Cinema LesMiserable = new Cinema("@drawable/lesmiserables", "Les Misérable", "Ladj Ly", "3h40");
        Cinema Abominable = new Cinema("@drawable/abominable", "Abominable", "Jill Culton et Todd Wilderman", "1h37");
        ArrayList<Cinema> cinemaArrayList = new ArrayList<Cinema>();
        cinemaArrayList.add(HungerGame);
        cinemaArrayList.add(Lahaut);
        cinemaArrayList.add(LeRoiLion);
        cinemaArrayList.add(Annabelle);
        cinemaArrayList.add(LesMiserable);
        cinemaArrayList.add(Abominable);

        //initialisation de l addapter pour cinema
        final Adapter adapter = new Adapter(this, cinemaArrayList);
        //recuperation de la listView
        ListView list = (ListView)findViewById(R.id.cinemaList);
        //passage des donne a la liste
        list.setAdapter(adapter);

        //on ajoute un listener

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cinema selected = (Cinema) adapter.getItem(position);
                CharSequence titre = "Titre : " +
                        ""+ selected.getTitre();
                Toast.makeText(getApplicationContext(),titre ,10).show();
            }
        });
    }
}
