package com.example.apptel;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class UnlockTelActivity extends Activity {
    private String identifient = "";
    private String motPasse = "";
    private Button buttonNum;
    private EditText num ;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unlock);
        //recuperer les valeurs passer par le main
        Bundle bundle = getIntent().getExtras();
        identifient = bundle.getString("ident");
        motPasse = bundle.getString("mp");

        //deroulment activitee
        buttonNum = (Button)findViewById(R.id.buttonNum);
        num = (EditText)findViewById(R.id.textNum);

        // ajoute l'action des bouton

        buttonNum.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                finish();
            }
        });
        //fin activitee


    }

    public void finish(){
        //retourne les infos
        Intent returnIntent = new Intent();

        returnIntent.putExtra("ident", identifient);
        returnIntent.putExtra("mp" , motPasse);
        returnIntent.putExtra("numero" , num.getText().toString());

        setResult(RESULT_OK , returnIntent);
        super.finish();
    }
}
