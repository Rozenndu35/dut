package com.example.apptel;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText identifient;
    private EditText motPasse;
    private Button sidentifier;
    private Button appeler;
    private boolean ok = false;
    int REQUEST_CODE = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //creation des widget
        identifient = (EditText)findViewById(R.id.id);
        motPasse = (EditText)findViewById(R.id.mdp);
        sidentifier = (Button)findViewById(R.id.sident);
        appeler = (Button)findViewById(R.id.appel);

        // ajoute l'action des bouton
        sidentifier.setOnClickListener(this);

        appeler.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Appele lancer ?" ,Toast.LENGTH_LONG).show();
            }
        });


    }

    public void onClick(View v) {

        //verifie l id et le mdp
        if ( identifient.getText()!= null && motPasse.getText()!=null) {
            //lancer une activitee
            Intent activiterId = new Intent(this, UnlockTelActivity.class);
            activiterId.putExtra("ident", identifient.getText().toString());
            activiterId.putExtra("mp" , motPasse.getText().toString());
            startActivityForResult(activiterId, REQUEST_CODE);

        }
    }

    //recuperer les reponses


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if (data.hasExtra("numero")){
                Toast.makeText(this, data.getExtras().getString("numero"), Toast.LENGTH_SHORT).show();
                Intent telIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+data.getExtras().getString("numero")));
                startActivity(telIntent);
            }
        }else{
            Toast.makeText(this, "Erreur", Toast.LENGTH_SHORT).show();

        }
    }
}
