package com.example.powerbrowser;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText url ;
    private Button rechercher;
    private WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        url  = (EditText)findViewById(R.id.eSearch);
        rechercher = (Button)findViewById(R.id.bSearch);
        rechercher.setOnClickListener(this);
        webview = (WebView)findViewById(R.id.webView);

        //recuperer les parametre
        WebSettings params = webview.getSettings();
        //activer javascript
        params.setJavaScriptEnabled(true);
        //ajouter un bouton de zoom
        params.setBuiltInZoomControls(true);
        //ajouter une interface javascript
        webview.addJavascriptInterface(new WebAppInterface(this), "Android");
        //atribution d'un client personalise
        webview.setWebViewClient(new MyWebViewClient());

        //ajoute l'action sur l'editText
        url.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            // action des lors que l'on ecrit un caractere
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                rechercher.setEnabled(s.toString().length() != 0);

            }
            public void afterTextChanged(Editable s) {

            }
        });
    }

    //action bouton
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bSearch:
                webview.loadUrl(url.getText().toString());
        }

    }
    //bouton retour pour revenir au site precedent
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        //verifie si retour et historique
        if ((keyCode==KeyEvent.KEYCODE_BACK)&& webview.canGoBack()){
            webview.goBack();
            return true;
        }
        return super.onKeyDown(keyCode,event);
    }
}
