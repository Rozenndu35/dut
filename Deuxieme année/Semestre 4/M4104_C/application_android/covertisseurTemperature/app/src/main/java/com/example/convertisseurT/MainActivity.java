package com.example.convertisseurT;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.CheckBox;
import android.widget.Button;
import android.widget.EditText;
import android.text.TextWatcher;
import android.text.Editable;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private EditText initiale;
    private CheckBox fahrenheit;
    private CheckBox celsius;
    private Button validated;
    private TextView nouveau;
    private TextView titre;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // creer les wwidjets
        initiale = (EditText)findViewById(R.id.editText);
        fahrenheit = (CheckBox)findViewById(R.id.checkBox2);
        celsius = (CheckBox)findViewById(R.id.checkBox);
        nouveau = (TextView)findViewById(R.id.textView2);
        titre = (TextView)findViewById(R.id.textView3);
        validated = (Button)findViewById(R.id.valider);
        validated.setEnabled(false);

        //action sur le edit text
        initiale.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            // action des lors que l'on ecrit un caractere
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                validated.setEnabled(s.toString().length() != 0);

            }
            public void afterTextChanged(Editable s) {

            }
        });
        // ajoute l'action au boutonvalider lorsque l'on clic dessus
        validated.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (fahrenheit.isChecked() && !celsius.isChecked()) {
                    nouveau.setText(String.valueOf(fahrenheitCelsius(Double.parseDouble(initiale.getText().toString()))));
                } else if (!fahrenheit.isChecked() && celsius.isChecked()) {
                    nouveau.setText(String.valueOf(celsiusFahrenheit(Double.parseDouble(initiale.getText().toString()))));
                } else {
                    nouveau.setText("error");
                }
            }
        });
    }

    /**
     * Parse degrees Celsius and convert to Fahrenheit
     * @param celsius : the value in Celsius
     * @return Fahrenheit
     */
    public double celsiusFahrenheit(double celsius) {
        return (celsius * 1.8 + 32);
    }
    /**
     * Parse degrees Fahrenheit as a double and convert to Celsius
     * @param fahrenheit : The value in Fahrenheit
     * @return Celsius
     */
    public double fahrenheitCelsius(double fahrenheit) {
        return (fahrenheit-32) / 1.8;
    }
}