package servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class RepertoireCourant extends HttpServlet{
	public static final String REP_RACINE ="/home/rozenn/TP5_Bautrais_Costiou";//nom de la racine
	private File fichierCourant = new File(REP_RACINE);
	/**
	 * 
	 * @param request : 
	 * @param response : 
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response ) {
		//Analyser la requête et definition du type de réponse 
		// 0 erreur , 1 liste des fichiers du répertoire, 2 changement de répertoire, 3 chargement du contenu du fichier
		int typeRequete =0; 
		if(request.getParameter("type").equals("liste")) {
			typeRequete = 1;
			response.setContentType("text/html");
			response.setCharacterEncoding( "UTF-8" );
		}else if(request.getParameter("type").equals("changer")) {
			typeRequete = 2;
			response.setContentType("text/html");
			response.setCharacterEncoding( "UTF-8" );
		}else if(request.getParameter("type").equals("contenu")) {
			typeRequete = 3;
			String fichier = request.getParameter("fichier");
			String[] sufFichier = fichier.split(".");
			TypeFichierExtension[] types;
			types = TypeFichierExtension.values();
			for(int i = 0 ; i<types.length ; i++ ) {
				if(types[i].getSuffixe().equals(sufFichier)) {
					response.setContentType(types[i].getTypeMine());
					response.setCharacterEncoding( "UTF-8" );
				}				
			}			
		}
		//Réalisation de la requête
		if(typeRequete == 1) {
			//liste des fichiers du répertoire
			try {
				PrintWriter pw;
				JSONArray jsonFichiers;
				File[] listNomFils = this.fichierCourant.listFiles();
				JSONObject obT = new JSONObject();
				obT.put("nom", this.fichierCourant.getName());
				jsonFichiers=new JSONArray();
				jsonFichiers.add(obT);
				for (File f : listNomFils) {
					JSONObject obj = new JSONObject();
					obj.put("nom",f.getName());
					obj.put("taille",f.length());
					obj.put("rep",f.isDirectory());
					jsonFichiers.add(obj);
				}			
				if(this.fichierCourant.getAbsolutePath().equals(REP_RACINE)) {
					JSONObject obj = new JSONObject();
					obj.put("parent",  null);
					jsonFichiers.add(obj);
				}else {
					JSONObject obj = new JSONObject();
					obj.put("parent",  "..");
					jsonFichiers.add(obj);
				}
				pw = response.getWriter();
				pw.println(jsonFichiers.toJSONString());
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(typeRequete == 2 ) {
			//changement de répertoire
			String repertoire = request.getParameter("repertoire");
			if(repertoire.equals("..")) {
				if(!this.fichierCourant.getAbsolutePath().equals(REP_RACINE)) {
					String[] parties = this.fichierCourant.getAbsolutePath().split(this.fichierCourant.separator);
					String chemin="";
					for (int i= 0 ; i<parties.length-1; i++) {
						chemin += "/" + parties[i];
					}
					this.fichierCourant=new File(chemin);
				}
			}else {
				File[] listNomFils = this.fichierCourant.listFiles();
				for (File f : listNomFils) {
					if(f.getName().equals(repertoire)) {
						this.fichierCourant=f;
					}
				}	
			}
			
		}else if(typeRequete == 3 ) {
			//chargement du contenu du fichier
			
			
		}
	}

}
