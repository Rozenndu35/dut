package servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class RepertoireCourant extends HttpServlet{
	public static final String REP_RACINE ="/home/TP5_Bautrais_Costiou";//nom de la racine
	private File fichierCourant = new File(REP_RACINE);
	/**
	 * 
	 * @param request : 
	 * @param response : 
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response ) {
		//Analyser la requête et definition du type de réponse 
		// 0 erreur , 1 liste des fichiers du répertoire, 2 changement de répertoire, 3 chargement du contenu du fichier
		int typeRequete =0; 
		System.out.println("rentre");
		if(request.getParameter("type").equals("liste")) {
			typeRequete = 1;
			System.out.println("bon type");
			response.setContentType("text/html");
			response.setCharacterEncoding( "UTF-8" );
		}else if(request.getParameter("type").equals("changer")) {
			typeRequete = 2;
			response.setContentType("text/html");
			response.setCharacterEncoding( "UTF-8" );
		}else if(request.getParameter("type").equals("contenu")) {
			typeRequete = 3;
			String fichier = request.getParameter("fichier");
			String[] sufFichier = fichier.split(".");
			TypeFichierExtension[] types;
			types = TypeFichierExtension.values();
			for(int i = 0 ; i<types.length ; i++ ) {
				if(types[i].getSuffixe().equals(sufFichier)) {
					response.setContentType(types[i].getTypeMine());
					response.setCharacterEncoding( "UTF-8" );
				}				
			}			
		}
		//Réalisation de la requête
		if(typeRequete == 1) {
			//liste des fichiers du répertoire
			System.out.println("rentre realisation");
			try {
				PrintWriter pw;
				JSONArray jsonFichiers;
				File[] listNomFils = this.fichierCourant.listFiles();
				
				jsonFichiers=new JSONArray();
				for (File f : listNomFils) {
					JSONObject obj = new JSONObject();
					obj.put("nom",f.getName());
					obj.put("taille",f.length());
					jsonFichiers.add(obj);
				}			
				
				/**if(this.fichierCourant.getAbsolutePath().equals(REP_RACINE)) {
					obj.put("parent",  null);
				}else {
					obj.put("parent",  "..");
				}*/
				pw = response.getWriter();
				pw.println(jsonFichiers.toJSONString());
				System.out.println(jsonFichiers);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
