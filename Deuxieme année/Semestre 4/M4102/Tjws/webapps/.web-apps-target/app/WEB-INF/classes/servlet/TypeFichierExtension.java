package servlet;
/**
 * Enumération contenant tous les types de fichiers pouvant être contenu dans le répertoire courant.
 * @author rozenn
 *
 */

public enum TypeFichierExtension {
	Pedef("pdf", "application/pdf") ,
	Texte("txt" , "text/plain") , 
	Jpaigue("jpg/jpeg", "image/jpeg") , 
	Htmlweb("html", "text/html");

	private String suffixe;
	private String typeMine;
	
	private TypeFichierExtension(String suf, String typeMine) {
		this.suffixe=suf;
		this.typeMine = typeMine;
	}
	
	public String getSuffixe() {
		return suffixe;
	}

	public void setSuffixe(String suffixe) {
		this.suffixe = suffixe;
	}

	public String getTypeMine() {
		return typeMine;
	}

	public void setTypeMine(String typeMine) {
		this.typeMine = typeMine;
	}
	
	
}
