import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ServeurDNS {
	Socket soc ;
	String adServeur;
	int port;
	/**
	 * Constructeur de la classe ServeurDNS
	 * @param nAdServeur
	 * @param nPort
	 */
	public ServeurDNS(String nAdServeur , int nPort){
		this.adServeur = nAdServeur;
		this.port = nPort;
	}
	/**
	 * Methode qui envoie une requete au serveur
	 * @throws IOException
	 */
	private void requete(String ip , String nomHote , String mdp) throws IOException {
		OutputStream out = soc.getOutputStream();
		PrintWriter write = new PrintWriter(out);
		String authorizationS = nomHote+":"+mdp;
		String authorizationB = Base64.getEncoder().encodeToString(authorizationS.getBytes());
		
		write.print("GET /tp6/dynDNS?hostname="+nomHote+"&myip="+ip+" HTTP/1.1\r\n");
		write.print("Authorization: Basic "+ authorizationB+"\r\n" );
		write.print("Connection: close \r\n");
		write.print("Host: "+this.adServeur+"\r\n");
		write.print("\r\n");
		write.flush();
	/**
	 * Methode qui affiche la reponse du serveur
	 */
	}
	private void lecture() throws IOException {
		InputStream in = soc.getInputStream();
		BufferedReader reader = new BufferedReader (new InputStreamReader(in));
		String ligne;
		boolean saut = false;
		while((ligne = reader.readLine())!=null) {
			if(ligne.equals("")) {
				saut = true;
			}else if (saut) {
				System.out.println("Retour de la requête du serveur : " +ligne);
			}	
		}
	}
	
	/**
	 * Methode qui modifie l adresse ip sur le serveur
	 * @param ip
	 * @param nomHote
	 * @param mdp
	 */
	public void envoyerIP(String ip , String nomHote , String mdp ) {
		soc=null;
		try {
			//se connecte 
			soc = new Socket(this.adServeur, this.port);
			
			//la requête
			requete(ip, nomHote , mdp);
			
			//lecture de la réponse
			lecture();
			
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			//déconnexion
			try {
				if(soc!= null) {soc.close();}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
}
