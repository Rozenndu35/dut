
public class Lanceur {
	public static void main( String[] strs) {
		ClientDNS client = new ClientDNS("www.pencouelo.fr" , 80);
		ServeurDNS serveur = new ServeurDNS("www.pencouelo.fr" , 80);
		String ip = "";
		String ipNew = "";
		String nomHote= "KR";
		long heureModif = -1;
		
		//après être connecté
		while(true) {
			try {
				ipNew = client.recupererIP();
				if(!ipNew.equals(ip)) {
					System.out.println("Modification avec ip modifiée");
					ip = ipNew;
					serveur.envoyerIP(ip, nomHote, Long.toString(System.currentTimeMillis()));
					heureModif = System.currentTimeMillis();
				}else if ((heureModif + 120000) <= System.currentTimeMillis() ) {
					System.out.println("Modification après 2min");
					serveur.envoyerIP(ip, nomHote, Long.toString(System.currentTimeMillis()));
					heureModif = System.currentTimeMillis();
					
				}
				System.out.println("");
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
	}
}
