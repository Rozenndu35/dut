import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.regex.*;


public class ClientDNS {
	Socket soc;
	String adServeur ;
	int port;
	/**
	 * Constructeur de la classe ClientDNS
	 * @param nAdServeur
	 * @param nPort
	 */
	public ClientDNS(String nAdServeur , int nPort){
		this.adServeur = nAdServeur;
		this.port = nPort;
	}
	/**
	 * Methode qui envoie une requete au serveur
	 * @throws IOException
	 */
	private void requete() throws IOException {
		OutputStream out = soc.getOutputStream();
		PrintWriter write = new PrintWriter(out);
		
		write.print("GET /tp6/ HTTP/1.1\r\n");
		write.print("Connection: close \r\n");
		write.print("Host: "+adServeur+"\r\n");
		write.print("\r\n");
		write.flush();
	}
	
	/**
	 * Methode qui recupere l ip retournee par le serveur 
	 * @throws IOException
	 */
	private String lectureIP() throws IOException {
		InputStream in = soc.getInputStream();
		BufferedReader reader = new BufferedReader (new InputStreamReader(in));
		String ligne;
		int nbLigne=0;
		String ip="";
		Pattern patIp = Pattern.compile("[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}");
		Pattern patCode = Pattern.compile("2[0-9]{2}");
		boolean saut= false;
		boolean codeR = false;
	
		while ( (ligne=reader.readLine())!= null) {
			//récupère l'erreur 
			if(nbLigne == 0) {
				Matcher matchLigne = patCode.matcher(ligne);
				//vérifie le code
				if(matchLigne.find()) {
					codeR = true;
				}else {
					System.out.println("Désolé la réponse nous a fourni une erreur");
				}
				nbLigne++;
			}
			
			//récupère les lignes de la réponse
			if(ligne.equals("")) {
				saut = true;
			}else if(saut && codeR) {
				Matcher matchLigne = patIp.matcher(ligne);
				//récupère l'ip lorsqu'elle est trouvée
				if(matchLigne.find()) {
					ip = matchLigne.group();
				}
			}
		}
		return ip;
	}
	/**
	 * Methode qui recupere l ip sur le serveur
	 * @return
	 */
	public String recupererIP() {
		String ip= "";
		try {
			//se connecte 
			soc = new Socket(this.adServeur, this.port);
			
			//la requête
			requete();
			
			//lecture de la réponse
			ip = lectureIP();
			System.out.println("l'adresse ip est : "+ip);
			
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			//déconnexion
			try {
				soc.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return ip;
	}
	
}
