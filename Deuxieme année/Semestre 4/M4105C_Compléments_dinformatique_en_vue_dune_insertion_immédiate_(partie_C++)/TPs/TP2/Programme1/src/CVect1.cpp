#include "CVect1.h"

CVect1 :: CVect1 ( int n ) {
	m_nbe = n;
	m_taille = n;
	m_pAdr = new double[m_taille];
	cout << "Constructeur usuel - adr objet : " << this << " - adr vecteur : " << m_pAdr << endl;
}

/************************/

CVect1 :: ~CVect1 () {

	cout << "Destructeur objet - adr objet : " << this << " - adr vecteur: " << m_pAdr << endl;

	delete[] m_pAdr;
}

CVect1 :: CVect1 ( const CVect1& vectToCopy ){
	this->m_nbe = vectToCopy.m_nbe;
	this->m_pAdr = new double[m_taille];
	for (int i= 0; i<m_taille; i++){
		this->m_pAdr[i]= vectToCopy.m_pAdr[i];
	};
}