#ifndef CVECT1_H_
#define CVECT1_H_

#include "AllIncludes.h"

class CVect1 {

	private :
		int		m_nbe;
		double*	m_pAdr;
		int m_taille;

	public:
		CVect1 ( int n );
		// Destructeur
		~CVect1();
		CVect1 ( const CVect1& vectToCopy );
};

#endif /*CVECT1_H_*/
