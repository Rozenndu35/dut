#include "CVect1.h"

void fct ( CVect1 b );

int main() {
	
		CVect1	a(5);
		fct(a);
		return 0;
}

/************************/

void fct ( CVect1 b ) {
	
		cout << "Appel de la fonction" << endl;
}
