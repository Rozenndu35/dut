#ifndef CPOINT_H
#define CPOINT_H

#include "AllIncludes.h"



class CPoint{
    
    private:
        // attributs d’instance 
        int m_abs;
        int m_ord;
     
    public :
        // Constructeur publique
        CPoint(int abs, int ord);
        CPoint(); 
        //copy constructeur
        CPoint (const CPoint& pointToCopy);
        //methode public
        int getAbs();
        int getOrd();
        void presentation();
        // DESTRUCTEUR
        ~CPoint();
    
};

#endif