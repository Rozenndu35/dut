#ifndef CPOINT_H
#define CPOINT_H

#include "AllIncludes.h"
#include "CPoint.h"

void fonction1 ( CPoint theP );
void fonction2 ( CPoint& theP );
void fonction3 ( CPoint* pTheP );
void fonction4 ( CPoint* pTabPt, int tailleTab );

#endif