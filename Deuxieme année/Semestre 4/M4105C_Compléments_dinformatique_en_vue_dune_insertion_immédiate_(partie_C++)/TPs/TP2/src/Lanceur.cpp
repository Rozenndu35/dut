#include "Lanceur.h"

int main(){
    //par valeur
    CPoint theP1(12, 3) ; //objet
    fonction1 (theP1);
    
    //par reference
    CPoint& theP2(12, 3) ; //objet
    fonction2 ( theP2 );

    // pointeur sur l'objet
    CPoint*  pTheP = new CPoint(5,10); //pointeur
    fonction3 (pTheP);

    //tableau de point
    int tailleTab=4;
    CPoint p1(12, 1);
    CPoint p2(12, 2);
    CPoint p3(12, 3);
    CPoint p4(12, 4);
    CPoint  pTabPt {p1, p2, p3 , p4}; //pointeur ver tableau
    int tailleTab;
    fonction4 ( pTabPt, tailleTab );

}

//type objet ou primitif
void fonction1 ( CPoint theP ){
    theP->presentation();

}

//type objet ou primitif
void fonction2 ( CPoint& theP ){
    theP->presentation();

}
//type pointeur
void fonction3 ( CPoint* pTheP ){
    &ptheP->presentation();
}
//type pointeur vers un tableau
void fonction4 ( CPoint* pTabPt, int tailleTab ){
    for(int i= 0 ; i<4; i++){
        pTabPt[i]->presentation();
    }
}