#include "CPoint.h"

CPoint::CPoint(int abs, int ord){
    this->m_abs = abs ;
    this->m_ord = ord;
    cout << "construction de l'objetCPoint d'adresse :" << this << endl;
}

CPoint::CPoint(){
    this->m_abs = 0 ;
    this->m_ord = 0 ;
    cout << "..." << endl;
}

CPoint::CPoint ( const CPoint& pointToCopy ){
    this->m_abs= pointToCopy.m_abs;
    this->m_ord= pointToCopy.m_ord;
    cout<< "la copie est creer"<< endl;
}

void CPoint::presentation(){
    cout<< "Le point :" << endl;
    cout<< "d'absice :" << m_abs << endl;
    cout<< "d'ordonnée :" << m_ord << endl;
}

int CPoint::getAbs(){
    return this->m_abs;
}
int CPoint::getOrd(){
    return this->m_ord;
}

CPoint::~CPoint(){
    cout<< "destruction de l'objet CPoint d'adresse :" << this << endl;
}