#include "CPoint.h"

int main(){
    //test constructeur sans parametre
    (cout << "constructeur sans parametre"<< endl);
    CPoint *point1 = new CPoint ();

    //test constructeur avec parametre
    (cout << "constructeur avec parametre"<< endl);
    CPoint *point2 = new CPoint (5,6);

    //test des acceseurs
    (cout << "getAbs"<< endl);
    if ( point1->getAbs() == 0){
        (cout << "Ok"<< endl);
    }else{
        (cout << "Erreur"<< endl);
    }
    if ( point2->getAbs() == 5){
        (cout << "Ok"<< endl);
    }else{
        (cout << "Erreur"<< endl);
    }
    (cout << "getOrd"<< endl);
    if ( point1->getOrd() == 0){
        (cout << "Ok"<< endl);
    }else{
        (cout << "Erreur"<< endl);
    }
    if ( point2->getOrd() == 6){
        (cout << "Ok"<< endl);
    }else{
        (cout << "Erreur"<< endl);
    }

    //test de presentation
    (cout << "presentation:"<< endl);
    (cout << "point1"<< endl);
    point1->presentation();
    (cout << "point2"<< endl);
    point2->presentation();

    //test de copyconstructeur
    (cout << "copyconstructeur:"<< endl);
    point1 = new CPoint(*point2);
    if ( point1->getOrd() == point2->getOrd() && point1->getAbs() == point2->getAbs()){
        (cout << "OK"<< endl);
    }else{
        (cout << "erreur"<< endl);
    }

    //test du destructeur
    (cout << "destructeur:"<< endl);
    delete point1;
    delete point2;
}
