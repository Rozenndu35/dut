 #include "CManFch.h"

CManFch::CManFch(string str){
    int separateur = str.find_last_of("/");
    m_emplcmtFich = str.substr(0, separateur+1);
    m_nomFich = str.substr(separateur+1, str.size()-separateur);
    if(str.find_last_of(".txt") == str.size()-1){
        m_typeFich = 1;
    }else if(str.find_last_of(".bin") == str.size()-1 ){
        m_typeFich = 2;
    }else{
        m_typeFich = 0;
    }
    cout << "construction de l'objet CManFch d'adresse :" << this << endl;
}

CManFch::CManFch(){
    this->m_nomFich = "";
    this->m_emplcmtFich = "";
    this->m_typeFich = 0;
    cout << "..." << endl;
}

CManFch::CManFch ( const CManFch& pointToCopy ){
    cout<< "copie constructeur ";
}


CManFch::~CManFch(){
    cout<< "destruction de l'objet CManFch d'adresse : " << this << endl;
}

void CManFch::setNomFichier(string str){
    int separateur = str.find_last_of("/");
    m_emplcmtFich = str.substr(0, separateur+1);
    m_nomFich = str.substr(separateur+1, str.size()-separateur);
    if(str.find_last_of(".txt") == str.size()-1){
        m_typeFich = 1;
    }else if(str.find_last_of(".bin") == str.size()-1 ){
        m_typeFich = 2;
    }else{
        m_typeFich = 0;
    }
    cout << "modification faite" << endl;
}
void CManFch::afficherAttributs(){
    cout << "emplacement : " << m_emplcmtFich << endl;
    cout << "nom : " << m_nomFich << endl;
    cout << "type de fichier : " <<m_typeFich << endl;
}
void CManFch::afficherFichier(){
    if(m_typeFich == 1){
        int ligne = 1;
        string nom= m_emplcmtFich + m_nomFich;
        ifstream fluxIn(nom, ios::in);
        string maChaine ;
        while (!fluxIn.eof()){
            getline(fluxIn, maChaine,'\n');
            if(ligne<10){
                std::cout.fill('0');
                std::cout.width(4);
                std::cout <<std::right << ligne << ":   "<< maChaine << '\n';
            }else if(ligne<100){
                std::cout.fill('0');
                std::cout.width(3);
                std::cout <<std::right << ligne << ":   "<< maChaine << '\n';
            }else if(ligne<1000){
                std::cout.fill('0');
                std::cout.width(2);
                std::cout <<std::right << ligne << ":   "<< maChaine << '\n';
            }else if(ligne<10000){
                std::cout.fill('0');
                std::cout.width(1);
                std::cout <<std::right << ligne << ":   "<< maChaine << '\n';
            }else if(ligne<10000){
                std::cout <<std::right << ligne << ":   "<< maChaine << '\n';
            }
            ligne=ligne+1;
        }
        fluxIn.close();        
    }else if(m_typeFich == 2){
        //_____________________a faire ____________________
    }else{
        cout << "desoler nous ne savons pas lire ce type de fichier " << endl;
    }
}
string CManFch::getLigne( int numLign1 , int numLign2){
    string ret = "";
    if (numLign1 > 0){
        int ligne = 1;
        if(m_typeFich == 1){
            string nom= m_emplcmtFich + m_nomFich;
            ifstream fluxIn(nom, ios::in);
            string maChaine ;
            while (!fluxIn.eof()){
                if( ligne == numLign1 || ligne == numLign2){
                    cout << "rentre " << endl;
                    getline(fluxIn, maChaine,'\n');
                    ret = ret + maChaine;
                }
                ligne=ligne+1;
            }
            fluxIn.close();
        }else if(m_typeFich == 2){
            //_____________________a faire ____________________
        }else{
            cout << "desoler nous ne savons pas lire ce type de fichier " << endl;
        }
        if(ligne < numLign2){
            cout << "erreur numLign2 est pas dans le fichier " << endl;
            ret ="";
        }
    }else{
        cout << "erreur numLign1 ne peut pas etre negatif ou null " << endl;
    }
    return ret;
}
string CManFch::getGroupeOctets(){
    return "";
}