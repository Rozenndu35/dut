#ifndef CMANFCH_H
#define CMANFCH_H

#include "AllIncludes.h"



class CManFch{
    
    private:
        // attributs d’instance 
        string m_nomFich;
        string m_emplcmtFich;
        int m_typeFich;
     
    public :
        // Constructeur publique
        CManFch(string str);
        CManFch(); 
        //copy constructeur
        CManFch (const CManFch& pointToCopy);
        //methode public
        void setNomFichier( string str);
        void afficherAttributs();
        void afficherFichier();
        string getLigne( int numLign1 , int numLign2);
        string getGroupeOctets();
        // DESTRUCTEUR
        ~CManFch();
    
};

#endif