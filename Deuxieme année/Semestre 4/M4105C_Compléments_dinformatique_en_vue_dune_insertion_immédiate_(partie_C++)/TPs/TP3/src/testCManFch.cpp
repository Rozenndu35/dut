#include "CManFch.h"

int main(){ 
    //test constructeur sans parametre
    cout << "constructeur sans parametre"<< endl;
    CManFch *manFch1 = new CManFch ();
    //test des constructeur avec parametre
    cout << "constructeur avec parametre"<< endl;
    CManFch *manFch2 = new CManFch ("../test.txt");
    CManFch *manFch3 = new CManFch ("../test.txt");
    //test des acceseurs
    
    //test des setters
    cout << "Test de setNomFichier"<< endl;
    manFch3->setNomFichier("../modifier.bin");

    //test de afficherAttributs   
    cout << "Test de afficherAttributs"<< endl;
    manFch1->afficherAttributs();
    manFch2->afficherAttributs();
    manFch3->afficherAttributs();

    //test de afficherFichier
    cout << "Test de afficherFichier"<< endl;
    manFch2->afficherFichier();

    //test de getLigne
    cout << "Test de getLigne"<< endl;
    cout << "OK : " << manFch2->getLigne(2,5) << endl;
    cout << "OK : " << manFch2->getLigne(1,5) << endl;
    cout << "OK : " << manFch2->getLigne(6,9) << endl;
    cout << "Erreur : " << manFch2->getLigne(6,10) << endl;
    cout << "Erreur : " << manFch2->getLigne(-1,5) << endl;
    
}
