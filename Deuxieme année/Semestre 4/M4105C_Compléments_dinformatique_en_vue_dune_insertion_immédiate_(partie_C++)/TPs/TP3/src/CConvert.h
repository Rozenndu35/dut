#ifndef CCONVERT_H
#define CCONVERT_H

#include "AllIncludes.h"



class CConvert{
    
    private:
        // attributs d’instance 
        string m_str;
        double m_dbl;
     
    public :
        // Constructeur publique
        CConvert(string theCh);
        CConvert(double theD);
        CConvert(); 
        //copy constructeur
        CConvert (const CConvert& pointToCopy);
        //methode public
        double getReel();
        string getString();
        void affiche();
        // DESTRUCTEUR
        ~CConvert();
    
};

#endif