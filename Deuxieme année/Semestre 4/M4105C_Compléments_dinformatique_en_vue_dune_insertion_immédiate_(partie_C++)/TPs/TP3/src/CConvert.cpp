#include "CConvert.h"

CConvert::CConvert(string theCh){
    
    this->m_str = theCh ;
    istringstream tmp(m_str);
    tmp >> this->m_dbl;

    cout << "construction de l'objet CConvert d'adresse :" << this << endl;
}
CConvert::CConvert(double theD){
    ostringstream tmp;
    this->m_dbl = theD;
    tmp << this->m_dbl;
    this->m_str = tmp.str();
    cout << "construction de l'objet CConvert d'adresse :" << this << endl;
}

CConvert::CConvert(){
    this->m_str = "0" ;
    this->m_dbl = 0 ;
    cout << "..." << endl;
}

CConvert::CConvert ( const CConvert& pointToCopy ){

}

void CConvert::affiche(){
    cout.setf(ios::fixed, ios::floatfield);
    std::cout.precision(3);
    cout<< "La valeur est :" << m_str << endl;
}

string CConvert::getString(){
    return this->m_str;
}
double CConvert::getReel(){
    return this->m_dbl;
}

CConvert::~CConvert(){
    cout<< "destruction de l'objet CConvert d'adresse : " << this << endl;
}