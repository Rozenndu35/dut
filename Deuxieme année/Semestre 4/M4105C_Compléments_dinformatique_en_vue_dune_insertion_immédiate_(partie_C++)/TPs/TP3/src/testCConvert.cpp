#include "CConvert.h"

int main(){
    //test constructeur sans parametre
    cout << "constructeur sans parametre"<< endl;
    CConvert *convert1 = new CConvert ();
    //test des constructeur avec parametre
    cout << "constructeur avec parametre"<< endl;
    CConvert *convert2 = new CConvert ("10");
    CConvert *convert3 = new CConvert (5);
    //test des acceseurs
    cout << "getReel"<< endl;
    if ( convert2->getReel() == 10){
        (cout << "Ok"<< endl);
    }else{
        (cout << "Erreur"<< endl);
    }
    if ( convert3->getReel() == 5){
        (cout << "Ok"<< endl);
    }else{
        (cout << "Erreur"<< endl);
    }
    cout << "getString"<< endl;
    if ( convert2->getString() == "10"){
        (cout << "Ok"<< endl);
    }else{
        (cout << "Erreur"<< endl);
    }
    if ( convert3->getString() == "5"){
        (cout << "Ok"<< endl);
    }else{
        (cout << "Erreur"<< endl);
    }
    //test de afficher   
    cout << "Test de afficher"<< endl;
    convert1->affiche();
    convert2->affiche();
    convert3->affiche();
    //reponse C3 = C1 = C2 marche
}
