#include "Exo7.h"

int main () {
    // Création d’un tableau
    char pCh[TAILLE];
    // Saisie de la chaîne de caractères
    cout << "Une chaîne : "<< '\n';
    cin.getline ( pCh, TAILLE, '\n' );
    //calcul de la taille
    int tailleChaine = strlen (pCh);
    int occurence[26];
    char alphabet[26];
    char* p1 = &pCh[0]; // parcour la chaine pCh
    int* p2 = &occurence[0];
    for (int i =0; i< 26 ; i++){
        occurence[i]=0;
    }
    for (int i =0; i< 26 ; i++){
       alphabet[i] = 'A' + i;
    }
    //cherche les aucurence des lettre
    for (char i ='A' ; i<= 'Z' ; i++){
        for (int j =0 ; j <tailleChaine ; j++){
            if (*p1 == i || *p1==(i+32)){
                (*(int*)p2) ++;
            }
            p1 ++;
        }
        p1 = &pCh[0];
        p2 ++;
    }
    // affiche le resultat
    cout << "Il y a  : "<< '\n';
    for (int i =0; i< 26 ; i++){
        if ( occurence[i]!=0){
            cout << alphabet[i] << " : " << occurence[i] << '\n';
        }
    }
    

}