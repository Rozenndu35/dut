#include "Exo6.h"

int main () {
    // Création d’un tableau
    char unTab[TAILLE];
    // Saisie de la chaîne de caractères
    cout << "Une chaîne (attention max 120 caractere) : "<< '\n';
    cin.getline ( unTab, TAILLE, '\n' );
    //calcul de la taille
    int tailleChaine = strlen ( unTab );
    cout << "La taille de la chaine est de : " << tailleChaine << '\n';
    for( int i =0 ; i<tailleChaine; i++){
        cout << unTab[i]<< '\n';
    }
}
