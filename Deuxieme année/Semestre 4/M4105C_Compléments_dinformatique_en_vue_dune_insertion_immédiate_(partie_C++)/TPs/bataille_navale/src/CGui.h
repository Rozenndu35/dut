#ifndef CGUI_H
#define CGUI_H
#include "BiblioStd.h"
#include "CErreurCreation.h"
#include "CArmada.h"
#include "CCoups.h"
#include "CBaseJeu.h"
using namespace std;

class CGui : public CBaseJeu {

    private:
        char m_grilleJou[TAILLE_GRILLE-1][TAILLE_GRILLE-1];
        char m_grilleAdv[TAILLE_GRILLE-1][TAILLE_GRILLE-1];
        CArmada* m_pArmada;
        CCoups* m_pCoups;

    public :
        CGui();
        CGui(CArmada* pArmada, CCoups* pCoups);
        virtual ~CGui();
        void setArmadaCoups(CArmada* pArmada, CCoups* pCoups);
        virtual bool positionnerBateaux();
        virtual pair<int,int> choisirAttaque();
        virtual void afficheGagne();
        virtual void affichePerdu();
        friend ostream& operator<<( ostream&os, CGui& theG );
        virtual void remplirDeuxGrilles(ostream& os);
    private:
        
        virtual void afficherLaGrille (ostream& os , string jouOuAdv);


};

#endif