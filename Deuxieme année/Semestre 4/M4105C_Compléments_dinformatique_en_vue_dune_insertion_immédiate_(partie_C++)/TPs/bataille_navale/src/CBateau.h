#ifndef CBATEAU_H
#define CBATEAU_H
#include "BiblioStd.h"
#include "CErreurCreation.h"
using namespace std;

class CBateau{
    
    private:
        // attributs d’instance 
        int m_taille; //nombre de case occupe
        string m_nom; //nom du bateau
        pair<int,int> m_position; //coord du point d'ancrage (colone, verticale)
        bool* m_Degats; //tableau de dega true si degat

    public :
        //methode public
        CBateau();
        CBateau(string n, pair<int, int> p , int t );
        ~CBateau();
        CBateau(const CBateau& pointToCopy);
        bool getDegats(int i);
        string getNom();
        int getTaille();
        pair<int , int> getPosition();
        void setPosition(int i , int j);
        bool estCoule();
        bool tirAdverse(pair<int, int> p);
        CBateau& operator= ( const CBateau& bateauToCopy );
        friend ostream& operator<<( ostream&os, CBateau& theB );
    
};

#endif