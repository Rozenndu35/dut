#include "CJoueurServ.h"

CJoueurServ::CJoueurServ ( CBaseJeu* g ) : CJoueur(g) {
	m_pSocServ = NULL;
}

/**********************************************/

void CJoueurServ::openSocket ( int port ) throw (logic_error) {

	// Si cela se passe mal, une exception logic_error est lanc�e
	m_pSocServ = new CServerSocket ( port );
	cout << "Ouverture socket serveur : reussi..." << endl;		
}

/**********************************************/

void CJoueurServ::attenteClient() {

	m_pSocCli = new CClientSocket();
    m_pSocServ->accept ( *m_pSocCli );
    cout << "Client connecte" << endl;
}

/**********************************************/

CArmada* CJoueurServ::getArmadaFromFile() {

	CArmada* f = new CArmada();
	int nbTotalBat = 0;

    // on lit le fichier texte "data.ini" qui d�crit la flottille (elle est identique pour le client et le serveur)
	ifstream fichier("./data.ini");

	if ( fichier ) {

		cout << "Lecture du fichier flotille qui sera envoye ensuite au client" << endl;
		string ligne; // variable contenant chaque ligne lue

		// cette boucle s'arr�te d�s qu'une erreur de lecture survient
		while ( getline(fichier, ligne) ) {
			
			// si c'est une ligne de commentaire ou une ligne vide, on passe
			if (ligne[0] == ';' || ligne[0] == ' ') continue;

			// on d�coupe la chaine suivant le :
			string nom;
			string nombre;
			string taille;
			istringstream iss(ligne);
			getline( iss, nom, ':' );
			getline( iss, nombre, ':' );
			getline( iss, taille, ':' );

			istringstream snombre(nombre);
			int inombre, itaille;
			snombre >> inombre;
			istringstream staille(taille);
			staille >> itaille;

			// mise � jour du nombre total de bateaux
			nbTotalBat = nbTotalBat + inombre;

			// on rajoute les bateaux dans la flotte de depart, ils ne sont pas encore positionnes
			for (int i=0; i<inombre; i++) {
				CBateau b ( nom, pair<int,int>(0,0), itaille );
				//cout <<"___________________________"<<endl;
				//cout << b <<endl;
				f->ajouterBateau(b);
			}
		}

    }

	else cout << "impossible d'ouvrir le fichier data.ini" << endl;

	return f;
}

/**********************************************/

int CJoueurServ::envoisArmadaVersClient ( CArmada& f ) {

	CBateau* unBat;

	// on attend que le client soit pr�t � recevoir
	attendre();

	// il faut envoyer la flotte au client
	string aEnvoyer;

	// on envoie le nombre de lignes � envoyer
	ostringstream oss;    
	oss << f.getEffectifTotal();
	aEnvoyer = oss.str();
	aEnvoyer = aEnvoyer + '\n';
	*m_pSocCli << aEnvoyer;

	// on boucle sur tous les bateaux
	for ( int i = 0; i<f.getEffectifTotal(); i++ ) {
		
		unBat = f.getBateau ( i );
		ostringstream oss;
		// on envoie le nom du bateau et sa taille
		oss << unBat->getTaille();
		aEnvoyer = unBat->getNom() + ":"+ oss.str();
		cout << "Bateau envoye au client : " << aEnvoyer << endl;
		aEnvoyer = aEnvoyer + '\n';
		*m_pSocCli << aEnvoyer;
	}

	cout << "Toute l'armada est envoyee au client" << endl;

	return 0;
}

/**********************************************/

CJoueurServ::~CJoueurServ() {

	cout << "Destructeur de CJoueurServ, adr = " << this << endl;
	if ( m_pSocServ != NULL ) delete m_pSocServ;

	if ( m_pSocCli != NULL ) {
		delete m_pSocCli;
		m_pSocCli = NULL;
	}
}

