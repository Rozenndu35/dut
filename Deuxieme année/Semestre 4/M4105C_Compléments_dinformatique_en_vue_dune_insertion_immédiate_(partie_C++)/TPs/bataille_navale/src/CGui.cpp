/**
 * @author Rozenn Costiou
 * 
*/

#include "CGui.h"
#include "CErreurCreation.h"
#include "CArmada.h"
#include "CCoups.h"

/**
* The default constructor
*/
CGui::CGui(){
    //cout<<"construit sans parametre"<<endl;
    this->m_pArmada = NULL;
    this->m_pCoups = NULL;
}

/**
 * The default constructor
 * @param pArmada : unique player armada
 * @param pCoups : unique fire recording structure
*/
CGui::CGui(CArmada* pArmada, CCoups* pCoups){
    //cout<<"construit avec parametre"<<endl;
    this->m_pArmada = pArmada;
    this->m_pCoups = pCoups;
}
/**
 * the delete
*/
CGui::~CGui(){
    
}
/**
 * Set the value
 * @param pArmada : unique player armada
 * @param pCoups : unique fire recording structure
*/
void CGui::setArmadaCoups(CArmada* pArmada, CCoups* pCoups){
    if (pArmada!=NULL){
        m_pArmada = pArmada;
    }else{
        cout<<"armada pas modifier"<<endl;
    }
    if (pCoups!=NULL){
        m_pCoups = pCoups;
    }else{
        cout<<"coup pas modifier"<<endl;
    }
}
/**
 * position all boats on the grid
*/
bool CGui::positionnerBateaux(){
    bool ret = false;
    if(m_pArmada !=NULL){
        ret= m_pArmada->placerAleatoirement();
    }
    return ret;
}

/**
 * Entering the coordinate (line, column) of the attack
 *@return  the coordinate
*/
pair<int,int> CGui::choisirAttaque(){
    pair<int,int> positionA;
    bool valid = false;
    int ligne=0;
    cout << "Cordonnee de la ligne : "<< '\n';
    cin >> ligne;
    while(!valid){
        if(ligne<0 || ligne >(TAILLE_GRILLE-1)){
            cout << "Cordonnee de la ligne invalide veuiller resaisir : "<< '\n';
            cin >> ligne;
        }else{
            valid = true;
        }
    }
    int colone = 0;
    valid = false;
    cout << "Cordonnee de la colone : "<< '\n';
    cin >> colone;
    while(!valid){
        if(colone<0 || colone >(TAILLE_GRILLE-1)){
            cout << "Cordonnee de la colone invalide veuiller resaisir : "<< '\n';
            cin >> colone;
        }else{
            valid = true;
        }
    }
    positionA.first = ligne;
    positionA.second = colone;
    return positionA; 
}

/**
 *win poster
*/
void CGui::afficheGagne(){
    cout << "La partie est gagner "<< '\n';
}

/**
 * lost poster
*/
void CGui::affichePerdu(){
    cout << "La partie est perdu "<< '\n';
}

/**
* overload <<
*/
ostream& operator<<(ostream& os , CGui& theG){
    theG.remplirDeuxGrilles(os);
    return os;
}

/**
 * fill and display the grids
 * @param os
*/
void CGui::remplirDeuxGrilles(ostream& os){
    //remplis la grille du joueur
    for (int i = 0 ; i < TAILLE_GRILLE -1; i++){
        for (int j = 0 ; j < TAILLE_GRILLE -1 ; j++){
            m_grilleJou[i][j] = '_';
        }
    }
    //recupere les ploufs
    int taille = this->m_pCoups->getTaille("ploufAdverse");
    for (int i = 0; i< taille; i++){
        pair<int,int> position = m_pCoups->getPair("ploufAdverse",i);
        m_grilleJou[position.first][position.second] = 'O';
    }
    //recupere les bateaux et les touche
    taille = m_pArmada->getEffectifTotal();
    for (int i = 0; i< taille; i++){
        CBateau bateau = *m_pArmada->getBateau(i);
        int tailleB = bateau.getTaille();
        int x = bateau.getPosition().first;
        int y = bateau.getPosition().second;
        for (int i = 0 ; i< tailleB; i++){
            if( bateau.getDegats(i)){
                m_grilleJou[x][y] = 'X';
            }else{
                m_grilleJou[x][y] = 'B';
            }
            y++;
        }
    }

    //affiche grille joueur
    afficherLaGrille(os, "joueur");

    //remplis la grille du joueur
    for (int i = 0 ; i < TAILLE_GRILLE -1; i++){
        for (int j = 0 ; j < TAILLE_GRILLE -1 ; j++){
            m_grilleJou[i][j] = '_';
        }
    }
    //recupere les plouf
    taille = m_pCoups->getTaille("dansLEau");
    for (int i = 0; i< taille; i++){
        pair<int,int> position = m_pCoups->getPair("dansLEau",i);
        m_grilleJou[position.first][position.second] = 'O';
    }
    //recupere les toucher
    taille = m_pCoups->getTaille("touche");
    for (int i = 0; i< taille; i++){
        pair<int,int> position = m_pCoups->getPair("touche",i);
        m_grilleJou[position.first][position.second] = 'X';
    }

    //affiche grille advairse
    afficherLaGrille(os, "adversaires");
}

/**
 *display the content of m_grilleJou or m_grilleAdv
 * @param os
 * @param jouOuAdv : the player are the grid should be displayed
*/
void CGui::afficherLaGrille (ostream& os , string jouOuAdv){

    if (jouOuAdv == "joueur"){
        os << "votre grille"<<endl;
        os << endl;
        for(int i = 0; i<TAILLE_GRILLE; i++){
            if(i!=0){
                char iA = 47+i;
                os<< iA;
            }
            for(int j =0 ; j<TAILLE_GRILLE; j++ ){
                if(i==0 && j!=0 ){
                    char jB = 47+j;
                    os<< jB << "\t";
                }else if(i!=0 && j!= 0){
                    os << m_grilleJou[i-1][j-1]<< "\t" ;
                }else{
                    os << "\t";
                }
            }
            os << endl;
        }
        os << endl;

    }else if (jouOuAdv == "adversaires"){
        os << "grille advairse"<<endl;
        os << endl;
        for(int i = 0; i<TAILLE_GRILLE; i++){
            if(i!=0){
                char iA = 47+i;
                os<< iA;
            }
            for(int j =0 ; j<TAILLE_GRILLE; j++ ){
                if(i==0 && j!=0 ){
                    char jB = 47+j;
                    os<< jB << "\t";
                }else if(i!=0 && j!= 0){
                    os << m_grilleJou[i-1][j-1]<< "\t" ;
                }else{
                    os << "\t";
                }
            }
            os << endl;
        }
        os << endl;
    }
}
