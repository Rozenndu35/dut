/**
 * @author Rozenn Costiou
 * Compose from the game's boat collection
*/
#include "CArmada.h"
#include "CBateau.h"
#include "CErreurCreation.h"

/**
 * Add the boat to the armada boat vector
 * @param unBat : the boat to add
 * 
*/
void CArmada::ajouterBateau(CBateau& unBat){
    this->m_listeBateaux.push_back(unBat);
}
/**
 * return the boat to the index give
 * @param i : index
 * @return the boat
*/
CBateau* CArmada::getBateau(int i){
    CBateau* ret;
    int nbBateau = m_listeBateaux.size();
    if (i >= 0 && i < nbBateau ){
        ret = & m_listeBateaux[i];
    }else {
        string erreur = "Ce bateau n'existe pas";

        CErreurCreation e ( (char*)erreur.c_str() );
        throw e;
    }
    return ret;
}
/**
* returns the total number of boats
* @return number of boats
*/
int CArmada::getEffectifTotal(){
    return m_listeBateaux.size();
}

/**
* returns the total number of cases occupied by the armada
* @return the total number of cases occupied
*/
int CArmada::getNbreTotCases(){
    int ret=0;
    int nbBateau = m_listeBateaux.size();
    for (int i = 0 ; i < nbBateau ; i++){
        ret += m_listeBateaux[i].getTaille();
    }
    return ret;
}

/**
* Returns the number of ships that have not yet sunk
* @return the number of boats that are not sunk
*/
int CArmada::getEffectif(){
    int ret=0;
    int nbBateau = m_listeBateaux.size();
    for (int i = 0 ; i < nbBateau ; i++){
        if(!m_listeBateaux[i].estCoule()){
            ret++;
        }
    }
    return ret;
}
/**
* Random and automatic horizontal lapping of all boats on the grid
* @return true if he managed to place them otherwise false
*/
bool CArmada::placerAleatoirement(){
    bool ret = true;
    int nbBateau = m_listeBateaux.size();
    //si il y a asser de place
    if (getNbreTotCases() <= ((TAILLE_GRILLE * TAILLE_GRILLE)-TAILLE_GRILLE)){
        bool sort = false;
        //parcour tout les bateaux
        for (int i = 0; i < nbBateau && !sort; i++){
            bool trouver = false;
            int essai = 0;
            //essaille tant que le test n'a pas trouver et le nombre d'essai autoriser n'est pas atteint
            while ( !trouver && ( essai < MAX_ESSAI)){

                int x = rand()%(TAILLE_GRILLE-1);
                int y = rand()%(TAILLE_GRILLE - m_listeBateaux[i].getTaille()-1);
                essai++;
                
                // verifie paraport au autre bateau
                bool erreur = false;
                for(int j = 0 ; j< i && !erreur ; j++){
                    if(m_listeBateaux[j].getPosition().first == x){
                        if ((y >= (m_listeBateaux[j].getPosition().second -1 )) &&(y <= (m_listeBateaux[j].getPosition().second + m_listeBateaux[j].getTaille()) ) ){
                            erreur = true;
                        }
                    }
                }
                if (!erreur){
                    try{
                        m_listeBateaux[i].setPosition(x,y);
                        trouver = true;
                    }catch( CErreurCreation& c ){}
                }
            }
            if ( !trouver){
                sort = true;
                ret = false;

            }
        }
       
    }else{
        ret = false;
    }
    return ret;
}