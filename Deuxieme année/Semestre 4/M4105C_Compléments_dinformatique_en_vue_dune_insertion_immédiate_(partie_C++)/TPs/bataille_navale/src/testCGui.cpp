
#include "testCGui.h"
#include "CGui.h"
#include "CErreurCreation.h"
#include "CArmada.h"
#include "CCoups.h"

void testConstructeur();
void testSetArmadaCoups();
void testPositionnerBateaux();
void testChoisirAttaque();
void testAfficheGagnePerdu();
void testAffichage();

int main(){ 
    //testConstructeur();
    //testSetArmadaCoups();
    //testPositionnerBateaux();
    //testChoisirAttaque();
    //testAfficheGagnePerdu();
    testAffichage();

}
void testConstructeur(){
    cout << "Constructeur sans parametre"<< endl;
    CGui gui0;
    cout << "Constructeur avec parametre"<< endl;
    cout<<"Cas normal"<<endl;
    CArmada pArmada;
    CCoups pCoups;
    CGui gui1(&pArmada,&pCoups);

}
void testSetArmadaCoups(){
    cout<<"Cas normal"<<endl;
    CArmada pArmada ;
    CCoups pCoups;
    CArmada pArmada1;
    CBateau cb1;
    pair<int,int> position;
    try {
        position.first = 1;
        position.second = 1;
        cb1 = CBateau("porte-avion", position , 4);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    pArmada1.ajouterBateau(cb1);
    CCoups pCoups1 ;
    CGui gui1(&pArmada,&pCoups);
    gui1.setArmadaCoups(&pArmada1,&pCoups1);
    cout<<"Cas limite"<<endl;
    CArmada* pArmada2=NULL;
    CCoups* pCoups2 = NULL;
    CGui gui2(&pArmada,&pCoups);
    gui2.setArmadaCoups(&pArmada1,pCoups2);
    gui2.setArmadaCoups(pArmada2,&pCoups1);
}
void testPositionnerBateaux(){
    cout <<" positionner bateau "<<endl;
    CCoups pCoups;
    int reussi = 0;
    pair<int,int> pos;
    pos.first = 0;
    pos.second = 0;
    CBateau cb1;
    try {
        cb1 = CBateau("bateau1", pos , 2);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb2;
    try {
        cb2 = CBateau("bateau2", pos , 2);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb3;
    try {
        cb3 = CBateau("bateau3", pos , 2);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb4;
    try {
        cb4 = CBateau("bateau4", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb5;
    try {
        cb5 = CBateau("bateau5", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb6;
    try {
        cb6 = CBateau("bateau6", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb7;
    try {
        cb7 = CBateau("bateau7", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb8;
    try {
        cb8 = CBateau("bateau8", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb9;
    try {
        cb9 = CBateau("bateau9", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb10;
    try {
        cb10 = CBateau("bateau10", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb11;
    try {
        cb11 = CBateau("bateau11", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb12;
    try {
        cb12 = CBateau("bateau12", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb13;
    try {
        cb13 = CBateau("bateau13", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb14;
    try {
        cb14 = CBateau("bateau14", pos , 4);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb15;
    try {
        cb15 = CBateau("bateau15", pos , 4);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb16;
    try {
        cb16 = CBateau("bateau16", pos , 4);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb17;
    try {
        cb17 = CBateau("bateau17", pos , 4);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb18;
    try {
        cb18 = CBateau("bateau18", pos , 5);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb19;
    try {
        cb19 = CBateau("bateau19", pos , 5);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb20;
    try {
        cb20 = CBateau("bateau20", pos , 5);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb21;
    try {
        cb21 = CBateau("bateau21", pos , 5);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb22;
    try {
        cb22 = CBateau("bateau22", pos , 5);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb23;
    try {
        cb23 = CBateau("bateau23", pos , 5);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb24;
    try {
        cb24 = CBateau("bateau24", pos , 5);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }

    CBateau cb25;
    try {
        cb25 = CBateau("bateau25", pos , 6);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb26;
    try {
        cb26 = CBateau("bateau26", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    
    CBateau cb27;
    try {
        cb27 = CBateau("bateau27", pos , 6);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb28;
    try {
        cb28 = CBateau("bateau28", pos , 6);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    //cout <<"Cas normal"<<endl;
    CArmada amardaOk;
    amardaOk.ajouterBateau(cb1);
    amardaOk.ajouterBateau(cb21);
    amardaOk.ajouterBateau(cb13);
    amardaOk.ajouterBateau(cb15);
    amardaOk.ajouterBateau(cb11);
    amardaOk.ajouterBateau(cb6);
    amardaOk.ajouterBateau(cb25);
    amardaOk.ajouterBateau(cb26);

    bool ret =false;
    int test =0;
    for (int i = 0; i<10 ; i++){
        ret = false;
        test ++;
        CGui gui1(&amardaOk,&pCoups);
        ret = gui1.positionnerBateaux();
        if (ret){
            reussi ++;
        }
    }
    
    //cout << "Cas d'erreur  " <<endl;
    CArmada amardaErreur;
    amardaErreur.ajouterBateau(cb1);
    amardaErreur.ajouterBateau(cb2);
    amardaErreur.ajouterBateau(cb3);
    amardaErreur.ajouterBateau(cb4);
    amardaErreur.ajouterBateau(cb5);
    amardaErreur.ajouterBateau(cb6);
    amardaErreur.ajouterBateau(cb7);
    amardaErreur.ajouterBateau(cb8);
    amardaErreur.ajouterBateau(cb9);
    amardaErreur.ajouterBateau(cb10);
    amardaErreur.ajouterBateau(cb11);
    amardaErreur.ajouterBateau(cb12);
    amardaErreur.ajouterBateau(cb13);
    amardaErreur.ajouterBateau(cb14);
    amardaErreur.ajouterBateau(cb15);
    amardaErreur.ajouterBateau(cb16);
    amardaErreur.ajouterBateau(cb17);
    amardaErreur.ajouterBateau(cb18);
    amardaErreur.ajouterBateau(cb19);
    amardaErreur.ajouterBateau(cb20);
    amardaErreur.ajouterBateau(cb21);
    amardaErreur.ajouterBateau(cb22);
    amardaErreur.ajouterBateau(cb23);
    amardaErreur.ajouterBateau(cb24);
    amardaErreur.ajouterBateau(cb24);
    amardaErreur.ajouterBateau(cb24);
    amardaErreur.ajouterBateau(cb25);
    amardaErreur.ajouterBateau(cb26);
    amardaErreur.ajouterBateau(cb27);
    amardaErreur.ajouterBateau(cb28);

    for (int i = 0; i<10 ; i++){
        ret=false;
        test ++;
        CGui gui1(&amardaErreur,&pCoups);
        ret = gui1.positionnerBateaux();
        if (!ret){
            reussi ++;
        }
    }

    cout <<"Test reussi : " << reussi << " sur " << test <<endl;
}
void testChoisirAttaque(){
    cout<<"Cas normal avec 5-2"<<endl;
    CArmada pArmada;
    CCoups pCoups;
    CGui gui1(&pArmada,&pCoups);
    pair<int,int> position1 = gui1.choisirAttaque();
    if(position1.first == 5 && position1.second == 2){
        cout<<"test OK"<<endl;
    }
    cout<<"Cas limite"<<endl;
    cout<<"pour 0-2"<<endl;
    pair<int,int> position2 = gui1.choisirAttaque();
    if(position2.first == 0 && position2.second == 2){
        cout<<"test OK"<<endl;
    }
    cout<<"pour 10-2"<<endl;
    pair<int,int> position3 = gui1.choisirAttaque();
    if(position3.first == 10 && position3.second == 2){
        cout<<"test OK"<<endl;
    }
    cout<<"pour 2-0"<<endl;
    pair<int,int> position4 = gui1.choisirAttaque();
    if(position4.first == 2 && position4.second == 0){
        cout<<"test OK"<<endl;
    }
    cout<<"pour 2-10"<<endl;
    pair<int,int> position5 = gui1.choisirAttaque();
    if(position5.first == 2 && position5.second == 10){
        cout<<"test OK"<<endl;
    }
    cout<<"Cas erreur"<<endl;
    cout<<"pour x -3 et 12 / pour y = -6 et 12"<<endl;
    pair<int,int> positionErreur = gui1.choisirAttaque();
    
}
void testAfficheGagnePerdu(){
    CArmada pArmada;
    CCoups pCoups;
    CGui gui1(&pArmada,&pCoups);
    gui1.afficheGagne();
    gui1.affichePerdu();
}
void testAffichage(){
    CArmada pArmada;
    CCoups pCoups;
    CGui gui1(&pArmada,&pCoups);
    cout << "grille vide" << endl;
    //cout << gui1 << endl;
    cout << "grille avec des bateaux"<<endl;
    CBateau cb2;
    pair<int,int> position2;
    try {
        position2.first = 1;
        position2.second = 1;
        cb2 = CBateau("bateau2", position2 , 4);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb1;
    pair<int,int> position;
    try {
        position.first = 5;
        position.second = 3;
        cb1 = CBateau("bateau1", position , 5);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    pair<int,int> tir1;
    tir1.first =1;
    tir1.second = 1;
    bool ret = cb2.tirAdverse(tir1);
    pArmada.ajouterBateau(cb1);
    pArmada.ajouterBateau(cb2);
    
    //cout<<cb1<<endl;
    //cout<<cb2<<endl;
    cout<<gui1;
}