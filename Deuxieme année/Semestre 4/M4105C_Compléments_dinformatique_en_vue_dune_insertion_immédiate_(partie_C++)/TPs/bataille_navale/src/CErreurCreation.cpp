#include "CErreurCreation.h"

// Constructeur
CErreurCreation::CErreurCreation ( char* theText ) {
    m_pText = new char [ strlen ( theText ) + 1 ];
    strcpy ( m_pText, theText );
}

// Copy-Constructeur 
CErreurCreation::CErreurCreation ( const CErreurCreation& theObj ) {
    m_pText = new char [ strlen ( theObj.m_pText ) + 1 ];
    strcpy ( m_pText, theObj.m_pText );
}

// Accesseur
char* CErreurCreation::getText ( ) { return m_pText; }

// Destructeur
CErreurCreation::~CErreurCreation () {
    if ( m_pText != NULL ) delete m_pText;
}