#include "CClientSocket.h"

CClientSocket::CClientSocket () {
	// On laisse les attributs ind�termin�s (voir CSocket)
}

/**********************************************/

CClientSocket::CClientSocket ( string host, int port ) throw (logic_error) {

	if ( !methCreate() ) {
        cout << "Impossible de se connecter au serveur " << host << endl;
		logic_error e ( "Erreur : impossible de creer la socket Client" );
		throw e;
    }

  	if ( !methConnect(host, port) ) {
        cout << "Impossible de se connecter au serveur " << host << endl;
		logic_error e ( "Erreur : pas de connexion au port" );
		throw e;
    }
}

/**********************************************/

const CClientSocket& CClientSocket::operator << (const string& s) const {

	if ( !methSend (s) ) {
	  	cout << "Impossible d'ecrire sur la socket..." << endl;
	}

  	return *this;
}

/**********************************************/

const CClientSocket& CClientSocket::operator >> ( string& s ) const {

	if ( !methRecv (s) ) {
      		cout << "Impossible de lire sur la socket..." << endl;
    }

  	return *this;
}

/**********************************************/

string CClientSocket::uneLigne () {
	// m�thode h�rit�e
	return ( recvLine () );
}

