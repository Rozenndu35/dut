#ifndef CTESTCARMADA_H
#define CTESTCARMADA_H
#include "BiblioStd.h"
#include "CBateau.h"
#include "CArmada.h"

#include "CErreurCreation.h"
using namespace std;

void testCArmadaAjouterBateau();
void testCArmadaGet();
void testCArmadaPlacerAleatoirement();

#endif