#include "CBateau.h"
#include "CErreurCreation.h"

void testCBateauConstructeur();
void testCBateauGet();
void testCBateauSet();
void testCBateauEstCoule();
void testCBateauTirAdversaire();
void testCBateauFriend();

int main(){ 
    //testCBateauConstructeur();
    //testCBateauGet();
    //testCBateauSet();
    //testCBateauEstCoule();
    //testCBateauTirAdversaire();
    testCBateauFriend();

}
void testCBateauConstructeur(){
    int reussi = 0;
    cout <<"Constructeur sans parametre"<<endl;
    CBateau cb1 = CBateau();
    cout <<"Constructeur avec parametre"<<endl;
    //cout <<"Cas normal"<<endl;
    try {
        pair<int,int> position;
        position.first = 1;
        position.second = 1;
        CBateau cbOK = CBateau("porte-avion", position , 4);
        reussi = reussi+1;
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    //cout << "Cas d'erreur sur la taille " <<endl;
    try {
        pair<int,int> position;
        position.first = 1;
        position.second = 1;
        CBateau cbOK = CBateau("porte-avion", position , -1);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
        reussi = reussi+1;
    }
    try {
        pair<int,int> position;
        position.first = 1;
        position.second = 1;
        CBateau cbOK = CBateau("porte-avion", position , TAILLE_GRILLE+1);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
        reussi = reussi+1;
    }
    try {
        pair<int,int> position;
        position.first = 1;
        position.second = 1;
        CBateau cbOK = CBateau("porte-avion", position , 10);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
        reussi = reussi+1;
    }
    //cout << "Cas l'imitte sur la taille " <<endl;
    try {
        pair<int,int> position;
        position.first = 1;
        position.second = 1;
        CBateau cbOK = CBateau("porte-avion", position , 1);
        reussi = reussi+1;
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    try {
        pair<int,int> position;
        position.first = 1;
        position.second = 1;
        CBateau cbOK = CBateau("porte-avion", position , 6);
        reussi = reussi+1;
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }

    //cout << "Cas d'erreur sur la position" <<endl;
    try {
        pair<int,int> position;
        position.first = -1;
        position.second = 1;
        CBateau cbOK = CBateau("porte-avion", position , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
        reussi = reussi+1;
    }
    try {
        pair<int,int> position;
        position.first = 18;
        position.second = 1;
        CBateau cbOK = CBateau("porte-avion", position , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
        reussi = reussi+1;
    }
    try {
        pair<int,int> position;
        position.first = 1;
        position.second = -5;
        CBateau cbOK = CBateau("porte-avion", position , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
        reussi = reussi+1;
    }
    try {
        pair<int,int> position;
        position.first = 1;
        position.second = 20;
        CBateau cbOK = CBateau("porte-avion", position , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
        reussi = reussi+1;
    }
    //cout << "Cas l'imitte sur la position " <<endl;
    try {
        pair<int,int> position;
        position.first = 0;
        position.second = 1;
        CBateau cbOK = CBateau("porte-avion", position , 5);
        reussi = reussi+1;
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    try {
        pair<int,int> position;
        position.first = 9;
        position.second = 1;
        CBateau cbOK = CBateau("porte-avion", position , 5);
        reussi = reussi+1;
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    try {
        pair<int,int> position;
        position.first = 1;
        position.second = 0;
        CBateau cbOK = CBateau("porte-avion", position , 5);
        reussi = reussi+1;
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    try {
        pair<int,int> position;
        position.first = 1;
        position.second = 4;
        CBateau cbOK = CBateau("porte-avion", position , 5);
        reussi = reussi+1;
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }

    cout <<"Test reussi : " << reussi << " sur 14" <<endl;

}
void testCBateauGet(){
    int reussi = 0;
    CBateau cb;
    pair<int,int> position;
    try {
        position.first = 1;
        position.second = 1;
        cb = CBateau("porte-avion", position , 4);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    cout <<"getDegats Attention ici rien n'est touché"<<endl;
    //cout <<"Cas normal"<<endl;
    if (!cb.getDegats(1)){
            reussi ++;
    }
    
    //cout <<"Cas limit"<<endl;
    if (!cb.getDegats(0)){
            reussi ++;
    }
    if (!cb.getDegats(3)){
            reussi ++;
    }
   
    //cout <<"Cas d'erreur"<<endl;
    try {
        bool degat = cb.getDegats(-4);
    }catch ( CErreurCreation& c ) {
        reussi ++;
        //cout <<"Pb : " << c.getText() <<endl;
    }
    try {
        bool degat = cb.getDegats(6);
    }catch ( CErreurCreation& c ) {
        reussi ++;
        //cout <<"Pb : " << c.getText() <<endl;
    }
    cout <<"Test reussi : " << reussi << " sur 5" <<endl;

    cout <<"getDegats une case a etait touche"<<endl;
    reussi = 0;
    pair<int,int> tir1;
    tir1.first = 2;
    tir1.second = 1;
    cb.tirAdverse(tir1);
    if (cb.getDegats(1)){
            reussi ++;
    }
     cout <<"Test reussi : " << reussi << " sur 1" <<endl;

    cout <<"getNom "<<endl;
    //cout <<"Cas normal"<<endl;
    reussi = 0;
    if (cb.getNom() == "porte-avion"){
            reussi ++;
    }
    cout <<"Test reussi : " << reussi << " sur 1" <<endl;

    cout <<"getTaille "<<endl;
    //cout <<"Cas normal"<<endl;
    reussi = 0;
    if (cb.getTaille() == 4){
            reussi ++;
    }
    cout <<"Test reussi : " << reussi << " sur 1" <<endl;

    cout <<"getPosition "<<endl;
    //cout <<"Cas normal"<<endl;
    reussi = 0;
    if (cb.getPosition() == position){
            reussi ++;
    }
    cout <<"Test reussi : " << reussi << " sur 1" <<endl;

   

}
void testCBateauSet(){
    int reussi = 0;
    CBateau cb;
    pair<int,int> position;
    try {
        position.first = 1;
        position.second = 1;
        cb = CBateau("porte-avion", position , 5);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    //cout <<"Cas normal"<<endl;
    
    pair<int,int> nPosition;
    nPosition.first = 2;
    nPosition.second = 2;
    try{
        cb.setPosition(2 , 2);
        if(cb.getPosition() == nPosition){
            reussi++;
        }
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    
    
    //cout <<"Cas limit"<<endl;
    nPosition.first = 0;
    nPosition.second = 3;
    try{
        cb.setPosition(0 , 3);
        if(cb.getPosition() == nPosition){
            reussi++;
        }
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    nPosition.first = 9;
    nPosition.second = 3;
    try{
        cb.setPosition(9 , 3);
        if(cb.getPosition() == nPosition){
            reussi++;
        }
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    nPosition.first = 2;
    nPosition.second = 0;
    try{
        cb.setPosition(2 , 0);
        if(cb.getPosition() == nPosition){
            reussi++;
        }
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    nPosition.first = 2;
    nPosition.second = 4;
    try{
        cb.setPosition(2 , 4);
        if(cb.getPosition() == nPosition){
            reussi++;
        }
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
   
    //cout <<"Cas d'erreur"<<endl;
    try{
        cb.setPosition(-1 , 2 );
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
        reussi++;
    }
    try{
        cb.setPosition(15 , 2);
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
        reussi++;
    }
    try{
        cb.setPosition(4 , -3);
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
        reussi++;
    }
    try{
        cb.setPosition(2 , 19);
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
        reussi++;
    }
    cout <<"Test reussi : " << reussi << " sur 9" <<endl;
}
void testCBateauEstCoule(){
    int reussi = 0;
    CBateau cb;
    pair<int,int> position;
    try {
        position.first = 1;
        position.second = 1;
        cb = CBateau("porte-avion", position , 4);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    cout <<"estCoule Attention ici rien n'est touché"<<endl;
    //cout <<"Cas normal"<<endl;
    if (!cb.estCoule()){
            reussi ++;
    }
    cout <<"estCoule touche a etait tester"<<endl;
    pair<int,int> tir1;
    tir1.first = 2;
    tir1.second = 1;
    cb.tirAdverse(tir1);
    if (!cb.estCoule()){
        reussi ++;
    }
    pair<int,int> tir2;
    tir2.first = 1;
    tir2.second = 1;
    cb.tirAdverse(tir2);
    pair<int,int> tir3;
    tir3.first = 3;
    tir3.second = 1;
    cb.tirAdverse(tir3);
    pair<int,int> tir4;
    tir4.first = 4;
    tir4.second = 1;
    cb.tirAdverse(tir4);
    if (cb.estCoule()){
        reussi ++;
    }
    cout <<"Test reussi : " << reussi << " sur 3" <<endl;
}
void testCBateauTirAdversaire(){
    int reussi = 0;
    CBateau cb;
    try {
        pair<int,int> position;
        position.first = 1;
        position.second = 1;
        cb = CBateau("porte-avion", position , 4);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    cout <<"tirAdverse "<<endl;
    //cout <<"Cas normal"<<endl;
    pair<int,int> tir1;
    tir1.first = 2;
    tir1.second = 1;
    if (cb.tirAdverse(tir1)){
            reussi ++;
    }
    pair<int,int> tir2;
    tir2.first = 2;
    tir2.second = 1;
    if (!cb.tirAdverse(tir2)){
            reussi ++;
    }
    pair<int,int> tir3;
    tir3.first = 6;
    tir3.second = 1;
    if (!cb.tirAdverse(tir3)){
            reussi ++;
    }
    pair<int,int> tir4;
    tir4.first = 2;
    tir4.second = 5;
    if (!cb.tirAdverse(tir4)){
        reussi ++;
    }
    cout <<"Test reussi : " << reussi << " sur 4" <<endl;
}
void testCBateauFriend(){
    cout <<"affichage";
    CBateau cb;
    pair<int,int> position;
    try {
        position.first = 1;
        position.second = 1;
        cb = CBateau("porte-avion", position , 4);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    cout <<"bateau complet"<<endl;
    cout << cb;
    cout <<"bateau toucher"<<endl;
    pair<int,int> tir1;
    tir1.first = 2;
    tir1.second = 1;
    cb.tirAdverse(tir1);
    cout << cb;
    cout <<"bateau couler"<<endl;
    pair<int,int> tir2;
    tir2.first = 1;
    tir2.second = 1;
    cb.tirAdverse(tir2);
    pair<int,int> tir3;
    tir3.first = 3;
    tir3.second = 1;
    cb.tirAdverse(tir3);
    pair<int,int> tir4;
    tir4.first = 4;
    tir4.second = 1;
    cb.tirAdverse(tir4);
    cout << cb;
}