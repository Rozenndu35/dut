#ifndef CARMADA_H
#define CARMADA_H
#include "BiblioStd.h"
#include "CErreurCreation.h"
#include "CBateau.h"
using namespace std;

class CArmada{
    
    private:
        // attributs d’instance 
        vector<CBateau> m_listeBateaux;

    public :
        //methode public
        void ajouterBateau(CBateau& unBat);
        CBateau* getBateau(int i);
        int getEffectifTotal();
        int getNbreTotCases();
        int getEffectif();
        bool placerAleatoirement();
    
};

#endif