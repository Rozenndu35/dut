#ifndef TESTCGUI_H
#define TESTCGUI_H
#include "BiblioStd.h"
#include "CBateau.h"
#include "CArmada.h"
#include "CGui.h"
#include "CCoups.h"
#include "CBaseJeu.h"
#include "CErreurCreation.h"
using namespace std;

void testConstructeur();
void testSetArmadaCoups();
void testPositionnerBateaux();
void testChoisirAttaque();
void testAfficheGagnePerdu();
void testAffichage();

#endif