#ifndef CTESTCBATEAU_H
#define CTESTCBATEAU_H
#include "BiblioStd.h"
#include "CBateau.h"
#include "CErreurCreation.h"
using namespace std;

void testCBateauConstructeur();
void testCBateauGet();
void testCBateauSet();
void testCBateauEstCoule();
void testCBateauTirAdversaire();
void testCBateauFriend();

#endif
