#ifndef CTEST_H
#define CTEST_H
#include "BiblioStd.h"
using namespace std;

class CErreurCreation {
    
    private :
        char* m_pText;
    
    public :
    // Constructeurs
        CErreurCreation ( char* theText );
        CErreurCreation (const CErreurCreation& theObj); 
    // Accesseur
        char* getText ( );
    // Destructeur
    ~CErreurCreation ();
};


#endif
