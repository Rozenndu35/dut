#include "CServerSocket.h"

CServerSocket::CServerSocket ( int port ) throw ( logic_error ) {

	if ( !methCreate() ) {
		cout << "Erreur d'ouverture de socket..." << endl;
		logic_error e ( "Erreur : impossible de creer la socket Serveur" );
		throw e;
  	}

  	if ( !methBind (port) ) {
		cout << "Erreur d'ouverture de socket..." << endl;
		logic_error e ( "Erreur : pas de connexion au port" );
		throw e;
  	}

  	if ( !methListen () ) {
		cout << "Erreur d'ouverture de socket..." << endl;
		logic_error e ( "Erreur : pas d'ecoute sur la socket" );
		throw e;
	}
}

/**********************************************/

const CServerSocket& CServerSocket::operator << ( const string& s ) const {

  	if ( !methSend (s) ) {
	      cout << "Impossible d'ecrire sur la socket..." << endl;
  	}

  	return *this;
}

/**********************************************/

const CServerSocket& CServerSocket::operator >> ( string& s ) const {

	if ( !methRecv (s) ) {
      		cout << "Impossible de lire sur la socket..." << endl;
	}

  	return *this;
}

/**********************************************/

void CServerSocket::accept ( CClientSocket& sock ) {

	if ( !methAccept (sock) ) {
		cout << "Refus d'accepter la socket client..." << endl;
	}
}

