/**
 * @author Rozenn Costiou
 * 
*/

#include "CBateau.h"
#include "CErreurCreation.h"
/**
* The default constructor
*/
CBateau::CBateau(){
    m_nom = "neant";
    m_position.first = 0;
    m_position.second = 0;
    m_taille = 0;
    m_Degats = NULL;
    //cout << "Constructeur sans parametre" << endl;
}
/**
* The constructor
* @param n : name of the boat
* @param p : the coordinate of the boat
* @param t : the length of the boat
*/
CBateau::CBateau(string n, pair<int, int> p , int t ){
    //cout << "Constructeur avec parametre" << endl;
    m_nom = n;
    if(t>0 && t<=6 && t<=TAILLE_GRILLE-1){
        m_taille = t;
    }else{
        m_taille = 0;
        string erreur = "Taille incorrecte pour un bateau";

        CErreurCreation e ( (char*)erreur.c_str() );
        throw e;
        
    }
    if ( p.first >= 0 && p.first < TAILLE_GRILLE-1 && p.second >= 0 && p.second < (TAILLE_GRILLE - m_taille-1) ){
        m_position = p;
    }else{
        m_position.first = 0;
        m_position.second = 0;
        string erreur = "Position incorect pour un bateau";
        CErreurCreation e ( (char*) erreur.c_str() );
        throw e;
    }
    m_Degats = new bool[m_taille];
    for (int i ; i < m_taille ; i++){
        m_Degats[i]=false;
    }
    
}
/**
* the copy constructeur
*/
CBateau::CBateau ( const CBateau& bateauToCopy ){
    this-> m_nom = bateauToCopy.m_nom;
    this-> m_position = bateauToCopy.m_position;
    this->m_taille = bateauToCopy.m_taille;
    this->m_Degats = new bool[this->m_taille];
    for(int i = 0; i<this->m_taille; i++){
        m_Degats[i]=bateauToCopy.m_Degats[i];
    }
}
/**
* overload ==
*/
CBateau& CBateau::operator= ( const CBateau& bateauToCopy ){
    if (this != &bateauToCopy){
        this-> m_nom = bateauToCopy.m_nom;
        this-> m_position = bateauToCopy.m_position;
        this->m_taille = bateauToCopy.m_taille;
        //detruire tableau c1 
        //copier tabeau c2 dans c1
        if (m_Degats!= NULL){
            delete[] m_Degats;
        }
        this->m_Degats = new bool[this->m_taille];
        for(int i = 0; i<this->m_taille; i++){
            m_Degats[i]=bateauToCopy.m_Degats[i];
        }
    }
    return *this;
}
/**
* the delete
*/
CBateau::~CBateau(){
    if (m_Degats!=NULL){
        delete[] m_Degats;
    }
}
/**
 * @return true if the boat's box was hit
*/
bool CBateau::getDegats(int i){
    bool ret = false;
    if(i>=0 && i<=m_taille){
        ret = m_Degats[i];
    }else{
        string erreur = "En dehors du bateau";
        CErreurCreation e ( (char*) erreur.c_str() );
        throw e;
    }
    return ret;
}
/**
 * get the name of the boat
 * @return the name
*/
string CBateau::getNom(){
    return m_nom;
}
/**
* get the size of the boat
* @return the size
*/
int CBateau::getTaille(){
    return m_taille;
}

/**
* get the place of the boat
*/
pair<int , int> CBateau::getPosition(){
    return m_position;
}

/**
* @param i : the x
* @param j : the y
* set the place of the boat
*/
void CBateau::setPosition(int i , int j){
    if ( i >= 0 && i < TAILLE_GRILLE-1 && j >= 0 && j < (TAILLE_GRILLE - m_taille-1) ){
        m_position.first = i;
        m_position.second = j;
    }else{
        string erreur = "Position incorect pour un bateau";
        CErreurCreation e ( (char*) erreur.c_str() );
        throw e;
    }
}
/**
* 
* @return true if the boat was stuck
*/
bool CBateau::estCoule(){

    bool ret = true;
    for (int i =0; i < m_taille && ret ; i++){ 
        if(!m_Degats[i]){
            ret = false;
        }
    }
    return ret;
}
/**
*/
bool CBateau::tirAdverse(pair<int, int> p){
    bool ret = false;
        if (p.second == m_position.second ){
            if (p.first >= m_position.first && p.first < m_position.first+m_taille){
                if(m_Degats[p.first - m_position.first] == false){
                    ret = true;
                    m_Degats[p.first - m_position.first] = true;
                }
            }
        }
    return ret;

}

/**
* overload <<
*/
ostream& operator<<(ostream& os , CBateau& theB){
    os << "nom : "<< theB.m_nom << endl;
    os << "position : ( "<< theB.m_position.first <<" ; " << theB.m_position.second <<" )" << endl;
    for (int i = 0 ; i < theB.m_taille ; i++){
        if (theB.m_Degats[i]){
             os << " X " ;
        }else{
            os << " B " ;
        }
    }
    os << endl;
    return os;
}