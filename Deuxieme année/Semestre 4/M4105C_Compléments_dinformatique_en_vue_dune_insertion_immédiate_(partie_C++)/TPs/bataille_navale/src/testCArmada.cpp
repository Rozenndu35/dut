#include "CArmada.h"
#include "CErreurCreation.h"

void testCArmadaAjouterBateau();
void testCArmadaGet();
void testCArmadaPlacerAleatoirement();

int main(){ 
    //testCArmadaAjouterBateau();
    testCArmadaGet();
    //testCArmadaPlacerAleatoirement();

}
void testCArmadaAjouterBateau(){
    CArmada amarda;
    cout <<" ajouterBateau "<<endl;
    //cout <<"Cas normal"<<endl;
    CBateau cb1;
    pair<int,int> position;
    try {
        position.first = 1;
        position.second = 1;
        cb1 = CBateau("porte-avion", position , 4);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    amarda.ajouterBateau(cb1);
}
void testCArmadaGet(){
    int reussi = 0;
    CArmada amardaVide;
    CArmada amardaTrois;
    CArmada amardaDeuxUnCoule;
    CArmada amardaCoule;
    // ________________________________________________
    CBateau cb1;
    pair<int,int> position1;
    try {
        position1.first = 1;
        position1.second = 1;
        cb1 = CBateau("bateau1", position1 , 4);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    amardaTrois.ajouterBateau(cb1);
    CBateau cb2;
    pair<int,int> position2;
    try {
        position2.first = 1;
        position2.second = 1;
        cb2 = CBateau("bateau2", position2 , 5);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    amardaTrois.ajouterBateau(cb2);
    CBateau cb3;
    pair<int,int> position3;
    try {
        position3.first = 1;
        position3.second = 1;
        cb3 = CBateau("bateau3", position3 , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    amardaTrois.ajouterBateau(cb3);
    // ________________________________________________
    CBateau cb4;
    pair<int,int> position4;
    try {
        position4.first = 5;
        position4.second = 1;
        cb4 = CBateau("bateau1", position4 , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    amardaDeuxUnCoule.ajouterBateau(cb4);
    CBateau cb5;
    pair<int,int> position5;
    try {
        position5.first = 1;
        position5.second = 1;
        cb5 = CBateau("bateau2", position2 , 2);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    amardaDeuxUnCoule.ajouterBateau(cb5);
    pair<int,int> tir1;
    tir1.first = 1;
    tir1.second = 1;
    pair<int,int> tir2;
    tir2.first = 2;
    tir2.second = 1;
    cb5.tirAdeverse(tir1);
    cb5.tirAdeverse(tir2);
    // ________________________________________________
    CBateau cb6;
    pair<int,int> position6;
    try {
        position6.first = 1;
        position6.second = 1;
        cb6 = CBateau("bateau1", position6 , 2);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    amardaCoule.ajouterBateau(cb6);
    cb6.tirAdeverse(tir1);
    cb6.tirAdeverse(tir2);
    


    cout <<" getBateau "<<endl;
    //cout <<"Cas normal"<<endl;
    try{
        CBateau* gb1= amardaTrois.getBateau(1);
        if ((*gb1).getNom() == cb2.getNom()){
            reussi ++;
        }
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    
        
    //cout <<"Cas limit"<<endl; 
     try{
        CBateau* gb2= amardaTrois.getBateau(0);
        if ((*gb2).getNom() == cb1.getNom()){
            reussi ++;
        }
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    
    try{
        CBateau* gb3= amardaTrois.getBateau(2);
        if ((*gb3).getNom() == cb3.getNom()){
            reussi ++;
        }
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    
    //cout <<"Cas d'erreur"<<endl;
    try{
        CBateau* gb4= amardaTrois.getBateau(-2);
    }catch ( CErreurCreation& c ) {
        reussi ++;
        //cout <<"Pb : " << c.getText() <<endl;
    }
    try{
        CBateau* gb5= amardaTrois.getBateau(12);
    }catch ( CErreurCreation& c ) {
        reussi ++;
        //cout <<"Pb : " << c.getText() <<endl;
    }
    cout <<"Test reussi : " << reussi << " sur 5 " <<endl;

    reussi = 0;
    cout <<" getEffectifTotal "<<endl;
    //cout <<"Cas normal"<<endl;
    if( amardaTrois.getEffectifTotal() == 3){
        reussi ++;
    }
    //cout <<"Cas limit"<<endl; 
    if( amardaVide.getEffectifTotal() == 0){
        reussi ++;
    }

    cout <<"Test reussi : " << reussi << " sur 2" <<endl;

    reussi = 0;
    cout <<" getNbreTotCases "<<endl;
    //cout <<"Cas normal"<<endl;
    if( amardaTrois.getNbreTotCases() == 12){
        reussi ++;

    }
    //cout <<"Cas limit"<<endl; 
    if( amardaVide.getNbreTotCases() == 0){
        reussi ++;
    }
    //cout <<"Cas d'erreur"<<endl;

    cout <<"Test reussi : " << reussi << " sur 2" <<endl;
    
    reussi = 0;
    cout <<" getEffectif "<<endl;
    //cout <<"Cas normal"<<endl;
    if( amardaTrois.getEffectif() == 3){
        reussi ++;
    }
    if( amardaDeuxUnCoule.getEffectif() == 1){
        reussi ++;
    }
    //cout <<"Cas limit"<<endl; 
    if( amardaCoule.getEffectif() == 0){
        reussi ++;
    }
    cout <<"Test reussi : " << reussi << " sur 3 " <<endl;


   
}
void testCArmadaPlacerAleatoirement(){
    cout <<" placerAleatoirement "<<endl;
    int reussi = 0;
    pair<int,int> pos;
    pos.first = 0;
    pos.second = 0;
    CBateau cb1;
    try {
        cb1 = CBateau("bateau1", pos , 2);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb2;
    try {
        cb2 = CBateau("bateau2", pos , 2);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb3;
    try {
        cb3 = CBateau("bateau3", pos , 2);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb4;
    try {
        cb4 = CBateau("bateau4", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb5;
    try {
        cb5 = CBateau("bateau5", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb6;
    try {
        cb6 = CBateau("bateau6", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb7;
    try {
        cb7 = CBateau("bateau7", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb8;
    try {
        cb8 = CBateau("bateau8", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb9;
    try {
        cb9 = CBateau("bateau9", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb10;
    try {
        cb10 = CBateau("bateau10", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb11;
    try {
        cb11 = CBateau("bateau11", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb12;
    try {
        cb12 = CBateau("bateau12", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb13;
    try {
        cb13 = CBateau("bateau13", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb14;
    try {
        cb14 = CBateau("bateau14", pos , 4);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb15;
    try {
        cb15 = CBateau("bateau15", pos , 4);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb16;
    try {
        cb16 = CBateau("bateau16", pos , 4);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb17;
    try {
        cb17 = CBateau("bateau17", pos , 4);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb18;
    try {
        cb18 = CBateau("bateau18", pos , 5);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb19;
    try {
        cb19 = CBateau("bateau19", pos , 5);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb20;
    try {
        cb20 = CBateau("bateau20", pos , 5);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb21;
    try {
        cb21 = CBateau("bateau21", pos , 5);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb22;
    try {
        cb22 = CBateau("bateau22", pos , 5);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb23;
    try {
        cb23 = CBateau("bateau23", pos , 5);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb24;
    try {
        cb24 = CBateau("bateau24", pos , 5);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }

    CBateau cb25;
    try {
        cb25 = CBateau("bateau25", pos , 6);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb26;
    try {
        cb26 = CBateau("bateau26", pos , 3);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    
    CBateau cb27;
    try {
        cb27 = CBateau("bateau27", pos , 6);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    CBateau cb28;
    try {
        cb28 = CBateau("bateau28", pos , 6);
        
    }catch ( CErreurCreation& c ) {
        //cout <<"Pb : " << c.getText() <<endl;
    }
    //cout <<"Cas normal"<<endl;
    CArmada amardaOk;
    amardaOk.ajouterBateau(cb1);
    amardaOk.ajouterBateau(cb21);
    amardaOk.ajouterBateau(cb13);
    amardaOk.ajouterBateau(cb15);
    amardaOk.ajouterBateau(cb11);
    amardaOk.ajouterBateau(cb6);

    bool ret =false;
    int test =0;
    for (int i = 0; i<100 ; i++){
        try{
            
            ret = false;
            test ++;
            ret = amardaOk.placerAleatoirement();
            if (ret){
                reussi ++;
            }

        }catch( CErreurCreation& c ){
        }

    }
    
    //cout << "Cas d'erreur  " <<endl;
    CArmada amardaErreur;
    amardaErreur.ajouterBateau(cb1);
    amardaErreur.ajouterBateau(cb2);
    amardaErreur.ajouterBateau(cb3);
    amardaErreur.ajouterBateau(cb4);
    amardaErreur.ajouterBateau(cb5);
    amardaErreur.ajouterBateau(cb6);
    amardaErreur.ajouterBateau(cb7);
    amardaErreur.ajouterBateau(cb8);
    amardaErreur.ajouterBateau(cb9);
    amardaErreur.ajouterBateau(cb10);
    amardaErreur.ajouterBateau(cb11);
    amardaErreur.ajouterBateau(cb12);
    amardaErreur.ajouterBateau(cb13);
    amardaErreur.ajouterBateau(cb14);
    amardaErreur.ajouterBateau(cb15);
    amardaErreur.ajouterBateau(cb16);
    amardaErreur.ajouterBateau(cb17);
    amardaErreur.ajouterBateau(cb18);
    amardaErreur.ajouterBateau(cb19);
    amardaErreur.ajouterBateau(cb20);
    amardaErreur.ajouterBateau(cb21);
    amardaErreur.ajouterBateau(cb22);
    amardaErreur.ajouterBateau(cb23);
    amardaErreur.ajouterBateau(cb24);
    amardaErreur.ajouterBateau(cb24);
    amardaErreur.ajouterBateau(cb24);
    amardaErreur.ajouterBateau(cb25);
    amardaErreur.ajouterBateau(cb26);
    amardaErreur.ajouterBateau(cb27);
    amardaErreur.ajouterBateau(cb28);
    for (int i = 0; i<100 ; i++){
        try{
            test ++;
            ret= amardaErreur.placerAleatoirement();
            if (!ret){
                reussi ++;
            }
        }
        catch ( CErreurCreation& c ){
        }
    }

    cout <<"Test reussi : " << reussi << " sur " << test <<endl;
}