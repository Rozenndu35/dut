#ifndef CAMARDA_H
#define CAMARDA_H

#include "BiblioStd.h"



class CAmarda{
    
    private:
        // attributs d’instance 
        string* m_pTabSousMarins;
        int m_nbTotSSM;

     
    public :
        // Constructeur
        CAmarda(int nbSouSMarin);
        // DESTRUCTEUR
        ~CAmarda();
        // Accesseurs
        string* getpTabSousMarins();
        int getnbTotSSM();
        void remplirStruct();
    
        friend ostream& operator<<(ostream& os , CAmarda& theA);

    private:
        bool analyser(string laSaisie);

    

    
};

#endif