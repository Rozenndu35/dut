#ifndef CGui_H
#define CGui_H

#include "BiblioStd.h"
#include "CAmarda.h"



class CGui{
    
    private:
        // attributs d’instance 
        #define NB_COL_LIG = 11 
        string m_grille[NB_COL_LIG];
        CArmada* m_pTheArmada;

     
    public :
        CGUI( CArmada* pTheArmada ) ;
        friend ostream& operator<<( ostream&os, CGUI& theG );

    private:
         void printGrille( ostream& os );
    
};

#endif