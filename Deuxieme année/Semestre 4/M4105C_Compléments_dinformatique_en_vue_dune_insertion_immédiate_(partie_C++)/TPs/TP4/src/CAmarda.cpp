#include "CAmarda.h"

//constructeur
CAmarda::CAmarda(int nbSouSMarin){
    if(nbSouSMarin>=0 && nbSouSMarin<=100){
        this->m_nbTotSSM = nbSouSMarin;
        cout << "construit"<< endl;
    }else{
        cout << "erreur le nombre de sous marin est incorect"<< endl;
        this->m_nbTotSSM = 0;
    }
    
    this->m_pTabSousMarins= new string[m_nbTotSSM];
}

CAmarda::~CAmarda(){
    if (m_pTabSousMarins != NULL){
        delete[] m_pTabSousMarins;
    }
}

string* CAmarda::getpTabSousMarins(){
    return this->m_pTabSousMarins;
}

int CAmarda::getnbTotSSM(){
    return this->m_nbTotSSM;
}

//rentrer les positions des sous-marins un par un sous forme d'une chaîne de caractères LettreMajusculeChiffre
void CAmarda::remplirStruct(){
    string saisie;
    for (int i = 0 ; i < m_nbTotSSM ; i++){
        bool valide=false;
        cout << " saisir le case du sous marrin "<< i+1 << endl;
        while (valide == false){
            getline(cin , saisie);
            valide = analyser(saisie);
            if(valide == false){
                cout << "erreur : saisir une case "<< endl;
            }
        }
        m_pTabSousMarins[i] = saisie;
    }
}
//verifie la chaine de caractere
bool CAmarda::analyser(string laSaisie){
    bool ret = true;
    if(laSaisie.size() != 2){
        ret = false;
    }
    else {
        if (laSaisie.at(0) < 64 || laSaisie.at(0) > 75){
            ret = false;
        }
        if(laSaisie.at(1) < 47 || laSaisie.at(1) > 58) {
            ret = false;
        }
    }

    for (int i = 0 ; i < m_nbTotSSM && ret == true; i++){
        if (laSaisie == m_pTabSousMarins[i]){
            ret = false;
        }
    }
    
    return ret;
}
//afficher à l'écran toutes les positions des sous-marins
ostream& operator<<(ostream& os , CAmarda& theA){
    for (int i = 0 ; i < theA.m_nbTotSSM ; i++){
        os << "sous marin "<< i +1 << " : " << theA.m_pTabSousMarins[i] << endl;
    }
    return os;
}

