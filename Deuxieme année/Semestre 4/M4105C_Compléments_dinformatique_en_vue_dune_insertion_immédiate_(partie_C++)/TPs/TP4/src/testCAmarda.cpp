#include "CAmarda.h"

int main(){ 
    //test des constructeur avec parametre
    cout << "constructeur avec parametre"<< endl;
    cout << "ok : "<< endl;
    CAmarda* amardazero = new CAmarda(0);
    CAmarda *amardasix = new CAmarda(6);
    cout << "erreur : "<< endl;
    CAmarda *amardaErcinq = new CAmarda(-5);
    CAmarda *amardaErdeuxcent = new CAmarda(200);
    
    //test des getter
    cout << "getnbTotSSM "<< endl;
    int reussi = 0;
    if(amardazero->getnbTotSSM() == 0){reussi=reussi+1;}
    if(amardasix->getnbTotSSM()==6){reussi=reussi+1;}
    if(amardaErcinq->getnbTotSSM()==0){reussi=reussi+1;}
    if(amardaErdeuxcent->getnbTotSSM()==0){reussi=reussi+1;}
    cout << "test reussi " << reussi << " sur 4"<< endl;

    cout << "getpTabSousMarins "<< endl;
    reussi = 0;
    if(amardazero->getnbTotSSM() == 0){reussi=reussi+1;}
    if(amardasix->getnbTotSSM()==6){reussi=reussi+1;}
    if(amardaErcinq->getnbTotSSM()==0){reussi=reussi+1;}
    if(amardaErdeuxcent->getnbTotSSM()==0){reussi=reussi+1;}
    cout << "test reussi " << reussi << " sur 4"<< endl;

    //test remplirStruct
    cout << "remplirStruct "<< endl;
    cout << "pour 0 "<< endl;
    amardazero->remplirStruct();
    cout << "pour6 "<< endl;
    amardasix->remplirStruct();

    //test de <<
    cout << "test de l'affichage << "<< endl;
    cout << *amardasix ;
}