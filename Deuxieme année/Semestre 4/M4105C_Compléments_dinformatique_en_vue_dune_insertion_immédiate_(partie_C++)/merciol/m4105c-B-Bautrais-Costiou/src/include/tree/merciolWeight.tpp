/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_WEIGHT_TPP
#define _MERCIOL_WEIGHT_TPP


inline DimNodeId
MinWeight::pointIdx (const Point &p) const {
  return point2idx (raster.getSize (), p);
}

inline
MinWeight::MinWeight (const Raster &raster)
  : raster (raster) {
}

inline const Size &
MinWeight::getSize () const {
  return raster.getSize ();
}

inline const Pixel &
MinWeight::getValue (const DimImg &idx) const {
  return raster.getPixels () [idx];
}

inline const Pixel &
MinWeight::getValue (const Point &p) const {
  return getValue (pointIdx (p));
}

inline bool
MinWeight::getDecr () const {
  return false;
}

inline bool
MinWeight::isWeightInf (const Weight &a, const Weight &b) {
  return a < b;
}

inline bool
MinWeight::isEdgeInf (const Edge &a, const Edge &b) {
  return isWeightInf (a.weight, b.weight);
}

inline void
MinWeight::sort (Edge *edges, DimEdge count) {
  std::sort (edges, edges+count, isEdgeInf);
}

inline Weight
MinWeight::getWeight (const DimImg &idx) const {
  return getValue (idx);
}

inline Weight
MinWeight::getWeight (const Point &a, const Point &b) const {
  return std::max (getWeight (pointIdx (a)),
		   getWeight (pointIdx (b)));
}

#endif // _MERCIOL_WEIGHT_TPP
