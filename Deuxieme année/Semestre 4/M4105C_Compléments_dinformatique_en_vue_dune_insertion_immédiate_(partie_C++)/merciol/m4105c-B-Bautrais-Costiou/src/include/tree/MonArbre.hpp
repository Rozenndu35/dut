#ifndef _MON_ARBRE_HPP
#define _MON_ARBRE_HPP

#include "tree/merciolArrayTree.hpp"

using namespace merciol;

class MonArbre : public ArrayTree {
  virtual void buildInit (const GraphWalker &graphWalker);
  virtual void buildParents (const GraphWalker &graphWalker, const MinWeight &weightClass);
  virtual void compressParents ();
  virtual void buildChildren ();
  virtual void buildFinish ();
};

#endif
