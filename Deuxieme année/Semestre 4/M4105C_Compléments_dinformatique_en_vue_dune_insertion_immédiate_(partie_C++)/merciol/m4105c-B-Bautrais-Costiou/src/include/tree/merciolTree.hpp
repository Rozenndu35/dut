/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_TREE_HPP
#define _MERCIOL_TREE_HPP

#include "tree/merciolGraphWalker.hpp"
#include "tree/merciolWeight.hpp"

namespace merciol {

  // ========================================

  class Tree {
  public:
    Tree ();
    virtual ~Tree ();

    virtual DimImg	getLeafCount () const = 0;
    virtual DimImg	getCompCount () const = 0;
    virtual DimImg	getLeafParent (const DimImg &leafId) const = 0;
    virtual DimImg	getCompParent (const DimImg &compId) const = 0;
    virtual DimImg	getChildrenCount (const DimImg &compId) const = 0;
    virtual DimNodeId	getChild (const DimImg &compId, const DimImg &childId) const = 0;
    virtual bool	isLeaf (const DimNodeId &nodeId) const = 0;
    virtual DimImg	getLeafId (const DimNodeId &nodeId) const = 0;
    virtual DimImg	getCompId (const DimNodeId &nodeId) const = 0;
    virtual Weight	getWeight (const DimImg &compId) const = 0;
    virtual void	build (const GraphWalker &graphWalker, const MinWeight &weight) = 0;

    bool		compareTo (const Tree &tree, const bool testChildren = false) const;
  };

  // ========================================
} // namespace merciol
#endif // _MERCIOL_TREE_HPP
