/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_WEIGHT_HPP
#define _MERCIOL_WEIGHT_HPP

#include <iostream>
#include <typeinfo>

#include "misc/merciolGeo.hpp"
#include "misc/merciolRaster.hpp"

namespace merciol {
  /*! Structure définissant les poids entre les pixels */
  struct MinWeight {
  protected:
    const Raster &raster;
    inline DimNodeId	pointIdx (const Point &p) const;

  public:
    inline MinWeight (const Raster &raster);

    inline const Size	&getSize () const;
    inline const Pixel	&getValue (const DimImg &idx) const;
    inline const Pixel	&getValue (const Point &p) const;
    inline bool		getDecr () const;

    static inline bool	isWeightInf (const Weight &a, const Weight &b);
    static inline bool	isEdgeInf (const Edge &a, const Edge &b);
    static inline void	sort (Edge *edges, DimEdge count);

    inline Weight	getWeight (const DimImg &idx) const;
    inline Weight	getWeight (const Point &a, const Point &b) const;
  };

#include "merciolWeight.tpp"
} // namespace merciol

#endif // _MERCIOL_WEIGHT_HPP
