/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_GRAPH_WALKER_HPP
#define _MERCIOL_GRAPH_WALKER_HPP

#include <boost/assert.hpp>
#include <algorithm>    // std::fill_n
#include <numeric>      // std::partial_sum
#include <vector>

#include "misc/merciolDebug.hpp"
#include "misc/merciolGeo.hpp"

namespace merciol {
  // ========================================

  using namespace std;

  class GraphWalker {
  public:
    const Size size;

    GraphWalker (const Size &size);

    inline DimImg vertexMaxCount () const;
    inline DimEdge edgeMaxCount () const;

    template<typename Funct>
    inline DimImg forEachVertexIdx (const Funct &lambda /*lambda (idx)*/) const;
    template<typename Funct>
    DimImg forEachVertexPt (const Funct &lambda /*lambda (p)*/) const;
    template<typename Funct>
    inline DimEdge forEachEdgePt (const Funct &lambda /*lambda (p0, p1)*/) const;

    template<typename EdgeWeightFunction>
    inline DimEdge getEdges (Edge *edges, const EdgeWeightFunction &ef /*ef.getWeight (p0, p1)*/) const;
    template<typename EdgeWeightFunction>
    inline DimEdge getSortedEdges (Edge *edges, const EdgeWeightFunction &ef /*ef.getWeight (p0, p1) ef.sort ()*/) const;
    template<typename EdgeWeightFunction>
    inline DimEdge getCountingSortedEdges (Edge *edges, const EdgeWeightFunction &ef /*ef.getWeight (p0, p1)*/) const;
  };

#include "merciolGraphWalker.tpp"

  // ========================================
} // namespace merciol

#endif // _MERCIOL_GRAPH_WALKER_HPP
