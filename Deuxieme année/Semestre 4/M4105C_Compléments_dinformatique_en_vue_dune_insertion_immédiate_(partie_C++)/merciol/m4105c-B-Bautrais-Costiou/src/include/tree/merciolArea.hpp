/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_AREA_HPP
#define _MERCIOL_AREA_HPP

#include "misc/merciolRaster.hpp"
#include "merciolTree.hpp"

namespace merciol {

  // ========================================

  class AreaAttr {
  private:
    const ArrayTree &arrayTree;
    const Raster &raster;

    DimImg compCount;
    DimImg *areas;

    void book (const DimImg &compCount);
    void reset ();
    void free ();

  public:
    AreaAttr (const ArrayTree &arrayTree, const Raster &raster);
    ~AreaAttr ();

    virtual void	update ();
    virtual void	cut (const DimImg &threshold, Raster &outputRaster);
    virtual void	cut (const DimImg &thresholdA, const DimImg &thresholdB, Raster &outputRaster);
  };

#include "merciolArrayTree.tpp"
  // ========================================
} // namespace merciol

#endif // _MERCIOL_AREA_HPP
