/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_GRAPH_WALKER_TPP
#define _MERCIOL_GRAPH_WALKER_TPP

// ========================================
inline
GraphWalker::GraphWalker (const Size &size)
  : size (size) {
  BOOST_ASSERT (size.width  >= DimImg (0));
  BOOST_ASSERT (size.height >= DimImg (0));
}

// ========================================
inline DimImg
GraphWalker::vertexMaxCount () const {
  return DimImg (size.width)*DimImg (size.height);
}

// ----------------------------------------
inline DimEdge
GraphWalker::edgeMaxCount () const {
  if (!size.width || !size.height)
    return 0;
  return
    DimImg (size.width)*DimImg (size.height-1) +
    DimImg (size.width-1)*DimImg (size.height);
}

// ========================================
template<typename Funct>
inline DimImg
GraphWalker::forEachVertexIdx (const Funct &lambda /*lambda (idx)*/) const {
  DimImg maxCount = vertexMaxCount ();
  DimImg vertexCount = 0;
  for (DimImg idx = 0; idx < maxCount; ++idx)
    lambda (idx), ++vertexCount;
  return vertexCount;
}

// ----------------------------------------
template<typename Funct>
DimImg
GraphWalker::forEachVertexPt (const Funct &lambda /*lambda (p)*/) const {
  return forEachVertexIdx ([this, &lambda] (const Point &p) { lambda (point2idx (size, p)); });
}

// ----------------------------------------
template<typename Funct>
inline DimEdge
GraphWalker::forEachEdgePt (const Funct &lambda /*lambda (p0, p1)*/) const {
  DimImg edgeCount = 0;
  if (!size.width || !size.height)
    return edgeCount;
  const DimSideImg firstX = 0, endX = firstX+ size.width;
  const DimSideImg firstY = 0, endY = firstY+size.height;
  for (DimSideImg x = firstX+1; x < endX; ++x) {
    Point pha1 (x-1, firstY), pha2 (x, firstY);
    lambda (pha1, pha2), ++edgeCount;
  }
  for (DimSideImg y = firstY+1; y < endY; ++y) {
    Point pva1 (firstX, y-1), pva2 (firstX, y);
    lambda (pva1, pva2), ++edgeCount;
    for (DimSideImg x = firstX+1; x < endX; ++x) {
      Point ph1 (x-1, y), pv1 (x, y-1), p2 (x, y);
      lambda (ph1, p2), ++edgeCount;
      lambda (pv1, p2), ++edgeCount;
    }
  }
  return edgeCount;
}

// ========================================
template <typename EdgeWeightFunction>
inline DimEdge
GraphWalker::getEdges (Edge *edges, const EdgeWeightFunction &ef /*ef.getWeight (p0, p1)*/) const {
  BOOST_ASSERT (size.width);
  BOOST_ASSERT (size.height);
  BOOST_ASSERT (edges);
  DimEdge edgeCount = 0;
  forEachEdgePt ([&edges, &edgeCount, &ef] (Point const &a, Point const &b) {
      Edge &edge = edges[edgeCount++];
      edge.points[0] = a;
      edge.points[1] = b;
      edge.weight = ef.getWeight (a, b);
    });
  return edgeCount;
}

// ----------------------------------------
template <typename EdgeWeightFunction>
inline DimEdge
GraphWalker::getSortedEdges (Edge *edges, const EdgeWeightFunction &ef /*ef.getWeight (p0, p1) ef.sort ()*/) const {
  DimEdge edgeCount = getEdges (edges, ef);
  ef.sort (edges, edgeCount);
  return edgeCount;
}

template <typename EdgeWeightFunction>
// template DimImg
inline DimEdge
GraphWalker::getCountingSortedEdges (Edge *edges, const EdgeWeightFunction &ef /*ef.getWeight (p0, p1)*/) const {
  int bits = 8*sizeof (Weight);
  BOOST_ASSERT (bits < 17);
  DimEdge dim = 1UL << bits;
  DimImg *indices = new DimImg [dim+1];
  DimImg *histogram = indices+1;
  DimImg sum = 0;
  ::std::fill_n (histogram, dim, 0);
  forEachEdgePt ([&histogram, &ef] (Point const &a, Point const &b) {
      ++histogram[(size_t) ef.getWeight (a, b)];
    });
  
  // get indices by prefix sum
  std::partial_sum (histogram, histogram+dim, histogram);
  sum = indices[dim];
  if (ef.getDecr ()) {
    for (size_t i = 0; i < dim; ++i)
      histogram[i] = sum - histogram[i];
    indices++;
  } else
    indices[0] = 0;
  // extract edges
  forEachEdgePt ([
		  &ef, &edges, &indices] (Point const &a, Point const &b) {
		   Weight weight = ef.getWeight (a, b);
		   Edge &edge = edges[indices[(size_t) weight]++];
		   edge.points[0] = a;
		   edge.points[1] = b;
		   edge.weight = weight;
		 });
  delete [] (histogram-1);
  return sum;
}


#endif // _MERCIOL_GRAPH_WALKER_TPP
