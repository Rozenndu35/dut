/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_ARRAY_TREE_TPP
#define _MERCIOL_ARRAY_TREE_TPP
// ========================================

inline void
ArrayTree::createParent (DimImg &compCount, const Weight &weight, DimImg &childA, DimImg &childB) {
  childCountRec [compCount] = 2;
  compWeights [compCount] = weight;
  childA = childB = compCount;
  ++compCount;
}

inline void
ArrayTree::addChild (const DimImg &parent, DimImg &child) {
  ++childCountRec [parent];
  child = parent;
}

inline void
ArrayTree::addChildren (const DimImg &parent, const DimImg &sibling) {
  childCountRec [parent] += childCountRec [sibling];
  compParents [sibling] = parent;
}

// ========================================
inline void
ArrayTree::updateNewId (DimImg newCompId[], const DimImg &comp, DimImg &compCount) {
  DEF_LOG ("ArrayTree::updateNewId", "comp: " << comp << " compCount: " << compCount);
  if (newCompId[comp] != DimImg_MAX)
    // top already set
    return;
  const DimImg &top = findCompMultiChild (newCompId, comp);
  if (comp != top) {
    // 0 => merge || no more child
    // 1 => unnecessary node
    DimImg newTopId = newCompId[top];
    if (newTopId == DimImg_MAX) {
      newTopId = newCompId[top] = compCount++;
      if (top == DimImg_MAX)
	// XXX arbres non-connexe ?
	cerr << "coucou top: " << comp << endl;
      else if (compParents[top] == leafCount)
	// XXX arbres non-connexe ?
	cerr << "coucou ptop-top: " << comp << endl;
      else if (compParents[top] == DimImg_MAX)
	// XXX arbres non-connexe ?
	cerr << "coucou ptop-max: " << comp << endl;
    }
    const DimNodeId &newTopChildCountRec = childCountRec[top];
    const DimImg &newTopCompParent = compParents[top];
    const Weight &newTopWeight = compWeights[top]; // only in case of unnecessary comp
    for (DimImg sibling = comp; sibling != top; ) {
      DimImg nextSibling = compParents[sibling];
      newCompId[sibling] = newTopId;
      childCountRec[sibling] = newTopChildCountRec;
      compParents[sibling] = newTopCompParent;
      // only in case of unnecessary comp
      compWeights[sibling] = newTopWeight;
      sibling = nextSibling;
    }
    return;
  }
  newCompId[comp] = compCount++;
}

// ========================================
inline DimImg
ArrayTree::findCompMultiChild (const DimImg newCompId[], DimImg comp) {
  for (;;) {
    if (newCompId [comp] != DimImg_MAX)
      return comp;
    const DimImg &parent = compParents[comp];
    if (parent == DimImg_MAX ||
	parent == comp) // only if previous setRoot
      return comp;
    if (childCountRec[comp] > 1 && compWeights[comp] != compWeights[parent])
      return comp;
    comp = parent;
  }
}

// ========================================
inline void
ArrayTree::compress (DimImg newCompId[], const DimImg &compCount) {
  DEF_LOG ("ArrayTree::compress", "compCount: " << compCount);
  LOG ("compress leaves: " << leafCount);
  for (DimImg leaf = 0; leaf < leafCount; ++leaf) {
    DimImg old = leafParents[leaf];
    if (old != DimImg_MAX)
      leafParents[leaf] = newCompId[old];
  }

  LOG ("compress comp: " << compCount);
  for (DimImg comp = 0; comp < compCount; ++comp) {
    DimImg old = compParents[comp];
    if (old != DimImg_MAX)
      compParents[comp] = newCompId[old];
  }

  LOG ("swap comp: " << compCount);
  for (DimImg comp = 0; comp < compCount; ) {
    DimImg newIdComp = newCompId[comp];
    if (newIdComp == comp || newIdComp == DimImg_MAX || newCompId[newIdComp] == newIdComp) {
      ++comp;
      continue;
    }
    swap (compParents[comp], compParents[newIdComp]);
    swap (compWeights[comp], compWeights[newIdComp]);
    swap (childCountRec[comp], childCountRec[newIdComp]);
    swap (newCompId[comp], newCompId[newIdComp]);
  }
}

// ========================================
template<typename LeafFunc>
inline void
ArrayTree::forEachLeaf (const LeafFunc &leafFunc) const {
  for (DimImg leafId = 0; leafId < leafCount; ++leafId)
    leafFunc (leafId);
}

template<typename CompFunc>
inline void
ArrayTree::forEachComp (const CompFunc &compFunc) const {
  DimImg compCount = getCompCount ();
  for (DimImg compId = 0; compId < compCount; ++compId)
    compFunc (compId);
}

template<typename ChildFunc>
inline void
ArrayTree::forEachChild (const DimImg &compId, const ChildFunc &childFunc) const {
  const DimNodeId minChild = childCount [compId], maxChild = childCount [compId];
  for (DimNodeId childId = minChild; childId < maxChild; ++childId) {
    const bool isLeaf = childId < leafCount;
    childFunc (isLeaf, (DimImg) (isLeaf ? childId : childId-leafCount));
  }
}

// ========================================
inline void
ArrayTree::printTree (const Size &size, const bool &rec) {
  cout << "tree weight parent " << (rec ? "countRec" : "count") << endl;
  Size doubleSize (size.width, 2*size.height);
  printMap (cout, compWeights, size, (DimNodeId) getCompCount ()) << endl << endl;
  printMap (cout, leafParents, doubleSize, nodeCount) << endl << endl;
  printMap (cout, rec ? childCountRec : childCount, size, ((DimNodeId) getCompCount ()) + 1 + (rec ? 2 : 0)) << endl << endl;
  // XXX en plus
  if (!rec)
    printMap (cout, children, doubleSize, nodeCount) << endl << endl;
}

inline void
ArrayTree::printLeaders (const Size &size) {
  cout << "leaders" << endl;
  printMap (cout, leaders.getLeaders (), size) << endl;
}

// ========================================
#endif // _MERCIOL_ARRAY_TREE_TPP
