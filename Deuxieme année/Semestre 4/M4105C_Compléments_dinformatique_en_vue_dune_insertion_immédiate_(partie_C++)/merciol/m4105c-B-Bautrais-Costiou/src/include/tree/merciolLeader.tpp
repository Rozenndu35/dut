/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_LEADER_TPP
#define _MERCIOL_LEADER_TPP

// ========================================
inline DimImg *
Leader::getLeaders () {
  return leaders;
}

// ========================================
inline
Leader::Leader ()
  : size (0),
    leaders (NULL) {
}

inline
Leader::~Leader () {
  free ();
}

// ========================================
inline DimImg
Leader::find (DimImg a) const {
  BOOST_ASSERT (a < size);
  for (;;) {
    // On recherche le leader de a dans le tableau
    DimImg p = leaders[a];
    // Si p est size, p n'a pas de leader donc p était le leader de a
    if (p == DimImg_MAX)
      break;
    a = p;
  }
  BOOST_ASSERT (a < size);
  return a;
}

// ----------------------------------------
inline void
Leader::link (DimImg a, const DimImg &r) {
  BOOST_ASSERT (a < size);
  BOOST_ASSERT (r < size);
  for (; a != r;) {
    // On affecte une variable "temporaire" qui joue le rôle du leader de a
    DimImg p = leaders[a];
    //
    leaders[a] = r;
    if (p == DimImg_MAX)
      return;
    a = p;
  }
}

// ========================================
inline void
Leader::book (DimImg vertexCount) {
#ifdef SMART_LOG
  DEF_LOG ("Leader::book", "vertexCount:" << vertexCount);
#endif
  if (vertexCount == size) {
    reset ();
    return;
  }
  free ();
  if (!vertexCount)
    return;
  size = vertexCount;
  leaders = new DimImg [vertexCount];
  reset ();
}

// ---------------------------------------
inline void
Leader::reset () {
  if (!size)
    return;
  // XXX dealThreadFill_n
  std::fill_n (leaders, size, DimImg_MAX);
}

// ---------------------------------------
inline void
Leader::free () {
#ifdef SMART_LOG
  DEF_LOG ("Leader::free", "");
#endif
  if (leaders)
    delete [] leaders;
  leaders = NULL;
  size = 0;
}

// ========================================
#endif // _MERCIOL_LEADER_TPP
