/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_ARRAY_TREE_HPP
#define _MERCIOL_ARRAY_TREE_HPP

#include "merciolTree.hpp"
#include "merciolLeader.hpp"

namespace merciol {

  // ========================================

  class ArrayTree : public Tree {
  protected:
    DimNodeId	leafCount;
    DimNodeId	nodeCount;
    DimNodeId	nodeCapacity; // = 2*leafCount-1

    DimImg	*leafParents;	// [nodeCapacity]
    DimImg	*compParents;	// [nodeCapacity-leafCount] = leafParents+leafCount
    DimNodeId	*childCount;	// [nodeCapacity-leafCount+2] (two additional elements for easy counting)
    DimNodeId	*childCountRec;	// [nodeCapacity-leafCount] = childCount+2
    DimNodeId	*children;	// [(nodeCapacity-leafCount)*2]

    Weight	*compWeights;	// [leafCount]
    Leader	leaders;

    void book (const DimNodeId &vertexCount);
    void reset ();
    void free ();
    void setRoot (const DimImg &compCount);

  public:
    ArrayTree ();
    ~ArrayTree ();

    DimImg	getLeafCount () const;
    DimImg	getCompCount () const;
    DimImg	getLeafParent (const DimImg &leafId) const;
    DimImg	getCompParent (const DimImg &compId) const;
    DimImg	getChildrenCount (const DimImg &compId) const;
    DimNodeId	getChild (const DimImg &compId, const DimImg &childId) const;
    bool	isLeaf (const DimNodeId &nodeId) const;
    DimImg	getLeafId (const DimNodeId &nodeId) const;
    DimImg	getCompId (const DimNodeId &nodeId) const;
    Weight	getWeight (const DimImg &compId) const;
    void	build (const GraphWalker &graphWalker, const MinWeight &weight);

    virtual void buildInit (const GraphWalker &graphWalker);
    virtual void buildParents (const GraphWalker &graphWalker, const MinWeight &weightClass);
    virtual void compressParents ();
    virtual void buildChildren ();
    virtual void buildFinish ();

    template<typename LeafFunc>
    inline void forEachLeaf (const LeafFunc &leafFunc /* leafFunc (DimImg leafId)*/) const;
    template<typename CompFunc>
    inline void forEachComp (const CompFunc &compFunc /* compFunc (DimImg compId)*/) const;
    template<typename ChildFunc>
    inline void forEachChild (const DimImg &compId, const ChildFunc &childFunc /* childFunc (bool isLeaf, DimImg LeafId/compId) */) const;

    inline void printTree (const Size &size, const bool &rec);
    inline void printLeaders (const Size &size);
  private:
    inline void		createParent (DimImg &compCount, const Weight &weight, DimImg &childA, DimImg &childB);
    inline void		addChild (const DimImg &parent, DimImg &child);
    inline void		addChildren (const DimImg &parent, const DimImg &sibling);
    inline void		updateNewId (DimImg newCompId[], const DimImg &comp, DimImg &compCount);
    inline DimImg	findCompMultiChild (const DimImg newCompId[], DimImg comp);
    inline void		compress (DimImg newCompId[], const DimImg &compCount);
  };

#include "merciolArrayTree.tpp"
  // ========================================
} // namespace merciol

#endif // _MERCIOL_ARRAY_TREE_HPP
