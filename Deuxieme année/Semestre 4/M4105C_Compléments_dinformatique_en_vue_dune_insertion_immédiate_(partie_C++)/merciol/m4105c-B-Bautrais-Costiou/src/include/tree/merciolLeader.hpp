/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_LEADER_HPP
#define _MERCIOL_LEADER_HPP

#include <memory>
#include <boost/assert.hpp>

#include "misc/merciolDebug.hpp"
#include "misc/merciolGeo.hpp"

namespace merciol {

  // ========================================
  class Leader {
  private:

    /*! Taille de l'image (donc des tableaux leaderSetSize et leader) */
    DimImg size;

    /*! Tableau des leaders, chaque case contient une référence vers principal leader connu */
    DimImg *leaders;

  public:
    inline DimImg *getLeaders ();

    Leader ();
    ~Leader ();

    /*! Remet à 0 et redéfinit la taille des tableaux */
    inline void book (DimImg vertexCount);

    /*! Libère la mémoire allouée par les tableaux et met size à 0 */
    inline void free ();

    /*! Remplit leaders de DimImg_MAX */
    inline void reset ();

    /*! Cherche le leaders du pixel a, Si a n'en a pas, cela retourne a */
    inline DimImg	find (DimImg a) const;

    /*! Rédéfinit les leaders : a et tous les leaders de a ont pour leader r */
    inline void	link (DimImg a, const DimImg &r);
  };

#include "merciolLeader.tpp"

  // ========================================
} // namespace merciol

#endif // _MERCIOL_LEADER_HPP
