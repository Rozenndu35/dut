/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_GDAL_TPP
#define _MERCIOL_GDAL_TPP

using namespace std;

inline const string &
GDALImage::getFileName () const {
  return fileName;
}

inline DimImg
GDALImage::getPixelsCount () const {
  return DimImg (size.width)*DimImg (size.height);
}

inline const Size &
GDALImage::getSize () const {
  return size;
}

inline const int &
GDALImage::getW () const {
  return w;
}

inline const int &
GDALImage::getH () const {
  return h;
}

inline bool
GDALImage::isEmpty () const {
  return DimImg (size.width)*DimImg (size.height)*bandCount == 0;
}

inline int
GDALImage::getBandCount () const {
  return bandCount;
}

inline GDALDataType
GDALImage::getDataType () const {
  return dataType;
}

inline GDALDataType
GDALImage::getDataType (const int &band) const {
  // !!! GDAL bands start at 1 (not 0 :-( )
  return gDalInputDataset->GetRasterBand (band+1)->GetRasterDataType ();
}

inline void
GDALImage::readBand (int band, Pixel *pixels) const {
  readBand (band, pixels, NullPoint, size);
}

#endif // _MERCIOL_GDAL_TPP
