/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_DEBUG_HPP
#define _MERCIOL_DEBUG_HPP

#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <boost/date_time/posix_time/posix_time.hpp>

#ifndef ENABLE_LOG

#ifndef DEF_LOG
#define DEF_LOG(name, expr)
#endif
#ifndef LOG
#define LOG(expr) {}
#endif

#ifndef DEBUG
#define DEBUG(expr) {}
#endif

#else

#ifndef DEF_LOG
#define DEF_LOG(name, expr) ::merciol::Log log (name); { if (merciol::debug) cerr << expr << endl << flush; }
#endif

#ifndef LOG
#define LOG(expr) { if (merciol::debug) cerr << log << "| " << expr << endl << flush; }
#endif

#ifndef DEBUG
#define DEBUG(expr) { if (merciol::debug) cerr << expr << endl << flush; }
#endif

#endif

namespace merciol {

  using namespace std;

  extern bool debug;

  // =======================================
  inline string getLocalTimeStr ();

  // ========================================
  class Log {
    static unsigned int indent;
    string functName;
  public:
    Log (const string &functName);
    ~Log ();

    friend inline
    ostream &operator << (ostream &out, const Log &log);
  };

#include "merciolDebug.tpp"
  // ========================================
}//namespace merciol

#endif //_MERCIOL_DEBUG_HPP
