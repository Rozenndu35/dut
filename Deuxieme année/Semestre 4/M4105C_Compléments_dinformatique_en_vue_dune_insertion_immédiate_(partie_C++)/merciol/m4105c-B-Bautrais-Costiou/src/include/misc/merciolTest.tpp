/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_TEST_TPP
#define _MERCIOL_TEST_TPP
    
#include <iostream>
#include <iomanip>

namespace merciol {
  template<typename TestFunction, typename RefFunction>
  inline size_t
  valideFunction (const TestFunction &testFunction, const RefFunction &refFunction, const size_t &nbTests) {
    srand (time (NULL));
    size_t bad = 0;
    for (size_t i = 0; i < nbTests; ++i) {
      uint64_t value = std::rand ();
      //cerr << "value: " << "0x" << uppercase << setfill ('0') << setw (8) << hex << value << " " << dec << refFunction (value) << endl;
      if (testFunction (value) != refFunction (value))
	cerr << "error " << bad++ << ": value: " << value << " result: " << testFunction (value) << endl;;
    }
    cerr << bad << " error" << endl;
    return bad;
  }

  template<typename TestFunction>
  inline void
  perfFunction (const string &label, const TestFunction &testFunction, const size_t &nbMacroTests, const size_t &nbMicroTests) {
    vector<string> timeStatsLabels;
    timeStatsLabels.push_back (label);
    vector<TimeStat> timeStats (timeStatsLabels.size ());
    srand (time (NULL));
    for (size_t i = 0; i < nbMacroTests; ++i) {
      uint64_t value = std::rand ();
      recordTime (timeStats[0], nbMicroTests, testFunction, value);
    }
    showTime (cerr, timeStats, timeStatsLabels);
  }

  template<typename TestFunction>
  inline void
  perfNoArgFunction (const string &label, const TestFunction &testFunction, const size_t &nbMacroTests) {
    vector<string> timeStatsLabels;
    timeStatsLabels.push_back (label);
    vector<TimeStat> timeStats (timeStatsLabels.size ());
    for (size_t i = 0; i < nbMacroTests; ++i)
      recordTime (timeStats[0], testFunction);
    showTime (cerr, timeStats, timeStatsLabels);
  }
} // namespace merciol

#endif // _MERCIOL_TEST_TPP
