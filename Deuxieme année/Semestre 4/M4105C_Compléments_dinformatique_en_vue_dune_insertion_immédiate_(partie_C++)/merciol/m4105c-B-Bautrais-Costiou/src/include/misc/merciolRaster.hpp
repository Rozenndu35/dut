/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_RASTER_HPP
#define _MERCIOL_RASTER_HPP

#include <numeric>
#include <boost/assert.hpp>

#include "merciolGeo.hpp"

namespace merciol {

  // ========================================
  class Raster {
  private:
    Size size;
    Pixel *pixels;
  public:
    const Size	&getSize () const;
    const Pixel	*getPixels () const;
    Pixel	*getPixels ();
    DimNodeId	pointIdx (const Point &p) const;
    Pixel	getValue (const DimImg &idx) const;
    Pixel	getValue (const Point &point) const;

    Raster (const Size &size);
    ~Raster ();
  };

#include "merciolRaster.tpp"
  // ========================================
} // namespace merciol

#endif // _MERCIOL_RASTER_HPP
