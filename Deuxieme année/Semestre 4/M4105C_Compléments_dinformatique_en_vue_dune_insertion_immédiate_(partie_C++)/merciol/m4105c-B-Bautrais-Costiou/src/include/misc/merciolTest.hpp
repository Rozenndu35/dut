/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_TEST_HPP
#define _MERCIOL_TEST_HPP

namespace merciol {
  /*!
   * Test pour nbTests valeurs aléatoire la correspondance des résultats entre deux fonctions et retourne le nombre de différences
   *   testFunction est la fonction à évaluer
   *   refFunction est la fonction étalon
   *   nbTests est le nombre de tirage au sort à effectuer pour les tests
   * L'appel de cette fonction a pour effet de bord d'afficher sur la sortie d'erreur les valeurs dont le résultat différes.
   */
  template<typename TestFunction, typename RefFunction>
  inline size_t
  valideFunction (const TestFunction &testFunction, const RefFunction &refFunction, const size_t &nbTests = 1);

  /*!
   * Test le temps d'éxecution moyen d'un fonction.
   *   label est le nom de la fonction à tester.
   *   testFunction est la fonction à évaluer
   *   nbMacroTests est le nombre de messure de temps à lancer
   *   nbMicroTests est le nombre d'appel à la fonction pour une mesure de temps
   * Le temps moyen afficher est à diviser par le nbMicroTests pour connaître la valeur réelle.
   */
  template<typename TestFunction>
  inline void
  perfFunction (const string &label, const TestFunction &testFunction, const size_t &nbMacroTests = 1, const size_t &nbMicroTests = 1);

  template<typename TestFunction>
  inline void
  perfNoArgFunction (const string &label, const TestFunction &testFunction, const size_t &nbMacroTests = 1);

} // namespace merciol

#include "merciolTest.tpp"

#endif // _MERCIOL_TEST_HPP
