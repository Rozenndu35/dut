/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_GDAL_HPP
#define _MERCIOL_GDAL_HPP

#include <string>
#include <gdal/gdal_priv.h>

#include "merciolDebug.hpp"
#include "merciolGeo.hpp"

namespace merciol {
  using namespace std;

  // ========================================
  /*!
   * Classe utile pour l'interfaçage avec la librairie GDAL pour l'écriture et la lecture d'images
   */

  class GDALImage {
  private:
    /*! Compteur du nombre d'images chargées, utile pour savoir quand charger et décharger la bibliothèque */
    static size_t gdalCount;

    /*! Nom du fichier */
    string fileName;

    /*! Dimensions de l'image */
    Size size;
    int w, h;

    /*! Nombre de bande que l'image comporte */
    int bandCount;

    /*! Type des données lues, format définit par GDAL */
    GDALDataType dataType;

    /*! Utilitaire pour lire une image */
    GDALDataset *gDalInputDataset;

    /*! Utilitaire pour créer et écrire dans une image */
    GDALDataset *gDalOutputDataset;

  public:
    /*! Retourne le nom du fichier */
    const string &getFileName () const;

    /*! Retourne le nombre de pixels de l'image */
    DimImg getPixelsCount () const;

    /*! Retourne la taille de l'image */
    const Size &getSize () const;
    const int &getW () const;
    const int &getH () const;

    /*! Vérifie si l'image est vide */
    bool isEmpty () const;

    /*! Retourne le nombre de bandes */
    int getBandCount () const;

    /*! Retourne le type de données d'après un format GDAL */
    GDALDataType getDataType () const;

    /*! Retourne le type de données d'une bande d'après un format GDAL */
    GDALDataType getDataType (const int &band) const;

    /*! Constructeur, on lui passe le nom du fichier */
    GDALImage (string fileName = "");
    ~GDALImage ();

    void setFileName (string fileName);

    /*! Ferme les fichiers */
    void close ();

    /*! Ouvre le fichier et lit chacune des bandes de l'image */
    void readImage ();

    /*! Lit une bande d'id band et la sotcke dans le tableau pixels */
    void readBand (int band, Pixel *pixels) const;

    /*! Crée le fichier image */
    void createImage (const Size &size, GDALDataType dataType, int nbOutputBands);


    /*! Lit une bande de l'image */
    void readBand (int band, Pixel *pixels, const Point &cropOrig, const Size &cropSize) const;

    /*! Ecrit une bande de l'image */
    void writeBand (int band, Pixel *pixels);
  };

#include "merciolGDAL.tpp"
  // ========================================
} // namespace merciol

#endif // _MERCIOL_GDAL_HPP
