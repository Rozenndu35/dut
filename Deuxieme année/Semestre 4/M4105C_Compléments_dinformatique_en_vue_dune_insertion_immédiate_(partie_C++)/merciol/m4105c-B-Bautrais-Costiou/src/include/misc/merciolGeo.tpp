/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_GEO_TPP
#define _MERCIOL_GEO_TPP

inline
Point::Point ()
  : x (0),
    y (0) {
}

inline
Point::Point (const DimSideImg &abs, const DimSideImg &ord)
  : x (abs),
    y (ord) {
    }

inline
std::ostream &
operator << (std::ostream &out, const Point &p) {
  return out << "(" << p.x << "," << p.y << ")";
}

inline
Size::Size ()
  : width (0),
    height (0) {
}

inline
Size::Size (const DimSideImg &w, const DimSideImg &h)
  : width (w),
    height (h) {
    }

inline
std::ostream &operator << (std::ostream &out, const Size &s) {
  return out << "[" << s.width << "," << s.height << "]";
}

inline
DimImg pointToId (const Size &size, const Point &p) {
  return (DimImg)p.x + (DimImg)p.y * size.width;
}

inline
Point idToPoint (const Size &size, const DimImg &id) {
  return Point (id % size.width, id / size.width);
}

inline
Rect::Rect ()
  : x (0),
    y (0),
    width (0),
    height (0) {
}

inline
Rect::Rect (const Rect &rect)
  : x (rect.x),
    y (rect.y),
    width (rect.width),
    height (rect.height) {
}

inline
Rect::Rect (const Point &orig, const Size &size)
  : x (orig.x),
    y (orig.y),
    width (size.width),
    height (size.height) {
}

inline
Rect::Rect (const DimSideImg &abs, const DimSideImg &ord, const DimSideImg &w, const DimSideImg &h)
  : x (abs),
    y (ord),
    width (w),
    height (h) {
    }

inline
std::ostream &
operator << (std::ostream &out, const Rect &r) {
  return out << "[" << r.x << "," << r.y << " " << r.width << "x" << r.height << "]";
}

inline
DimImg
point2idx (const Size &size, const Point &p) {
  return DimImg (p.x) +  DimImg (p.y)*DimImg (size.width);
}

inline
Point
idx2point (const Size &size, const DimImg &idx) {
  return Point (idx % size.width, idx / size.width);
}

template <typename DimImg>
inline std::ostream &
printMap (std::ostream &out, const DimImg *map, const Size &size, DimNodeId maxValues) {
  if (size.width > prinMapMaxSide || size.height > prinMapMaxSide) {
    return out << "map too big to print!" << std::endl;
  }
  if (maxValues == 0)
    maxValues = ((DimNodeId) size.width)*size.height;
  for (DimSideImg y = 0; y < size.height; ++y) {
    for (DimSideImg x = 0; x < size.width; ++x, ++map, --maxValues) {
      if (!maxValues)
	return out;
      if (*map == DimImg_MAX)
	out << "   M";
      else
	out << " " << std::setw (3) << *map;
    }
    out << std::endl;
  }
  return out;
}

inline void
swapEdge (Edge &a, Edge &b) {
  Edge c = a;
  a = b;
  b = c;
}

inline
std::ostream &
operator << (std::ostream &out, const Edge &edge) {
  return out << edge.points[0] << ":" << edge.points[1] << ":" << edge.weight;
}

inline std::ostream &
printEdge (std::ostream &out, const Edge &edge, const Size &size) {
  return out << edge.points[0] << ":" << edge.points[1] << ":" << edge.weight << ":"
	     << point2idx (size, edge.points[0]) << ":" << point2idx (size, edge.points[1]);
}

template <typename Edge>
inline std::ostream &
printEdges (std::ostream &out, const std::vector<Edge> &edges) {
  for (int i = 0; i < edges.size (); ++i)
    out << " " << edges [i];
  return out;
}

template <typename Edge>
inline std::ostream &
printEdges (std::ostream &out, const Edge edges[], const Size &size, const DimEdge edgesCount) {
  for (int i = 0; i < edgesCount; ++i)
    printEdge (out, edges[i], size) << std::endl;
  return out;
}

template <typename Edge>
inline std::ostream &
printEdges (std::ostream &out, const std::vector<Edge> &edges, const Size &size) {
  for (int i = 0; i < edges.size (); ++i)
    out << " [" << point2idx (size, edges [i].points[0])
	<< " " << point2idx (size, edges [i].points[1])
	<< " " << edges [i].weight << "]";
  return out;
}

#endif // _MERCIOL_GEO_TPP
