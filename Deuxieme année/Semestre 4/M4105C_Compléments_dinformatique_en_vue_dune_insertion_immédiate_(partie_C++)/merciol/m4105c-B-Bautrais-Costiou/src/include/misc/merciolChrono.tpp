/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_CHRONO_TPP
#define _MERCIOL_CHRONO_TPP

#include <iostream>
#include <iomanip>
  
namespace merciol {
  using namespace std;

  template<typename Function, typename Argument>
  inline void
  recordTime (TimeStat &timeStat, const size_t &nbMicroTests, const Function &function, const Argument &arg) {
    using namespace boost::chrono;
    high_resolution_clock::time_point start = high_resolution_clock::now ();
    for (int i = 0; i < nbMicroTests; ++i)
      function (arg);
    timeStat (duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
  }

  template<typename Function>
  inline void
  recordTime (TimeStat &timeStat, const Function &function) {
    using namespace boost::chrono;
    high_resolution_clock::time_point start = high_resolution_clock::now ();
    function ();
    timeStat (duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
  }

  inline ostream &
  showTime (ostream &out, const vector<TimeStat> &timeStats, const vector<string> &timeStatsLabels) {
    cout << endl
	 << setw (11) << left << "Time"  << "\t"
	 << setw (15) << left << "Sum"   << "\t"
	 << setw (3)  << left << "Count" << "\t"
	 << setw (15) << left << "Mean"  << "\t"
	 << setw (15) << left << "Min"  << "\t"
	 << setw (15) << left << "Max"  << endl;
    for (size_t i = 0; i < timeStats.size (); ++i) {
      if (!ba::count (timeStats[i]))
	continue;
      out << setw (11) << right << timeStatsLabels[i] << "\t"
	  << ns2string (ba::sum (timeStats[i]))  << "\t"
	  << setw (3) << ba::count (timeStats[i]) << "\t"
	  << ns2string (ba::mean (timeStats[i])) << "\t"
	  << ns2string (ba::min (timeStats[i]))  << "\t"
	  << ns2string (ba::max (timeStats[i]))
	  << endl << flush;
    }
    return out;
  }

  inline string
  ns2string (const double &delta) {
    using namespace boost::chrono;
    using namespace std;
    ostringstream oss;
    duration<double> ns (delta);
    oss.fill ('0');
    // typedef duration<int, ratio<86400> > days;
    // days d = duration_cast<days>(ns);
    // ns -= d;
    hours h = duration_cast<hours> (ns);
    ns -= h;
    minutes m = duration_cast<minutes> (ns);
    ns -= m;
    oss << setw (2) << h.count () << ":"
	<< setw (2) << m.count () << ":"
	<< setw (9) << fixed << setprecision (6) << ns.count ();
    return oss.str ();
  }
} // namespace merciol

#endif // _MERCIOL_CHRONO_TPP
