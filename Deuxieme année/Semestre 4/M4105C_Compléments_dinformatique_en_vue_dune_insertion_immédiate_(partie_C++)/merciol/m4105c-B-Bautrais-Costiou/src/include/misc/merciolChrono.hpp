/* http://m4105.parlenet.org (c) 2018 */
/* (c) http://m4105.parlenet.org/ */
#ifndef _MERCIOL_CHRONO_HPP
#define _MERCIOL_CHRONO_HPP

#include <vector>
#include <boost/chrono.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/sum.hpp>
#include <boost/accumulators/statistics/count.hpp>
#include <boost/accumulators/statistics/min.hpp>
#include <boost/accumulators/statistics/max.hpp>
#include <boost/accumulators/statistics/mean.hpp>

namespace ba = boost::accumulators;

namespace merciol {
  using namespace std;

  /*!
   * Fabrication d'un nouveau type incluant les éléments statistique que l'on souhaite mesurer pour une accumulation de valeurs réelles.
   * c'est valeurs correspondent à des temps d'exécution en secondes.
   */
  typedef ba::accumulator_set<double, ba::stats<ba::tag::sum,
						ba::tag::count,
						ba::tag::mean,
						ba::tag::min,
						ba::tag::max> > TimeStat;


  /*!
   * Enregistrement du temps d'exécution d'un appel de fonction.
   *   timeStat est l'accumulateur des valeurs mesurée
   *   function est la fonction à chronometrer
   *   arg est l'argument à fournir à la fonction
   *   nbMicroTests est le nombre de test à réaliser pour une seul mesure.
   *     du fait que les temps sont très court, il faut parfoit répéter l'opération mille fois pour quelle soit supérieur à une milliseconde.
   */
  template<typename Function, typename Argument>
  inline void
  recordTime (TimeStat &timeStat, const size_t &nbMicroTests, const Function &function, const Argument &arg);

  /*!
   * Enregistrement du temps d'exécution d'un appel de fonction.
   *   timeStat est l'accumulateur des valeurs mesurée
   *   function est la fonction à chronometrer
   */
  template<typename Function>
  inline void
  recordTime (TimeStat &timeStat, const Function &function);

  /*!
   * affiche le résultat d'un ensemble d'accumulateurs.
   *   out est le canal de sortie
   *   timeStats est un ensemble d'accumulateur
   *   timeStatsLabels est la liste des noms correspondant aux accumulateurs précédants
   */
  inline ostream &
  showTime (ostream &out, const vector<TimeStat> &timeStats, const vector<string> &timeStatsLabels);

  /*! concertie des nanosecondes en une chaîne de caractère affichable. */
  inline string
  ns2string (const double &delta);
} // namespace merciol

#include "merciolChrono.tpp"

#endif // _MERCIOL_CHRONO_HPP
