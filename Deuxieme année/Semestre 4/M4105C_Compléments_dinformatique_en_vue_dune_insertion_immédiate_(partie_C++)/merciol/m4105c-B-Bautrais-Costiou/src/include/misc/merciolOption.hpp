/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_OPTION_HPP
#define _MERCIOL_OPTION_HPP

#include <iostream>
#include <vector>

#include "misc/merciolGeo.hpp"
#include "misc/merciolGDAL.hpp"
#include "misc/merciolOption.hpp"

namespace merciol {

  // ========================================

  class Option {
  public:
    GDALImage inputImage;
    GDALImage outputImage;
    Point topLeft;
    Size size;
    DimChanel chanel = 0;
    vector<long> params;

    Option ();
    Option (int argc, char** argv);

    void usage (string msg = "");
    void parse (int argc, char** argv);
  };

}//namespace merciol

#endif //_MERCIOL_OPTION_HPP
