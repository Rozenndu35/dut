/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_RASTER_TPP
#define _MERCIOL_RASTER_TPP

inline const Size &
Raster::getSize () const {
  return size;
}

inline const Pixel *
Raster::getPixels () const {
  return pixels;
}

inline Pixel *
Raster::getPixels () {
  return pixels;
}

inline DimNodeId
Raster::pointIdx (const Point &p) const {
  return point2idx (size, p);
}

inline Pixel
Raster::getValue (const DimImg &idx) const {
  return pixels[idx];
}

inline Pixel
Raster::getValue (const Point &point) const {
  return pixels [pointIdx (point)];
}


inline Raster::Raster (const Size &size)
  : size (size),
    pixels (new Pixel [DimImg (size.width)*DimImg (size.height)]) {
}

inline Raster::~Raster () {
  if (pixels) delete [] pixels; pixels = NULL;
}

#endif // _MERCIOL_RASTER_TPP
