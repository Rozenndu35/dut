/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_GEO_HPP
#define _MERCIOL_GEO_HPP

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <functional>
#include <cstdint>
#include <vector>

#define DimImg_MAX UINT32_MAX

namespace merciol {

  /*! Image band type */
  typedef uint16_t DimChanel; // hyperspectral > 256

  /*! Image band depth */
  typedef uint8_t Pixel; // 8 bits depth
  
  /*! Image size type */
  typedef uint16_t DimSideImg;

  /*! Number of pixels */
  typedef uint32_t DimImg;		// side*side

  /*! Node weight */
  typedef Pixel Weight;
  
  /*! Number of nodes */
  typedef uint32_t DimNodeId;		// <2*nbPixels-1

  /*! Number of neighbour */
  typedef uint32_t DimEdge;		// ~2*nbPixels (4-connectivity)

  struct Point {
    DimSideImg x, y;
    inline Point ();
    inline Point (const DimSideImg &abs, const DimSideImg &ord);
  };
  inline std::ostream &operator << (std::ostream &out, const Point &p);
  extern Point NullPoint;

  struct Size {
    DimSideImg width, height;
    inline Size ();
    inline Size (const DimSideImg &w, const DimSideImg &h);
  };
  inline std::ostream &operator << (std::ostream &out, const Size &s);
  inline DimImg pointToId (const Size &size, const Point &p);
  inline Point idToPoint (const Size &size, const DimImg &id);
  extern Size NullSize;

  struct Rect {
    DimSideImg x, y, width, height;
    inline Rect ();
    inline Rect (const Rect &rect);
    inline Rect (const Point &orig, const Size &size);
    inline Rect (const DimSideImg &abs, const DimSideImg &ord, const DimSideImg &w, const DimSideImg &h);
  };
  inline std::ostream &operator << (std::ostream &out, const Rect &r);
  extern Rect NullRect;

  /*! Convertit un point d'un tableau (on peut imaginer une image à 2 dimension) en un index */
  inline DimImg point2idx (const Size &size, const Point &p);

  /*! Convertit un index d'un tableau (on peut imaginer une image à 2 dimension) en point correspondant à des coordonnées */
  inline Point idx2point (const Size &size, const DimImg &idx);

  static const DimSideImg prinMapMaxSide = 20;
  /*! Affiche le contenu d'un tableau en indiquant sa taille */
  template <typename DimImg>
  inline std::ostream &printMap (std::ostream &out, const DimImg *map, const Size &size, DimNodeId maxValues = 0);

  /*! */
  struct Edge {
    Point points[2];
    Weight weight;
  };

  /*! Effectue l'échange entre 2 Edge */
  inline void swapEdge (Edge &a, Edge &b);

  /*! Opérateur de flux sur les voisins */
  inline std::ostream &operator << (std::ostream &out, const Edge &edge);
  inline std::ostream &printEdge (std::ostream &out, const Edge &edge, const Size &size);

  /*! Affiche tous les voisins d'un ensemble */
  template <typename Edge>
  inline std::ostream &printEdges (std::ostream &out, const std::vector<Edge> &edges);
  template <typename Edge>
  inline std::ostream &printEdges (std::ostream &out, const Edge edges[], const Size &size, const DimEdge edgesCount);

  /*! Affiche tous les voisins d'un ensembles à l'aide de leurs coordonnées */
  template <typename Edge>
  inline std::ostream &printEdges (std::ostream &out, const std::vector<Edge> &edges, const Size &size);

#include "merciolGeo.tpp"
} // namespace merciol

#endif // _MERCIOL_GEO_HPP
