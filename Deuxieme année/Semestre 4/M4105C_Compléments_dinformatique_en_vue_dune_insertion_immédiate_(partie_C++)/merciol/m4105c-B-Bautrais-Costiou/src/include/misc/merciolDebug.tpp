/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_DEBUG_TPP
#define _MERCIOL_DEBUG_TPP

// =======================================
inline string getLocalTimeStr () {
  using namespace boost::posix_time;
  using namespace std;
  ptime now = second_clock::second_clock::local_time ();
  stringstream ss;
  auto date = now.date ();
  auto time = now.time_of_day ();
  ss << setfill ('0') << "["
     << setw (2) << static_cast<int> (date.month ()) << "/" << setw (2) << date.day ()
     << "] " << setw (2)
     << time.hours () << ":" << setw (2) << time.minutes ();
  return ss.str();
}

// ========================================
inline
Log::Log (const string &functName)
  : functName (functName) {
  ++indent;
  if (merciol::debug)
    cerr << *this << "> ";
}

inline
Log::~Log () {
  if (merciol::debug) cerr << *this << "<" << endl << flush; --indent;
}

inline ostream &operator << (ostream &out, const Log &log) {
  return out << getLocalTimeStr () << setw (3) << setw ((log.indent % 20)*2) << "" << log.functName;
}

// ========================================
#endif //_MERCIOL_DEBUG_TPP
