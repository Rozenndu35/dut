/* http://m4105.parlenet.org (c) 2018 */
#ifndef _MERCIOL_ANSWER_HPP
#define _MERCIOL_ANSWER_HPP

namespace merciol {

  extern size_t bitCount [];

  /*! fonction de référence */
  size_t ones64 (uint64_t x);

  /*! fonction de référence */
  size_t size64 (uint64_t x);

} // namespace merciol
#endif // _MERCIOL_ANSWER_HPP
