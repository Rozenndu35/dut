/* http://m4105.parlenet.org (c) 2018 */
#include <string.h>

#include "misc/merciolOption.hpp"
#include "misc/merciolRaster.hpp"

using namespace merciol;

int
main (int argc, char **argv, char **envp) {
  Option option (argc, argv);
  Raster raster (option.size);
  option.inputImage.readBand (option.chanel, raster.getPixels (), option.topLeft, option.size);
  return 0;
}
