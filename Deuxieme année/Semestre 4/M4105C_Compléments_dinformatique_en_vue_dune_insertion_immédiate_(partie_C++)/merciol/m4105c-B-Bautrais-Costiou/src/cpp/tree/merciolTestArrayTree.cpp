/* http://m4105.parlenet.org (c) 2018 */
#include <string.h>

#include "misc/merciolOption.hpp"
#include "misc/merciolRaster.hpp"
#include "tree/merciolGraphWalker.hpp"
#include "tree/merciolWeight.hpp"
#include "tree/merciolArrayTree.hpp"

using namespace merciol;

int
main (int argc, char **argv, char **envp) {
  Option option (argc, argv);
  DEF_LOG ("main", "");
  Size size = option.size;
  
  Raster raster (size), raster2 (size);
  Pixel *const pixels = raster.getPixels ();
  Pixel *const pixels2 = raster2.getPixels ();

  option.inputImage.readBand (option.chanel, pixels, option.topLeft, size);
  option.inputImage.readBand (option.chanel, pixels2, NullPoint, size);

  ArrayTree arrayTree, arrayTree2;

  arrayTree.build (GraphWalker (size), MinWeight (raster));
  arrayTree.printTree (size, false);

  // arrayTree2.build (GraphWalker (size), MinWeight (raster2));
  // arrayTree2.printTree (size, false);

  // cerr << "res " << arrayTree.compareTo (arrayTree2) << endl;
  return 0;
}
