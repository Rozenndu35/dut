/* http://m4105.parlenet.org (c) 2018 */
#include <string.h>

#include "misc/merciolOption.hpp"
#include "misc/merciolRaster.hpp"
#include "tree/merciolGraphWalker.hpp"
#include "tree/merciolWeight.hpp"
#include "tree/merciolArrayTree.hpp"
#include "tree/merciolArea.hpp"

using namespace merciol;

int
main (int argc, char **argv, char **envp) {
  Option option (argc, argv);
  DEF_LOG ("main", "");
  if (option.outputImage.getFileName ().empty ())
    option.usage ("need output file name");
  if (option.params.size () < 1)
    option.usage ("need 1 or 2 params : thresholds");
  Size size = option.size;
  
  Raster raster (size), raster2 (size);

  option.inputImage.readBand (option.chanel, raster.getPixels (), option.topLeft, size);
  ArrayTree arrayTree;

  arrayTree.build (GraphWalker (size), MinWeight (raster));
  arrayTree.printTree (size, false);

  AreaAttr areaAttr (arrayTree, raster);
  switch (option.params.size ()) {
  case 1:
    areaAttr.cut (option.params [0], raster2);
    break;
  case 2:
    areaAttr.cut (option.params [0], option.params [1], raster2);
    break;
  }
  option.outputImage.createImage (size, option.inputImage.getDataType (), 1);
  option.outputImage.writeBand (0, raster2.getPixels ());

  return 0;
}
