#include "misc/merciolDebug.hpp"
#include "tree/MonArbre.hpp"

using namespace std;
using namespace merciol;

// ========================================
void
MonArbre::buildInit (const GraphWalker &graphWalker) {
  DEF_LOG ("MonArbre::buildInit", "graphWalker:" << graphWalker.size);
  ArrayTree::buildInit (graphWalker);
}

void
MonArbre::buildParents (const GraphWalker &graphWalker, const MinWeight &weightClass) {
  DEF_LOG ("MonArbre::buildParents", "");
  ArrayTree::buildParents (graphWalker, weightClass);
}

void
MonArbre::compressParents () {
  DEF_LOG ("MonArbre::compressParents", "");
  ArrayTree::compressParents ();
}

void
MonArbre::buildChildren () {
  DEF_LOG ("MonArbre::buildChildren", "");

  // childCountRec tableau des compteurs
  // childCount tableau définitif
  DimNodeId *childGetOrder = childCount+1;

  // A: somme cumulée sur childCountRec
  // ? partial_sum

  // B: placement avec childGetOrder

  
  ArrayTree::buildChildren ();
}

void
MonArbre::buildFinish () {
  DEF_LOG ("MonArbre::buildFinish", "");
  ArrayTree::buildFinish ();
}

// ========================================
