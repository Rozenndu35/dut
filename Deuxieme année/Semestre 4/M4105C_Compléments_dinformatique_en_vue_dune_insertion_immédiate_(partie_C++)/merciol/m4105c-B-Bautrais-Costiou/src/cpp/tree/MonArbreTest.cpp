#include <string.h>

#include "misc/merciolOption.hpp"
#include "misc/merciolRaster.hpp"
#include "tree/merciolGraphWalker.hpp"
#include "tree/merciolWeight.hpp"
#include "tree/merciolArrayTree.hpp"

#include "tree/MonArbre.hpp"

using namespace merciol;

int
main (int argc, char **argv, char **envp) {
  Option option (argc, argv);
  DEF_LOG ("main", "");
  Size size = option.size;
  
  Raster raster1 (size);
  option.inputImage.readBand (option.chanel, raster1.getPixels (),
			      option.topLeft, size);
  ArrayTree arrayTree;
  arrayTree.build (GraphWalker (size), MinWeight (raster1));
  arrayTree.printTree (size, false);

  Raster raster2 (size);
  option.inputImage.readBand (option.chanel, raster2.getPixels (),
			      option.topLeft, size);
  MonArbre monArbre;
  monArbre.build (GraphWalker (size), MinWeight (raster2));
  monArbre.printTree (size, false);

  cerr << "res " << arrayTree.compareTo (monArbre) << endl;
  return 0;
}
