/* http://m4105.parlenet.org (c) 2018 */
#include <iostream>
#include <iomanip>
#include <string.h>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/sum.hpp>
#include <boost/accumulators/statistics/count.hpp>
#include <boost/accumulators/statistics/min.hpp>
#include <boost/accumulators/statistics/max.hpp>
#include <boost/accumulators/statistics/mean.hpp>

namespace ba = boost::accumulators;
typedef ba::accumulator_set<long, ba::stats<ba::tag::sum,
					    ba::tag::count,
					    ba::tag::mean,
					    ba::tag::min,
					    ba::tag::max> > PixelStat;

#include "misc/merciolOption.hpp"
#include "misc/merciolRaster.hpp"
#include "tree/merciolGraphWalker.hpp"

using namespace std;
using namespace merciol;

int
main (int argc, char **argv, char **envp) {
  Option option (argc, argv);

  Raster raster (option.size);
  option.inputImage.readBand (option.chanel, raster.getPixels (), option.topLeft, option.size);
  const Pixel *const pixels = raster.getPixels ();

  PixelStat pixelStat;
  GraphWalker graphWalker (option.size);
  graphWalker.forEachVertexIdx ([&pixels, &pixelStat] (const DimImg &leafId) {
      pixelStat (pixels [leafId]);
    });
  static const int colWidth = 10;
  cout << endl
       << setw (colWidth) << left << "Sum"
       << setw (colWidth) << left << "Count"
       << setw (colWidth) << left << "Mean"
       << setw (colWidth) << left << "Min"
       << setw (colWidth) << left << "Max"
       << endl;
  cout << setw (colWidth) << ba::sum (pixelStat)
       << setw (colWidth) << ba::count (pixelStat)
       << setw (colWidth) << ba::mean (pixelStat)
       << setw (colWidth) << ba::min (pixelStat)
       << setw (colWidth) << ba::max (pixelStat)
       << endl << flush;

  return 0;
}
