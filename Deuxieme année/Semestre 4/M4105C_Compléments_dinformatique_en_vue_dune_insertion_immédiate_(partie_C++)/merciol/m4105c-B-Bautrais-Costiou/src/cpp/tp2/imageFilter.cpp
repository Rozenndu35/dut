/* http://m4105.parlenet.org (c) 2018 */
#include <string.h>

#include "misc/merciolOption.hpp"
#include "misc/merciolRaster.hpp"
#include "tree/merciolGraphWalker.hpp"

using namespace merciol;

int
main (int argc, char **argv, char **envp) {
  Option option (argc, argv);
  if (option.outputImage.getFileName ().empty ())
    option.usage ("need output file name");
  if (option.params.size () < 2)
    option.usage ("need 2 params : type threshold");

  Raster raster (option.size);
  option.inputImage.readBand (option.chanel, raster.getPixels (), option.topLeft, option.size);
  
  Raster raster2 (option.size);
  int type = (int) option.params[0];
  int threshold = (int) option.params[1];
  const Pixel *const pixels = raster.getPixels ();
  Pixel *const pixels2 = raster2.getPixels ();

  GraphWalker graphWalker (option.size);
  graphWalker.forEachVertexIdx ([&pixels2, &pixels, &type, &threshold] (const DimImg &leafId) {
      int val = pixels [leafId];
      switch (type) {
      case 0:
	val += threshold;
	break;
      case 1:
	val -= threshold;
	break;
      case 2:
	val = min (threshold, val);
	break;
      case 3:
	val = max (threshold, val);
	break;
      case 4:
	val = 255-val;
	break;
      case 5:
	val = (val-24)*2.6;
	break;
      }
      pixels2 [leafId] = min (255, max (0, val));
    });
  option.outputImage.createImage (option.size, option.inputImage.getDataType (), 1);
  option.outputImage.writeBand (0, pixels2);
  return 0;
}
