/* http://m4105.parlenet.org (c) 2019 */

#include <boost/lexical_cast.hpp>
#include "misc/merciolChrono.hpp"
#include "misc/merciolTest.hpp"

using namespace merciol;

static int tab[64 * 1024 * 1024];
static int steps = 64 * 1024 * 1024;


static int modMask = 1024-1;

/*! fonction à écrire */
void
loop () {
  // XXX
}

int
main (int argc, char **argv, char **envp) {
  for (int i = 10 ; i < 27; ++i) {
    modMask = (1 << i)-1;
    perfNoArgFunction ("2^"+boost::lexical_cast<string> (i)+" "+boost::lexical_cast<string> (modMask), loop, 100);
  }
  return 0;
}
