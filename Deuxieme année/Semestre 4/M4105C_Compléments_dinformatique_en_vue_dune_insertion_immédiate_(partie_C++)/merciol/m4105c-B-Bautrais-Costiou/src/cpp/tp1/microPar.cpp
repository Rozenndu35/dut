/* http://m4105.parlenet.org (c) 2018 */
#include <vector>

#include "misc/merciolChrono.hpp"
#include "misc/merciolTest.hpp"
#include "tp1/merciolAnswer.hpp"

using namespace merciol;

/*! fonction à écrire */
inline size_t
ones64seq (uint64_t x) {
  return 0;
}

/*! fonction à écrire */
inline size_t
size64seq (uint64_t x) {
  return 0;
}

int
main (int argc, char **argv, char **envp) {
  valideFunction (ones64seq, ones64, 1000);
  perfFunction ("test", ones64seq, 1000, 1000);
  // perfFunction ("ref", ones64, 1000, 1000);

  // valideFunction (size64seq, size64, 1000);
  // perfFunction ("test", size64seq, 1000, 1000);
  // perfFunction ("ref", size64, 1000, 1000);
  return 0;
}
