#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  5 15:21:51 2019

@author: rozenn
"""
import numpy as np


class CalculIntensite:
    # variable de classe

    # constructeur
    def __init__(self):
        "le constructeur"

    def minIntensite(self, image_data):
        """Calcule l intensite minimum d une image a partir de sa matrice

        Parameters
        ----------
        image_data : la matrice de l image (donnees de l image)

        Return :
        ----------
        une chaine avec l intensite minimum et le pixel correspondant
        """
        if type(image_data[0]) == np.ndarray:
            minIm = image_data[0][0]
            xMin = yMin = 0
            for x in range(image_data.shape[0]):
                for y in range(image_data.shape[1]):
                    if minIm > image_data[x][y]:
                        minIm = image_data[x][y]
                        xMin = x
                        yMin = y
            statistiqueMin = (
                "min : " + str(minIm) + " pixel : (" + str(xMin) + ";" + str(yMin) + ")"
            )
            return statistiqueMin
        else:
            minIm = image_data[0]
            xMin = 0
            for x in range(image_data.shape[0]):
                if minIm > image_data[x]:
                    minIm = image_data[x]
                    xMin = x
            statistiqueMin = "min : " + str(minIm) + " pixel : (" + str(xMin) + ")"
            return statistiqueMin

    def maxIntensite(self, image_data):
        """Calcule l intensite maximum d une image a partir de sa matrice

        Parameters
        ----------
        image_data : la matrice de l image (donnees de l image)

        Return :
        ----------
        une chaine avec l intensite maximum et le pixel correspondant
        """
        # si deux dimention
        if type(image_data[0]) == np.ndarray:
            maxIm = image_data[0][0]
            xMax = 0
            for x in range(image_data.shape[0]):
                if maxIm < image_data[x]:
                    maxIm = image_data[x]
                    xMax = x
            statistiqueMax = "max : " + str(maxIm) + " pixel : (" + str(xMax) + ")"
            return statistiqueMax
        else:
            maxIm = image_data[0]
            xMax = 0
            for x in range(image_data.shape[0]):
                if maxIm < image_data[x]:
                    maxIm = image_data[x]
                    xMax = x
            statistiqueMax = "max : " + str(maxIm) + " pixel : (" + str(xMax) + ")"
            return statistiqueMax

    def moyIntensite(self, image_data):
        """Calcule l intensite moyenne d une image a partir de sa matrice

        Parameters
        ----------
        image_data : la matrice de l image (donnees de l image)

        Return :
        ----------
        une chaine avec l intensite moyenne
        """
        sommeIm = 0
        # si deux dimention
        if type(image_data[0]) == np.ndarray:
            for x in range(image_data.shape[0]):
                for y in range(image_data.shape[1]):
                    sommeIm += image_data[x][y]
            moyenneIm = sommeIm / (image_data.shape[0] * image_data.shape[1])
            statistiqueMoyenne = "average : " + str(moyenneIm)
            return statistiqueMoyenne

        # si une dimention
        else:
            for x in range(image_data.shape[0]):
                sommeIm += image_data[x]
            moyenneIm = sommeIm / (image_data.shape[0])
            statistiqueMoyenne = "average : " + str(moyenneIm)
            return statistiqueMoyenne

    def toutIntensite(self, image_data):
        """Calcule l intensite minimum , maximum , moyenne  d une image a partir de sa matrice

        Parameters
        ----------
        image_data : la matrice de l image (donnees de l image)

        Return :
        ----------
         une chaine avec l intensite minimum et le pixel correspondant, l intensite maximum et le pixel correspondant, l intensite moyenne
        """
        # si deux dimention
        if type(image_data[0]) == np.ndarray:
            minIm = image_data[0][0]
            maxIm = image_data[0][0]
            xMin = xMax = yMin = yMax = 0
            sommeIm = 0

            for x in range(image_data.shape[0]):
                for y in range(image_data.shape[1]):
                    if minIm > image_data[x][y]:
                        minIm = image_data[x][y]
                        xMin = x
                        yMin = y
                    if maxIm < image_data[x][y]:
                        maxIm = image_data[x][y]
                        xMax = x
                        yMax = y
                    sommeIm += image_data[x][y]
            moyenneIm = sommeIm / (image_data.shape[0] * image_data.shape[1])
            statistiquesImage = (
                "max : "
                + str(maxIm)
                + " pixel : ("
                + str(xMax)
                + ";"
                + str(yMax)
                + ")"
                + "\n"
                + " min : "
                + str(minIm)
                + " pixel : ("
                + str(xMin)
                + ";"
                + str(yMin)
                + ")"
                + "\n"
                + " average : "
                + str(moyenneIm)
            )
            return statistiquesImage
        # si une dimention
        else:
            minIm = image_data[0]
            maxIm = image_data[0]
            xMin = xMax = 0
            sommeIm = 0

            for x in range(image_data.shape[0]):
                if minIm > image_data[x]:
                    minIm = image_data[x]
                    xMin = x
                if maxIm < image_data[x]:
                    maxIm = image_data[x]
                    xMax = x
                sommeIm += image_data[x]
            moyenneIm = sommeIm / (image_data.shape[0])
            statistiquesImage = (
                "max : "
                + str(maxIm)
                + " pixel : ("
                + str(xMax)
                + ")"
                + "\n"
                + " min : "
                + str(minIm)
                + " pixel : ("
                + str(xMin)
                + ")"
                + "\n"
                + " average : "
                + str(moyenneIm)
            )
            return statistiquesImage
