from astropy.io import fits
from astropy import units as u
import numpy as np
import matplotlib

matplotlib.use("TkAgg")

from matplotlib import pyplot as plt
plt.style.use(['dark_background'])
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure


class NavigationToolbar(NavigationToolbar2Tk):
    # Modifier la barre de navigation pour n avoir que les fonctionallites necessaires
    toolitems = [
        t
        for t in NavigationToolbar2Tk.toolitems
        if t[0] in ("Home", "Back", "Forward", "Pan", "Zoom", "Save")
    ]


def spectre(image, fenetre, axis, deuxiemeCharge):
    if axis == "":

        specData = image[0].data  # Recuperation des données du spectre dans un tableau
        specHeader = image[0].header  # Recuperation des headers

        # Si le fichier n a qu une dimension
        if specHeader["NAXIS"] == 1:
            tab = np.arange(0, len(specData))

            f = Figure(figsize=[16, 9], dpi=60)
            a = f.add_subplot(111)
            a.plot(tab, specData)
            a.set_xlabel("A")  # set le titre de l'abcisse
            a.set_ylabel("Jy")  # set le titre de l'ordonnée

            canvas = FigureCanvasTkAgg(f, master=fenetre)

            toolbar = NavigationToolbar(canvas, fenetre)
            toolbar.config(bg="black")
            toolbar._message_label.config(bg="black", fg="white")
            toolbar.update()

            ret = [canvas, a, toolbar, tab, specData]
        # Si le spectre a deux dimensions
        elif specHeader["NAXIS"] == 2:
            tab = np.arange(0, specData.shape[1])
            ff = Figure(figsize=[16, 9], dpi=60)
            aa = ff.add_subplot(111)
            aa.imshow(specData, cmap="gray")

            canva = FigureCanvasTkAgg(ff, master=fenetre)

            toolbar = NavigationToolbar(canva, fenetre)
            toolbar.config(bg="black")
            toolbar._message_label.config(bg="black", fg="white")
            toolbar.update()

            ret = [canva, aa, toolbar, tab]

        else:
            showerror("graphe error", "impossible to display")
        return ret

    else:
        specData = image[0].data
        tab = np.arange(0, len(specData))

        f = Figure(figsize=[16, 9], dpi=60)
        a = axis
        a.plot(tab, specData)
        a.set_xlabel("A")  # set le titre de l'abcisse
        a.set_ylabel("Jy")  # set le titre de l'ordonnée

        canvas = FigureCanvasTkAgg(f, master=fenetre)

        if deuxiemeCharge == 0:
            toolbar = NavigationToolbar(canvas, fenetre)
            toolbar.config(bg="black")
            toolbar._message_label.config(bg="black", fg="white")
            toolbar.update()

        ret = [canvas, a, toolbar, tab, specData]
        return ret


def affichGrille():
    plt.grid(True)


def cacheGrille():
    plt.grid(False)
