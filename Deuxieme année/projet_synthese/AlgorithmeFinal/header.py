from tkinter import *
from tkinter import ttk
from astropy.io import fits
from tkinter.messagebox import * #pour les fenetre de message

class InterfaceHeader:
    headerTest =""
    file_image = ""
    # constructeur
    def __init__(self):
        "le constructeur"

    def popup(self, headerTest, fileImage):
        """affiche dans une deuxieme fenetre le header

        Parameters
        ----------
        self :
        headerTest : string
            le header
        """
        
        
        InterfaceHeader.file_image = fileImage
        fInfos = Toplevel()		  # Popup -> Toplevel()
        fInfos.title('header')
        fInfos.config( bg="black")
        cadre = Frame(fInfos, borderwidth=1,bg="black")
        cadre.pack(fill=BOTH)
        #le label contenant les info
        canevas=Canvas(cadre, bg='black')

        #met le texte
        InterfaceHeader.headerTest = headerTest
        #
        texte=canevas.create_text(0, 0,text=repr(InterfaceHeader.headerTest), anchor=NW, fill = 'white' )

        #creer les scrollbar
        scrollbarY = Scrollbar(cadre, activebackground ="white", bg="black", cursor="double_arrow", highlightbackground = "black", troughcolor ="black")
        scrollbarY.pack(side=RIGHT, fill=Y)
        scrollbarY.config(command=canevas.yview)
        scrollbarX = Scrollbar(cadre, orient='horizontal',  activebackground ="white", bg="black", cursor="double_arrow", highlightbackground = "black", troughcolor ="black")
        scrollbarX.pack(side=BOTTOM, fill=X)
        scrollbarX.config(command=canevas.xview)

        canevas.config(xscrollcommand=scrollbarX.set, yscrollcommand=scrollbarY.set )
        canevas.pack(padx=10,pady=10)

        #les bouton qui sont dans une frame
        boutons = Frame(fInfos,borderwidth=0,relief=GROOVE,bg="black")
        boutons.pack(padx=10,pady=10,fill=X)
        fermer = Button(boutons, text='Fermer',bg="black",fg="white", command=fInfos.destroy)
        fermer.pack(side=RIGHT,padx=10,pady=10)
        modifier = Button(boutons, text='Modifier',bg="black",fg="white", command=InterfaceHeader.modifierErreur)
        modifier.pack(side=LEFT,padx=10,pady=10)

    def modifierErreur():
        """Pour modifier le header

        Parameters
        ----------

        """
        ModifHeader().popup(InterfaceHeader.headerTest , InterfaceHeader.file_image)

def afficheHeaders(image):
    """Pour recuperer le header

        Parameters
        ----------
        image : image

    """
    specHeader = image[0].header #Recuperation des headers
    return specHeader


class ModifHeader:
    # constructeur
    header_info =""
    file_image = ""
    def __init__(self):
        "le constructeur"

    def popup(self, headerTest, fileImage):
        """affiche dans une deuxieme fenetre le header

        Parameters
        ----------
        self :
        header : string
            le header
        """
        
        ModifHeader.file_image = fileImage
        fInfos = Toplevel()		  # Popup -> Toplevel()
        fInfos.title('Modifier header')
        fInfos.config( bg="black")
        cadre = Frame(fInfos, borderwidth=1,bg="black")
        cadre.pack(fill=BOTH)
        #le label contenant les info
        scrollbarY = Scrollbar(cadre, activebackground ="white", bg="black", cursor="double_arrow", highlightbackground = "black", troughcolor ="black")
        scrollbarY.pack(side=RIGHT, fill=Y)
        scrollbarX = Scrollbar(cadre, orient='horizontal',  activebackground ="white", bg="black", cursor="double_arrow", highlightbackground = "black", troughcolor ="black")
        scrollbarX.pack(side=BOTTOM, fill=X)
        ModifHeader.header_info = Text(cadre, bg="black",fg="white", cursor="xterm", insertbackground="white", undo="true", wrap='none', yscrollcommand=scrollbarY.set, xscrollcommand=scrollbarX.set)

        scrollbarX.config(command=ModifHeader.header_info.xview)
        ModifHeader.header_info.insert('1.0', repr(headerTest))
        ModifHeader.header_info.pack(padx=10,pady=10)
        #les bouton qui sont dans une frame
        boutons = Frame(fInfos,borderwidth=1,bg="black")
        boutons.pack(padx=10,pady=10,fill=X)
        annuler = Button(boutons, text='Annuler',bg="black",fg="white", command=fInfos.destroy)
        annuler.pack(side=RIGHT,padx=10,pady=10)
        valider = Button(boutons, text='Valider',bg="black",fg="white", command=ModifHeader.valider)
        valider.pack(side=LEFT,padx=10,pady=10)

    def valider():
        """Pour modifier le header

        Parameters
        ----------

        """
        contents = ModifHeader.header_info.get('1.0','end')#recupere le text ecrit par l'utilisateur
        while( contents.find('\n') != -1):
            posLigne = contents.find('\n') #recupeere ligne par ligne les information
            ligne = contents[0:posLigne]
            contents= contents[posLigne+1:len(contents)]
            nom = ligne[0: (ligne.find("=")-1)]
            contenu = ligne [(ligne.find("=")+1): len(ligne)]
            #permet d'enregistrer mais ne marche pas
            with fits.open(ModifHeader.file_image, 'update') as f:
                for hdu in f:
                    hdu.header[nom]=contenu
        showerror("header erreur", "Cette fonctionalité n'est pas disponible")
