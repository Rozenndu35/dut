import sys

if sys.version_info[0] == 3:
    # for Python3
    from tkinter import *
else:
    # for Python2
    from Tkinter import *
from tkinter.filedialog import *
from tkinter import filedialog  # pour load et export
from tkinter.messagebox import *  # pour les fenetres de message

import matplotlib.pyplot as plt
from matplotlib.widgets import SpanSelector

import numpy as np

from astropy.utils.data import (
    get_pkg_data_filename,
)  # astropy.utils.data pour telecharger le fichier
from astropy.io import fits  # astropy.io.fits pour l ouvrir
import os  # pour verifier l extention du fit et fits

import CalculIntensite as cI
import graphInTkinterLabel as gTL
import header as hd



class Interface:
    # variable de classe
    fenetre = Tk()  # la fenetre
    width = 800
    height = 2600
    fenetre.geometry(str(width)+"x"+str(height)+"+0+0")
    fenetre.tk.call('wm', 'iconphoto', fenetre._w, PhotoImage(file='StellarAI.gif'))
    image_data = []  # les donnees de l image du client
    image = ""  # l image du client
    file_image = "" #le pase de l'image client
    statistic_Info = ""  # les statistiques de l image
    statistic_zone_info = ""  # les statistiques de la zone selectionee
    val_grille = IntVar()
    val_coordonee = IntVar()
    val_header = IntVar()
    graphe = ""
    formatG = ""
    graphe_label=""
    nb_fois = 0
    span = ""
    zoneSelectionnee = ""
    val_sens= IntVar()
    orientation = ""

    # constructeur
    def __init__(self):
        """Le constructeur de la classe, permet de creer l'interface

        Parameters
        ----------
        self :
        """
        # Information de la fenetre en elle meme
        Interface.fenetre.config(bg="black")
        Interface.fenetre.title("Stellar AI")
        Interface.fenetre.rowconfigure(1, weight=1)
        Interface.fenetre.columnconfigure(0, weight=1)

        #met l icone dans la fenetre
        Interface.fenetre.tk.call('wm', 'iconphoto', Interface.fenetre._w, PhotoImage(file='StellarAI.gif'))

        Interface.cadre = Frame(Interface.fenetre, width=1200, height=800, borderwidth=1, bg="black")
        Interface.cadre.grid(row=0, column=0, sticky="ew")

        #creation de la partie basse
        Interface.graphe_cadre = Frame(Interface.fenetre, borderwidth=1, bg="black")
        Interface.graphe_cadre.grid(row=1, column=0, sticky="nsew")

        # creation de la partie gauche
        self.ImageBouton = Frame(Interface.cadre, borderwidth=0, relief=GROOVE, bg="black")
        self.ImageBouton.pack(side=LEFT, padx=10, pady=10 , fill=Y)

        # image et bouton load
        image_ac = PhotoImage(file="./StellarAI.gif")
        canvas_image = Label(self.ImageBouton, image= image_ac )
        canvas_image.pack()
        self.bouton_load = Button(
            self.ImageBouton,
            text="Load",
            bg="black",
            fg="white",
            command=Interface.load,
        )
        self.bouton_load.pack(side=LEFT, padx=10, pady=10)
        self.bouton_sauvgarder = Button(
            self.ImageBouton,
            text="Save informations",
            bg="black",
            fg="white",
            command=Interface.saveInformation,
        )
        self.bouton_sauvgarder.pack(side=LEFT, padx=10, pady=10)
        # creation de la partie droite
        self.Info = LabelFrame(
            Interface.cadre, text="Informations", bg="black", fg="white", labelanchor="n"
        )
        self.Info.pack(
            side=RIGHT, padx=10, pady=10, fill = Y
        )

        # creation d un troisieme widget Frame dans le info
        self.optionAffichage = LabelFrame(
            self.Info, text="Options", bg="black", fg="white"
        )
        self.optionAffichage.pack(
            padx=10, pady=10
        )
        # les options dans cette partie
        self.grille = Checkbutton(
            self.optionAffichage,
            text="Grid",
            bg="black",
            fg="white",
            highlightthickness=0,
            justify=LEFT,
            command=Interface.grid,
            variable=Interface.val_grille,
            selectcolor="black",
            bd=3,
        )
        self.grille.pack(padx=10, pady=10)
        self.coordonnee = Checkbutton(
            self.optionAffichage,
            text="Coordinates",
            bg="black",
            fg="white",
            highlightthickness=0,
            justify=LEFT,
            command=Interface.coordinates,
            variable=Interface.val_coordonee,
            selectcolor="black",
            bd=3,
        )
        self.coordonnee.pack(padx=10, pady=10)
        Interface.orientation = Button(
            self.optionAffichage,
            text="Landscape Layout",
            bg="black",
            fg="white",
            justify=LEFT,
            command=Interface.sens,
        )
        Interface.orientation.pack(padx=10, pady=10)
        Interface.orientation.configure(state=DISABLED)
        self.entete = Button(
            self.optionAffichage,
            text="Fits header",
            bg="black",
            fg="white",
            justify=LEFT,
            command=Interface.header,
        )
        self.entete.pack(padx=10, pady=10)

        # la zone des informations pour l image
        self.statistics = LabelFrame(
            self.Info, text="Image's informations", bg="black", fg="white"
        )
        self.statistics.pack(
            padx=10, pady=10
        )
        # label de la partie
        Interface.statistic_Info = Label(
            self.statistics, text="", bg="black", fg="white"
        )
        Interface.statistic_Info.pack(
            padx=10, pady=10,
        )

        # la zone des informations pour la zone de l image
        self.statistic_zone = LabelFrame(
            self.Info, text="Area's informations", bg="black", fg="white"
        )
        self.statistic_zone.pack(
            padx=10, pady=10,
        )
        # label de la partie
        Interface.statistic_zone_info = Label(
            self.statistic_zone, text="", bg="black", fg="white"
        )
        Interface.statistic_zone_info.pack(
            padx=10, pady=10,
        )

        Interface.val_coordonee.set(1)
        # On demarre la boucle Tkinter qui s interompt quand on ferme la fenetre
        Interface.fenetre.mainloop()

    def load(event=None):
        """La methode pour charger un fichier

        Parameters
        ----------

        """
        # recupere le lien vers le fichier et affiche la fenetre pop up
        file_image = filedialog.askopenfilename(initialdir=os.path.dirname(__file__) , filetypes = [("Fit(s) files", ("*.fit","*.fits")), ("Fits file", "*.fits"), ("Fit file", "*.fit")])
        Interface.file_image = file_image
        # verifier que c est un fit
        if type(file_image) is str:

            if Interface.graphe != "" and file_image != "":
                Interface.graphe[0].get_tk_widget().pack_forget()
                Interface.graphe[2].destroy()

            extention = os.path.splitext(file_image)
            if ".fit" in extention[1] or ".fits" in extention[1]:
                # recupere les donnees de l image et l image en elle meme
                Interface.image_data = fits.getdata(file_image, ext=0)
                Interface.image = fits.open(file_image)
                # recupere les stats de l image
                Interface.statistic_Info["text"] = cI.CalculIntensite().toutIntensite(
                    Interface.image_data
                )

                #selection sur le graphe
                def onselect(xmin, xmax):
                    x = Interface.graphe[3]
                    y = Interface.image_data

                    indmin, indmax = np.searchsorted(x, (xmin, xmax))
                    indmax = min(len(x) - 1, indmax)

                    thisy = y[indmin:indmax]

                    # recupere les donnes de la zone selectionnee
                    Interface.zoneSelectionnee = np.c_[thisy]

                    # les affiche dans le label
                    Interface.statistic_zone_info["text"] = cI.CalculIntensite().toutIntensite(
                        Interface.zoneSelectionnee
                    )

                # recupere le graphe et les axees et l insere
                if Interface.graphe_label == "" :
                    Interface.graphe_label = LabelFrame(
                        Interface.graphe_cadre, text="Graphe", bg="black", fg="white", labelanchor="n"
                    )
                    Interface.graphe_label.pack(fill = X)
                    Interface.graphe = gTL.spectre(Interface.image, Interface.graphe_label, "",Interface.nb_fois)
                    Interface.span = SpanSelector(Interface.graphe[1], onselect, 'horizontal', useblit=True,
                                        rectprops=dict(alpha=0.5, facecolor='blue'))
                    Interface.nb_fois = 1

                else:
                    Interface.graphe_label.pack(fill = X)
                    Interface.graphe = gTL.spectre(Interface.image, Interface.graphe_label, "",Interface.nb_fois)
                    Interface.span = SpanSelector(Interface.graphe[1], onselect, 'horizontal', useblit=True,
                                        rectprops=dict(alpha=0.5, facecolor='blue'))

                Interface.graphe[0].get_tk_widget().pack(expand=True)
                Interface.formatG = Interface.graphe[1].format_coord
                Interface.orientation.configure(state=NORMAL)

                # coche les cordonnees
                Interface.val_coordonee.set(1)
				#met le sens
                Interface.val_sens.set(1)
				# coche la grille
                Interface.val_grille.set(1)
                Interface.graphe[1].grid(True)
                Interface.graphe[0].get_tk_widget().pack()
                Interface.fenetre.update()
                h = Interface.fenetre.winfo_height() - 1
                w = Interface.fenetre.winfo_width() - 1

                newSize = str(w) + "x" + str(h)
                Interface.fenetre.geometry(newSize)

            else:
                showerror("format error", "The selected file isn't a fit file")
        else:
            showerror("format error", "The selected file isn't a fit file")

    def saveInformation():
        if Interface.image == "":
            showerror("saving error", "There's no image to save informations from")
        else:
            filename = asksaveasfilename(initialdir=os.path.dirname(__file__) , initialfile = "information_image", title ="Saves the informations of the image", filetypes = [("Text file", "*.txt"), ("PDF", "*.pdf")], defaultextension='.txt')
            mon_fichier = open(filename, "w")
            mon_fichier.write("___________________________________ \n")
            # inserer les infos des statistiques de l image
            mon_fichier.write("Statistics of the image\n")
            text = cI.CalculIntensite().maxIntensite( Interface.image_data)
            mon_fichier.write(text)
            mon_fichier.write("\n")
            text = cI.CalculIntensite().minIntensite( Interface.image_data)
            mon_fichier.write(text)
            mon_fichier.write("\n")
            text = cI.CalculIntensite().moyIntensite( Interface.image_data)
            mon_fichier.write(text)
            mon_fichier.write("\n ___________________________________ \n")
            # inserer ici pour stocker de nouvelle information
            mon_fichier.close()

    def grid():
        """La methode pour afficher ou supprimer la grille

        Parameters
        ----------

        """
        # si il n y a pas de fichier affiche une erreur
        if Interface.image == "":
            showerror("grid error", "There's no image selected")
            Interface.val_grille.set(0)
        else:
            check = Interface.val_grille.get()
            # affiche la grille
            if check == 1:
                Interface.graphe[1].grid(True)
                Interface.graphe[0].get_tk_widget().pack(expand=True)
                Interface.fenetre.update()

                #permet d actualiser pour que la grille s affiche
                h = Interface.fenetre.winfo_height()
                w = Interface.fenetre.winfo_width() - 3

                newSize = str(w) + "x" + str(h)
                Interface.fenetre.geometry(newSize)
            # supprime la grille
            else:
                Interface.graphe[1].grid(False)
                Interface.canvas_image = (
                    Interface.graphe[0].get_tk_widget().pack(expand=True)
                )
                #permet d actualiser pour que la grille s affiche
                h = Interface.fenetre.winfo_height()
                w = Interface.fenetre.winfo_width() + 3

                newSize = str(w) + "x" + str(h)
                Interface.fenetre.geometry(newSize)
                Interface.fenetre.update()

    def coordinates():
        """La methode pour afficher ou supprimer la grille

        Parameters
        ----------

        """
        # si il n y a pas de fichier affiche une erreur
        if Interface.image == "":
            showerror("coordinates error", "There's no image selected")
            Interface.val_coordonee.set(0)
        else:
            check = Interface.val_coordonee.get()
            # affiche les cordonnee
            if check == 1:
                Interface.graphe[1].format_coord = Interface.formatG
                Interface.fenetre.update()
            # supprime l'affichage des coordonee
            else:
                Interface.axis = Interface.graphe[1]
                Interface.axis = Interface.graphe[1].format_coord(0, 0)
                Interface.graphe[1].format_coord = lambda x, y: ""
                Interface.fenetre.update()

    def header():
        """La methode pour afficher le header

        Parameters
        ----------

        """
        # si il n'y a pas de fichier affiche une erreur
        if Interface.image == "":
            showerror("header error", "There's no image selected")
        # ouvre une fentre avec le header
        else:
            texte = hd.afficheHeaders(Interface.image)
            hd.InterfaceHeader().popup(texte, Interface.file_image)

    def sens():
        if Interface.image != "":
            Interface.fenetre.geometry("1525x600")
            check = Interface.val_sens.get()
            vide =Label(Interface.fenetre, text="", bg="black", fg="white")
            # met le graph a droite
            vide.grid(row=1, column=0, sticky="nsew")
            Interface.fenetre.columnconfigure(0, weight=1)
            Interface.fenetre.columnconfigure(1, weight=1)
            Interface.fenetre.rowconfigure(1, weight=0)
            Interface.graphe_cadre.grid(row=0, column=1, sticky="nsew")
            Interface.graphe_label.config(height=800, width=1200)
            Interface.graphe_label.pack(fill = X)
            #desactive le bouton pour mettre en paysage
            Interface.orientation.configure(state=DISABLED)

interface = Interface()
