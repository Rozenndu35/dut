# TDD-lab

## installation des dépendances
Installation de mocha

`npm install mocha --global`

Installation des dépendances d'éxécution et de développement

`npm ci`

Commande pour lancer les tests mocha

`npm run test`

ou

`npm test`

Commande pour lancer le serveur

`npm start`

Vous pouvez alors récupérer un résultat en appelant

`http://localhost:3000/api/words/count?text=my%20text%20with%20words`