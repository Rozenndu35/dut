package jwallet.ui;

import java.util.Arrays;
import java.util.Collection;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import java.io.File;
import java.io.IOException;
import java.lang.Object.*;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import com.google.gson.stream.JsonWriter; 
import com.google.gson.stream.JsonReader;



/**
 * Application de gestion de mot de passe pour applications Web.
 * @author Nicolas Le Sommer
 * @version 1.0
 */

public class JWalletFrame extends javax.swing.JFrame {

	private javax.swing.JMenuItem addMenuItem;
	private javax.swing.JMenu editMenu;
	private javax.swing.JMenu fileMenu;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JMenuBar menuBar;
	private javax.swing.JMenuItem quitMenuItem;
	private javax.swing.JMenuItem removeMenuItem;
	private javax.swing.JMenuItem updateMenuItem;
	private javax.swing.JTable table;
	private javax.swing.table.DefaultTableModel model;
	private Vector<String> data;




    /**
     * Creates new form MainFrame
     */
    public JWalletFrame() {
        initComponents();
    }

    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        quitMenuItem = new javax.swing.JMenuItem();
        editMenu = new javax.swing.JMenu();
        addMenuItem = new javax.swing.JMenuItem();
        removeMenuItem = new javax.swing.JMenuItem();
        updateMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        this.data = new Vector<>();
        Vector<String> headers = new Vector<>();
        headers.add("URL");
        headers.add("Identifiant");
        headers.add("Mot de passe");
        headers.add("Commentaire");
        this.model = new javax.swing.table.DefaultTableModel(data,headers);

        table.setModel(this.model);

        jScrollPane1.setViewportView(table);

        fileMenu.setText("Fichier");

        quitMenuItem.setText("Quitter");
        quitMenuItem.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					quitMenuItemActionPerformed(evt);
				}
			});
        fileMenu.add(quitMenuItem);

        menuBar.add(fileMenu);

        editMenu.setText("Edition");

        addMenuItem.setText("Ajouter");
        addMenuItem.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					addMenuItemActionPerformed(evt);
				}
			});
        editMenu.add(addMenuItem);

        updateMenuItem.setText("Modifier");
		updateMenuItem.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					updateMenuItemActionPerformed(evt);
				}
			});
        editMenu.add(updateMenuItem);

		removeMenuItem.setText("Supprimer");
		removeMenuItem.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					removeMenuItemActionPerformed(evt);
				}
			});
        editMenu.add(removeMenuItem);

        menuBar.add(editMenu);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
								  layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								  .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
								  );
        layout.setVerticalGroup(
								layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 287, Short.MAX_VALUE)
								);

        pack();
    }

    //--------------------------------------------------------------------------
    // Action listeners
    //--------------------------------------------------------------------------

    private void addMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        model.addRow(new Object[]{null, null, null, null});
		// TODO:  compléter le code de la méthode pour ajouter un élément.
		// persister les données dans le fichier JSON
    }


	private void updateMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO:  compléter le code de la méthode pour mettre à un élément.
		// persister les données dans le fichier JSON

    }

	private void removeMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO : compléter le code de la méthode pour supprimer un élément.
		// persister les données dans le fichier JSON
		int selectedRow = table.getSelectedRow();
		model.removeRow(selectedRow);
    }
	
    private void quitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        this.dispose();
    }


    //--------------------------------------------------------------------------
    // Gestion de la persistence des données dans un fichier JSON
    //--------------------------------------------------------------------------
	private void readJSONFile(File f) throws IOException{
		/** JsonReader jsonReader = Json.createReader(f);
		 JsonObject object = jsonReader.readObject(jsonReader);
		 jsonReader.close();*/
		 
		try {
			JsonReader reader = new JsonReader(f);
			reader.beginObject();
			while (reader.hasNext()) {
			  String name = reader.nextName();
			  if (name.equals("name")) {
					System.out.println(reader.nextString());
			  } else if (name.equals("age")) {
					System.out.println(reader.nextInt());
			  } else if (name.equals("message")) {
			   //read array
					reader.beginArray();
					while (reader.hasNext()) {
						System.out.println(reader.nextString());
					}
					reader.endArray();
			  } else {
					reader.skipValue();//avoid some unhandle events
			  }
			}
			reader.endObject();
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writeJSONFile(File f) throws IOException{
		/**JsonWriter writer = new JsonWriter (f);
		JsonObject obj = new JSonObject(writer);
		writer.writeObject(obj);*/
		  JsonWriter writer;
     try {
		writer = new JsonWriter(f);

		writer.beginObject();//{
		writer.name("name").value("klervia");//"name" : "mkyong"
		writer.name("age").value(19);//"age" : 29

		writer.name("messages");//"messages" :
		writer.beginArray();//[    writer.value("msg 1");//"msg 1"
		writer.value("msg 2");//"msg 2"
		writer.value("msg 3");//"msg 3"
		writer.endArray();//]
		writer.endObject();//}
		writer.close();

		System.out.println("Done");

		 } catch (IOException e) {
		e.printStackTrace();
		 }
	}
	
    //--------------------------------------------------------------------------
    // Méthode main
    //--------------------------------------------------------------------------

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JWalletFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JWalletFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JWalletFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JWalletFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
				public void run() {
					new JWalletFrame().setVisible(true);
				}
			});
    }
}
