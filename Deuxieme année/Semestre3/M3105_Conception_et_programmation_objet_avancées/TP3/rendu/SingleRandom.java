import java.util.Random;

public class SingleRandom{
    private static SingleRandom uniqueInstance = new SingleRandom();
    private Random rand = new Random();

    private SingleRandom(){}

    public static SingleRandom instance(){
        return uniqueInstance;
    }

    public int sRNextInt(){
        return this.rand.nextInt();
    }

}