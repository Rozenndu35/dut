package weather;
import java.util.Observer;

public class CurrentConditionsDisplay implements Observer{
  private float temp = 0;
  private float humidity;
  private float pressure;
  
  private WeatherData weatherData; 
  
  public CurrentConditionsDisplay(WeatherData weatherData) {
    this.weatherData = weatherData;
    weatherData.addObserver(this);
  }
  
  public void weatherChanged() {
    temp =weatherData.getTemperature();
    humidity = weatherData.getHumidity();
    pressure= weatherData.getPressure();
    
    this.display();
  }
  
  public void update(java.util.Observable obs, Object org) {
	temp = weatherData.getTemperature();
	humidity = weatherData.getHumidity();
	pressure = weatherData.getPressure();
	this.display();
  }
  
  
  public void display() {
    System.out.println("Current Conditions = " +
                       " + temperature :" + temp
                         + "/" + "humidity : "+ humidity
                         + "/" + "pressure : "+ pressure);
  }
}
