package weather;
import java.util.Observer;
/*
 * Cette classe fait des statistiques sur les donneees climatiques donnees
 * par un objetde type WeatherData
 */

public class StatisticsDisplay implements Observer {
  private float maxTemp = 0; // temperature maximale de la saison
  private float minTemp = 10; // temperature minimale de la saison
  private float tempSum= 0;
  private int numReadings;
  
  private WeatherData weatherData; 
  
  public StatisticsDisplay(WeatherData weatherData) {
    this.maxTemp = 0;
	this.minTemp = 10;
	this.tempSum= 0;
	this.weatherData = weatherData;
	this.weatherData.addObserver(this);
  }
  
  /**
   * collects the temperature data from the WeatherData
   */
  public void collectData() {
    float temp =weatherData.getTemperature() ;
    tempSum += temp;
    numReadings++;
    if (temp > maxTemp) {
      maxTemp = temp;
    }
    if (temp < minTemp) {
      minTemp = temp;
    }
    this.display();
  }
  
  //update
  public void update(java.util.Observable obs, Object o) {
	float temp = weatherData.getTemperature() ;
	tempSum += temp;
	numReadings++;
	if (temp > maxTemp)
	maxTemp = temp;
	else if (temp < minTemp)
	minTemp = temp;
	this.display();
  }
  
  public void display() {
    System.out.println("Avg/Max/Min temperature = " +
                       " + (tempSum / numReadings)"
                         + "/" + maxTemp + "/" + minTemp);
  }
}
