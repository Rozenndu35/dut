package pass;
public class PasswordSecurityHandler implements StringHandler {
  
  private int length;
  private boolean digit;
  private boolean otherCharacter;
  
  /** default constructor
    * sets length to 0, digit and otherCharacter flags to false
    */
  public PasswordSecurityHandler() {
    length = 0;
    digit = false;
    otherCharacter = false;
  }
  
  /** processLetter method
    * @param c : character to process
    * adds 1 to length
    */
  public void processLetter(char c){
    length++;
  }
  
    /** processDigit method
    * @param c : character to process
    * adds 1 to length, sets digit flag to true
    */
  public void processDigit(char c)  {
    length++;
    digit = true;
  }
  
    /** processOther method
    * @param c : character to process
    * adds 1 to length
    * sets otherCharacter flag to true
    */
  public void processOther(char c){
     length++;
     otherCharacter = true;
  }
  
  public void parse(String s) {
    if (s != null) {
       for (int i = 0;i< s.length(); i++) {
           char c = s.charAt(i);
           if (Character.isDigit(c))
             this.processDigit(c);
           else if (Character.isLetter(c))
             this.processLetter(c);
           else this.processOther(c);
       }
     }
     else System.out.println("String s is null");
  }
  
  /** securityLevel method
    * @return "weak" if password contains fewer than 6 characters
    *   "strong" if password has at least 8 characters, at leat one digit,
    *   and at least one other character that is neither a letter nor a digit
    *  "medium" otherwise
    */
  public String securityLevel() {
    String result = null;
    if (length < 6) result="weak";
    else if (length >= 8 && digit && otherCharacter)
      result = "strong";
    else result = "medium";
    return result;
  }
}
