import java.util.Scanner;
import pass.PasswordSecurityHandler;

public class PasswordSecurityHandlerClient {
  public static void main (String [] args) {
    Scanner scna = new Scanner(System.in);
    System.out.println("A string password has at least 8  \n"
                         + "characters and contains at least one digit\n"
                         +"and one speical character.");
    System.out.println("Enter a password >");
    String password = scna.next();
    
    PasswordSecurityHandler psh = new PasswordSecurityHandler();
    psh.parse(password);
    System.out.println(password + "'s security is " + psh.securityLevel());
  }
}