public abstract class  StateDoor {
    public abstract void close();
    public abstract void open();
}