public class DoorContext {

	StateDoor stateDoor;

	// On suppose que 0 = porte fermée et 1 = porte ouverte lors de l'instanciation
	public DoorContext(){

		this.stateDoor= new DoorClose();
	}
	public void close() {
		switch(getState()){
			case "class DoorClose":
				System.out.println("La Porte est déjà fermée");
				break;
			case "class DoorClosing":
				System.out.println("La Porte est déjà en train de se fermer");
				break;
			case "class DoorOpen":
				this.stateDoor.close();
				this.stateDoor=new DoorClosing();
				break;
			case "class DoorOpenning":
				cut();
		}

	}

	public void open() {
		switch(getState()){
			case "class DoorClose":
				this.stateDoor.open();
				this.stateDoor=new DoorOpenning();
				break;
			case "class DoorClosing":
				cut();
				break;
			case "class DoorOpen":
				System.out.println("La porte est déjà ouverte");
				break;
			case "class DoorOpenning":
				System.out.println("La porte est déjà en train de s'ouvrir");
		}
	}

	public void cut() {
		switch(getState()){
			case "class DoorClosing":
				this.stateDoor.open();
				this.stateDoor= new DoorOpenning();
				System.out.print(" /:\\ La porte est en train de se re-ouvrir (franchissiement du laser)");
				break;
			case "class DoorOpenning":
				this.stateDoor.close();
				this.stateDoor = new DoorClosing();
				System.out.println(" /:\\ La porte est en train de se refermer (appuie du bouttons pendant ouverture)");
		}
	}

		public void finir() {
		switch(getState()){
			case "class DoorClosing":
				this.stateDoor.close();
				this.stateDoor= new DoorClose();
				System.out.println("La porte s'est fermée");
				break;
			case "class DoorOpenning":
				this.stateDoor.open();
				this.stateDoor = new DoorOpen();
				System.out.println("La porte s'est ouverte");
		}
	}

	public String getState() {
		return this.stateDoor.getClass().toString();
	}

}
