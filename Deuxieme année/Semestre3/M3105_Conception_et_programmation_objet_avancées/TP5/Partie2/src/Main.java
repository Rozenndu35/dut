public class Main {
    public static void main(String[] args){
        DoorContext dc = new DoorContext();
        //La porte est déjà fermée de base donc erreur
        dc.close();
        // La porte va s'ouvrir (DoorClose -> DoorOpenning)
        dc.open();
        // On laisse finir le processus (DoorOpenning -> DoorOpen)
        dc.finir();
        // La porte va se fermer (DoorOpen -> DoorClosing)
        dc.close();
        // On interrompt le processus en franchissant la porte (DoorClosing -> DoorOpen)
        dc.cut();

    }
}