import binome.Binome;
import binome.BinomeFactory;

public class WriterFacade {
	
	Binome b ;
		
	public WriterFacade() {
		this.b = BinomeFactory.create(0,0,0);
		this.b.computeRoots();
	}
	public WriterFacade(Binome bin) {
		this.b = bin;
		this.b.computeRoots();
	}
	public WriterFacade(double a, double b, double c) {
		this.b = BinomeFactory.create(0,0,0);
		this.b.computeRoots();
	}
	
	public void println(String text) {
		this.b.displayRoots();
	}
}
