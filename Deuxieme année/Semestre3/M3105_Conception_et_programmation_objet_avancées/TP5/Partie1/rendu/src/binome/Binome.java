package binome;

/**
 *Binome is responsible to create binome suxh as : ax2 + bx + c
 */
public abstract class Binome {

   
    protected double a; // X2 coefficient 
	protected double b; // X coefficient/
    protected double c; // constant of the binome

	protected Discriminant aDisc; // The binome discriminant

    /**
     *  A standard constructor inherited by subclasses
     */
    protected Binome (double cx2, double cx, double cons, Discriminant delta) {
        a = cx2 ; b = cx ; c = cons ; aDisc = delta ;
    }

	


    /**
     *  Abstract operation that computes the roots
     */
    public abstract void computeRoots();

    /**
     *  Abstract operation that displays the roots
     */
    public abstract void displayRoots();

} // end Binome



