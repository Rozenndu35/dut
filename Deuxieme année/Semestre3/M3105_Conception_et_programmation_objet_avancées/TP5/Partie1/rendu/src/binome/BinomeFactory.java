package binome;
/**
 *  Binome object whit one root
 */
public class BinomeFactory {
	
	public static Binome create(double a, double b, double c) {
		Discriminant d;         // a discriminant instance
		double delta;           // the discriminant value

		d = new Discriminant (a, b, c);
		delta = d.value();
		Binome aBin;
		if ( delta == 0.0 ) {
		    aBin = new BinomeWithOne (a, b, c, d);
		} else if ( delta > 0.0 ) {
		    aBin = new BinomeWithTwo (a, b, c, d);
		} else {
		    aBin = new BinomeWithComplex (a, b, c, d);
		}
		return aBin;
	}

}
