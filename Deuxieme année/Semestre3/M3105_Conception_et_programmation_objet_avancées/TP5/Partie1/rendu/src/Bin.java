import binome.Binome;
import binome.BinomeFactory;

/**
 * Testing class for the Binome package
 */
public class Bin {

    /**
     * The test launcher
     */
    public static void main(String args[]) {
     
	
        Binome b = BinomeFactory.create(1,0,1);
        WriterFacade fac = new WriterFacade(b);
        fac.println();
        
        b = BinomeFactory.create(1.0, 0.0, -1.0);
        fac = new WriterFacade(b);
        fac.println();
        
        b = BinomeFactory.create(1.0, 2.0, 1.0);
        fac = new WriterFacade(b);
        fac.println();
    } 

} // end Bin

