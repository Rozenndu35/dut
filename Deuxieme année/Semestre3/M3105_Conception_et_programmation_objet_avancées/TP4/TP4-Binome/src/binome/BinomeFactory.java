/**
 *  Binome object whit one root
 */
class BinomeFactory extends Binome {
	public Binome create(double a, double b, double c) {
		Discriminant d;         // a discriminant instance
		double delta;           // the discriminant value
		Binome aBin;            // the instancied Binome

		d = new Discriminant (a, b, c);
		delta = d.value();
		if ( delta == 0.0 ) {
		    aBin = new BinomeWithOne (a, b, c, d);
		} else if ( delta > 0.0 ) {
		    aBin = new BinomeWithTwo (a, b, c, d);
		} else {
		    aBin = new BinomeComplex (a, b, c, d);
		}
		return aBin;
	    } // end create
}
