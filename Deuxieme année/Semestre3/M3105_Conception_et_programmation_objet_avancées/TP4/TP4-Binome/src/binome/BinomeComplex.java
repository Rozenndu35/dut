package binome;
import util.*;

class BinomeWithComplex extends Binome {

    private Complexe firstRoot = new ComplexeNumber(0.0,0.0);
    private Complexe secondRoot = new ComplexeNumber(0.0,0.0);

    protected BinomeWithComplex ( double cx2,double cx, double cons, Discriminant delta) {
        super (cx2, cx, cons, delta);
    }

    public void computeRoots() {
        this.firstRoot.setPartieReelle(-b/2*a);
        this.firstRoot.setPartieImaginaire( Math.sqrt (aDisc.value()) / (2.0 * a));
        this.secondRoot.setPartieReelle(-b/2*a);
        this.secondRoot.setPartieImaginaire(-(Math.sqrt (aDisc.value()) / (2.0 * a)));
    }

    public void displayRoots() {
        System.out.println("Deux racines distincts : ");
        System.out.println("z1 = "+this.firstRoot.toString());
        System.out.println("z2 = "+this.secondRoot.toString());
    }

}
