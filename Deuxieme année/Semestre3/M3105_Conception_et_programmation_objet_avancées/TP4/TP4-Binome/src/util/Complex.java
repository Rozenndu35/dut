package util;
public class Complexe{
	private double partieReelle;
	private double partieImaginaire;

	public Complexe(double pR, double pI){
		this.partieReelle = pR;
		this.partieImaginaire = pI;
	}

	public double getPartieReelle(){
		return this.partieReelle;
	}

	public double getPartieImaginaire(){
		return this.partieImaginaire;
	}

	public void setPartieImaginaire(double newPI){
		this.partieImaginaire = newPI;
	}

	public void setPartieReelle(double newPR){
		this.partieReelle = newPR;
	}

	public String toString(){
		string ret = "";
		if (this.partieImaginaire>0){
			ret = this.partieReelle+" + i "+this.partieImaginaire;
		}
		else{
			ret = this.partieReelle+" - i "+this.partieImaginaire;
		}
		return ret;
	}
}
