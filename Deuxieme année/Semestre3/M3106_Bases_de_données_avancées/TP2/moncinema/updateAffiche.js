var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("cinemadb");
  var myquery = { titre: "Joker"};
  var newvalues = {$set: { affiche: "https://images-na.ssl-images-amazon.com/images/I/71wbalyU7tL._SY606_.jpg"} };
  dbo.collection("film").updateOne(myquery, newvalues, function(err, res) {
    if (err) throw err;
    console.log("Joker document updated");
    db.close();
  });
});