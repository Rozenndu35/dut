var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("cinemadb");
  var query = { titre: "Là-haut" };
  var newvalues = { $push: { diffusion: new Date("2019-12-20T19:00:00Z") } };
  dbo.collection("film").updateOne(query, newvalues, function(err, res) {
    if (err) throw err;
    console.log("Là-haut horraire ajouter");
    db.close();
  });
});