var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("cinemadb");
  var query = { realisateur: "Gary Ross" };
  dbo.collection("film").find(query).toArray(function(err, result) {
    if (err) throw err;
    result.forEach(film => console.log(film));
    db.close();
  });
}); 