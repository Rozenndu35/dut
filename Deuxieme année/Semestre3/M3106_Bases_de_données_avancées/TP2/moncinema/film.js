var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("cinemadb");
  dbo.createCollection("film", function(err, res) {
    if (err) throw err;
    console.log("film created!");
    db.close();
  });
}); 