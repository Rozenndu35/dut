var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("cinemadb");
    var myobj = [
        { titre: "Hunger Game", realisateur: "Gary Ross", affiche: "http://ekladata.com/De9hteXiA7VteIYkN5HhPmHkht8.jpg", acteurs: ["Jennifer Lawrence","Josh Hutcherson", "Liam Hemsworth","Woody Harrelson","Alizabeth Banks", "Lenny Kravity","Wes Bentley","Donald Sutherland"], diffusion: [new Date("2019-11-29T20:00:00Z"),new Date("2019-11-30T20:00:00Z"),new Date("2019-12-01T19:00:00Z")]},
        { titre: "Là-haut", realisateur: "Albert Dupontel", affiche: "http://fr.web.img3.acsta.net/r_1024_576/medias/nmedia/18/66/97/50/19104240.jpg", acteurs: ["Nahuel Perez Biscyart","Albert Dupontel", "Laurent Lafitte","Niels Arestrup","Emilie Dequenne", "Mélanie Thierry","Héloise Balster","Pjilippe Uchan"], diffusion: [new Date("2019-11-29T19:00:00Z"),new Date("2019-11-30T19:00:00Z"),new Date("2019-12-01T15:00:00Z")] },
        { titre: "Le roi lion", realisateur: " Jon Favreau", affiche: "http://fr.web.img6.acsta.net/medias/nmedia/18/85/97/82/19858447.jpg", acteurs: ["Donald Glover","Beyoucé Knowles Carter", "James Earl Jones","Chiwetel Ejiofor","Billy Eichner", "Sth Rogen","John oliver","Alfre Woodard","John Kani", "Eric André", "Florence Kasumba", "Keegan-Michael Key"], diffusion: [new Date("2019-12-06T19:00:00Z"),new Date("2019-12-07T19:00:00Z"),new Date("2019-12-08T15:00:00Z")] },
        { titre: "Annabelle", realisateur: " Jon R. Leonetti", affiche: "http://fr.web.img5.acsta.net/c_215_290/pictures/14/10/09/10/20/418344.jpg", acteurs: ["Annebelle Walis","Ward Horton", "Alfre Woodard","Eric Ladin","Kerry O'Malley", "Tony Amendola","Brian Howe","Ivar Brodgger"], diffusion: [new Date("2019-12-06T20:00:00Z"),new Date("2019-12-07T20:00:00Z"),new Date("2019-12-08T19:00:00Z")], age: 12 },
        { titre: "Joker", realisateur: "Todd Phillips", affiche: "http://www.cinemalefoyer.fr/media/movies/c4234f.jpg", acteurs: ["Joaquin Phoenix","Robert De Niro", "Zazie Beetz","Frances Conroy","Brett Cullen", "Shea Whigham","Bill Camp","Douglas Hodge"], diffusion: [new Date("2019-12-12T20:30:00Z"),new Date("2019-12-13T20:30:00Z")], age: 12 },
        { titre: "La Reine des Neiges II", realisateur: ["Jennifer Lee", "Chris Buck"], affiche: "https://www.cinemaspathegaumont.com/media/movie/9102736/poster/md/51/film_923629.jpg", acteurs: ["Emmylou Homs","Charlotte Hervieux", "Dany Boon","Donald Reignoux","Baudouin Sama", "Prisca Desmarez","Anne Barbier"], diffusion: [new Date("2019-12-12T20:00:00Z"),new Date("2019-12-13T15:00:00Z")] },
        { titre: "Proxima", realisateur: "Alice Winocour", affiche: "https://www.cinechronicle.com/wp-content/uploads/2019/11/Proxima-affiche.jpg", acteurs: ["Eva Green","Zélie Boulant-Lemesle", "Matt Dillon","Alexia Fateev","Lars Eidinger", "Sandra Hüller","Jan Olivier Schreder","Nancy Tate"], diffusion: [new Date("2019-12-12T20:00:00Z"),new Date("2019-12-13T15:00:00Z")] },
        { titre: "Les Misérable", realisateur: "Ladj Ly", affiche: "http://fr.web.img2.acsta.net/r_1024_576/pictures/19/09/10/11/58/5875477.jpg", acteurs: ["Damien Bonnard","Alexi Manenti", "Djebril Didier Zonga","Issa Perica","Al-Hassan Ly", "Steve Tientcheu","Almany Kanoute","Sofia Lesaffre"], diffusion: [new Date("2019-11-29T20:00:00Z"),new Date("2019-11-30T20:00:00Z"),new Date("2019-12-01T19:00:00Z")] },
        { titre: "La Ch'tite Famille", realisateur: "Dany Boon", affiche: "http://fr.web.img3.acsta.net/r_1024_576/pictures/17/11/27/12/03/3859476.jpg", acteurs: ["Dany Boon","Laurence Arné", "Francois Berléand","Guy Lecluse","Line Rnaud", "Valerie Bonneton","Pierre Richard","Juliane Lepoureau"], diffusion: [new Date("2019-11-29T20:00:00Z"),new Date("2019-11-30T20:00:00Z"),new Date("2019-12-01T19:00:00Z")] },
        { titre: "Abominable", realisateur: ["Jill Culton", "Todd Wilderman"], affiche: "http://www.cinemalefoyer.fr/media/movies/2873fd.jpg", acteurs: ["Chloe Bennet","Albert Tsai", "Tenzing Norgay Trainor","Joseph Izzo","Sarah Paulson", "Eddie Izzard","Tsai Chin","Michelle Wong"], diffusion: [new Date("2019-11-30T18:15:00Z"),new Date("2019-12-01T17:30:00Z")] }
        ];
    dbo.collection("film").insertMany(myobj, function(err, res) {
    if (err) throw err;
    console.log("Number of documents inserted: " + res.insertedCount);
    db.close();
    });
}); 