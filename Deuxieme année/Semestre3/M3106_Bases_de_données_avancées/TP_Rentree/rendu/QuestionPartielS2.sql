/*
Klervia Bautrais
Rozenn Costiou
Questions partiel S2
cf TD
*/

--3)

CREATE OR REPLACE VIEW MedecinForService (ServiceSansMedecin)
AS(
    SELECT unNumSer
    FROM Medecin
    GROUP BY unNumSer
    HAVING COUNT(numMed) = 0
);

--4)

CREATE OR REPLACE TRIGGER trig_nbDossierInf200
AFTER INSERT OR UPDATE ON Dossier
DECLARE
    CURSOR cur_medAvecPb IS
        SELECT unNumMed
        FROM Dossier
        GROUP BY unNumMed
        HAVING COUNT (*) >200;
    v_nbPbs NUMBER :=0;
BEGIN
    OPEN cur_medAvecPb;
    FOR v_medAvecPb IN cur_medAvecPb
        LOOP
            DBMS_OUTPUT.PUT_LINE('Probleme');
            v_nbPbs := v_nbPbs+1;
        END LOOP;
    IF (v_nbPbs>0) THEN
        RAISE_APPLICATION_ERROR(-20000, 'Trop de dossier pour un medecin');
    END IF;
END;
/

--5)

SELECT * FROM MedecinForService ;/

CREATE OR REPLACE TRIGGER DossierPatient
AFTER INSERT OR UPDATE ON Dossier
DECLARE
CURSOR DiffServDof IS
    SELECT nomPat
    FROM Dossier,Medecin
    WHERE numMed = numDos
    GROUP BY nomPat
    HAVING (COUNT(DISTINCT(unNumSer))-COUNT(numDos)) < 0;
    nomPatient VARCHAR2(10);
BEGIN
    OPEN DiffServDof;
    LOOP
    FETCH DiffServDof INTO nomPatient;
    IF(nomPatient != NULL)THEN
        RAISE_APPLICATION_ERROR(-20010,'Plusieurs dossier d un meme patient dans le meme service');
    END IF;
    EXIT WHEN DiffServDof%NOTFOUND;
    END LOOP;
    CLOSE DiffServDof;
END;
/


--6)

    --1
    
SELECT nomPat
FROM Dossier,Medecin
WHERE unNumMed=numMed
AND unNumSer =3;

/*
NOMPAT                        
------------------------------
Lagaffe
Lebrac
Mademoiselle Jeanne
De Mesmaeker
Lagaffe
Asterix
Obelix
Panoramix

8 lignes sélectionnées. 
*/

  --2
  
SELECT nomMed
FROM Medecin, Dossier,ContientMal
WHERE unNumMed=numMed
AND unNumDos = numDos
AND UPPER (unNomMal) = 'APPENDICITE'
ORDER BY nomMed ;

/*
NOMMED              
--------------------
HautLeCoeur
MemePasMal
*/
    
    --3
    
SELECT COUNT (*)
FROM Medecin, Service
WHERE unNumSer= numSer
GROUP BY unNumHop;

/*
 COUNT(*)
----------
         5
         4
         3
*/

    --4
    
SELECT unNumMed
FROM Dossier
GROUP BY unNumMed
HAVING COUNT(numDos) = (
                                SELECT MAX(COUNT(numDos))
                                FROM Dossier
                                GROUP BY unNumMed
                            )
;

/*
 UNNUMMED
----------
         1
         2
        21
         4
         3
*/

    --5
    
SELECT nomPat
FROM Dossier
GROUP BY nomPat
HAVING COUNT(unNumMed) > 1
;
/*
NOMPAT                        
------------------------------
AssuranceTourix
Haddock
Lagaffe
*/

    --6
    
SELECT unNumMed
FROM ContientMal,Dossier
WHERE numDos = unNumDos
GROUP BY unNumMed
HAVING COUNT(DISTINCT(unNomMal)) = (
                                    SELECT COUNT(nomMal)
                                    FROM Maladie
                                )
;

/*
 UNNUMMED
----------
        13
        20
*/

