var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("magasin");
    var myobj = [
        { marque: "adidas", prix: 15 , nom: "Tshirt", reference: 1211, categorie:"junior", listeTailles:["XS", "S"], fournisseur: {nom:"Adidas",ville:"Herzogenaurach"}, identifientRayon:1},
        { marque: "nike", prix: 20 , nom: "Tshirt", reference: 1221, categorie:"junior", listeTailles:["XS","S"], fournisseur:{nom:"Nike",ville:"Beaverton"}, identifientRayon:1},
        { marque: "nike", prix: 40 , nom: "Tshirt", reference: 1121, categorie:"enfant", listeTailles:["XS","S"], fournisseur:{nom:"Nike",ville:"Beaverton"}, identifientRayon:1}
    ];
    dbo.collection("article").insertMany(myobj, function(err, res) {
    if (err) throw err; 
    console.log("Number of documents inserted: " + res.insertedCount);
    db.close();
    });
}); 