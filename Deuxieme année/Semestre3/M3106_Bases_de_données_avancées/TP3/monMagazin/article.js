var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("magasin");
  dbo.createCollection("article",{
    validator: {
      $jsonSchema: {
        bsonType: "object",
        required: [ "marque", "prix",  "nom", "reference","categorie", "listeTailles", "fournisseur", "identifientRayon" ],
        properties: {
          marque: {
            bsonType: "string"
          },
          prix: {
            bsonType: "int",
            minimum: 0
          },
          nom: {
            bsonType: "string"
          },
          reference: {
            bsonType: "int"
          },
          categorie: {
            enum: [ "enfant", "junior", "senior"]
          },
          listeTailles: {
            bsonType: [ "array" ],
            items: {
              bsonType: ["string"]
            }
          },
          fournisseur: {
            bsonType: "object",
            required: [ "nom", "ville" ],
            properties: {
              nom: {
                bsonType: "string"
              },
              ville: {
                bsonType: "string"
              }
            }
          },
          identifientRayon: {
            bsonType: "int"
          },
        }
      }
    }
  }, function(err, res) {
    if (err) throw err;
    console.log("Article created!");
    db.close();
  });
}); 