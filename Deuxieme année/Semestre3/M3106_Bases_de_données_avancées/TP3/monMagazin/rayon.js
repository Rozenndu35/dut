var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("magasin");
  dbo.createCollection("rayon",{
    validator: {
      $jsonSchema: {
        bsonType: "object",
        required: [ "description", "employe",  "id" ],
        properties: {
          description: {
            bsonType: "string"
          },
          employe: {
            bsonType: "string"
          },
          id: {
            bsonType: "int"
          },
        }
      }
    }
  }, function(err, res) {
    if (err) throw err;
    console.log("Rayon created!");
    db.close();
  });
}); 