//modifie la categorie enfant en senior de l'article 1121
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/updateEnfantSenior";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("magasin");
  var myquery = { reference:1121};
  var newvalues = {$set: { "fournisseur.ville": "modif"} };
  dbo.collection("article").updateOne(myquery, newvalues, function(err, res) {
    if (err) throw err;
    console.log("1121 article modifier");
    db.close();
  });
});