//modifie la categorie junior en loisir de l'article 1221 -> provoque erreur
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/updateJuniorLoisir";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("magasin");
  var myquery = { reference:1221};
  var newvalues = {$set: { categorie: "loisir"} };
  dbo.collection("article").updateOne(myquery, newvalues, function(err,rows){
      if(err != null){
          console.log("erreur loisir n'existe pas");
          db.close();
      }
  });
});