var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("magasin");
  var query = { reference:1221};
  dbo.collection("article").find(query).toArray(function(err, result) {
    if (err) throw err;
    var rayon=""
    result.forEach(article => rayon=article.identifientRayon);
    var queryRayon = { id: rayon};
    dbo.collection("rayon").find(queryRayon).toArray(function(err, result) {
      if (err) throw err;
      console.log(result)
      db.close();
    });
    db.close();
  });
}); 