package fr.ubs;
import io.javalin.*;
import io.javalin.http.staticfiles.Location;

public class WebApp {
	public static void main(String[] args) {
		Javalin app = Javalin.create(config ->{
			config.addStaticFiles("public", Location.EXTERNAL);
			}).start(8081);
		//app.get("/", ctx->ctx.result("Hello World"));
		app.get("/", ctx->ctx.redirect("webform.html"));
		app.post("/form.html", ctx->{

		ctx.html(
			"<table><caption>Information </caption><tr><th>name</th><th>Information</th></tr><tr><td>First name</td><td>"+ 				ctx.formParam("firstname")+"</td></tr><tr><td>Last name</td><td>"+ ctx.formParam("lastname")+"</td></tr><tr><td>EMail</td><td>"+ ctx.formParam("email")+"</td></tr><tr><td>birthday</td><td>"+ ctx.formParam("birthday")+"</td></tr></table> "+
	 "<style type='text/css'>table{border-collapse: collapse;}td, th {border: 1px solid black;}</style>");
		});
	}
}
