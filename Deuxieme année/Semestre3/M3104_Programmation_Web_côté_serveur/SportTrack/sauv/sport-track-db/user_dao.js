var db = require('./sqlite_connection');
var UserDAO = function(){

    /**
     * values : le tableau de valeur a inserer
     * callback : pour le message d'erreur
     */
    this.insert = function(values, callback){
        let stmt = db.prepare("INSERT INTO Compte VALUES(?,?,?,?,?,?,?,?,?)");
        stmt.run([values[0] ,values[1] ,values[2] ,values[3] ,values[4] ,values[5] ,values[6] ,values[7] ,values[8]] , callback );
    }; 

    /**
     * key : la cle d'identification
     * values : le tableau de valeur a modifier
     * callback : pour le message d'erreur
     */
    this.update = function(key, values, callback){
        let stmt = db.prepare("UPDATE Compte SET adresseMail =?, motPass = ?, nom = ?, prenom = ?, dateN = ?, sexe = ?, taille = ?, poids = ? WHERE idCompte=?");
        stmt.run([values[1], values[2], values[3], values[4], values[5], values[6], values[7], values[8],key],function(err,rows){
            if(err == null){
                console.log("modifier");
            }
            else{
                callback(err);
            }
        });
    };

     /**
     * key : la cle d'identification
     * callback : pour le message d'erreur
     */
    this.delete = function(key, callback){
        db.run("DELETE FROM Compte WHERE idCompte=?",key,callback);
    };
     /**
     * callback : pour le message d'erreur
     */
    this.findAll = function(callback){
        db.all("SELECT * FROM Compte",function(err,rows){
			if(err){
				console.log(err.message);
			}else{
				callback(rows);
			}
			});
    };
     /**
     * key : la cle d'identification
     * callback : pour le message d'erreur
     */


	this.findByKey = function(key, callback){
			db.all("SELECT * FROM Compte WHERE idCompte =?",key, function(err,rows){
				if(err){
					console.log(err.message);
				}else{
					callback(rows);
				}
			});
	};
};



var dao = new UserDAO();
module.exports = dao;
