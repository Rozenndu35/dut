<?php
    ini_set('display_errors', 'On');
    error_reporting(E_ALL);

    require_once('Controller.php');
    require_once('./model/Activite.php');
    require_once('./model/Donnees.php');
    require_once('./model/ActivityDAO.php');
    require_once('./model/ActivityEntryDAO.php');
    require_once('./model/CalculDistanceImpl.php');
    require_once('./model/Compte.php');
    require_once('./model/UserDAO.php');
    
    class UploadActivityController implements Controller {
        public function handle($request){
            //connection a la base activiter et recupere l'index de fin
            $daoA = ActivityDAO::getInstance();
            $listeA = $daoA->findAll();
            $idAct = sizeof($listeA);
            $idAct = $idAct;
            //connection a donner et recuper l'index de fin
            $daoD = ActivityEntryDAO::getInstance();
            $listeD = $daoD->findAll();
            $idDonnee = sizeof($listeD);

            //recuperer le fichier et le stocké
            $fichier = $_FILES['file']['tmp_name'];
            $strJsonFileContents = file_get_contents($fichier);            
            
            //creer dans la base
            if($strJsonFileContents  <> FALSE){
                //recupere dans un tableau les informations
                $trace = json_decode($strJsonFileContents, true);
                $activite = $trace['activity'];
                $donnee = $trace['data'];
                $newActivite = new Activite();

                //recupere l'id du compte
                $dao = UserDAO::getInstance();
                $listeU = $dao->findAll();
                $oldEMail = $_SESSION['connecter'];
                foreach ($listeU as $user) {
                    $email = $user-> getAdresseMail();
                    if($email == $oldEMail){
                        $id = $user->getID();
                    }
                }

                //creer un activite
                $newActivite->init($idAct,$activite['date'],$activite['description'],null,null,null,null,null,null,null,$id);
                $daoA->insert($newActivite);
                
                // initialise et enregistre les données
                for ($i=0 ; $i<count($donnee)-1 ; $i++){
                    $temp = $donnee[$i]['time'];
                    $des = $donnee[$i]['des'];
                    $f = $donnee[$i]['cardio_frequency'];
                    $lat = $donnee[$i]['latitude'];
                    $long = $donnee[$i]['longitude'];
                    $altitude = $donnee[$i]['altitude'];
                    $newDonnee = new Donnees();
                    $newDonnee->init($idDonnee, $temp, $des, $f, $altitude, $lat, $long, $idAct);
                    $daoD->insert($newDonnee);
                    $idDonnee= $idDonnee+1;
                }
                //fini la creation et l'enregistrement de l'activite
                $cal = new CalculDistanceImpl();
                $dist=$cal->calculDistanceTrajet($donnee);
                $newActivite->setDistance($dist);
                $daoA->update($newActivite);
                //renvoi l'id de l'activiter
                $_SESSION["messageAddActivite"] = $idAct ; 
            }
            else{
                $_SESSION["messageAddActivite"] = -1 ; 
            }
        }
    }
?>
