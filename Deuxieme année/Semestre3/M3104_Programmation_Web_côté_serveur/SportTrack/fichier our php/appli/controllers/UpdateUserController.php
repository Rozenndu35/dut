<?php
    ini_set('display_errors', 'On');
    error_reporting(E_ALL);

    require_once('Controller.php');
    require_once('./model/Compte.php');
    require_once('./model/UserDAO.php');

    class UpdateUserController implements Controller {
        public function handle($request){
            $dao = UserDAO::getInstance();
            $listeU = $dao->findAll();
            $oldEMail = $_SESSION['connecter'];
            foreach ($listeU as $user) {
                $email = $user-> getAdresseMail();
                if($email == $oldEMail){
                    $id = $user->getID();
                    $oldMotPas = $user->getMotPass();
                    $nom = $user->getNom();
                    $prenom = $user->getPrenom();
                    $naissance = $user->getDateN();
                    $sexe = $user->getSexe();
                    $taille = $user->getTaille();
                    $poids = $user->getPoids();

                    if($oldMotPas == $request["oldmotPasse"]){
                        if($request["email"]!= null){
                            $email = $request["email"];
                        }
                        if($request["nom"]!= null){
                            $nom = $request["nom"];
                        }
                        if($request["prenom"]!= null){
                            $email = $request["prenom"];
                        }
                        if($request["naissance"]!= null){
                            $naissance = $request["naissance"];
                        }
                        if($request["sexe"]!= null){
                            $sexe = $request["sexe"];
                        }
                        if($request["taille"]!= null){
                            $taille = $request["taille"];
                        }
                        if($request["motPasse"]!= null){
                            $oldMotPas = $request["motPasse"];
                        }
                        if($request["poids"]!= null){
                            $poids = $request["poids"];
                        }
                        $newUser = new Compte();

                        $newUser->init($id,$email , $oldMotPas,$nom , $prenom, $naissance, $sexe, $taille, $poids); 
                        $dao->update($newUser);
                        $_SESSION['messageUpdateUser']= 'Modifications effectuées avec succès';
                    }
                    else{
                        $_SESSION['messageUpdateUser']= 'Vous vous êtes trompé dans votre mot de passe';
                    }
                }
                else{
                    $_SESSION['messageUpdateUser']= 'nous n\'avons pas trouvé votre compte'; 
                }
            }
           
        }
    }
?>
