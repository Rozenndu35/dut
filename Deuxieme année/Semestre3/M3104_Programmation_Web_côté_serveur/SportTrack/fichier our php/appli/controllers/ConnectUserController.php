<?php
    ini_set('display_errors', 'On');
    error_reporting(E_ALL);

    require_once('Controller.php');
    require_once('./model/Compte.php');
    require_once('./model/UserDAO.php');

    class ConnectUserController implements Controller {
        public function handle($request){
            $dao = UserDAO::getInstance();
            $listeU = $dao->findAll();
            $corect = false;

            foreach ($listeU as $user) {
                $email = $user-> getAdresseMail();
                $mp = $user-> getMotPass();
                if(($email == $request["email"]) and ($mp == $request["motPasse"])) {
                    $corect = true;
                } 
            }
            
            if($corect){
                $_SESSION["connecter"] = $request["email"] ; 
                $_SESSION["messageConnection"] = "Vous vous êtes connecté" ;
            }  
            else{
                $_SESSION["connecter"] = null ;
                $_SESSION["messageConnection"] = "Vous vous êtes trompé" ; 
            }

             
        }
    }
?>
