<?php
    ini_set('display_errors', 'On');
    error_reporting(E_ALL);

    require_once('Controller.php');
    require_once('./model/Compte.php');
    require_once('./model/UserDAO.php');

    class DisconnectUserController implements Controller {
        public function handle($request){
            $_SESSION["connecter"]=null;
        }
    }
?>