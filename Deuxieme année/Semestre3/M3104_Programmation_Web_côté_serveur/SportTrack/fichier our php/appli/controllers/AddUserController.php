<?php
    ini_set('display_errors', 'On');
    error_reporting(E_ALL);

    require_once('Controller.php');
    require_once('./model/Compte.php');
    require_once('./model/UserDAO.php');

    class AddUserController implements Controller {
        public function handle($request){
            $dao = UserDAO::getInstance();
            $listeU = $dao->findAll();
            $newUser = new Compte();
            $id = sizeof($listeU);
            $newUser->init($id, $request["email"], $request["motPasse"], $request["nom"], $request["prenom"], $request["naissance"], $request["sexe"], $request["taille"], $request["poids"]); 
            if($request["email"] != ""){
                if($request["motPasse"]!= ""){
                    if($request["nom"] != ""){
                        if( $request["prenom"] != ""){
                            if($request["naissance"]!= ""){
                                if($request["taille"]!= ""){
                                    if($request["poids"] != ""){
                                        $requetEmail = $newUser->getAdresseMail();
                                        $existe = false;
                                        foreach ($listeU as $user) {
                                            $email = $user-> getAdresseMail();
                                            if($email == $requetEmail){
                                                $existe = true;
                                            }
                                        }
                                        if ($existe){
                                            $_SESSION['messageAddUser']= 'Il existe déjà un compte avec cette Email';
                                        }
                                        else{
                                            $_SESSION['messageAddUser']= 'Créér'; 
                                            $dao->insert($newUser);  
                                        }
                                    }
                                    else{
                                        $_SESSION['messageAddUser']= 'vous n\'avez pas rentré votre poids';
                                    }
                                }
                                else{
                                    $_SESSION['messageAddUser']= 'vous n\'avez pas rentré votre taille';
                                }
                            }
                            else{
                                $_SESSION['messageAddUser']= 'vous n\'avez pas rentré votre date de naissance';
                            }
                        }
                        else{
                            $_SESSION['messageAddUser']= 'vous n\'avez pas rentré votre nom';
                        }
                    }
                    else{
                        $_SESSION['messageAddUser']= 'vous n\'avez pas rentré votre prenom';
                    }
                }
                else{
                    $_SESSION['messageAddUser']= 'vous n\'avez pas rentré votre mot de passe';
                }
            }
            else{
                $_SESSION['messageAddUser']= 'vous n\'avez pas rentré votre adresse mail';
            }
            
        }
    }
?>
