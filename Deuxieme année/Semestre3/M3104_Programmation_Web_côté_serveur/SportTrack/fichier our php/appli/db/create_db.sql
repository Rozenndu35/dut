DROP TABLE Donnees;
DROP TABLE Activite;
DROP TABLE Compte;


CREATE TABLE Compte (
    idCompte INTEGER PRIMARY KEY,
    adresseMail TEXT NOT NULL UNIQUE,
    motPass TEXT NOT NULL,
    nom TEXT NOT NULL,
    prenom TEXT NOT NULL,
    dateN DATE NOT NULL,
    sexe TEXT NOT NULL,
    taille INTEGER NOT NULL,
    poids INTEGER NOT NULL,
    CHECK (sexe = 'femme' OR sexe ='homme'),
    CHECK (	taille > 0
			AND poids > 0)
);

CREATE TABLE Activite (
	idAct INTEGER PRIMARY KEY,
	dateAct DATE NOT NULL,
	description TEXT,
	distance INTEGER, 
	heureD TIME,
	heureF TIME,
	duree REAL,
	fcMax REAL,
	fcMin REAL,
	fcMoyenne REAL,
	leCompte INTEGER NOT NULL,
	FOREIGN KEY (leCompte) REFERENCES Compte(idCompte),
		CHECK (	distance > 0 OR distance = null
			AND fcMin < fcMax
			AND fcMoyenne < fcMax
			AND fcMin > fcMoyenne
			AND heureD < heureF)
);


CREATE TABLE Donnees (
	idDonnees INTEGER PRIMARY KEY,
	temps TEXT NOT NULL,
	description TEXT,
	fc REAL NOT NULL,
	altitude REAL NOT NULL,
	latitude REAL NOT NULL,
	longitude REAL NOT NULL,
	lActivite INTEGER NOT NULL,
	FOREIGN KEY (lActivite) REFERENCES Activite(idAct),
	CHECK (	fc >30)
);
/*
faire trigger
manque curseur  fc< 220 - (age recupere avec la date de naissance)
calculer distance : voir PHP car manque pointeur
*/

CREATE TRIGGER IF NOT EXISTS fcMax
AFTER INSERT ON Donnees
BEGIN
	UPDATE Activite
	SET fcMax = (SELECT MAX(fc)
				FROM Donnees
				WHERE new.lActivite = lActivite);
END;
----------------------------------------------------------	
CREATE TRIGGER IF NOT EXISTS fcMin
AFTER INSERT ON Donnees
BEGIN
	UPDATE Activite
	SET fcMin = (SELECT MIN(fc)
				FROM Donnees
				WHERE new.lActivite = lActivite);
END;
----------------------------------------------------------
CREATE TRIGGER IF NOT EXISTS fcMoyenne
AFTER INSERT ON Donnees
BEGIN
	UPDATE Activite
	SET fcMoyenne = (SELECT AVG(fc)
					FROM Donnees
					WHERE new.lActivite = lActivite);
END;
----------------------------------------------------------
CREATE TRIGGER IF NOT EXISTS heureF
AFTER INSERT ON Donnees
BEGIN
	UPDATE Activite
	SET heureF = (SELECT MAX(temps)
				FROM Donnees
				WHERE new.lActivite = lActivite);
END;
----------------------------------------------------------
CREATE TRIGGER IF NOT EXISTS heureD
AFTER INSERT ON Donnees
BEGIN
	UPDATE Activite
	SET heureD = (SELECT MIN(temps)
				FROM Donnees
				WHERE new.lActivite = lActivite);
END;
----------------------------------------------------------
CREATE TRIGGER IF NOT EXISTS duree
AFTER INSERT ON Donnees
BEGIN
	UPDATE Activite
	SET duree = (SELECT MAX(temps)
				FROM Donnees
				WHERE new.lActivite = lActivite)-(SELECT MIN(temps)
												  FROM Donnees
												  WHERE new.lActivite = lActivite);
END;
