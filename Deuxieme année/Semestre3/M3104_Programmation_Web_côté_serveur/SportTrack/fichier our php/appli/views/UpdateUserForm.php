<!DOCTYPE html>
<html lang="fr-FR">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="./resources/css/style.css" />
        <title>Modifier votre compte</title>
    </head>
	<body>
		<form method="post" action="index.php?page=user_update">
            <fieldset>

                <legend> Modifier vos informations </legend>
               
                <label for="nom">Nom </label>
                <input type="text" id="nom" name="nom" value="" size="20" maxlength="60" />
                <br />
                
                <label for="prenom">Prenom </label>
                <input type="text" id="prenom" name="prenom" value="" size="20" maxlength="60" />
                <br />
                
                <label for="naissance"> Date de Naissance </label>
                <input type="date" id="naissance" name="naissance" value="" size="20" maxlength="60" />
                <br />

                <br />
                Sexe 
                <input type="radio" name="sexe" value="femme" id="femme" /> <label for="femme">Femme</label>
                <input type="radio" name="sexe" value="homme" id="homme" /> <label for="homme">Homme</label>
                <input type="radio" name="sexe" value="null" id="null" checked /> <label for="homme"></label>
                <br />

                <label for="taille">Taille </label>
                <input type="number" id="taille" name="taille" step="0.01" />
                <br />

                <label for="poids">Poids</label>
                <input type="number" id="poids" name="poids" step="0.01" />
                <br />

                <label for="email">Adresse email</label>
                <input type="text" id="email" name="email"  size="20" maxlength="60" />
                <br />

                <label for="oldmotPasse"> Mot de passe actuel <span class="requis">*</span></label>
                <input type="password" id="oldmotPasse" name="oldmotPasse"/>
                <br />

                <label for="motPasse"> Nouveau mot de passe </label>
                <input type="password" id="motPasse" name="motPasse"/>
                <br />

                <input type="submit" value="Modifier" class="sansLabel" /> 
                
            </fieldset>
        </form>
        <form method="post" action="index.php?page=/">
            <input type="submit" value="Accueil" class="sansLabel" /> 
        </form>
    </body>
</html>