<!DOCTYPE html>
<html lang="fr-FR">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="./resources/css/style.css" />
        <title>Déconnexion</title>
    </head>

    <body>
        <header>
			<a href="http://m3104.iut-info-vannes.net/m3104_8/">SportTrack</a>

		</header>
        <section>
	        <div id= "info">
                <p> Vous venez de vous déconnecter.
            </div>
		</section>
        <form method="post" action="index.php?page=/">
            <input type="submit" value="Accueil" class="sansLabel" /> 
        </form>
    </body>
</html>
