<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="./resources/css/style.css" />
        <title>Accueil</title>
    </head>
    <body>
        <div id="conteneur1">
            <header>
                <h1><a href="http://m3104.iut-info-vannes.net/m3104_8/">SportTrack</a></h1>
            </header>
            <div id="conteneur2">
                <nav>
                    <ul>
                        <li><a href="http://m3104.iut-info-vannes.net/m3104_8/index.php?page=user_disconnect">Déconnexion</a></li>
                        <li><a href="http://m3104.iut-info-vannes.net/m3104_8/index.php?page=upload_activity_form">Ajouter une activité</a></li>
                        <li><a href="http://m3104.iut-info-vannes.net/m3104_8/index.php?page=user_update_form">Modifier votre compte</a></li>
                        <li><a href="http://m3104.iut-info-vannes.net/m3104_8/index.php?page=list_activities">Afficher les activités</a></li>
                    </ul>
                </nav>
                
                <section>
                    
                </section>
            </div>
        </div>
    </body>
</html>
