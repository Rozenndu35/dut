<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="./resources/css/style.css" />
        <title>Inscription</title>
    </head>
    <header>
                <h1><a href="http://m3104.iut-info-vannes.net/m3104_8/">SportTrack</a></h1>
    </header>
    <body>
		<form method="post" action="index.php?page=user_add">
            <fieldset>

                <legend> Creation de votre compte</legend>
               
                <label for="nom">Nom <span class="requis">*</span></label>
                <input type="text" id="nom" name="nom" value="nom" size="20" maxlength="60" />
                <br />
                
                <label for="prenom">Prenom <span class="requis">*</span></label>
                <input type="text" id="prenom" name="prenom" value="prenom" size="20" maxlength="60" />
                <br />
                
                <label for="naissance"> Date de Naissance <span class="requis">*</span></label>
                <input type="date" id="naissance" name="naissance" value="" size="20" maxlength="60" />
                <br />

                <br />
                Sexe 
                <input type="radio" name="sexe" value="femme" id="femme" /> <label for="femme">Femme</label>
                <input type="radio" name="sexe" value="homme" id="homme" checked /> <label for="homme">Homme</label>
                <br />

                <label for="taille">Taille <span class="requis">*</span></label>
                <input type="number" id="taille" name="taille" step="0.01" />
                <br />

                <label for="poids">Poids <span class="requis">*</span> </label>
                <input type="number" id="poids" name="poids" step="0.01" />
                <br />

                <label for="email">Adresse email <span class="requis">*</span></label>
                <input type="text" id="email" name="email" value="exemple@gmail.com" size="20" maxlength="60" />
                <br />

                <label for="motPasse"> Mot de passe <span class="requis">*</span></label>
                <input type="password" id="motPasse" name="motPasse"/>
                <br />

                <input type="submit" value="Valider" class="sansLabel" /> 
                
            </fieldset>
        </form>
        <form method="post" action="index.php?page=/">
            <input type="submit" value="Accueil" class="sansLabel" /> 
        </form>
    </body>
</html>
