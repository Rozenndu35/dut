<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="./resources/css/style.css" />
        <title>Liste des activitées</title>
    </head>
    <header>
                <h1><a href="http://m3104.iut-info-vannes.net/m3104_8/">SportTrack</a></h1>
    </header>
    <body style= >
        <div>
             <table>
                <caption>Liste des activité</caption>
                <thead> <!-- En-tête du tableau -->
                    <tr>
                        <th>Date</th>
                        <th>Description</th>
                        <th>Distance</th>
                        <th>Debut</th>
                        <th>Fin</th>
                        <th>Duree</th>
                        <th>Fréquence Card max</th>
                        <th>Fréquence Card min</th>
                        <th>Fréquence Card moy</th>
                    </tr>
                </thead>
                <tbody> <!-- Corps du tableau -->
                    <?php 
                        $listeA = $_SESSION['listActivite'];
                        foreach ($listeA as $activite) {
                            $date = $activite["dateAct"];
                            $des = $activite["description"];
                            $dis = $activite["distance"];
                            $deb = $activite["heureD"];
                            $fin = $activite["heureF"];
                            $dur = $activite ["duree"];
                            $fcMa = $activite["fcMax"];
                            $fcMi = $activite["fcMin"];
                            $fcMo = $activite ["fcMoyenne"];
                            echo("
                                <tr>
                                    <th>$date</th>
                                    <th>$des</th>
                                    <th>$dis</th>
                                    <th>$deb</th>
                                    <th>$fin</th>
                                    <th>$dur</th>
                                    <th>$fcMa</th>
                                    <th>$fcMi</th>
                                    <th>$fcMo</th>
                                </tr>
                            ");
                        }
                    ?>
                    

                </tbody>
            </table>
            <form method="post" action="index.php?page=/">
                <input type="submit" value="Accueil" class="sansLabel" /> 
            </form>
        </div>
    </body>
</html>
