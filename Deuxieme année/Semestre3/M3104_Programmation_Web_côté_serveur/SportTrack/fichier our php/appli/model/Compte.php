<?php
 class Compte{
   private $idCompte;
   private $adresseMail;
   private $motPass;
   private $nom;
   private $prenom;
   private $dateN;
   private $sexe;
   private $taille;
   private $poids;

   public function  __construct() { }
   public function init($id, $adresse, $motP, $n, $pre, $date, $s, $t, $poi){

     $this->idCompte = $id;
     $this->adresseMail = $adresse;
     $this->motPass = $motP;
     $this->nom = $n;
     $this->prenom = $pre;
     $this->dateN = $date;
     $this->sexe = $s;
     $this->taille = $t;
     $this->poids = $poi;
   }

   public function getID(){ return $this->idCompte; }
   public function getAdresseMail(){ return $this->adresseMail; }
   public function getMotPass(){ return $this->motPass; }
   public function getNom(){ return $this->nom; }
   public function getPrenom(){ return $this->prenom; }
   public function getDateN(){ return $this->dateN; }
   public function getSexe(){ return $this->sexe; }
   public function getTaille(){ return $this->taille; }
   public function getPoids(){ return $this->poids; }
   public function  __toString() { return $this->idCompte. " ". $this->adresseMail. " ". $this->motPass. " ". $this->nom. " ". $this->prenom. " ". $this->dateN. " ". $this->sexe. " ". $this->taille. " ". $this->poids; }
  }
?>
