<?php
require_once('SqliteConnection.php');
class ActivityDAO {
    private static $dao;
	
    private function __construct() {}

    public final static function getInstance() {
       if(!isset(self::$dao)) {
           self::$dao= new ActivityDAO();
       }
       return self::$dao;
    }

    public final function findAll(){
       $dbc = SqliteConnection::getInstance()->getConnection();
       $query = "select * from Activite order by leCompte";
       $stmt = $dbc->query($query);
       $results = $stmt->fetchALL(PDO::FETCH_CLASS, 'Activite');
       return $results;
    }

   public final function insert($activite){
      if($activite instanceof Activite){
         $dbc = SqliteConnection::getInstance()->getConnection();
         // prepare the SQL statement
         $query = "insert into Activite(idAct, dateAct, description, distance, heureD, heureF, duree, fcMax, fcMin, fcMoyenne, leCompte) values (:id, :date, :desc, :dist, :hdep, :hfin, :d, :fcMaxi, :fcMini, :fcMoy, :compte)";
         $stmt = $dbc->prepare($query);

         // bind the paramaters
         $stmt->bindValue(':id',$activite->getIdAct(),PDO::PARAM_STR);
         $stmt->bindValue(':date',$activite->getDate(),PDO::PARAM_STR);
         $stmt->bindValue(':desc',$activite->getDescription(),PDO::PARAM_STR);
         $stmt->bindValue(':dist',(string)($activite->getDistance()),PDO::PARAM_STR);
         $stmt->bindValue(':hdep',$activite->getHeureD(),PDO::PARAM_STR);
         $stmt->bindValue(':hfin',$activite->getHeureF(),PDO::PARAM_STR);
         $stmt->bindValue(':d',$activite->getDuree(),PDO::PARAM_STR);
         $stmt->bindValue(':fcMaxi',$activite->getFcMax(),PDO::PARAM_STR);
         $stmt->bindValue(':fcMini',$activite->getFcMin(),PDO::PARAM_STR);
         $stmt->bindValue(':fcMoy',$activite->getFcMoy(),PDO::PARAM_STR);
         $stmt->bindValue(':compte',$activite->getCompte(),PDO::PARAM_STR);

         // execute the prepared statement
         $stmt->execute();
       }
   }

  public function delete($activite , $user){ 
    $dbc = SqliteConnection::getInstance()->getConnection();
    $delete = $dbc->prepare("DELETE FROM Activite WHERE idAct = :id and leCompte = :idU");
    // bind the paramaters
    $delete->bindValue(':id', $activite ,PDO::PARAM_INT);
    $delete->bindValue(':idU', $user ,PDO::PARAM_INT);

    // delete
    $delete->execute();
   }

  public function update($activite){
    if($activite instanceof Activite){
        $dbc = SqliteConnection::getInstance()->getConnection();

        // bind the paramaters
        
        $update =$dbc->prepare("UPDATE Activite SET idAct=:id, dateAct=:date, description=:desc, distance=:dist, heureD=:hdep, heureF=:hfin, duree=:d, fcMax=:fcMaxi, fcMin=:fcMini, fcMoyenne=:fcMoy, leCompte=:compte WHERE idAct =:id");
        
        $update->bindValue(':date',$activite->getDate(),PDO::PARAM_STR);
        $update->bindValue(':desc',$activite->getDescription(),PDO::PARAM_STR);
        $update->bindValue(':dist',$activite->getDistance(),PDO::PARAM_STR);
        $update->bindValue(':hdep',$activite->getHeureD(),PDO::PARAM_STR);
        $update->bindValue(':hfin',$activite->getHeureF(),PDO::PARAM_STR);
        $update->bindValue(':d',$activite->getDuree(),PDO::PARAM_STR);
        $update->bindValue(':fcMaxi',$activite->getFcMax(),PDO::PARAM_STR);
        $update->bindValue(':fcMini',$activite->getFcMin(),PDO::PARAM_STR);
        $update->bindValue(':fcMoy',$activite->getFcMoy(),PDO::PARAM_STR);
        $update->bindValue(':compte',$activite->getCompte(),PDO::PARAM_STR);
        $update->bindValue(':id',$activite->getIdAct(),PDO::PARAM_STR);


        // update
        $update->execute();
    }
  }
  
  public final function findActivity($user){
    $dbc = SqliteConnection::getInstance()->getConnection();
    $query = "select * from Activite WHERE leCompte = :user";
    $stmt = $dbc->prepare($query);
    $stmt->bindValue(':user',$user,PDO::PARAM_STR);
    $stmt->execute();
    $results = $stmt->fetchall(PDO::FETCH_ASSOC);
    return $results;
  }

  public final function findLActivity($activite , $user){
    $dbc = SqliteConnection::getInstance()->getConnection();
    $query = "select * from Activite WHERE leCompte = :user and idAct = :act";
    $stmt = $dbc->prepare($query);
    $stmt->bindValue(':user',$user,PDO::PARAM_INT);
    $stmt->bindValue(':act',$activite,PDO::PARAM_INT);
    $stmt->execute();
    $results = $stmt->fetch(PDO::FETCH_ASSOC);
    return $results;
  }
}
?>
