<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL);
require_once('SqliteConnection.php');
class ActivityEntryDAO {
    private static $dao;

    private function __construct() {}

    public final static function getInstance() {
       if(!isset(self::$dao)) {
           self::$dao= new ActivityEntryDAO();
       }
       return self::$dao;
    }

	public final function findAll(){
	  $dbc = SqliteConnection::getInstance()->getConnection();
	  $query = "select * from Donnees order by lActivite";
	  $stmt = $dbc->query($query);
	  $results = $stmt->fetchALL(PDO::FETCH_CLASS, 'Donnees');
	  return $results;
	}


   public final function insert($donnee){
      if($donnee instanceof Donnees){
         $dbc = SqliteConnection::getInstance()->getConnection();
         // prepare the SQL statement
         $query = "insert into Donnees(idDonnees, temps, description, fc, altitude, latitude, longitude, lActivite) values (:id, :t, :d, :f, :a, :la, :lo, :lA)";
         $stmt = $dbc->prepare($query);

         // bind the paramaters
         $stmt->bindValue(':id',$donnee->getID(),PDO::PARAM_STR);
         $stmt->bindValue(':t',$donnee->getTemps(),PDO::PARAM_STR);
         $stmt->bindValue(':d',$donnee->getDescription(),PDO::PARAM_STR);
         $stmt->bindValue(':f',$donnee->getFC(),PDO::PARAM_STR);
         $stmt->bindValue(':a',$donnee->getAltitude(),PDO::PARAM_STR);
         $stmt->bindValue(':la',$donnee->getLatitude(),PDO::PARAM_STR);
         $stmt->bindValue(':lo',$donnee->getLongitude(),PDO::PARAM_STR);
         $stmt->bindValue(':lA',$donnee->getLActivite(),PDO::PARAM_STR);

         // execute the prepared statement
         $stmt->execute();
       }
   }

  public function delete($donnee){ 
    $dbc = SqliteConnection::getInstance()->getConnection();
    $delete = $dbc->prepare("DELETE FROM Donnees WHERE idDonnees = :id");

    // bind the paramaters
    $delete->bindValue(':id',$donnee,PDO::PARAM_INT);

    // delete
    $delete->execute();
   }

  public function update($donnee){
    if($donnee instanceof Donnees){
        $dbc = SqliteConnection::getInstance()->getConnection();

        // bind the paramaters
        
        $update =$dbc->prepare("UPDATE Donnees SET idDonnees=:id, temps=:t, description=:d, fc=:f, altitude=:a, latitude=:la, longitude=:lo, lActivite=:lA WHERE idDonnees =:id");

        $update->bindValue(':t',$donnee->getTemps(),PDO::PARAM_STR);
        $update->bindValue(':d',$donnee->getDescription(),PDO::PARAM_STR);
        $update->bindValue(':f',$donnee->getFC(),PDO::PARAM_STR);
        $update->bindValue(':a',$donnee->getAltitude(),PDO::PARAM_STR);
        $update->bindValue(':la',$donnee->getLatitude(),PDO::PARAM_STR);
        $update->bindValue(':lo',$donnee->getLongitude(),PDO::PARAM_STR);
        $update->bindValue(':lA',$donnee->getLActivite(),PDO::PARAM_STR);
		$update->bindValue(':id',$donnee->getID(),PDO::PARAM_STR);

        // update
        $update->execute();
    }
  }
  
  public final function findDonnee($activite){
    $dbc = SqliteConnection::getInstance()->getConnection();
    $query = "SELECT * FROM Donnees WHERE lActivite = :activite";
    $stmt = $dbc->prepare($query);
    $stmt->bindValue(':activite',$activite,PDO::PARAM_STR);
    $stmt->execute();
    $results = $stmt->fetchALL(PDO::FETCH_ASSOC);
    return $results;
 }

}
?>
