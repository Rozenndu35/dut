<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL);
require_once('SqliteConnection.php');

class UserDAO {
    private static $dao;

    private function __construct() {}

    public final static function getInstance() {
       if(!isset(self::$dao)) {
           self::$dao= new UserDAO();
       }
       return self::$dao;
    }

    public final function findAll(){
       $dbc = SqliteConnection::getInstance()->getConnection();
       $query = "select * from Compte order by idCompte";
       $stmt = $dbc->query($query);
       $results = $stmt->fetchALL(PDO::FETCH_CLASS, 'Compte');
       return $results;
    }

   public final function insert($compte){
      if($compte instanceof Compte){
         $dbc = SqliteConnection::getInstance()->getConnection();
         
         // prepare the SQL statement
         $query = "insert into Compte(idCompte, adresseMail, motPass, nom, prenom, dateN, sexe, taille, poids) values (:id, :adresse, :motP, :n, :pre, :date, :s, :t, :poi)";
         $stmt = $dbc->prepare($query);

         // bind the paramaters
         $stmt->bindValue(':id',$compte->getID(),PDO::PARAM_STR);
         $stmt->bindValue(':adresse',$compte->getAdresseMail(),PDO::PARAM_STR);
         $stmt->bindValue(':motP',$compte->getMotPass(),PDO::PARAM_STR);
         $stmt->bindValue(':n',$compte->getNom(),PDO::PARAM_STR);
         $stmt->bindValue(':pre',$compte->getPrenom(),PDO::PARAM_STR);
         $stmt->bindValue(':date',$compte->getDateN(),PDO::PARAM_STR);
         $stmt->bindValue(':s',$compte->getSexe(),PDO::PARAM_STR);
         $stmt->bindValue(':t',$compte->getTaille(),PDO::PARAM_STR);
         $stmt->bindValue(':poi',$compte->getPoids(),PDO::PARAM_STR);
         
         // execute the prepared statement
         $stmt->execute();
       }
   }

  public function delete($compte){ 
    $dbc = SqliteConnection::getInstance()->getConnection();
    $delete = $dbc->prepare("DELETE FROM Compte WHERE idCompte = :id");

    // bind the paramaters
    $delete->bindValue(':id',$compte,PDO::PARAM_INT);

    // delete
    $delete->execute();
   }

  public function update($compte){
    if($compte instanceof Compte){
        $dbc = SqliteConnection::getInstance()->getConnection();

        // bind the paramaters
        
        $update = $dbc->prepare("UPDATE Compte SET idCompte =:id, adresseMail=:adresse, motPass=:motP, nom=:n, prenom=:pre, dateN=:dat, sexe=:s, taille=:t, poids=:poi WHERE idCompte=:id");
        
        $update->bindValue(':adresse',$compte->getAdresseMail(),PDO::PARAM_STR);
        $update->bindValue(':motP',$compte->getMotPass(),PDO::PARAM_STR);
        $update->bindValue(':n',$compte->getNom(),PDO::PARAM_STR);
        $update->bindValue(':pre',$compte->getPrenom(),PDO::PARAM_STR);
        $update->bindValue(':dat',$compte->getDateN(),PDO::PARAM_STR);
        $update->bindValue(':s',$compte->getSexe(),PDO::PARAM_STR);
        $update->bindValue(':t',$compte->getTaille(),PDO::PARAM_STR);
        $update->bindValue(':poi',$compte->getPoids(),PDO::PARAM_STR);
		    $update->bindValue(':id',$compte->getID(),PDO::PARAM_STR);
		
        // update
        $update->execute();
    }
  }

  /**
   * Chercher un utilisateur a partir de son adresse
   */
  public final function findUser($user){
    $dbc = SqliteConnection::getInstance()->getConnection();
    $query = "select * from Compte WHERE adresseMail = :user";
    $stmt = $dbc->prepare($query);
    $stmt->bindValue(':user',$user,PDO::PARAM_STR);
    $stmt->execute();
    $results =$stmt->fetch(PDO::FETCH_ASSOC);
    return $results;

 }
}
?>
