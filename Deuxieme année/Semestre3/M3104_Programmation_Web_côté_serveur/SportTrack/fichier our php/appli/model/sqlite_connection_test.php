<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL);

require_once("Activite.php");
require_once("Compte.php");
require_once("Donnees.php");
require_once("ActivityDAO.php");
require_once("ActivityEntryDAO.php");
require_once("UserDAO.php");

/**
 * fonction affichage et gestion du tableau d'utilisateur
 */
function AffichageTableUser(){
	
	$dao = UserDAO::getInstance();
	
	//afficher tous les utilisateurs
	$comptes = $dao->findAll();
	print_r($comptes);

	print("\n _______________________________________________________________________________________________________________________________________________\n");
	//ajouter un utilisateur
	$newUser = new Compte();
	$newUser->init(3, "gym@gmail.com", "gym", "Costiou", "Rozenn", 2000-04-25, "femme", "161", "52.5"); 
	//$dao->insert($newUser);


	//afficher l'utilisateur selectionné
	$compte = $dao->findUser("gym@gmail.com");
	print("\nle compte est : \n \n");
	print_r($compte);
	
	print("\n _______________________________________________________________________________________________________________________________________________");
	//modifier l'utilisateur selectionné
	$newUser->init(3, "essai@gmail.com", "jesaispas", "bautrais", "evan", 2002-04-25, "homme", "175", "62.5"); 
	//$compte = $dao->update($newUser);

	//afficher l'utilisateur selectionné
	print("\nle compte modifier est : \n \n");
	$compte = $dao->findUser("essai@gmail.com");
	print_r($compte);

	print("_______________________________________________________________________________________________________________________________________________");
	//supprimer l'utilisateur selectionné
	//$dao->delete(1);
	
	//afficher l'utilisateur selectionné qui n'existe plus
	$compte = $dao->findUser("jeplus@gmail.com");
	//print_r($compte);

	print("_______________________________________________________________________________________________________________________________________________");
	//afficher tous les utilisateur
	$comptes = $dao->findAll();
	print_r($comptes);

}
/**
 * fonction affichage et gestion du tableau d'activités
 */
function AffichageTableActivity(){

	$dao = ActivityDAO::getInstance();
	
	//afficher toutes les activites
	$activite = $dao->findAll();
	print_r($activite);

	print("\n _______________________________________________________________________________________________________________________________________________\n");
	//ajouter une activité
	$newAct = new Activite();
	$newAct->init(7,"2019-09-25", "promenade du dimanche", null, null, null, null, null, null, null,1); 
	//$dao->insert($newAct);


	//afficher l'activite selectionnée
	$act = $dao->findLActivity(7,1);
	print("\n l\'activite est : \n \n");
	print_r($act);
	
	print("\n _______________________________________________________________________________________________________________________________________________");
	//modifier l'utilisateur selectionner
	$newAct->init(7,"2019-09-25", "promenade du samedi", null, null, null, null, null, null, null,1); 
	//$activite = $dao->update($newAct);
	
	//afficher l'activite selectionné
	$act = $dao->findLActivity(7,1);
	print("\n l\'activite est : \n \n");
	print_r($act);

	print("_______________________________________________________________________________________________________________________________________________");
	//suprimer la donnée
	$dao->delete(8,0);

	print("_______________________________________________________________________________________________________________________________________________");
	//afficher toutes les activités d'un utilisateur
	$activite = $dao->findActivity(1);
	print("\n les activites de l'utilisateur 3 sont : \n \n");
	print_r($activite);

}
/**
 * fonction affichage et gestion du tableau de données
 */
function AffichageTableActivityEntry(){
	
	$dao = ActivityEntryDAO::getInstance();
	
	//afficher toutes les données
	$listD = $dao->findAll();
	print_r($listD);

	print("\n _______________________________________________________________________________________________________________________________________________\n");
	//ajouter une activité
	$newDonnee = new Donnees();
	$newDonnee->init(1, 15,"desc",70,47.644795,-2.776605,18 , 3); 
	//$dao->insert($newDonnee);

	print("\n _______________________________________________________________________________________________________________________________________________");
	//modifier la donnee
	$newDonnee->init(1, "00:15:00","descUpdate",70,47.644795,-2.776605,18 , 3); 
	$dao->update($newDonnee);

	print("_______________________________________________________________________________________________________________________________________________");
	//suprimer la donnee
	$dao->delete(0,0);
	
	
	print("_______________________________________________________________________________________________________________________________________________");
	//afficher toutes les données
	$listD = $dao->findAll();
	print_r($listD);

	print("_______________________________________________________________________________________________________________________________________________");
	//afficher toutes les données d'une activite
	$listD = $dao->findDonnee(3);
	print("\n les donnees de l'activite 1 sont : \n \n");
	print_r($listD);

}

AffichageTableUser();
AffichageTableActivity();
//AffichageTableActivityEntry();
?>

