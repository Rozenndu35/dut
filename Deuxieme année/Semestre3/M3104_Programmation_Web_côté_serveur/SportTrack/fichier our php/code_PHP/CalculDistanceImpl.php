<?php
    require_once('CalculDistance.php');
    class CalculDistanceImpl implements CalculDistance {
       
        /**
         * Retourne la distance en mètres entre 2 points GPS exprimés en degrés.
         * @param float $lat1 Latitude du premier point GPS
         * @param float $long1 Longitude du premier point GPS
         * @param float $lat2 Latitude du second point GPS
         * @param float $long2 Longitude du second point GPS
         * @return float La distance entre les deux points GPS
         */
        public function calculDistance2PointsGPS($lat1, $long1, $lat2, $long2){
            $R = 6378.137;
            $lat1r = (M_PI * $lat1) / 180;
            $lat2r = (M_PI * $lat2) / 180;
            $long1r = (M_PI * $long1) / 180;
            $long2r = (M_PI * $long2) / 180;
      
            $distance = $R*acos(sin($lat2r)*sin($lat1r)+cos($lat2r)*cos($lat1r)*cos($long2r-$long1r));
            return $distance;
        }

        /**
         * Retourne la distance en metres du parcours passé en paramètres. Le parcours est
         * défini par un tableau ordonné de points GPS.
         * @param Array $parcours Le tableau contenant les points GPS
         * @return float La distance du parcours
         */
        public function calculDistanceTrajet(Array $parcours){
            $dist = 0;
            for ($i=0 ; $i<count($parcours)-1 ; $i++) {
                $lat1 = $parcours[$i]['latitude'];
                $long1 = $parcours[$i]['longitude'];
                $lat2 = $parcours[$i+1]['latitude'];
                $long2 = $parcours[$i +1]['longitude'];
                $dist+=$this->calculDistance2PointsGPS($lat1, $long1, $lat2, $long2);
            }
            return $dist;
        }
    }
?>