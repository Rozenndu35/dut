<?php
 class Donnees{
   private $idDonnees;
   private $temps;
   private $description;
   private $fc;
   private $altitude;
   private $latitude;
   private $longitude;
   private $lActivite;

   public function  __construct() { }
   public function init($id, $t, $d, $f, $a, $la, $lo, $lA){
     $this->idDonnees = $id;
     $this->temps = $t;
     $this->description = $d;
     $this->fc = $f;
     $this->altitude= $a;
     $this->latitude= $la;
     $this->longitude = $lo;
     $this->lActivite = $lA;
   }

   public function getID(){ return $this->idDonnees; }
   public function getTemps(){ return $this->temps; }
   public function getDescription(){ return $this->description; }
   public function getFC(){ return $this->fc; }
   public function getAltitude(){ return $this->altitude; }
   public function getLatitude(){ return $this->latitude; }
   public function getLongitude(){ return $this->longitude; }
   public function getLActivite(){ return $this->lActivite; }
   public function  __toString() { return $this->idDonnees. " ". $this->temps. " ". $this->description. " ". $this->fc. " ". $this->altitude. " ". $this->latitude. " ". $this->longitude; }
  }
?>
