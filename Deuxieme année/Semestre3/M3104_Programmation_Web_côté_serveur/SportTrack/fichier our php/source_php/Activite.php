<?php

	class Activite {
	   private $idAct;
	   private $dateAct;
	   private $description;
	   private $distance;
	   private $heureD;
	   private $heureF;
	   private $fcMax;
	   private $fcMin;
	   private $fcMoyenne;
	   private $leCompte;

	   public function  __construct() { }
	   public function init($id,$date,$desc,$dist,$hdep,$hfin,$fcMaxi,$fcMini,$fcMoy,$compte){
		 $this->idAct = $id;
		 $this->dateAct = $date;
		 $this->description = $desc;
		 $this->distance = $dist;
		 $this->heureD = $hdep;
		 $this->heureF = $hfin;
		 $this->fcMax = $fcMaxi;
		 $this->fcMin = $fcMini;
		 $this->fcMoyenne = $fcMoy;
		 $this->leCompte = $compte;
	   }

	   public function getIdAct(){ return $this->idAct; }
	   public function getDate(){ return $this->dateAct; }
	   public function getDescription(){ return $this->description; }
	   public function getDistance(){ return $this->distance; }
	   public function getHeureD(){ return $this->heureD; }
	   public function getHeureF(){ return $this->heureF; }
	   public function getFcMax(){ return $this->fcMax; }
	   public function getFcMin(){ return $this->fcMin; }
	   public function getFcMoy(){ return $this->fcMoyenne; }
	   public function getCompte(){ return $this->leCompte; }
	   
	   public function  __toString() { return $this->idAct. " ". $this->dateAct. " ". $this->description. " ". $this->distance. " ". $this->heureD. " ". $this->heureF. " ". $this->fcMax. " ". $this->fcMin. " ". $this->fcMoyenne; }
	  }
?>
