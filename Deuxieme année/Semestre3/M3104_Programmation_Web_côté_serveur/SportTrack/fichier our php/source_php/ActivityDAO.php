<?php
require_once('SqliteConnection.php');
class ActivityDAO {
    private static $dao;

    private function __construct() {}

    public final static function getInstance() {
       if(!isset(self::$dao)) {
           self::$dao= new ActivityDAO();
       }
       return self::$dao;
    }

    public final function findAll(){
       $dbc = SqliteConnection::getInstance()->getConnection();
       $query = "select * from Activite order by leCompte";
       $stmt = $dbc->query($query);
       $results = $stmt->fetchALL(PDO::FETCH_CLASS, 'Activite');
       return $results;
    }

   public final function insert($activite){
      if($activite instanceof Activite){
         $dbc = SqliteConnection::getInstance()->getConnection();
         // prepare the SQL statement
         $query = "insert into Activite(idAct, dateAct, description, distance, heureD, heureF, fcMax, fcMin, fcMoyenne, leCompte) values (:id, :date, :desc, :dist, :hdep, :hfin, :fcMaxi, :fcMini, :fcMoy, :compte)";
         $stmt = $dbc->prepare($query);

         // bind the paramaters
         $stmt->bindValue(':id',$activite->getIdAct(),PDO::PARAM_STR);
         $stmt->bindValue(':date',$activite->getDate(),PDO::PARAM_STR);
         $stmt->bindValue(':desc',$activite->getDescription(),PDO::PARAM_STR);
         $stmt->bindValue(':dist',$activite->getDistance(),PDO::PARAM_STR);
         $stmt->bindValue(':hdep',$activite->getHeureD(),PDO::PARAM_STR);
         $stmt->bindValue(':hfin',$activite->getHeureF(),PDO::PARAM_STR);
         $stmt->bindValue(':fcMaxi',$activite->getFcMax(),PDO::PARAM_STR);
         $stmt->bindValue(':fcMini',$activite->getFcMin(),PDO::PARAM_STR);
         $stmt->bindValue(':fcMoy',$activite->getFcMoy(),PDO::PARAM_STR);
         $stmt->bindValue(':compte',$activite->getCompte(),PDO::PARAM_STR);

         // execute the prepared statement
         $stmt->execute();
       }
   }

  public function delete($activite){ 
    if($activite instanceof Activite){
        $dbc = SqliteConnection::getInstance()->getConnection();

        // bind the paramaters
        $stmt->bindValue(':id',$activite->getIdAct(),PDO::PARAM_STR);

        // delete
        $delete =$dbc->exec("delete FROM Activite WHERE idAct =:stmt");
        $delete->execute();
    }
   }

  public function update($obj){
    if($activite instanceof Activite){
        $dbc = SqliteConnection::getInstance()->getConnection();

        // bind the paramaters
        $ligne->bindValue(':id',$activite->getIdAct(),PDO::PARAM_STR);
        $stmt->bindValue(':id',$activite->getIdAct(),PDO::PARAM_STR);
        $stmt->bindValue(':date',$activite->getDate(),PDO::PARAM_STR);
        $stmt->bindValue(':desc',$activite->getDescription(),PDO::PARAM_STR);
        $stmt->bindValue(':dist',$activite->getDistance(),PDO::PARAM_STR);
        $stmt->bindValue(':hdep',$activite->getHeureD(),PDO::PARAM_STR);
        $stmt->bindValue(':hfin',$activite->getHeureF(),PDO::PARAM_STR);
        $stmt->bindValue(':fcMaxi',$activite->getFcMax(),PDO::PARAM_STR);
        $stmt->bindValue(':fcMini',$activite->getFcMin(),PDO::PARAM_STR);
        $stmt->bindValue(':fcMoy',$activite->getFcMoy(),PDO::PARAM_STR);
        $stmt->bindValue(':compte',$activite->getCompte(),PDO::PARAM_STR);


        // delete
        $delete =$dbc->exec("UPDATE Activite SET (:id, :date, :desc, :dist, :hdep, :hfin, :fcMaxi, :fcMini, :fcMoy, :compte) WHERE idAct =:ligne");
        $delete->execute();
    }
  }
  public final function findUser($user){
    $dbc = SqliteConnection::getInstance()->getConnection();
    $query = "select * from Activite WHERE leCompte = :user";
    $stmt = $dbc->query($query);
    $results = $stmt->fetchALL(PDO::FETCH_CLASS, 'Activite');
    return $results;
 }
}
?>
