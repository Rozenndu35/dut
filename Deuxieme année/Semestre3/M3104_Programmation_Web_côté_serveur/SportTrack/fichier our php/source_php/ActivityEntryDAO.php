<?php
require_once('SqliteConnection.php');
class ActivityEntryDAO {
    private static $dao;

    private function __construct() {}

    public final static function getInstance() {
       if(!isset(self::$dao)) {
           self::$dao= new ActivityEntryDAO();
       }
       return self::$dao;
    }

   public final function insert($donnee){
      if($donnee instanceof Donneee){
         $dbc = SqliteConnection::getInstance()->getConnection();
         // prepare the SQL statement
         $query = "insert into Donee(idDonnees, temps, description, fc, altitude, latitude, longitude, lActivite) values (:id, :t, :d, :f, :a, :la, :lo, :lA)";
         $stmt = $dbc->prepare($query);

         // bind the paramaters
         $stmt->bindValue(':id',$donnee->getID(),PDO::PARAM_STR);
         $stmt->bindValue(':t',$donnee->getTemps(),PDO::PARAM_STR);
         $stmt->bindValue(':d',$donnee->getDescription(),PDO::PARAM_STR);
         $stmt->bindValue(':f',$donnee->getFC(),PDO::PARAM_STR);
         $stmt->bindValue(':a',$donnee->getAltitude(),PDO::PARAM_STR);
         $stmt->bindValue(':la',$donnee->getLatitude(),PDO::PARAM_STR);
         $stmt->bindValue(':lo',$donnee->getLongitude(),PDO::PARAM_STR);
         $stmt->bindValue(':lA',$donnee->getLActivite(),PDO::PARAM_STR);

         // execute the prepared statement
         $stmt->execute();
       }
   }

  public function delete($donnee){ 
    if($donnee instanceof Donnee){
        $dbc = SqliteConnection::getInstance()->getConnection();

        // bind the paramaters
        $stmt->bindValue(':id',$donnee->getId(),PDO::PARAM_STR);

        // delete
        $delete =$dbc->exec("delete FROM Donnee WHERE idDonnee =:stmt");
        $delete->execute();
    }
   }

  public function update($obj){
    if($donnee instanceof Donnnee){
        $dbc = SqliteConnection::getInstance()->getConnection();

        // bind the paramaters
        $ligne->bindValue(':id',$donnee->getID(),PDO::PARAM_STR);
        $stmt->bindValue(':id',$donnee->getID(),PDO::PARAM_STR);
         $stmt->bindValue(':t',$donnee->getTemps(),PDO::PARAM_STR);
         $stmt->bindValue(':d',$donnee->getDescription(),PDO::PARAM_STR);
         $stmt->bindValue(':f',$donnee->getFC(),PDO::PARAM_STR);
         $stmt->bindValue(':a',$donnee->getAltitude(),PDO::PARAM_STR);
         $stmt->bindValue(':la',$donnee->getLatitude(),PDO::PARAM_STR);
         $stmt->bindValue(':lo',$donnee->getLongitude(),PDO::PARAM_STR);
         $stmt->bindValue(':lA',$donnee->getLActivite(),PDO::PARAM_STR);


        // delete
        $delete =$dbc->exec("UPDATE Donnee SET (:id, :t, :d, :f, :a, :la, :lo, :lA) WHERE id =:ligne");
        $delete->execute();
    }
  }
  public final function findActivity($activite){
    $dbc = SqliteConnection::getInstance()->getConnection();
    $query = "select * from Donneee WHERE lActivite = :activite";
    $stmt = $dbc->query($query);
    $results = $stmt->fetchALL(PDO::FETCH_CLASS, 'Donnee');
    return $results;
 }
}
?>
