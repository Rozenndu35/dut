<?php
ini_set('display_errors', 'On');
        error_reporting(E_ALL);

require_once("Activite.php");
require_once("Compte.php");
require_once("ActivityDAO.php");
require_once("ActivityEntryDAO.php");
require_once("UserDAO.php");

function testUserDAO() {
    $dao = UserDAO::getInstance();
    $users = $dao->findAll();
    foreach ($users as $user) {
        echo $user-> __toString()."\n";
    }

    $updatedUser = $dao->findUser(138);
    $dao->update($updatedUser);

    $UserById = $dao->findUser(49);
    echo $UserById-> __toString(). "\n";

    $deletedUser = new UserDAO();
    $dao->delete($deletedUser);

    
}

function testActivityDAO() {
    $dao = ActivityDAO::getInstance();
    $activitys = $dao->findAll();
    foreach ($activitys as $activity) {
        echo $activity-> __toString()."\n";
    }

    $updatedActivity = $dao->findActivity(138);
    $dao->update($updatedActivity);

    $ActivityById = $dao->findActivity(49);
    echo $ActivityById-> __toString(). "\n";

    $deletedActivity = new ActivityDAO();
    $dao->delete($deletedActivity);

    
}

function testActivityEntryDAO() {
    $dao = ActivityEntryDAO::getInstance();
    $acts = $dao->findAll();
    foreach ($acts as $act) {
        echo $act-> __toString()."\n";
    }

    $updatedAct = $dao->findActivity(138);
    $dao->update($updatedAct);

    $ActById = $dao->findActivity(49);
    echo $ActById-> __toString(). "\n";

    $deletedAct = new ActivityDAO();
    $dao->delete($deletedAct);

    
}

testUserDAO();
testActivityDAO();
testActivityEntryDAO();

?>

