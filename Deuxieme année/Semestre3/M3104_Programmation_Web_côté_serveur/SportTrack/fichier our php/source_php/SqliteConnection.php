<?php

    ini_set('display_errors', 'On');
    error_reporting(E_ALL);

    class SqliteConnection{
        private static $instance;
        private $connection;
        
        public function __construct(){
			try {
				$this->connection = new PDO ('sqlite:'.__DIR__.'/../db/sport_track.db');
				$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
			catch (PDOException $exception){
				echo $exception->getMessage();
			}
		}
			
        public function getConnection(){
			return $this->connection;
        }
        
        public final static function getInstance() {
			if (!isset(self::$instance)) {
				self::$instance=new SqliteConnection();
			}
			return self::$instance;
		}
    }
?>
