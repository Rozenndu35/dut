<?php
require_once('SqliteConnection.php');
class UserDAO {
    private static $dao;

    private function __construct() {}

    public final static function getInstance() {
       if(!isset(self::$dao)) {
           self::$dao= new UserDAO();
       }
       return self::$dao;
    }

    public final function findAll(){
       $dbc = SqliteConnection::getInstance()->getConnection();
       $query = "select * from Compte order by nom,prenom";
       $stmt = $dbc->query($query);
       $results = $stmt->fetchALL(PDO::FETCH_CLASS, 'Compte');
       return $results;
    }

   public final function insert($compte){
      if($compte instanceof Compte){
         $dbc = SqliteConnection::getInstance()->getConnection();
         // prepare the SQL statement
         $query = "insert into Compte(idCompte, adresseMail, motPass, nom, prenom, detaN, sexe, taille, poids) values (:id, :adresse, :motP, :n, :pre, :date, :s, :t, :poi)";
         $stmt = $dbc->prepare($query);

         // bind the paramaters
         $stmt->bindValue(':id',$compte->getID(),PDO::PARAM_STR);
         $stmt->bindValue(':adresse',$compte->getAdresseMail(),PDO::PARAM_STR);
         $stmt->bindValue(':motP',$compte->getMotPass(),PDO::PARAM_STR);
         $stmt->bindValue(':n',$compte->getNom(),PDO::PARAM_STR);
         $stmt->bindValue(':pre',$compte->getPrenom(),PDO::PARAM_STR);
         $stmt->bindValue(':date',$compte->getDateN(),PDO::PARAM_STR);
         $stmt->bindValue(':s',$compte->getSexe(),PDO::PARAM_STR);
         $stmt->bindValue(':t',$compte->getTaille(),PDO::PARAM_STR);
         $stmt->bindValue(':poi',$compte->getPoids(),PDO::PARAM_STR);

         // execute the prepared statement
         $stmt->execute();
       }
   }

  public function delete($compte){ 
    if($compte instanceof Compte){
        $dbc = SqliteConnection::getInstance()->getConnection();

        // bind the paramaters
        $stmt->bindValue(':id',$compte->getID(),PDO::PARAM_STR);

        // delete
        $delete =$dbc->exec("delete FROM Compte WHERE idCompte =:stmt");
        $delete->execute();
    }
   }

  public function update($obj){
    if($compte instanceof Compte){
        $dbc = SqliteConnection::getInstance()->getConnection();

        // bind the paramaters
        $ligne->bindValue(':id',$compte->getID(),PDO::PARAM_STR);
        $stmt->bindValue(':id',$compte->getID(),PDO::PARAM_STR);
        $stmt->bindValue(':adresse',$compte->getAdresseMail(),PDO::PARAM_STR);
        $stmt->bindValue(':motP',$compte->getMotPass(),PDO::PARAM_STR);
        $stmt->bindValue(':n',$compte->getNom(),PDO::PARAM_STR);
        $stmt->bindValue(':pre',$compte->getPrenom(),PDO::PARAM_STR);
        $stmt->bindValue(':dat',$compte->getDateN(),PDO::PARAM_STR);
        $stmt->bindValue(':s',$compte->getSexe(),PDO::PARAM_STR);
        $stmt->bindValue(':t',$compte->getTaille(),PDO::PARAM_STR);
        $stmt->bindValue(':poi',$compte->getPoids(),PDO::PARAM_STR);

        // delete
        $delete =$dbc->exec("UPDATE Compte SET (:id, :adresse, :motP, :n, :pre, :dat, :s, :t, :poi) WHERE idCompte =:ligne");
        $delete->execute();
    }
  }
}
?>
