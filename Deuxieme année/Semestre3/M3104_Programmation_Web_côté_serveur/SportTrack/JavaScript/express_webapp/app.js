var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var localStorage = require('localStorage');
localStorage.setItem('connexion', ' ');


var indexRouter = require('./routes/index'); //le controleur de la page d'acceuil
var accueilRouter = require('./routes/accueil'); //le controleur de la page d'acceuil
var usersInscriptionRouter = require('./routes/usersForm'); //le controleur du formulaire d'inscription
var usersRouter = require('./routes/users'); //le controleur recupere les information du formulaire
var usersConnexionRouter = require('./routes/connexionForm'); //le controleur du formulaire de connection
var usersConnexionVerifRouter = require('./routes/connexion'); //le controleur qui recupere les info de la connection
var usersDeconnexionRouter = require('./routes/deconnexion'); //le controleur de deconnection
var usersUploadFormRouter = require('./routes/uploadForm'); //le controleur de du formulaire pour ajouter un fichier
var usersUploadRouter = require('./routes/upload'); //le controleur recupere le fichier client et le traite
var usersAfficherRouter = require('./routes/afficher'); //le controleur qui affiche les activites
var usersUpdateFormRouter = require('./routes/updateForm'); //le controleur qui affiche le formulaire de modif des info
var usersUpdateRouter = require('./routes/update'); //le controleur qui recupere les informations

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/accueil', accueilRouter);
app.use('/usersForm', usersInscriptionRouter);
app.use('/users', usersRouter);
app.use('/connexionForm', usersConnexionRouter);
app.use('/connexion', usersConnexionVerifRouter);
app.use('/deconnection', usersDeconnexionRouter);
app.use('/uploadForm', usersUploadFormRouter);
app.use('/upload', usersUploadRouter);
app.use('/affiche', usersAfficherRouter);
app.use('/updateForm', usersUpdateFormRouter);
app.use('/update', usersUpdateRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
