var express = require('express');
var localStorage = require('localStorage');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  if(localStorage.getItem('connexion') != ' '){
    res.render('accueil', { title: '' });
  }
  else{
    res.render('index', { title: '' });
  }
});

module.exports = router;
