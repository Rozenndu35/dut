var express = require('express');
var localStorage = require('localStorage');
var router = express.Router();
var multi = require('multer');
var upload = multer({dest: 'uploads/'});
var jsonfile = require("jsonfile");
var activities_dao = require('sport-track-db').activity_dao;
var donnees_dao = require('sport-track-db').activity_enrty_dao;

/* GET home page. */
router.post('/', function(req, res, next) {
  if(localStorage.getItem('connexion') != ' '){
    //recupere le fichier
    let doc = req.files.uploadFile.data;
    let json = JSON.parse(doc);

    //recupere l'activite
    let act = json.activity;
    //recupere les données
    let data = json.data;
    
    //recupere l'id de l'activite
    activities_dao.findAll(function(err,rows){
      if(err){
          console.log(err.message);
      }else{
        var idAct =rows.length ;
        //inseres les données lignes par lignes
        for (let i = 0 ; i<data.lenght;i++){
          //recupere l'id
          donnees_dao.findAll(function(err,rows){
            if(err){
                console.log(err.message);
            }else{
              var id = rows.length;
              var temps = data[i]["time"];
              var desc = data[i]["des"];
              var fc = data[i]["cardio_frequency"];
              var alti = data[i]["latitude"];
              var lat = data[i]["longitude"];
              var lon = data[i]["altitude"];

              activityEntry_dao.insert ([id, temps,desc,fc,alti,lat,lon,idAct] , (err)=>console.log(err));
            }
          });
        } 
        //inserer l'activite
        var date = act[i]["date"];
        var descr = act[i]["description"];
        var dist = null;
        var heureD = null;
        var heurF = null;
        var duree = null;
        var fcMax = null;
        var fcMin = null;
        var fcMoyen = null;
        var email = req.body.email; 
        user_dao.findByMail(email,function(err,rows){
            if(rows.toString()!=""){
              //on regarde l'id Compte
              var leCompte = rows[0]['idCompte'];
              activities_dao.insert ([idAct, date,descr,dist,heureD,heurF,duree,fcMax,fcMin, fcMoyen, fcCompte] , (err)=>console.log(err));
              //affiche la page des activitees
              res.render('activities', { title: 'Vous avez ajouter une activités' }); 
            }
        });
      }
    });
  }else{
    res.render('connexionForm', { title: "Vous n'ete pas connecter" })
  }
}

module.exports = router;