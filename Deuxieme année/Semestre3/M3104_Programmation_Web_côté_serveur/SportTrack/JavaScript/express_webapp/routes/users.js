

   var express = require('express');
   var router = express.Router();
   var user_dao = require('sport-track-db').user_dao;
   
   
   router.post('/', function(req, res, next) {
        //regarde si l'inscription est possible
        var email = req.body.email;      
        user_dao.findByMail(email,function(err,rows){
            if(rows.toString()==""){
                //on l'ajoute
                //recupere l'identifiant
                user_dao.findAll(function(err,rows){
                    if(err){
                        console.log(err.message);
                    }else{
                        var id = rows.length;
                        //initilisation des valeurs(depuis le formulaire)
                        var nom = req.body.nom;
                        var prenom = req.body.prenom;
                        var date = req.body.naissance;
                        var sexe = req.body.sexe;
                        var taille = req.body.taille;
                        var poids = req.body.poids;
                        var motPasse = req.body.motPasse;

                        //insertion de l'utilisateur
                        user_dao.insert([id, email, motPasse, nom ,  prenom, date, sexe , taille,  poids], (err)=>{
                            if(err == null){
                                res.render('connexionForm');
                            }else{
                                console.log(err.message);
                            }
                        });
                    }
                });
            }else{
               //un utilisateur possede deja cette adresse mail ou il y a eu une erreur
               res.render('usersForm', { title: "addresse deja utiliser" });
            }
        });
        
   });
   module.exports = router;
