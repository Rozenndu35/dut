var express = require('express');
var user_dao = require("sport-track-db").user_dao;
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('connexionForm', { title: '' });
});

module.exports = router;