var express = require('express');
var localStorage = require('localStorage');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  if(localStorage.getItem('connexion') != ' '){
    res.render('activities', { title: '' });
  }
  else{
    res.render('connexionForm', { title: "Vous n'ete pas connecter" })
  }
  
});

module.exports = router;