var db = require('./sqlite_connection');
var ActivityEntryDAO = function(){

    /**
     * values : le tableau de valeur a inserer
     * callback : pour le message d'erreur
     */
    this.insert = function(values, callback){
        let stmt = db.prepare("INSERT INTO Donnees VALUES (?,?,?,?,?,?,?,?)");
        stmt.run([values[0],values[1], values[2], values[3], values[4], values[5], values[6], values[7]], (err)=>{
            if(err == null){
                console.log("insere");
            }else{
                console.log(err.message);
            }
        });
    }; 

    /**
     * key : la cle d'identification
     * values : le tableau de valeur a modifier
     * callback : pour le message d'erreur
     */
    this.update = function(key, values, callback){
        let stmt = db.prepare("UPDATE Donnees SET temps =?, description = ?, fc = ?, altitude = ?, latitude = ?, longitude = ?, lActivite = ? WHERE idDonnees=?");
        stmt.run([values[0],values[1], values[2], values[3], values[4], values[5], values[6], values[7]], function(err,rows){
            if(err == null){
                console.log("modifier");
            }
            else{
                callback(err);
            }
        });
    };




     /**
     * key : la cle d'identification
     * callback : pour le message d'erreur
     */
    this.delete = function(key, callback){
        db.run("DELETE FROM Donnees WHERE idDonnees=?",key, function(err,rows){
            if(err == null){
                console.log("supprimer");
            }
            else{
                callback(err);
            }
        });
    };
     /**
     * callback : pour le message d'erreur
     */
    this.findAll = function(callback){
        db.all("SELECT * FROM Donnees",callback);
    };
     /**
     * key : la cle d'identification
     * callback : pour le message d'erreur
     */
    this.findByActivity = function(activite, callback){
        db.all("SELECT * FROM Donnees WHERE lActivite=?",activite, function(err,rows){
            if(err){
                console.log(err.message);
            }else{
                callback(rows);
            }
        });
    };
};
var dao = new ActivityEntryDAO();
module.exports = dao;



