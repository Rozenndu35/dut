var db = require('./sqlite_connection');
var activity_dao = require('./sport-track-db').activity_dao;

var ActivityDAO = function(){

    /**
     * values : le tableau de valeur a inserer
     * callback : pour le message d'erreur
     */
    this.insert = function(values, callback){
        let stmt = db.prepare("INSERT INTO Activite VALUES (?,?,?,?,?,?,?,?,?,?,?)");
        stmt.run([values[0],values[1], values[2], values[3], values[4], values[5], values[6], values[7], values[8], values[9] ,values[10]], (err)=>{
            if(err == null){
                console.log("insere");
            }else{
                console.log(err.message);
            }
        });
    }; 

    /**
     * key : la cle d'identification
     * values : le tableau de valeur a modifier
     * callback : pour le message d'erreur
     */
    this.update = function(key, values, callback){
        let stmt = db.prepare("UPDATE Activite SET dateAct =?, description = ?, distance = ?, heureD = ?, heureF = ?, duree = ?, fcMax = ?, fcMin = ?, fcMoyenne = ?, leCompte = ? WHERE idAct=?");
        stmt.run([values[1], values[2], values[3], values[4], values[5], values[6], values[7], values[8], values[9] ,values[10],key],function(err,rows){
            if(err == null){
                console.log("modifier");
            }
            else{
                callback(err);
            }
        });
    };

     /**
     * key : la cle d'identification
     * callback : pour le message d'erreur
     */
    this.delete = function(key, callback){
        db.run("DELETE FROM Activite WHERE idAct=?", key, function(err,rows){
            if(err == null){
                console.log("supprimer");
            }
            else{
                callback(err);
            }
        });
    };
     /**
     * callback : pour le message d'erreur
     */
    this.findAll = function(callback){
        db.all("SELECT * FROM Activite",callback);
    };
     /**
     * key : la cle d'identification
     * callback : pour le message d'erreur
     */
    this.findByUser = function(user, callback){
        db.all("SELECT * FROM Activite WHERE leCompte=?", user, function(err,rows){
            if(err){
                console.log(err.message);
            }else{
                callback(rows);
            }
        });
    };
};
var dao = new ActivityDAO();
module.exports = dao;
