
public interface FilePriorite<E>{
	/**
	 * add an item at the top of the stack
	 * @param nouveau the new item
	 */
	public void ajout(E nouveau , int prioriter);
	
	/**
	 * returns the element add first
	 * @return the first element
	 */
	public E valeur();
	
	/**
	 * delete the oldest value
	 * @return the oldest value of the file
	 */
	public E suppression();
	
	/**
	 * check if the battery is empty
	 * @return true if there is no element and the pile exist
	 */
	public boolean estvide();
	
}
