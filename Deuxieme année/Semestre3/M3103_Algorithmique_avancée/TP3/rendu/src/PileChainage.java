import java.util.Arrays;

/**
 * The class for a pile with chainage
 * @author rozenn
 *
 * @param <E>
 */
public class PileChainage<E> implements Pile<E> {
	Cellule<E> element;
	/**
	 *
	 * @param <E>
	 */
	public class Cellule<E> {
		E valeur;
		Cellule<E> precedent;

		/**
		 * 
		 * @param valeur
		 * @param suivant
		 */
		public Cellule(E valeur , Cellule<E> precedent) {
			this.valeur = valeur;
			this.precedent = precedent;
		}
		/**
		 * 
		 * @return
		 */
		public E getValeur() {
			return valeur;
		}
		/**
		 * 
		 * @param valeur
		 */
		public void setValeur(E valeur) {
			this.valeur = valeur;
		}
		/**
		 * 
		 * @return
		 */
		public Cellule<E> getPrecedent() {
			return precedent;
		}
		/**
		 * 
		 * @param suivant
		 */
		public void setPrecedent(Cellule<E> precedent) {
			this.precedent = precedent;
		}
	}
	
	/**
	 * The constructor
	 * @param nouveau the new element
	 */
	public PileChainage(E nouveau) {
		this.element = (PileChainage<E>.Cellule<E>) nouveau;
	}
	
	
	/**
	 * add an item at the top of the stack
	 * @param nouveau the new item
	 * @throws CollectionPleineException 
	 */
	public void ajout(E nouveau) throws CollectionPleineException {
		//rentre si i est valide
		if(this.element!= null) {
			Cellule nouvelle = new Cellule(nouveau ,this.element);
			this.element = nouvelle;
			
		}else {
			System.err.println("PileChainage : ajout : Sorry the list is null ");
		}
		
	}

	/**
	 * get an item at the top of the stack
	 * @return the element in the top
	 */
	public E sommet() {
		E ret = null;
		if (this.element != null) {
			ret = this.element.getValeur();
		}else {
			System.err.println("Sorry but the pile is invalibel");
		}
		return null;
	}

	/**
	 * delete an item at the top of the stack
	 * @return the item that has just been removed
	 */
	public E suppression() {
		E ret = null;
		if(this.element != null) {
			ret = this.element.getValeur();
			this.element = this.element.getPrecedent();
		}
		return null;
	}

	/**
	 * check if the battery is empty
	 * @return true if there is no element and the pile exist
	 */
	public boolean estvide() {
		boolean ret = true;
		if(this.element.getValeur() != null || this.element.getPrecedent() != null ) {
			ret=false;
		}		
		return ret;
	}
	
	/**
	 * sprint the table (using in the class test to check
	 */
	public String toString() {
		String ret = "null" ;
		if(this.element != null) {
			ret = "valeur : " + this.element.getValeur() + " element : "+ this.element.getPrecedent();
		}
		return ret;
	}
}
