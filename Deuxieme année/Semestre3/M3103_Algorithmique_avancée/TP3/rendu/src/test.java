/**
 * the clas of the test
 * @author rozenn
 */

import java.util.Scanner;

public class test {
	/**
	 * The launcher for the test
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Veuillez saisir l'exercice :");
		String str = sc.nextLine();
		if(str.equals("2")) {
			testPileChainage();
		}else if(str.equals("3")) {
			testPileTableau();
		}else if(str.equals("6")) {
			testFileChainage();
		}else if(str.equals("7")) {
			testFileTableau();
		}else if(str.equals("8")) {
			testFilePrio();
		}else {
			System.out.print("cette exercice ne possede pas de test ou n'existe pas");
		}
	}
	
	/**
	 * test for the class PileChainage
	 */
	public static void testPileChainage() {
		//PileTableau(int n ) 
		int test = 0;
		int reussi = 0;
		test++;
		PileChainage testnull = new PileChainage(null);
		System.out.println(testnull.toString());
		if (testnull.toString().equals("null")) {
			reussi ++;
		}
		//test++;
		//PileChainage testOK = new PileChainage(new PileChainage.Cellule("valeur1",null));
		//System.out.println(testOK.toString());
		//if (testOK.toString().equals("null")) {
			//reussi ++;
		//}
		System.out.println("test pour PileChainage : PileChainage(Cellule element )  "+ reussi + "/" + test);
		
	}
	/**
	 * test for the class PileTableau
	 */
	public static void testPileTableau() {
		//PileTableau(int n ) 
		int test = 0;
		int reussi = 0;
		test ++;
		PileTableau pilenull = new PileTableau(-1 );
		if(pilenull.toString().equals("null")) {
			reussi ++;
		}
		test ++;
		PileTableau pile0 = new PileTableau(0 );
		if(pile0.toString().equals("[]")) {
			reussi ++;
		}
		test ++;
		PileTableau pile = new PileTableau(10 );
		if(pile.toString().equals("[null, null, null, null, null, null, null, null, null, null]")) {
			reussi ++;
		}
		System.out.println("test pour PileTableau : PileTableau(int n )  "+ reussi + "/" + test);
		//ajout(E nouveau)
		test = 0;
		reussi = 0;
		test ++;
		try {
			pilenull.ajout("nouveau");
		} catch (CollectionPleineException e4) {}
		if(pilenull.sommet()==null) {
			reussi ++;
		}
		test ++;
		try {
			pile0.ajout("nouveau");
		} catch (CollectionPleineException e3) {}
		if(pile0.sommet()==null) {
			reussi ++;
		}
		
		test ++;
		try {
			pile.ajout("nouveau");
			if(pile.sommet().equals("nouveau")) {
				reussi ++;
			}
		} catch (CollectionPleineException e2) {}
		
		test ++;
		PileTableau pile2 = new PileTableau(1 );
		try {
			pile2.ajout("nouveau");
			pile2.ajout("essai");
		} catch (CollectionPleineException e1) {
			if(pile2.sommet().equals("nouveau")) {
				reussi ++;
			}
		}
		
		
		System.out.println("test pour PileTableau : ajout(E nouveau)  "+ reussi + "/" + test);
		//sommet()
		test = 0;
		reussi = 0;
		test ++;
		if(pilenull.sommet()==null) {
			reussi ++;
		}
		test ++;
		if(pile0.sommet()==null) {
			reussi ++;
		}
		test ++;
		if(pile.sommet().equals("nouveau")) {
			reussi ++;
		}
		System.out.println("test pour PileTableau : sommet()  "+ reussi + "/" + test);
		//suppression()
		test = 0;
		reussi = 0;
		test ++;
		if(pilenull.suppression()==null) {
			reussi ++;
		}
		test ++;
		if(pile0.suppression()==null) {
			reussi ++;
		}
		test ++;
		if(pile.suppression().equals("nouveau")) {
			reussi ++;
		}
		System.out.println("test pour PileTableau : suppression()  "+ reussi + "/" + test);
		//estvide()
		test = 0;
		reussi = 0;
		test ++;
		if(pilenull.estvide()==false) {
			reussi ++;
		}
		test ++;
		if(pile0.estvide()==true) {
			reussi ++;
		}
		test ++;
		if(pile.estvide()== true) {
			reussi ++;
		}
		test ++;
		try {
			pile.ajout("test");
			if(pile.estvide()== false) {
				reussi ++;
			}
		} catch (CollectionPleineException e) {}
		
		System.out.println("test pour PileTableau : estvide()  "+ reussi + "/" + test);
	}
	
	private static void testFileChainage() {
		// TODO Auto-generated method stub
		
	}
	
	private static void testFileTableau() {
		//FileTableau(int n ) 
		int test = 0;
		int reussi = 0;
		test ++;
		FileTableau filenull = new FileTableau(-1 );
		if(filenull.toString().equals("null")) {
			reussi ++;
		}
		test ++;
		FileTableau file0 = new FileTableau(0 );
		if(file0.toString().equals("[]")) {
			reussi ++;
		}
		test ++;
		FileTableau file = new FileTableau(10 );
		if(file.toString().equals("[null, null, null, null, null, null, null, null, null, null]")) {
			reussi ++;
		}
		System.out.println("test pour FileTableau : FileTableau(int n )  "+ reussi + "/" + test);
		//ajout(E nouveau) 
		test = 0;
		reussi = 0;

		System.out.println("Ne doit pas ajouter :" );
		try {
			filenull.ajout("nouveau");
		} catch (CollectionPleineException e4) {}
		
		System.out.println("Ne doit pas ajouter :" );
		try {
			file0.ajout("nouveau");
		} catch (CollectionPleineException e3) {}
		
		System.out.println("Doit ajouter :" );
		try {
			file.ajout("nouveau");
		} catch (CollectionPleineException e2) {}
		
		test ++;
		FileTableau file2 = new FileTableau(1 );
		try {
			System.out.println("Doit ajouter une fois et etre rempli:" );
			file2.ajout("nouveau");
			file2.ajout("essai");
		} catch (CollectionPleineException e1) {
		}
		
		//E valeur()
		test = 0;
		reussi = 0;
		test ++;
		if(filenull.valeur()==null) {
			reussi ++;
		}
		test ++;
		if(file0.valeur()==null) {
			reussi ++;
		}
		test ++;
		if(file.valeur().equals("nouveau")) {
			reussi ++;
		}
		System.out.println("test pour FileTableau : valeur()  "+ reussi + "/" + test);
		//E suppression()
		test = 0;
		reussi = 0;
		test ++;
		if(filenull.suppression()==null) {
			reussi ++;
		}
		test ++;
		if(file0.suppression()==null) {
			reussi ++;
		}
		test ++;
		if(file.suppression().equals("nouveau")) {
			reussi ++;
		}
		System.out.println("test pour FileTableau : suppression()  "+ reussi + "/" + test);
		//estvide()
		test = 0;
		reussi = 0;
		test ++;
		if(filenull.estvide()==false) {
			reussi ++;
		}
		test ++;
		if(file0.estvide()==true) {
			reussi ++;
		}
		test ++;
		if(file.estvide()== true) {
			reussi ++;
		}
		test ++;
		try {
			file.ajout("test");
			if(file.estvide()== false) {
				
				reussi ++;
			}
		} catch (CollectionPleineException e) {}
		
		System.out.println("test pour FileTableau : estvide()  "+ reussi + "/" + test);
				
				
		
	}
	
	private static void testFilePrio() {
		// TODO Auto-generated method stub
		
	}
	
}