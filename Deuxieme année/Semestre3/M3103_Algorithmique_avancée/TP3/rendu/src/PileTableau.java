import java.util.Arrays;

public class PileTableau<E> implements Pile<E> {
	private E[] pile;
	private int point;
	
	/**
	 *  the constructor
	 */
	public PileTableau(int n ) {
		if (n>=0) {
			this.pile = (E[])new Object[n];
			this.point = -1;
		}else {
			System.err.println("PileChainage : PileChainage : Sorry but the length can't be negative");
			this.pile =  null;
			this.point = -1;
		}
	}
	/**
	 * add an item at the top of the stack
	 * @param nouveau the new item
	 */
	public void ajout(E nouveau) throws  CollectionPleineException{
		
		if(this.pile != null) {
			this.point ++;
			if( nouveau != null && this.point<pile.length) {
				this.pile[this.point]= nouveau;
			}else {
				this.point --;
				throw new CollectionPleineException();
			}
		}
		else {
			System.err.println("PileChainage : ajout : Sorry but the pile dont exist");
		}
		
		

	}

	/**
	 * get an item at the top of the stack
	 * @return the element in the top
	 */
	public E sommet() {
		E element = null ;
		if(this.pile != null && this.point<this.pile.length && this.point>-1) {
			element = this.pile[this.point];
		}
		else {
			System.err.println("PileChainage : sommet : Sorry but the pile dont exist");
		}
		return element;
	}

	/**
	 * delete an item at the top of the stack
	 * @return the item that has just been removed
	 */
	public E suppression() {
		E element = null ;
		if(this.pile != null && this.point<this.pile.length && this.point>-1) {
			element = this.pile[this.point];
			this.pile[this.point] = null;
			this.point --;
		}
		else {
			System.err.println("PileChainage : suppression : Sorry but the pile dont exist");
		}
		return element;
	}

	/**
	 * check if the battery is empty
	 * @return true if there is no element and the pile exist
	 */
	public boolean estvide() {
		boolean vide = false ;
		if(this.pile != null && this.point == -1) {
			vide = true;
		}
		else {
			System.err.println("PileChainage : estvide : Sorry but the pile dont exist");
		}
		return vide;
	}
	/**
	 * sprint the table (using in the class test to check
	 */
	public String toString() {
		String ret = "null" ;
		if(this.pile != null) {
			ret = Arrays.toString(this.pile);
		}
		return ret;
	}

}
