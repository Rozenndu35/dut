
/**
 * the exeption when the pile is full
 * @author rozenn
 */
public class CollectionPleineException extends Exception {
	public CollectionPleineException(){
		System.out.println("the pile is full");
	}  
}
