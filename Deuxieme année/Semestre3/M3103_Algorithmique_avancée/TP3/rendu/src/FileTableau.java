import java.util.Arrays;

public class FileTableau<E> implements File<E> {
	private E[] file; 
	private int tete; // le plus recent
	private int fin;// le plus vieux
	
	/**
	 * the constructor
	 */
	public FileTableau(int n ) {
		if (n>=0) {
			this.file = (E[])new Object[n];
			this.tete = 0;
			this.fin = 0;
		}else {
			System.err.println("FileTableau : FileTableau : Sorry but the length can't be negative");
			this.tete = -1;
			this.fin = -1;
			this.file =  null;
		}
	}

	/**
	 * add an item at the top of the stack
	 * @param nouveau the new item
	 * @throws CollectionPleineException 
	 */
	public void ajout(E nouveau) throws CollectionPleineException {
		if (this.file != null && nouveau != null && this.file.length != 0) {
			if(this.file[this.tete] != null && this.tete == this.fin){
				throw new CollectionPleineException();
			}else {
				if(this.tete == this.file.length-1) {
					this.fin =0;
					this.file[this.fin] = nouveau;
					System.out.println("ajouter ");
				}else {
					this.file[this.fin] = nouveau;
					this.fin ++;
					System.out.println("ajouter");
				}
			}
		}else {
			System.err.println("FileTableau : ajout : Sorry but the table is invalibel");
		}
	}

	/**
	 * returns the element add first
	 * @return the first element
	 */
	public E valeur() {
		E ret = null;
		if (this.file != null && this.file.length!= 0) {
			this.fin --;
			ret = this.file[this.fin];
			this.fin ++;
		}else {
			System.err.println("FileTableau : estvide : Sorry but the table is invalibel");
		}
		return ret;
	}

	/**
	 * delete the oldest value
	 * @return the oldest value of the file
	 */
	public E suppression() {
		E ret =null;
		if (this.file != null && !estvide() && this.file.length != 0) {
			ret = this.file[this.tete];
			this.file[this.tete] = null;
			if(this.tete == this.file.length-1) {
				this.tete =0;
			}else {
				this.tete ++;
			}
		}else {
			System.err.println("FileTableau : suppression : Sorry but the table is invalibel or is empty");
		}
		return ret;
	}

	/**
	 * check if the battery is empty
	 * @return true if there is no element and the pile exist
	 */
	public boolean estvide() {
		boolean ret = false;
		if (this.file != null ) {
			if(this.file.length == 0){
				ret = true;
			}else if( this.file[this.tete] == null && this.tete == this.fin) {
				ret=true;
			}
		}else {
			System.err.println("FileTableau : estvide : Sorry but the table is invalibel");
		}
		return ret;
	}
	/**
	 * sprint the table (using in the class test to check
	 */
	public String toString() {
		String ret = "null" ;
		if(this.file != null) {
			ret = Arrays.toString(this.file);
		}
		return ret;
	}
	

}
