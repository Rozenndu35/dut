
/**
 * The interface for the piles
 * @author rozenn
 * @param <E> 
 */
public interface Pile<E> {
	
	/**
	 * add an item at the top of the stack
	 * @param nouveau the new item
	 * @throws CollectionPleineException 
	 */
	public void ajout(E nouveau) throws CollectionPleineException;
	
	/**
	 * get an item at the top of the stack
	 * @return the element in the top
	 */
	public E sommet();
	
	/**
	 * delete an item at the top of the stack
	 * @return the item that has just been removed
	 */
	public E suppression();
	
	/**
	 * check if the battery is empty
	 * @return true if there is no element and the pile exist
	 */
	public boolean estvide();
}
	
