/**
 * The class for a pile with chainage
 * 
 * @author rozenn
 *
 * @param <E>
 */
public class FileChainage<E> implements File<E> {
	Cellule<E> element;
	/**
	 *
	 * @param <E>
	 */
	public class Cellule<E> {
		E valeur;
		Cellule<E> precedent;

		/**
		 * 
		 * @param valeur
		 * @param suivant
		 */
		public Cellule(E valeur , Cellule<E> precedent) {
			this.valeur = valeur;
			this.precedent = precedent;
		}
		/**
		 * 
		 * @return
		 */
		public E getValeur() {
			return valeur;
		}
		/**
		 * 
		 * @param valeur
		 */
		public void setValeur(E valeur) {
			this.valeur = valeur;
		}
		/**
		 * 
		 * @return
		 */
		public Cellule<E> getPrecedent() {
			return precedent;
		}
		/**
		 * 
		 * @param suivant
		 */
		public void setPrecedent(Cellule<E> precedent) {
			this.precedent = precedent;
		}
	}
	
	/**
	 * The constructor
	 * @param nouveau the new element
	 */
	public FileChainage(E nouveau) {
		this.element = (FileChainage<E>.Cellule<E>) nouveau;
	}

	/**
	 * add an item at the top of the stack
	 * @param nouveau the new item
	 */
	public void ajout(E nouveau) {
		//rentre si i est valide
		if(this.element!= null) {
			Cellule nouvelle = new Cellule(nouveau ,this.element);
			this.element = nouvelle;
			
		}else {
			System.err.println("FileChainage : ajout : Sorry the list is null ");
		}
		
	}

	/**
	 * returns the element add first
	 * @return the first element
	 */
	public E valeur() {
		E ret = null;
		Cellule cellule = this.element;
		if (this.element != null && !estvide()) {
			while(cellule.getPrecedent() != null) {
				cellule = cellule.getPrecedent();
			}
			ret = (E) cellule.getValeur();
		}
		return ret;
	}

	/**
	 * delete the oldest value
	 * @return the oldest value of the file
	 */
	public E suppression() {
		E ret = null;
		Cellule cellule = this.element;
		Cellule suivant = this.element;
		if (this.element != null && !estvide()) {
			while(cellule.getPrecedent() != null) {
				suivant = cellule;
				cellule = cellule.getPrecedent();
			}
			ret = (E) cellule.getValeur();
			suivant.setPrecedent(null);
		}
		return ret;
	}

	/**
	 * check if the battery is empty
	 * @return true if there is no element and the pile exist
	 */
	public boolean estvide() {
		boolean ret = true;
		if(this.element.getValeur() != null || this.element.getPrecedent() != null ) {
			ret=false;
		}		
		return ret;
	}
	

}
