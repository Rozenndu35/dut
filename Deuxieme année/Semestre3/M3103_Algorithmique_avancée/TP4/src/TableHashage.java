import java.util.Arrays;

/**
 * 
 * @author rozenn
 *
 */
public class TableHashage {
	int[] tab;
	
	
	/**
	 * the constructor
	 * @param n the length of the table
	 */
	public TableHashage(int n) {
		if(n>-1) {
			this.tab = new int[n];
			Arrays.fill(this.tab, -1);
		}else {
			this.tab = null;
			System.err.println("TableHashage : TableHashageInt : Sorry but the length can't be negative");
		}
	}
	/**
	 * add the element in the table
	 * @param element the element to want add
	 */
	public void ajout(int element) {
		
		if ( this.tab!=null && this.tab.length != 0 && element >-1 ) {
			boolean mis = false;
			for(int i = 0; i<this.tab.length && !mis ; i++) {
				if (this.tab[i]==-1) {
					this.tab[i]= element;
					mis = true;
					//System.out.println(" ajouter");
				}
			}			
			
		}else {
			System.err.println("TableHashage : ajout : Sorry but the element can't be negative or the table id invaled");
		}
	}
	
	/**
	 * delete the element in the table
	 * @param element the element to want delete
	 */
	public void suppression(int element) {
		if ( this.tab!=null && element > -1) {
			for(int i = 0; i<this.tab.length; i++) {
				if (this.tab[i]== element) {
					this.tab[i] = -1;
				}
			}
		}else {
			System.err.println("TableHashage : ajout : Sorry but the table id invaled the element is not negatif");
		}
	}
	/**
	 * shearch the element in the table
	 * @param element the element to shearch
	 * @return true if the element is in the table
	 */
	public boolean existe(int element) {
		boolean ret = false;
		if ( this.tab!=null && this.tab.length != 0  && element >-1) {
			for(int i = 0; i<this.tab.length && !ret ; i++) {
				if (this.tab[i]== element) {
					ret = true;
				}
			}
		}else {
			System.err.println("TableHashage : ajout : Sorry but the table id invaled the element is not negatif");
		}
		return ret;
		
	}
	/**
	 * calculate the density of the table
	 * @return the density
	 */
	public double densite() {
		double ret = 0;
		if (this.tab != null && this.tab.length != 0) {
			int rempli = 0;
			for (int i = 0; i<tab.length; i++) {
				if (tab[i]!= -1) {
					rempli ++;
				}
			}
			ret = (double)rempli/this.tab.length;
		}
		return ret;
		
	}
	
	
	/**
	 * 
	 * @return
	 */
	public int index(){
		int ret = -1;
		
		return ret;
		
	}
	
	/**
	 * sprint the table (using in the class test to check
	 */
	public String toString() {
		String ret = "null" ;
		if(this.tab != null) {
			ret = Arrays.toString(this.tab);
		}
		return ret;
	}
}
