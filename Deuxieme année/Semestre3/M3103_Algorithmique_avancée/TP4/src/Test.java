/**
 * the clas of the test
 * @author rozenn
 */

import java.util.Arrays;
import java.util.Scanner;

public class Test {
	/**
	 * The launcher for the test
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Veuillez saisir l'exercice :");
		String str = sc.nextLine();
		if(str.equals("1")) {
			testTable();
		}else if(str.equals("2")) {
			testTableSort();
			testEfficaciter();
		}else if(str.equals("3")) {
			testTableHashageInt();
		}else {
			System.out.print("cette exercice ne possede pas de test ou n'existe pas");
		}
	}
	
	





	/**
	 * test for the class Table
	 */
	public static void testTable() {
		int test = 0;
		int reussi = 0;
		//test de Table
		test ++;
		Table tablenull = new Table(-1 );
		if(tablenull.toString().equals("null")) {
			reussi ++;
		}
		test ++;
		Table table0 = new Table(0 );
		if(table0.toString().equals("[]")) {
			reussi ++;
		}
		test ++;
		Table tableOK = new Table(10);
		if(tableOK.toString().equals("[null, null, null, null, null, null, null, null, null, null]")) {
			reussi ++;
		}
		System.out.println("test pour Table : Table(int n ) "+ reussi + "/" + test);
		//test de ajout
		
		test =0;
		reussi=0;
		test++;
		tablenull.ajout("cle", "valeur");
		if(!tablenull.existe("cle") && tablenull.valeur("cle") == null) {
			reussi ++;
		}
		test++;
		table0.ajout("cle", "valeur");
		if(!table0.existe("cle") && table0.valeur("cle") == null) {
			reussi ++;
		}
		test++;
		tableOK.ajout("cle", "valeur");
		if(tableOK.existe("cle") && tableOK.valeur("cle").equals("valeur")) {
			reussi++;
		}
		System.out.println("test pour Table : ajout( C cle , V valeur ) "+ reussi + "/" + test);
		//test de existe
		test =0;
		reussi=0;
		test++;
		if(!tablenull.existe("cle")) {
			reussi ++;
		}
		test++;
		if(!table0.existe("cle")) {
			reussi ++;
		}
		test++;
		if(tableOK.existe("cle")) {
			reussi ++;
		}
		test++;
		if(!tableOK.existe("erreur") ) {
			reussi ++;
		}
		System.out.println("test pour Table : existe( C cle ) "+ reussi + "/" + test);
		//test de valeur
		test =0;
		reussi=0;
		
		test++;
		if(tablenull.valeur("cle") == null) {
			reussi ++;
		}
		test++;
		if(table0.valeur("cle") == null) {
			reussi ++;
		}
		test++;
		if(tableOK.valeur("cle").equals("valeur")) {
			reussi ++;
		}
		test++;
		if(tableOK.valeur("erreur") == null ) {
			reussi ++;
		}
		System.out.println("test pour Table : valeur( C cle ) "+ reussi + "/" + test);
		//test de suppression
		test =0;
		reussi=0;
		
		tablenull.suppression("cle");
		table0.suppression("cle");
		tableOK.suppression("erreur");
		test++;
		tableOK.suppression("cle");
		if(!tableOK.existe("cle")) {
			reussi ++;
		}
		
		System.out.println("test pour Table : suppression( C cle  ) "+ reussi + "/" + test);
		
		//test de estVide
		test =0;
		reussi=0;
		test++;
		if(!tablenull.estvide()) {
			reussi ++;
		}
		test++;
		if(table0.estvide()) {
			reussi ++;
		}
		test++;
		if(tableOK.estvide()) {
			reussi ++;
		}
		test++;
		tableOK.ajout("cle", "valeur");
		if(!tableOK.estvide() ) {
			reussi ++;
		}
		System.out.println("test pour Table : estvide( ) "+ reussi + "/" + test);
	}
	/**
	 * test for the class TableSort
	 */
	public static void testTableSort() {
		int test = 0;
		int reussi = 0;
		//test de Table
		test ++;
		TableSort tablenull = new TableSort(-1 );
		if(tablenull.toString().equals("null")) {
			reussi ++;
		}
		test ++;
		TableSort table0 = new TableSort(0 );
		if(table0.toString().equals("[]")) {
			reussi ++;
		}
		test ++;
		TableSort tableOK = new TableSort(10);
		if(tableOK.toString().equals("[null, null, null, null, null, null, null, null, null, null]")) {
			reussi ++;
		}
		System.out.println("test pour Table : Table(int n ) "+ reussi + "/" + test);
		//test de ajout 
		
		test =0;
		reussi=0;
		test++;
		tablenull.ajout(0, "valeur");
		if(!tablenull.existe(0) && tablenull.valeur(0) == null) {
			reussi ++;
			
		}
		test++;
		table0.ajout(0, "valeur");
		if(!table0.existe(0) && table0.valeur(0) == null) {
			reussi ++;
			
		}
		test++;
		tableOK.ajout(0, "valeur");
		if(tableOK.existe(0) && tableOK.valeur(0).equals("valeur")) {
			reussi++;
			
		}
		System.out.println("test pour Table : ajout( int cle , V valeur ) "+ reussi + "/" + test);
		
		//test de existe
		test =0;
		reussi=0;
		test++;
		if(!tablenull.existe(0)) {
			reussi ++;
		}
		test++;
		if(!table0.existe(0)) {
			reussi ++;
		}
		tableOK.ajout(1, "valeur");
		test++;
		if(tableOK.existe(1)) {
			reussi ++;
			
		}
		test++;
		if(!tableOK.existe(2) ) {
			reussi ++;
			
		}
		System.out.println("test pour Table : existe( int cle ) "+ reussi + "/" + test);
		
		//test de valeur
		test =0;
		reussi=0;
		
		test++;
		if(tablenull.valeur(0) == null) {
			reussi ++;
		}
		test++;
		if(table0.valeur(0) == null) {
			reussi ++;
		}
		test++;
		if(tableOK.valeur(0).equals("valeur")) {
			reussi ++;
		}
		test++;
		if(tableOK.valeur(2) == null ) {
			reussi ++;
		}
		System.out.println("test pour Table : valeur( int cle ) "+ reussi + "/" + test);
		//test de suppression
		test =0;
		reussi=0;
		
		tablenull.suppression(0);
		table0.suppression(0);
		tableOK.suppression(1);
		test++;
		tableOK.suppression(0);
		if(!tableOK.existe(0)) {
			reussi ++;
		}
		
		System.out.println("test pour Table : suppression( int cle  ) "+ reussi + "/" + test);
		
		//test de estVide
		test =0;
		reussi=0;
		test++;
		if(!tablenull.estvide()) {
			reussi ++;
		}
		test++;
		if(table0.estvide()) {
			reussi ++;
		}
		test++;
		tableOK.suppression(1);
		if(tableOK.estvide()) {
			reussi ++;
		}
		test++;
		
		tableOK.ajout(6, "valeur");
		if(!tableOK.estvide() ) {
			reussi ++;
		}
		System.out.println("test pour Table : estvide( ) "+ reussi + "/" + test);
		
	}
	/**
	 * test the efficiency between table and tableSort
	 */
	private static void testEfficaciter() {
		Table table = new Table(100000);
		table.ajout("essai", "valeur");
		table.ajout("essai2", "valeur");
		table.ajout("essai6", "valeur");
		table.ajout("essai7", "valeur");
		table.ajout("essai5", "valeur");
		table.ajout("essai10", "valeur");
		table.ajout("essai19", "valeur");
		table.ajout("essai189", "valeur");
		table.ajout("essai69", "valeur");
		table.ajout("essai26", "valeur");
		table.ajout("essai64", "valeur");
		table.ajout("essai79", "valeur");
		table.ajout("essai56", "valeur");
		table.ajout("essai106", "valeur");
		table.ajout("essai197", "valeur");
		table.ajout("essai1893", "valeur");
		long t1 = System.nanoTime();
		table.ajout("cle", "valeur");
		long t2 = System.nanoTime();
		long diffT= (t2 - t1);
		TableSort tablesort = new TableSort(100);
		table.ajout(0, "valeur");
		table.ajout(2, "valeur");
		table.ajout(6, "valeur");
		table.ajout(7, "valeur");
		table.ajout(5, "valeur");
		table.ajout(10, "valeur");
		table.ajout(19, "valeur");
		table.ajout(189, "valeur");
		table.ajout(50, "valeur");
		table.ajout(60, "valeur");
		table.ajout(45, "valeur");
		table.ajout(78, "valeur");
		table.ajout(8909, "valeur");
		table.ajout(12345, "valeur");
		
		long t1sort = System.nanoTime();
		tablesort.ajout(9, "valeur");
		long t2sort = System.nanoTime();
		long diffTsort= (t2 - t1);
		
		if (diffT < diffTsort) {
			System.out.println(" l'ajout pour un tableau non trier vas plus vite ");
		}else {
			System.out.println(" l'ajout pour un tableau trier vas plus vite ");
		}
		
		t1 = System.nanoTime();
		table.existe("cle");
		t2 = System.nanoTime();
		diffT= (t2 - t1);
			
		t1sort = System.nanoTime();
		tablesort.existe(9);
		t2sort = System.nanoTime();
		diffTsort= (t2 - t1);
		
		if (diffT < diffTsort) {
			System.out.println(" existe pour un tableau non trier vas plus vite ");
		}else {
			System.out.println(" existe pour un tableau trier vas plus vite ");
		}
		t1 = System.nanoTime();
		table.valeur("cle");
		t2 = System.nanoTime();
		diffT= (t2 - t1);
			
		t1sort = System.nanoTime();
		tablesort.valeur(9);
		t2sort = System.nanoTime();
		diffTsort= (t2 - t1);
		
		if (diffT < diffTsort) {
			System.out.println(" existe pour un tableau non trier vas plus vite ");
		}else {
			System.out.println(" valeur pour un tableau trier vas plus vite ");
		}
		t1 = System.nanoTime();
		table.suppression("cle");
		t2 = System.nanoTime();
		diffT= (t2 - t1);
			
		t1sort = System.nanoTime();
		tablesort.suppression(9);
		t2sort = System.nanoTime();
		diffTsort= (t2 - t1);
		
		if (diffT < diffTsort) {
			System.out.println("supprimer pour un tableau non trier vas plus vite ");
		}else {
			System.out.println(" supprimmer pour un tableau trier vas plus vite ");
		}
	}
	/**
	 * test for the class TableHashageInt
	 */
	private static void testTableHashageInt() {
		int test = 0;
		int reussi = 0;
		//test de TableHashageInt
		test ++;
		TableHashageInt tablenull = new TableHashageInt(-1 );
		if(tablenull.toString().equals("null")) {
			reussi ++;
		}
		test ++;
		TableHashageInt table0 = new TableHashageInt(0 );
		if(table0.toString().equals("[]")) {
			reussi ++;
		}
		test ++;
		TableHashageInt tableOK = new TableHashageInt(10);
		if(tableOK.toString().equals("[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]")) {
			reussi ++;
		}
		System.out.println("test pour TableHashageInt : TableHashageInt(int n ) "+ reussi + "/" + test);
		
		// test de ajout
		test =0;
		reussi=0;
		test++;
		tablenull.ajout(0);
		if(!tablenull.existe(0)) {
			reussi ++;
			
			
		}
		test++;
		table0.ajout(0);
		if(!table0.existe(0)) {
			reussi ++;
		}
		test++;
		tableOK.ajout(0);
		if(tableOK.existe(0)) {
			reussi++;
			
		}
		System.out.println("test pour  TableHashageInt : ajout( int element ) "+ reussi + "/" + test);
		
		//test de existe
		test =0;
		reussi=0;
		test++;
		if(!tablenull.existe(0)) {
			reussi ++;
		}
		test++;
		if(!table0.existe(0)) {
			reussi ++;
		}
		test++;
		if(tableOK.existe(0)) {
			reussi ++;				
		}
		test++;
		if(!tableOK.existe(1) ) {
			reussi ++;
			
		}
		System.out.println("test pour TableHashageInt : existe( int cle ) "+ reussi + "/" + test);
		
		//test de suppression
		test =0;
		reussi=0;
		
		tablenull.suppression(0);
		table0.suppression(0);
		tableOK.suppression(1);
		
		test++;
		tableOK.suppression(0);
		if(!tableOK.existe(0)) {
			reussi ++;
		}
		
		System.out.println("test pour TableHashageInt : suppression( int cle  ) "+ reussi + "/" + test);
		
		//test de dencite
		
		test =0;
		reussi=0;
		test++;
		if (tablenull.densite() == 0) {
			reussi ++;
		}
		test++;
		if (table0.densite() == 0) {
			reussi ++;
		}
		test++;
		if (tableOK.densite() == 0) {
			reussi ++;
		}
		test++;
		tableOK.ajout(0);
		tableOK.ajout(1);
		tableOK.ajout(2);
		tableOK.ajout(3);
		tableOK.ajout(4);
		
		if (tableOK.densite() == 0.5) {
			reussi ++;
		}
		
		
		System.out.println("test pour TableHashageInt : densite() "+ reussi + "/" + test);
	}
}