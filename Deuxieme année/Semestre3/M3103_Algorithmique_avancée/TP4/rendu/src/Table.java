import java.util.Arrays;

/**
 * @author rozenn
 * @param <C>
 * @param <V>
 */
public class Table<C, V> {
	private Association[] tab;
	/**
	 * @author rozenn
	 * @param <C>
	 * @param <V>
	 */
	public class Association<C, V> {
		private C cle ;
		private V valeur;
		public Association(C cle, V valeur) {
			this.cle = cle;
			this.valeur = valeur;
		}
		/**
		 * @return the cle
		 */
		public C getCle() {
			return cle;
		}
		/**
		 * @param cle the cle to set
		 */
		public void setCle(C cle) {
			this.cle = cle;
		}
		/**
		 * @return the valeur
		 */
		public V getValeur() {
			return valeur;
		}
		/**
		 * @param valeur the valeur to set
		 */
		public void setValeur(V valeur) {
			this.valeur = valeur;
		}
		

	}
	
	
	/**
	 * the constructor
	 * @param n : the length of the table
	 */
	public Table(int n) {
		if (n>=0) {
			this.tab = new Association[n];
			for (int i = 0; i<tab.length; i++) {
				this.tab[i]=null;
			}
		}else {
			System.err.println("Table : Table : Sorry but the length can't be negative");
			this.tab =  null;
		}
	}
	/**
	 * add a association on the table
	 * @param cle the key 
	 * @param valeur the value
	 */
	public void ajout(C cle, V valeur) {
		if (this.tab!=null) {
			boolean mis = false;
			for(int i = 0; i<this.tab.length && !mis; i++) {
				if(this.tab[i]==null) {
					mis=true;
					this.tab[i]= new Association(cle,valeur);
					//System.out.println("ajouter");
				}
			}
			if(!mis) {
				System.err.println("Table : ajout : Sorry but the table is full");
			}
		}else {
			System.err.println("Table : ajout : Sorry but the table is invalibel");
		}
	}
	/**
	 * sherch if the key is in the table
	 * @param cle the key
	 * @return true if the key exist
	 */
	public boolean existe(C cle) {
		boolean ret = false;
		if(this.tab!=null) {
			boolean trouver = false;
			for(int i = 0; i<this.tab.length && !trouver; i++) {
				if(this.tab[i] != null && this.tab[i].getCle()== cle) {
					trouver=true;
					ret =true;
					//System.out.println ("trouver");
				}
			}
		}else {
			System.err.println("Table : existe : Sorry but the table is invalibel");
		}
		return ret;
	}
	/**
	 * get the value with the key
	 * @param cle the key
	 * @return return the value if the key not exist return null
	 */
	public V valeur(C cle) {
		V ret = null;
		if (this.tab!=null) {
			boolean trouver = false;
			for(int i = 0; i<this.tab.length && !trouver; i++) {
				if(this.tab[i] != null && this.tab[i].getCle()== cle) {
					trouver=true;
					ret = (V) this.tab[i].getValeur();
				}
			}
		}else {
			System.err.println("Table : valeur : Sorry but the table is invalibel");
		}
		return ret;
		
	}
	/**
	 * delete the essociation whith the key
	 * @param cle the key
	 */
	public void suppression(C cle) {
		if (this.tab!=null) {
			boolean trouver = false;
			for(int i = 0; i<this.tab.length && !trouver; i++) {
				if(this.tab[i]!= null &&this.tab[i].getCle()== cle) {
					trouver=true;
					this.tab[i]=null;
				}
			}
			if(!trouver) {
				System.err.println("Table : suppression : Sorry but the key is not in the table");
			}
		}else {
			System.err.println("Table : suppression : Sorry but the table is invalibel");
		}
	}
	/**
	 * return true if the table is vide if the table is not valid return false
	 * @return true if the table is vide if the table is not valid return false
	 */
	public boolean estvide() {
		boolean ret = true;
		if (this.tab!=null) {
			for(int i = 0; i<this.tab.length && ret; i++) {
				if(this.tab[i]!= null ) {
					ret=false;
				}
			}
		}else {
			ret = false;
			System.err.println("Table : suppression : Sorry but the table is invalibel");
		}
		return ret;
		
	}
	/**
	 * sprint the table (using in the class test to check
	 */
	public String toString() {
		String ret = "null" ;
		if(this.tab != null) {
			ret = Arrays.toString(this.tab);
		}
		return ret;
	}

}
