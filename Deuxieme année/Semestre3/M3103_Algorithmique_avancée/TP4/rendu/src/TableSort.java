import java.util.Arrays;

/**
 * @author rozenn
 * @param <C>
 * @param <V>
 */
public class TableSort<C, V> {
	private Association[] tab;
	/**
	 * @author rozenn
	 * @param <C>
	 * @param <V>
	 */
	public class Association<C, V> {
		private int cle ;
		private V valeur;
		public Association(int cle, V valeur) {
			this.cle = cle;
			this.valeur = valeur;
		}
		/**
		 * @return the cle
		 */
		public int getCle() {
			return cle;
		}
		/**
		 * @param cle the cle to set
		 */
		public void setCle(int cle) {
			this.cle = cle;
		}
		/**
		 * @return the valeur
		 */
		public V getValeur() {
			return valeur;
		}
		/**
		 * @param valeur the valeur to set
		 */
		public void setValeur(V valeur) {
			this.valeur = valeur;
		}
		

	}
	
	
	/**
	 * the constructor
	 * @param n : the length of the table
	 */
	public TableSort(int n) {
		if (n>=0) {
			this.tab = new Association[n];
			for (int i = 0; i<tab.length; i++) {
				this.tab[i]=null;
			}
		}else {
			System.err.println("Table : Table : Sorry but the length can't be negative");
			this.tab =  null;
		}
	}
	/**
	 * add a association on the table
	 * @param cle the key 
	 * @param valeur the value
	 */
	public void ajout(int cle, V valeur) {
		if (this.tab!=null) {
			boolean mis = false;
			for(int i = 0; i<this.tab.length && !mis; i++) {
				if(this.tab[i]==null ) {
					mis=true;
					this.tab[i]= new Association(cle,valeur);
					Arrays.sort(this.tab, 0, i);
					//System.out.println("ajouter");
				}
			}
			if(!mis) {
				System.err.println("Table : ajout : Sorry but the table is full");
			}
		}else {
			System.err.println("Table : ajout : Sorry but the table is invalibel");
		}
	}
	/**
	 * sherch if the key is in the table
	 * @param cle the key
	 * @return true if the key exist
	 */
	public boolean existe(int cle) {
		 boolean ret = false;
		 if(this.tab!=null) {
			 int indD = 0;
			 int indM;
			 int indF = this.tab.length - 1;
			 while (indD !=indF){
				 //compte l'efficaciter cpt ++;
				 indM = (int) ((indD + indF)/2);
				 if(indM <this.tab.length -1 && this.tab[indM] == null ) {
					 indF = indM;
				 }
				 else if (indM <this.tab.length -1 && cle > this.tab[indM].getCle()){
					 indD = indM + 1;
				 }
				 else {
					 indF=indM;
				 }
			 }
			 if (indD <this.tab.length && this.tab[indD] != null && cle == this.tab[indD].getCle()) {
				 ret = true;
			 }
		 }else {
				System.err.println("Table : existe : Sorry but the table is invalibel");
		 }
		return ret;
	}
	/**
	 * get the value with the key
	 * @param cle the key
	 * @return return the value if the key not exist return null
	 */
	public V valeur(int cle) {
		 V ret = null;
		 if(this.tab!=null) {
			 int indD = 0;
			 int indM;
			 int indF = this.tab.length - 1;
			 while (indD !=indF){
				 //compte l'efficaciter cpt ++;
				 indM = (int) ((indD + indF)/2);
				 if(indM <this.tab.length -1 && this.tab[indM] == null ) {
					 indF = indM;
				 }
				 else if (indM <this.tab.length-1 && cle > this.tab[indM].getCle()){
					 indD = indM + 1;
				 }
				 else {
					 indF=indM;
				 }
			 }
			 if (indD <this.tab.length && this.tab[indD] != null && cle == this.tab[indD].getCle()) {
				 ret = (V) this.tab[indD].getValeur();
			 }
		 }else {
				System.err.println("Table : existe : Sorry but the table is invalibel");
		 }
		return ret;
		
	}
	/**
	 * delete the essociation whith the key
	 * @param cle the key
	 */
	public void suppression(int cle) {
		if (this.tab!=null) {
			boolean trouver = false;
			boolean fin= false;
			for(int i = 0; i<this.tab.length && !fin ; i++) {
				if(!trouver && this.tab[i]!= null && this.tab[i].getCle()== cle) {
					trouver=true;
					this.tab[i]=null;
					while(this.tab[i+1]!= null) {
						this.tab[i]=this.tab[i+1];
						i++;
					}
					this.tab[i]=null;
				}else {
					fin = true;
				}
				
			}
			if(!trouver) {
				System.err.println("Table : suppression : Sorry but the key is not in the table");
			}
		}else {
			System.err.println("Table : suppression : Sorry but the table is invalibel");
		}
	}
	/**
	 * return true if the table is vide if the table is not valid return false
	 * @return true if the table is vide if the table is not valid return false
	 */
	public boolean estvide() {
		boolean ret = true;
		if (this.tab!=null) {
			for(int i = 0; i<this.tab.length && ret; i++) {
				if(this.tab[i]!= null ) {
					ret=false;
				}
			}
		}else {
			ret = false;
			System.err.println("Table : suppression : Sorry but the table is invalibel");
		}
		return ret;
		
	}
	/**
	 * sprint the table (using in the class test to check
	 */
	public String toString() {
		String ret = "null" ;
		if(this.tab != null) {
			ret = Arrays.toString(this.tab);
		}
		return ret;
	}

}
