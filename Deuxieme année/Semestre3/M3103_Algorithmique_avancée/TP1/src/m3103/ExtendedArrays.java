package m3103;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * @author rozenn
 *
 */
/**
 * @author rozenn
 *
 */
public class ExtendedArrays {
	private static boolean debugMode = false;
	/**
	 * Returns true if the two specified arrays of ints are equal to one another. Two arrays are considered equal if both arrays contain the same number of elements, and all corresponding pairs of elements in the two arrays are equal.  Also, two array references are considered equal if both are null.
	 * @param t1 one array to be tested for equality
	 * @param t2 the other array to be tested for equality
	 * @return true if the two arrays are equal
	 */
	public boolean equals(int[] t1, int[] t2){
		boolean ret = false;
		 if (t1 != null && t2 != null){
			 int[] tab1 = t1;
				int[] tab2 = t2;
				boolean continu = true;
				if (tab1.length == tab2.length) {
					for(int i = 0; i<tab1.length && continu ; i++) {
						if (tab1[i] != tab2[i]) {
							continu = false;
						}
					}
				}
				else {
					continu =false;
				}
				if (continu) {
					ret = true;
				}
		}else if(t1 == null && t2 == null){
			ret = true;
		}
		return ret;
	}
	
	
	/**
	 * Returns true if the two specified arrays of Objects are equal to one another. The two arrays are considered equal if both arrays contain the same number of elements, and all corresponding pairs of elements in the two arrays are equal. Two objects are null.
	 * @param t1 one array to be tested for equality
	 * @param t2 the other array to be tested for equality
	 * @return true if the two arrays are equal
	 */
	public static boolean equals(Object[]t1, Object[] t2){
		boolean ret = false;
		if (t1 != null && t2 != null){
			Object[] tab1 = t1;
			Object[] tab2 = t2;
			boolean continu = true;
			if (tab1.length == tab2.length) {
				for(int i = 0; i<tab1.length && continu ; i++) {
					if (tab1[i] != tab2[i]) {
						continu = false;
					}
				}
			}
			else {
				continu =false;
			}
			if (continu) {
				ret = true;
			}
		}else if(t1 == null && t2 == null){
			ret = true;
		}
		return ret;
	}
	
	
	/**
	 * Returns true if the two specified arrays are deeply equal to one another. Two arrays are considered equally if they are null, or if they refer to arrays that contain the same number of elements and all of them are equal to one another.
	 * @param t1 one array to be tested for equality
	 * @param t2 the other array to be tested for equality
	 * @return true if the two arrays are equal
	 */
	public static boolean deepEquals(Object[] t1, Object[] t2){
		boolean ret = false;
	    if(t1 != null && t2 != null){
	      if(t1.length == t2.length){
	        ret = true;
	        for(int i = 0; i<t1.length && ret; i++){
	          if(!deepEquals(t1[i], t2[i])){
	            ret = false;
	          }
	        }
	      }
	    }
	    else if(t1 ==null && t2 == null) {
	    	ret= true;
	    }
	    return ret;
	}
	
	/**
	   * Tests the values equality of two instance.
	   * @param t1 one object
	   * @param t2 the other object
	   * @return true if the two object are equal
	   */
	  private static boolean deepEquals(Object o1, Object o2){
		  boolean ret = false;
		  if(o1==null && o2 == null) {
			  ret = true;
		  }
		  else if(o1 != null && o2 != null){
			  ret = o1.equals(o2);
		  }
		  return ret;
	  }
	  /**
	   * Tests the values equality of two instance.
	   * @param t1 one boolean
	   * @param t2 the other boolean
	   * @return true if the two boolean are equal
	   */
	  private static boolean deepEquals(boolean o1, boolean o2){
		  boolean ret = false;
		  if(o1==o2 ) {
			  ret = true;
		  }
		  return ret;
	  }
	  /**
	   * Tests the values equality of two instance. 
	   * @param t1 one int
	   * @param t2 the other int
	   * @return true if the two int are equal
	   */
	  private static boolean deepEquals(int o1, int o2){
		  boolean ret = false;
		  if(o1==o2 ) {
			  ret = true;
		  }
		 
		  return ret;
	  }
	  /**
	   * Tests the values equality of two instance.
	   * @param t1 one float
	   * @param t2 the other float
	   * @return true if the two float are equal
	   */
	  private static boolean deepEquals(float o1, float o2){
		  boolean ret = false;
		  if(o1==o2 ) {
			  ret = true;
		  }
		  return ret;
	  }
	  /**
	   * Tests the values equality of two instance. 
	   * @param t1 one double
	   * @param t2 the other double
	   * @return true if the two double are equal
	   */
	  private static boolean deepEquals(double o1, double o2){
		  boolean ret = false;
		  if(o1==o2 ) {
			  ret = true;
		  }
		  return ret;
	  }
	  /**
	   * Tests the values equality of two instance. 
	   * @param t1 one char
	   * @param t2 the other char
	   * @return true if the two char are equal
	   */
	  private static boolean deepEquals(char o1, char o2){
		  boolean ret = false;
		  if(o1==o2 ) {
			  ret = true;
		  }
		  return ret;
	  }
	/**
	 * reverse content of a table (the first element is in the last position, the second in penultimate and so on until the last element that is in first position)
	 * @param original  the table to reverse
	 */
	public void revert(int[] original) {
		if (original != null){
			int echange = original.length - 1;
			for (int i = 0; i < echange ; i++) {
				int stock = original[i];
				original[i] = original[echange];
				original[echange] = stock;
				echange --;
			}
		}
	}
	/**
	 * limit this inversion to the part of the table between the indices from and to
	 * @param original  the table to reverse
	 * @param from the first point when you change
	 * @param to the lasr point when you change
	 */
	public void revert(int[] original, int from, int to) {
		if (original != null && from>=0 && to<original.length && from<=to){
			int echange = to;
			
			for (int i = from; i < echange ; i++) {
				int stock = original[i];
				original[i] = original[echange];
				original[echange] = stock;
				echange --;
			}
			int i= 0;
		}
	}
	/**
	 * fill a table with numbers between min and max
	 * @param a the table to fill
	 * @param min the min not to depose
	 * @param max the max not to depose
	 */
	public void fillRandom(int[] a, int min, int max) {
		if (a != null){
			int[] tab = a;
			boolean ok = min<=max;
			for (int i =0; i< tab.length ; i++) {
				if (ok){
					tab[i] = min + (int)(Math.random()* ((max-min)+1));
				}
				else {
					tab[i] = 0;
				}
			}
		}
	}
	
	/**
	 * Returns a string representation of the contents of the specified array. The string representation consists of a list of the array's elements, enclosed in square brackets ("[]"). Adjacent elements are separated by the characters ", " (a comma followed by a space). Elements are converted to strings as by String.valueOf(int). Returns "null" if a is null.
	 * @param tab  the array whose string representation to return
	 * @return a string representation of tab
	 */
	public String toString(int[] tab) {
		
		String str = "null";
		if (tab != null) {
			str="[";
			for(int i=0; i < tab.length; i++){
				if(i== tab.length-1) {
					str+= Integer.toString(tab[i]);
				}
				else {
					str+= Integer.toString(tab[i]) + ", ";
				}
			}
			str+="]";
		}
		return str;
	}
	/**
	 * counts the number of occurrences of the variable in the array if the arrau is null return -1
	 * @param a the table
	 * @param val the value to search
	 * @return the number of occurrences of the variable
	 */
	public static int occurrences1 (int[] a, int val) {
		int nb=-1;
		if (a != null){
			nb=0;
			int[] tab = a;
			for (int i =0 ;i < tab.length ; i++){
				if (tab[i]== val){
					nb=nb+1;
				}
			}
		}
		return nb;
	}
	/**
	 * counts the number of occurrences of the variable in the array sort if the arrau is null return -1
	 * @param a the table
	 * @param val the value to search
	 * @return the number of occurrences of the variable
	 */
	public static int occurrences2 (int[] a, int val) {
		int nb=-1;
		if (a != null){
			nb=0;
			int[] tab = a;
			boolean fin = false;
			boolean trouver = false;
			for (int i =0 ; !fin ; i++) {
				if (tab[i] == val) {
					nb ++ ;
				}
				else if (tab[i]>val) {
					fin = true;
				}
			}
		}
		return nb;
	}
	/**
	 * counts the number of occurrences of the variable in the array if the arrau is null return -1 (sorting in a new table with Arrays.sort and use Arrays.binarySearch)
	 * @param a the table
	 * @param val the value to search
	 * @return the number of occurrences of the variable
	 */
	public static int occurrences3 (int[] a, int val) {
		int nb=-1;
		if (a != null){
			nb=0;
			Arrays.sort(a);
			int[] tab = a;
			int debut = Arrays.binarySearch(tab, val);
			boolean fin = false;
			if (debut>=0) {
				for (int i = debut ; !fin ; i++) {
					if (tab[i] == val) {
						nb ++ ;
					}
					else {
						fin = true;
					}
				}
			}
		}
		return nb;
	}
	

	/**
	 * counts the number of occurrences in a of each value between min and max
	 * @param a the table of value
	 * @param min the min
	 * @param max the max
	 * @return a table with the occurence between the min and the max
	 */
	public static int[] histogramme1(int[] a, int min, int max) {
		int[] ret =null;
		if(a!= null) {
			ret = new int[max- min +1];
			int j=0;
			for (int i = min; i<=max; i++) {
				ret[j]= occurrences1 (a, i);
				j++;
			}
		}
		return ret;
	}
	
	/**
	 * counts the number of occurrences in a of each value between min and max
	 * @param a the table of value
	 * @param min the min
	 * @param max the max
	 * @return a table with the occurence between the min and the max
	 */
	public static int[] histogramme2(int[] a, int min, int max) {
		int[] ret =null;
		if(a!= null) {
			int nb=0;
			int[] tab = a;
			ret = new int[max- min +1];
			int indice = 0;
			Arrays.sort(tab);
			int debut = Arrays.binarySearch(tab, min);
			for (int j = min; j<=max ; j++) {
				nb=0;
				boolean fin = false;
				for (int i =0 ; !fin ; i++) {
					if (tab[i] == j) {
						nb ++ ;
					}
					else if (tab[i]>j) {
						fin = true;
					}
				}
				ret[indice]= nb;
				indice ++;
			}
		}
		return ret;
	}
	
	/**
	 * search the first occurence in a table of a value
	 * @param a the table
	 * @param val the value to search
	 * @return the indice wher is the value 
	 */
	public static int search(int[] a, int val) {
		int ret= -1;
		if(a!= null) {
			boolean trouver =false;
			for (int i = 0; i < a.length && !trouver ; i++) {
				if (a[i] == val) {
					trouver = true;
					ret = i;
				}
			}
		}
		return ret;
	}
	/**
	 * Searches the specified array for the specified value using the binary search algorithm. 
	 * @param tab the array to be searched
	 * @param key the value to be searched for
	 * @return index of the search key, if it is contained in the array; otherwise -1
	 */
	public static int binarySearch(int[] tab, int key) {
		int ret = -1;
		if (tab!=null){
			int indD;
			int indM;
			int indF;
			indD = 0;
			indF = tab.length - 1;
			while (indD !=indF){
				indM = (int) ((indD + indF)/2);
				if (key > tab[indM]){
					indD = indM + 1;
				}
				else {
					indF=indM;
				}
			}
			if (key == tab [indD]) {
				ret = indD;
			}
			else {
				ret = -1;
			}
		}
		return ret;
	}
	
	/**
	 * return the min of the table
	 * @param a the table
	 * @return the index of the min
	 */
	public static int min( int [] a){
		int min= -1;
		int i;
		if (a!=null){
			i=0;
			min = a[i];
			while (i < a.length-1){
				if (a[i] > a[i+1] ){
					min = a[i+1];
				}
				i++;
			}
		} 
		return min;
	}
	/**
	 * return the max of the table
	 * @param a the table
	 * @return the index of the max
	 */
	public static int max( int [] a){
		int max = -1;
		int i;
		if (a!=null){
			i=0;
			max = a[i];
			while (i < a.length-1){
				if (a[i] < a[i+1] ){
					max = a[i+1];
				}
				i++;
			}
		}
		return max;
	}
	/**
	 * returns the k th smallest value of a (hence sounded (a, 0) is min (a) and sounded (a, length-1) is max (a)) if the table is null return -1
	 * @param a the table 
	 * @param k the k th
	 * @return the k th smallest value of a 
	 */
	public static int rang(int[] a, int k) {
		int ret = -1;
		if(a!= null) {
			if (k == 0) {
				ret = min(a);
			}
			else if (k == a.length-1) {
				ret = max(a);
			}
			else {
				ArrayList list = new ArrayList();
				
			}
		}
		return ret;
	}
}
