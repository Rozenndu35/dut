package m3103;

import java.util.Arrays;
import java.util.Scanner;

public class Test {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Veuillez saisir l'exercice :");
		String str = sc.nextLine();
		if(str.equals("1")) {
			test1();
			test2();
			test3();
		}
		else if(str.equals("2")) {
			test4();
			test5();
		}
		else if(str.equals("3")) {
			test6();
			test7();
		}
		else if(str.equals("4")) {
			test8();
			test9();
			test10();
		}
		else if(str.equals("5")) {
			test11();
			test12();
		}
		else if(str.equals("6")) {
			test13();
		}
		else if(str.equals("7")) {
			test14();
		}
		else if(str.equals("8")) {
			test15();
			test16();
		}
	}
	
	/**
	 * test for the function is equal with int
	 */
	public static void test1() {
		ExtendedArrays ea = new ExtendedArrays();
		int test= 0;
		int reussi = 0;
		int[] tab1 = null;
		int[] tab2 = null;
		int[] tab3 = {1,2};
		int[] tab4 = {1,2};
		int[] tab5 = {1,3};
		int[] tab6 = {1,3,4};
		int[] tab7 = tab3;
		test++;
		if (ea.equals(tab1, tab2)) {
			reussi++;
		}
		test++;
		if (!ea.equals(tab1, tab3)) {
			reussi++;
		}
		test++;
		if (ea.equals(tab3, tab4)) {
			reussi++;
		}
		test++;
		if (!ea.equals(tab3, tab5)) {
			reussi++;
		}
		test++;
		if (!ea.equals(tab3, tab6)) {
			reussi++;
		}
		test++;
		if (ea.equals(tab3, tab7)) {
			reussi++;
		}
		System.out.println("test pour equals(int[],int[]) "+ reussi + "/" + test);
	}
	
	/**
	 * test for the function is equal with Object
	 */
	public static void test2() {
		ExtendedArrays ea = new ExtendedArrays();
		int test= 0;
		int reussi = 0;
		Integer[] tabO1 = null;
		Integer[] tabO2 = null;
		Integer[] tabO3 = {1,2,3,4};
		Integer[] tabO4 = {1,2,3,4};
		Integer[] tabO5= {1,2,3,5};
		Integer[] tabO6 = {1,2,3,4,5,6};
		Integer[] tabO7 = tabO3;
		test++;
		if (ea.equals(tabO1, tabO2)) {
			reussi++;
		}
		test++;
		if (!ea.equals(tabO1, tabO3)) {
			reussi++;
		}
		test++;
		if (ea.equals(tabO3, tabO4)) {
			reussi++;
		}
		test++;
		if (!ea.equals(tabO3, tabO5)) {
			reussi++;
		}
		test++;
		if (!ea.equals(tabO3, tabO6)) {
			reussi++;
		}
		test++;
		if (ea.equals(tabO3, tabO7)) {
			reussi++;
		}
		System.out.println("test pour equals(Object[],Object[]) "+ reussi + "/" + test);
	}
	
	/**
	 * test for the function is deepEqual
	 */
	public static void test3() {
		ExtendedArrays ea = new ExtendedArrays();
		int test= 0;
		int reussi = 0;
		Integer[][] tabO1 = null;
		Integer[][] tabO2 = null;
		Integer[][] tabO3 = {{1,2},{3,4}};
		Integer[][] tabO4 = {{1,2},{3,4}};
		Integer[][] tabO5 = {{1,2},{3,5}};
		Integer[][] tabO6 = {{1,2},{3,4},{5,6}};
		Integer[][] tabO7 = tabO3;
		Integer[][] tabO8 = {{1,2},null};
		Integer[][] tabO9 = {{1,2},null};
		Integer[][] tabO10 = {null,null};
		Integer[][] tabO11 = {null,null};
		Object[][] tabO12 = {{new Object(),new Object()},{new Object(),new Object()}};
		Object[][] tabO13 = {{new Object(),new Object()},{new Object(),new Object()}};
		Object[][] tabO14 = {{new Object(),new Object()},null};
		Object[][] tabO15 = {{new Object(),new Object()},null};
		test++;
		if (ea.deepEquals(tabO1, tabO2) == Arrays.deepEquals(tabO1, tabO2)) {
			reussi++;
		}
		test++;
		if (ea.deepEquals(tabO1, tabO3) == Arrays.deepEquals(tabO1, tabO3)) {
			reussi++;
			
		}
		
		test++;
		if (ea.deepEquals(tabO3, tabO4) == Arrays.deepEquals(tabO3, tabO4)) {
			reussi++;
			
		}
		else {
			System.out.println("erreur : "+ ea.deepEquals(tabO3, tabO4)+ " pour tabO3 = {{1,2},{3,4}} tabO4 = {{1,2},{3,4}};");
			System.out.println("Pour arrays.deepEquals : " + Arrays.deepEquals(tabO3, tabO4));
		}
		test++;
		if (ea.deepEquals(tabO3, tabO5) == Arrays.deepEquals(tabO3, tabO5)) {
			reussi++;
		}
		test++;
		if (ea.deepEquals(tabO3, tabO6) == Arrays.deepEquals(tabO3, tabO6)) {
			reussi++;
		}
		test++;
		if (ea.deepEquals(tabO3, tabO7) == Arrays.deepEquals(tabO3, tabO7)) {
			reussi++;
		}
		test++;
		if (ea.deepEquals(tabO8, tabO9) == Arrays.deepEquals(tabO8, tabO9)) {
			reussi++;
		}else {
			System.out.println("erreur : "+ ea.deepEquals(tabO8, tabO9)+ " pour tabO8 = {{1,2},null} tabO9 = {{1,2},null}");
			System.out.println("Pour arrays.deepEquals : " + Arrays.deepEquals(tabO8, tabO9));
		}
		test++;
		if (ea.deepEquals(tabO10, tabO11) == Arrays.deepEquals(tabO10, tabO11)) {
			reussi++;
		}
		test++;
		if (ea.deepEquals(tabO12, tabO13) == Arrays.deepEquals(tabO12, tabO13)) {
			reussi++;
		}
		test++;
		if (ea.deepEquals(tabO14, tabO15) == Arrays.deepEquals(tabO14, tabO15)) {
			reussi++;
		}
		System.out.println("test pour deepEquals(Object[],Object[]) "+ reussi + "/" + test);
	}
	/**
	 * test for the function is revert
	 */
	public static void test4() {
		ExtendedArrays ea = new ExtendedArrays();
		int test= 0;
		int reussi = 0;
		int[] tab1 = null;
		int[] tab2 = {1,2,3,4};
		int[] tab3 = {1,2,3};
		int[] tab4 = {1};
		int[] tab11 = null;
		int[] tab22 = {4,3,2,1};
		int[] tab33 = {3,2,1};
		int[] tab44 = {1};
		ea.revert(tab1);
		test++;
		if(ea.equals(tab1,tab11)) {
			reussi++;
		}
		ea.revert(tab2);
		test++;
		if(ea.equals(tab2,tab22)) {
			reussi++;
		}
		ea.revert(tab3);
		test++;
		if(ea.equals(tab3,tab33)) {
			reussi++;
		}
		ea.revert(tab4);
		test++;
		if(ea.equals(tab4,tab44)) {
			reussi++;
		}
		System.out.println("test pour equals(Object[],Object[]) "+ reussi + "/" + test);
	}
	/**
	 * test for the function is revert(int[] original, int from, int to)
	 */
	public static void test5() {
		ExtendedArrays ea = new ExtendedArrays();
		int test= 0;
		int reussi = 0;
		int[] tab1 = null;
		int[] tab2 = {1,2,3,4,5,6,7,8,9,10};
		int[] tab3 = {1,2,3,4,5,6,7,8,9};
		int[] tab4 = {1,2};
		int[] tab5= {1,2,3,4};
		int[] tab6= {1,2,3,4};
		int[] tab7 = {1,2,3,4,5,6,7,8,9,10};
		int[] tab11 = null;
		int[] tab22 = {10,9,8,7,6,5,4,3,2,1};
		int[] tab33 = {1,2,3,6,5,4,7,8,9};
		int[] tab44 = {1,2};
		int[] tab55= {1,2,3,4};
		int[] tab66= {1,2,3,4};
		int[] tab77 = {1,2,3,4,5,6,7,8,9,10};
		ea.revert(tab1,1,3);
		test++;
		if(ea.equals(tab1,tab11)) {
			reussi++;
		}
		ea.revert(tab2,0,9);
		test++;
		if(ea.equals(tab2,tab22)) {
			reussi++;
		}
		ea.revert(tab3,3,5);
		test++;
		if(ea.equals(tab3,tab33)) {
			reussi++;
		}
		ea.revert(tab4,0,0);
		test++;
		if(ea.equals(tab4,tab44)) {
			reussi++;
		}
		ea.revert(tab5,-1,2);
		test++;
		if(ea.equals(tab5,tab5)) {
			reussi++;
		}
		ea.revert(tab6,1,15);
		test++;
		if(ea.equals(tab6,tab66)) {
			reussi++;
		}
		ea.revert(tab7,7,1);
		test++;
		if(ea.equals(tab7,tab77)) {
			reussi++;
		}
		System.out.println("test pour equals(Object[],Object[]) "+ reussi + "/" + test);
	}
	/**
	 * test for the function is fillRandom
	 */
	public static void test6() {
		int tab[] = new int[100];
		ExtendedArrays ea = new ExtendedArrays();
		ea.fillRandom(tab, -1000, 1000);
		System.out.println("Version avec une boucle : ");
		System.out.println("le tableau est : ");
		for(int i=0; i < tab.length; i++){
			System.out.println(tab[i]+ " ");
		}
		System.out.println("Version avec la classe arrays : ");
		System.out.println("le tableau est : ");
		System.out.println(Arrays.toString(tab)); 
	}
	/**
	 * test for the function is toString
	 */
	public static void test7() {
		int test= 0;
		int reussi = 0;
		ExtendedArrays ea = new ExtendedArrays();
		int[] tab1 = {1,2,3,4,5,6,7,8,9,10};
		String str1 = ea.toString(tab1);
		test++;
		if(str1.equals("[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]")) {
			reussi++;
		}
		int[] tab2 = null;
		test++;
		String str2 = ea.toString(tab2);
		if(str2.equals("null")) {
			reussi++;
		}
		System.out.println("test pour toString(int[] tab) "+ reussi + "/" + test);
	}
	/**
	 * test for the function is occurrences1
	 */
	public static void test8() {
		int test= 0;
		int reussi = 0;
		ExtendedArrays ea = new ExtendedArrays();
		int[] tab1 = {1,6,3,4,5,6,6,8,9,10};
		int[] tab2 = null;
		
		test++;
		if(ea.occurrences1(tab1, 6) == 3) {
			reussi++;
		}
		test++;
		if(ea.occurrences1(tab1, 0) == 0) {
			reussi++;
		}
		test++;
		if(ea.occurrences1(tab2, 0) == -1) {
			reussi++;
		}
		System.out.println("test pour occurrences1 (int[] a, int val) "+ reussi + "/" + test);
	}
	/**
	 * test for the function is occurrences2
	 */
	public static void test9() {
		int test= 0;
		int reussi = 0;
		ExtendedArrays ea = new ExtendedArrays();
		int[] tab1 = {1,2,3,4,6,6,6,8,9,10};
		int[] tab2 = null;
		
		test++;
		if(ea.occurrences1(tab1, 6) == 3) {
			reussi++;
		}
		test++;
		if(ea.occurrences1(tab1, 0) == 0) {
			reussi++;
		}
		test++;
		if(ea.occurrences1(tab2, 0) == -1) {
			reussi++;
		}
		System.out.println("test pour occurrences2 (int[] a, int val) "+ reussi + "/" + test);
	}
	/**
	 * test for the function is occurrences3
	 */
	public static void test10() {
		int test= 0;
		int reussi = 0;
		ExtendedArrays ea = new ExtendedArrays();
		int[] tab1 = {1,6,3,4,5,6,6,8,9,10};
		int[] tab2 = null;
		
		test++;
		if(ea.occurrences3(tab1, 6) == 3) {
			reussi++;
		}
		test++;
		if(ea.occurrences3(tab1, 0) == 0) {
			reussi++;
		}
		test++;
		if(ea.occurrences3(tab2, 0) == -1) {
			reussi++;
		}
		System.out.println("test pour occurrences3 (int[] a, int val) "+ reussi + "/" + test);
	}
	/**
	 * test for the function is histogramme1
	 */
	public static void test11() {
		int test= 0;
		int reussi = 0;
		ExtendedArrays ea = new ExtendedArrays();
		int[] tab1 = {1,6,3,4,5,6,6,8,9,10};
		int[] tabResult1 ={1,1,1,3,0,1};
		int[] tab2 = {1,6,4,10,11,1,4,5,6,6,8,9,10};
		int[] tabResult2 ={0,2,0,0,2,1,3,0,1};
		int[] tab3 = null;
		test++;
		int[] resultat = ea.histogramme1(tab1, 3,8);
		
		if(ea.equals(resultat,tabResult1)) {
			reussi++;
		}
		test++;
		resultat = ea.histogramme1(tab2, 0,8);
		if(ea.equals(resultat,tabResult2)) {
			reussi++;
		}
		test++;
		if(ea.histogramme1(tab3, 0,8) == null) {
			reussi++;
		}
		System.out.println("test pour histogramme1 (int[] a, int val) "+ reussi + "/" + test);
	}
	/**
	 * test for the function is histogramme2
	 */
	public static void test12() {
		int test= 0;
		int reussi = 0;
		ExtendedArrays ea = new ExtendedArrays();
		int[] tab1 = {1,6,3,4,5,6,6,8,9,10};
		int[] tabResult1 ={1,1,1,3,0,1};
		int[] tab2 = {1,6,4,10,11,1,4,5,6,6,8,9,10};
		int[] tabResult2 ={0,2,0,0,2,1,3,0,1};
		int[] tab3 = null;
		test++;
		int[] resultat = ea.histogramme2(tab1, 3,8);
		if(ea.equals(resultat,tabResult1)) {
			reussi++;
		}
		test++;
		resultat = ea.histogramme2(tab2, 0,8);
		if(ea.equals(resultat,tabResult2)) {
			reussi++;
		}
		test++;
		if(ea.histogramme2(tab3, 0,8) == null) {
			reussi++;
		}
		System.out.println("test pour histogramme2 (int[] a, int val) "+ reussi + "/" + test);
	}
	/**
	 * test for the function is search
	 */
	public static void test13() {
		int test= 0;
		int reussi = 0;
		ExtendedArrays ea = new ExtendedArrays();
		int[] tab1 = {1,2,3,4,5,6,7,8,9,10};
		int[] tab2 = {1,6,4,10,11,1,4,5,6,6,8,9,10};
		int[] tab3 = null;
		test++;
		if(ea.search(tab1, 1) == 0) {
			reussi++;
		}
		test++;
		if(ea.search(tab1, 50) == -1) {
			reussi++;
		}
		test++;
		if(ea.search(tab2, 6) == 1) {
			reussi++;
		}
		test ++;
		if(ea.search(tab3, 6) == -1) {
			reussi++;
		}
		System.out.println("test pour histogramme2 (int[] a, int val) "+ reussi + "/" + test);
	}
	/**
	 * test for the function is binarySearch
	 */
	public static void test14() {
		int test= 0;
		int reussi = 0;
		ExtendedArrays ea = new ExtendedArrays();
		int[] tab1 = {1,2,3,4,5,6,7,8,9,10};
		int[] tab2 = {1,4,6,6,8,9,10};
		int[] tab3 = null;
		test++;
		if(ea.binarySearch(tab1,1) == 0) {
			reussi++;
			
		}
		test++;
		if(ea.binarySearch(tab1, 50) == -1) {
			reussi++;
		}
		test++;
		if(ea.binarySearch(tab2, 6) == 2 ||ea.binarySearch(tab2, 6) == 3 ) {
			reussi++;
		}
		test ++;
		if(ea.binarySearch(tab3, 6) == -1) {
			reussi++;
		}
		System.out.println("test pour histogramme2 (int[] a, int val) "+ reussi + "/" + test);
		System.out.println("Comparaison avec celui d'array");
		System.out.println("	pour {1,4,6,6,8,9,10} recherche 6");
		if(ea.binarySearch(tab2, 6) == Arrays.binarySearch(tab2, 6)) {
			System.out.println("		C'est la meme reponse");
		}
		else {
			System.out.println("		celle creer : " + ea.binarySearch(tab2, 6) );
			System.out.println("		celle arrays : " + Arrays.binarySearch(tab2, 6) );
		}
		System.out.println("	pour  {1,2,3,4,5,6,7,8,9,10} recherche 1");
		if(ea.binarySearch(tab1,1) == Arrays.binarySearch(tab1,1)) {
			System.out.println("		C'est la meme reponse");
		}
		else {
			System.out.println("		celle creer : " + ea.binarySearch(tab1,1) );
			System.out.println("		celle arrays : " + Arrays.binarySearch(tab1,1) );
		}
		System.out.println("	pour  {1,2,3,4,5,6,7,8,9,10} recherche 15");
		if(ea.binarySearch(tab1,15) == Arrays.binarySearch(tab1,15)) {
			System.out.println("		C'est la meme reponse");
		}
		else {
			System.out.println("		celle creer : " + ea.binarySearch(tab1,15) );
			System.out.println("		celle arrays : " + Arrays.binarySearch(tab1,15) );
		}
		System.out.println("	pout un tableau nul");
		System.out.println("		celle creer : cela nous renvoi -1");
		System.out.println("		celle arrays : cela creer une erreur" );
	}
	/**
	 * test for the function is min
	 */
	public static void test15(){
		int test= 0;
		int reussi = 0;
		ExtendedArrays ea = new ExtendedArrays();
		int[] tab1 = {1,2,3,4,5,6,7,8,9,10};
		int[] tab2 = {1,4,6,6,6,1,9,10};
		int[] tab3 = null;
		test++;
		if(ea.min(tab1) == 1) {
			reussi++;
		}
		test++;
		if(ea.min(tab2) == 1) {
			reussi++;
		}
		test ++;
		if(ea.min(tab3) == -1) {
			reussi++;
		}
		System.out.println("test pour min( int [] a) "+ reussi + "/" + test);
	}
	/**
	 * test for the function is max
	 */
	public static void test16(){
		int test= 0;
		int reussi = 0;
		ExtendedArrays ea = new ExtendedArrays();
		int[] tab1 = {1,2,3,4,5,6,7,8,9,10};
		int[] tab2 = {1,4,6,6,10,1,9,10};
		int[] tab3 = null;
		test++;
		if(ea.max(tab1) == 10) {
			reussi++;
		}
		test++;
		if(ea.max(tab2) == 10) {
			reussi++;
		}
		test ++;
		if(ea.max(tab3) == -1) {
			reussi++;
		}
		System.out.println("test pour max( int [] a) "+ reussi + "/" + test);
	}
	
}
