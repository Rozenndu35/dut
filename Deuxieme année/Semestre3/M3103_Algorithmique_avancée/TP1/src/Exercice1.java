import java.util.Arrays;

/**
 * The exercice one
 * The test of diferents with equals , == , deepEquals
 * @author rozenn
 */
public class Exercice1 {


	/**
	 * the launcher 
	 * @param args
	 */
	public static void main(String[] args) {
		// the test of the differents
		int[] tab1 = {1,2};
		int[] tab2 = {1,2};
		int[] tab3 = {1,3};
		int[] tab4 = {1,2,3};
		int[] tab22 = tab2;
		Integer[] tabO1 = {1,2,3,4};
		Integer[] tabO2 = {1,2,3,4};
		Integer[] tabO3 = {1,2,3,5};
		Integer[] tabO4 = {1,2,3,4,5,6};
		Integer[] tabO22 = tabO2;
		Integer[][] tabO5 = {{1,2},{3,4}};
		Integer[][] tabO6 = {{1,2},{3,4}};
		Integer[][] tabO7 = {{1,2},{3,5}};
		Integer[][] tabO8 = {{1,2},{3,4},{5,6}};
		Integer[][] tabO55 = tabO5;
		 
		System.out.println("pour des tableaux identique sans objet");
		System.out.println("	avec ==: " + (tab1==tab2));
		System.out.println("	avec equals: " + Arrays.equals(tab1,tab2));
		System.out.println("pour des tableau qui point le meme tableau");
		System.out.println("	avec ==: " + (tab2==tab22));
		System.out.println("	avec equals: " + Arrays.equals(tab2,tab22));
		System.out.println("pour des tableau qui sont diferent");
		System.out.println("	avec ==: " + (tab1==tab3));
		System.out.println("	avec equals: " + Arrays.equals(tab1,tab3));
		System.out.println("pour des tableau qui sont de longueur diferente");
		System.out.println("	avec ==: " + (tab3==tab4));
		System.out.println("	avec equals: " + Arrays.equals(tab3,tab4));
		System.out.println("_____________________________________________________________________________");
		System.out.println("pour des tableaux identique avec objet a une dimention");
		System.out.println("	avec ==: " + (tabO1==tabO2));
		System.out.println("	avec equals: " + Arrays.equals(tabO1,tabO2));
		System.out.println("	avec deepEquals: " + Arrays.deepEquals(tabO1, tabO2));
		System.out.println("pour des tableau qui point le meme tableau a une dimentions");
		System.out.println("	avec ==: " + (tabO2==tabO22));
		System.out.println("	avec equals: " + Arrays.equals(tabO2,tabO22));
		System.out.println("	avec deepEquals: " + Arrays.deepEquals(tabO2, tabO22));
		System.out.println("pour des tableau a une dimentions qui sont diferent");
		System.out.println("	avec ==: " + (tabO1==tabO3));
		System.out.println("	avec equals: " + Arrays.equals(tabO1,tabO3));
		System.out.println("	avec deepEquals: " + Arrays.deepEquals(tabO1, tabO3));
		System.out.println("pour des tableau a une dimentions de longueur diferente");
		System.out.println("	avec ==: " + (tabO1==tabO4));
		System.out.println("	avec equals: " + Arrays.equals(tabO1,tabO4));
		System.out.println("	avec deepEquals: " + Arrays.deepEquals(tabO1, tabO4));
		System.out.println("pour des tableaux identique avec objet a deux dimention");
		System.out.println("	avec ==: " + (tabO5==tabO6));
		System.out.println("	avec equals: " + Arrays.equals(tabO5,tabO6));
		System.out.println("	avec deepEquals: " + Arrays.deepEquals(tabO5, tabO6));
		System.out.println("pour des tableau qui point le meme tableau a deux dimentions");
		System.out.println("	avec ==: " + (tabO5==tabO55));
		System.out.println("	avec equals: " + Arrays.equals(tabO5,tabO55));
		System.out.println("	avec deepEquals: " + Arrays.deepEquals(tabO5, tabO55));
		System.out.println("pour des tableau a une dimentions qui sont diferent");
		System.out.println("	avec ==: " + (tabO5==tabO7));
		System.out.println("	avec equals: " + Arrays.equals(tabO5,tabO7));
		System.out.println("	avec deepEquals: " + Arrays.deepEquals(tabO5, tabO7));
		System.out.println("pour des tableau a une dimentions de longueur diferente");
		System.out.println("	avec ==: " + (tabO5==tabO8));
		System.out.println("	avec equals: " + Arrays.equals(tabO5,tabO8));
		System.out.println("	avec deepEquals: " + Arrays.deepEquals(tabO5, tabO8));
		System.out.println("_____________________________________________________________________________");
	}
}
