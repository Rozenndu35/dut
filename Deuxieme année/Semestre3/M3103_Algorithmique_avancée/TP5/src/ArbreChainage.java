import java.util.ArrayList;
import java.util.Arrays;

/**
 * The class to make a tree
 * @author rozenn
 *
 */
public class ArbreChainage {
	protected Noeud racine;

	/**
	 * the constructor
	 */
	public ArbreChainage() {
		this.racine = null;
		//System.out.println("constructeur rentrer");
	}
	/**
	 * The constructor with root
	 * @param racineN
	 */
	public ArbreChainage(Noeud racineN) {
		this.racine = racineN;
		
	}

	/**
	 * look if the tree is empty
	 * @return true if the racine don't have value and son 
	 */
	public boolean estVide() {
		boolean ret = false;
		if( this.racine != null) {
			if (racine.getFils() == null && this.racine.toString().equals("null : null")) {
				ret = true;
			}
		}else {
			System.err.println("ArbreChainage : estVide : Sorry the root is null ");
		}
		return ret;
	}
	/**
	 * search the node in the tree and returns the subtrees of root n if it's not find returns null
	 * @param n the node to whant
	 * @return the subtrees of root n
	 */
	public ArbreChainage sousArbre(Noeud n) {
		ArbreChainage ret = null;
		if( this.racine != null) {
			Noeud[] fils = this.racine.getFils();
			boolean trouver = false;
			for(int i = 0; fils != null && i<fils.length && !trouver && ret == null; i++) {
				if( fils[i].equals(n)) {
					ret = new ArbreChainage(fils[i]);
					trouver = true;
				}
				else {
					ret = new ArbreChainage(fils[i]).sousArbre(n);
				}
			}
		}else {
			System.err.println("ArbreChainage : hauteur : Sorry the root is null ");
		}
		return ret;
	}
	
	/**
	 * check if the tree is binary
	 * @return true if the tree is bianairy
	 */
	public boolean estBinaire() {
		boolean ret = false;
		if( racine != null) {
			boolean erreur = false;
			Noeud[] fils = racine.getFils(); 
			for(int i = 0; fils != null && i< fils.length && fils.length <3 && !erreur ; i++) {
				boolean ok = new ArbreChainage(fils[i]).estBinaire();
				if(!ok) {
					erreur = true;
				}
			}
			if(!erreur &&fils == null|| fils.length <3 ) {
				ret = true;
			}
		}else {
			System.err.println("ArbreChainage : hauteur : Sorry the root is null ");
		}
		return ret;
	}
	
	/**
	 * calculate the height of the tree
	 * @return the height
	 */
	public int hauteur() {
		int ret = 0;
		if (racine != null) {
			ret = 1;
			Noeud[] fils = racine.getFils(); 
			for(int i = 0; fils != null && i< fils.length ; i++) {
				int hauteur = new ArbreChainage(fils[i]).hauteur() +1;
				if( hauteur > ret) {
					ret = hauteur;
				}
			}
		}else {
			System.err.println("ArbreChainage : hauteur : Sorry the root is null ");
		}
		return ret;
	}
	/**
	 * Create a binairy tree
	 * @param n the number ao value
	 * @return the new tree
	 */
	public static ArbreChainage suite(int n) {
		ArbreChainage ret = null;
		if (n >1){
			int hauteur = 1;
			int cle=1;
			ArrayList<Noeud> listPere = new ArrayList<Noeud>();
			ArrayList<Noeud> listFils = new ArrayList<Noeud>();
			Noeud racine = new Noeud(cle,"valeur",null,null); 
			ret = new ArbreChainage (racine);
			cle ++;
			hauteur ++;
			listPere.add(racine);
			while (hauteur < n) {
				//Creer les fils 
				for(int i = 0 ; i<Math.pow(2, hauteur); i++ ) {
					Noeud fils = new Noeud(cle,"valeur",null,null); 
					listFils.add(i,fils);
					cle++;
				}
				//Modifi les peres en leur ajoutant leur fils
				int filsAPlacer = 0;
				for(int i = 0 ; i<Math.pow(2, hauteur-1); i++ ) {
					Noeud lePere = listPere.get(i);
					listPere.remove(i);
					Noeud[] lesFils = new Noeud[2];
					for(int iF = 0; iF<2; i++) {
						lesFils[i]=listFils.get(filsAPlacer);
						filsAPlacer++;
					}
					lePere.setFils(lesFils);
					cle++;
				}
				//les fils deviennent peres
				listPere.clear();
				for(int i = 0; i<Math.pow(2, hauteur);i++) {
					listPere.add(i, listFils.get(i));
				}
				listFils.clear();
				hauteur ++;
			}
		}else if (n == 1) {
			ret = new ArbreChainage (new Noeud(1, "valeur" , null ,null));
		}else {
			ret = new ArbreChainage (null);
			System.err.println("ArbreChainage : suite : Sorry the tree can't have 0 node");
		}
		return ret;
	}
	
	/**
	 * check if the tree is ABR
	 * @return true if is ABR
	 */
	public  boolean estABR() {
		boolean ret = false;
		if(estBinaire()) {
			//verifie les egaliter des fils et peres
			boolean ordre=false;
			if((int)this.racine.getCle() > (int)this.racine.getFilsGauche().getCle() && (int)this.racine.getFilsDroite().getCle() > (int)this.racine.getCle()) {
				boolean droit = new ArbreChainage(this.racine.getFilsDroite()).estABR();
				boolean gauche = new ArbreChainage(this.racine.getFilsGauche()).estABR();
				if(droit && gauche) {
					ordre = true;
				}
			}
			//verifier cle unique?
		}else {
			System.err.println("ArbreChainage : estABR : Sorry the tree is not bianaire");
		}
		return ret;
	}
	/**
	 * check if the tree is AVL
	 * @return tru if is AVL
	 */
	public boolean estAVL() {
		boolean ret = false;
		if(estABR()) {
			int hauteurdroit = new ArbreChainage(this.racine.getFilsDroite()).hauteur() +1;
			int hauteurgauche = new ArbreChainage(this.racine.getFilsGauche()).hauteur() +1;
			if (hauteurdroit == hauteurgauche || hauteurdroit == hauteurgauche+1 ||hauteurdroit+1 == hauteurgauche) {
				boolean droit = new ArbreChainage(this.racine.getFilsDroite()).estAVL();
				boolean gauche = new ArbreChainage(this.racine.getFilsGauche()).estAVL();
				if(droit && gauche) {
					ret = true;
				}
			}		
		}else {
			System.err.println("ArbreChainage : estAVL : Sorry the treeis not ABR");
		}
		return ret;
	}

	/**
	 * look up the value for an ABR tree
	 * @param cle the key to search
	 * @return the value
	 */
	public Object rechercher(int cleR) {
		Object ret = null;
		if(estABR()) {
			if ((int)this.racine.getCle() == cleR) {
				ret=this.racine.getValeur();
			}
			if( cleR < (int) this.racine.getCle()) {
				Object trouver = new ArbreChainage(this.racine.getFilsGauche()).rechercher(cleR);
				if(trouver == null) {
					ArbreChainage arbreDroit = new ArbreChainage(this.racine.getFilsDroite());
					if(arbreDroit.hauteur() > 1) {
						trouver =  arbreDroit.rechercher(cleR);
					}
				}
				ret = trouver;
			}
			if( cleR > (int) this.racine.getCle()) {
				Object trouver = new ArbreChainage(this.racine.getFilsDroite()).rechercher(cleR);
				if(trouver == null) {
					ArbreChainage arbreGauche = new ArbreChainage(this.racine.getFilsGauche());
					if(arbreGauche.hauteur() > 1) {
						trouver =  arbreGauche.rechercher(cleR);
					}
				}
				ret = trouver;
			}
		}else {
			System.err.println("ArbreChainage : recherche : Sorry the treeis not ABR");
		}
		return ret;
		
	}
	/**
	 * add a value based on his key in an ABR tree
	 * @param cle the key to add
	 * @param valeur the value to add
	 */
	public void ajout(int cleN , Object valeur) {
		if(estABR()&& rechercher(cleN) != null) {
			if( cleN < (int) this.racine.getCle()) {
				if((int)this.racine.getFilsDroite().getCle() > cleN){
					Noeud[] fils = {null, this.racine.getFilsDroite() };
					this.racine.setFilsDroit(new Noeud(cleN, valeur, this.racine, fils));
				}else {
					Noeud[] fils = {this.racine.getFilsDroite(), null };
					this.racine.setFilsDroit(new Noeud(cleN, valeur, this.racine, fils));
				}
			}
			if( cleN >= (int) this.racine.getCle()) {
				if((int)this.racine.getFilsGauche().getCle() > cleN){
					Noeud[] fils = {null, this.racine.getFilsGauche() };
					this.racine.setFilsGauche(new Noeud(cleN, valeur, this.racine, fils));
				}else {
					Noeud[] fils = {this.racine.getFilsGauche(), null };
					this.racine.setFilsGauche(new Noeud(cleN, valeur, this.racine, fils));
				}
			}
		}else {
			System.err.println("ArbreChainage : ajout : Sorry the treeis not ABR or the key is in the tree");
		}
	}
	/**
	 * remove a neud from an ABR tree
	 * @param cle the key to delete
	 */
	public void supprimer (int cle) {
		if(estABR()) {
			
		}
	}
	/**
	 * calaculate the number of elements of an ABR tree
	 * @return the number of element
	 */
	public int taille() {
		int ret =0;
		if(estABR()) {
			ret = 1;
			Noeud[] fils = racine.getFils(); 
			for(int i = 0; fils != null && i< fils.length ; i++) {
				int nb = new ArbreChainage(fils[i]).taille();
				ret = ret +nb;
			}
		}
		return ret;
	}
	/**
	 * get the string key : value of the root
	 * @return the "key : value" 
	 */
	public String toString() {
		return this.racine.toString();
	}
}
