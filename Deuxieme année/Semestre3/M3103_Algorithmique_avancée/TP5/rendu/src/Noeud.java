import java.util.Arrays;

/**
 * The class for the node
 * @author rozenn
 *
 * @param <C>
 * @param <V>
 */
public class Noeud<C, V> {
	private C cle;
	private V valeur;
	private Noeud pere;
	private Noeud[] fils; //le 0 est le droit et la 1 est le gauche dans les cas binaires.
	private boolean binaire;
	
	/**
	 * @return the cle
	 */
	public C getCle() {
		return cle;
	}
	/**
	 * @param cle the cle to set
	 */
	public void setCle(C cle) {
		this.cle = cle;
	}
	/**
	 * @return the valeur
	 */
	public V getValeur() {
		return valeur;
	}
	/**
	 * @param valeur the valeur to set
	 */
	public void setValeur(V valeur) {
		this.valeur = valeur;
	}
	/**
	 * @return the pere
	 */
	public Noeud getPere() {
		return pere;
	}
	/**
	 * @param pere the pere to set
	 */
	public void setPere(Noeud pere) {
		this.pere = pere;
	}
	/**
	 * @return the fils
	 */
	public Noeud[] getFils() {
		return this.fils;
	}
	/**
	 * @return the right fils for the binairy tree
	 */
	public Noeud getFilsDroite() {
		return this.fils[0];
	}
	/**
	 * @return the left fils for the binairy tree
	 */
	public Noeud getFilsGauche() {
		return this.fils[1];
	}
	/**
	 * @param fils the fils to set
	 */
	public void setFils(Noeud[] fils) {
		if(binaire && fils.length<=2) {
			this.fils = fils;
		}else if(!binaire) {
			this.fils = fils;
		}else {
			System.err.println("Noeud : setFils : Sorry the binairy tree cant have more than two sons ");
		}
	}
	/**
	 * @param fils the fils right for binairy tree
	 */
	public void setFilsDroit(Noeud fils) {
		this.fils[0]=fils;
		
	}
	/**
	 * @param fils the fils left for binairy tree
	 */
	public void setFilsGauche(Noeud fils) {
		this.fils[1]=fils;
		
	}
	/**
	 * the constructeur
	 * @param cle the key to the node
	 * @param valeur the value to the node
	 * @param Pere the father
	 * @param Fils the liste of the sons
	 */
	public Noeud(C cleN , V valeurN, Noeud pereN, Noeud[] filsN) {
		this.cle = cleN;
		this.valeur = valeurN;
		this.pere = pereN;
		if(filsN != null) {
			if(binaire && fils.length<=2) {
				this.fils = fils;
			}else if(!binaire) {
				this.fils = fils;
			}else {
				System.err.println("Noeud : setFils : Sorry the binairy tree cant have more than two sons ");
				this.fils=null;
			}
		}else {
			this.fils = null;
		}
		
	}
	
	
	/**
	 * get the string key : value
	 * @return the "key : value"
	 */
	public String toString() {
		String ret ="null : null";
		if(this.cle != null && this.valeur!= null) {
			ret = this.cle.toString() + " : "+ this.valeur.toString();
		}else {
			System.err.println("Noeud : toString : Sorry but the key or the valus is null ");
		}
		return ret;
	}
}
