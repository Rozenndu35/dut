import java.util.Arrays;
import java.util.Scanner;

public class Test {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Veuillez saisir l'exercice :");
		String str = sc.nextLine();
		if(str.equals("1")) {
			testExo1();
		}else if(str.equals("2")) {
			testExo2();
		}else if(str.equals("3")) {
			testExo3();
		}else if(str.equals("4")) {
			testExo4();
		}else if(str.equals("5")) {
			testExo5();
		}else {
			System.out.print("cette exercice ne possede pas de test ou n'existe pas");
		}
	}

	private static void testExo1() {
		System.out.println("--------------Test de Noeud---------------");
		int test = 0;
		int reussi = 0;
		test++;
		Noeud noeudNull = new Noeud(null , null, null,null);
		if (noeudNull.toString().equals("null : null")) {
			reussi ++;
		}
		test++;
		Noeud noeudRacineSansEnfant = new Noeud(1 , "racine", null,null);
		if (noeudRacineSansEnfant.toString().equals("1 : racine")) {
			reussi ++;
		}
		test++;
		Noeud noeudEnfant = new Noeud(3 , "enfant", new Noeud(1 , "parent ", null,null) ,null);
		if (noeudEnfant.toString().equals("3 : enfant")) {
			reussi ++;
		}
		System.out.println("test pour Noeud : toString()  "+ reussi + "/" + test);
		
	}

	private static void testExo2() {
		System.out.println("--------------Test de ArbreChainage---------------");
		ArbreChainage arbre = new ArbreChainage();
		
	}

	private static void testExo3() {
		ArbreChainage arbrenull = new ArbreChainage();
		ArbreChainage arbrevide = new ArbreChainage(new Noeud(null, null, null,null));
		Noeud testN = new Noeud("1", "fils1", null ,null);
		Noeud erreurN = new Noeud("erreur", "erreur", null ,null);
		Noeud[] fils = {testN,new Noeud("2", "fils2", null ,null),new Noeud("3", "fils3", null ,null)};
		Noeud racine = new Noeud("test", "pasvide", null , fils);
		ArbreChainage arbrerempli = new ArbreChainage(racine);
		ArbreChainage arbreTest = new ArbreChainage(testN);
		
		System.out.println("--------------Test de estVide---------------");
		int test = 0;
		int reussi = 0;
		test++;
		if(!arbrenull.estVide()) {
			reussi++;
		}
		test++;
		if(arbrevide.estVide()) {
			reussi++;
		}
		test++;
		if(!arbrerempli.estVide()) {
			reussi++;
		}
		System.out.println("test pour ArbreChainage : estvide()  "+ reussi + "/" + test);
		
		System.out.println("--------------Test de sousarbre---------------");
		test = 0;
		reussi = 0;
		test++;
		if(arbrenull.sousArbre(erreurN) == null) {
			reussi++;
		}
		test++;
		if(arbrevide.sousArbre(erreurN) == null) {
			reussi++;
		}
		test++;
		if(arbrerempli.sousArbre(erreurN)== null) {
			reussi++;
		}
		test++;
		if(arbrerempli.sousArbre(testN).toString().equals(arbreTest.toString())) {
			reussi++;
		}
		System.out.println("test pour ArbreChainage : sousArbre(Noeud n) "+ reussi + "/" + test);
		
	}

	private static void testExo4() {
		Noeud[] filsNonBinaire = {new Noeud("1", "fils1", null ,null),new Noeud("2", "fils2", null ,null),new Noeud("3", "fils3", null ,null)};
		Noeud racineNonBinaire = new Noeud("test", "pasvide", null , filsNonBinaire);
		ArbreChainage arbreNonBinaire = new ArbreChainage(racineNonBinaire);
		Noeud[] filsBinaire = {new Noeud("2", "fils2", null ,null),new Noeud("3", "fils3", null ,null)};
		Noeud racineBinaire = new Noeud("test", "pasvide", null , filsBinaire);
		ArbreChainage arbreBinaire = new ArbreChainage(racineBinaire);
		Noeud[] filsNonBinaireD = {new Noeud("1", "fils1", null ,filsNonBinaire),new Noeud("3", "fils3", null ,null)};
		Noeud racineNonBinaireD = new Noeud("test", "pasvide", null , filsNonBinaire);
		ArbreChainage arbreNonBinaireD = new ArbreChainage(racineNonBinaire);
		Noeud[] filsBinaireD = {new Noeud("2", "fils2", null ,filsBinaire),new Noeud("3", "fils3", null ,null)};
		Noeud racineBinaireD = new Noeud("test", "pasvide", null , filsBinaireD);
		ArbreChainage arbreBinaireD = new ArbreChainage(racineBinaireD);
		ArbreChainage arbrenull = new ArbreChainage();
		
		int test = 0;
		int reussi = 0;
		test++;
		if(!arbreNonBinaire.estBinaire()) {
			reussi++;
		}
		test++;
		if(arbreBinaire.estBinaire()) {
			reussi++;
		}
		test++;
		if(!arbreNonBinaireD.estBinaire()) {
			reussi++;
		}
		test++;
		if(arbreBinaireD.estBinaire()) {
			reussi++;
		}
		test++;
		if(!arbrenull.estBinaire()) {
			reussi++;
		}
		System.out.println("test pour ArbreChainage : estBinaire()  "+ reussi + "/" + test);
		
		
	}

	private static void testExo5() {
		Noeud[] fils = {new Noeud("2", "fils2", null ,null),new Noeud("3", "fils3", null ,null)};
		Noeud racine = new Noeud("test", "pasvide", null , fils);
		ArbreChainage arbre = new ArbreChainage(racine);
		Noeud[] filsD = {new Noeud("1", "fils1", null ,fils),new Noeud("3", "fils3", null ,null)};
		Noeud racineD = new Noeud("test", "pasvide", null , filsD);
		ArbreChainage arbreD = new ArbreChainage(racineD);
		ArbreChainage arbrenull = new ArbreChainage();
		ArbreChainage arbre1 = new ArbreChainage(new Noeud("test", "pasvide", null , null));
		
		int test = 0;
		int reussi = 0;
		test++;
		if(arbre.hauteur() == 2) {
			reussi++;
		}
		test++;
		if(arbreD.hauteur() == 3) {
			reussi++;
		}
		test++;
		if(arbrenull.hauteur() == 0) {
			reussi++;
		}
		test++;
		if(arbre1.hauteur() == 1) {
			reussi++;
		}
		System.out.println("test pour ArbreChainage : estBinaire()  "+ reussi + "/" + test);
		
	}
}
