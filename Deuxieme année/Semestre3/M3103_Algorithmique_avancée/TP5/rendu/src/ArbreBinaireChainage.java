/**
 * 
 */

/**
 * @author rozenn
 *
 */
public class ArbreBinaireChainage  extends ArbreChainage{
	/**
	 * the constructor
	 */
	public ArbreBinaireChainage() {
		super();
	}
	/**
	 * The constructor with root
	 * @param racineN
	 */
	public ArbreBinaireChainage(Noeud racineN) {
		super();
		if(!super.estBinaire()) {
			super.racine = null;
		}
	}
}
