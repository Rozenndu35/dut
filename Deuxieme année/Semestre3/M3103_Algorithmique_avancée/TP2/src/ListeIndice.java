
/**
 * Lidt Interface
 * @author rozenn
 *
 */
public interface ListeIndice<E> {

	/**
	 * returns true if the list contains no elements, otherwise false
	 * @return true if the list contains no elements
	 */
	public boolean estvide();
	/**
	 * returns true if the list is full (that is, it is no longer possible to insert new elements),
	 * @return true if the list is full
	 */
	public boolean estpleine();
	/**
	 * return the first item in the list,
	 * @return the first item in the list,
	 */
	public E debut();
	/**
	 * return the last item in the list,
	 * @return the last item in the list,
	 */
	public E fin(); 
	/**
	 * returns the i th element of the list,
	 * @param i the index
	 * @return the i th element of the list
	 */
	public E element(int i); 	
	/**
	 * inserts a new element before the i th element from the list,
	 * @param i the index after you whant tu put the new element
	 * @param nouveau the new elements
	 */
	public void insertion(int i, E nouveau); 
	/**
	 * which adds a new element after the i th element of the listing
	 * @param i the index where you whant tu put the new element
	 * @param nouveau the new elements
	 */
	public void ajout(int i, E nouveau);
	/**
	 * delete the i th item from the list
	 * @param i the index wher you you want delete
	 */
	public void suppression(int i);
	/**
	 * replace the i th element of the list by the one passed in parameter
	 * @param i the index to whant replace
	 * @param nouveau the new element
	 */
	public void modification(int i, E nouveau);
}
