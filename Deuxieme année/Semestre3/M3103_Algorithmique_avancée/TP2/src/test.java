/**
 * the clas of the test
 * @author rozenn
 */

import java.util.Scanner;

public class test {
	/**
	 * The launcher for the test
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Veuillez saisir l'exercice :");
		String str = sc.nextLine();
		if(str.equals("6")) {
			testListeIndiceTableau();
		}else if(str.equals("7")) {
			testListeSentinelleTableau();
		}else if(str.equals("8")) {
			testListeTableau();
		}else if(str.equals("9")) {
			testListeIndiceChainageSimple();
		}else if(str.equals("10")) {
			testListeSentinelleChainageDouble();
		}else {
			System.out.print("cette exercice ne possede pas de test ou n'existe pas");
		}
	}
	/**
	 * test for the class ListeIndiceTableau
	 */
	public static void testListeIndiceTableau() {
		// test de ListeIndiceTableau
		int test = 0;
		int reussi = 0;
		test ++;
		ListeIndiceTableau listnull = new ListeIndiceTableau(-1 );
		if(listnull.toString().equals("null")) {
			reussi ++;
		}
		test ++;
		ListeIndiceTableau list0 = new ListeIndiceTableau(0 );
		if(list0.toString().equals("[]")) {
			reussi ++;
		}
		test ++;
		ListeIndiceTableau list10vide = new ListeIndiceTableau(10 );
		if(list10vide.toString().equals("[null, null, null, null, null, null, null, null, null, null]")) {
			reussi ++;
		}
		System.out.println("test pour ListeIndiceTableau : ListeIndiceTableau(int n ) "+ reussi + "/" + test);
		// test de estvide();
		test = 0;
		reussi = 0;
		test ++;
		if(!listnull.estvide()) {
			reussi ++;
		}
		test ++;
		if(list0.estvide()) {
			reussi ++;
		}
		test ++;
		if(list10vide.estvide()) {
			reussi ++;
		}
		ListeIndiceTableau list10rempli1 = new ListeIndiceTableau(10 );
		test ++;
		list10rempli1.ajout(5, "test");
		if(!list10rempli1.estvide()) {
			reussi ++;
		}
		ListeIndiceTableau list10remplientier = new ListeIndiceTableau(10 );
		test ++;
		for(int i = 0; i<10 ; i++) {
			list10remplientier.modification(i, "test");
		}
		if(!list10remplientier.estvide()) {
			reussi ++;
		}
		System.out.println("test pour ListeIndiceTableau : estvide() "+ reussi + "/" + test);
		// test de estpleine();
		test = 0;
		reussi = 0;
		test ++;
		if(!listnull.estpleine()) {
			reussi ++;
		}
		test ++;
		if(list0.estpleine()) {
			reussi ++;
		}
		test ++;
		if(!list10vide.estpleine()) {
			reussi ++;
		}
		test ++;
		if(!list10rempli1.estpleine()) {
			reussi ++;
		}
		test ++;
		if(list10remplientier.estpleine()) {
			reussi ++;
		}
		System.out.println("test pour ListeIndiceTableau : estpleine() "+ reussi + "/" + test);
		// test de debut();
		test = 0;
		reussi = 0;
		test ++;
		if(listnull.debut() == null) {
			reussi ++;
		}
		test ++;
		if(list0.debut()== null) {
			reussi ++;
		}
		test ++;
		if(list10vide.debut()== null) {
			reussi ++;
		}
		test ++;
		list10remplientier.modification(0, "debut");
		if(list10remplientier.debut().equals("debut")) {
			reussi ++;
		}
		System.out.println("test pour ListeIndiceTableau : debut() "+ reussi + "/" + test);
		// test de fin(); 
		test = 0;
		reussi = 0;
		test ++;
		if(listnull.fin() == null) {
			reussi ++;
		}
		test ++;
		if(list0.fin()== null) {
			reussi ++;
		}
		test ++;
		if(list10vide.fin()== null) {
			reussi ++;
		}
		test ++;
		list10remplientier.modification(9, "fin");
		if(list10remplientier.fin().equals("fin")) {
			reussi ++;
		}
		System.out.println("test pour ListeIndiceTableau : fin() "+ reussi + "/" + test);
		// test de element(int i); 	
		test = 0;
		reussi = 0;
		test ++;
		if(listnull.element(5) == null) {
			reussi ++;
		}
		test ++;
		if(list0.element(0)== null) {
			reussi ++;
		}
		test ++;
		if(list10vide.element(5)== null) {
			reussi ++;
		}
		test ++;
		list10remplientier.modification(6, "element rechercher");
		if(list10remplientier.element(6).equals("element rechercher")) {
			reussi ++;
		}
		System.out.println("test pour ListeIndiceTableau : element(int i) "+ reussi + "/" + test);
		// test de insertion(int i, E nouveau); 
		test = 0;
		reussi = 0;
		test ++;
		listnull.insertion(6, "insertion");
		if(listnull.element(5) == null) {
			reussi ++;
		}
		test ++;
		list0.insertion(0, "insertion");
		if(list0.element(0)== null) {
			reussi ++;
		}
		test ++;
		list10vide.insertion(6, "insertion");
		if(list10vide.element(5).equals("insertion")) {
			reussi ++;
			
		}
		test ++;
		list10remplientier.insertion(6, "insertion");
		if(list10remplientier.element(5).equals("test")) {
			reussi ++;
			
		}
		System.out.println("test pour ListeIndiceTableau : insertion(int i, Object nouveau) "+ reussi + "/" + test);
		// test de ajout(int i, E nouveau);
		test = 0;
		reussi = 0;
		test ++;
		listnull.ajout(4, "ajout");
		if(listnull.element(5) == null) {
			reussi ++;
		}
		test ++;
		list0.ajout(0, "ajout");
		if(list0.element(1)== null) {
			reussi ++;
		}
		test ++;
		list10vide.ajout(6, "ajout");
		if(list10vide.element(7).equals("ajout")) {
			reussi ++;
			
		}
		test ++;
		list10remplientier.ajout(6, "ajout");
		if(list10remplientier.element(7).equals("test")) {
			reussi ++;
			
		}
		System.out.println("test pour ListeIndiceTableau : ajout(int i, E nouveau) "+ reussi + "/" + test);
		// test de suppression(int i);
		test ++;
		listnull.suppression(5);
		if(listnull.element(5) == null) {
			reussi ++;
		}
		test ++;
		list0.suppression(0);
		if(list0.element(0)== null) {
			reussi ++;
		}
		test ++;
		list10vide.suppression(5);
		if(list10vide.element(5)== null) {
			reussi ++;
		}
		test ++;
		list10remplientier.suppression(6);
		if(list10remplientier.element(6) == null) {
			reussi ++;
		}
		System.out.println("test pour ListeIndiceTableau : suppression(int i) "+ reussi + "/" + test);
		
		// test de modification(int i, Object nouveau)
		test = 0;
		reussi = 0;
		test ++;
		if(listnull.element(5) == null) {
			reussi ++;
		}
		test ++;
		if(list0.element(0)== null) {
			reussi ++;
		}
		test ++;
		list10vide.modification(9, "element modifier");
		if(list10vide.element(9).equals("element modifier")) {
			reussi ++;
		}
		test ++;
		list10remplientier.modification(9, "element modifier");
		if(list10remplientier.element(9).equals("element modifier")) {
			reussi ++;
		}
		System.out.println("test pour ListeIndiceTableau : element(int i) "+ reussi + "/" + test);
	}
	/**
	 * test for the class ListeSentinelleTableau
	 */
	public static void testListeSentinelleTableau() {
		// test de ListeIndiceTableau
		int test = 0;
		int reussi = 0;
		test ++;
		ListeSentinelleTableau listnull = new ListeSentinelleTableau(-1 );
		if(listnull.toString().equals("null")) {
			reussi ++;
		}
		test ++;
		ListeSentinelleTableau list0 = new ListeSentinelleTableau(0 );
		if(list0.toString().equals("[]")) {
			reussi ++;
		}
		test ++;
		ListeSentinelleTableau list10vide = new ListeSentinelleTableau(10 );
		if(list10vide.toString().equals("[null, null, null, null, null, null, null, null, null, null]")) {
			reussi ++;
		}
		System.out.println("test pour ListeSentinelleTableau : ListeSentinelleTableau(int n ) "+ reussi + "/" + test);
				
		// test de estvide();
		test = 0;
		reussi = 0;
		test ++;
		if(!listnull.estvide()) {
			reussi ++;
		}
		test ++;
		if(list0.estvide()) {
			reussi ++;
		}
		test ++;
		if(list10vide.estvide()) {
			reussi ++;
		}
		ListeSentinelleTableau list10rempli1 = new ListeSentinelleTableau(10 );
		test ++;
		list10rempli1.ajout("test");
		if(!list10rempli1.estvide()) {
			reussi ++;
		}
		ListeSentinelleTableau list10remplientier = new ListeSentinelleTableau(10 );
		test ++;
		for(int i = 0; i<10 ; i++) {
			list10remplientier.modification("test");
			list10remplientier.suivent();
		}
		if(!list10remplientier.estvide()) {
			reussi ++;
		}
		System.out.println("test pour ListeSentinelleTableau : estvide() "+ reussi + "/" + test);
		// test de estpleine();
		test = 0;
		reussi = 0;
		test ++;
		if(!listnull.estpleine()) {
			reussi ++;
		}
		test ++;
		if(list0.estpleine()) {
			reussi ++;
		}
		test ++;
		if(!list10vide.estpleine()) {
			reussi ++;
		}
		test ++;
		if(!list10rempli1.estpleine()) {
			reussi ++;
		}
		test ++;
		if(list10remplientier.estpleine()) {
			reussi ++;
		}
		System.out.println("test pour ListeSentinelleTableau : estpleine() "+ reussi + "/" + test);
				
		// test de suivent()
		
		test = 0;
		reussi = 0;
		test ++;
		listnull.suivent();
		if(listnull.getSentinelle() == 0) {
			reussi ++;
		}
		test ++;
		list0.suivent();
		if(list0.getSentinelle() == 0) {
			reussi ++;
		}
		test ++;
		list10vide.setSentinelle(0);
		list10vide.suivent();
		if(list10vide.getSentinelle() == 1) {
			reussi ++;
		}
		test ++;
		list10vide.setSentinelle(9);
		list10vide.suivent();
		if(list10vide.getSentinelle() == 9) {
			reussi ++;
		}
		test ++;
		list10vide.setSentinelle(10);
		list10vide.suivent();
		if(list10vide.getSentinelle() == 10) {
			reussi ++;
		}
		System.out.println("test pour ListeSentinelleTableau : Suivent() "+ reussi + "/" + test);
		
		// test de precedent()
		test = 0;
		reussi = 0;
		test ++;
		listnull.precedent();
		if(listnull.getSentinelle() == 0) {
			reussi ++;
		}
		test ++;
		list10vide.setSentinelle(9);
		list10vide.precedent();
		if(list10vide.getSentinelle() == 8) {
			reussi ++;
		}
		test ++;
		list10vide.setSentinelle(0);
		list10vide.precedent();
		if(list10vide.getSentinelle() == 0) {
			reussi ++;
		}
		test ++;
		list10vide.setSentinelle(-1);
		list10vide.precedent();
		if(list10vide.getSentinelle() == -1) {
			
			reussi ++;
		}
		System.out.println("test pour ListeSentinelleTableau : precedent() "+ reussi + "/" + test);
		list10vide.setSentinelle(0);
		// test de debut()
		test = 0;
		reussi = 0;
		test ++;
		if(listnull.debut() == null && listnull.getSentinelle() == 0) {
			reussi ++;
			
		}
		test ++;
		if(list0.debut()== null && list0.getSentinelle() == 0) {
			reussi ++;
			
		}
		test ++;
		if(list10vide.debut()== null && list10vide.getSentinelle() == 0) {
			reussi ++;
			
		}
		test ++;
		list10remplientier.setSentinelle(0);
		list10remplientier.modification("debut");
		if(list10remplientier.debut().equals("debut") && list10remplientier.getSentinelle() == 0) {
			reussi ++;
		}
		System.out.println("test pour ListeSentinelleTableau : debut() "+ reussi + "/" + test);
		// test de fin(); 
		test = 0;
		reussi = 0;
		test ++;
		if(listnull.fin() == null && listnull.getSentinelle() == 0) {
			reussi ++;
		}
		test ++;
		if(list0.fin()== null && list0.getSentinelle() == 0) {
			reussi ++;
		}
		test ++;
		if(list10vide.fin()== null && list10vide.getSentinelle() == 9) {
			reussi ++;
			
		}
		test ++;
		list10remplientier.setSentinelle(9);
		list10remplientier.modification("fin");
		if(list10remplientier.fin().equals("fin") && list10remplientier.getSentinelle() == 9) {
			reussi ++;
		}
		System.out.println("test pour ListeSentinelleTableau : fin() "+ reussi + "/" + test);
		
		//element()
		test = 0;
		reussi = 0;
		test ++;
		if(listnull.element() == null) {
			reussi ++;
		}
		test ++;
		if(list0.element()== null ) {
			reussi ++;
		}
		test ++;
		if(list10vide.element()== null) {
			reussi ++;
			
		}
		test ++;
		list10remplientier.setSentinelle(8);
		list10remplientier.modification("rechercher");
		if(list10remplientier.element().equals("rechercher")) {
			reussi ++;
		}
		System.out.println("test pour ListeSentinelleTableau : element() "+ reussi + "/" + test);
		//insertion
		test = 0;
		reussi = 0;
		test ++;
		listnull.insertion("erreur");
		if(listnull.element() == null) {
			reussi ++;
		}
		test ++;
		list0.insertion("erreur");
		if(list0.element()== null ) {
			reussi ++;
		}
		test ++;
		list10vide.setSentinelle(8);
		list10vide.insertion("insere");
		if(list10vide.element().equals("insere")) {
			reussi ++;
			
		}
		test ++;
		list10remplientier.setSentinelle(8);
		list10remplientier.insertion("insere");
		if(!list10remplientier.element().equals("insere")) {
			reussi ++;
		}
		System.out.println("test pour ListeSentinelleTableau : insertion() "+ reussi + "/" + test);
		//insertion
		test = 0;
		reussi = 0;
		test ++;
		listnull.ajout("erreur");
		if(listnull.element() == null) {
			reussi ++;
		}
		test ++;
		list0.ajout("erreur");
		if(list0.element()== null ) {
			reussi ++;
		}
		test ++;
		list10vide.setSentinelle(8);
		list10vide.ajout("ajout");
		if(list10vide.element().equals("ajout")) {
			reussi ++;
			
		}
		test ++;
		list10remplientier.setSentinelle(8);
		list10remplientier.ajout("ajout");
		if(!list10remplientier.element().equals("ajout")) {
			reussi ++;
		}
		System.out.println("test pour ListeSentinelleTableau : ajout() "+ reussi + "/" + test);
		
		// test de suppression();
		test ++;
		listnull.suppression();
		if(listnull.toString().equals("null")) {
			reussi ++;
		}
		test ++;
		list0.suppression();
		if(list0.toString().equals("[]")) {
			reussi ++;
		}
		test ++;
		list10vide.suppression();
		if(list10vide.toString().equals("[null, null, null, null, null, null, null, insere, null, null]")) {
			reussi ++;
		}
		test ++;
		list10remplientier.suppression();
		if(list10remplientier.toString().equals("[debut, test, test, test, test, test, test, test, fin, null]")) {
			reussi ++;
		}
		System.out.println("test pour ListeIndiceTableau : suppression() "+ reussi + "/" + test);
		// test de modification(); 
		test = 0;
		reussi = 0;
		test ++;
		listnull.modification("modif");
		if(listnull.element() == null ) {
			reussi ++;
		}
		test ++;
		list0.modification("modif");
		if(list0.element() == null) {
			reussi ++;
		}
		test ++;
		list10vide.setSentinelle(3);
		list10vide.modification("modif");
		if(list10vide.element().equals("modif")) {
			reussi ++;
		}
		test ++;
		list10remplientier.setSentinelle(3);
		list10remplientier.modification("modif");
		if(list10remplientier.element().equals("modif")) {
			reussi ++;
		}
		System.out.println("test pour ListeSentinelleTableau : modification() "+ reussi + "/" + test);
	}
	/**
	 * test for the class 
	 */
	public static void testListeTableau() {
		//test de ListeTableau
		int test = 0;
		int reussi = 0;
		test ++;
		ListeTableau listnull = new ListeTableau(-1 );
		if(listnull.toString().equals("null")) {
			reussi ++;
		}
		test ++;
		ListeTableau list0 = new ListeTableau(0 );
		if(list0.toString().equals("[]")) {
			reussi ++;
		}
		test ++;
		ListeTableau list10vide = new ListeTableau(10 );
		if(list10vide.toString().equals("[null, null, null, null, null, null, null, null, null, null]")) {
			reussi ++;
		}
		System.out.println("test pour ListeTableau : ListeTableau(int n ) "+ reussi + "/" + test);
		// test de estvide();
		test = 0;
		reussi = 0;
		test ++;
		if(!listnull.estvide()) {
			reussi ++;
		}
		test ++;
		if(list0.estvide()) {
			reussi ++;
		}
		test ++;
		if(list10vide.estvide()) {
			reussi ++;
		}
		ListeTableau list10rempli1 = new ListeTableau(10 );
		test ++;
		list10rempli1.ajout(5, "test");
		if(!list10rempli1.estvide()) {
			reussi ++;
		}
		ListeTableau list10remplientier = new ListeTableau(10 );
		test ++;
		for(int i = 0; i<10 ; i++) {
			list10remplientier.modification(i, "test");
		}
		if(!list10remplientier.estvide()) {
			reussi ++;
		}
		System.out.println("test pour ListeTableau : estvide() "+ reussi + "/" + test);
		// test de estpleine();
		test = 0;
		reussi = 0;
		test ++;
		if(!listnull.estpleine()) {
			reussi ++;
		}
		test ++;
		if(list0.estpleine()) {
			reussi ++;
		}
		test ++;
		if(!list10vide.estpleine()) {
			reussi ++;
		}
		test ++;
		if(!list10rempli1.estpleine()) {
			reussi ++;
		}
		test ++;
		if(list10remplientier.estpleine()) {
			reussi ++;
		}
		System.out.println("test pour  ListeTableau : estpleine() "+ reussi + "/" + test);
		// test de debut();
		test = 0;
		reussi = 0;
		test ++;
		if(listnull.debut() == null) {
			reussi ++;
		}
		test ++;
		if(list0.debut()== null) {
			reussi ++;
		}
		test ++;
		if(list10vide.debut()== null) {
			reussi ++;
		}
		test ++;
		list10remplientier.modification(0, "debut");
		if(list10remplientier.debut().equals("debut")) {
			reussi ++;
		}
		System.out.println("test pour  ListeTableau : debut() "+ reussi + "/" + test);
		// test de fin(); 
		test = 0;
		reussi = 0;
		test ++;
		if(listnull.fin() == null) {
			reussi ++;
		}
		test ++;
		if(list0.fin()== null) {
			reussi ++;
		}
		test ++;
		if(list10vide.fin()== null) {
			reussi ++;
		}
		test ++;
		list10remplientier.modification(9, "fin");
		if(list10remplientier.fin().equals("fin")) {
			reussi ++;
		}
		System.out.println("test pour  ListeTableau : fin() "+ reussi + "/" + test);
		// test de element(int i); 	
		test = 0;
		reussi = 0;
		test ++;
		if(listnull.element(5) == null) {
			reussi ++;
		}
		test ++;
		if(list0.element(0)== null) {
			reussi ++;
		}
		test ++;
		if(list10vide.element(5)== null) {
			reussi ++;
		}
		test ++;
		list10remplientier.modification(6, "element rechercher");
		if(list10remplientier.element(6).equals("element rechercher")) {
			reussi ++;
		}
		System.out.println("test pour ListeTableau : element(int i) "+ reussi + "/" + test);
		// test de insertion(int i, E nouveau); 
		test = 0;
		reussi = 0;
		test ++;
		listnull.insertion(6, "insertion");
		if(listnull.element(5) == null) {
			reussi ++;
		}
		test ++;
		list0.insertion(0, "insertion");
		if(list0.element(0)== null) {
			reussi ++;
		}
		test ++;
		list10vide.insertion(6, "insertion");
		if(list10vide.element(5).equals("insertion")) {
			reussi ++;
			
		}
		test ++;
		list10remplientier.insertion(6, "insertion");
		if(list10remplientier.element(5).equals("test")) {
			reussi ++;
			
		}
		System.out.println("test pour  ListeTableau : insertion(int i, Object nouveau) "+ reussi + "/" + test);
		// test de ajout(int i, E nouveau);
		test = 0;
		reussi = 0;
		test ++;
		listnull.ajout(4, "ajout");
		if(listnull.element(5) == null) {
			reussi ++;
		}
		test ++;
		list0.ajout(0, "ajout");
		if(list0.element(1)== null) {
			reussi ++;
		}
		test ++;
		list10vide.ajout(6, "ajout");
		if(list10vide.element(7).equals("ajout")) {
			reussi ++;
			
		}
		test ++;
		list10remplientier.ajout(6, "ajout");
		if(list10remplientier.element(7).equals("test")) {
			reussi ++;
			
		}
		System.out.println("test pour  ListeTableau : ajout(int i, E nouveau) "+ reussi + "/" + test);
		// test de suppression(int i);
		test ++;
		listnull.suppression(5);
		if(listnull.element(5) == null) {
			reussi ++;
		}
		test ++;
		list0.suppression(0);
		if(list0.element(0)== null) {
			reussi ++;
		}
		test ++;
		list10vide.suppression(5);
		if(list10vide.element(5)== null) {
			reussi ++;
		}
		test ++;
		list10remplientier.suppression(6);
		if(list10remplientier.element(6) == null) {
			reussi ++;
		}
		System.out.println("test pour  ListeTableau : suppression(int i) "+ reussi + "/" + test);
		
		// test de modification(int i, Object nouveau)
		test = 0;
		reussi = 0;
		test ++;
		if(listnull.element(5) == null) {
			reussi ++;
		}
		test ++;
		if(list0.element(0)== null) {
			reussi ++;
		}
		test ++;
		list10vide.modification(9, "element modifier");
		if(list10vide.element(9).equals("element modifier")) {
			reussi ++;
		}
		test ++;
		list10remplientier.modification(9, "element modifier");
		if(list10remplientier.element(9).equals("element modifier")) {
			reussi ++;
		}
		System.out.println("test pour  ListeTableau : element(int i) "+ reussi + "/" + test);
	
		// test de suivent()
		
		test = 0;
		reussi = 0;
		test ++;
		listnull.suivent();
		if(listnull.getSentinelle() == 0) {
			reussi ++;
		}
		test ++;
		list0.suivent();
		if(list0.getSentinelle() == 0) {
			reussi ++;
		}
		test ++;
		list10vide.setSentinelle(0);
		list10vide.suivent();
		if(list10vide.getSentinelle() == 1) {
			reussi ++;
		}
		test ++;
		list10vide.setSentinelle(9);
		list10vide.suivent();
		if(list10vide.getSentinelle() == 9) {
			reussi ++;
		}
		test ++;
		list10vide.setSentinelle(10);
		list10vide.suivent();
		if(list10vide.getSentinelle() == 10) {
			reussi ++;
		}
		System.out.println("test pour  ListeTableau : Suivent() "+ reussi + "/" + test);
		listnull = new ListeTableau(-1 );
		list0 = new ListeTableau(0 );
		list10vide = new ListeTableau(10 );
		list10rempli1 = new ListeTableau(10 );
		list10rempli1.ajout(5, "test");
		list10remplientier = new ListeTableau(10 );
		for(int i = 0; i<10 ; i++) {
			list10remplientier.modification(i, "test");
		}
		// test de precedent()
		test = 0;
		reussi = 0;
		test ++;
		listnull.precedent();
		if(listnull.getSentinelle() == 0) {
			reussi ++;
		}
		test ++;
		list10vide.setSentinelle(9);
		list10vide.precedent();
		if(list10vide.getSentinelle() == 8) {
			reussi ++;
		}
		test ++;
		list10vide.setSentinelle(0);
		list10vide.precedent();
		if(list10vide.getSentinelle() == 0) {
			reussi ++;
		}
		test ++;
		list10vide.setSentinelle(-1);
		list10vide.precedent();
		if(list10vide.getSentinelle() == -1) {
			
			reussi ++;
		}
		System.out.println("test pour  ListeTableau : precedent() "+ reussi + "/" + test);
		//element()
		test = 0;
		reussi = 0;
		test ++;
		if(listnull.element() == null) {
			reussi ++;
		}
		test ++;
		if(list0.element()== null ) {
			reussi ++;
		}
		test ++;
		if(list10vide.element()== null) {
			reussi ++;
			
		}
		
		test ++;
		list10remplientier.setSentinelle(8);
		list10remplientier.modification("rechercher");
		if(list10remplientier.element().equals("rechercher")) {
			reussi ++;
		}
		System.out.println("test pour  ListeTableau : element() "+ reussi + "/" + test);
		//insertion
		test = 0;
		reussi = 0;
		test ++;
		listnull.insertion("erreur");
		if(listnull.element() == null) {
			reussi ++;
		}
		test ++;
		list0.insertion("erreur");
		if(list0.element()== null ) {
			reussi ++;
		}
		test ++;
		list10vide.setSentinelle(8);
		list10vide.insertion("insere");
		if(list10vide.element().equals("insere")) {
			reussi ++;
			
		}
		test ++;
		list10remplientier.setSentinelle(8);
		list10remplientier.insertion("insere");
		if(!list10remplientier.element().equals("insere")) {
			reussi ++;
		}
		System.out.println("test pour  ListeTableau : insertion() "+ reussi + "/" + test);
		//ajout
		test = 0;
		reussi = 0;
		test ++;
		listnull.ajout("erreur");
		if(listnull.element() == null) {
			reussi ++;
		}
		test ++;
		list0.ajout("erreur");
		if(list0.element()== null ) {
			reussi ++;
		}
		test ++;
		list10vide.setSentinelle(8);
		list10vide.ajout("ajout");
		if(list10vide.element().equals("ajout")) {
			reussi ++;
			
		}
		test ++;
		list10remplientier.setSentinelle(8);
		list10remplientier.ajout("ajout");
		if(!list10remplientier.element().equals("ajout")) {
			reussi ++;
		}
		System.out.println("test pour  ListeTableau : ajout() "+ reussi + "/" + test);
		
		// test de suppression();
		test ++;
		listnull.suppression();
		if(listnull.toString().equals("null")) {
			reussi ++;
		}
		test ++;
		list0.suppression();
		if(list0.toString().equals("[]")) {
			reussi ++;
		}
		test ++;
		list10vide.suppression();
		if(list10vide.toString().equals("[null, null, null, null, null, null, null, insere, null, null]")) {
			reussi ++;
		}
		test ++;
		list10remplientier.suppression();
		if(list10remplientier.toString().equals("[test, test, test, test, test, test, test, test, test, null]")) {

			reussi ++;
		}
		System.out.println("test pour  ListeTableau: suppression() "+ reussi + "/" + test);
		// test de modification(); 
		test = 0;
		reussi = 0;
		test ++;
		listnull.modification("modif");
		if(listnull.element() == null ) {
			reussi ++;
		}
		test ++;
		list0.modification("modif");
		if(list0.element() == null) {
			reussi ++;
		}
		test ++;
		list10vide.setSentinelle(3);
		list10vide.modification("modif");
		if(list10vide.element().equals("modif")) {
			reussi ++;
		}
		test ++;
		list10remplientier.setSentinelle(3);
		list10remplientier.modification("modif");
		if(list10remplientier.element().equals("modif")) {
			reussi ++;
		}
		System.out.println("test pour ListeTableau : modification() "+ reussi + "/" + test);
	}
	/**
	 * test for the class  ListeIndiceChainageSimple
	 */
	public static void testListeIndiceChainageSimple() {
		
	}
	/**
	 * test for the class ListeSentinelleChainageDouble
	 */
	public static void testListeSentinelleChainageDouble() {
		
	}
}
