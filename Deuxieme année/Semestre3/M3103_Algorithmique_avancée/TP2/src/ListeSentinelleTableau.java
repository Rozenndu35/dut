import java.util.Arrays;

public class ListeSentinelleTableau<E> implements ListeSentinelle<E> {
	private E[] tab;
	private int sentinelle;
	/**
	 * the constructor
	 */
	public ListeSentinelleTableau(int n ) {
		this.sentinelle = 0;
		if (n>=0) {
			this.tab = (E[])new Object[n];
		}else {
			System.err.println("ListeSentinelleTableau : ListeSentinelleTableau : Sorry but the length can't be negative");
			this.tab =  null;
		}
	}
	
	/**
	 * returns true if the list contains no elements, otherwise false
	 * @return true if the list contains no elements
	 */
	public boolean estvide() {
		boolean ret = true;
		if(this.tab != null) {
			for(int i = 0; i<this.tab.length ; i++) {
				if(this.tab[i]!= null) {
					ret=false;
				}
			}
		}else {
			ret = false;
			System.err.println("ListeSentinelleTableau :estvide : Sorry the list is null");
		}
		
		return ret;
	}

	/**
	 * returns true if the list is full (that is, it is no longer possible to insert new elements),
	 * @return true if the list is full
	 */
	public boolean estpleine() {
		boolean ret = true;
		if(this.tab != null){
			for(int i = 0; i<this.tab.length ; i++) {
				if(this.tab[i]==null) {
					ret=false;
				}
			}
		}else {
			ret = false;
			System.err.println("ListeSentinelleTableau : estpleine : Sorry the list is null");
		}
		return ret;
	}

	/**
	 * return the first item in the list, and chage the sentinelle
	 * @return the first item in the list,
	 */
	public E debut() {
		E ret = null;
		this.sentinelle = 0;
		if(this.tab != null && this.tab.length != 0){
			ret = this.tab[this.sentinelle];
		}else {
			System.err.println("ListeSentinelleTableau : debut : Sorry the list is null");
		}
		return ret;
	}

	/**
	 * return the last item in the list, and chage the sentinelle
	 * @return the last item in the list,
	 */
	public E fin() {
		E ret = null;
		if(this.tab != null) {
			if(this.tab != null && this.tab.length !=0){
				this.sentinelle = this.tab.length-1;
				ret = this.tab[this.sentinelle];
			}else {
				System.err.println("ListeSentinelleTableau : fin : Sorry the list is null");
			}
		}
		
		return ret;
	}
	
	/**
	 * returns the  element of the list,
	 * @return the  element of the list
	 */
	public E element() {
		E ret = null;
		if(this.tab != null && this.sentinelle>0 && this.sentinelle<this.tab.length) {
			ret = this.tab[this.sentinelle];
		}else {
			System.err.println("ListeSentinelleTableau : element : Sorry the list is null or the sentinelle is invalibel");
		}
		return ret;
	}

	/**
	 * inserts a new element
	 * @param nouveau the new elements
	*/
	public void insertion(E nouveau) {
		//rentre si la list n'est pas null i est valid et qu'il y a de la place
		if(this.tab != null && this.sentinelle>=0 && this.sentinelle<this.tab.length && !estpleine() ) {
			// rentre si la case est bide
			if(this.tab[this.sentinelle -1] == null) {
				precedent();
				this.tab[this.sentinelle]= (E)nouveau;
			}else {
				boolean stop = false;
				int j = this.sentinelle-1;
				//tant qu'il ne trouve pas la place dans les case suivente
				while(j<this.tab.length -1 && !stop) {
					if (this.tab[j+1] == null) {
						this.tab[j+1] = this.tab[j];
						this.tab[j] =(E) nouveau;
						stop = true;
						nouveau = null;
					}
					else {
						E stock =this. tab[j+1];
						this.tab[j+1] = this.tab[j];
						this.tab[j] =(E) nouveau;
						nouveau = stock;
						j=j+2;
					}
				}
				//si il n'a pas trouver dans les case suivante regarde le case qui le precedent
				if(nouveau != null) {
					j = 0;
					while(j<this.sentinelle -1 && !stop) {
						if (this.tab[j+1] == null) {
							this.tab[j+1] = this.tab[j];
							this.tab[j] =(E) nouveau;
							stop = true;
							nouveau = null;
						}
						else {
							E stock = this.tab[j+1];
							this.tab[j+1] = this.tab[j];
							this.tab[j] =(E) nouveau;
							nouveau = stock;
							j=j+2;
						}
					}
				}
				
			}
		}else {
			System.err.println("ListeSentinelleTableau : insertion : Sorry the list is null or thi indice is invalibel or the list is full");
		}
		
	}

	/**
	 * which adds a new element 
	 * @param nouveau the new elements
	 */
	public void ajout(E nouveau) {
		//rentre si i est valid et qu'il y a de la place
		if(this.tab != null && this.sentinelle>=0 && this.sentinelle<this.tab.length-1 && !estpleine() ) {
			if(this.tab[this.sentinelle+1] == null) {
				this.tab[this.sentinelle+1]= (E)nouveau;
			}else {
				boolean stop = false;
				E[] tabtest = (E[])new Object[tab.length];
				int j = this.sentinelle+1;
				//tant qu'il ne trouve pas la place dans les case suivente
				while(j<this.tab.length -1 && !stop) {
					if (this.tab[j+1] == null) {
						tabtest[j+1] = this.tab[j];
						tabtest[j] =(E) nouveau;
						stop = true;
						nouveau = null;
					}
					else {
						E stock = this.tab[j+1];
						tabtest[j+1] = this.tab[j];
						tabtest[j] =(E) nouveau;
						nouveau = stock;
						j=j+2;
					}
				}
				
				//si il n'a pas trouver dans les case suivante regarde le case qui le precedent
				if(nouveau != null) {
					j = 0;
					while(j<this.sentinelle && !stop) {
						if (this.tab[j+1] == null) {
							tabtest[j+1] = this.tab[j];
							tabtest[j] =(E) nouveau;
							stop = true;
							nouveau = null;
						}
						else {
							E stock = this.tab[j+1];
							tabtest[j+1] = this.tab[j];
							tabtest[j] =(E) nouveau;
							nouveau = stock;
							j=j+2;
						}
						for (int a = this.sentinelle; this.sentinelle>=j ; this.sentinelle --) {
							this.tab[a] = tabtest[a];
						}
					}
				}else {
					for (int a = this.sentinelle; this.sentinelle<=j ; this.sentinelle++) {
						this.tab[a] = tabtest[a];
					}
				}
				
			}
			suivent();
		}else {
			System.err.println("ListeSentinelleTableau : ajout : Sorry the list is null or thi indice is invalibel or the list is full");
		}
	}

	/**
	 * delete the element from the list
	 */
	public void suppression() {
		if(this.tab != null && this.sentinelle>=0 && this.sentinelle<this.tab.length) {
			for(int i = this.sentinelle ; i<this.tab.length-1 ; i++) {
				this.tab[i] = this.tab[i+1];
			}
			this.tab[this.tab.length-1] = null;
		}else {
			System.err.println("ListeSentinelleTableau : suppression : Sorry the list is null or thi indice is invalibel");
		}
		
	}

	/**
	 * replacethe element of the list by the one passed in parameter
	 * @param nouveau the new element
	 */
	public void modification(E nouveau) {
		if(this.tab != null && this.sentinelle >=0 && this.sentinelle<this.tab.length) {
			this.tab[this.sentinelle] = (E) nouveau;
		}else {
			System.err.println("ListeSentinelleTableau : modification : Sorry the list is null or the sentinelle is invalibel");
		}
		
	}

	/**
	 * return the previous cursor
	 */
	public void precedent() {
		if(this.tab != null && this.sentinelle < this.tab.length) {
			if(this.sentinelle <= 0) {
				System.err.println("ListeSentinelleTableau : precedent : Sorry the sentinelle is already 0");
			}
			else {
				this.sentinelle --;
			}
				
		}else {
			System.err.println("ListeSentinelleTableau : precedent: Sorry the list is null");
		}
	}

	/**
	 * returns the cursor follow
	 */
	public void suivent() {
		if(this.tab != null && this.sentinelle >=0) {
			if(this.sentinelle >= this.tab.length -1) {
				System.err.println("ListeSentinelleTableau : precedent : Sorry the sentinelle is already to the length");
			}
			else {
				this.sentinelle ++;
			}
		}else {
			System.err.println("ListeSentinelleTableau : estvide : Sorry the list is null");
		}
	}

	/**
	 * sprint the table (using in the class test to check)
	 */
	public String toString() {
		String ret = "null" ;
		if(this.tab != null) {
			ret = Arrays.toString(this.tab);
		}
		return ret;
	}
	/**
	 * get the sentinelle (using in the class test to check)
	 */
	public int getSentinelle() {
		return this.sentinelle;
	}
	/**
	 * change the sentinelle (using in the class test to check)
	 */
	public void setSentinelle(int i) {
		this.sentinelle = i;
	}
}
