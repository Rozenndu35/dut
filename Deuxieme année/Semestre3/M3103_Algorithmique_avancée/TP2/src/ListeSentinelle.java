
public interface ListeSentinelle<E> {
	/**
	 * returns true if the list contains no elements, otherwise false
	 * @return true if the list contains no elements
	 */
	public boolean estvide();
	/**
	 * returns true if the list is full (that is, it is no longer possible to insert new elements),
	 * @return true if the list is full
	 */
	public boolean estpleine();
	/**
	 * return the first item in the list,
	 * @return the first item in the list,
	 */
	public E debut();
	/**
	 * return the last item in the list,
	 * @return the last item in the list,
	 */
	public E fin(); 
	/**
	 * returns the  element of the list,
	 * @return the  element of the list
	 */
	public E element(); 	
	/**
	 * inserts a new element
	 * @param nouveau the new elements
	 */
	public void insertion(E nouveau); 
	/**
	 * which adds a new element 
	 * @param nouveau the new elements
	 */
	public void ajout(E nouveau);
	/**
	 * delete the element from the list
	 */
	public void suppression();
	/**
	 * replacethe element of the list by the one passed in parameter
	 * @param nouveau the new element
	 */
	public void modification(E nouveau);
	
	/**
	 * return the previous cursor
	 * @return the previous cursor
	 */
	public void precedent();
	
	/**
	 * returns the cursor follow
	 * @return returns the cursor follow
	 */
	public void suivent();
}
