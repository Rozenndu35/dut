
/**
 * @author rozenn
 *
 */
public class ListeSentinelleChainageDouble<E> {
	Cellule<E> element;
	/**
	 *
	 * @param <E>
	 */
	public class Cellule<E> {
		E valeur;
		Cellule<E> suivant;
		Cellule<E> precedent;

		/**
		 * 
		 * @param valeur
		 * @param suivant
		 */
		public Cellule(E valeur , Cellule<E> suivant, Cellule<E> precedent) {
			this.valeur = valeur;
			this.suivant = suivant;
			this.precedent = precedent;
			
		}
		/**
		 * 
		 * @return
		 */
		public E getValeur() {
			return valeur;
		}
		/**
		 * 
		 * @param valeur
		 */
		public void setValeur(E valeur) {
			this.valeur = valeur;
		}
		/**
		 * 
		 * @return
		 */
		public Cellule<E> getSuivant() {
			return suivant;
		}
		/**
		 * 
		 * @param suivant
		 */
		public void setSuivant(Cellule<E> suivant) {
			this.suivant = suivant;
		}
		/**
		 * @return the precedent
		 */
		public Cellule<E> getPrecedent() {
			return precedent;
		}
		/**
		 * @param precedent the precedent to set
		 */
		public void setPrecedent(Cellule<E> precedent) {
			this.precedent = precedent;
		}
	}
		
	public ListeSentinelleChainageDouble(Cellule nouveau) {
		this.element = nouveau;
	}

	/**
	 * returns true if the list contains no elements, otherwise false
	 * @return true if the list contains no elements
	 */
	public boolean estvide() {
		boolean ret = true;
		if(this.element != null && this.element.getPrecedent() != null && this.element.getSuivant() != null && this.element.getValeur()!= null) {
			ret = false;
		}		
		return ret;
	}
	
	/**
	 * returns true if the list is full (that is, it is no longer possible to insert new elements),
	 * @return true if the list is full
	 */
	
	/**
	 * return the first item in the list, and chage the sentinelle
	 * @return the first item in the list,
	 */
	public E debut() {
		E ret = null;
		if(this.element != null ){
			Cellule nouveau = (Cellule) this.element.getPrecedent();
			if( nouveau != null) {
				ListeSentinelleChainageDouble liste = new ListeSentinelleChainageDouble(nouveau);
				ret = (E) liste.fin();
			}
			else {
				ret = (E) nouveau.getValeur();
			}
		}else {
			System.err.println("ListeSentinelleTableau : debut : Sorry the list is null");
		}
		return ret;
	}
	
	/**
	 * return the last item in the list, and chage the sentinelle
	 * @return the last item in the list,
	 */
	public E fin() {
		E ret = null;
		if(this.element != null ){
			Cellule nouveau = (Cellule) this.element.getSuivant();
			if( nouveau != null) {
				ListeSentinelleChainageDouble liste = new ListeSentinelleChainageDouble(nouveau);
				ret = (E) liste.fin();
			}
			else {
				ret = (E) nouveau.getValeur();
			}
		}else {
				System.err.println("ListeSentinelleTableau : fin : Sorry the list is null");
		}		
		return ret;
	}
	
	/**
	 * returns the  element of the list,
	 * @return the  element of the list
	 */
	public E element() {
		E ret = null;
		if(this.element != null ) {
			ret = this.element.getValeur();
		}else {
			System.err.println("ListeSentinelleTableau : element : Sorry the list is null or the sentinelle is invalibel");
		}
		return ret;
	}
	
	/**
	 * inserts a new element
	 * @param nouveau the new elements
	*/
	public void insertion(E nouveau) {
		//rentre si la list n'est pas null i est valid et qu'il y a de la place
		if(this.element != null ) {
			Cellule precedent = this.element.getPrecedent();
			this.element.getPrecedent().setSuivant((ListeSentinelleChainageDouble<E>.Cellule<E>) nouveau);
			Cellule nouv = new Cellule(nouveau, this.element, precedent);
			this.element.setPrecedent(nouv);
			this.element = nouv;
			
		}else {
			System.err.println("ListeSentinelleTableau : insertion : Sorry the list is null or the list is full");
		}
		
	}
	
	/**
	 * which adds a new element 
	 * @param nouveau the new elements
	 */
	public void ajout(E nouveau) {
		//rentre si i est valid et qu'il y a de la place
		if(this.element != null ) {
			Cellule suivant = this.element.getSuivant();
			this.element.getSuivant().setPrecedent((ListeSentinelleChainageDouble<E>.Cellule<E>) nouveau);
			Cellule nouv = new Cellule(nouveau, suivant, this.element);
			this.element.setSuivant(nouv);
			this.element = nouv;
		}else {
			System.err.println("ListeSentinelleTableau : ajout : Sorry the list is null");
		}
	}
	
	/**
	 * delete the element from the list
	 */
	public void suppression() {
		if(this.element != null ) {
			this.element.getPrecedent().setSuivant(this.element.getSuivant());
			this.element.getSuivant().setPrecedent(this.element.getPrecedent());
			this.element = this.element.getSuivant();
			
		}else {
			System.err.println("ListeSentinelleTableau : suppression : Sorry the list is null");
		}
		
	}
	
	/**
	 * replacethe element of the list by the one passed in parameter
	 * @param nouveau the new element
	 */
	public void modification(E nouveau) {
		if(this.element != null ) {
			this.element.setValeur(nouveau);
		}else {
			System.err.println("ListeSentinelleTableau : modification : Sorry the list is null ");
		}
		
	}
	
	/**
	 * return the previous cursor
	 */
	public void precedent() {
		if(this.element != null ) {
			this.element = this.element.getPrecedent();
				
		}else {
			System.err.println("ListeSentinelleTableau : precedent: Sorry the list is null");
		}
	}
	
	/**
	 * returns the cursor follow
	 */
	public void suivent() {
		if(this.element != null ) {
			this.element = this.element.getSuivant();
		}else {
			System.err.println("ListeSentinelleTableau : estvide : Sorry the list is null");
		}
	}

}
