import java.util.Arrays;
/**
 * @author rozenn
 *
 */
public class ListeIndiceChainageSimple<E> {
	Cellule<E> element;
	/**
	 *
	 * @param <E>
	 */
	public class Cellule<E> {
		E valeur;
		Cellule<E> suivant;

		/**
		 * 
		 * @param valeur
		 * @param suivant
		 */
		public Cellule(E valeur , Cellule<E> suivant) {
			this.valeur = valeur;
			this.suivant = suivant;
		}
		/**
		 * 
		 * @return
		 */
		public E getValeur() {
			return valeur;
		}
		/**
		 * 
		 * @param valeur
		 */
		public void setValeur(E valeur) {
			this.valeur = valeur;
		}
		/**
		 * 
		 * @return
		 */
		public Cellule<E> getSuivant() {
			return suivant;
		}
		/**
		 * 
		 * @param suivant
		 */
		public void setSuivant(Cellule<E> suivant) {
			this.suivant = suivant;
		}
	}
	public ListeIndiceChainageSimple(E nouveau) {
		this.element = (ListeIndiceChainageSimple<E>.Cellule<E>) nouveau;
	}

	/**
	 * returns true if the list is full (that is, it is no longer possible to insert new elements),
	 * @return true if the list is full
	 */
	public boolean estvide() {
		boolean ret = true;
		if(this.element.getValeur() != null || this.element.getSuivant() != null ) {
			ret=false;
		}		
		return ret;
	}

	/**
	 * return the first item in the list,
	 * @return the first item in the list,
	 */
	public E debut() {	
		E ret = null;
		if(this.element != null){
			Cellule nouveau = (Cellule) this.element.getSuivant();
			if( nouveau != null) {
				ListeIndiceChainageSimple liste = new ListeIndiceChainageSimple(nouveau);
				ret = (E) liste.fin();
			}
			else {
				ret = (E) nouveau.getValeur();
			}
		}else {
			System.err.println("ListeIndiceChainageSimple : fin : Sorry the list is null");
		}
		return ret;
		
	}

	/**
	 * return the last item in the list,
	 * @return the last item in the list,
	 */
	public E fin() {
		E ret = null;
		if(this.element != null){
			ret = this.element.getValeur();
		}else {
			System.err.println("ListeIndiceChainageSimple : debut : Sorry the list is null");
		}
		return ret;
	}

	/**
	 * returns the element of the list,
	 * @return the i th element of the list
	 */
	public E element() {
		E ret = null;
		if(this.element != null ) {
			ret = element.getValeur();
		}else {
			System.err.println("ListeIndiceChainageSimple : element : Sorry the list is null");
		}
		return ret;
	}
	
	/**
	 * which adds a new element after the element of the listing
	 * @param nouveau the new elements
	 */
	public void ajout(Object nouveau) {
		//rentre si i est valid et qu'il y a de la place
		if(this.element!= null) {
			Cellule nouvelle = new Cellule(nouveau ,null);
			this.element.setSuivant(nouvelle);
			this.element = nouvelle;
			
		}else {
			System.err.println("ListeIndiceChainageSimple : ajout : Sorry the list is null ");
		}
		
	}

	/**
	 * replace the element of the list by the one passed in parameter
	 * @param nouveau the new element
	 */
	public void modification( Object nouveau) {
		if(this.element != null ) {
			this.element.setValeur((E) nouveau);
		}else {
			System.err.println("ListeIndiceChainageSimple : modification : Sorry the list is null ");
		}	
	}	
}
