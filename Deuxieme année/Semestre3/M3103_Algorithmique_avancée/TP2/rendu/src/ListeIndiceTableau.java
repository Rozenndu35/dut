import java.util.Arrays;

public class ListeIndiceTableau<E>  implements ListeIndice {
	private E[] tab;
	
	/**
	 * returns true if the list contains no elements, otherwise false
	 * @return true if the list contains no elements
	 */
	public ListeIndiceTableau(int n ) {
		if (n>=0) {
			this.tab = (E[])new Object[n];
		}else {
			System.err.println("ListeIndice : ListeIndiceTableau : Sorry but the length can't be negative");
			this.tab =  null;
		}
	}

	/**
	 * returns true if the list is full (that is, it is no longer possible to insert new elements),
	 * @return true if the list is full
	 */
	public boolean estvide() {
		boolean ret = true;
		if(this.tab != null) {
			for(int i = 0; i<this.tab.length ; i++) {
				if(this.tab[i]!= null) {
					ret=false;
				}
			}
		}else {
			ret = false;
			System.err.println("ListeIndice :estvide : Sorry the list is null");
		}
		
		return ret;
	}

	/**
	 * return true if the list is full
	 * @return true if the list is full
	 */
	public boolean estpleine() {
		boolean ret = true;
		if(this.tab != null){
			for(int i = 0; i<this.tab.length ; i++) {
				if(this.tab[i]==null) {
					ret=false;
				}
			}
		}else {
			ret = false;
			System.err.println("ListeIndice : estpleine : Sorry the list is null");
		}
		return ret;
	}

	/**
	 * return the first item in the list,
	 * @return the first item in the list,
	 */
	public E debut() {	
		E ret = null;
		if(this.tab != null && this.tab.length != 0){
			ret = this.tab[0];
		}else {
			System.err.println("ListeIndice : debut : Sorry the list is null");
		}
		return ret;
		
	}

	/**
	 * return the last item in the list,
	 * @return the last item in the list,
	 */
	public E fin() {
		E ret = null;
		if(this.tab != null && this.tab.length !=0){
			ret = this.tab[this.tab.length - 1];
		}else {
			System.err.println("ListeIndice : fin : Sorry the list is null");
		}
		return ret;
	}

	/**
	 * returns the i th element of the list,
	 * @param i the index
	 * @return the i th element of the list
	 */
	public E element(int i) {
		E ret = null;
		if(this.tab != null && i>0 && i<this.tab.length) {
			ret = this.tab[i];
		}else {
			System.err.println("ListeIndice : element : Sorry the list is null or thi indice is invalibel");
		}
		return ret;
	}
	/**
	 * inserts a new element before the i th element from the list,
	 * @param i the index after you whant tu put the new element
	 * @param nouveau the new elements
	 */
	public void insertion(int i, Object nouveau) {
		//rentre si la list n'est pas null i est valid et qu'il y a de la place
		if(this.tab != null && i>=0 && i<this.tab.length && !estpleine() ) {
			// rentre si la case est bide
			if(this.tab[i-1] == null) {
				this.tab[i-1]= (E)nouveau;
			}else {
				boolean stop = false;
				int j = i-1;
				//tant qu'il ne trouve pas la place dans les case suivente
				while(j<this.tab.length -1 && !stop) {
					if (this.tab[j+1] == null) {
						this.tab[j+1] = this.tab[j];
						this.tab[j] =(E) nouveau;
						stop = true;
						nouveau = null;
					}
					else {
						E stock =this. tab[j+1];
						this.tab[j+1] = this.tab[j];
						this.tab[j] =(E) nouveau;
						nouveau = stock;
						j=j+2;
					}
				}
				//si il n'a pas trouver dans les case suivante regarde le case qui le precedent
				if(nouveau != null) {
					j = 0;
					while(j<i -1 && !stop) {
						if (this.tab[j+1] == null) {
							this.tab[j+1] = this.tab[j];
							this.tab[j] =(E) nouveau;
							stop = true;
							nouveau = null;
						}
						else {
							E stock = this.tab[j+1];
							this.tab[j+1] = this.tab[j];
							this.tab[j] =(E) nouveau;
							nouveau = stock;
							j=j+2;
						}
					}
				}
				
			}
		}else {
			System.err.println("ListeIndice : insertion : Sorry the list is null or thi indice is invalibel or the list is full");
		}
		
	}

	/**
	 * which adds a new element after the i th element of the listing
	 * @param i the index where you whant tu put the new element
	 * @param nouveau the new elements
	 */
	public void ajout(int i, Object nouveau) {
		//rentre si i est valid et qu'il y a de la place
		if(this.tab != null && i>=0 && i<this.tab.length-1 && !estpleine() ) {
			if(this.tab[i+1] == null) {
				this.tab[i+1]= (E)nouveau;
			}else {
				boolean stop = false;
				E[] tabtest = (E[])new Object[tab.length];
				int j = i+1;
				//tant qu'il ne trouve pas la place dans les case suivente
				while(j<this.tab.length -1 && !stop) {
					if (this.tab[j+1] == null) {
						tabtest[j+1] = this.tab[j];
						tabtest[j] =(E) nouveau;
						stop = true;
						nouveau = null;
					}
					else {
						E stock = this.tab[j+1];
						tabtest[j+1] = this.tab[j];
						tabtest[j] =(E) nouveau;
						nouveau = stock;
						j=j+2;
					}
				}
				
				//si il n'a pas trouver dans les case suivante regarde le case qui le precedent
				if(nouveau != null) {
					j = 0;
					while(j<i && !stop) {
						if (this.tab[j+1] == null) {
							tabtest[j+1] = this.tab[j];
							tabtest[j] =(E) nouveau;
							stop = true;
							nouveau = null;
						}
						else {
							E stock = this.tab[j+1];
							tabtest[j+1] = this.tab[j];
							tabtest[j] =(E) nouveau;
							nouveau = stock;
							j=j+2;
						}
						for (int a = i; i>=j ; i--) {
							this.tab[a] = tabtest[a];
						}
					}
				}else {
					for (int a = i; i<=j ; i++) {
						this.tab[a] = tabtest[a];
					}
				}
				
			}
		}else {
			System.err.println("ListeIndice : ajout : Sorry the list is null or thi indice is invalibel or the list is full");
		}
		
	}

	/**
	 * delete the i th item from the list
	 * @param i the index wher you you want delete
	 */
	public void suppression(int i) {
		if(this.tab != null && i>=0 && i<this.tab.length) {
			this.tab[i] = null;
		}else {
			System.err.println("ListeIndice : suppression : Sorry the list is null or thi indice is invalibel");
		}
		
	}

	/**
	 * replace the i th element of the list by the one passed in parameter
	 * @param i the index to whant replace
	 * @param nouveau the new element
	 */
	public void modification(int i, Object nouveau) {
		if(this.tab != null && i >=0 && i<this.tab.length) {
			this.tab[i] = (E) nouveau;
		}else {
			System.err.println("ListeIndice : modification : Sorry the list is null or thi indice is invalibel");
		}	
	}
	
	/**
	 * sprint the table (using in the class test to check
	 */
	public String toString() {
		String ret = "null" ;
		if(this.tab != null) {
			ret = Arrays.toString(this.tab);
		}
		return ret;
	}
}
